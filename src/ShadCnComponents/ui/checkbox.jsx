import * as React from "react";
import * as CheckboxPrimitive from "@radix-ui/react-checkbox";
import { CheckIcon } from "@radix-ui/react-icons";

import { cn } from "lib/utils";
// **** To Change Color ****
// className='border-themeSuccess data-[state=checked]:bg-themeSuccess'
const Checkbox = React.forwardRef(({ className, ...props }, ref) => (
	<CheckboxPrimitive.Root
		ref={ref}
		className={cn(
			"border-[1px] border-themePrimary peer h-4 w-4 shrink-0 rounded-sm  shadow focus-visible:outline-none focus-visible:ring-1 focus-visible:ring-ring disabled:cursor-not-allowed disabled:opacity-50 data-[state=checked]:bg-themePrimary data-[state=checked]:text-themePrimary",
			className
		)}
		{...props}
	>
		<CheckboxPrimitive.Indicator
			className={cn("flex items-center justify-center text-current")}
			style={{ ...props.iconStyle, color: "white", position: "relative", bottom: "1px" }}
		>
			{props.customIcon ? props.customIcon : <CheckIcon className={cn("h-4 w-4", props.iconClassName)} />}
		</CheckboxPrimitive.Indicator>
	</CheckboxPrimitive.Root>
));
Checkbox.displayName = CheckboxPrimitive.Root.displayName;

export { Checkbox };
