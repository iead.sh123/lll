import Filter from "../Filter/Filter";
import Paginator from "../Paginator/Paginator";
import { Button } from "reactstrap";
import RLister from "../RComs/RLister";
import styles from "./PoolPicker.Module.scss";
import Loader from "utils/Loader";
import tr from "../RComs/RTranslator";

const PoolPicker = ({
	table_name,
	title,
	mainCourses,
	schoolClasses,
	use_tags,
	tags,
	handleApplyFilter,
	setSelectedMainCourses,
	setSelectedSchoolClasses,
	setSelectedTags,
	selectedMainCourses,
	selectedSchoolClasses,
	Records,
	ListerClass,
	ItemClass,
	check,
	ckeckedItem,
	onCheckChange,
	firstPageUrl,
	lastPageUrl,
	currentPage,
	lastPage,
	prevPageUrl,
	nextPageUrl,
	total,
	getData,
	parentHandleClose,
	checkedApprove,
	handelSave,
	hiddenCancel,
	hiddenSave,
	hiddenFilter,
	itemToShow,
	marginT,
	marginB,
	marginTStandard,
	marginBStandard,
	showListerMode,
	showCheckIcon,
}) => {
	return (
		<div className="">
			<span hidden={hiddenFilter}>
				<Filter
					mainCourses={mainCourses}
					schoolClasses={schoolClasses}
					use_tags={true}
					tags={tags}
					handleApplyFilter={handleApplyFilter}
					setSelectedMainCourses={setSelectedMainCourses}
					setSelectedSchoolClasses={setSelectedSchoolClasses}
					setSelectedTags={setSelectedTags}
					title={title}
					selectedMainCourses={selectedMainCourses}
					selectedSchoolClasses={selectedSchoolClasses}
				/>
			</span>
			{Records ? (
				Records.length == 0 ? (
					<div className="alert alert-danger text-center second-bg-color" role="alert">
						No items to show
					</div>
				) : (
					<>
						<RLister
							Records={Records}
							ListerClass={ListerClass}
							ItemClass={ItemClass}
							check={check}
							ckeckedItem={ckeckedItem}
							onCheckChange={onCheckChange}
							checkedApprove={checkedApprove}
							itemToShow={itemToShow}
							marginT={marginT}
							marginB={marginB}
							marginTStandard={marginTStandard}
							marginBStandard={marginBStandard}
							showListerMode={showListerMode ? showListerMode : "tableLister"}
							showCheckIcon={showCheckIcon ? showCheckIcon : false}
						/>

						<Paginator
							firstPageUrl={firstPageUrl}
							lastPageUrl={lastPageUrl}
							currentPage={currentPage}
							lastPage={lastPage}
							prevPageUrl={prevPageUrl}
							nextPageUrl={nextPageUrl}
							total={total}
							getData={getData}
						/>
						<div className="row">
							<Button
								hidden={hiddenSave ? true : false}
								className={"btn-round mr-3  " + styles.h_btn_inline}
								onClick={handelSave}
								color="info"
								type="submit"
								style={{ textTransform: "capitalize" }}
							>
								{tr`done`}
							</Button>

							<Button
								hidden={hiddenCancel ? true : false}
								className={"btn-round " + styles.h_btn}
								onClick={(e) => {
									parentHandleClose(e);
								}}
								color="info"
								outline
								style={{ textTransform: "capitalize" }}
							>
								{tr`cancel`}
							</Button>
						</div>
					</>
				)
			) : (
				Loader()
			)}
		</div>
	);
};

export default PoolPicker;
