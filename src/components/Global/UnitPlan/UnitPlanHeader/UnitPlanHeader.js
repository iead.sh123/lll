import { Card, CardBody, Col, Form, FormGroup, Input, Row } from "reactstrap";
import { useState } from "react";
import RTags from "../../RComs/RTags";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft, faChevronRight } from "@fortawesome/free-solid-svg-icons";
import { editUnitPlanProps } from "store/actions/global/unitPlan.actions";
import { useDispatch } from "react-redux";
import styles from "./UnitPlanHeader.Module.scss";
import tr from "components/Global/RComs/RTranslator";
import withDirection from "hocs/withDirection";

const UnitPlanHeader = ({ unitPlan, readOnly, isEdit, dir }) => {
	const [groupForm, setGroupForm] = useState({
		title: unitPlan?.title ? unitPlan?.title : null,
		time: unitPlan?.time ? unitPlan?.time : null,
		unit_number: unitPlan?.unit_number ? unitPlan?.unit_number : null,
		description: unitPlan?.description ? unitPlan?.description : null,
		tags: unitPlan?.tags,
	});
	const [tags, setTags] = useState([]);
	const [receiverIds, setReceiversIds] = useState([]);

	const dispatch = useDispatch();
	const changeValue = (e) => {
		setGroupForm({ ...groupForm, [e.target.id]: e.target.value });
	};
	const editUnitProps = () => {
		var resultObj = {
			title: groupForm.title,
			unit_number: groupForm.unit_number,
			description: groupForm.description,
			time: groupForm.time,
			tags: groupForm.tags,
		};
		// dispatch(editUnitPlanProps(resultObj));
	};

	return (
		<Card className="pt-1">
			<CardBody>
				<h6 style={{ textAlign: dir ? "left" : "right" }}>
					{" "}
					<FontAwesomeIcon icon={dir ? faChevronLeft : faChevronRight} />
					{tr("unit_plan_view")}
					{isEdit && !readOnly ? <span className="second-color"> ({tr("save_your_changes")})</span> : <></>}
				</h6>
				<Row>
					<Col md="6">
						<Form>
							<label className=" float-left">{tr("title")}:</label>
							<FormGroup>
								<Input
									readOnly={readOnly ? true : false}
									className={readOnly ? styles.input_text : ""}
									id="title"
									required
									type="text"
									value={groupForm.title}
									placeholder={tr("Title") + " *"}
									onChange={changeValue}
									onBlur={editUnitProps}
								/>
							</FormGroup>
						</Form>
					</Col>
					<Col md="6">
						<Form>
							<label className=" float-left">{tr("unit_number")}:</label>
							<FormGroup>
								<Input
									readOnly={readOnly ? true : false}
									className={readOnly ? styles.input_text : ""}
									id="unit_number"
									required
									type="text"
									value={groupForm.unit_number}
									placeholder={tr("unit_number")}
									onChange={changeValue}
									onBlur={editUnitProps}
									min={0}
								/>
							</FormGroup>
						</Form>
					</Col>
					<Col md="6">
						<Form>
							<label className=" float-left">{tr("description")}</label>
							<FormGroup>
								<Input
									className={readOnly ? styles.input_text : ""}
									readOnly={readOnly ? true : false}
									id="description"
									required
									type="text"
									value={groupForm.description}
									placeholder={tr("description")}
									onBlur={editUnitProps}
									onChange={changeValue}
								/>
							</FormGroup>
						</Form>
					</Col>
					<Col md="6">
						<Form>
							<label className=" float-left">{tr("time")}</label>
							<FormGroup>
								<Input
									className={readOnly ? styles.input_text : ""}
									readOnly={readOnly ? true : false}
									id="time"
									type="text"
									value={groupForm.time}
									placeholder={tr("time")}
									onChange={changeValue}
									onBlur={editUnitProps}
								/>
							</FormGroup>
						</Form>
					</Col>
					<Col md="12">
						<Form>
							<label className=" float-left">{tr("tags")}</label>
							<FormGroup className=" pt-4">
								<RTags
									defaultLabel="User Name"
									defaultValues={groupForm.tags}
									url="api/tags/getPrefixTags?prefix="
									getArrayFromResponse={(res) => (res && res.data && res.data.status ? res.data.data : null)}
									readOnly={readOnly ? true : false}
									getValueFromArrayItem={(i) => i.id}
									getLabelFromArrayItem={(i) => i.name}
									getIdFromArrayItem={(i) => i.id}
									onChange={(val) =>
										changeValue({
											target: {
												id: "tags",
												value: val,
											},
										})
									}
									onBlur={editUnitProps}
								/>
							</FormGroup>
						</Form>
					</Col>
				</Row>
			</CardBody>
		</Card>
	);
};
export default withDirection(UnitPlanHeader);
