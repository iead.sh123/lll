import UnitPlanHeader from "../UnitPlanHeader/UnitPlanHeader";
import { useDispatch, useSelector } from "react-redux";
import Sections from "../Sections/Sections";
import styles from "./UnitPlanHome.Module.scss";
import { Button } from "reactstrap";
import ReactBSAlert from "react-bootstrap-sweetalert";
import { useState } from "react";
import { useHistory } from "react-router";
import tr from "components/Global/RComs/RTranslator";
import { useParams } from "react-router-dom";
import { toast } from "react-toastify";

const UnitPlanHome = ({ unitPlan, readOnly, isEdit, isDone }) => {
	const [alert, setAlert] = useState(null);
	const dispatch = useDispatch();
	const history = useHistory();
	const userType = useSelector((state) => state?.auth?.user?.type);
	const { course_id } = useParams();
	const { courseId } = useParams();

	const saveUnit = () => {
		if (!isEdit) {
			return setAlert(
				<ReactBSAlert
					warning
					title="No changes"
					onCancel={() => hideAlert()}
					cancelBtnText="Cancel"
					showCancel
					showConfirm={false}
					cancelBtnBsStyle="danger"
					btnSize=""
				></ReactBSAlert>
			);
		}
		if (unitPlan.title == "" || unitPlan.title == null) {
			return toast.warning(tr`Title is required`);
		}
		if (unitPlan.description == "" || unitPlan.description == null) {
			return toast.warning(tr`Description is required`);
		}
		if (unitPlan.unit_number == "" || unitPlan.unit_number == null) {
			return toast.warning(tr`Unit Number is required`);
		}
		if (unitPlan.sections.length == 0) {
			return toast.warning(tr`Add Sections is required`);
		}
		// dispatch(saveUnitPlan(unitPlan, course_id ? course_id : courseId));
	};
	const hideAlert = () => {
		setAlert(null);
	};

	const successClose = () => {
		history.push(`${process.env.REACT_APP_BASE_URL}/teacher/course-unitplan`);
		hideAlert();
	};

	const cancelUnitPlan = () => {
		if (isEdit) {
			setAlert(
				<ReactBSAlert
					warning
					title="will lose your changes , are you sure close?"
					onConfirm={() => successClose()}
					onCancel={() => hideAlert()}
					confirmBtnBsStyle="info"
					cancelBtnBsStyle="danger"
					confirmBtnText="Yes,"
					cancelBtnText="Cancel"
					showCancel
					btnSize=""
				></ReactBSAlert>
			);
		} else {
			history.push(`${process.env.REACT_APP_BASE_URL}/teacher/course-unitplan`);
		}
	};

	const confirmClose = () => {};

	return (
		<div className="content">
			{/* <Prompt when={isEdit} message="Changes you made may not be saved." /> */}
			{alert}
			<UnitPlanHeader unitPlan={unitPlan} readOnly={readOnly} isEdit={isEdit} />
			<h6 className="pl-3">{tr("sections")}</h6>
			<Sections sections={unitPlan?.sections} unitPlanId={unitPlan?.id} readOnly={readOnly} courseId={course_id ? course_id : courseId} />
			<div className="pt-3">
				<Button
					hidden={readOnly ? true : false}
					className={"btn-round mr-3 " + styles.h_btn_inline}
					color="info"
					type="submit"
					onClick={saveUnit}
				>
					<strong className="btn-label ">
						<i className="nc-icon nc-simple-add font-weight-bold" />
					</strong>{" "}
					{tr("save")}
				</Button>
				<Button
					hidden={readOnly || userType != "teacher" ? true : false}
					onClick={cancelUnitPlan}
					className={"btn-round " + styles.h_btn}
					color="info"
					outline
				>
					{tr("cancel")}
				</Button>
			</div>
		</div>
	);
};
export default UnitPlanHome;
