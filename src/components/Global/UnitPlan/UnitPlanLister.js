import { Button, Card, CardBody, Table, UncontrolledTooltip } from "reactstrap";
import Paginator from "../Paginator/Paginator";
import { useHistory } from "react-router-dom";
import tr from "../RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";

const UnitPlanLister = ({ unitplans, has_add_permission, has_edit_permission, has_delete_permission }) => {
	const history = useHistory();
	const viewUnitPlan = (unitId) => {
		history.push(`${process.env.REACT_APP_BASE_URL}/teacher/unitplan-viewer/${unitId}`);
	};
	return (
		<div className="content">
			<Card>
				<CardBody>
					<div>
						<Button color="success" disabled={has_add_permission ? false : true}>
							{tr("add_new_unit_plan")}
							<span className="btn-label">
								<i className="nc-icon nc-simple-add"></i>
							</span>
						</Button>
						{unitplans ? (
							<div className="tableFixHead">
								<Table>
									<thead className="text-center">
										<tr>
											<th>{tr("unit_plan_title")}</th>
											<th>{tr("action")}</th>
										</tr>
									</thead>
									<tbody className="text-center">
										{unitplans &&
											unitplans.data &&
											unitplans.data.map((item, index) => {
												const max = 1000000;
												const tooltipid = "Tooltip" + Math.floor(Math.random() * max);
												return (
													<tr key={index}>
														<td>{item.title}</td>
														<td>
															<Button
																disabled={has_delete_permission ? false : true}
																className="btn-icon"
																color="danger"
																size="sm"
																type="button"
															>
																<span className="btn-label">
																	<i className={iconsFa6.delete} aria-hidden="true"></i>
																</span>
															</Button>
															&nbsp;
															<Button
																disabled={has_edit_permission ? false : true}
																className="btn-icon"
																color="success"
																id={tooltipid}
																size="sm"
																type="button"
															>
																<i className="fa fa-edit text-white" />
															</Button>
															<Button
																className="btn-icon"
																color="primary"
																id="nuitplan"
																size="sm"
																onClick={() => {
																	viewUnitPlan(item.id);
																}}
															>
																<i className="fa fa-eye"></i>
															</Button>
															<UncontrolledTooltip delay={0} target={tooltipid}>
																{tr("edit")}
															</UncontrolledTooltip>
															<UncontrolledTooltip delay={0} target="nuitplan">
																{tr("view")}
															</UncontrolledTooltip>
														</td>
													</tr>
												);
											})}
									</tbody>
								</Table>
								<Paginator
									firstPageUrl={unitplans?.first_page_url}
									lastPageUrl={unitplans?.last_page_url}
									currentPage={unitplans?.current_page}
									lastPage={unitplans?.last_page}
									prevPageUrl={unitplans?.prev_page_url}
									nextPageUrl={unitplans?.next_page_url}
									total={unitplans?.total}
									getData={"getData"}
								/>
							</div>
						) : (
							<></>
						)}
					</div>
				</CardBody>
			</Card>
		</div>
	);
};
export default UnitPlanLister;
