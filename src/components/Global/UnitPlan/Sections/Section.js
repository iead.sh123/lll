import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight, faChevronUp } from "@fortawesome/free-solid-svg-icons";
import { Row, Col, Collapse, Input, CardBody, Card, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from "reactstrap";
import styles from "./Section.Module.scss";
import ReactBSAlert from "react-bootstrap-sweetalert";
import Items from "../Sections/Items/Items";
import AppModal from "components/Global/ModalCustomize/AppModal";
import CallPoolPicker from "components/Global/CallPoolPicker/CallPoolPicker";
import { useDispatch } from "react-redux";
import CallRubricsPicker from "components/Global/CallRubricsPicker/CallRubricsPicker";
import { editSection, deleteSection, addNewItem } from "store/actions/global/unitPlan.actions";
import ImageUpload from "components/CustomUpload/ImageUpload";
import tr from "components/Global/RComs/RTranslator";
import { Draggable } from "react-beautiful-dnd";
import RFileUploader from "components/Global/RComs/RFileUploader";

const Section = ({ key, section, readOnly, courseId, index }) => {
	const [openedCollapses, setOpenedCollapses] = useState(null);
	const [alert, setAlert] = useState(null);
	const [show, setShow] = useState(false);
	const [showRubric, setShowRubric] = useState(false);
	const [showUploadFile, setShowUploadFile] = useState(false);
	const [emptyfile, setEmptyfile] = useState(false);
	const [tableName, setTableName] = useState("");
	const [dropdownOpen, setOpen] = useState(false);

	const dispatch = useDispatch();

	const collapsesToggle = (id) => {
		const isOpen = "collapse" + id;
		if (isOpen == openedCollapses) {
			setOpenedCollapses("collapse");
		} else {
			setOpenedCollapses("collapse" + id);
		}
	};

	const toggle = () => setOpen(!dropdownOpen);
	const changeValue = (e) => {
		var resultObj = {
			[e.target.id]: e.target.value,
			sectionId: section.id ? section.id : section.idTemp,
		};
		// dispatch(editSection(resultObj));
	};

	const hideAlert = () => {
		setAlert(null);
	};
	const successDelete = (sectionId) => {
		// dispatch(deleteSection(sectionId, key));
		hideAlert();
	};
	const deleteAnSection = (sectionId) => {
		setAlert(
			<ReactBSAlert
				warning
				title={tr("are_you_sure_to_delete_it")}
				onConfirm={() => successDelete(sectionId)}
				onCancel={() => hideAlert()}
				confirmBtnBsStyle="info"
				cancelBtnBsStyle="danger"
				confirmBtnText={tr("yes_delete_it")}
				cancelBtnText={tr("cancel")}
				showCancel
				btnSize=""
			></ReactBSAlert>
		);
	};
	const handleClose = () => {
		// if (tableName == "lesson_plans") {
		//   setShow(false);
		//   setShowRubric(false);
		//   setShowUploadFile(false);
		// } else {
		setShow(false);
		setShowRubric(false);
		setShowUploadFile(false);
		// }
	};

	const PickLessonPlanItem = (sectionId) => {
		setOpenedCollapses("collapse" + sectionId);
		setShow(true);
		setTableName("lesson_plans");
	};

	const pickRubric = (sectionId) => {
		setOpenedCollapses("collapse" + sectionId);
		setShowRubric(true);
		setTableName("lesson_plan_items_rubric");
	};
	const addFile = (sectionId) => {
		setOpenedCollapses("collapse" + sectionId);
		setShowUploadFile(true);
	};
	const handleCallback = (childData) => {
		// dispatch(
		//   addNewItem(
		//     section.id ? section.id : section.idTemp,
		//     null,
		//     null,
		//     null,
		//     null,
		//     null,
		//     childData
		//   )
		// );
		setEmptyfile(false);
	};

	const handelChooseItemLessonPlan = (child) => {
		// dispatch(
		//   addNewItem(
		//     section.id ? section.id : section.idTemp,
		//     null,
		//     null,
		//     null,
		//     child
		//   )
		// );
	};
	const handlePickRubric = (child) => {
		const rubricEvaluable = {
			rubric: child,
			rubric_id: child.id,
		};

		// dispatch(
		//   addNewItem(
		//     section.id ? section.id : section.idTemp,
		//     null,
		//     null,
		//     null,
		//     null,
		//     rubricEvaluable
		//   )
		// );
	};

	const inputConfirmAlert = (e, sectionId) => {
		// dispatch(addNewItem(sectionId, e));
		hideAlert();
	};
	const addItem = (sectionId) => {
		setOpenedCollapses("collapse" + sectionId);
		setAlert(
			<ReactBSAlert
				input
				inputType="textarea"
				showCancel
				validationRegex={/.*/gim}
				title={tr("enter_item_name")}
				onConfirm={(e) => inputConfirmAlert(e, sectionId)}
				onCancel={() => hideAlert()}
				confirmBtnBsStyle="info"
				cancelBtnBsStyle="danger"
				btnSize=""
			/>
		);
	};

	return (
		<Draggable style={{ position: "relative", top: "10px" }} draggableId={section.idTemp} index={section.order - 1} key={key}>
			{(provided, snapshot) => (
				<div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps} index className="card">
					<CardBody>
						{alert}
						<Row>
							<span
								className="text-dark  pl-3 cursor-pointer "
								aria-expanded={openedCollapses === "collapse" + (section.id ? section.id : section.idTemp)}
								data-parent="#accordion"
								data-toggle="collapse"
								onClick={() => collapsesToggle(section.id ? section.id : section.idTemp)}
							>
								<FontAwesomeIcon
									icon={openedCollapses === "collapse" + (section.id ? section.id : section.idTemp) ? faChevronUp : faChevronRight}
								/>
							</span>

							<AppModal
								show={show ? show : showRubric}
								parentHandleClose={handleClose}
								header={show ? tr("lesson_plan_picker") : tr("rubrics")}
								headerSort={
									show ? (
										<CallPoolPicker
											table_name={tableName}
											title={tr("lesson_plan_picker")}
											parentHandleClose={handleClose}
											unitPlan={true}
											pickItemToUnitPlan={handelChooseItemLessonPlan}
										/>
									) : (
										<CallRubricsPicker
											table_name={tableName}
											courseId={courseId}
											title={tr("pick_rubric_to_unit_plan")}
											parentHandleClose={handleClose}
											parentHandlePickRubric={handlePickRubric}
										/>
									)
								}
								classStyle={styles.lesson_plan_modal}
							></AppModal>

							<AppModal
								show={showUploadFile}
								parentHandleClose={handleClose}
								header={tr("Upload_File")}
								headerSort={
									<div style={{ display: "flex", justifyContent: "center" }}>
										<RFileUploader
											parentCallback={(values) => handleCallback(values) || handleClose()}
											singleFile={true}
											placeholder={"Replace the old selected file "}
											binary={true}
										/>
									</div>
								}
								classStyle={styles.lesson_plan_modal}
							></AppModal>
							<div
								style={{
									display: "flex",
									justifyContent: "space-between",
									width: "90%",
									height: "100%",
								}}
							>
								<Input
									readOnly={readOnly ? true : false}
									type="textarea"
									rows="6"
									style={{ flexGrow: 9 }}
									className={styles.input_text}
									id="name"
									value={section.name}
									onChange={changeValue}
									placeholder={tr("section_title")}
								/>
								<ButtonDropdown
									hidden={readOnly ? true : false}
									isOpen={dropdownOpen}
									toggle={toggle}
									style={{ alignSelf: "flex-end", flexGrow: 1 }}
									className={
										" float-right cursor-pointer text-dark mr-2 " //+ styles.btn_mt
									}
								>
									<DropdownToggle className={styles.btn_h + " btn btn-link"} id="dropdown-basic">
										<i className="fa fa-ellipsis-h fa-lg"></i>
									</DropdownToggle>

									<DropdownMenu aria-haspopup={true} caret color="default" data-toggle="dropdown" className={styles.dropdown_edit}>
										<DropdownItem onClick={() => addItem(section.id ? section.id : section.idTemp)}>
											<span className="float-center">
												&nbsp;&nbsp; <span>{tr("add_new_item")}</span>
											</span>
										</DropdownItem>
										<hr className={styles.hr_item_mt} />
										<DropdownItem
											onClick={() => {
												PickLessonPlanItem(section.id ? section.id : section.idTemp);
											}}
										>
											<span className="float-center">
												&nbsp; &nbsp; <span>{tr("pick_an_lessonplan")}</span>
											</span>
										</DropdownItem>
										<hr className={styles.hr_item_mt} />
										<DropdownItem
											onClick={() => {
												pickRubric(section.id ? section.id : section.idTemp);
											}}
										>
											<span className="float-center">
												&nbsp; &nbsp; <span>{tr("pick_an_rubric")}</span>
											</span>
										</DropdownItem>
										<hr className={styles.hr_item_mt} />
										<DropdownItem
											onClick={() => {
												addFile(section.id ? section.id : section.idTemp);
											}}
										>
											<span className="float-center">
												&nbsp; &nbsp; <span>{tr("add_file")}</span>
											</span>
										</DropdownItem>
										<hr className={styles.hr_item_mt} />
										<DropdownItem
											onClick={() => {
												deleteAnSection(section.id ? section.id : section.idTemp);
											}}
										>
											<span className="float-center">
												&nbsp; &nbsp; <span>{tr("delete_section")}</span>
											</span>
										</DropdownItem>
									</DropdownMenu>
								</ButtonDropdown>
							</div>
							<Input
								readOnly={readOnly ? true : false}
								type="textarea"
								className={styles.input_text_des}
								rows="6"
								onChange={changeValue}
								id="description"
								value={section.description}
								placeholder={tr("section_description")}
							/>
						</Row>
						<hr
							className={styles.hr_mt}
							hidden={openedCollapses === "collapse" + (section.id ? section.id : section.idTemp) ? false : true}
						/>
						<Collapse isOpen={openedCollapses === "collapse" + (section.id ? section.id : section.idTemp)}>
							<Items items={section.items} sectionId={section.id ? section.id : section.idTemp} readOnly={readOnly} courseId={courseId} />
						</Collapse>
					</CardBody>
				</div>
			)}
		</Draggable>
	);
};
export default Section;

/*show as text box
text length
text area when add
*/
