import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight, faChevronUp } from "@fortawesome/free-solid-svg-icons";
import { Row, Col, Collapse, Input, DropdownToggle, DropdownMenu, DropdownItem, ButtonDropdown } from "reactstrap";
import styles from "./Item.Module.scss";
import ReactBSAlert from "react-bootstrap-sweetalert";
import TreeItem from "./TreeItem/TreeItem";
import AppModal from "components/Global/ModalCustomize/AppModal";
import CallPoolPicker from "components/Global/CallPoolPicker/CallPoolPicker";
import {
	pickItemLessonToUnitPlan,
	pickRubricToUnitPlan,
	addNewItem,
	deleteItem,
	editItem,
	addFileToUnitPlan,
} from "store/actions/global/unitPlan.actions";
import { useDispatch } from "react-redux";
import CallRubricsPicker from "components/Global/CallRubricsPicker/CallRubricsPicker";
import ImageUpload from "components/CustomUpload/ImageUpload";
import tr from "components/Global/RComs/RTranslator";
import { deleteSweetAlert } from "components/Global/RComs/RAlert";
import RFileUploader from "components/Global/RComs/RFileUploader";

const Item = ({ item, sectionId, readOnly, courseId }) => {
	const [openedCollapses, setOpenedCollapses] = useState(null);
	const [alert, setAlert] = useState(null);
	const [show, setShow] = useState(false);
	const [showRubric, setShowRubric] = useState(false);
	const [showUploadFile, setShowUploadFile] = useState(false);
	const [emptyfile, setEmptyfile] = useState(false);
	const [itemId, setItemId] = useState(false);
	const [tableName, setTableName] = useState("");
	const [messageConfirm, setMessageConfirm] = useState("");
	const [dropdownOpen, setOpen] = useState(false);
	const [groupForms, setGroupForms] = useState({
		text: item.text,
	});
	const [alerts, setAlerts] = useState(false);
	const dispatch = useDispatch();
	const collapsesToggle = (id) => {
		const isOpen = "collapse" + id;
		if (isOpen == openedCollapses) {
			setOpenedCollapses("collapse");
		} else {
			setOpenedCollapses("collapse" + id);
		}
	};
	const changeValue = (e) => {
		setGroupForms({ ...groupForms, [e.target.id]: e.target.value });
		var resultObj = {
			[e.target.id]: e.target.value,
			sectionId: sectionId,
			itemId: item.id ? item.id : item.idTemp,
		};
		// dispatch(editItem(resultObj));
	};

	const hideAlert = () => {
		setAlert(null);
	};
	const successDelete = (itemId, parentId) => {
		const reultOj = {
			itemId: itemId,
			parentId: parentId,
			sectionId: sectionId,
		};
		// dispatch(deleteItem(reultOj));
		hideAlert();
	};
	const deleteAnItem = (itemId, parentId) => {
		setAlert(
			<ReactBSAlert
				warning
				title={tr("are_you_sure_to_delete_it")}
				onConfirm={() => successDelete(itemId, parentId)}
				onCancel={() => hideAlert()}
				confirmBtnBsStyle="info"
				cancelBtnBsStyle="danger"
				confirmBtnText={tr("yes_delete_it")}
				cancelBtnText={tr("cancel")}
				showCancel
				btnSize=""
			></ReactBSAlert>
		);
	};
	const toggle = () => setOpen(!dropdownOpen);

	const handleClose = () => {
		setShow(false);
		setShowRubric(false);
		setShowUploadFile(false);
	};

	const PickLessonPlanItem = (itemId) => {
		setOpenedCollapses("collapse" + itemId);
		setShow(true);
		setTableName("lesson_plans");
		setItemId(itemId);
	};

	const pickRubric = (itemId) => {
		setOpenedCollapses("collapse" + itemId);
		if (item.rubric_evaluable != null) {
			setMessageConfirm(tr("are_you_sure_to_replace_old"));
		}
		setShowRubric(true);
		setTableName("lesson_plan_items_rubric");
		setItemId(itemId);
	};
	const addFile = (itemId) => {
		setOpenedCollapses("collapse" + itemId);
		setShowUploadFile(true);
		setItemId(itemId);
	};

	const successReplace = (editForm) => {
		// dispatch(pickItemLessonToUnitPlan(editForm));
		hideAlert();
	};

	const handelChooseItemLessonPlan = (child) => {
		const editForm = {
			item: child,
			sectionId: sectionId,
			itemId: itemId,
		};
		if (item.lesson_plan_id != null) {
			setAlert(
				<ReactBSAlert
					warning
					title={tr("are_you_sure_to_replace_old")}
					onConfirm={() => successReplace(editForm)}
					onCancel={() => hideAlert()}
					confirmBtnBsStyle="info"
					cancelBtnBsStyle="danger"
					confirmBtnText={tr("yes_replace_it")}
					cancelBtnText={tr("cancel")}
					showCancel
					btnSize=""
				></ReactBSAlert>
			);
		} else {
			// dispatch(pickItemLessonToUnitPlan(editForm));
		}
	};

	const handlePickRubric = (child) => {
		setOpenedCollapses("collapse" + (item.id ? item.id : item.idTemp));
		const rubric_evaluable = {
			rubric: child,
			rubric_id: child.id,
		};
		const editForm = {
			item: rubric_evaluable,
			sectionId: sectionId,
			itemId: itemId,
		};
		// dispatch(pickRubricToUnitPlan(editForm));
	};

	const successReplaceFile = (editForm) => {
		// dispatch(addFileToUnitPlan(editForm));
		hideAlert();
		setShowUploadFile(false);
	};

	const handleCallback = (childData) => {
		setOpenedCollapses("collapse" + itemId);
		const editForm = {
			item: childData,
			sectionId: sectionId,
			itemId: itemId,
		};
		if (item?.attachments?.length != 0) {
			setAlert(
				<ReactBSAlert
					warning
					title={tr("are_you_sure_to_replace_old")}
					onConfirm={() => successReplaceFile(editForm)}
					onCancel={() => hideAlert()}
					confirmBtnBsStyle="info"
					cancelBtnBsStyle="danger"
					confirmBtnText={tr("yes_replace_it")}
					cancelBtnText={tr("cancel")}
					showCancel
					btnSize=""
				></ReactBSAlert>
			);
		} else {
			// dispatch(addFileToUnitPlan(editForm));
			setShowUploadFile(false);
		}

		setEmptyfile(false);
	};

	const inputConfirmAlert = (e, itemId) => {
		dispatch(addNewItem(sectionId, e, "Sub_Item", itemId));
		hideAlert();
	};

	const addItem = (itemId) => {
		setOpenedCollapses("collapse" + itemId);
		setAlert(
			<ReactBSAlert
				input
				inputType="textarea"
				showCancel
				validationRegex={/.*/gim}
				title={tr("enter_item_name")}
				onConfirm={(e) => inputConfirmAlert(e, itemId)}
				onCancel={() => hideAlert()}
				confirmBtnBsStyle="info"
				cancelBtnBsStyle="danger"
				btnSize=""
			/>
		);
	};
	return (
		<div className="pl-3 text-left">
			{alert}
			<Row>
				<span
					className="text-dark  pl-3 cursor-pointer "
					aria-expanded={openedCollapses === "collapse" + (item.id ? item.id : item.idTemp)}
					data-parent="#accordion"
					data-toggle="collapse"
					onClick={() => collapsesToggle(item.id ? item.id : item.idTemp)}
				>
					<FontAwesomeIcon
						className="cursor-pointer fa fa-ellipsis-v"
						// style={{
						//   lineHeight: 50,
						//   lineWidth: 50
						//   // fontSize: 20,
						// }}
						onClick={() => collapsesToggle(item.id ? item.id : item.idTemp)}
						icon={openedCollapses === "collapse" + (item.id ? item.id : item.idTemp) ? faChevronUp : faChevronRight}
					/>
				</span>

				<div
					style={{
						display: "flex",
						justifyContent: "space-between",
						width: "90%",
					}}
				>
					<Input
						style={{ flexGrow: 9 }}
						disabled={readOnly ? true : false}
						type="textarea"
						className={styles.input_text}
						id="text"
						rows="6"
						value={item.text}
						onChange={changeValue}
						placeholder={tr("item_name")}
					/>
					{readOnly ? (
						<div className={"float-right  text-dark mr-2  " + styles.size_readOnly}></div>
					) : (
						<ButtonDropdown
							disabled={readOnly ? true : false}
							isOpen={dropdownOpen}
							toggle={toggle}
							style={{ flexGrow: 1 }}
							className={"float-right cursor-pointer text-dark mr-2 " + styles.btn_mt}
						>
							<DropdownToggle className={" btn btn-link"} id="dropdown-basic">
								<i className="fa fa-ellipsis-h fa-lg"></i>
							</DropdownToggle>

							<DropdownMenu aria-haspopup={true} caret color="default" data-toggle="dropdown" className={styles.dropdown_edit}>
								<DropdownItem onClick={() => addItem(item.id ? item.id : item.idTemp)}>
									<span className="float-center">
										&nbsp;&nbsp; <span>{tr("add_new_item")}</span>
									</span>
								</DropdownItem>
								<hr className={styles.hr_mt} />
								<DropdownItem
									onClick={() => {
										PickLessonPlanItem(item.id ? item.id : item.idTemp);
									}}
								>
									<span className="float-center">
										&nbsp; &nbsp; <span>{tr("pick_an_lessonplan")}</span>
									</span>
								</DropdownItem>
								<hr className={styles.hr_mt} />
								<DropdownItem
									onClick={() => {
										pickRubric(item.id ? item.id : item.idTemp);
									}}
								>
									<span className="float-center">
										&nbsp; &nbsp; <span>{tr("pick_an_rubric")}</span>
									</span>
								</DropdownItem>
								<hr className={styles.hr_mt} />
								<DropdownItem
									onClick={() => {
										addFile(item.id ? item.id : item.idTemp);
									}}
								>
									<span className="float-center">
										&nbsp; &nbsp; <span>{tr("add_file")}</span>
									</span>
								</DropdownItem>
								<hr className={styles.hr_mt} />
								<DropdownItem
									onClick={() => {
										deleteAnItem(item.id ? item.id : item.idTemp, item.parent_id);
									}}
								>
									<span className="float-center">
										&nbsp; &nbsp; <span>{tr("delete_item")}</span>
									</span>
								</DropdownItem>
							</DropdownMenu>
						</ButtonDropdown>
					)}
				</div>
				<Col>
					<AppModal
						show={show ? show : showRubric}
						parentHandleClose={handleClose}
						header={show ? tr("lesson_plan_picker") : "Rubrics"}
						headerSort={
							show ? (
								<CallPoolPicker
									table_name={tableName}
									title={tr`lesson_plan_picker`}
									parentHandleClose={handleClose}
									unitPlan={true}
									pickItemToUnitPlan={handelChooseItemLessonPlan}
								/>
							) : (
								<CallRubricsPicker
									table_name={tableName}
									courseId={courseId}
									title="Pick Rubric To Unit Plan"
									parentHandleClose={handleClose}
									parentHandlePickRubric={handlePickRubric}
									messageConfirm={messageConfirm}
								/>
							)
						}
						classStyle={styles.lesson_plan_modal}
					></AppModal>
					<AppModal
						show={showUploadFile}
						parentHandleClose={handleClose}
						header={tr`Upload file`}
						headerSort={
							<div style={{ display: "flex", justifyContent: "center" }}>
								<RFileUploader
									parentCallback={handleCallback}
									singleFile={true}
									placeholder={"Replace the old selected file "}
									binary={true}
								/>
							</div>
						}
						classStyle={styles.lesson_plan_modal}
					></AppModal>
				</Col>
			</Row>
			<Collapse isOpen={openedCollapses === "collapse" + (item.id ? item.id : item.idTemp)}>
				{item.lesson_plan_id || item.rubric_evaluable || item.attachments ? (
					<TreeItem item={item} sectionId={sectionId} readOnly={readOnly} />
				) : (
					<></>
				)}
				{item.items.map((item, index) => {
					return (
						<>
							<hr className="mt-2 mb-2" />
							<Item item={item} key={index} sectionId={sectionId} readOnly={readOnly} courseId={courseId} />
							{/* <hr className="mt-2 mb-2"/> */}
						</>
					);
				})}
			</Collapse>
		</div>
	);
};
export default Item;
