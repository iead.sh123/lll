import React from "react";
import Item from "./Item";

const Items = ({ items, sectionId, readOnly, courseId }) => {
	return items?.map((item, index) => {
		return (
			<React.Fragment key={item.idTemp}>
				<Item item={item} key={index} sectionId={sectionId} readOnly={readOnly} courseId={courseId} />
				<hr className="mt-4 mb-4" />
			</React.Fragment>
		);
	});
};
export default Items;
