import styles from "./TreeItem.Module.scss";
import ReactBSAlert from "react-bootstrap-sweetalert";
import { useDispatch, useSelector } from "react-redux";
import { useState } from "react";
import { deleteRubricOrLessonPlan } from "store/actions/global/unitPlan.actions";
import { Link, useParams } from "react-router-dom";
import tr from "components/Global/RComs/RTranslator";

const TreeItem = ({ item, sectionId, readOnly }) => {
	const [alert, setAlert] = useState(null);
	const user = useSelector((state) => state?.auth?.user);
	const { courseId } = useParams();
	const dispatch = useDispatch();
	const hideAlert = () => {
		setAlert(null);
	};
	const successDelete = (type, index) => {
		const reultOj = {
			itemId: item.id ? item.id : item.idTemp,
			sectionId: sectionId,
			type: type,
			index: index,
		};
		// dispatch(deleteRubricOrLessonPlan(reultOj));
		hideAlert();
	};
	const deleteAnRubricOrLeassonPlan = (type, index) => {
		setAlert(
			<ReactBSAlert
				warning
				title={tr`are_you_sure_to_delete_it`}
				onConfirm={() => successDelete(type, index)}
				onCancel={() => hideAlert()}
				confirmBtnBsStyle="info"
				cancelBtnBsStyle="danger"
				confirmBtnText={tr`yes_delete_it`}
				cancelBtnText={tr`cancel`}
				showCancel
				btnSize=""
			></ReactBSAlert>
		);
	};
	return (
		<div className={styles.tree}>
			{alert}
			<ul className="pl-2 mt-4 mb-4">
				<li>
					<div>
						<ul>
							{item.lesson_plan_id ? (
								<li>
									<small>{tr("lesson_plan")}</small>
									<span>
										<p>
											<a
												href={
													user?.type == "teacher"
														? `${process.env.REACT_APP_BASE_URL}/teacher/lessonplan-viewer/${item.lesson_plan.id}`
														: `${process.env.REACT_APP_BASE_URL}/senior-teacher/lessonplan-viewer/${item.lesson_plan.id}`
												}
												target={"_blank"}
											>
												{" "}
												{item.lesson_plan.name}
											</a>
										</p>
									</span>
									<i
										hidden={readOnly ? true : false}
										onClick={() => {
											deleteAnRubricOrLeassonPlan("lessonPlan");
										}}
									>
										{" "}
										<i className={"fa fa-times-circle "}></i>
									</i>
								</li>
							) : (
								<></>
							)}
							{item?.rubric_evaluable ? (
								<li>
									<small> {tr("rubric")} </small> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<span>
										<p>
											<a
												href={
													user?.type == "teacher"
														? `${process.env.REACT_APP_BASE_URL}/teacher/courses/${courseId}/rubric-editor/${item?.rubric_evaluable?.rubric?.id}/:view?`
														: `${process.env.REACT_APP_BASE_URL}/senior-teacher/courses/${courseId}/rubric-editor/${item?.rubric_evaluable?.rubric?.id}/:view?`
												}
												target={"_blank"}
											>
												{item.rubric_evaluable?.rubric?.title}
											</a>
										</p>
									</span>
									<i
										hidden={readOnly ? true : false}
										onClick={() => {
											deleteAnRubricOrLeassonPlan("rubric");
										}}
									>
										<i className={"fa fa-times-circle "}></i>
									</i>
								</li>
							) : (
								<></>
							)}
							{item?.attachments?.length != 0 ? (
								<>
									{item?.attachments?.map((attachment, index) => {
										return (
											<li key={index}>
												<small>
													{tr("attachments")} {index + 1}
												</small>
												<span>
													<p>
														{attachment.mime_type ? (
															<a href={process.env.REACT_APP_RESOURCE_URL + attachment.url} alt="Download" target="_blank" rel="noreferrer">
																{" "}
																{attachment.file_name}
															</a>
														) : (
															<>{attachment.file_name}</>
														)}
													</p>
												</span>
												<i
													hidden={readOnly ? true : false}
													onClick={() => {
														deleteAnRubricOrLeassonPlan("fileUpload", index);
													}}
												>
													<i className={"fa fa-times-circle "}></i>
												</i>
											</li>
										);
									})}
								</>
							) : (
								<></>
							)}
						</ul>
					</div>
				</li>
			</ul>
		</div>
	);
};
export default TreeItem;
