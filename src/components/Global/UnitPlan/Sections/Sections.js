import React, { useState } from "react";
import { Button } from "reactstrap";
import Section from "./Section";
import ReactBSAlert from "react-bootstrap-sweetalert";
import { useDispatch } from "react-redux";
import { addNewSection } from "store/actions/global/unitPlan.actions";
import styles from "./Section.Module.scss";
import tr from "components/Global/RComs/RTranslator";
import { DragDropContext, Droppable } from "react-beautiful-dnd";

const Sections = ({ sections, unitPlanId, readOnly, courseId }) => {
	const [alert, setAlert] = useState(null);
	const dispatch = useDispatch();

	const hideAlert = () => {
		setAlert(null);
	};

	const inputConfirmAlert = (e) => {
		dispatch(addNewSection(e, unitPlanId));
		hideAlert();
	};

	const addSection = (sectionId) => {
		setAlert(
			<ReactBSAlert
				input
				confirmBtnText={tr("ok")}
				cancelBtnText={tr("cancel")}
				inputType="textarea"
				showCancel
				validationRegex={/.*/gim}
				title={tr("Enter Section Name")}
				onConfirm={(e) => inputConfirmAlert(e, sectionId)}
				onCancel={() => hideAlert()}
				confirmBtnBsStyle="info"
				cancelBtnBsStyle="danger"
				btnSize=""
			/>
		);
	};

	// const onDragEnd = (val) =>
	//   dispatch(changeSectionOrder(val.draggableId, val.destination.index + 1));

	return (
		<DragDropContext onDragEnd={onDragEnd}>
			<Droppable droppableId="sections">
				{(provided) => (
					<div ref={provided.innerRef}>
						{alert}
						{[...(sections ?? [])]
							?.sort((a, b) => (a.order > b.order ? 1 : -1))
							.map(
								(section, index) => (
									<Section key={section.idTemp} section={section} readOnly={readOnly} index={index} courseId={courseId} />
								) //{
								// return <Draggable draggableId={section.id} index={index} >{(provided,snapshot)=><div ref={provided.innerRef}>mdmamdmamdm</div>}</Draggable>
								//}
							)}
						<div className=" text-center ">
							<Button
								hidden={readOnly ? true : false}
								onClick={addSection}
								className={"btn-round mt-2 " + styles.btn_bgc}
								color="info"
								outline
								style={{ textTransform: "capitalize" }}
							>
								<strong className="btn-label ">
									<i className="nc-icon nc-simple-add font-weight-bold" />
								</strong>
								{tr("add_new_section")}
							</Button>
							<hr className={styles.space_line + " "} />
						</div>
					</div>
				)}
			</Droppable>
		</DragDropContext>
	);
};
export default Sections;
