import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { collaborationsCenterApi } from "api/seniorTeacher/collaborationsCenter";
import { get } from "config/api";
import { emptyReducer, chooseItem } from "store/actions/seniorTeacher/CollaborationCenter/collaborationCenter.actions";
import RAdvancedLister from "../RComs/RAdvancedLister";
import Loader from "utils/Loader";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

const CourseResourcesNomination2 = () => {
	const dispatch = useDispatch();
	const [refreshData, setRefreshData] = useState(false);

	const { collaborationsApproved } = useSelector((state) => state.collaborationCenter);

	const [processsedRecords, setProcessedRecords] = useState([]);
	const [loader, setLoader] = useState(false);

	const exploreLessonPlanItem = (item) => {
		if (item.LinkURL) {
			window.open(`${item.LinkURL}`);
		} else {
			window.open(`${process.env.REACT_APP_RESOURCE_URL}${item.attachments[0].url}`);
		}
	};

	const chooseAnItem = (item) => {
		dispatch(chooseItem(item));
	};

	const handleNominateSave = () => {
		if (collaborationsApproved.length == 0) {
			toast.warning(tr`No Changes To be Saved`);
		} else {
			setLoader(true);
			let newArr = [];
			for (let i = 0; i < collaborationsApproved.length; i++) {
				newArr.push(collaborationsApproved[i].id);
			}
			let request = {
				payload: {
					ids: newArr,
				},
			};
			collaborationsCenterApi
				.saveNominate(request)
				.then((response) => {
					if (response.data.status == 1) {
						dispatch(emptyReducer());
						setLoader(false);
						setRefreshData(true);
					} else {
						toast.error(response.data.msg);
						setLoader(false);
					}
				})
				.catch((error) => {
					toast.error(error?.message);
					setLoader(false);
				});
		}
	};

	const getDataFromBackend = async (specific_url) => {
		const url = "api/course-resources/get-records-for-nomination";
		let response1 = await get(specific_url ? specific_url : url);

		if (response1) {
			return response1;
		}
	};

	const setTableData = (response) => {
		if (!response) {
			return [];
		}

		const data = response?.data?.data?.data.map((rc) => ({
			id: rc.id,
			allTitle: rc.title,
			title: rc.title,
			initialCheckValue: true,
			Titles: [rc?.title],
			tableName: "collaborationNomination",
			images: [
				{
					src: `${process.env.REACT_APP_RESOURCE_URL}${rc.image}`,
				},
			],
			icons: [
				{
					src: `${process.env.REACT_APP_RESOURCE_URL}${rc.icon}`,
				},
			],
			details: [
				{
					icon: "nc-icon nc-check-2",
					key: "Title",
					value: rc.title,
				},
				{
					icon: "nc-icon nc-check-2",
					key: "description",
					value: rc.description,
				},
				{
					icon: "nc-icon nc-check-2",

					key: "LinkURL",
					value: rc.LinkURL,
				},
			],
			actions: [
				{
					id: "Explore" + rc.id,
					name: "Explore",
					icon: "fa fa-eye",
					showAction: true,

					onClick: () => {
						exploreLessonPlanItem(rc);
					},
				},
				{
					id: collaborationsApproved.find((el) => el.id == rc.id) ? "delete" + rc.id : "Nominate" + rc.id,
					name: "Nominate",
					icon: collaborationsApproved.find((el) => el.id == rc.id) ? "fa fa-times" : "fa fa-check-circle",
					color: collaborationsApproved.find((el) => el.id == rc.id) ? "success" : "info",

					onClick: () => {
						chooseAnItem(rc);
					},
				},
			],
		}));
		setProcessedRecords(response);
		setRefreshData(false);

		return data;
	};

	const propsLiterals = React.useMemo(
		() => ({
			listerProps: {
				records: setTableData(processsedRecords) || [],
				getDataFromBackend: getDataFromBackend,
				setData: setTableData,
				getDataObject: (response) => response.data.data,
				handelSaveFormData: handleNominateSave,
				actionFormData: true,
				table_name: refreshData,
			},
		}),
		[processsedRecords, collaborationsApproved]
	);

	return <div className="">{loader ? <Loader /> : <RAdvancedLister {...propsLiterals.listerProps} />}</div>;
};

export default CourseResourcesNomination2;
