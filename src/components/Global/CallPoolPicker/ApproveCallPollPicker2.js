import React, { useState } from "react";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import { useDispatch, useSelector } from "react-redux";
import { collaborationsCenterApi } from "api/seniorTeacher/collaborationsCenter";
import { HANDLE_NOTES } from "store/actions/global/globalTypes";
import { useHistory } from "react-router";
import { post } from "config/api";
import {
	changeCollaborationLevel,
	emptyReducer,
	changeApproved,
} from "store/actions/seniorTeacher/CollaborationCenter/collaborationCenter.actions";
import RAdvancedLister from "../RComs/RAdvancedLister";
import Loader from "utils/Loader";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

const ApproveCallPoolPicker2 = ({ table_name }) => {
	const dispatch = useDispatch();
	const history = useHistory();

	const [processsedRecords, setProcessedRecords] = useState([]);
	const [loader, setLoader] = useState(false);
	const [mainCourses, setMainCourses] = useState([]);
	const [schoolClasses, setSchoolClasses] = useState([]);
	const [tags, setTags] = useState([]);
	const [selectedMainCourses, setSelectedMainCourses] = useState([]);
	const [selectedSchoolClasses, setSelectedSchoolClasses] = useState([]);
	const [selectedTags, setSelectedTags] = useState([]);

	const { collaborationsApproved } = useSelector((state) => state.collaborationCenter);

	const { user } = useSelector((state) => state.auth);

	//Action explore
	const explore = (item) => {
		switch (table_name) {
			case "lesson_plan_items":
				history.push(
					`${process.env.REACT_APP_BASE_URL}/${
						user?.type === "teacher"
							? "teacher"
							: user?.type === "school_advisor" || user?.type === "seniorteacher" || user?.type === "principal"
							? "senior-teacher"
							: ""
					}/lessonplan-item-viewer/${item.id}`
				);
				break;
			case "lesson_plans":
				history.push(
					`${process.env.REACT_APP_BASE_URL}/${
						user?.type === "teacher"
							? "teacher"
							: user?.type === "school_advisor" || user?.type === "seniorteacher" || user?.type === "principal"
							? "senior-teacher"
							: ""
					}/lessonplan-viewer/${item.id}`
				);
				break;
			case "unit_plans":
				history.push(
					`${process.env.REACT_APP_BASE_URL}/${
						user?.type === "teacher"
							? "teacher"
							: user?.type === "school_advisor" || user?.type === "seniorteacher" || user?.type === "principal"
							? "senior-teacher"
							: ""
					}/unitplan-viewer/${item.id}`
				);
				break;

			case "topics":
				history.push(
					`${process.env.REACT_APP_BASE_URL}/${
						user?.type === "teacher"
							? "teacher"
							: user?.type === "school_advisor" || user?.type === "seniorteacher" || user?.type === "principal"
							? "senior-teacher"
							: ""
					}/courses/undefined/rabs/undefined/topics/view/${item.id}`
				);
				break;

			case "rubrics":
				history.push(
					`${process.env.REACT_APP_BASE_URL}/${
						user?.type === "teacher"
							? "teacher"
							: user?.type === "school_advisor" || user?.type === "seniorteacher" || user?.type === "principal"
							? "senior-teacher"
							: ""
					}/courses/undefined/rubric-editor/${item.id}/view`
				);
				break;

			case "course_resources":
				if (item.LinkURL) {
					window.open(`${item.LinkURL}`);
				} else if (item.attachment) {
					window.open(`${process.env.REACT_APP_RESOURCE_URL}${item.attachment.url}`);
				} else {
					return;
				}
				break;
			default:
				break;
		}
	};

	// Action historyNote
	const historyNote = (item) => {
		dispatch({
			type: HANDLE_NOTES,
			table_name: table_name,
			row_id: item.id,
			context: "collaborations",
			context_id: 0,
			nameToShow: item.subject ? item.subject : item.title ? item.title : item.name,
		});
	};

	const chooseApproved = (item, element) => {
		const resultObj = {
			item: item,
			element: element,
		};
		dispatch(changeApproved(resultObj));
	};

	const handleLevelChanged = (e, recId, element) => {
		const resultObj = {
			levelId: e,
			item: recId,
			element: element,
		};
		dispatch(changeCollaborationLevel(resultObj));
	};

	const handleSaveApprovedRecords = () => {
		if (collaborationsApproved.length == 0) {
			toast.warning(tr`please edit any thing, then click save`);
		} else {
			setLoader(true);
			let request = {
				payload: {
					tableName: table_name,
					items: collaborationsApproved,
				},
			};
			collaborationsCenterApi
				.saveApproveRecords(request)
				.then((response) => {
					setLoader(false);
					if (response.data.status === 1) {
						dispatch(emptyReducer());
					} else {
						toast.error(response.data.msg);
					}
				})
				.catch((error) => {
					toast.error(error?.message);
				});
		}
	};

	const getDataFromBackend = async (specific_url) => {
		let Request = {
			payload: {
				tableName: table_name,
				mainCourses: selectedMainCourses,
				schoolClasses: selectedSchoolClasses,
				tags: selectedTags,
			},
		};
		const url = `api/collaboration/get-records-to-approve`;
		let response1 = await post(specific_url ? specific_url : url, Request);

		if (response1) {
			return response1;
		}
	};

	const filterItems = [
		{
			Items: mainCourses,
			setSelectedItems: setSelectedMainCourses,
			placeHolderText: tr`Main Curricula`,
			isMulti: false,
			selected: selectedMainCourses,
		},

		{
			Items: schoolClasses,
			setSelectedItems: setSelectedSchoolClasses,
			placeHolderText: tr`School Classes`,
			isMulti: false,
			selected: selectedSchoolClasses,
		},

		{
			Items: tags,
			setSelectedItems: setSelectedTags,
			placeHolderText: tr`Tags`,
			isMulti: false,
			selected: selectedTags,
		},
	];

	const setTableData = (response) => {
		if (!response) {
			return [];
		}
		if (response) {
			setMainCourses(response?.data?.data?.courses);
			setSchoolClasses(response?.data?.data?.school_classes);
			setTags(response?.data?.data?.tags);
		}

		const data = response?.data?.data?.records?.data.map((rc) => ({
			id: rc.id,
			title: rc.title,
			Titles: [rc.title],

			initialCheckValue: true,
			tableName: "collaborationApprove",
			images: [
				{
					src: `${process.env.REACT_APP_RESOURCE_URL}${rc.image}`,
				},
			],
			icons: [
				{
					src: `${process.env.REACT_APP_RESOURCE_URL}${rc.icon}`,
				},
			],

			details: [
				{
					icon: "nc-icon nc-check-2",
					key: tr`name`,
					value: rc.subject ? rc.subject : rc.title ? rc.title : rc.name,
				},
				{
					icon: "nc-icon nc-check-2",
					key: tr`creator`,
					value: rc.details ? rc.details[1].value : rc.creator.full_name,
				},
				{
					mode: "form",
					type: "checkbox",
					name: "Approved",
					key: tr`Approved`,
					checked: collaborationsApproved.find((el) => (el.id == rc.id && el.approved == true ? el.approved : false)),
					disabled: collaborationsApproved.find((el) => (el.id == rc.id && el.not_approved == true ? el.not_approved : false)),
					onClick: () => {
						chooseApproved(rc, "approved");
					},
				},

				{
					mode: "form",
					type: "checkbox",
					name: "Do-not-Approve",
					key: tr`Don't Approve`,
					disabled: collaborationsApproved.find((el) => (el.id == rc.id && el.approved == true ? el.approved : false)),
					onClick: () => {
						chooseApproved(rc, "not_approved");
					},
				},

				{
					mode: "form",
					type: "select",
					name: "collaboration-level",
					key: tr`Collaboration Level`,
					option: response.data.data.collaboration_levels,
					isDisabled: collaborationsApproved.find((el) => (el.id == rc.id && el.not_approved == true ? el.not_approved : false)),
					// defaultValue: collaborationsApproved.find(
					//   (el) => el.id !== rc.id && {}
					// ),
					minWidth: "200px",
					onChange: (e) => {
						handleLevelChanged(e.value, rc, "approved");
					},
				},
			],
			actions: [
				{
					type: "coll",
					name: "view",
					icon: "fa fa-eye",
					showAction: true,

					onClick: () => {
						explore(rc);
					},
				},
				{
					name: "Note",
					icon: "nc-icon nc-caps-small",
					onClick: () => {
						historyNote(rc);
					},
				},
			],
		}));
		setProcessedRecords(response);

		return data;
	};

	const propsLiterals = React.useMemo(
		() => ({
			listerProps: {
				records: setTableData(processsedRecords) || [],
				getDataFromBackend: getDataFromBackend,
				setData: setTableData,
				getDataObject: (response) => response.data.data.records,
				handelSaveFormData: handleSaveApprovedRecords,
				actionFormData: true,
				table_name: table_name,
				filterItems: filterItems,
			},
		}),
		[processsedRecords, collaborationsApproved]
	);

	return <div className="">{loader ? <Loader /> : <RAdvancedLister {...propsLiterals.listerProps} />}</div>;
};

export default ApproveCallPoolPicker2;
