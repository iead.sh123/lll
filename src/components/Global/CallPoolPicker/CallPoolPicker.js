import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import tr from "components/Global/RComs/RTranslator";
import { pickItem } from "store/actions/teacher/lessonPlan.actions";
import { lessonPlanApi } from "api/teacher/lessonPlan";
import ReactBSAlert from "react-bootstrap-sweetalert";
import PoolPicker from "components/Global/PoolPicker/PoolPicker";
import { useHistory } from "react-router";
import { toast } from "react-toastify";

const CallPoolPicker = ({ unitPlan, table_name, title, parentHandleClose, pickItemToUnitPlan, parentCallPickResources }) => {
	const [records, setRecords] = useState(null);
	const [isMultiSelect, setIsMultiSelect] = useState(false);
	const [mainCourses, setMainCourses] = useState(null);
	const [schoolClasses, setSchoolClasses] = useState(null);
	const [tags, setTags] = useState(null);
	const [selectedMainCourses, setSelectedMainCourses] = useState([]);
	const [selectedSchoolClasses, setSelectedSchoolClasses] = useState([]);
	const [selectedTags, setSelectedTags] = useState([]);
	const [firstPageUrl, setFirstPageUrl] = useState(null);
	const [lastPageUrl, setLastPageUrl] = useState(null);
	const [nextPageUrl, setNextPageUrl] = useState(null);
	const [prevPageUrl, setPrevPageUrl] = useState(null);
	const [currentPage, setCurrentPage] = useState(null);
	const [lastPage, setLastPage] = useState(null);
	const [total, setTotal] = useState(null);
	const [ckeckedItem, setCkeckedItem] = useState([]);
	const [alert, setAlert] = useState(null);

	const user = useSelector((state) => state?.auth?.user);

	const dispatch = useDispatch();
	const pickedItems = useSelector((state) => state.lessonPlan.chooseItems);

	const getData = (url) => {
		let Request = {
			payload: {
				tableName: table_name,
				schoolClasses: selectedSchoolClasses,
				mainCourses: selectedMainCourses,
				tags: [],
			},
		};
		if (!url) {
			url = `api/collaboration/pick-items`;
		}
		lessonPlanApi
			.getPickItems(url, Request)
			.then((response) => {
				handleResponse(response);
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};

	useEffect(() => {
		getData();
	}, []);

	useEffect(() => {
		setCkeckedItem(pickedItems);
	}, [pickedItems]);

	const handleResponse = (response) => {
		if (response.data.status === 1) {
			setCurrentPage(response.data.data.records.current_page);
			setLastPage(response.data.data.records.last_page);
			setFirstPageUrl(response.data.data.records.first_page_url);
			setLastPageUrl(response.data.data.records.last_page_url);
			setNextPageUrl(response.data.data.records.next_page_url);
			setPrevPageUrl(response.data.data.records.prev_page_url);
			setTotal(response.data.data.records.total);
			setIsMultiSelect(response.data.data.multiSelect);
			setSchoolClasses(response.data.data.school_classes);
			setMainCourses(response.data.data.courses);
			setTags(response.data.data.tags);
			createArrayLister(response.data.data.records.data);
		} else {
			toast.error(response.data.msg);
		}
	};

	const chooseItem = (item) => {
		dispatch(pickItem(item));
	};
	const hideAlert = () => {
		setAlert(null);
	};
	const successPickLessonPlan = (item) => {
		// dispatch(importLessonPlan(item));
		hideAlert();
		parentHandleClose();
	};
	const chooseLessonPlan = (item) => {
		if (unitPlan) {
			pickItemToUnitPlan(item);
			parentHandleClose();
		} else {
			setAlert(
				<ReactBSAlert
					warning
					title="Current Lesson Plan would be replaced with this new one  
          Proceed ?"
					onConfirm={() => successPickLessonPlan(item)}
					onCancel={() => hideAlert()}
					confirmBtnBsStyle="info"
					cancelBtnBsStyle="danger"
					confirmBtnText="Ok, pick lesson plan"
					cancelBtnText="Cancel"
					showCancel
					btnSize=""
				></ReactBSAlert>
			);
		}
	};
	const history = useHistory();

	const exploreLessonPlan = (item) => {
		if (user.type === "school_advisor" || user.type === "seniorteacher" || user.type === "principal") {
			window.open(`${process.env.REACT_APP_BASE_URL}/senior-teacher/lessonplan-viewer/${item.id}`);
		} else {
			window.open(`${process.env.REACT_APP_BASE_URL}/teacher/lessonplan-viewer/${item.id}`);
		}
	};

	const exploreLessonPlanItem = (item) => {
		if (user.type === "school_advisor" || user.type === "seniorteacher" || user.type === "principal") {
			window.open(`${process.env.REACT_APP_BASE_URL}/senior-teacher/lessonplan-item-viewer/${item.id}`);
		} else {
			history.push(`${process.env.REACT_APP_BASE_URL}/teacher/lessonplan-item-viewer/${item.id}`);
		}
	};

	//resources
	const pickResources = (item) => {
		parentCallPickResources(item);
		hideAlert();
		parentHandleClose();
	};
	const exploreResources = (item) => {
		window.open(item.LinkURL);
	};

	const createArrayLister = (allRecord) => {
		let processsedRecords = [];
		if (table_name === "lesson_plan_items") {
			allRecord.map((item) => {
				let record = {};
				record.id = item.id;
				record.title = item.subject.substring(0, 25);
				record.allTitle = item.subject;
				record.initialCheckValue = true;
				record.details = [
					{ key: tr("Name"), value: item.subject.substring(0, 25) },
					{ key: tr("creator"), value: item.creator.full_name },
					{
						key: tr("Logo"),
						value: item.course.semester.school_class.education_stage.school.name,
					},
				];
				record.actions = [
					{
						name: tr("Choose"),
						icon: "fa fa-check-circle",
						onClick: () => {
							chooseItem(item);
						},
					},

					{
						name: tr("Explore"),
						icon: "fa fa-eye",
						onClick: () => {
							exploreLessonPlanItem(item);
						},
					},
				];

				processsedRecords.push(record);
			});
			setRecords(processsedRecords);
		} else if (table_name === "lesson_plans") {
			allRecord.map((item) => {
				let record = {};
				record.id = item.id;
				record.title = item.name;
				record.allTitle = item.name;
				record.initialCheckValue = true;
				record.details = [
					{ key: tr("Name"), value: item.name },
					{ key: tr("creator"), value: item.creator.full_name },
					{ key: tr("Logo"), value: item.school_name },
				];
				record.actions = [
					{
						name: tr("Choose"),
						icon: "fa fa-check-circle",
						onClick: () => {
							chooseLessonPlan(item);
						},
					},

					{
						name: tr("Explore"),
						icon: "fa fa-eye",
						onClick: () => {
							exploreLessonPlan(item);
						},
					},
				];

				processsedRecords.push(record);
			});
			setRecords(processsedRecords);
		} else {
			allRecord.map((item) => {
				let record = {};
				record.id = item.id;
				record.title = item.name ? item.name : item.title;
				record.allTitle = item.name ? item.name : item.title;
				record.initialCheckValue = true;
				record.details = [
					{
						key: item.name ? tr("Name") : tr("Title"),
						value: item.name ? item.name : item.title,
					},
					{ key: tr("creator"), value: item.creator.full_name },
					{ key: tr("Link URL"), value: item.LinkURL },
					{
						key: tr("Logo"),
						value: item.school_name ? item.school_name : item.course.semester.school_class.education_stage.school.name,
					},
				];
				record.actions = [
					{
						name: tr("Choose"),
						icon: "fa fa-check-circle",
						onClick: () => {
							pickResources(item);
						},
					},

					{
						name: tr("Explore"),
						icon: "fa fa-eye",
						onClick: () => {
							exploreResources(item);
						},
					},
				];

				processsedRecords.push(record);
			});
			setRecords(processsedRecords);
		}
	};

	const ExternalCheckHandler = () => {};

	const handleApplyFilter = () => {
		let Request = {
			payload: {
				tableName: table_name,
				schoolClasses: selectedSchoolClasses,
				mainCourses: selectedMainCourses,
				tags: selectedTags,
			},
		};
		const url = `api/collaboration/pick-items`;
		setRecords(null);
		lessonPlanApi
			.getPickItems(url, Request)
			.then((response) => {
				handleResponse(response);
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};

	const handlePickRecordsClicked = () => {
		switch (table_name) {
			case "lesson_plan_items":
				// dispatch(addLessonPlanItems(ckeckedItem));
				parentHandleClose();
				break;
			case "lesson_plans":
				// dispatch(importLessonPlan(ckeckedItem));
				break;
			case "unit_plans":
				//pickItemToUnitPlan(ckeckedItem);
				break;
			case "rubric_levels":
				// setPickedIds(selectedItems);
				break;
			case "rubrics":
				// setPickedIds(selectedItems);
				break;
			case "question_sets":
				// setPickedIds(selectedItems);
				break;
			case "course_resources":
				// setPickedIds(selectedItems);
				break;
			default:
				break;
		}
	};

	return (
		<div className="">
			{alert}
			<PoolPicker
				table_name={unitPlan ? "unit_plans" : table_name}
				title={title}
				mainCourses={mainCourses}
				schoolClasses={schoolClasses}
				use_tags={true}
				tags={tags}
				handleApplyFilter={handleApplyFilter}
				setSelectedMainCourses={setSelectedMainCourses}
				setSelectedSchoolClasses={setSelectedSchoolClasses}
				selectedSchoolClasses={selectedSchoolClasses}
				selectedMainCourses={selectedMainCourses}
				setSelectedTags={setSelectedTags}
				Records={records}
				ListerClass={""}
				ItemClass={""}
				check={1}
				ckeckedItem={ckeckedItem}
				onCheckChange={ExternalCheckHandler}
				firstPageUrl={firstPageUrl}
				lastPageUrl={lastPageUrl}
				currentPage={currentPage}
				lastPage={lastPage}
				prevPageUrl={prevPageUrl}
				nextPageUrl={nextPageUrl}
				total={total}
				getData={getData}
				parentHandleClose={parentHandleClose}
				pickItemToUnitPlan={pickItemToUnitPlan}
				itemToShow={2}
				handelSave={handlePickRecordsClicked}
			/>
		</div>
	);
};

export default CallPoolPicker;
