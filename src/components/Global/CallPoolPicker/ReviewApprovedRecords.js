import { collaborationsCenterApi } from "api/seniorTeacher/collaborationsCenter";
import { lessonPlanApi } from "api/teacher/lessonPlan";
import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import Loader from "utils/Loader";
import PoolPicker from "../PoolPicker/PoolPicker";

import { useHistory } from "react-router";
import { changeCollaborationLevelReview, emptyReducer } from "store/actions/seniorTeacher/CollaborationCenter/collaborationCenter.actions";
import iconsFa6 from "variables/iconsFa6";
import { toast } from "react-toastify";
import REmptyData from "components/RComponents/REmptyData";

const ReviewApprovedRecords = ({ unitPlan, table_name, title, parentHandleClose, url }) => {
	const [records, setRecords] = useState([]);
	const [ckeckedItem, setCkeckedItem] = useState([]);
	const [firstPageUrl, setFirstPageUrl] = useState(null);
	const [lastPageUrl, setLastPageUrl] = useState(null);
	const [nextPageUrl, setNextPageUrl] = useState(null);
	const [prevPageUrl, setPrevPageUrl] = useState(null);
	const [currentPage, setCurrentPage] = useState(null);
	const [lastPage, setLastPage] = useState(null);
	const [total, setTotal] = useState(null);
	const [loader, setLoader] = useState(false);

	const dispatch = useDispatch();

	const pickedItems = useSelector((state) => state.collaborationCenter.collaborationsApproved);

	const getData = (url) => {
		setLoader(true);
		if (!url) {
			url = "api/collaboration/review-approved-records";
		}
		lessonPlanApi
			.reviewApprovedRecords(url)
			.then((response) => {
				setLoader(false);
				handleResponse(response);
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};

	useEffect(() => {
		getData(url);
	}, [url]);

	useEffect(() => {
		setCkeckedItem(pickedItems);
	}, [pickedItems]);

	const handleResponse = (response, save = false) => {
		if (response.data.status === 1) {
			let data = response.data.data;
			setCurrentPage(data.records.current_page);
			setLastPage(data.records.last_page);
			setFirstPageUrl(data.records.first_page_url);
			setLastPageUrl(data.records.last_page_url);
			setNextPageUrl(data.records.next_page_url);
			setPrevPageUrl(data.records.prev_page_url);
			setTotal(data.records.total);

			if (data.records.data.length != 0) {
				createArrayLister(data.records.data, data.collaboration_levels);
			}
		} else {
			toast.error(response.data.msg);
		}
	};

	const handleLevelChanged = (e, allRecord, collaborationLevels, index, key, item) => {
		allRecord[index].actions[key].value = e;
		createArrayLister(allRecord, collaborationLevels);
		const resultObj = {
			levelId: e,
			item: item,
		};
		dispatch(changeCollaborationLevelReview(resultObj));
	};
	const history = useHistory();
	const exploreItem = (item) => {
		const tableName = item?.collaborative_type?.substring(11, 40);

		switch (tableName) {
			case "Lesson_Plan_Item":
				history.push(`${process.env.REACT_APP_BASE_URL}/senior-teacher/lessonplan-item-viewer/${item.id}`);
				break;
			case "Lesson_Plan":
				history.push(`${process.env.REACT_APP_BASE_URL}/senior-teacher/lessonplan-viewer/${item.id}`);
				break;
			case "UnitPlan":
				history.push(`${process.env.REACT_APP_BASE_URL}/senior-teacher/unitplan-viewer/${item.collaborative.id}`);
				break;

			case "CourseResource":
				if (item.LinkURL) {
					window.open(`${item.LinkURL}`);
				} else if (item.attachments) {
					window.open(`${process.env.REACT_APP_RESOURCE_URL}${item.attachments[0].url}`);
				} else {
					return;
				}
				break;

			case "Topic":
				history.push(
					`${process.env.REACT_APP_BASE_URL}/senior-teacher/courses/undefined/rabs/undefined/topics/view/${item.collaborative.id}`
				);
				break;

			case "Rubric":
				history.push(`${process.env.REACT_APP_BASE_URL}/senior-teacher/courses/undefined/rubric-editor/${item.collaborative.id}/view`);
				break;

			default:
				history.push(`${process.env.REACT_APP_BASE_URL}/senior-teacher/${tableName}/${item.collaborative.id}`);
				break;
		}
	};

	const chooseItem = (allRecord, collaborationLevels, item) => {
		// dispatch(deleteItem(item));
		// createArrayLister(allRecord, collaborationLevels, item);
	};

	const createArrayLister = (allRecord, collaborationLevels, selectedItem) => {
		let processsedRecords = [];

		allRecord?.map((item, index) => {
			let record = {};
			record.id = item.id;
			(record.allTitle = item.title
				? item.title
				: item.collaborative.name
				? item.collaborative.name
				: item.collaborative.title
				? item.collaborative.title
				: item.collaborative.subject
				? item.collaborative.subject
				: item.collaborative.id),
				(record.title = item.title
					? item.title.substring(0, 15)
					: item.collaborative.name
					? item.collaborative.name.substring(0, 15)
					: item.collaborative.title
					? item.collaborative.title.substring(0, 15)
					: item.collaborative.subject
					? item.collaborative.subject.substring(0, 15)
					: item.collaborative.id),
				(record.initialCheckValue = true);
			record.Titles = [
				item.title
					? item.title.substring(0, 15)
					: item.collaborative.name
					? item.collaborative.name.substring(0, 15)
					: item.collaborative.title
					? item.collaborative.title.substring(0, 15)
					: item.collaborative.subject
					? item.collaborative.subject.substring(0, 15)
					: item.collaborative.id,
			];

			record.images = [
				{
					src: `${process.env.REACT_APP_RESOURCE_URL}${item.image}`,
					placeholder: "11",
				},
			];

			record.icons = [
				{
					src: `${process.env.REACT_APP_RESOURCE_URL}${item.icon}`,
					placeholder: "11",
				},
			];

			record.details = [
				{
					icon: "nc-icon nc-check-2",
					key: tr("Pool"),
					value: item.details ? item.details[0].value : item.collaborative_type.substring(11, 25),
				},
				{
					icon: "nc-icon nc-check-2",

					key: tr("Item"),
					value: item.details
						? item.details[1].value
						: item.collaborative.name
						? item.collaborative.name.substring(0, 15)
						: item.collaborative.title
						? item.collaborative.title.substring(0, 15)
						: item.collaborative.subject
						? item.collaborative.subject.substring(0, 15)
						: item.collaborative.id,
				},
				{
					icon: "nc-icon nc-check-2",

					key: tr("creator"),
					value: item.details ? item.details[2].value : item.collaborative.creator.first_name,
				},
			];

			record.actions = [
				{
					name: tr("Explore"),
					icon: "fa fa-eye",
					showAction: true,
					disable:
						item.collaborative_type == "App\\Models\\QuestionSet" || item.collaborative_type == "App\\Models\\CourseResource"
							? true
							: false,

					onClick: () => {
						exploreItem(item);
					},
				},

				{
					type: "select",
					name: tr("colaboration level"),
					value: item.actions ? item.actions[1].value : item?.collaboration_level_id,
					name: tr("coll-level"),
					options: collaborationLevels,
					onClick: (e, allRecord, key) => {
						handleLevelChanged(e.target.value, allRecord, collaborationLevels, index, key, item);
					},
				},
				{
					icon: iconsFa6.delete,
					name: tr("Remove"),
					onClick: () => {
						chooseItem(allRecord, collaborationLevels, item);
					},
				},
			];
			if (item.id != selectedItem?.id) {
				processsedRecords.push(record);
			}
		});

		setRecords(processsedRecords);
	};
	const handleSaveApprovedRecords = () => {
		if (pickedItems.length == 0) {
			toast.warning(tr`No Changes To be Saved`);
		} else {
			setLoader(true);
			let request = {
				payload: {
					records: pickedItems,
				},
			};
			collaborationsCenterApi
				.updateReviewedRecords(request)
				.then((response) => {
					if (response.data.status == 1) {
						setLoader(false);
						dispatch(emptyReducer());

						getData();
					} else {
						toast.error(response.data.msg);
					}
				})
				.catch((error) => {
					toast.error(error?.message);
				});
		}
	};

	return (
		<div className="">
			{loader ? (
				Loader()
			) : records.length != 0 ? (
				<PoolPicker
					Records={records}
					check={1}
					ckeckedItem={ckeckedItem}
					firstPageUrl={firstPageUrl}
					lastPageUrl={lastPageUrl}
					currentPage={currentPage}
					lastPage={lastPage}
					prevPageUrl={prevPageUrl}
					nextPageUrl={nextPageUrl}
					total={total}
					getData={getData}
					parentHandleClose={parentHandleClose}
					hiddenFilter={true}
					hiddenSave={false}
					hiddenCancel={true}
					handelSave={handleSaveApprovedRecords}
					itemToShow={2}
					showListerMode={"cardLister"}
					showCheckIcon={true}
				/>
			) : (
				<REmptyData />
			)}
		</div>
	);
};

export default ReviewApprovedRecords;
