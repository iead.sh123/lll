import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import tr from "components/Global/RComs/RTranslator";
import { lessonPlanApi } from "api/teacher/lessonPlan";
import PoolPicker from "components/Global/PoolPicker/PoolPicker";
import { useHistory } from "react-router";
import {
	changeCollaborationLevel,
	emptyReducer,
	changeApproved,
} from "store/actions/seniorTeacher/CollaborationCenter/collaborationCenter.actions";
import { collaborationsCenterApi } from "api/seniorTeacher/collaborationsCenter";
import { HANDLE_NOTES } from "store/actions/global/globalTypes";
import Loader from "../Loader";
import { toast } from "react-toastify";
import REmptyData from "components/RComponents/REmptyData";

const ApproveCallPoolPicker = ({ unitPlan, table_name, title, parentHandleClose, pickItemToUnitPlan, url }) => {
	const history = useHistory();

	const [records, setRecords] = useState(null);
	const [isMultiSelect, setIsMultiSelect] = useState(false);
	const [mainCourses, setMainCourses] = useState(null);
	const [schoolClasses, setSchoolClasses] = useState(null);
	const [tags, setTags] = useState(null);
	const [selectedMainCourses, setSelectedMainCourses] = useState([]);
	const [selectedSchoolClasses, setSelectedSchoolClasses] = useState([]);
	const [selectedTags, setSelectedTags] = useState([]);
	const [firstPageUrl, setFirstPageUrl] = useState(null);
	const [lastPageUrl, setLastPageUrl] = useState(null);
	const [nextPageUrl, setNextPageUrl] = useState(null);
	const [prevPageUrl, setPrevPageUrl] = useState(null);
	const [currentPage, setCurrentPage] = useState("");
	const [lastPage, setLastPage] = useState(null);
	const [total, setTotal] = useState(null);
	const [ckeckedItem, setCkeckedItem] = useState([]);
	const [checkedApprove, setCheckedApprove] = useState(null);
	const [loader, setLoader] = useState(false);

	const dispatch = useDispatch();
	const reducer = useSelector((state) => state.collaborationCenter.collaborationsApproved);

	const pickedItems = useSelector((state) => state.lessonPlan.chooseItems);

	const getData = (url) => {
		setLoader(true);
		let Request = {
			payload: {
				tableName: table_name,
				schoolClasses: selectedSchoolClasses,
				mainCourses: selectedMainCourses,
				tags: selectedTags,
			},
		};
		if (!url) {
			url = `api/collaboration/get-records-to-approve`;
		}
		lessonPlanApi
			.getPickItems(url, Request)
			.then((response) => {
				setLoader(false);
				handleResponse(response);
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};

	useEffect(() => {
		getData(url);
	}, [url, table_name]);

	useEffect(() => {
		setCkeckedItem(pickedItems);
	}, [pickedItems]);

	const handleResponse = (response) => {
		if (response.data.status === 1) {
			setCurrentPage(response.data.data.records.current_page);
			setLastPage(response.data.data.records.last_page);
			setFirstPageUrl(response.data.data.records.first_page_url);
			setLastPageUrl(response.data.data.records.last_page_url);
			setNextPageUrl(response.data.data.records.next_page_url);
			setPrevPageUrl(response.data.data.records.prev_page_url);
			setTotal(response.data.data.records.total);
			setIsMultiSelect(response.data.data.multiSelect);
			setSchoolClasses(response.data.data.school_classes);
			setMainCourses(response.data.data.courses);
			setTags(response.data.data.tags);
			setRecords(response.data.data.records.data);

			handelBuildingArray(response.data.data.records.data, response.data.data.collaboration_levels);
		} else {
			// t
		}
	};

	const handelBuildingArray = (records, collaboration_levels) => {
		if (reducer.length != 0) {
			for (let i = 0; i < records.length; i++) {
				for (let j = 0; j < reducer.length; j++) {
					if (records[i].id == reducer[j].id) {
						records[i].approved = reducer[j]?.approved;
						records[i].not_approved = reducer[j]?.not_approved;
						if (reducer[j]?.collaboration_level_id) {
							records[i].collaboration_level_id = reducer[j]?.collaboration_level_id;
						}
					}
				}
			}
			createArrayLister(records, collaboration_levels);
		} else {
			createArrayLister(records, collaboration_levels);
		}
	};

	const handleLevelChanged = (e, allRecord, collaborationLevels, index, key, item) => {
		allRecord[index].actions[key].value = e;
		createArrayLister(allRecord, collaborationLevels);
		const resultObj = {
			levelId: e,
			item: item,
			not_approved: allRecord[index].actions[1].checked,
			approved: allRecord[index].actions[2].checked,
		};
		dispatch(changeCollaborationLevel(resultObj));
	};

	const chooseApproved = (allRecord, collaborationLevels, checked, index, key, item) => {
		allRecord[index].actions[key].checked = !checked;

		setRecords(allRecord);
		createArrayLister(allRecord, collaborationLevels);
		const resultObj = {
			item: item,
			not_approved: allRecord[index].actions[1].checked,
			approved: allRecord[index].actions[2].checked,
		};
		dispatch(changeApproved(resultObj));
	};

	const chooseDontApproved = (allRecord, collaborationLevels, checked, index, key, item) => {
		allRecord[index].actions[key].checked = !checked;
		setRecords(allRecord);
		createArrayLister(allRecord, collaborationLevels);
		const resultObj = {
			item: item,
			not_approved: allRecord[index].actions[1].checked,
			approved: allRecord[index].actions[2].checked,
		};
		dispatch(changeApproved(resultObj));
	};

	const explore = (item) => {
		switch (table_name) {
			case "lesson_plan_items":
				history.push(`${process.env.REACT_APP_BASE_URL}/senior-teacher/lessonplan-item-viewer/${item.id}`);
				break;
			case "lesson_plans":
				history.push(`${process.env.REACT_APP_BASE_URL}/senior-teacher/lessonplan-viewer/${item.id}`);
				break;
			case "unit_plans":
				history.push(`${process.env.REACT_APP_BASE_URL}/senior-teacher/unitplan-viewer/${item.id}`);
				break;

			case "topics":
				history.push(`${process.env.REACT_APP_BASE_URL}/senior-teacher/courses/undefined/rabs/undefined/topics/view/${item.id}`);
				break;

			case "rubrics":
				history.push(`${process.env.REACT_APP_BASE_URL}/senior-teacher/courses/undefined/rubric-editor/${item.id}/view`);
				break;

			case "course_resources":
				if (item.LinkURL) {
					window.open(`${item.LinkURL}`);
				} else if (item.attachment) {
					window.open(`${process.env.REACT_APP_RESOURCE_URL}${item.attachment.url}`);
				} else {
					return;
				}
				break;
			default:
				break;
		}
	};

	const historyNote = (item) => {
		dispatch({
			type: HANDLE_NOTES,
			table_name: table_name,
			row_id: item.id,
			context: "collaborations",
			context_id: 0,
			nameToShow: item.subject ? item.subject : item.title ? item.title : item.name,
		});
	};

	const createArrayLister = (allRecord, collaborationLevels) => {
		let processsedRecords = [];
		allRecord.map((item, index) => {
			let record = {};
			record.id = item.id;
			record.allTitle = item.subject ? item.subject : item.title ? item.title : item.name;
			record.title = item.subject ? item.subject : item.title ? item.title : item.name;
			record.initialCheckValue = true;
			record.Titles = [item?.title];

			record.images = [
				{
					src: item?.images && item?.images[0] ? `${item?.images[0]?.src}` : `${process.env.REACT_APP_RESOURCE_URL}${item.image}`,
					placeholder: "11",
				},
			];

			record.reImage = `${process.env.REACT_APP_RESOURCE_URL}${item.image}`;

			record.icons = [
				{
					src: item?.icons && item?.icons[0] ? `${item?.icons[0]?.src}` : `${process.env.REACT_APP_RESOURCE_URL}${item.icon}`,
					placeholder: "11",
				},
			];

			record.details = [
				{
					icon: "nc-icon nc-check-2",
					key: tr("Name"),
					value: item.subject ? item.subject : item.title ? item.title : item.name,
				},
				{
					icon: "nc-icon nc-check-2",
					key: tr("creator"),
					value: item.details ? item.details[1].value : item.creator.full_name,
				},
				{
					icon: "nc-icon nc-check-2",
					key: tr("Logo"),
					value: "",
				},
			];

			record.actions = [
				{
					type: "coll",
					name: "view",
					icon: "fa fa-eye",
					showAction: true,

					onClick: () => {
						explore(item);
					},
				},

				{
					checked: item.actions ? item.actions[1].checked : item?.not_approved,
					type: "checkbox",
					style: true,
					disable: item.actions ? item.actions[2].checked : item?.approved,
					name: "Do-not-Approve",
					onClick: (allRecord, checked, key) => {
						chooseDontApproved(allRecord, collaborationLevels, checked, index, key, item);
					},
				},

				{
					checked: item.actions ? item.actions[2].checked : item?.approved,
					type: "checkbox",
					disable: item.actions ? item.actions[1].checked : item?.not_approved,
					name: "Approved",
					onClick: (allRecord, checked, key) => {
						chooseApproved(allRecord, collaborationLevels, checked, index, key, item);
					},
				},

				{
					type: "select",
					value: item.actions ? item.actions[3].value : item?.collaboration_level_id,
					name: "collaboration-level",
					disable: item.actions ? item.actions[1].checked : item?.not_approved,
					options: collaborationLevels,
					onClick: (e, allRecord, key) => {
						handleLevelChanged(e.target.value, allRecord, collaborationLevels, index, key, item);
					},
				},

				{
					name: "Note",
					icon: "nc-icon nc-caps-small",
					onClick: () => {
						historyNote(item);
					},
				},
			];

			processsedRecords.push(record);
		});
		setRecords(processsedRecords);
	};

	const handleApplyFilter = () => {
		let Request = {
			payload: {
				tableName: table_name,
				schoolClasses: selectedSchoolClasses,
				mainCourses: selectedMainCourses,
				tags: selectedTags,
			},
		};
		const url = `api/collaboration/get-records-to-approve`;
		setRecords(null);
		lessonPlanApi
			.getPickItems(url, Request)
			.then((response) => {
				handleResponse(response);
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};

	const handleSaveApprovedRecords = () => {
		if (reducer.length == 0) {
			toast.warning(tr`please edit any thing, then click save`);
		} else {
			setLoader(true);
			let request = {
				payload: {
					tableName: table_name,
					items: reducer,
				},
			};
			collaborationsCenterApi
				.saveApproveRecords(request)
				.then((response) => {
					setLoader(false);
					if (response.data.status === 1) {
						dispatch(emptyReducer());
						handleResponse(response, true);
					} else {
						toast.error(response.data.msg);
					}
				})
				.catch((error) => {
					toast.error(error?.message);
				});
		}
	};

	return (
		<div className="">
			{loader ? (
				<Loader />
			) : records ? (
				<PoolPicker
					table_name={unitPlan ? "unit_plans" : table_name}
					title={title}
					mainCourses={mainCourses}
					schoolClasses={schoolClasses}
					use_tags={true}
					tags={tags}
					handleApplyFilter={handleApplyFilter}
					setSelectedMainCourses={setSelectedMainCourses}
					selectedSchoolClasses={selectedSchoolClasses}
					selectedMainCourses={selectedMainCourses}
					selectedTags={selectedTags}
					setSelectedSchoolClasses={setSelectedSchoolClasses}
					setSelectedTags={setSelectedTags}
					Records={records}
					ListerClass={""}
					ItemClass={""}
					check={0}
					ckeckedItem={ckeckedItem}
					onCheckChange={""}
					firstPageUrl={firstPageUrl}
					lastPageUrl={lastPageUrl}
					currentPage={currentPage}
					lastPage={lastPage}
					prevPageUrl={prevPageUrl}
					nextPageUrl={nextPageUrl}
					total={total}
					getData={getData}
					parentHandleClose={parentHandleClose}
					pickItemToUnitPlan={pickItemToUnitPlan}
					checkedApprove={checkedApprove}
					handelSave={handleSaveApprovedRecords}
					hiddenSave={false}
					hiddenCancel={true}
					itemToShow={2}
					characterCount={10}
					marginT={"mt-0"}
					marginB={"mb-0"}
					marginTStandard={"mt-4"}
					marginBStandard={"mb-4"}
					showListerMode={"cardLister"}
				/>
			) : (
				<REmptyData line1={tr`no_items_to_show`} />
			)}
		</div>
	);
};

export default ApproveCallPoolPicker;
