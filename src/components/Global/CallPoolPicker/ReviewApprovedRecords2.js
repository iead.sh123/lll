import React, { useState } from "react";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import { useSelector, useDispatch } from "react-redux";
import { collaborationsCenterApi } from "api/seniorTeacher/collaborationsCenter";
import { useHistory } from "react-router";
import { changeCollaborationLevelReview, emptyReducer } from "store/actions/seniorTeacher/CollaborationCenter/collaborationCenter.actions";
import { get } from "config/api";
import RAdvancedLister from "../RComs/RAdvancedLister";
import Loader from "utils/Loader";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";
import { toast } from "react-toastify";

const ReviewApprovedRecords2 = () => {
	const dispatch = useDispatch();
	const history = useHistory();

	const [processsedRecords, setProcessedRecords] = useState([]);
	const [loader, setLoader] = useState(false);
	const [refreshData, setRefreshData] = useState(false);

	const { collaborationsApproved } = useSelector((state) => state.collaborationCenter);
	const { user } = useSelector((state) => state.auth);

	const exploreItem = (item) => {
		const tableName = item?.collaborative_type?.substring(11, 40);

		switch (tableName) {
			case "Lesson_Plan_Item":
				history.push(
					`${process.env.REACT_APP_BASE_URL}/${
						user?.type === "teacher"
							? "teacher"
							: user?.type === "school_advisor" || user?.type === "seniorteacher" || user?.type === "principal"
							? "senior-teacher"
							: ""
					}/lessonplan-item-viewer/${item.id}`
				);
				break;
			case "Lesson_Plan":
				history.push(
					`${process.env.REACT_APP_BASE_URL}/${
						user?.type === "teacher"
							? "teacher"
							: user?.type === "school_advisor" || user?.type === "seniorteacher" || user?.type === "principal"
							? "senior-teacher"
							: ""
					}/lessonplan-viewer/${item.id}`
				);
				break;
			case "UnitPlan":
				history.push(
					`${process.env.REACT_APP_BASE_URL}/${
						user?.type === "teacher"
							? "teacher"
							: user?.type === "school_advisor" || user?.type === "seniorteacher" || user?.type === "principal"
							? "senior-teacher"
							: ""
					}/unitplan-viewer/${item.collaborative.id}`
				);
				break;

			case "CourseResource":
				if (item.LinkURL) {
					window.open(`${item.LinkURL}`);
				} else if (item.attachments) {
					window.open(`${process.env.REACT_APP_RESOURCE_URL}${item.attachments[0].url}`);
				} else {
					return;
				}
				break;

			case "Topic":
				history.push(
					`${process.env.REACT_APP_BASE_URL}/${
						user?.type === "teacher"
							? "teacher"
							: user?.type === "school_advisor" || user?.type === "seniorteacher" || user?.type === "principal"
							? "senior-teacher"
							: ""
					}/courses/undefined/rabs/undefined/topics/view/${item.collaborative.id}`
				);
				break;

			case "Rubric":
				history.push(
					`${process.env.REACT_APP_BASE_URL}/${
						user?.type === "teacher"
							? "teacher"
							: user?.type === "school_advisor" || user?.type === "seniorteacher" || user?.type === "principal"
							? "senior-teacher"
							: ""
					}/courses/undefined/rubric-editor/${item.collaborative.id}/view`
				);
				break;

			default:
				history.push(
					`${process.env.REACT_APP_BASE_URL}/${
						user?.type === "teacher"
							? "teacher"
							: user?.type === "school_advisor" || user?.type === "seniorteacher" || user?.type === "principal"
							? "senior-teacher"
							: ""
					}/${tableName}/${item.collaborative.id}`
				);
				break;
		}
	};

	const handleLevelChanged = (e, recId) => {
		const resultObj = {
			levelId: e,
			item: recId,
		};
		dispatch(changeCollaborationLevelReview(resultObj));
	};

	const chooseItem = (item) => {
		// dispatch(deleteItem(item));
	};
	const handleSaveApprovedRecords = () => {
		if (collaborationsApproved.length == 0) {
			toast.warning(tr`No Changes To be Saved`);
		} else {
			setLoader(true);
			let request = {
				payload: {
					records: collaborationsApproved,
				},
			};
			collaborationsCenterApi
				.updateReviewedRecords(request)
				.then((response) => {
					if (response.data.status == 1) {
						setLoader(false);
						dispatch(emptyReducer());
						setRefreshData(true);
					} else {
						toast.error(response.data.msg);
					}
				})
				.catch((error) => {
					toast.error(error?.message);
				});
		}
	};

	const getDataFromBackend = async (specific_url) => {
		const url = "api/collaboration/review-approved-records";
		let response1 = await get(specific_url ? specific_url : url);

		if (response1) {
			return response1;
		}
	};

	const setTableData = (response) => {
		if (!response) {
			return [];
		}

		const data = response?.data?.data?.records?.data.map((rc) => ({
			id: rc.id,
			title: rc.title
				? rc.title
				: rc.collaborative.name
				? rc.collaborative.name
				: rc.collaborative.title
				? rc.collaborative.title
				: rc.collaborative.subject
				? rc.collaborative.subject
				: rc.collaborative.id,
			Titles: rc.title
				? [rc.title]
				: rc.collaborative.name
				? [rc.collaborative.name]
				: rc.collaborative.title
				? [rc.collaborative.title]
				: rc.collaborative.subject
				? [rc.collaborative.subject]
				: [rc.collaborative.id],

			initialCheckValue: true,
			tableName: "collaborationReview",
			images: [
				{
					src: `${process.env.REACT_APP_RESOURCE_URL}${rc.image}`,
				},
			],
			icons: [
				{
					src: `${process.env.REACT_APP_RESOURCE_URL}${rc.icon}`,
				},
			],
			details: [
				{
					icon: "nc-icon nc-check-2",
					key: tr("Pool"),
					value: rc.details ? rc.details[0].value : rc.collaborative_type,
				},
				{
					icon: "nc-icon nc-check-2",

					key: tr("Item"),
					value: rc.details
						? rc.details[1].value
						: rc.collaborative.name
						? rc.collaborative.name
						: rc.collaborative.title
						? rc.collaborative.title
						: rc.collaborative.subject
						? rc.collaborative.subject
						: rc.collaborative.id,
				},
				{
					icon: "nc-icon nc-check-2",
					key: tr("creator"),
					value: rc.details ? rc.details[2].value : rc.collaborative.creator.first_name,
				},

				{
					mode: "form",
					type: "select",
					name: "collaboration-level",
					key: tr`Collaboration Level`,
					option: response.data.data.collaboration_levels,
					// isDisabled: record && record.details && record?.details[3]?.checked,
					// minWidth: "200px",
					defaultValue: rc.collaboration_level,
					onChange: (e) => {
						handleLevelChanged(e.value, rc);
					},
				},
			],
			actions: [
				{
					name: tr("Explore"),
					icon: "fa fa-eye",
					showAction: true,
					disable:
						rc.collaborative_type == "App\\Models\\QuestionSet" || rc.collaborative_type == "App\\Models\\CourseResource" ? true : false,

					onClick: () => {
						exploreItem(rc);
					},
				},
				{
					id: collaborationsApproved.find((el) => el.id == rc.id) ? "delete" + rc.id : "Nominate" + rc.id,

					icon: iconsFa6.delete,
					name: tr("Remove"),

					color: collaborationsApproved.find((el) => el.id == rc.id) ? "success" : "info",
					onClick: () => {
						chooseItem(rc);
					},
				},
			],
		}));
		setProcessedRecords(response);
		setRefreshData(false);

		return data;
	};

	const propsLiterals = React.useMemo(
		() => ({
			listerProps: {
				records: setTableData(processsedRecords) || [],
				getDataFromBackend: getDataFromBackend,
				setData: setTableData,
				getDataObject: (response) => response.data.data.records,
				handelSaveFormData: handleSaveApprovedRecords,
				actionFormData: true,
				table_name: refreshData,
			},
		}),
		[processsedRecords, collaborationsApproved]
	);

	return <div className="">{loader ? <Loader /> : <RAdvancedLister {...propsLiterals.listerProps} />}</div>;
};

export default ReviewApprovedRecords2;
