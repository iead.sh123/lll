import { lessonPlanApi } from "api/teacher/lessonPlan";
import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router";
import tr from "components/Global/RComs/RTranslator";
import Loader from "utils/Loader";
import { emptyReducer, chooseItem } from "store/actions/seniorTeacher/CollaborationCenter/collaborationCenter.actions";
import PoolPicker from "../PoolPicker/PoolPicker";
import { collaborationsCenterApi } from "api/seniorTeacher/collaborationsCenter";
import { toast } from "react-toastify";

const CourseResourcesNomination = ({ title, parentHandleClose, url }) => {
	const [records, setRecords] = useState([]);
	const [ckeckedItem, setCkeckedItem] = useState([]);
	const [firstPageUrl, setFirstPageUrl] = useState(null);
	const [lastPageUrl, setLastPageUrl] = useState(null);
	const [nextPageUrl, setNextPageUrl] = useState(null);
	const [prevPageUrl, setPrevPageUrl] = useState(null);
	const [currentPage, setCurrentPage] = useState(null);
	const [lastPage, setLastPage] = useState(null);
	const [total, setTotal] = useState(null);
	const [loader, setLoader] = useState(false);

	const dispatch = useDispatch();

	const pickedItems = useSelector((state) => state.collaborationCenter.collaborationsApproved);

	useEffect(() => {
		getData(url);
	}, [url]);

	const getData = (url) => {
		setLoader(true);
		if (!url) {
			url = "api/course-resources/get-records-for-nomination";
		}
		lessonPlanApi
			.getRecordsNomination(url)
			.then((response) => {
				setLoader(false);
				handleResponse(response);
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};

	useEffect(() => {
		setCkeckedItem(pickedItems);
	}, [pickedItems]);

	const handleResponse = (response, save = false) => {
		if (response.data.status === 1) {
			let data = response.data.data;
			setCurrentPage(data.current_page);
			setLastPage(data.last_page);
			setFirstPageUrl(data.first_page_url);
			setLastPageUrl(data.last_page_url);
			setNextPageUrl(data.next_page_url);
			setPrevPageUrl(data.prev_page_url);
			setTotal(data.total);

			// if (data.data.length != 0) {
			createArrayLister(data.data);
			// }
		} else {
			toast.error(response.data.msg);
		}
	};

	const history = useHistory();

	const exploreLessonPlanItem = (item) => {
		if (item.LinkURL) {
			window.open(`${item.LinkURL}`);
		} else {
			window.open(`${process.env.REACT_APP_RESOURCE_URL}${item.attachments[0].url}`);
		}
	};
	const chooseAnItem = (item) => {
		dispatch(chooseItem(item));
	};

	const createArrayLister = (allRecord, collaborationLevels) => {
		let processsedRecords = [];

		allRecord.map((item) => {
			let record = {};
			record.id = item.id;
			record.allTitle = item.title;
			record.title = item.title.substring(0, 25);
			record.initialCheckValue = true;
			record.Titles = [item?.title];

			record.images = [
				{
					src: `${process.env.REACT_APP_RESOURCE_URL}${item.image}`,
					placeholder: "11",
				},
			];

			record.icons = [
				{
					src: `${process.env.REACT_APP_RESOURCE_URL}${item.icon}`,
					placeholder: "11",
				},
			];

			record.details = [
				{
					icon: "nc-icon nc-check-2",
					key: "Title",
					value: item.title.substring(0, 25),
				},
				{
					icon: "nc-icon nc-check-2",
					key: "description",
					value: item.description,
				},
				{
					icon: "nc-icon nc-check-2",

					key: "LinkURL",
					value: item.LinkURL,
				},
			];

			record.actions = [
				{
					name: "Explore",
					icon: "fa fa-eye",
					showAction: true,

					onClick: () => {
						exploreLessonPlanItem(item);
					},
				},

				{
					icon: "fa fa-check-circle",
					name: "Nominate",
					onClick: () => {
						chooseAnItem(item);
					},
				},
			];

			processsedRecords.push(record);
		});

		setRecords(processsedRecords);
	};

	const handleApplyFilter = () => {
		let Request = {
			payload: {
				tableName: table_name,
				schoolClasses: selectedSchoolClasses,
				mainCourses: selectedMainCourses,
				tags: selectedTags,
			},
		};
		const url = `api/collaboration/pick-items`;
		setRecords(null);
		lessonPlanApi
			.getPickItems(url, Request)
			.then((response) => {
				handleResponse(response);
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};

	const handleNominateSave = () => {
		if (pickedItems.length == 0) {
			toast.warning(tr`No Changes To be Saved`);
		} else {
			setLoader(true);
			let newArr = [];
			for (let i = 0; i < pickedItems.length; i++) {
				newArr.push(pickedItems[i].id);
			}
			let request = {
				payload: {
					ids: newArr,
				},
			};
			collaborationsCenterApi
				.saveNominate(request)
				.then((response) => {
					if (response.data.status == 1) {
						dispatch(emptyReducer());
						getData();
					} else {
						toast.error(response.data.msg);
					}
				})
				.catch((error) => {
					toast.error(error?.message);
				});
		}
	};

	return (
		<div className="">
			{loader ? (
				Loader()
			) : records.length != 0 ? (
				<PoolPicker
					handleApplyFilter={handleApplyFilter}
					Records={records}
					check={1}
					ckeckedItem={ckeckedItem}
					firstPageUrl={firstPageUrl}
					lastPageUrl={lastPageUrl}
					currentPage={currentPage}
					lastPage={lastPage}
					prevPageUrl={prevPageUrl}
					nextPageUrl={nextPageUrl}
					total={total}
					getData={getData}
					parentHandleClose={parentHandleClose}
					hiddenFilter={true}
					hiddenSave={false}
					hiddenCancel={true}
					handelSave={handleNominateSave}
					itemToShow={2}
					showListerMode={"cardLister"}
					showCheckIcon={true}
				/>
			) : (
				<div className="alert alert-danger text-center second-bg-color" role="alert">
					No items to show
				</div>
			)}
		</div>
	);
};

export default CourseResourcesNomination;
