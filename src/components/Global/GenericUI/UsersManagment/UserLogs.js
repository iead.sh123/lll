import React, { useState } from "react";
import { Table } from "reactstrap";
import { get, post } from "config/api";
import RAdvancedLister from "../../RComs/RAdvancedLister";
import AppModal from "components/Global/ModalCustomize/AppModal";
import tr from "../../RComs/RTranslator";
import moment from "moment";

const UserLogs = ({ userId, recordId, tableName }) => {
  const [processsedRecords, setProcessedRecords] = useState([]);
  const [showDataAfter, setShowDataAfter] = useState(false);
  const [dataAfter, setDataAfter] = useState({});

  const getDataFromBackend = async (specific_url) => {
    if (recordId && tableName) {
      let Request = {
        payload: {
          id: recordId,
          TableName: tableName,
          with_pagination: true,
        },
      };

      const urlGeneric = `api/generic/get-history-record`;
      let response0 = await post(
        specific_url ? specific_url : urlGeneric,
        Request
      );

      if (response0) {
        return response0;
      }
    } else {
      const urlUsers = `api/users/history?user_id=${userId}&with_pagination=${true}`;

      let response1 = await get(specific_url ? specific_url : urlUsers);

      if (response1) {
        return response1;
      }
    }
  };

  const setData = (response) => {
    let processsedRecords1 = [];
    response?.data?.data.logs?.data.map((r) => {
      let record = {};

      record.allTitle = r?.operation;
      record.title = r?.operation;
      record.table_name = "userLogs";

      record.details = [
        {
          key: tr`logable_type`,
          value: r.logable_type,
          hidden: userId ? false : true,
        },
        {
          key: tr`operation`,
          value: r?.operation,
        },
        {
          key: tr`actor`,
          value: JSON.parse(r.actor).full_name,
          hidden: userId ? true : false,
        },
        {
          key: tr`date`,
          value: moment(new Date(r.created_at).toString()).format(
            "MM-DD-YYYY hh:mm:ss a"
          ),
        },
        {
          key: tr`data_after`,
          value: (
            <a
              style={{ color: "#fd832c" }}
              onClick={() => {
                showModalDataAfter();
                setDataAfter(JSON.parse(r.data_after));
              }}
            >{tr`show`}</a>
          ),
        },
      ];

      processsedRecords1.push(record);
    });
    setProcessedRecords(processsedRecords1);
  };

  const showModalDataAfter = () => {
    setShowDataAfter(true);
  };
  const handleCloseDataAfter = () => {
    setShowDataAfter(false);
  };

  return (
    <>
      <AppModal
        size="lg"
        show={showDataAfter}
        parentHandleClose={handleCloseDataAfter}
        header={tr`data_after`}
        headerSort={
          <Table responsive>
            <thead>
              <tr>
                {Object.keys(dataAfter).map((key, index) => (
                  <th style={{ textAlign: "center" }} key={index}>
                    {key}
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              <tr>
                {Object.values(dataAfter).map((value, index) => (
                  <td style={{ textAlign: "center" }} key={index}>
                    {value}
                  </td>
                ))}
              </tr>
            </tbody>
          </Table>
        }
      />
      <RAdvancedLister
        getDataFromBackend={getDataFromBackend}
        setData={setData}
        records={processsedRecords}
        getDataObject={(response) => response.data.data.logs}
        characterCount={15}
        marginT={"mt-3"}
        marginB={"mb-2"}
        showListerMode="tableLister"
      />
    </>
  );
};

export default UserLogs;
