import React, { useState, useEffect } from "react";
import RAddEdit from "components/Global/RComs/RAddEdit";
import { useHistory, useParams } from "react-router-dom";
import { adminApi } from "api/admin/UsersManagments/index";
import tr from "../../RComs/RTranslator";
import { useSelector, useDispatch } from "react-redux";
import { fetchGradesLevels, fetchUserById } from "store/actions/admin/manageSchool.Action";
import Loader from "utils/Loader";
import { toast } from "react-toastify";

const UsersAddEdit = () => {
	const history = useHistory();
	const dispatch = useDispatch();
	const { entity_specific_name, entity_specific_id } = useParams();

	const [changeNameButton, setChangeNameButton] = useState(false);
	const [optionsValue, setOptionsValue] = useState([]);
	const [educationStageOptionsValue, setEducationStageOptionsValue] = useState([]);
	const { allGradesLevels, allEducationStages, user, user_loading, gradesLevels_loading } = useSelector((state) => state.manageSchoolRed);

	//set education stage name in select in edit principals
	const educationstatename = educationStageOptionsValue.filter((e) => {
		if (e.value == user?.education_stage_id) {
			return true;
		}
	});

	//Global State
	const [formGroup, setFormGroup] = useState({
		username: "",
		password: "",
		first_name: "",
		last_name: "",
		class_id: null,
		full_name: "",
		email1: null,
		email2: null,
		student_nb: "",
		school_user_id: null,
		education_stage_id: null,
		id: null,
		user_id: null,
	});

	//Global Inputs
	const mutual_inputs = [
		{
			id: 0,
			type: "text",
			name: "username",
			label: tr`user_name`,
			required: true,
			onChange: (e) => handleChange(e),
			defaultValue: entity_specific_id && user?.school_user?.user?.username,
		},
		{
			id: 1,
			type: "password",
			name: "password",
			label: tr`password`,
			required: true,
			onChange: (e) => handleChange(e),
			defaultValue: entity_specific_id && user?.school_user?.user?.password,
		},
		{
			id: 2,
			type: "text",
			name: "first_name",
			label: tr`first_name`,
			required: true,
			onChange: (e) => handleChange(e),
			defaultValue: entity_specific_id && user?.school_user?.user?.first_name,
		},
		{
			id: 3,
			type: "text",
			name: "last_name",
			label: tr`last_name`,
			required: true,
			onChange: (e) => handleChange(e),
			defaultValue: entity_specific_id && user?.school_user?.user?.last_name,
		},
		{
			id: 4,
			type: "text",
			name: "full_name",
			label: tr`full_name`,
			required: true,
			onChange: (e) => handleChange(e),
			defaultValue: entity_specific_id && user?.school_user?.user?.full_name,
		},
		{
			id: 5,
			type: "email",
			name: "email1",
			label: tr`email1`,
			onChange: (e) => handleChange(e),
			defaultValue: entity_specific_id && user?.school_user?.user?.email1,
			// disabled: changeNameButton ? true : false,
		},
		{
			id: 6,
			type: "email",
			name: "email2",
			label: tr`email2`,
			onChange: (e) => handleChange(e),
			defaultValue: entity_specific_id && user?.school_user?.user?.email2,
		},
	];

	//Customized Inputs For Each User
	if (entity_specific_name == "students") {
		mutual_inputs.push(
			{
				id: 7,
				type: "text",
				name: "student_nb",
				label: tr`student_number`,
				onChange: (e) => handleChange(e),
				defaultValue: entity_specific_id && user?.student_nb,
			},
			{
				id: 8,
				type: "select",
				name: "class_id",
				label: tr`grade_level`,
				option: optionsValue,
				onChange: (e) => handleSelectChange(e),
				defaultValue: [
					{
						value: entity_specific_id && user?.class_id,
						label: entity_specific_id && user?.school_class?.code,
					},
				],
				// isDisabled: changeNameButton ? true : false,
			}
		);
	} else if (entity_specific_name == "principals") {
		mutual_inputs.push({
			id: 7,
			type: "select",
			name: "education_stage_id",
			label: tr`education_stage`,
			option: educationStageOptionsValue,
			onChange: (e) => handleSelectEducationStage(e),
			defaultValue: [
				{
					value: entity_specific_id && user?.education_stage_id,
					label: entity_specific_id && educationstatename[0]?.label,
				},
			],
		});
	}

	//Action Add Or Edit
	const onAddEditSubmit = (e) => {
		e.preventDefault();
		if (changeNameButton) {
			let request = {
				payload: {
					record: {
						username: formGroup.username ? formGroup.username : user?.school_user?.user?.username,
						password: formGroup.password ? formGroup.password : user?.school_user?.user?.password,
						first_name: formGroup.first_name ? formGroup.first_name : user?.school_user?.user?.first_name,
						last_name: formGroup.last_name ? formGroup.last_name : user?.school_user?.user?.last_name,
						class_id: formGroup.class_id ? formGroup.class_id : user.class_id,
						full_name: formGroup.full_name ? formGroup.full_name : user?.school_user?.user?.full_name,
						email1: formGroup.email1 !== null ? formGroup.email1 : user?.school_user?.user?.email1,
						email2: formGroup.email2 !== null ? formGroup.email2 : user?.school_user?.user?.email2,
						student_nb: formGroup.student_nb ? formGroup.student_nb : user.student_nb,
						// school_user_id: formGroup.school_user_id
						//   ? formGroup.school_user_id
						//   : user?.school_user_id,
						school_user_id: entity_specific_id ? user?.school_user_id : +"null",
						education_stage_id: formGroup?.education_stage_id ? formGroup?.education_stage_id : user.school_class?.education_stage_id,
						id: entity_specific_id,
						user_id: entity_specific_id ? user?.school_user?.user_id : +"null",
					},
					table_name: entity_specific_name,
				},
			};

			adminApi
				.addUsers(request)
				.then((response) => {
					if (response.data.status == 1) {
						toast.success(tr`Added Successfully`);

						history.push(`${process.env.REACT_APP_BASE_URL}/admin/users-management/${entity_specific_name}`);
					} else {
						toast.error(response.data.msg);
					}
				})
				.catch((error) => {
					toast.error(error?.message);
				});
		}
	};

	//HandelChange Inputs
	const handleChange = (e) => {
		setFormGroup({
			...formGroup,
			[e.target.name]: e.target.value,
		});
	};

	//Handel Select Used Student
	const handleSelectChange = (e) => {
		setFormGroup({
			...formGroup,
			class_id: e.value,
		});
	};

	//Handel Select Used Principals
	const handleSelectEducationStage = (e) => {
		setFormGroup({
			...formGroup,
			education_stage_id: e.value,
		});
	};

	//Option Used In Student
	const getOptions = () => {
		const arrayData = [];
		allGradesLevels &&
			allGradesLevels.map((gradelevel) => {
				arrayData.push({
					value: gradelevel.id,
					label: gradelevel.code,
				});
			});
		setOptionsValue(arrayData);
	};

	//Option Used In Principals
	const getEducationStageOptions = () => {
		const arrayData = [];
		allEducationStages &&
			allEducationStages.map((educationStage) => {
				arrayData.push({
					value: educationStage.id,
					label: educationStage.name,
				});
			});
		setEducationStageOptionsValue(arrayData);
	};

	useEffect(() => {
		if (entity_specific_name == "students" || entity_specific_name == "principals") {
			dispatch(fetchGradesLevels());
		}
		//Use This Endpoint In Edit
		if (entity_specific_id) {
			dispatch(
				fetchUserById(entity_specific_name, {
					payload: { id: entity_specific_id },
				})
			);
		}
	}, []);

	//Fetch Option To Use In Select In Student And Principals
	useEffect(() => {
		if (entity_specific_name == "students") {
			getOptions();
		} else if (entity_specific_name == "principals") {
			getEducationStageOptions();
		}
	}, [allGradesLevels, allEducationStages]);

	return (
		<>
			{entity_specific_id && (user_loading || gradesLevels_loading) ? (
				Loader()
			) : (
				<>
					<RAddEdit
						title={entity_specific_id ? "Edit " + entity_specific_name : "Add " + entity_specific_name}
						initialFormItems={mutual_inputs}
						onSubmit={(e) => onAddEditSubmit(e)}
						oldObject={entity_specific_id && mutual_inputs}
						onClick={() => setChangeNameButton(!changeNameButton)}
						type={changeNameButton ? "submit" : "button"}
						changeNameButton={changeNameButton}
					/>
				</>
			)}
		</>
	);
};

export default UsersAddEdit;
