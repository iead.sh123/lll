import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {useParams} from 'react-router-dom';
import EmployeeLayout from '../../EmployeeLayout';
import GenericUIManyToMany from './GenericUIManyToMany';


import {resetStore, getTableData, getTableTitle} from '../../../../store/actions/genericUIHasManyActions';
import axios from "../../../../utils/Http";
import {backendUrl} from "../../../../config/constants";

const GenericUIHasMany = ({match}) => {

    let masterTable = match.params['master'];
    let detailTable = match.params['detail'];
    let pivotTable = match.params['pivot'] == undefined ? detailTable : match.params['pivot'];
    const [readPermission, setReadPermission] = useState(false);
    const [permissionMessage, setPermissionMessage] = useState(null);
    const [forceRerender, setForceRerender] = useState(null);
    const dispatch = useDispatch();
    useEffect(() => {
        axios.get(backendUrl + 'api/permissions/user-table-permission/' + pivotTable + '/read')
            .then(response => {
                if (response.data.status == 1) {
                    let data = response.data.data;
                    setReadPermission(data.hasPermission);
                    setPermissionMessage(data.messg);
                }
            }).catch(error => {
        });
        dispatch(resetStore());
        dispatch(getTableData(masterTable, 'master', null));
        dispatch(getTableData(detailTable, 'detail', null));
        dispatch(getTableTitle(masterTable, 'master'));
        dispatch(getTableTitle(detailTable, 'detail'));
        setForceRerender(1);
    }, [masterTable, detailTable])
    return (
        <EmployeeLayout>
            <div className="container-fluid mt-5" style={{marginLeft:'23px'}}>
                            <div className="row justify-content-center">
                <div className='col-md-12'>
                    <section className='employee-home'>
                        <div className='container'>
                            <div className='emp-cont'>
                                <div className='row justify-content-center'>
                                    <div className='col-md-12'>
                                        {!readPermission ?
                                            <div className='row'>
                                                <div className='col-3'></div>
                                                <div className='col-3'>
                                                    <h5>{permissionMessage}</h5>
                                                </div>
                                            </div>
                                            :
                                            <GenericUIManyToMany masterTable={masterTable} detailTable={detailTable}
                                                                 pivotTable={pivotTable}/>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            </div>
        </EmployeeLayout>
    );
}

export default GenericUIHasMany;