import React from 'react';
import EmployeeLayout from '../../EmployeeLayout';
import GenericUIMaster from './GenericUIMaster';
import GenericUIDetail from './GenericUIDetail';
import GenericUIPivot from './GenericUIPivot';

const GenericUIManyToMany = ({masterTable, detailTable, pivotTable}) => {
    return (
        <div className='row col-12'>
            <div className='col-4'>
                <GenericUIMaster masterTable={masterTable}/>
            </div>
            <div className='col-4'>
                <GenericUIPivot pivotTable={pivotTable} masterTable={masterTable} detailTable={detailTable}/>
            </div>
            <div className='col-4'>
                <GenericUIDetail masterTable={masterTable} detailTable={detailTable} pivotTable={pivotTable}/>
            </div>
        </div>
    );
}

export default GenericUIManyToMany;