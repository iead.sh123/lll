import React, { useState } from "react";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import tr from "../../../../Global/RComs/RTranslator";
import styles from "../genericUIRelations.module.scss";
import ExcelPage from "../../ExcelPage";

const Stage1 = ({
  listerProps,
  actions,
  hasAddPermission,
  hasAddNewChildPermission,
  ok_to_upload_from_file,
  hasPublishReport,
}) => (
  <>
    <RAdvancedLister {...listerProps} />

    {(hasAddPermission ||
      hasAddNewChildPermission ||
      ok_to_upload_from_file ||
      hasPublishReport) && (
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
          padding: "5px",
        }}
      >
        {hasAddPermission && (
          <label
            className={styles["Add-Button"] + " cursor-pointer ml-2 mr-2"}
            onClick={actions.handleAdd}
          >
            <i
              className="nc-icon nc-simple-add "
              style={{ cursor: "pointer" }}
            >{tr`add existing`}</i>
          </label>
        )}

        {hasAddNewChildPermission && (
          <label
            className={styles["Add-Button"] + " cursor-pointer ml-2 mr-2"}
            onClick={actions.handleAddNew}
          >
            <i
              className="nc-icon nc-simple-add "
              style={{ cursor: "pointer" }}
            >{tr`Add New Detail`}</i>
          </label>
        )}
        {ok_to_upload_from_file && (
          <label
            className={styles["Add-Button"] + " cursor-pointer ml-2 mr-2"}
            onClick={actions.handleShowExcel}
          >
            <i style={{ cursor: "pointer" }}>
              {ok_to_upload_from_file?.button_text}
            </i>
          </label>
        )}
        {hasPublishReport && (
          <label
            className={styles["Add-Button"] + " cursor-pointer ml-2 mr-2"}
            onClick={actions.handlePublishReport}
          >
            <i style={{ cursor: "pointer" }}>{tr`Publish To Parents`}</i>
          </label>
        )}
      </div>
    )}
  </>
);

export default Stage1;
