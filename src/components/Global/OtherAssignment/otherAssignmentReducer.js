export const TYPES = {
  INITIALIZE_ASSIGNMENT: "INITIALIZE_ASSIGNMENT",
  GO_TO_NEXT_GROUP: "GO_TO_NEXT_GROUP",
  GO_TO_PREVIOUS_GROUP: "GO_TO_PREVIOUS_GROUP",
  GO_TO_NEXT_TOPIC: "GO_TO_NEXT_TOPIC",
  GO_TO_PREVIOUS_TOPIC: "GO_TO_PREVIOUS_TOPIC",
  GO_TO_NEXT_QUESTION: "GO_TO_NEXT_QUESTION",
  GO_TO_PREVIOUS_QUESTION: "GO_TO_PREVIOUS_QUESTION",
  DISABLE_NEXT_BUTTON: "DISABLE_NEXT_BUTTON",
  TOGGLE_PREVIOUS_BUTTON: "TOGGLE_PREVIOUS_BUTTON",
  CHANGE_QUESTION_TREE: "CHANGE_QUESTION_TREE",
};

export const otherAssignmentInitialState = {
  nextButtonDisabled: false,
  previousButtonDisabled: true,
  currentActiveGroup: 0,
  currentActiveTopic: 0,
  currentActiveQuestion: 0,
  StudentQsQuestionId: null,
  StudentQsPatternId: null,
  assignmentReadMode: false,
  studentsTotalMarks: [],
};

export const otherAssignmentReducer = (state, action) => {
  const { type, payload } = action;

  switch (type) {
    case TYPES.INITIALIZE_ASSIGNMENT:
      return {
        ...state,
        StudentQsQuestionId:
          payload?.studentQsQuestionId?.inst_pattern_groups[
            state?.currentActiveGroup
          ]?.inst_topics[state?.currentActiveTopic]?.inst_questions[
            state?.currentActiveQuestion
          ]?.id,
        StudentQsPatternId: payload?.studentQsPatternId,
      };

    case TYPES.GO_TO_NEXT_GROUP:
      return {
        ...state,
        currentActiveGroup: state.currentActiveGroup + 1,
        currentActiveQuestion: 0,
        currentActiveTopic: 0,
        StudentQsQuestionId:
          payload?.inst_pattern_groups[state?.currentActiveGroup + 1]
            ?.inst_topics[0]?.inst_questions[0]?.id,
        StudentQsPatternId:
          payload?.inst_pattern_groups[state?.currentActiveGroup + 1]?.id,
      };
    case TYPES.GO_TO_PREVIOUS_GROUP:
      return {
        ...state,
        currentActiveGroup: state.currentActiveGroup - 1,
        currentActiveQuestion: payload.question,
        currentActiveTopic: payload.topic,
        StudentQsQuestionId:
          payload?.lessonGroups1?.inst_pattern_groups[
            state?.currentActiveGroup - 1
          ]?.inst_topics[payload.topic]?.inst_questions[payload.question]?.id,
      };

    case TYPES.GO_TO_NEXT_TOPIC:
      return {
        ...state,
        currentActiveTopic: state.currentActiveTopic + 1,
        currentActiveQuestion: 0,
        StudentQsQuestionId:
          payload?.inst_pattern_groups[state?.currentActiveGroup]?.inst_topics[
            state?.currentActiveTopic + 1
          ]?.inst_questions[0]?.id,
      };
    case TYPES.GO_TO_PREVIOUS_TOPIC:
      return {
        ...state,
        currentActiveTopic: state.currentActiveTopic - 1,
        currentActiveQuestion: payload.lessonGroups0,
        nextButtonDisabled: false,
        StudentQsQuestionId:
          payload?.lessonGroups1?.inst_pattern_groups[state?.currentActiveGroup]
            ?.inst_topics[state?.currentActiveTopic - 1]?.inst_questions[
            payload.lessonGroups0
          ]?.id,
      };

    case TYPES.GO_TO_NEXT_QUESTION:
      return {
        ...state,
        currentActiveQuestion: state.currentActiveQuestion + 1,
        previousButtonDisabled: false,
        StudentQsQuestionId:
          payload?.inst_pattern_groups[state?.currentActiveGroup]?.inst_topics[
            state?.currentActiveTopic
          ]?.inst_questions[state?.currentActiveQuestion + 1]?.id,
      };
    case TYPES.GO_TO_PREVIOUS_QUESTION:
      return {
        ...state,
        currentActiveQuestion: state.currentActiveQuestion - 1,
        nextButtonDisabled: false,
        StudentQsQuestionId:
          payload?.inst_pattern_groups[state?.currentActiveGroup]?.inst_topics[
            state?.currentActiveTopic
          ]?.inst_questions[state?.currentActiveQuestion - 1]?.id,
      };

    case TYPES.DISABLE_NEXT_BUTTON: {
      return {
        ...state,
        nextButtonDisabled: payload,
      };
    }

    case TYPES.TOGGLE_PREVIOUS_BUTTON: {
      return {
        ...state,
        previousButtonDisabled: payload,
      };
    }

    case TYPES.CHANGE_QUESTION_TREE: {
      if (!(payload.info && payload.info.type)) {
        return state;
      }
      if (payload.info.type === "group") {
        return {
          ...state,
          currentActiveGroup: payload.info.groupId,
          currentActiveTopic: 0,
          currentActiveQuestion: 0,
          StudentQsQuestionId:
            payload?.lessonGroups?.inst_pattern_groups[payload.info.groupId]
              ?.inst_topics[0]?.inst_questions[0]?.id,
        };
      }
      if (payload.info.type === "topic") {
        return {
          ...state,
          currentActiveGroup: payload.info.groupId,
          currentActiveTopic: payload.info.topicId,
          currentActiveQuestion: 0,
          StudentQsQuestionId:
            payload?.lessonGroups?.inst_pattern_groups[payload.info.groupId]
              ?.inst_topics[payload.info.topicId]?.inst_questions[0]?.id,
        };
      }
      if (payload.info.type === "question") {
        return {
          ...state,
          currentActiveGroup: payload.info.groupId,
          currentActiveTopic: payload.info.topicId,
          currentActiveQuestion: payload.info.questionId,
          StudentQsQuestionId:
            payload?.lessonGroups?.inst_pattern_groups[payload.info.groupId]
              ?.inst_topics[payload.info.topicId]?.inst_questions[
              payload.info.questionId
            ]?.id,
        };
      }
    }
  }
};
