import React, { useEffect, useState } from "react";
import { resourcesBaseUrl } from "config/constants";
import ImageEnlarge from "utils/ImageEnlarge";

const SingleAnswerQuestion = ({ choices, assignmentReadMode, student }) => {
  const [selectedChoices, setSelectedChoices] = useState([]);
  const [correctAnswer, setCorrectAnswer] = useState();

  useEffect(() => {
    if (assignmentReadMode) {
      if (student.detail.notSolved === 1) {
        setSelectedChoices([]);
        setCorrectAnswer([]);
        return;
      }
      setSelectedChoices(student.detail.choices.map((choice) => choice));
      setCorrectAnswer(
        choices.filter((choice) => choice.is_correct === 1)[0].id
      );
    }
  }, []);

  return choices.map((choice, index) => {
    return (
      <div
        key={index + choice.id}
        className="col-md-12 d-flex align-items-center justify-content-center row mt-2"
        style={{ width: "600px" }}
      >
        <div
          className={`${choice.attachments ? "col-md-7" : "col-md-11"} mr-auto`}
        >
          <div
            className={`form-control ${
              choice.id === correctAnswer
                ? "correct_answer"
                : student.detail.choices.find((el) => el) == choice.id &&
                  choice.id !== correctAnswer
                ? "wrong_answer"
                : ""
            }`}
          >
            {choice.choice && choice.choice.includes("<math") ? (
              <MathJax math={choice.choice} />
            ) : (
              <div dangerouslySetInnerHTML={{ __html: choice.choice }} />
            )}
          </div>
        </div>
        <div className="col-md-1 text-center">
          <div className="custom-control custom-checkbox custom-control-inline mx-0 pl-3">
            <div className="form-check">
              <input
                name={`student${student.id}IsCorrect${choice.id}`}
                id={`student${student.id}IsCorrect${choice.id}`}
                type="radio"
                value={choice.id}
                checked={selectedChoices.includes(choice.id)}
                className="form-check-input assignment_checkbox"
                disabled={assignmentReadMode}
              />
            </div>
          </div>
        </div>
        {choice.attachments && (
          <div className="col-md-3" style={{ textAlign: "center" }}>
            {["png", "jpeg"].includes(choice.attachments.mime_type) ? (
              <ImageEnlarge
                imageSrc={resourcesBaseUrl + choice.attachments.url}
              />
            ) : (
              <a
                href={resourcesBaseUrl + choice.attachments.url}
                alt=""
                className="btn btn-primary"
              >
                Download
              </a>
            )}
          </div>
        )}
      </div>
    );
  });
};

export default SingleAnswerQuestion;
