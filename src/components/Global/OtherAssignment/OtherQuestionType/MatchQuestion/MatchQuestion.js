import React, { useEffect, useRef, useState } from "react";
import { shuffleArray, getRandomColor } from "utils/helper";
import { resourcesBaseUrl } from "config/constants";
import ImageEnlarge from "utils/ImageEnlarge";
import { Row, Col } from "reactstrap";

const MatchQuestion = ({ choices, assignmentReadMode, student }) => {
  const [selectedChoices, setSelectedChoices] = useState([]);

  const choicesColorRef = useRef([]);
  const choicesLeftRef = useRef([]);
  const choicesRightRef = useRef([]);

  useEffect(() => {
    choices.map((choice, index) => {
      if (index % 2 === 0) {
        choicesLeftRef.current.push(choice);
      } else {
        choicesRightRef.current.push(choice);
      }
    });
    if (assignmentReadMode) {
      if (student.detail.notSolved === 1) {
        setSelectedChoices([]);
        return;
      }
      setSelectedChoices(student.detail.choices.map((choice) => choice));
      student.detail.choices.map((choice, index) => {
        if (index % 2 === 0) {
          choicesColorRef.current.push({
            id: choice,
            color: getRandomColor(),
          });
        } else {
          choicesColorRef.current.push({
            id: choice,
            color: choicesColorRef.current[index - 1].color || "",
          });
        }
      });

      return;
    }

    choicesRightRef.current = shuffleArray(choicesRightRef.current);
  }, []);

  const chooseBackgroundColor = (choiceId) => {
    let chosenColor;
    if (assignmentReadMode) {
      chosenColor = choicesColorRef.current.filter(
        (choiceColor) => choiceColor.id === choiceId
      )[0];
    } else {
      chosenColor = choicesColor.filter(
        (choiceColor) => choiceColor.id === choiceId
      )[0];
    }
    return chosenColor ? chosenColor.color : "";
  };

  return (
    <Row style={{ width: "600px" }}>
      <Col md={6} style={{ textAlign: "center" }}>
        {choicesLeftRef.current.map((leftChoice) => {
          return (
            <div key={leftChoice.id} className="form-check mb-3">
              <label
                className="form-check-label assignment_match_label"
                style={{
                  backgroundColor: chooseBackgroundColor(leftChoice.id),
                }}
              >
                <input
                  className="form-check-input assignment_checkbox"
                  id={leftChoice.id}
                  disabled={assignmentReadMode}
                  checked={selectedChoices.includes(leftChoice.id)}
                  // onChange={(e) => leftColumnAnswersHandler(e, leftChoice.id)}
                  type="checkbox"
                  readOnly={assignmentReadMode}
                />
                {leftChoice.choice && leftChoice.choice.includes("<math") ? (
                  <MathJax math={leftChoice.choice} />
                ) : (
                  <div
                    dangerouslySetInnerHTML={{ __html: leftChoice.choice }}
                  />
                )}
              </label>
              {leftChoice.attachments && (
                <div className="col-sm-10">
                  {["png", "jpeg"].includes(
                    leftChoice.attachments.mime_type
                  ) ? (
                    <ImageEnlarge
                      imageStyle={{
                        outline: `${chooseBackgroundColor(
                          leftChoice.id
                        )} solid 10px`,
                      }}
                      imageSrc={resourcesBaseUrl + leftChoice.attachments.url}
                    />
                  ) : (
                    <a
                      href={resourcesBaseUrl + leftChoice.attachments.url}
                      alt=""
                      class="btn btn-primary"
                    >
                      {translate("Download")}
                    </a>
                  )}
                </div>
              )}
            </div>
          );
        })}
      </Col>
      <Col md={6} style={{ textAlign: "center" }}>
        {choicesRightRef.current.map((rightChoice) => {
          return (
            <div key={rightChoice.id} className="form-check mb-3">
              <label
                className="form-check-label assignment_match_label"
                style={{
                  backgroundColor: chooseBackgroundColor(rightChoice.id),
                }}
              >
                <input
                  className="form-check-input assignment_checkbox"
                  id={rightChoice.id}
                  disabled={assignmentReadMode}
                  checked={selectedChoices.includes(rightChoice.id)}
                  // onChange={(e) => rightColumnAnswersHandler(e, rightChoice.id)}
                  type="checkbox"
                />
                {rightChoice.choice && rightChoice.choice.includes("<math") ? (
                  <MathJax math={rightChoice.choice} />
                ) : (
                  <div
                    dangerouslySetInnerHTML={{ __html: rightChoice.choice }}
                  />
                )}
              </label>

              {rightChoice.attachments && (
                <div className="col-sm-10">
                  {["png", "jpeg"].includes(
                    rightChoice.attachments.mime_type
                  ) ? (
                    <ImageEnlarge
                      imageStyle={{
                        outline: `${chooseBackgroundColor(
                          rightChoice.id
                        )} solid 10px`,
                      }}
                      imageSrc={resourcesBaseUrl + rightChoice.attachments.url}
                    />
                  ) : (
                    <a
                      href={resourcesBaseUrl + rightChoice.attachments.url}
                      alt=""
                      class="btn btn-primary"
                    >
                      {translate("Download")}
                    </a>
                  )}
                </div>
              )}
            </div>
          );
        })}
      </Col>
    </Row>
  );
};

export default MatchQuestion;
