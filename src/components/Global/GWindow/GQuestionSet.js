import React, { useEffect, useMemo, useState } from "react";
import RWindowScroller from "../RComs/RWindowScroller/RWindowScroller";
import tr from "../RComs/RTranslator";
import TopicEditor from "components/TopicEditor/TopicEditor";
import iconsFa6 from "variables/iconsFa6";

const GQuestionSet = () => {
	const [windowData, setWindowData] = useState({});

	const [pattern, setPattern] = useState([
		{
			key: "1",
			patternName: "Pattern A",
			id: "1",
			PatternId: 0,
			group: [
				{
					GroupName: "Group",
					key: "1",
					id: "1",
					GroupId: 0,
					topic: [
						{
							TopicName: "Topic",
							key: "1",
							id: "1",
							TopicId: 0,
							question: [
								{
									QuestionName: "Question",
									key: "1",
									id: "1",
									QuestionId: 0,
								},
								{
									QuestionName: "Question1",
									key: "2",
									id: "2",
									QuestionId: 1,
								},
							],
						},
					],
				},
				{
					GroupName: "Group1",
					key: "2",
					id: "2",
					GroupId: 1,
					topic: [
						{
							TopicName: "Topic",
							key: "1",
							id: "1",
							TopicId: 0,
							question: [
								{
									QuestionName: "Question",
									key: "1",
									id: "1",
									QuestionId: 0,
								},
								{
									QuestionName: "Question1",
									key: "2",
									id: "2",
									QuestionId: 1,
								},
							],
						},
					],
				},
			],
		},
	]);

	const PatternData = useMemo(() => {
		return pattern?.map((pattern, patternIndex) => {
			const groupItems = pattern.group?.map((group, groupIndex) => {
				const topic = group.topic?.map((topic, topicIndex) => {
					const question = topic.question?.map((question, questionIndex) => {
						return {
							nodeIndex: 1,
							name: question.QuestionName,
							key: `${question.id}${groupIndex}${topicIndex}${questionIndex}`,
							id: `${question.id}${groupIndex}${topicIndex}${questionIndex}`,
							groupId: groupIndex,
							topicId: topicIndex,
							questionId: questionIndex,
							actions: [
								{
									color: "#333",
									icon: iconsFa6.delete,
									color: "#FBC658",

									onClick: (id) => {},
								},
								{
									color: "green",
									icon: "fa fa-pencil-square-o",
									color: "#0bf",

									onClick: (id) => {},
								},
							],
						};
					});
					return {
						nodeIndex: 0,
						groupId: groupIndex,
						topicId: topicIndex,
						name: topic.TopicName,
						key: `${topic.id}${groupIndex}${topicIndex}`,
						id: `${topic.id}${groupIndex}${topicIndex}`,
						items: question,
						actions: [
							{
								color: "#333",
								icon: iconsFa6.delete,
								color: "#FBC658",

								onClick: (id) => {},
							},
							{
								color: "green",
								icon: "fa fa-pencil-square-o",
								color: "#0bf",

								onClick: (id) => {},
							},
						],
					};
				});
				return {
					nodeIndex: -1,
					groupId: groupIndex,
					name: group.GroupName,
					key: `${group.id}${groupIndex}`,
					id: `${group.id}${groupIndex}`,
					items: topic,
					actions: [
						{
							color: "#333",
							icon: iconsFa6.delete,
							color: "#FBC658",

							onClick: (id) => {},
						},
						{
							color: "green",
							icon: "fa fa-pencil-square-o",
							color: "#0bf",

							onClick: (id) => {},
						},
					],
				};
			});

			return {
				nodeIndex: -1,

				name: pattern.patternName,
				key: `${pattern.patternName}${patternIndex}`,
				id: `${pattern.id}${patternIndex}`,
				patternId: patternIndex,
				itemsGroup: groupItems,
			};
		});
	}, []);

	useEffect(() => {
		setDataToWindow();
	}, []);

	const setDataToWindow = () => {
		let items = {};
		items.actions = [
			{
				id: -1,
				name: tr`Add group`,
				icon: "fa fa-plus",
				color: "#fd822b",

				onClick: () => {
					alert("Add Group");
				},
			},
			{
				id: 0,
				name: tr`Add Topic`,
				icon: "fa fa-eye",
				color: "#fd822b",
				component: <TopicEditor />,

				onClick: () => {
					alert("ADD Topic");
				},
			},
			{
				id: 1,
				name: tr`Add Question`,
				icon: "fa fa-eye",
				color: "#fd822b",

				onClick: () => {
					alert("ADD Question");
				},
			},
		];

		setWindowData(items);
	};

	return (
		<div className="content">
			<RWindowScroller data={PatternData} windowData={windowData} />
		</div>
	);
};

export default GQuestionSet;
