import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { searchByPost } from "store/actions/global/socialMedia.actions";
import { useParams } from "react-router-dom";
import InfiniteScroll from "react-infinite-scroll-component";
import RDiscussions from "components/Global/RComs/RDiscussion/RDiscussions";
import Loader from "utils/Loader";

const GDiscussionsSearch = () => {
  const { query } = useParams();
  const dispatch = useDispatch();

  const { posts, loadMorePage, postsLoading } = useSelector(
    (state) => state.discussionsRed
  );

  useEffect(() => {
    dispatch(searchByPost(query, 1));
  }, []);

  const fetchData = () => {
    loadMorePage !== null && dispatch(searchByPost(query, loadMorePage));
  };

  return (
    <div className="content">
      {/* <InfiniteScroll
        dataLength={posts.length}
        next={fetchData}
        hasMore={loadMorePage !== null ? true : false}
        style={{ overflow: "hidden" }}
        loader={<Loader />}
        // endMessage={<h4>You Show All Posts</h4>}
      >
        <RDiscussions
          discussions={posts}
          postsLoading={postsLoading}
          addVisable={false}
        />
      </InfiniteScroll> */}
    </div>
  );
};

export default GDiscussionsSearch;
