import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getPost } from "store/actions/global/socialMedia.actions";
import { trackLastSeenAsync } from "store/actions/global/track.action";
import InfiniteScroll from "react-infinite-scroll-component";
import Loader from "utils/Loader";
import RDiscussions from "../../RComs/RDiscussion/RDiscussions";
import tr from "../../RComs/RTranslator";

const GDiscussions = () => {
  const dispatch = useDispatch();
  const { user } = useSelector((state) => state?.auth);
  const { posts, loadMorePage, postsLoading } = useSelector(
    (state) => state.discussionsRed
  );

  useEffect(() => {
    dispatch(trackLastSeenAsync({ payload: { trackable_type: "posts" } }));
  }, [posts]);

  useEffect(() => {
    dispatch(getPost(1));
  }, []);

  const fetchData = () => {
    loadMorePage !== null && dispatch(getPost(loadMorePage));
  };

  return (
    <div className="content">
      <div>
        <InfiniteScroll
          dataLength={posts?.length}
          next={fetchData}
          hasMore={loadMorePage !== null ? true : false}
          loader={<Loader />}
          style={{ overflow: "hidden" }}
          // endMessage={
          //   <h4 style={{ textAlign: "center" }}>{tr`no_data_to_show`}</h4>
          // }
        >
          <RDiscussions
            discussions={posts}
            postsLoading={postsLoading}
            addVisable={false}
          />
        </InfiniteScroll>
      </div>
    </div>
  );
};

export default GDiscussions;
