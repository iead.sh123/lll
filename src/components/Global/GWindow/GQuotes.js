import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getQuoteAsync } from "store/actions/global/quote.action";
import Loader, { ELLIPSIS } from "utils/Loader";
import RQuote from "../RComs/RText/RQuote";

const GQuotes = ({basicStyle=false}) => {
  const dispatch = useDispatch();
  const { quotes, quotes_Loading } = useSelector(
    (state) => state.quoteServiceRed
  );

  useEffect(() => {
    dispatch(getQuoteAsync());
  }, []);

  return (
    <>
      {quotes_Loading
        ? Loader(ELLIPSIS)
        : quotes !== null && (
            <RQuote
              color={quotes.color}
              image={quotes.image}
              text={quotes.text}
              author={quotes.author}
              basicStyle={basicStyle}
              minHeight={"90%"}
            />
          )}
    </>
  );
};

export default GQuotes;
