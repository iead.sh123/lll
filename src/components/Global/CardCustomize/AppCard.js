import React, { useState } from "react";

import { CardBody, Col, Row } from "reactstrap";
import styles from "./AppCard.Module.scss";

function AppCard({ title, details, actions }) {
	const [style, setStyle] = useState({ display: "none" });
	return (
		<div className="content pb-2 ">
			<Row>
				{details.map((detail, index) => {
					return (
						<Col
							className="mb-3"
							key={index}
							md="4"
							onMouseEnter={() => {
								setStyle(index);
							}}
							onMouseLeave={() => {
								setStyle(null);
							}}
						>
							<CardBody className={"card bgTree p-0 " + styles.b_card_topic}>
								{style != index ? (
									<Row>
										<Col md="2" className={styles.topic_card} hidden={1 + 1 == 2 ? true : false}>
											<p className={"text-white mt-2 ml-1 " + styles.text_direction}>{title}</p>
										</Col>

										<Col md={1 == 1 ? "12" : "10"} className="pt-2 pl-4 pb-1 ">
											{" "}
											<h6 className={styles.fixedCircle}>
												<i className={"nc-icon nc-check-2 mt-1 ml-1 " + styles.check_circle}></i>
											</h6>
											<table className={"col-md-12 " + styles.line_space}>
												{details.map((it, i) => {
													return it[i] ? (
														<tbody key={i}>
															<tr>
																<td>{it[i]?.key}:</td>
																<td className="float-right pr-3">&nbsp; {it[i]?.value}</td>
															</tr>
														</tbody>
													) : (
														<></>
													);
												})}
											</table>
										</Col>
									</Row>
								) : (
									<>
										<Col md="12" className={styles.topic_card_hover}>
											<p className="text-white mt-3 font-weight-bold mb-3  text-center">{title}</p>
										</Col>
										<Col md="12" className="card-body mb-0 mt-0 ">
											<Row>
												{actions?.map((action) => {
													<Col md={12 / actions.length} sm={12 / actions.length}>
														<p onClick={action.onClick} style={{ cursor: "pointer" }} className="  m-0 p-0">
															<i className="nc-icon nc-simple-add"></i> {action.actionname}
														</p>
													</Col>;
												})}
											</Row>
										</Col>
									</>
								)}
							</CardBody>
						</Col>
					);
				})}
			</Row>
		</div>
	);
}

export default AppCard;
