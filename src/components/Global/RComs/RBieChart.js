import React from 'react';
import { useMemo } from 'react';
import { Bar } from 'react-chartjs-2';
import Helper from './Helper';

const RBieChart = ({ data,color,label }) => {
  const labels = data.map((item) => item.text);
  const counts = data.map((item) => item.count);

  const getRandomColors = (length) => {
    const colors = ['#FF6384', '#36A2EB', '#FFCE56', '#4BC0C0', '#9966FF', '#FF9F40', '#FF6384', '#36A2EB', '#FFCE56', '#4BC0C0'];
    const selectedColors = [];

    for (let i = 0; i < length; i++) {
      const randomIndex = Math.floor(Math.random() * colors.length);
      const color = colors[randomIndex];
      
      selectedColors.push(color);
      colors.splice(randomIndex, 1);
    }

    return selectedColors;
  };

  const backgroundColors = color??getRandomColors(data.length);

  const chartData ={
    labels: labels,
    datasets: [
      {
        label: label??'Counts',
        data: counts,
        backgroundColor: backgroundColors,
      },
    ],
  } ;
  const options = {
    animation: {
      duration: 0, // Set the duration to 0 to stop animation
    },
  };
  return (
    <div>
      {Helper.cl(data)}
      {Helper.cl(color)}
      {Helper.cl(label)}
      {/* <h3>Chart</h3> */}
      <Bar data={chartData}  options={options} />
    </div>
  );
};

export default RBieChart;