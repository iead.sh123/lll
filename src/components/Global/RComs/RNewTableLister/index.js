import React from "react";
import RDropdownIcon from "../RDropdownIcon/RDropdownIcon";
import withDirection from "hocs/withDirection";
import RTableImage from "../RTableImage";
import RFormItem from "../RFormItem";
import iconsFa6 from "variables/iconsFa6";
import RButton from "../RButton";
import RColor from "../RColor";
import styles from "./RNewTableLister.module.scss";
import RFlex from "../RFlex/RFlex";
import tr from "../RTranslator";
import { Col, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from "reactstrap";
import { RLabel } from "../RLabel";
import { Table } from "reactstrap";

const RNewTableLister = ({
	Records,
	recordsArr,
	hideTableHeader = false,
	firstCellImageProperty,
	align,
	noBorders = false,
	center = false,
	emptyTable = false,
	fixedWidth,
	fixedHeight,
	tableStyle,
	activeScroll = false,
	maxHeight = "50vh",
	minHeight,
	disableXScroll = false,
}) => {
	let style = { ...tableStyle };
	style = activeScroll ? { ...style, maxHeight: maxHeight } : style;

	const keyGenerator = (function* () {
		let i = 1;
		while (true) yield i++;
	})();
	return (
		<Col
			id="First Div in Table"
			xs={12}
			className={`p-0 ${!disableXScroll ? styles.newTable : ""} ${activeScroll ? styles.scrollableTable : ""}  `}
			style={{ ...style }}
		>
			<Table
				id="Second Div In Table"
				hover={!noBorders}
				style={noBorders ? {} : { border: "1px solid #D9D9D9", padding: "0px", minHeight: minHeight ? minHeight : "" }}
			>
				{/* - - - - - - - Header - - - - - - - */}
				{!hideTableHeader && (
					<thead>
						<tr style={{ backgroundColor: "rgb(238, 242, 251)" }}>
							{/* - - - - - - - Header Details - - - - - - - */}
							{Records?.[0]?.details?.map((detailName, index) => {
								return (
									<th
										key={"rhd" + index}
										style={{
											border: noBorders ? "" : "1.5px solid #eee",
											...detailName.keyStyle,
										}}
										hidden={detailName.hidden ? detailName.hidden : false}
										// className="fixed_th"
									>
										<RFlex
											styleProps={fixedWidth ? { width: fixedWidth } : {}}
											className={`align-items-center ${center && index !== 0 ? "justify-content-center" : ""}`}
										>
											{detailName.keyType == "component" ? detailName.key : tr(detailName.key)}
											{detailName.dropdown && (
												<RDropdownIcon
													component={
														<RFlex className={"dropdown-arrows"}>
															<i className={iconsFa6.chevronUp} aria-hidden="true" style={{ fontSize: "8px" }} />
															<i className={iconsFa6.chevronDown} aria-hidden="true" style={{ fontSize: "8px" }} />
														</RFlex>
													}
													actions={detailName.dropdownData.map((data) => ({
														label: data.title,
														action: data.onClick,
														name: data.name,
														value: data.value,
													}))}
													menuClassname={styles.tableDropdown}
													itemStyle={detailName?.itemStyle ? detailName?.itemStyle : ""}
												/>
											)}
										</RFlex>
									</th>
								);
							})}

							{/* - - - - - - - Header Actions - - - - - - - */}
							{Records?.[0]?.actions?.length > 0 && (
								<th
									// className="fixed_th"
									style={{ border: "1.5px solid #eee", textAlign: "center", fontSize: Records?.[0]?.actions[0].fontSize ? "18px" : "" }}
								>
									{tr("")}
								</th>
							)}
						</tr>
					</thead>
				)}

				{/* - - - - - - - Body - - - - - - - */}
				{!emptyTable && (
					<tbody>
						{Records?.map((record, index) => {
							return (
								<tr key={"rec" + index} style={noBorders ? { border: "0px solid white" } : {}}>
									{/* - - - - - - - Body Details - - - - - - - */}
									{record?.details?.map((detail, ind) => {
										return (
											<td
												hidden={detail.hidden ? detail.hidden : false}
												key={"rd" + ind}
												style={{
													minWidth: detail.minWidth ? detail.minWidth : "100px",
													color: detail.color ?? "black",
													textAlign: detail.alignment ? detail.alignment : "left",
													fontSize: detail.fontSize ? detail.fontSize : "",
													borderTop: noBorders ? "0px solid white" : "1.5px solid #eee",
												}}
											>
												<RFlex
													onClick={detail.onClickRow}
													styleProps={{ margin: detail.margin ? detail.margin : "", cursor: detail.cursor ? detail.cursor : "" }}
													className={`${center && ind !== 0 ? "align-items-center justify-content-center" : "align-items-center"}`}
												>
													{[
														"image",
														"main course image",
														"main course icon",
														"main course icon double",
														"main course svg",
														"logo",
													].includes(detail?.type) ? (
														<RTableImage detail={detail} />
													) : detail?.html || firstCellImageProperty ? (
														detail?.value
													) : detail?.type == "component" ? (
														detail?.value
													) : detail?.type == "color" ? (
														<RColor value={detail} disabled={true} />
													) : detail?.mode == "form" ? (
														<RFormItem data={detail} />
													) : detail.render ? (
														detail.render(detail.value)
													) : (
														<RLabel value={tr(detail?.value)} lettersToShow={50} dangerous={false} />
													)}
												</RFlex>
											</td>
										);
									})}

									{/* - - - - - - - Body Actions - - - - - - - */}
									{record?.actions && (
										<td
											className="text-center"
											style={{
												borderTop: noBorders ? "0px solid white" : "1.5px solid #eee",
											}}
										>
											<RFlex className={"justify-center items-center"}>
												{/* - - - - - - - Body OutSide DropDown - - - - - - - */}
												{record?.actions
													?.filter((action) => !action.inDropdown)
													.map((action, index) => {
														let key = keyGenerator.next().value;
														if (action.justIcon) {
															return (
																<div key={index}>
																	{action.loading ? (
																		<i className={iconsFa6.spinner + " m-0" + action.actionIconClass} />
																	) : action.hidden ? (
																		""
																	) : (
																		<i
																			className={`${action.icon} ${action.actionIconClass} `}
																			style={{ width: "15px", height: "15px", cursor: "pointer", ...action.actionIconStyle }}
																			onClick={action.onClick}
																		/>
																	)}
																</div>
															);
														} else if (action.actionType === "component") {
															return action.value;
														}
														return (
															<RButton
																hidden={action.permissions == false ? true : false}
																key={index}
																// key={action.id ?? "ac" + record.id + index}
																className="btn-icon ml-1 mr-1"
																color={action.color ? action.color : "info"}
																size="sm"
																type="button"
																onClick={(event) => {
																	action.onClick(recordsArr);
																}}
																disabled={action.disabled ? action.disabled : false}
																outline={action.outline ? action.outline : false}
																faicon={action.icon}
																text={action.text}
																loading={action.loading}
															/>
														);
													})}

												{/* - - - - - - - Body InSide DropDown - - - - - - - */}
												{record?.actions.some((action) => action.inDropdown) && !record?.removeDropDownActions && (
													<UncontrolledDropdown className="me-2" direction="down">
														<DropdownToggle
															aria-haspopup={true}
															// caret
															color="default"
															data-toggle="dropdown"
															nav
															className="p-0"
														>
															<i
																onClick={record?.rowSelected}
																className={iconsFa6.horizontalOptions}
																aria-hidden="true"
																style={{ cursor: "pointer", color: "#333" }}
															></i>
														</DropdownToggle>
														<DropdownMenu
															persist
															aria-labelledby="navbarDropdownMenuLink"
															right
															className={styles.tableActionsDropdown}
															// container="body"
														>
															{record?.actions
																?.filter((action) => action.inDropdown)
																.map((action, index) => (
																	<DropdownItem
																		// key={action.id ?? "ac" + record.id + index}
																		key={index}
																		disabled={action.disabled ? action.disabled : false}
																		onClick={() => {
																			action.onClick(recordsArr);
																		}}
																		hidden={action.hidden ? action.hidden : false}
																		className={styles.table__actions}
																	>
																		<RFlex
																			styleProps={{
																				alignItems: "center",
																				justifyContent: "start",
																				color: action.color && action.color,
																			}}
																		>
																			{action.loading ? (
																				<RFlex>
																					<i className="fa-solid fa-spinner fa-spin pt-1"></i>
																				</RFlex>
																			) : (
																				<>
																					<i className={action?.icon} aria-hidden="true" />
																					<span>{action.name}</span>
																				</>
																			)}
																		</RFlex>
																	</DropdownItem>
																))}
														</DropdownMenu>
													</UncontrolledDropdown>
												)}
											</RFlex>
										</td>
									)}
								</tr>
							);
						})}
					</tbody>
				)}
			</Table>
		</Col>
	);
};
export default withDirection(RNewTableLister);
