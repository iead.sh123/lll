import React, { useEffect } from "react";
import { useHistory, useParams, Link, useLocation } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Row, Col, Card, CardHeader, CardBody, Button } from "reactstrap";
import Loader from "utils/Loader";
import styles from "./RAnnouncement.Module.scss";
import withDirection from "hocs/withDirection";
import { PostVolunteerInAnnouncementsAsync, SingleAnnouncementAsync } from "store/actions/admin/schoolEvents";
import tr from "../RTranslator";
import AnnouncementSetting from "./AnnouncementSetting";
import { trackRecordsAsync } from "store/actions/global/track.action";
import ReadMoreContent from "./ReadMoreContent";
import ImageThumbnail from "components/SocialMedia/Content/ImageThumbnail";
import PeopleSeenAnnouncement from "./PeopleSeenAnnouncement";
import moment from "moment";

const SingleAnnouncement = ({ dir }) => {
	const dispatch = useDispatch();
	const history = useHistory();

	const { announcibleID, schoolEventID, iDHomeRoom, announcementID } = useParams();

	const { announcement, editAnnouncementLoading, flagAnnouncement } = useSelector((state) => state.SchoolEventsRed);

	const { user } = useSelector((state) => state?.auth);

	useEffect(() => {
		dispatch(SingleAnnouncementAsync(announcementID));

		dispatch(
			trackRecordsAsync({
				payload: {
					trackable_type: "announcements",
					trackable_id: [+announcementID],
					// flag: flagAnnouncement,
				},
			})
		);
	}, [announcementID]);

	const flag = true;

	return (
		<div
			className="content"
			style={{
				paddingTop: (user?.type == "parent" || user?.type == "student") && !user?.useNewTheme && "25vh",
			}}
		>
			{editAnnouncementLoading ? (
				<Loader />
			) : (
				<Row>
					<Col xs={10} style={{ margin: "auto" }}>
						<Link
							to="#"
							style={{
								display: "flex",
								padding: "10px",
								cursor: "pointer",
								width: "20px",
								color: "#333",
							}}
							all-announcement
							onClick={() =>
								user?.type == "parent"
									? history.push(`${process.env.REACT_APP_BASE_URL}/parent/announcement`)
									: user?.type == "student"
									? history.push(`${process.env.REACT_APP_BASE_URL}/student/announcement`)
									: user?.type == "teacher"
									? history.goBack()
									: user?.type == "employee" && flagAnnouncement == true
									? history.goBack()
									: user?.type == "employee" && flagAnnouncement == false
									? history.push(`${process.env.REACT_APP_BASE_URL}/admin/all-announcement`)
									: user?.type === "school_advisor" || user?.type === "seniorteacher" || user?.type === "principal"
									? history.push(`${process.env.REACT_APP_BASE_URL}/senior-teacher/announcements`)
									: ""
							}
						>
							<i className={dir ? "fa fa-arrow-left" : "fa fa-arrow-right"}></i>
							<h6 style={{ padding: "0 5px" }}>{tr`back`}</h6>
						</Link>
					</Col>
					<Col xs={10} style={{ margin: "auto" }}>
						<Card className="gedf-card">
							<CardHeader
								className={styles.card_header}
								style={{
									background: !announcement.seen ? "#efdbc1" : "rgba(0, 0, 0, 0.03)",
								}}
							>
								<Row
									style={{
										display: "flex",
										justifyContent: "space-between",
									}}
								>
									<Col md={6} className={styles.padding_card_header}>
										<div style={{ display: "flex" }}>
											<Link to="#" className="mr-2">
												<img className="rounded-circle" width="45" src={""} alt="" />
											</Link>
											<Link to="#">
												<div className="title-Details m-0 text-dark">
													<span>{announcementID && announcement.title}</span>
												</div>
											</Link>
										</div>

										{announcementID && announcement.published_at && (
											<div className={dir ? `${styles.icon_date_ltr}` : `${styles.icon_date_rtl}`}>
												<i className="fa fa-clock-o" style={{ padding: 3 }}></i>{" "}
												<Link to="#">
													<span className="text-muted h7 mb-2">
														{moment(new Date(announcement.published_at).toString()).format("MM-DD-YYYY hh:mm a")}
													</span>
												</Link>
											</div>
										)}
									</Col>

									<Col md={4} style={{ display: "flex", justifyContent: "end" }}>
										{user?.type == "parent" && (
											<div>
												{announcement.accept_volunteers !== 0 && announcement.can_join == 1 && (
													<Button
														outline
														color="info"
														onClick={() => dispatch(PostVolunteerInAnnouncementsAsync(announcement.announcible_id, announcement.id))}
													>{tr`volunteer`}</Button>
												)}

												{announcement.accept_volunteers !== 0 && announcement.can_join == 0 && (
													<Button color="success" disabled>{tr`volunteered`}</Button>
												)}
											</div>
										)}
										{user?.type !== "parent" && announcement.isPublished == 0 && <AnnouncementSetting announcement={announcement} />}
									</Col>
								</Row>
							</CardHeader>
							<CardBody className={dir ? styles.card_body_ltr : styles.card_body_rtl}>
								<>
									<Row>
										<Col xs={12} className={dir ? "" : styles.card_body_image_rtl}>
											<ReadMoreContent textContent={announcementID && announcement?.content?.text} />
										</Col>
										<Col xs={12} className={dir ? "" : styles.card_body_image_rtl}>
											{announcement &&
											announcement.content &&
											announcement.content.resource &&
											announcement.content.resource.length !== 0 ? (
												<ImageThumbnail images={announcementID && announcement.content.resource} />
											) : (
												<></>
											)}
										</Col>
									</Row>
									{(schoolEventID || iDHomeRoom || flagAnnouncement == true) && (
										<Row>
											<Col xs={12}>
												<PeopleSeenAnnouncement announcement={announcement && announcement} />
											</Col>
										</Row>
									)}
								</>
							</CardBody>
						</Card>
					</Col>
				</Row>
			)}
		</div>
	);
};

export default withDirection(SingleAnnouncement);
