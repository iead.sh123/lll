import React, { useState } from "react";
import styles from "./RAnnouncement.Module.scss";
import tr from "../RTranslator";

const ReadMoreContent = ({ textContent }) => {
	const textShow = textContent?.slice(0, 150);
	const [isReadMore, setIsReadMore] = useState(true);

	const toggleReadMore = () => {
		setIsReadMore(!isReadMore);
	};

	return (
		<>
			{textContent && (
				<>
					<span
						className={styles.title_text}
						dangerouslySetInnerHTML={{
							__html: isReadMore ? textContent.slice(0, 150) : textContent,
						}}
					/>
					<span className={styles.readOrHide} onClick={toggleReadMore}>
						{isReadMore && textContent != textShow ? tr` ...See More` : textContent?.length > 150 ? tr` See less` : ""}
					</span>
				</>
			)}
		</>
	);
};

export default ReadMoreContent;
