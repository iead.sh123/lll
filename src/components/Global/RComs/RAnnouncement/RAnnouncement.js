import React, { useState } from "react";
import ReadMoreContent from "./ReadMoreContent";
import styles from "./RAnnouncement.Module.scss";
import tr from "../RTranslator";
import { Row, Col, Card, CardHeader, CardBody, Button, UncontrolledTooltip } from "reactstrap";
import { PostVolunteerInAnnouncementsAsync, replaceSeenAnnouncement } from "store/actions/admin/schoolEvents";
import { useDispatch, useSelector } from "react-redux";
import { trackRecordsAsync } from "store/actions/global/track.action";
import { Link, useHistory } from "react-router-dom";
import ImageThumbnail from "components/SocialMedia/Content/ImageThumbnail";
import RButton from "../RButton";
import AnnouncementSetting from "./AnnouncementSetting";
import withDirection from "hocs/withDirection";
import RMiddle from "../RMiddle";
import PeopleSeenAnnouncement from "./PeopleSeenAnnouncement";
import moment from "moment";
import REmptyData from "components/RComponents/REmptyData";

const RAnnouncement = ({ Announcements, hasAddPermission, schoolEventID, iDHomeRoom, dir, flag }) => {
	const dispatch = useDispatch();
	const history = useHistory();
	const { user } = useSelector((state) => state?.auth);

	const handleSeenAnnouncement = (announcementId) => {
		dispatch(
			trackRecordsAsync({
				payload: {
					trackable_type: "announcements",
					trackable_id: [+announcementId],
				},
			})
		);
	};
	return (
		<>
			{user?.type !== "parent" && hasAddPermission && (
				<Row>
					<Col xs={12}>
						<RButton
							text={tr`add_announcement`}
							onClick={() =>
								user?.type == "teacher"
									? history.push(`${process.env.REACT_APP_BASE_URL}/teacher/home-room/${iDHomeRoom}/announcement/add`)
									: user?.type == "employee" && !schoolEventID
									? history.push(`${process.env.REACT_APP_BASE_URL}/admin/announcement/add`)
									: history.push(`${process.env.REACT_APP_BASE_URL}/admin/school-event/${schoolEventID}/announcement/add`)
							}
							color="info"
							outline
						/>
					</Col>
				</Row>
			)}
			{Announcements && Announcements.length !== 0 && (
				<Row>
					{Announcements?.map((announcement) => (
						<Col xs={10} style={{ margin: "auto" }}>
							<Card className="gedf-card">
								<CardHeader
									className={styles.card_header}
									style={{
										background: !announcement?.seen ? "#efdbc1" : "rgba(0, 0, 0, 0.03)",
									}}
								>
									<Row
										style={{
											display: "flex",
											justifyContent: "space-between",
										}}
									>
										<Col md={6} className={styles.padding_card_header}>
											<div style={{ display: "flex" }}>
												<Link to="#" className="mr-2">
													<img className="rounded-circle" width="45" src={""} alt="" />
												</Link>
												<Link to="#">
													<div className="title-Details m-0 text-dark">
														<span
															onClick={() =>
																user?.type == "parent"
																	? history.push(`${process.env.REACT_APP_BASE_URL}/parent/single-announcement/${announcement.id}`)
																	: user?.type == "student"
																	? history.push(`${process.env.REACT_APP_BASE_URL}/student/single-announcement/${announcement.id}`)
																	: user?.type == "teacher"
																	? history.push(`${process.env.REACT_APP_BASE_URL}/teacher/single-announcement/${announcement.id}`)
																	: user?.type == "employee"
																	? history.push(`${process.env.REACT_APP_BASE_URL}/admin/single-announcement/${announcement.id}`)
																	: user?.type === "school_advisor" || user?.type === "seniorteacher" || user?.type === "principal"
																	? history.push(`${process.env.REACT_APP_BASE_URL}/senior-teacher/single-announcement/${announcement.id}`)
																	: ""
															}
														>
															{announcement.title}
														</span>
													</div>
												</Link>
											</div>

											{announcement.published_at && (
												<div className={dir ? `${styles.icon_date_ltr}` : `${styles.icon_date_rtl}`}>
													<i className="fa fa-clock-o" style={{ padding: 3 }}></i>{" "}
													<Link to="#">
														<span className="text-muted h7 mb-2">
															{moment(new Date(announcement.published_at).toString()).format("MM-DD-YYYY hh:mm a")}
														</span>
													</Link>
												</div>
											)}
										</Col>

										<Col md={4} style={{ display: "flex", justifyContent: "end" }}>
											{user?.type == "parent" && (
												<div>
													{announcement.accept_volunteers !== 0 && announcement.can_join == 1 && (
														<Button
															outline
															color="info"
															onClick={() => dispatch(PostVolunteerInAnnouncementsAsync(announcement.announcible_id, announcement.id))}
														>{tr`volunteer`}</Button>
													)}

													{announcement.accept_volunteers !== 0 && announcement.can_join == 0 && (
														<Button color="success" disabled>{tr`volunteered`}</Button>
													)}
												</div>
											)}
											{user?.type !== "parent" && announcement.isPublished == 0 && <AnnouncementSetting announcement={announcement} />}
										</Col>

										{!announcement.seen && (
											<Col md={1}>
												<div
													className="empty-div"
													style={{
														position: "relative",
														top: "7px",
														cursor: "pointer",
													}}
													id={"t" + `${announcement.id}`}
													onClick={() => handleSeenAnnouncement(announcement.id)}
												>
													<UncontrolledTooltip delay={0} target={"t" + `${announcement.id}`}>
														{tr`Mark As Seen`}
													</UncontrolledTooltip>
												</div>
											</Col>
										)}
									</Row>
								</CardHeader>
								<CardBody className={dir ? styles.card_body_ltr : styles.card_body_rtl}>
									<>
										<Row>
											<Col xs={12} className={dir ? "" : styles.card_body_image_rtl}>
												<ReadMoreContent textContent={announcement?.content?.text} />
											</Col>
											<Col xs={12} className={dir ? "" : styles.card_body_image_rtl}>
												{announcement.content.resource.length !== 0 ? <ImageThumbnail images={announcement.content.resource} /> : <></>}
												<div className="row">
													{announcement.content &&
														announcement.content.resource &&
														announcement.content.resource.map((item, i) => {
															return (
																<div key={i}>
																	{["png", "jpeg", "mp4", "jpg"].includes(item.mime_type) ? null : (
																		<div className={"col-sm-2 " + styles.show_file}>
																			<a
																				href={process.env.REACT_APP_RESOURCE_URL + item.url}
																				alt="Download"
																				target="_blank"
																				className="btn btn-link btn-icon  "
																				rel="noreferrer"
																				id={"tooltip" + i}
																			>
																				<i className={"nc-icon nc-cloud-download-93 " + styles.file_size}></i>
																				<UncontrolledTooltip delay={0} target={"tooltip" + i}>
																					{item.file_name}
																				</UncontrolledTooltip>
																			</a>
																		</div>
																	)}
																</div>
															);
														})}
												</div>
											</Col>
										</Row>
										{flag == true && (
											<Row>
												<Col xs={12}>
													<PeopleSeenAnnouncement announcement={announcement} />
												</Col>
											</Row>
										)}
									</>
								</CardBody>
							</Card>
						</Col>
					))}
				</Row>
			)}
			{Announcements && Announcements.length === 0 && (
				<RMiddle>
					<REmptyData line1="No Announcements yet" line2="You'll find all new announcements in here" dir="column" />
				</RMiddle>
			)}
		</>
	);
};

export default withDirection(RAnnouncement);
