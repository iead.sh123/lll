import { Row, Col, Card, CardImg, CardBody, UncontrolledTooltip, Button } from "reactstrap";
import { useHistory } from "react-router-dom";
import { useState } from "react";
import { RLabel } from "./RLabel";
import styles from "./AppCard.Module.scss";
import tr from "components/Global/RComs/RTranslator";
import "./RCard.css";

const RCard = ({ cardInput, cardWidth, link, classButtonSession, linkTitle, user, flag, singleAbuseLink, notifications, banner }) => {
	const history = useHistory();
	const lang = localStorage.getItem("language") || "english";
	const float = lang == "arabic" ? "right" : "left";
	const [openedButton, setOpenedButton] = useState(false);

	const buttonToggle = (id) => {
		const isOpen = "collapse" + id;
		if (isOpen == openedButton) {
			setOpenedButton("collapse");
		} else {
			setOpenedButton("collapse" + id);
		}
	};

	return (
		<div
			md="12"
			style={{
				position: "relative",
				marginBottom: "20px",
				marginTop: "0px",
				marginLeft: "0px",
				marginRight: "20px",
				paddingBottom: "0px",
				paddingTop: "0px",
				paddingLeft: "0px",
				paddingRight: "0px",
				textAlign: float,
				float: float,
			}}
			sm="12"
			lg={user == "teacher" && flag ? 12 : 6}
			xlg="8"
			className={link ? "cursor-pointer div-up  " : "div-up "}
			onClick={() => {
				if (link) {
					const LINK = `${link}`;
					history.push(LINK);
				}
			}}
			onClick1={() => {
				link
					? user == "teacher" && flag
						? history.push(`${linkTitle.link}/${cardInput.idHomeRoom}/Attendance`)
						: user == "teacher" && !singleAbuseLink
						? history.push(`${linkTitle.link}/${cardInput.courseId}/rabs/${cardInput.id}/Lessons`)
						: user == "teacher" && singleAbuseLink
						? history.push(`${cardInput.link}`)
						: user == "employee" && !singleAbuseLink
						? history.push(`${linkTitle.link}/${cardInput.id}/Resources`)
						: user == "employee" && singleAbuseLink
						? history.push(`${cardInput.link}`)
						: (user == "school_advisor" && cardInput.link) ||
						  (user == "principal" && cardInput.link) ||
						  (user == "seniorteacher" && cardInput.link)
						? history.push(`${cardInput.link}`)
						: ""
					: null;
			}}
		>
			{notifications ? (
				<div
					id="redCircle"
					style={{
						position: "absolute",
						width: "25px",
						height: "25px",
						background: "red",
						borderRadius: "50%",
						zIndex: "19999999",

						right: "-13px",
						top: "-13px",
						textAlign: "center",
						fontWeight: "800",
						fontSize: "17px",
						color: "white",
					}}
				>
					{notifications}
				</div>
			) : (
				<></>
			)}
			<Card
				style={{
					width: cardWidth,
					backgroundColor: "#fff",
				}}
			>
				{cardInput.images
					? cardInput?.images.map((img, index) => {
							if (index == 0) {
								return (
									<CardImg
										top
										// src={`${process.env.REACT_APP_RESOURCE_URL}${img.src}`}
										src={img.src}
										width={"100%"}
										height={"150px"}
									></CardImg>
								);
							}
					  })
					: ""}

				<CardBody style={{ padding: "15px 1px 10px 1px " }}>
					<Row className="m-2">
						<Col lg="4" md="4" sm="4" className="the-card-icon no-padding-no-margin ">
							{cardInput.icons
								? cardInput.icons.map((icon, index) => {
										if (index == 0) {
											return (
												<div className="icon-img">
													<CardImg
														/* style={{border:"1px solid blue"}} */
														src={icon.src}
													></CardImg>
												</div>
											);
										}
								  })
								: ""}
						</Col>

						<Col lg="7" md="7" sm="7" className=" the-card-title no-padding-no-margin ml-2 mt-2 ">
							<Row>
								<Col lg="12" md="12" sm="12" className="no-padding-no-margin ">
									{cardInput.Titles
										? cardInput.Titles.map((title, index) => {
												if (index == 0) {
													return (
														<div className="card-name">
															{/* {title.toString().length <= 15 ? (
                                <span>{tr(title)}</span>
                              ) : (
                                <>
                                  <span id={title.substring(0, 2)}>
                                    {tr(title).substring(0, 10) + "...."}
                                  </span>
                                  {/* <UncontrolledTooltip
                                    delay={0}
                                    target={title.substring(0, 2)}
                                  >
                                    {tr(title)}
                                  </UncontrolledTooltip> 
                             
                               }
                             */}
															<RLabel value={tr(title)} lettersToShow={14}></RLabel>
														</div>
													);
												}
										  })
										: ""}
								</Col>
							</Row>

							<Row>
								<Col lg="12" md="12" sm="12" className="no-padding-no-margin "></Col>
							</Row>
						</Col>
					</Row>

					{cardInput.details
						? cardInput.details.map((d, i) => (
								<Row style={{ marginTop: i == 0 ? "1.3rem" : null }}>
									{/* <Col xs="1" className="no-padding-no-margin icon-col">
                    {/* <CardImg  src={cardInput.details[0].icon}   height={'60px'} style={{border:"1px solid yellow"}}></CardImg> */}
									{/* </Col> */}
									<Col xs="12" className="no-padding-no-margin ">
										<div className="Details-Key" onClick={() => d.link && history.push(d.link)} style={{ cursor: d.link && "pointer" }}>
											<Row>
												<Col xs={1}>
													<span className="icon-Details pr-1">
														<i className={d.icon}></i>
													</span>
												</Col>{" "}
												<Col xs={6}>
													<span style={{ whiteSpace: "nowrap" }} className="font-weight-bold">
														{tr(d.key)}
													</span>
												</Col>
												<Col xs={4} style={{ textAlign: "center" }}>
													{/* <span
                                  id={
                                    "data" +
                                    (typeof d?.value === "string" ||
                                    d?.value instanceof String
                                      ? d?.value?.substring(0, 2)
                                      : d?.value) +
                                    cardInput.id
                                  }
                                >
                                  {typeof d.value === "string" ||
                                  d.value instanceof String
                                    ? tr(d.value).substring(0, 4) + "...."
                                    : tr(d.value)}
                                </span>
                                {typeof d.value === "string" ||
                                d.value instanceof String ? (
                                  // <UncontrolledTooltip
                                  //   delay={0}
                                  //   target={
                                  //     "data" +
                                  //     (typeof d?.value === "string" ||
                                  //     d?.value instanceof String
                                  //       ? d?.value.substring(0, 2)
                                  //       : d?.value) +
                                  //     cardInput.id
                                  //   }
                                  // >
                                  //   {d?.value}
                                  // </UncontrolledTooltip>
                                  null */}
													<RLabel
														lettersToShow={6}
														value={d?.value}
														//value={"65 4654 654654 4133123131d fg"}
													></RLabel>
												</Col>
											</Row>
										</div>
									</Col>
								</Row>
						  ))
						: ""}

					<Row className={styles.styleEvent}>
						{cardInput?.actions && cardInput?.actions[0]?.showAction && (
							<Button
								className="btn-icon btn-round"
								color="primary"
								onClick={() => {
									buttonToggle(cardInput?.id);
								}}
							>
								{openedButton === "collapse" + cardInput?.id ? <i className="fa fa-close" /> : <i className="fa fa-ellipsis-v" />}
							</Button>
						)}

						{cardInput.actions &&
							cardInput.actions.map((action, index) => {
								const tooltipId = "Tool1tip" + cardInput?.table_name + index;
								if (action?.type === "play") {
									return (
										<h6 className={classButtonSession + " " + styles.fixedButtonShadow + " " + styles.dot} onClick={action?.onClick}>
											<div className="row">
												<div className="container">
													<div className="row">
														<span className="intro-banner-vdo-play-btn pinkBg cursor-pointer">
															<i className={" nc-icon nc-button-play  " + styles.check_circle_card}></i>

															<span className="ripple pinkBg"></span>
															<span className="ripple pinkBg"></span>
															<span className="ripple pinkBg"></span>
														</span>
													</div>
												</div>
											</div>
										</h6>
									);
								} else if (action?.type == "disable") {
									return (
										<h6>
											{/* className={styles.fixedButton_edit} */}
											<div className="row">
												<div className="container">
													<div className="row">
														<span className="intro-banner-vdo-play-btn grayBg">
															<i className={" nc-icon nc-button-play  " + styles.check_circle_card}></i>
														</span>
													</div>
												</div>
											</div>
										</h6>
									);
								} else {
									return (
										<>
											{openedButton === "collapse" + cardInput?.id && (
												<>
													<Button
														className="classButtonLeft btn-icon btn-round"
														color={action.color}
														onClick={action?.onClick}
														id={tooltipId}
														key={"rat" + index + action?.name}
													>
														<i className={action.icon}></i>
													</Button>

													<UncontrolledTooltip delay={0} key={"rat" + index + action?.name} target={tooltipId}>
														{action?.name}
													</UncontrolledTooltip>
												</>
											)}
										</>
									);
								}
							})}
					</Row>

					{notifications ? (
						<div
							id="banner"
							style={{
								width: "100%",
								height: "25px",
								borderTop: "lightgray 1px solid ",
								right: "0",
								textAlign: "center",
								fontWeight: "400",
								fontSize: "13px",
								color: "black",
							}}
						>
							<marquee width="90%" direction="left" height="100px">
								{banner.map((el) => (
									<span>{el.breif} &nbsp;&nbsp;&nbsp;</span>
								))}
							</marquee>{" "}
						</div>
					) : (
						""
					)}
				</CardBody>
				{cardInput.FeaturedMarks ? (
					<div className="Featured-Marks">
						{cardInput.FeaturedMarks.map((mark) => (
							<div
								className="Featured-Mark"
								style={
									mark.markAdditionalStyle ?? {
										backgroundColor: mark.color ?? "rgba(253, 131, 44, 1)",
										display: mark?.permission ? "none" : "initial",
									}
								}
							>
								{mark.image ? (
									<img
										src={mark.image}
										style={{
											width: "100%",
											/* height: 80%; */
											position: "relative",
											borderRadius: "50%",
											border: "black 1px solid",
										}}
										draggable={false}
									/>
								) : mark.icon ? (
									<i className={mark.icon}></i>
								) : null}

								{
									<>
										<br />
										<span
											style={{
												fontSize: mark?.fontSize,
												color: mark?.fontColor,
											}}
										>
											{mark.text.length < 5 && !notifications ? (
												mark.text
											) : (
												<marquee width="80%" direction="left" height="100px" behavior="alternate" scrolldelay="600">
													{mark.text}
												</marquee>
											)}
										</span>
									</>
								}
							</div>
						))}
					</div>
				) : (
					""
				)}
			</Card>
		</div>
	);
};
export default RCard;
