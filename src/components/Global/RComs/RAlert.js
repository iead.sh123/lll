import ReactBSAlert from "react-bootstrap-sweetalert";
import tr from "./RTranslator";

export const deleteSweetAlert = (
  showAlerts,
  hideAlert,
  successDelete,
  prameters,
  message,
  confirm
) => {
  showAlerts(
    <ReactBSAlert
      warning
      title={message ? message : tr`are_you_sure_to_delete_it`}
      onConfirm={() => successDelete(prameters)}
      onCancel={() => hideAlert()}
      confirmBtnBsStyle="danger"
      // cancelBtnBsStyle="primary"
      confirmBtnText={confirm ? confirm : tr`yes_delete_it`}
      cancelBtnText={tr`cancel`}
      showCancel
      btnSize=""
    />
  );
  return;
};
