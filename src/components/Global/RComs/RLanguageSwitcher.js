import React from "react";
import { DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown } from "reactstrap";
import tr, { languages, swapLanguage } from "./RTranslator";
import RFlex from "./RFlex/RFlex";

const RLanguageSwitcher = () => {
	return (
		<div>
			<UncontrolledDropdown className="btn-magnify" nav>
				<DropdownToggle
					aria-haspopup={true}
					caret
					color="default"
					data-toggle="dropdown"
					id="navbarDropdownMenuLink"
					nav
					style={{ display: "flex", alignItems: "center", gap: 10 }}
				>
					{/* <i className="nc-icon nc-world-2" /> */}
					<RFlex>
						{/* <img
							style={{
								width: "20px",
								height: "20px",
								objectFit: "cover",
								position: "relative",
								top: "3px",
							}}
							src={languages.list[languages.selected].icon}
						/> */}
						<span>{languages.list[languages.selected].title}</span>
					</RFlex>
				</DropdownToggle>
				<DropdownMenu
					persist
					aria-labelledby="navbarDropdownMenuLink"
					right
					// className={`${styles.dropdown__language}`}
				>
					{Object.keys(languages.list)
						.filter((el) => el !== languages.list[languages.selected].name)
						.map((key) => {
							return (
								<DropdownItem key={languages.list[key].name} onClick={() => swapLanguage(languages.list[key].name)}>
									<RFlex>
										{/* <img
											style={{
												width: "20px",
												height: "20px",
												objectFit: "cover",
												position: "relative",
												top: "3px",
											}}
											src={languages.list[key].icon}
										/> */}
										{languages.list[key].title}
									</RFlex>
								</DropdownItem>
							);
						})}
				</DropdownMenu>
			</UncontrolledDropdown>
		</div>
	);
};

export default RLanguageSwitcher;
