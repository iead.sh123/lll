import React, { useEffect } from "react";
import { Collapse, Container, Row } from "reactstrap";
import RFlex from "./RFlex/RFlex";
function RCollapsible({
  children,
  buttonLabel = "",
  activate,
  disabled,
  navbar,
  icon,
  color
}) {
  const [collapseOpen, setCollapseOpen] = React.useState(false);

  const toggleCollapse = () => {
    // if (!collapseOpen) {
    //  // setColor("bg-white");
    // } else {
    // //  setColor("navbar-transparent");
    // }
   // activate();
    setCollapseOpen(!collapseOpen);
  };

const r = Math.random();


  return (
    <div
    style={{position:"relative",width:"100%",zIndex:"100000",height:'fit-content' ,backgroundColor:'#EEF2FB'}}
    className="collapsdiv"
    id="rcollabsiple"
  >
       <div
        aria-controls="navigation-index"
        aria-expanded={collapseOpen}
        aria-label={"Toggle navigation" + r}
        // data-target="#navigation"
        data-toggle="collapse"
        type="button"
    
        style={{ width: "100%",height:"30px", minHeight: "10px",background:"#EEF2FB" }}
        onClick={toggleCollapse}
      >
        
        <RFlex className="justify-content-between align-items-center">
          <RFlex className="align-items-center">
            <i className={icon} style={{color:color}} />
            {buttonLabel}
          </RFlex>
            <i class={`${collapseOpen?"fa-solid fa-chevron-down":"fa-solid fa-chevron-right"}`}></i>
          </RFlex>
      </div>

      <Collapse
        className="justify-content-end"
          id="Collapse Container"
          style={{ position: "relative", zIndex: 1000,background:"#EEF2FB" }}
        isOpen={collapseOpen && !disabled}
        navbar={navbar}
      >
        {children}
      </Collapse>

  </div>
  );
}

export default RCollapsible;
