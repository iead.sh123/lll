import React from "react";
import withDirection from "hocs/withDirection";
import RTableImage from "../RTableImage";
import RFormItem from "../RFormItem";
import RButton from "../RButton";
import RColor from "../RColor";
import styles from "./RTableLister.module.scss";
import RFlex from "../RFlex/RFlex";
import tr from "../RTranslator";
import { Col, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from "reactstrap";
import { RLabel } from "../RLabel";
import { Table } from "reactstrap";

const RTableLister = ({
	Records,
	recordsArr,
	hideTableHeader = false,
	firstCellImageProperty,
	align,
	noBorders = false,
	center = false,
	emptyTable = false,
	fixedWidth,
	fixedHeight,
	tableStyle,
	stickyHeader = false,
}) => {
	return (
		<Col xs={12} className={`p-0 ${stickyHeader ? styles.scrollableTable : ""}`} style={tableStyle}>
			<Table hover={!noBorders} responsive style={noBorders ? {} : { border: "1px solid #D9D9D9" }}>
				{!hideTableHeader && (
					<thead>
						<tr style={{ backgroundColor: "rgb(238, 242, 251)" }}>
							{Records?.[0]?.details?.map((detailName, index) => {
								return (
									<th
										key={"rhd" + index}
										style={{
											paddingLeft: "20px",
											border: noBorders ? "" : "1.5px solid #eee",
											...detailName.keyStyle,
										}}
										hidden={detailName.hidden ? detailName.hidden : false}
										className="fixed_th"
									>
										<RFlex
											styleProps={fixedWidth ? { width: fixedWidth } : {}}
											className={`align-items-center ${center && index !== 0 ? "justify-content-center" : ""}`}
										>
											{detailName.keyType == "component" ? detailName.key : tr(detailName.key)}
											{detailName.dropdown && (
												<UncontrolledDropdown direction="right">
													<DropdownToggle aria-haspopup={true} color="default" data-toggle="dropdown" nav style={{ padding: "0px" }}>
														<RFlex className={"dropdown-arrows"}>
															<i class="fa fa-chevron-up" aria-hidden="true" style={{ fontSize: "8px" }} />
															<i class="fa fa-chevron-down" aria-hidden="true" style={{ fontSize: "8px" }} />
														</RFlex>
													</DropdownToggle>
													<DropdownMenu persist aria-labelledby="navbarDropdownMenuLink" right className="mb-4">
														{detailName.dropdownData?.map((data, index) => (
															<DropdownItem
																key={data.id ?? data.name}
																onClick={() => {
																	detailName.onClick(data);
																}}
															>
																<span>{data.title}</span>
															</DropdownItem>
														))}
													</DropdownMenu>
												</UncontrolledDropdown>
											)}
										</RFlex>
									</th>
								);
							})}

							{Records?.[0]?.actions?.length > 0 && (
								<th
									className="fixed_th"
									style={{ border: "1.5px solid #eee", textAlign: "center", fontSize: Records?.[0]?.actions[0].fontSize ? "18px" : "" }}
								>
									{tr("")}
								</th>
							)}
						</tr>
					</thead>
				)}
				{!emptyTable && (
					<tbody>
						{Records?.map((record, index) => {
							return (
								<tr key={"rec" + index} style={noBorders ? { border: "0px solid white" } : {}}>
									{record?.details?.map((detail, indexx) => {
										return (
											<td
												hidden={detail.hidden ? detail.hidden : false}
												key={"rd" + indexx}
												style={{
													minWidth: detail.minWidth ? detail.minWidth : "100px",
													paddingLeft: "20px",
													color: detail.color ?? "black",
													textAlign: detail.alignment ? detail.alignment : "left",
													fontSize: detail.fontSize ? detail.fontSize : "",
													borderTop: noBorders ? "0px solid white" : "1.5px solid #eee",
												}}
											>
												<RFlex
													onClick={detail.onClickRow}
													styleProps={{ margin: detail.margin ? detail.margin : "", cursor: detail.cursor ? detail.cursor : "" }}
													className={`${center && indexx !== 0 ? "align-items-center justify-content-center" : "align-items-center"}`}
												>
													{[
														"image",
														"main course image",
														"main course icon",
														"main course icon double",
														"main course svg",
														"logo",
													].includes(detail?.type) ? (
														<RTableImage detail={detail} />
													) : detail?.html || firstCellImageProperty ? (
														detail?.value
													) : detail?.type == "component" ? (
														detail?.value
													) : detail?.type == "color" ? (
														<RColor value={detail} disabled={true} />
													) : detail?.mode == "form" ? (
														<RFormItem data={detail} />
													) : detail.render ? (
														detail.render(detail.value)
													) : (
														<RLabel value={tr(detail?.value)} lettersToShow={50} dangerous={false} />
													)}
												</RFlex>
												{/* {Helper.js(detail?.type)} */}
											</td>
										);
									})}

									{record?.actions && (
										<td
											className="text-center"
											style={{
												borderTop: noBorders ? "0px solid white" : "1.5px solid #eee",
											}}
										>
											{record?.actions.length > 1 ? (
												<UncontrolledDropdown className="me-2" direction="down">
													<DropdownToggle
														aria-haspopup={true}
														// caret
														color="default"
														data-toggle="dropdown"
														nav
													>
														<i class="fa fa-ellipsis-v" aria-hidden="true" style={{ cursor: "pointer", color: "#333" }}></i>
													</DropdownToggle>
													<DropdownMenu
														persist
														aria-labelledby="navbarDropdownMenuLink"
														right
														// container="body"
													>
														{record?.actions?.map((action, index) => (
															<DropdownItem
																key={action.id ?? "ac" + record.id + index}
																disabled={action.disabled ? action.disabled : false}
																onClick={() => {
																	action.onClick(recordsArr);
																}}
																hidden={action.hidden ? action.hidden : false}
																className={styles.table__actions}
															>
																<RFlex
																	styleProps={{
																		alignItems: "center",
																		justifyContent: "start",
																		color: action.color && action.color,
																	}}
																>
																	{action.loading ? (
																		<RFlex>
																			<i className="fa-solid fa-spinner fa-spin pt-1"></i>
																		</RFlex>
																	) : (
																		<>
																			<i className={action?.icon} aria-hidden="true" />
																			<span>{action.name}</span>
																		</>
																	)}
																</RFlex>
															</DropdownItem>
														))}
													</DropdownMenu>
												</UncontrolledDropdown>
											) : (
												<>
													{record?.actions?.map((action, index) => {
														if (action.justIcon) {
															return (
																<i
																	className={`${action.faicon} ${action.actionIconClass} `}
																	style={{ width: "15px", height: "15px", cursor: "pointer", ...action.actionIconStyle }}
																	onClick={action.onClick}
																></i>
															);
														} else if (action.actionType === "component") {
															return action.value;
														}
														return (
															<RButton
																hidden={action.permissions == false ? true : false}
																key={action.id ?? "ac" + record.id + index}
																className="btn-icon ml-1 mr-1"
																color={action.color ? action.color : "info"}
																size="sm"
																type="button"
																onClick={() => {
																	action.onClick(recordsArr);
																}}
																disabled={action.disabled ? action.disabled : false}
																outline={action.outline ? action.outline : false}
																faicon={action.faicon}
																text={action.text}
																loading={action.loading}
															/>
														);
													})}
												</>
											)}
										</td>
									)}
								</tr>
							);
						})}
					</tbody>
				)}
			</Table>
		</Col>
	);
};
export default withDirection(RTableLister);
