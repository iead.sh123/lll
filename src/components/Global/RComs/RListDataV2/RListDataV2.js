import React from 'react'
import { dataTypes } from 'logic/SchoolManagement/constants'
import RProfileName from '../RProfileName/RProfileName'
import RIconDiv from '../RIconDiv/RIconDiv'
import RFlex from '../RFlex/RFlex'
import iconsFa6 from 'variables/iconsFa6'
import { types } from 'logic/SchoolManagement/constants'
import tr from '../RTranslator'
const RListDataV2 = (
    {
        userType,
        data,
        userTypeId,
        onClick,
        inSelectMode,
        setEnrollUsersOpen,
        elementsToShow = 12,

    }
) => {
    const [showAll, setShowAll] = React.useState(false)
    return (

        data.length <= 0 ?
            <RFlex className="flex-column">
                <span className="p-0 m-0 font-weight-bold">{userType}</span>

                <p className="p-0 m-0" style={{ cursor: "pointer" }} onClick={() => setEnrollUsersOpen({ isOpen: true, userTypeId, userType })}>
                    No {userType} Yet &nbsp;
                    <span className="text-primary p-0 m-0"> {tr(`Enroll Or Create New`)} Account</span>
                </p>
            </RFlex> :
            < RFlex className='flex-column' styleProps={{ width: '100%' }
            }>
                <RFlex className="justify-content-between align-items-center" styleProps={{ width: '100%' }}>
                    <RFlex>
                        {<span className='p-0 m-0 font-weight-bold'>{userType}</span>}
                        <RIconDiv
                            text={data?.ids?.length}
                            divStyle={{
                                borderRadius: '100%', width: '19px', height: '19px',
                                fontSize: '12px', cursor: 'default', margin: '0px'
                            }} />
                    </RFlex>
                    <span className='p-0 m-0 text-primary' style={{ cursor: 'pointer' }} onClick={() => setEnrollUsersOpen({ isOpen: true, userTypeId, userType })}>Enroll {userType}</span>
                </RFlex>
                <RFlex styleProps={{ flexWrap: 'wrap', gap: '20px 10px' }}>
                    {data.map((entity, index) => {

                        if (index + 1 <= elementsToShow || showAll) {
                            return (
                                <RFlex id="flex" key={index} className="justify-content-center align-items-center" styleProps={{ position: 'relative' }} onClick={() => { inSelectMode ? onClick(userType, entity) : '' }}>
                                    <RProfileName
                                        key={entity.name}
                                        name={entity.name}
                                        img={entity.image ? entity.image : null}

                                        flexStyleProps={{ width: '149px', height: '32px', gap: '5px', cursor: inSelectMode ? 'pointer' : 'default' }} />
                                    {inSelectMode && entity.selected &&
                                        <i
                                            className={`${iconsFa6.check} text-success fa-sm`}
                                            style={{ position: 'absolute', right: '0px', bottom: '40%' }}
                                        />}
                                </RFlex>
                            )
                        }
                        else {
                            return null
                        }
                    })}
                </RFlex>
                {
                    data?.ids?.length > elementsToShow ?
                        <span className='p-0 m-0 text-primary'
                            style={{ cursor: 'pointer' }}
                            onClick={() => setShowAll(!showAll)}
                        >{showAll ? "showLess" : "ShowMore"}</span> : ""
                }
            </RFlex >

    )

}

export default RListDataV2