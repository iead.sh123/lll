import { useSelector } from "react-redux";
import { Button } from "reactstrap";
import Helper from "./Helper";
import RCollapsible from "./RCollapsible";

function RMonitor() {
	const data = useSelector((state) => state.Monitor.data); //([{label:"string",value:"lskjfdh"},{label:"obj",value:{hello:"hello"}}]));//dataToMonitor()

	const getString = (obj) => {
		const label = obj.label;
		const s = obj.value;

		if (label !== "") {
			if (typeof s != "object") {
				return label + " = " + s;
			} else {
				return label + "=" + JSON.stringify(s);
			}
		} else {
			return JSON.stringify(s);
		}
	};

	const print = (s) => {};
	return (
		<RCollapsible buttonLable={"-----------------"}>
			<div style={{ background: "yellow", color: "black", width: "200px" }}>
				{data.map((s) => (
					<>
						<Button
							style={{
								width: "10px",
								height: "10px",
								padding: "0",
								margin: "0",
								fontSize: "8px",
							}}
							onClick={print(s)}
						>
							c
						</Button>
						{getString(s)}
						<hr />
					</>
				))}
			</div>
		</RCollapsible>
	);
}
export default RMonitor;
