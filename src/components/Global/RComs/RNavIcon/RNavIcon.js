import React from 'react'
import {NavLink } from 'reactstrap'

const RNavIcon = (
    {
        linkText='link',
        linkIcon,
        href='#',
        className,
        style,
        iconStyle,
        iconOnRight=false
    }
) => {
  return (
    <NavLink className={`d-flex align-items-center p-0 ${className}`} style={{ fontSize: '16px', gap: '10px',...style}} href={href}>
    {!iconOnRight&&<i className={linkIcon} style={{ width: "16px", height: '15px', color: "#668AD7",...iconStyle }}></i>}
    {linkText}
    {iconOnRight&&<i className={linkIcon} style={{ width: "16px", height: '15px', color: "#668AD7",...iconStyle }}></i>}
  </NavLink>
  )
}

export default RNavIcon