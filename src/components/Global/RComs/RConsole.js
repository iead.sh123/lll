import React from "react";

import RCollapsible from "./RCollapsible";
import Helper from "./Helper";
function RConsole() {
	const show = (data, name = "") => {
		return (
			<div>
				<RCollapsible buttonLable={name}>
					{Object.keys(data).map(function (key) {
						let value = data[key];
						return (
							<div>
								{key}: {typeof value == "object" ? show(value, key) : value}
							</div>
						);
					})}
				</RCollapsible>
			</div>
		);
	};
	return (
		<div
			style={{
				position: "fixed",
				//  width:"50%",
				//  height:"70%",
				background: "black",
				color: "white",
				left: "20%",
				top: "30%",
			}}
		>
			<RCollapsible buttonLable="Console">{Helper.consoleObjects?.map((o) => show(o.data, o.name))}</RCollapsible>
		</div>
	);
}

export default RConsole;
