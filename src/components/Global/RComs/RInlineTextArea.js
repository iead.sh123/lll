import React, { useState }  from "react";
import './RInlineTextbox.css';

 function RInlineTextArea({text,setText,label}) {

  const [mode,setMode]=useState('label')
  const edit=()=>{
    setMode('edit')
     }
  
  const agree=(event)=>{
    setMode('label')
     }   
     return(
      <div style={{width:"100%",display:'flex',gap:'5px',flexDirection:'column'}}>
       <div className="text_label_edit">
        <div  className="text_label">
       <span>{label}</span>
        </div>
        <div className="edit_but">
          <button style={{backgroundColor:'#D7E3FD'}} onClick={mode=='label'?()=>edit():()=>agree()}>
             {mode=='label'?<i className="fa-solid fa-pen"> </i> :  <i className="fa-solid fa-check"> </i> }
          </button>
        </div>
       </div>
       {/* {"mode=="+mode} */}
       {mode=='label'?<p className="p-0 m-0" style={{wordBreak:'break-all'}}>{text}</p>: 
       <textarea style={{width: "100%"}} name="postContent" rows={4} cols={20} onChange={event => 
       setText(event.target.value)} />
      } 
     </div>
      )
     }
 
export default RInlineTextArea;