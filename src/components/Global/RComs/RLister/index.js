import React from "react";
import RNewTableLister from "../RNewTableLister";
import withDirection from "hocs/withDirection";
import RQPaginator from "../RQPaginator/RQPaginator";
import REmptyData from "components/RComponents/REmptyData";
import RTable from "components/RComponents/RTable/RTable";

function RLister({
	Records,
	info,
	withPagination = false,
	handleChangePage,
	page,
	hideTableHeader = false,
	firstCellImageProperty,
	align = "center",
	center = false,
	emptyTable = false,
	fixedWidth,
	fixedHeight,
	noBorders,
	tableStyle,
	activeScroll = false,
	maxHeight = "50vh",
	Image,
	line1,
	component,
	minHeight,
	disableXScroll = false,
	removeImage,
	convertRecordsShape = true,
	containerClassName = "max-h-[450px]",
	callBack,
}) {
	return (
		<>
			{Records?.columns ? (
				<RTable
					Records={Records}
					convertRecordsShape={convertRecordsShape}
					containerClassName={containerClassName}
					emptyData={line1}
					callBack={callBack}
				/>
			) : (
				<>
					{Records?.length == 0 ? (
						<REmptyData Image={Image} removeImage={removeImage} line1={line1} component={component} />
					) : (
						<RNewTableLister
							Records={Records}
							recordsArr={Records}
							hideTableHeader={hideTableHeader}
							firstCellImageProperty={firstCellImageProperty}
							align={align}
							center={center}
							emptyTable={emptyTable}
							fixedWidth={fixedWidth}
							fixedHeight={fixedHeight}
							noBorders={noBorders}
							tableStyle={tableStyle}
							activeScroll={activeScroll}
							maxHeight={maxHeight}
							minHeight={minHeight}
							disableXScroll={disableXScroll}
						/>
					)}
				</>
			)}
			{(Records?.columns ? Records?.data?.length !== 0 : Records?.length !== 0) && info && withPagination && (
				<RQPaginator info={info} handleChangePage={handleChangePage} page={page} />
			)}
		</>
	);
}
export default withDirection(RLister);
