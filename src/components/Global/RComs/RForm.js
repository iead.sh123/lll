import React, { useEffect } from "react";
import { Col, Form, Row } from "reactstrap";
import RFormItem from "./RFormItem";
import RSubmit from "./RSubmit";
import withDirection from "hocs/withDirection";
import { useRef } from "react";
import Helper from "./Helper";

function RForm({
	onSubmit,
	formItems,
	submitLabel,
	inline,
	className,
	disabled,
	loading,
	withSubmit = true,
	type,
	onClick,
	dir,
	preventEnterMove = false,
	formRef,
}) {
	const lang = localStorage.getItem("language") || "english";
	const float = lang == "arabic" ? "right" : "left";

	const RFormRef = useRef();

	return (
		<div className="content" style={{ textAlign: float }}>
			{/* <button onClick={()=>{
     //  const sss=RFormRef.current.submit();
    
      }
    }></button> */}
			<Form
				onSubmit={onSubmit}
				inline={inline}
				innerRef={formRef}
				onKeyDown={(e) => {
					e.key === "Enter" && preventEnterMove && e.preventDefault();
				}}
			>
				{formItems?.map((fi) => {
					const item = <RFormItem data={fi} key={fi.id} dir={dir} />;
					const col = (
						<div style={{ width: (100 * fi.col) / 12 + "%", display: "inline-block", position: "relative" }}>
							{" "}
							<>{item}</>
						</div>
					);
					return fi.col ? <>{col}</> : <>{item}</>;
				})}

				{withSubmit && (
					<RSubmit
						dir={dir}
						value={submitLabel ? submitLabel : "Submit"}
						className={className}
						disabled={disabled ? disabled : false}
						type={type ? type : "submit"}
						onClick={onClick ? onClick : ""}
						loading={loading ?? false}
					/>
				)}
			</Form>
		</div>
	);
}

export default withDirection(RForm);
