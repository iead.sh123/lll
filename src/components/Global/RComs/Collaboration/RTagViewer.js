import React from "react";
import "./RTagViewer.css";

function RTagViewer({
  name,
  size = "12px",
  icon,
  iconColor,
  backgroundColor,
  textColor,
}) {
  return (
    <div
      className="tag_container"
      style={{
        fontSize: size,
        backgroundColor: backgroundColor ?? "white",
        color: textColor ? textColor : iconColor ? iconColor : "black",
        padding: "1px 13px",
      }}
    >
      {icon ? <i className={icon}></i> : ""}
      <span>{name}</span>
    </div>
  );
}

export default RTagViewer;
