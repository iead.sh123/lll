import react, { useState } from "react"
import './RRecentActivities.css'
import Helper from "../Helper";

function RRecentActivities({activities}){
  
  const activitiesItems=Object.entries(activities);
  const [active,setActive]=useState(-1)
  Helper.cl(activities,"activities");
  Helper.cl(activitiesItems,"activitiesItems");
    return(
        <div style={{margin:"1rem"}}>
           {/* {Helper.js(activitiesItems ,"raitem")} */}
         {/* <div className="Activity_title"> Recent Activities </div>  <br/> */}
            <div className="Activities_container">
           
                {activitiesItems.map((item,index)=>(
                  <div className="Activity" onClick={()=>setActive(index)} style={{borderLeft:active==[index]?'5px solid rgb(28,113,170)':'5px solid rgb(65,181,255)'}}>
                       <div className="Activity_title">
                        

                        {/* {Helper.js(item ,"raitem")} */}
                        {item?.[0]}
                        
                        </div>
                         {item?.[1]?.map(i=>(
                            <div className="Activity_item">{i}</div>
                          ))}
                  </div>
                    )
                         
                        )
                }
             
            </div>     
        </div>
    )
}
export default RRecentActivities;