import React from 'react';
import './RTip.css';

function RTip({image , component }){

    return(
        <div className='tip'>
             <div className='tip_img'>
              <img src={image}/>
             </div>

             <div className='tip_component'>
                {component}
             </div>
        </div>
    )
}
export default RTip