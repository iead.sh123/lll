import React, { useState }  from "react";
import './RCategoryCard.css';
import RCategoryCard from "./RCategoryCard";
 function RCategoryCardTest() {
  const categoryCards=[
    {title:"Collaboration Dashboard",text:"Collaboration Collaboration",icon:"",backGroundColor:"gray",cornerColor:"blue",iconColor:"gray",onClick:()=>{}},
    {title:"Nominate",text:"Nominate Nominate",icon:"fa fa-paperclip",backGroundColor:"red",cornerColor:"yellow",iconColor:"red",onClick:()=>{}},
    {title:"Approve",text:"Approve Approve Approve",icon:"fa fa-check-square",backGroundColor:"blue" ,cornerColor:"green",iconColor:"blue",onClick:()=>{}},
    {title:"Review",text:"Review Review Review",icon:"fa fa-search",backGroundColor:"",cornerColor:"green",iconColor:"",onClick:()=>{}},
             ]
   return(
     <div className="cards">
      {
        categoryCards.map(i=> 
          <div className="card_1"> 
          <RCategoryCard 
         // height='125px'
          title={i.title}
          text={i.text}
          icon={i.icon}
         // backGroundColor={i.backGroundColor}
         // cornerColor={i.cornerColor}
          //iconColor={i.iconColor}
          onClick={i.onClick}
          />
          </div> )
          
      }
      
     
     </div>
  
    )
   }
 
export default RCategoryCardTest;