import React, { useState } from "react";
import "./RCollaborationCard.css";
import Helper from "../Helper";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import { Services } from "engine/services";
import RDropdown from "../RDropdown";
import { RMenu } from "../RMenu";
import RLiveAction from "./RLiveAction";
import { addDataToConstraint } from "store/actions/global/learningObjects";
import placeholder from "assets/img/placeholder.jpg";
const RCollaborationCard = (props) => {
  // return <div> dfgs dfg sdfg sdf g</div>
  const {title,subtitle,Description,Author,onClick1,
    UpLeftIcon,UpLeftText,Actions,bottomBorderColor,padding,margin,borderLess,focused,
    additionalComponent,menuActions,approveStatus}=props;

  //  Helper.cl(props,"collboration card stuff")
// Actions: null
// Author: {image: 'tea.png', text: 'Canvas'}
// Description: null
// UpLeftIcon: "fa fa-angle-right"
// UpLeftText: null
// additionalComponent: null
// bottomBorderColor: null
// onClick: ƒ onClick()
// subtitle: {text: 'Unit Plan'}
// title: {text: 'Arabic Letters'}

const imgsrc =(path)=>{
  if(!path) return false;
  const b=Services.storage.file;
  if(path.includes(b))
  {
    return path;
  }
  else return b+path;
  };


// Helper.cl(Author.image,"author image");
// Helper.cl(imgsrc(Author.image),"imgsrcauthor image");
// Helper.cl(placeholder,"placeholder");

    return(
        <div className={borderLess?"card_Container_no_border":"card_Container"} onClick={onClick1} style={{borderBottom:bottomBorderColor?`2px solid ${bottomBorderColor}`:'2px solid #668AD7',padding:padding??"15px",margin:margin??"1rem"}}>
          {/* {Helper.js(props)} */}

      {/* {title} */}
      <div className="card_Container_c">
        {title && (
          <div className="card_title">
            {title.image && (
              <div className="card_title_img">
                <img src={title.image}></img>{" "}
              </div>
            )}
            {title.text && (
              <div className="card_title_text" style={{fontWeight:focused?"1000":"200"}}>
                <span>{title.text}</span>{" "}
              </div>
            )}
          </div>
        )}
        {subtitle && (
          <div className="card_subtitle">
            {subtitle.image && (
              <div className="card_subtitle_img">
                <img src={imgsrc(subtitle.image)??placeholder}></img>{" "}
              </div>
            )}
            {subtitle.text && (
              <div className="card_subtitle_text">
                <span>{subtitle.text}</span>{" "}
              </div>
            )}
          </div>
        )}
        {Author && (
          <div className="card_Author">
           
              <div className="card_Author_img">
                <img src={Author.image?imgsrc(Author.image):placeholder}></img>{" "}
              </div>
           
            {Author.text && (
              <div className="card_Author_text">
                <span>{Author.text}</span>{" "}
              </div>
            )}
          </div>
        )}

        {approveStatus && <RLiveAction
                title={approveStatus.title}
                icon={approveStatus.icon}
                color={approveStatus.color}
                text={approveStatus.title}
                value={approveStatus.value}
                loading={approveStatus.loading}
                type={approveStatus.type}
                onClick={approveStatus.onClick}
              /> 
          // (<button
          //   className="action_btn"
          //   onClick={approveStatus.onClick}
          //   style={{
          //     color: approveStatus.color,
          //     border:
          //       approveStatus.withBorder == true
          //         ? `0px solid ${approveStatus.color}`
          //         : "none",
          //   }}
          // >
          //   {approveStatus.icon && <i className={approveStatus.icon} />}
          //   {approveStatus.title && <span> {approveStatus.title} </span>}
          // </button> )
       }

        {Description && (
          <div className="card_Description">
            <span>{Description}</span>
          </div>
        )}
        {additionalComponent && (
          <div className="additionalComponent">{additionalComponent}</div>
        )}

        {Actions && (
          <div className="card_actions">
            {Actions.map((action) => (
              // {if(action.type=="checkbox")
              //  return <AppCheckbox
              //     onChange={action.onClick }
              //     label={action.title}
              //     checked={action.value}
              //     />
              //      else return <button className="action_btn" onClick={action.onClick} style={{color:action.color,border:action.withBorder==true?`1px solid ${action.color}`:'none'}}>
              //       {action.icon&&<i className={action.icon}/>}
              //       {action.title&&<span> {action.title} </span>}
              //   </button>}

             action&& <RLiveAction
                title={action?.title}
                icon={action?.icon}
                color={action?.color}
                text={action?.title}
                value={action?.value}
                loading={action?.loading}
                type={action?.type}
                onClick={action?.onClick}
                border={action.border}
                withBorder={action.withBorder}
                disabled={action.disabled}
                disabledNote={action.disabledNote}
                
              />
            ))}
          </div>
        )}
      </div>

      {UpLeftIcon ? (
        <div className="card_icon_text">
          <i className={UpLeftIcon} />{" "}
        </div>
      ) : UpLeftText ? (
        <div className="card_icon_text">
          <span>{UpLeftText}</span>{" "}
        </div>
      ) : null}

      {menuActions &&<div style={{position:"relative",right:"-9px",top:"-9px"}}> <RMenu actions={menuActions} /></div>}
    </div>
  );
};
export default RCollaborationCard;
