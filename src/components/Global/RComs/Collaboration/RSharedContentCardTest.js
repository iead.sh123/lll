import React from "react";
import './RSharedContentCard.css';
import RSharedContentCard from "./RSharedContentCard";

function RSharedContentCardTest({}){
  const tags =[{name:"Grade1",iconColor:"rgb(125,166,254)",backgroundColor:"rgb(217,229,254)"},
               {name:"tag tag",iconColor:"rgb(245,95,37)",backgroundColor:"rgb(254,241,236)"}]
  const Author={title:'lms',image:'marie.jpg'}
return(
    <>
    <RSharedContentCard
    title='Make A Statement'
    icon="fa fa-folder"
    tags={tags}
    Author={Author}
    />
    </>
)
}
export default RSharedContentCardTest;