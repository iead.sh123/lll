import React from "react";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "ckeditor5-build-classic-mathtype";

const RCkEditor = ({ data, handleChange, itemOptions, disabled }) => {
	return (
		<CKEditor
			// onInit={(editor) => {
			// 	editor.editing.view.focus();
			// }}
			disabled={disabled}
			editor={ClassicEditor}
			config={{
				toolbar: {
					items: itemOptions
						? itemOptions
						: [
								"heading",
								"MathType",
								"ChemType",
								"|",
								"bold",
								"italic",
								"link",
								"bulletedList",
								"numberedList",
								"imageUpload",
								"mediaEmbed",
								"insertTable",
								"blockQuote",
								"undo",
								"redo",
						  ],
				},
			}}
			data={data}
			onChange={(event, editor) => {
				const newData = editor.getData();
				handleChange(newData);
			}}
			onReady={(editor) => {
				editor.editing.view.focus(); // Focus on the editor when it is ready
			}}
		/>
	);
};

export default RCkEditor;
