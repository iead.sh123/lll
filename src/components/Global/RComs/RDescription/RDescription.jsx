import React from "react";
import RCkEditor from "../RCkEditor";
import RFlex from "../RFlex/RFlex";
import tr from "../RTranslator";
import { FormGroup, FormText } from "reactstrap";
import iconsFa6 from "variables/iconsFa6";

const RDescription = ({ value, handleChange, error, openCollapseValue, collapseToggle, defaultLabel = true }) => {
	return (
		<RFlex className={"gap-[5px] flex-col"}>
			{defaultLabel && (
				<RFlex className={"items-center text-themePrimary cursor-pointer w-fit"} onClick={collapseToggle}>
					<i className={openCollapseValue ? iconsFa6.minusSolid : iconsFa6.plus} alt="alt" />
					<p className="text-xs">{tr`description`}</p>
				</RFlex>
			)}
			{openCollapseValue && (
				<FormGroup>
					{defaultLabel && <label>{tr("description")}</label>}
					<RCkEditor data={value} handleChange={handleChange} />
					{error && <FormText color="danger">{value}</FormText>}
				</FormGroup>
			)}
		</RFlex>
	);
};

export default RDescription;
