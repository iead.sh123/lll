import React, { useState, useEffect } from 'react';
import styles from "./RCatalogCard.module.scss";  
const TruncatedDescription = ({ description, maxHeight }) => {
  const [showFullContent, setShowFullContent] = useState(false);
  const [contentHeight, setContentHeight] = useState(0);

  useEffect(() => {
    // Calculate the height of the content after rendering
    if (contentRef.current) {
      setContentHeight(contentRef.current.clientHeight);
    }
  }, [description]);

  const contentRef = React.createRef();

  const toggleContent = () => {
    setShowFullContent(!showFullContent);
  };

  return (
    <div id="truncated-description" style={{
      maxHeight: showFullContent ? '43px' : `${maxHeight+1}px`,
      position: "relative",
      height:'43px'
      }}>
      <div id="truncated-description1"
        ref={contentRef}
        style={{
          maxHeight: showFullContent ? 'none' : `${maxHeight+1}px`,
          overflow: 'hidden',
          position: 'relative',
        }}
        dangerouslySetInnerHTML={{ __html: description }}
      />
      {/* {contentHeight}
      {maxHeight} */}
      {contentHeight > maxHeight && (
        <div
          id="truncated-description2"
          style={{
            position: 'absolute',
            bottom: '0',
            right: '0',
            background: 'white',
            padding: '5px',
            cursor: 'pointer',
          }}
          className={styles.course__description__readmore}
          // onClick={}
        >
          {showFullContent ? 'Read less' : 'Read more'}
        </div>
      )}
    </div>
  );
};

export default TruncatedDescription;
