import React, { useState } from "react";
import { dangerColor } from "config/constants";
import { Card } from "reactstrap";
import RFlex from "../RFlex/RFlex";
import styles from "./RCategoryCard.module.scss";
import tr from "../RTranslator";

const RCategoryCard = ({ categories, handlePushToAllCourses }) => {
  const [isHovered, setIsHovered] = useState(null);
  const handleMouseEnter = (id) => {
    setIsHovered(id);
  };

  const handleMouseLeave = () => {
    setIsHovered(null); // Set isHovered back to null when leaving any card
  };

  return (
    <RFlex
      styleProps={{
        justifyContent: "space-between",
        margin: "20px 0px",
        flexWrap: "wrap",
      }}
    >
      {categories.map((category) => (
        <Card
          className={styles.r_card}
          key={category.id}
          style={
            isHovered === category.id
              ? {
                  borderBottom:
                    isHovered !== null
                      ? `1px solid ${category.color}`
                      : "white",
                  color: isHovered !== null ? category.color : "black",
                  transition: "background-color 0.3s ease",
                }
              : {}
          }
          onMouseEnter={() => handleMouseEnter(category.id)}
          onMouseLeave={handleMouseLeave}
          onClick={() => handlePushToAllCourses(category.id)}
        >
          <RFlex styleProps={{ justifyContent: "center" }}>
            <h6>{category.name}</h6>
          </RFlex>
          <RFlex styleProps={{ justifyContent: "center" }}>
            <h6 className="text-muted">
              {category.courses} {tr`courses`}
            </h6>
          </RFlex>
        </Card>
      ))}
    </RFlex>
  );
};

export default RCategoryCard;
