import React, { useState } from "react";
import { get } from "config/api";
import AsyncSelect from "react-select/async";
import tr from "./RTranslator";

let debounceTimer = null;

function RTags({
	url,
	getArrayFromResponse,
	getValueFromArrayItem,
	getLabelFromArrayItem,
	getIdFromArrayItem,
	value,
	onChange,
	disableAddition, //to remove data returned from backend
	defaultValues,
	placeholder,
	readOnly = false,
	withOutSearch = false,
	onBlur,
}) {
	const [text, setText] = useState("");

	const inputChanged = (e) => {
		setText(e);
	};

	const doSearch = async () => {
		if (text.length < 3) return;

		const res = await get(url + text);

		if (res) {
			let options1 = [];

			if (!getArrayFromResponse) {
				// because i don't want to create tag if this text return from backend
				if (res && res.data && res.data.data) {
					const theItemExists = res.data.data.some((el) => el.name == text);
					if (!theItemExists) {
						!disableAddition && options1.push({ value: text, label: text, id: null });
					}
				}
			}

			// if return data from backend
			getArrayFromResponse(res)?.forEach(function (i, index) {
				if (index < 10)
					options1.push({
						value: getValueFromArrayItem(i),
						label: getLabelFromArrayItem(i),
						id: getIdFromArrayItem(i),
					});
			});

			return options1;
		}

		return [];
	};

	// EX: Languages in course information
	const doWithOutSearch = async () => {
		let options1 = [];
		!disableAddition && options1.push(...options1, { value: text, label: text, id: null });
		return options1;
	};

	const debounceLoadOptions = (delay) => {
		return new Promise((resolve) => {
			clearTimeout(debounceTimer);
			debounceTimer = setTimeout(() => {
				resolve(doSearch());
			}, delay);
		});
	};

	const setLoadOptions = (delay) => {
		return new Promise((resolve) => {
			clearTimeout(debounceTimer);
			debounceTimer = setTimeout(() => {
				resolve(doWithOutSearch());
			}, delay);
		});
	};

	return (
		<AsyncSelect
			styles={{
				control: (baseStyles, state) => ({
					...baseStyles,
					width: "100%",
					height: "38px",
				}),
			}}
			aria-label={"aria label"}
			placeholder={placeholder ?? tr("select") + "..."}
			isMulti
			onInputChange={inputChanged}
			defaultValue={defaultValues}
			loadOptions={() => (withOutSearch ? setLoadOptions(100) : debounceLoadOptions(1000))}
			onChange={onChange}
			onBlur={onBlur}
			isDisabled={readOnly}
			value={value}
		/>
	);
}
export default RTags;
