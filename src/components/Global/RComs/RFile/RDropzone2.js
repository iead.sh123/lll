import React, { useMemo, useState } from "react";
import Swal, { WARNING } from "utils/Alert";
import { convertBase64 } from "utils/convertToBase64";
import { primaryColor } from "config/constants";
import { useDropzone } from "react-dropzone";
import { Services } from "engine/services";
import { post } from "config/api";
import upload_states from "variables/upload_states";
import dropZoneIcon from "assets/img/new/svg/upload-file.svg";
import RFlex from "../RFlex/RFlex";
import axios from "axios";
import tr from "../RTranslator";
import RButton from "../RButton";
import { imageTypes } from "config/mimeTypes";
import iconsFa6 from "variables/iconsFa6";
import { toast } from "react-toastify";

const baseStyle = {};

const activeStyle = {
	borderColor: "#2196f3",
};

const acceptStyle = {
	borderColor: "#00e676",
};

const rejectStyle = {
	borderColor: "#ff1744",
};

function RDropzone2({ fileType, fileSize = 10, singleFile, placeholder, setFiles, theme = "default", dropZoneRef, binary, widthHeight }) {
	const fileIdsGen = React.useRef(null);

	React.useEffect(() => {
		fileIdsGen.current = (function* () {
			let i = 0;
			while (true) {
				yield `file${++i}`;
			}
		})();
	}, []);
	//--------------------------------------------------------------
	//------------------------------------------------------Binary action

	const uploadToBackend = async (file) => {
		const res = await post(`${Services.storage.backend}api/upload/initiate`);
		if (res && res.data.status == 1 && res.data.data && res.data.data.upload_id) {
			//-------------------------------------
			const url = `${Services.storage.backend}api/upload/file`;
			var bodyFormData = new FormData();
			bodyFormData.append("upload_id", res.data.data.upload_id);
			bodyFormData.append("name", file?.name);
			bodyFormData.append("file", file);
			const Uploadpromis = axios({
				method: "post",
				url: url,
				data: bodyFormData,
				headers: {
					"Content-Type": "multipart/form-data",
				},
			});
			return Uploadpromis;
		}
	};
	const uploadSuccess = (response, file) => {
		const preview1 = file.url + "";
		const attachment = Object.assign(file, {
			url: { upload_id: response.data.data.upload_id },
			preview1: preview1,
			tempid: response.data.data.upload_id,
			id: response.data.data.upload_id,
			upload_state: upload_states.finished_uploading,
		});
		if (typeof setSpecificAttachment === "function") {
			setSpecificAttachment(attachment);
		}
		const atts = [];
		atts.push(attachment);
		setFiles(atts);
	};

	//--------------------------------------------------------
	const b64Handle = async (fs) => {
		let Attachments = [];
		if (fs[0] !== undefined) {
			for (const file of Array.from(fs)) {
				const size = file.size / 1024 / 1024;
				if (size > fileSize) {
					toast.warning(fileSize ? fileSize + "GB is the Maximum size Allowed " : "10GB is the Maximum size Allowed");
				} else {
					//------------------------------------------binary

					const base64 = await convertBase64(file);
					Attachments.push(
						Object.assign(file, {
							url: base64,
							preview1: base64,
							file_name: file.name,
							mime_type: file.type,
						})
					);
				}
			}
			setFiles(Attachments);
		} else {
			toast.warning("File is undefined");
		}
	};

	const binaryHandle = async (fs) => {
		for (const file of Array.from(fs)) {
			let file1 = file;

			if (imageTypes.includes(file.type)) {
				const base64 = await convertBase64(file);
				file1 = Object.assign(file, { url: base64 });
			}

			const uploadPromise = await uploadToBackend(file1);

			if (uploadPromise && uploadPromise.data.status == 1 && uploadPromise.data.data && uploadPromise.data.data.upload_id) {
				uploadSuccess(uploadPromise, file1);
			} else {
				uploadPromise
					.then(function (response) {
						if (response && response.data.status == 1 && response.data.data && response.data.data.upload_id) {
							uploadSuccess(response, file1);
						} else {
							toast.warning("Failure response");
						}
					})
					.catch(function (response) {
						toast.warning("Failure response");
					});
			}
		}
	};

	// - - - - - - - - - - onDrop - - - - - - - - - -

	const onDrop = (acceptedFiles) => {
		if (singleFile && acceptedFiles?.length > 1) {
			toast.warning(tr`Only 1 file is allowed`);
			return;
		}

		const newFiles = acceptedFiles.map((file) => {
			const size = file.size / (1024 * 1024);
			const reader = new FileReader();

			reader.onload = () => {
				const img = new Image();
				img.onload = () => {
					const width = img.width;
					const height = img.height;

					if (widthHeight.checkingWHImage) {
						if (width < widthHeight.minWidth || height < widthHeight.minHeight) {
							toast.warning(`Image dimensions must be at least ${widthHeight.minWidth}x${widthHeight.minHeight} pixels`);
							return;
						}
					} else {
						if (size >= fileSize) {
							toast.warning(fileSize ? `${fileSize}GB is the Maximum size Allowed` : "10GB is the Maximum size Allowed");
							return;
						}
					}
				};
				img.src = reader.result;
			};

			reader.readAsDataURL(file);

			return Object.assign(file, {
				preview: URL.createObjectURL(file),
				id: fileIdsGen.current.next().value,
			});
		});

		!binary && b64Handle(newFiles);
		binary && binaryHandle(newFiles);
	};

	const { getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject } = useDropzone({
		onDrop,
		accept: fileType
			? fileType
			: "video/* , image/jpeg, image/* , image/png ,image/jpg, image/svg, image/svg+xml, .rar, .zip, application/x-zip-compressed,  application/pdf,application/vnd.openxmlformats-officedocument, application/vnd.openxmlformats-officedocument.wordprocessingml.document",
	});

	const style = useMemo(
		() => ({
			...baseStyle,
			...(isDragActive ? activeStyle : {}),
			...(isDragAccept ? acceptStyle : {}),
			...(isDragReject ? rejectStyle : {}),
		}),
		[isDragActive, isDragReject, isDragAccept]
	);

	const hiddenStyle = useMemo(
		() => ({
			display: "none",
			...baseStyle,
			...(isDragActive ? {} : {}),
			...(isDragAccept ? {} : {}),
			...(isDragReject ? {} : {}),
		}),
		[isDragActive, isDragReject, isDragAccept]
	);
	return (
		<div>
			{theme == "default" || theme == "row" ? (
				<>
					<div
						id="df"
						{...getRootProps({ style })}
						// hidden={files.length != 0 ? true : false}
						style={{ border: "1px solid #ddd", padding: "20px" }}
					>
						<input {...getInputProps()} /> {/*ref={dropZoneRef} />*/}
						<RFlex styleProps={{ justifyContent: "center" }}>
							<img src={dropZoneIcon} width={"100px"} height={"100px"} />
						</RFlex>
						<RFlex styleProps={{ justifyContent: "center" }}>
							<span style={{ color: primaryColor }}>
								<span
									style={{
										color: primaryColor,
										textDecoration: "underline",
										fontWeight: "bold",
										padding: "0px 2px",
									}}
								>
									{tr`Upload`}
								</span>
								{tr`or Drag and Drop files here`}
							</span>
						</RFlex>
					</div>
				</>
			) : theme == "row1" ? (
				<>
					<div
						id="df"
						{...getRootProps({ style })}
						// hidden={files.length != 0 ? true : false}
					>
						<input {...getInputProps()} /> {/*ref={dropZoneRef} />*/}
						<RFlex styleProps={{ justifyContent: "center" }}>
							<img src={dropZoneIcon} width={"100px"} height={"100px"} />
						</RFlex>
						{/* <RFlex styleProps={{ justifyContent: "center" }}>
							<span style={{ color: primaryColor }}>
								<span
									style={{
										color: primaryColor,
										textDecoration: "underline",
										fontWeight: "bold",
										padding: "0px 2px",
									}}
								>
									{tr`Upload`}
								</span>
								{tr`or Drag and Drop files here`}
							</span>
						</RFlex> */}
					</div>
				</>
			) : theme == "round" ? (
				<>
					<div {...getRootProps({ style })}>
						<input {...getInputProps()} /> {/*ref={dropZoneRef} />*/}
						<div
							style={{
								minWidth: "22px",
								minHeight: "22px",
								border: `1px solid ${primaryColor}`,
								background: "white",
								position: "absolute",
								borderRadius: "50%",
								bottom: 0,
								right: 0,
								display: "flex",
								alignItems: "center",
								justifyContent: "center",
								cursor: "pointer",
							}}
						>
							<i className={iconsFa6.pen + " fa-sm"} style={{ color: primaryColor }}></i>
						</div>
					</div>
				</>
			) : theme == "button" || theme == "button_with_replace" ? (
				<div {...getRootProps({ style })}>
					<input {...getInputProps()} />
					<RButton className={"pick_buttons m-0"} faicon="fa fa-paperclip" />
				</div>
			) : theme == "button_replace_theme" ? (
				<div {...getRootProps({ style })}>
					<input {...getInputProps()} />
					<i className="fa fa-refresh" aria-hidden="true" style={{ cursor: "pointer" }} />
				</div>
			) : theme == "hidden" ? (
				<div {...getRootProps({ hiddenStyle })}>
					<input {...getInputProps()} ref={dropZoneRef} />
				</div>
			) : (
				""
			)}
		</div>
	);
}

export default RDropzone2;
