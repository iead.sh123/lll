import React, { useReducer } from "react";
import { useState } from "react";
import RFileAdd from "./RFileAdd";
import RFileList from "./RFileList";
import RFlex from "../RFlex/RFlex";
import { useEffect } from "react";
import { Services } from "engine/services";
import { useRef } from "react";
import UserAvatar from "assets/img/new/svg/user_avatar.svg";
import GroupAvatar from "assets/img/new/svg/group_avatar.svg";
import DefaultSchoolLogo from "assets/img/new/school_default logo.svg";

function RFileSuite({
	parentCallback,
	singleFile,
	fileSize,
	removeButton,
	value,
	showReplace = true,
	showDelete = true,
	showFileList = true,
	showFileAdd = true,
	theme,
	binary,
	fileType,
	swiperStyle,
	slideStyle,
	dynamicSlide,
	fullWidth,
	roundWidth = { width: "100px", height: "100px", type: "default" },
	widthHeight = { checkingWHImage: false, minWidth: "", minHeight: "" },
}) {
	//-------------------------------------------------------Reducer

	const initialState = { files: [] };
	function reducer(state, action) {
		switch (action.type) {
			case "SET_FILES":
				return { ...state, files: action.payload };
			case "ADD_FILE":
				return { ...state, files: [...state.files, action.payload] };
			default:
				throw new Error(`Unhandled action type: ${action.type}`);
		}
	}

	const [state, dispatch] = useReducer(reducer, initialState);

	function handleAddFile(file) {
		dispatch({ type: "ADD_FILE", payload: file });
	}

	//-----------------------------------------------------

	const files = state.files;
	function setFiles(files) {
		dispatch({ type: "SET_FILES", payload: files });
	}

	const fileIdsGen = React.useRef(null);
	React.useEffect(() => {
		fileIdsGen.current = (function* () {
			let i = 0;
			while (true) {
				yield `file${++i}`;
			}
		})();
	}, []);

	React.useEffect(() => {
		if (value)
			setFiles([
				...value
					.filter((val) => !val?.hash_id)
					.map((val) => {
						let fileObject;

						if (binary) {
							fileObject = {
								preview: val,
								id: fileIdsGen.current.next().value,
							};
						} else {
							fileObject = dataURLtoFileObject(val.url ?? val.hash_id, val.file_name ?? val.name);
							fileObject.preview = URL.createObjectURL(fileObject, fileObject.name);
							fileObject.id = fileIdsGen.current.next().value;
						}

						return fileObject;
					}),
				...value.filter((val) => val?.hash_id),
			]);
	}, []);
	// React.useEffect(() => {
	//   if (value)
	//     setFiles([
	//       ...value
	//         .filter((val) => !val?.hash_id)
	//         .map((val) => {
	//           let fileObject;
	//           if (binary) {
	//             fileObject = {
	//               preview: val,
	//               id: fileIdsGen.current.next().value,
	//             };
	//           } else {
	//             fileObject = dataURLtoFileObject(
	//               val.url ?? val.hash_id,
	//               val.file_name ?? val.name
	//             );
	//             fileObject.preview = URL.createObjectURL(
	//               fileObject,
	//               fileObject.name
	//             );
	//             fileObject.id = fileIdsGen.current.next().value;
	//           }

	//           return fileObject;
	//         }),
	//       ...value.filter((val) => val?.hash_id),
	//     ]);
	// }, [value]);

	const replaceSingleFile = (fls) => {
		setFiles(fls);
	};
	const removeFile = (fileID) => {
		const dd = files.filter((item) => (item.hash_id ? item.hash_id !== fileID : item.id !== fileID));
		setFiles(dd);
	};

	const handleOpenFileDialog = () => {
		//event.preventDefault();
		if (dropZoneRef.current) {
			dropZoneRef.current.click();
		}
	};

	const [fileToReplace, setFileToReplace] = useState();
	const replaceFile = (fileID) => {
		handleOpenFileDialog();
		setFileToReplace(fileID);
	};
	const finishReplace = (new_files) => {
		const index = files.findIndex((item) => (item.hash_id ? item.hash_id == fileToReplace : item.id == fileToReplace));
		const files1 = files; //.filter((item) =>
		//   item.hash_id ? item.hash_id !== fileToReplace : item.id !== fileToReplace
		//  );

		if (index !== -1) {
			files1.splice(index, 1, new_files[0]);
		}
		//   const files2=[...new_files,...files1];
		setFiles(files1);
	};
	const addFiles = (fls) => {
		fls.map((f) => handleAddFile(f));
	};

	useEffect(() => {
		const filesWithoutPreview = files.map((file) => {
			const { preview, tempid, upload_state, preview1, ...otherProperties } = file;
			return otherProperties;
		});

		// if (filesWithoutPreview && filesWithoutPreview.length > 0)
		if (filesWithoutPreview) parentCallback(filesWithoutPreview);
	}, [files]);

	const dropZoneRef = useRef();
	const fileAdd = showFileAdd && (
		<RFileAdd
			singleFile={singleFile}
			fileSize={fileSize}
			fileType={fileType}
			uploadName={"uploadName"}
			setSpecificAttachment={(e) => {}}
			setFiles={singleFile || ["round", "button", "button_replace_theme"].includes(theme) ? replaceSingleFile : addFiles}
			binary={binary}
			theme={theme}
			files={value}
			widthHeight={widthHeight}
		></RFileAdd>
	);

	const fileList = showFileList && (
		<RFileList
			parentCallback={parentCallback}
			removeButton={removeButton}
			theme={theme}
			removeFile={removeFile}
			files={files}
			setFiles={setFiles}
			showReplace={showReplace}
			showDelete={showDelete}
			replaceFile={replaceFile}
			swiperStyle={swiperStyle}
			slideStyle={slideStyle}
			dynamicSlide={dynamicSlide}
		/>
	);

	return (
		<div style={theme == "row1" ? { border: "1px solid #ddd", padding: "20px", width: fullWidth ? "100%" : "" } : null}>
			<RFileAdd
				singleFile={true}
				fileSize={fileSize}
				fileType={fileType}
				uploadName={"uploadName"}
				setSpecificAttachment={(e) => {}}
				setFiles={finishReplace}
				binary={binary}
				theme={"hidden"}
				files={[]}
				dropZoneRef={dropZoneRef}
				widthHeight={widthHeight}
			></RFileAdd>

			{theme == "row" || theme == "row1" ? (
				<RFlex>
					{fileAdd}
					{fileList}
				</RFlex>
			) : theme == "round" ? (
				<div
					style={{
						width: roundWidth.width,
						height: roundWidth.height,
						borderRadius: "50%",
						position: "relative",
					}}
				>
					<img
						style={{
							width: "100%",
							height: "100%",
							borderRadius: "50%",
							position: "absolute",
							objectFit: "cover",
							objectPosition: "center center",
						}}
						src={
							files?.[0]?.preview1
								? files?.[0]?.preview1
								: value?.[0]?.hash_id && value?.[0]?.hash_id !== null
								? Services.storage.file + value?.[0]?.hash_id
								: roundWidth.type == "default"
								? UserAvatar
								: roundWidth.type == "school"
								? DefaultSchoolLogo
								: GroupAvatar
						}
					/>
					{fileAdd}
				</div>
			) : ["button", "button_replace_theme", "button_with_replace"].includes(theme) ? (
				<>
					{files.length > 0 && theme == "button_with_replace" ? "" : fileAdd}
					{files.length > 0 && theme == "button_with_replace" && fileList}
				</>
			) : (
				<>
					{fileAdd}
					{fileList}
				</>
			)}
		</div>
	);
}

export default RFileSuite;
//fileAdd => functionality and contain design icon or button to open upload file interface
//fileList => design for each theme after upload file
