import React from "react";
import { Input } from "reactstrap";
import "./Button.Module.scss";

function RButtonIcon({
	id,
	text,
	icon,
	className,
	onClick,
	color,
	type,
	disabled,
	display,
	visibility,
	outline,
	key,
	padding,
	position,
	left,
	loading,
	faicon,
	backgroundColor,
	ButtonAdditionalStyle,
}) {
	return (
		<button
			key={key ? key : ""}
			className={className ? className : ""}
			value={Input}
			onClick={onClick}
			icon={icon}
			color={color}
			id={id}
			type={type ? type : "button"}
			disabled={disabled ? disabled : false}
			outline={outline ? outline : ""}
			style={{
				display: display ? display : "auto",
				visibility: visibility ? visibility : "visible",
				position: position ? position : "static",
				left: left ? left : "0px",
				textTransform: "capitalize",
				backgroundColor: backgroundColor ?? "",
				padding: padding ?? "",
				...ButtonAdditionalStyle,
			}}
		>
			{loading ? (
				<>
					{text}
					<i className="fa fa-refresh fa-spin"></i>
				</>
			) : faicon ? (
				<i className={"fa fa-" + faicon}></i>
			) : (
				text
			)}
		</button>
	);
}
export default RButtonIcon;

// {
//   loading == true ? (
//     <>
//       {text}
//       <i className="fa fa-refresh fa-spin"></i>
//     </>
//   ) : (
//     { text }
//   );
// }
