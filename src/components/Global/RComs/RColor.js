const RColor = ({
  value,
  disabled = false,
  id,
  name,

  readOnly,
  defaultValue,

  className,

  onChange,
}) => (
  <input
    style={{ float: "left", width: "100%" }}
    type="color"
    className={className}
    name={name}
    id={id}
    value={value?.value ? value?.value : value}
    defaultValue={defaultValue}
    disabled={disabled}
    readOnly={readOnly}
    onChange={onChange}
  ></input>
);

export default RColor;
