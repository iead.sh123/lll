import React  from "react";
import './RPlus.css';

 function RPlus({onClick}) {
   return(
    <div className="plus_but">
      <button onClick={onClick} > 
        <i class="fa fa-plus"></i>
      </button>
    </div>
    )
   }

export default RPlus;