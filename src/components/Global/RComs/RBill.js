import React from "react";

const RBill = ({ count, color = "#668ad7", fontColor = "#668AD7" }) => {
	const colors = {
		textColor: fontColor ? `text-[${fontColor}]` : "text-[#ffffff]",
		billBgColor: color ? `bg-[${color}]` : "bg-[#F3F3F3]",
	};
	return (
		<span
			className={`text-themePrimary flex align-items-center justify-center w-[20px] h-[20px] font-normal text-[9px] rounded-[50px] text-sm ${colors.billBgColor} ${colors.textColor}`}
		>
			{count}
		</span>
	);
};
export default RBill;
