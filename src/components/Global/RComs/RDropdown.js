import React from "react";

import {
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  UncontrolledDropdown,
} from "reactstrap";


import tr from "./RTranslator";
function RDropdown({
  items,
  itemCallback,
  selectedItemIndex,
  className,
  disabled,
  outline,
  loading,
  onClick,
  hidden,
  faicon,
  style,
  color,
  text,
  type,
  key,
  id,
  active,
}) {
  return (
    <UncontrolledDropdown>
      <style>
        {`
          #chatfolders1:hover {background-color: white !important;color: black !important;};
          #chatfolders1:focus{background-color: white !important;color: black !important;};
          #chatfolders1:active{background-color: white !important;color: black !important;};
          #chatfolders1.active{background-color: white !important;color: black !important;};
          #chatfolders1:active:focus{background-color: white !important;color: black !important;};
          #chatfolders1:active:hover{background-color: white !important;color: black !important;};
          #chatfolders1.active:focus{background-color: white !important;color: black !important;};
          #chatfolders1.active:hover{background-color: white !important;color: black !important;};
        `}
      </style>
    <DropdownToggle
      aria-haspopup={true}
      caret
      id="chatfolders1"
      color="default"
      data-toggle="dropdown" 
      style={{ backgroundColor: "white",color: "black"}}
    >
   
     {items?.[selectedItemIndex]?.name}
    </DropdownToggle>
    <DropdownMenu persist aria-labelledby="navbarDropdownMenuLink" style={{border:"green 0px solid"}}>
      {items?.map((item,i) => {
        return (
          <DropdownItem
            key={i}
            onClick={() => itemCallback(item)}
            style={
              selectedItemIndex=== i
                ? { backgroundColor: "#DDD", color: "#FFF" }
                : {}
            }
          >
            <i>{tr(item.name)}</i>
          </DropdownItem>
        );
      })}
    </DropdownMenu>
  </UncontrolledDropdown>
  );
}
export default RDropdown;
