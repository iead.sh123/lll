import React, { useEffect } from "react";
import { useState } from "react";
import { Nav, NavItem, NavLink, TabContent, TabPane, Row, Col, Collapse } from "reactstrap";
import { useHistory, useParams } from "react-router-dom";
import "./RTabs.css";
import { primaryColor } from "config/constants";
import RFlex from "./RFlex/RFlex";
import iconsFa6 from "variables/iconsFa6";
import tr from "./RTranslator";

const RTabsPanel_Vertical = ({ title, Tabs }) => {
	// ********* NOTE *********
	// i convert tan.name to string because we compare several ay once using number and another once using string
	// so it's better to standardize the type
	const history = useHistory();
	const { tabTitle, tabName, categoryId, semesterId: tabIndex } = useParams();

	const [horizontalTabs, setHorizontalTabs] = useState(tabName ? tabName : tabTitle ? tabTitle : categoryId);
	const [horizontalIndex, setHorizontalIndex] = useState(tabIndex);
	const [open, setOpen] = useState(true);

	const handleOpenCollapse = (tabName, id) => {
		setOpen(!open);
		changeTab(tabName, id);
	};

	const changeTab = (title, ind) => {
		setHorizontalTabs(title);
		if (ind) setHorizontalIndex(ind);
	};

	useEffect(() => {
		setHorizontalTabs(tabName ? tabName : tabTitle ? tabTitle : categoryId);
		setHorizontalIndex(tabIndex);
	}, [tabTitle, tabName, categoryId, tabIndex]);

	return (
		<div className="content">
			{title && (
				<Row>
					<Col sm="12" className="no-padding-no-margin ">
						<div className=" px-1 py-3  Curriculum_title">{title}</div>
					</Col>
				</Row>
			)}
			<Row>
				<Col md={2} xs={12} className="no-padding-no-margin ">
					<div className="vertical-tabs">
						<Nav vertical>
							{Tabs?.map((tab, index) => {
								return (
									<>
										{tab?.type == "collapse" ? (
											<>
												<NavItem style={{ listStyleType: "none" }}>
													<NavLink
														aria-expanded={horizontalTabs === String(tab.name)}
														data-toggle="tab"
														role="tab"
														onClick={() => (tab.loading ? null : handleOpenCollapse(String(tab.name), tab?.data[0]?.id))}
														className={horizontalTabs === String(tab.name) ? `active font-weight-bold` : ""}
														style={{
															color: primaryColor,
															cursor: "pointer",
														}}
													>
														<RFlex>
															{tab.title}
															{tab.loading ? (
																<i className={iconsFa6.spinner + " pt-1"}></i>
															) : (
																<i style={{ position: "relative", top: "4px" }} className={open ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
															)}
														</RFlex>
													</NavLink>
												</NavItem>
												<Collapse isOpen={open}>
													{tab?.data?.map((d) => (
														<NavItem key={d.id} style={{ listStyleType: "none" }}>
															<NavLink
																aria-expanded={horizontalIndex == d.id}
																data-toggle="tab"
																onMouseUp={() => {
																	history.push(`${tab.url}/${d.id}`);
																}}
																role="tab"
																className={horizontalIndex == d.id ? `active font-weight-bold` : ""}
																style={{
																	color: horizontalIndex == d.id ? primaryColor : "#333",
																	cursor: "pointer",
																}}
																onClick={() => changeTab(String(tab.name), d.id)}
															>
																{d.name}
															</NavLink>
														</NavItem>
													))}
												</Collapse>
											</>
										) : (
											<NavItem key={index} style={{ listStyleType: "none" }}>
												<NavLink
													aria-expanded={horizontalTabs == String(tab.name)}
													data-toggle="tab"
													onMouseUp={() => {
														history.push(tab.url);
													}}
													role="tab"
													className={horizontalTabs == String(tab.name) ? `active font-weight-bold` : ""}
													style={{ color: primaryColor, cursor: "pointer" }}
													onClick={() => changeTab(String(tab.name))}
													disabled={tab.disabled}
												>
													<RFlex>
														{tr(tab.title)}
														{tab.count && (
															<span
																style={{
																	borderRadius: "100%",
																	background: "#EEF2FB",
																	width: "20px",
																	height: "20px",
																	textAlign: "center",
																}}
															>
																{tab.count}
															</span>
														)}
													</RFlex>
												</NavLink>
											</NavItem>
										)}
									</>
								);
							})}
						</Nav>
					</div>
				</Col>
				<Col xs={12} md={10}>
					<TabContent className="text-center tabs_horizontal" id="my-tab-content" activeTab={horizontalTabs}>
						{Tabs.map((tab, index) => {
							return horizontalTabs == String(tab.name) ? (
								tab.type !== "collapse" ? (
									<TabPane key={index} tabId={String(tab.name)} role="tabPanel">
										{tab.content()}
									</TabPane>
								) : (
									tab?.data.map((d) => {
										return horizontalIndex == d.id && horizontalTabs == String(tab.name) ? (
											<TabPane key={index} tabId={tab.name} role="tabPanelCollapse">
												{tab.content()}
											</TabPane>
										) : (
											""
										);
									})
								)
							) : (
								""
							);
						})}
					</TabContent>
				</Col>
			</Row>
		</div>
	);
};

export default RTabsPanel_Vertical;
