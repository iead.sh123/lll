import React from "react";
import Tree from "./RTree/RTree";

const RCompassCard = ({ siteMap, setActiveNode, activeNodeId, linkPrefix }) => {
	return siteMap ? (
		<Tree linkPrefix={linkPrefix} activeNodeId={activeNodeId} setActiveNode={setActiveNode} siteMap={siteMap} />
	) : (
		<div className="container-fluid">
			<div className="row justify-content-center">
				<div className="row">
					<img style={{ paddingTop: "90px" }} src={"assets/img/dashboard/tree-loader.gif"} alt="tree loading" />
				</div>
			</div>
		</div>
	);
};

export default RCompassCard;
