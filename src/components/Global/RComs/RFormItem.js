import React from "react";
import { Row, Col, FormGroup, Input, Label, FormFeedback, UncontrolledTooltip } from "reactstrap";
import styles from "./form.Module.css";
import RTags from "./RTags";
import RSelect from "./RSelect";
import RTabsPanel from "./RTabsPanel";
import RColor from "./RColor";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import tr from "./RTranslator";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "ckeditor5-build-classic-mathtype";
import { faQuestionCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import RFileSuite from "./RFile/RFileSuite";

import RMulticheckboxDropdown from "./RMulticheckboxDropdown";
import RToggle from "components/RComponents/RToggle";

function RFormItem({ data }) {
	const validationProps = data.valid === undefined ? {} : { valid: data.valid, invalid: !data.valid };

	const trimmedValue = data?.labelinfo ?? "";
	const max = 1000000;
	const tooltipId = data?.labelinfo ? "data" + Math.floor(Math.random() * max) + trimmedValue.substring(0, 150).replace(/\s/g, "") : "";

	return (
		<Row style={data.rowStyle}>
			<Col style={data.col1Style}>
				<Label
					size={data.size}
					for={data.label}
					style={
						data.labelStyle ?? {
							fontWeight: "bold",
							marginTop: "8px",
							marginBottom: "0px",
							textTransform: "capitalize",
						}
					}
				>
					{data.label}
					{data.labelinfo ? (
						<div style={{ display: "inline" }} id={tooltipId}>
							<FontAwesomeIcon icon={faQuestionCircle} className="text-muted" />
							<UncontrolledTooltip style={{ border: "1px solid red" }} delay={0} target={tooltipId}>
								{data.labelinfo}
							</UncontrolledTooltip>
						</div>
					) : (
						<></>
					)}
				</Label>
			</Col>
			<Col md={data.md ? data.md : 12} style={data.col2Style}>
				<div floating={data.floating} className={`${data.className} `} style={data.containingDivStyle}>
					{data.type == "checkbox" && (
						<AppCheckbox
							checked={data.checked}
							onChange={data.onChange}
							onClick={data.onClick}
							disabled={data.disabled}
							style={data.style}
						/>
					)}

					{data.type == "text_editor" && (
						<CKEditor
							editor={ClassicEditor}
							style={data.style}
							config={{
								toolbar: {
									items: data.items,
								},
							}}
							data={data.value}
							onChange={data.onChange}
						/>
					)}

					{data.type == "multicheckbox_dropdown" && (
						<RMulticheckboxDropdown style={data.style} options={data.options} disabled={data.isDisabled} onChange={data.onChange} />
					)}
					{data.type == "file" && (
						<RFileSuite
							parentCallback={data.handleData}
							placeholder={tr`Replace the old selected file`}
							fileSize={data.fileSize}
							value={data.value}
							fileType={data.typeFile}
							singleFile={data.singleFile}
							binary={data.binary}
							uploadName={data.uploadName}
							setSpecificAttachment={data.setSpecificAttachment}
							theme={data.theme}
							style={data.style}
						/>
					)}

					{data.type == "email" ||
					data.type == "text" ||
					data.type == "number" ||
					data.type == "textarea" ||
					data.type == "password" ||
					// data.type == "checkbox" ||
					data.type == "date" ||
					data.type == "datetime-local" ||
					// data.type == "file" ||
					data.type == "month" ||
					data.type == "radio " ||
					data.type == "range" ||
					data.type == "reset" ||
					data.type == "search" ||
					data.type == "tel" ||
					data.type == "textarea" ||
					data.type == "time" ||
					data.type == "url" ||
					data.type == "week" ? (
						<Input
							value={data.value}
							readOnly={data.readOnly ? true : false}
							type={data.type}
							defaultValue={data.defaultValue ? data.defaultValue : ""}
							name={data.name}
							placeholder={data.label}
							className={styles.input_text}
							id={data.id}
							disabled={data.disabled ? data.disabled : ""}
							onChange={data.onChange}
							onBlur={data.onBlur}
							onKeyUp={data.onKeyUp}
							onCut={data.onCut}
							required={data.required ? data.required : ""}
							min={data.min}
							checked={data.checked}
							maximumCharacterSize={data.maximumCharacterSize ? data.maximumCharacterSize : ""}
							{...validationProps}
							autoComplete={data.autoComplete}
							style={data.style}
						/>
					) : data.type == "tags" ? (
						<RTags
							defaultLabel={data.defaultLabel}
							url={data.url}
							getArrayFromResponse={data.getArrayFromResponse}
							getValueFromArrayItem={data.getValueFromArrayItem}
							getLabelFromArrayItem={data.getLabelFromArrayItem}
							getIdFromArrayItem={data.getIdFromArrayItem}
							setId={data.setId}
							defaultValues={data.defaultValues}
							readOnly={data.readOnly}
							onChange={data.onChange}
							style={data.style}
						/>
					) : data.type == "color" ? (
						<RColor
							value={data.value}
							readOnly={data.readOnly ? true : false}
							defaultValue={data.defaultValue}
							name={data.name}
							className={styles.input_text}
							id={data.id}
							disabled={data.disabled}
							onChange={data.onChange}
							style={data.style}
						/>
					) : data.type == "hidden" ? (
						<Input type={data.type} value={data.value} defaultValue={data.defaultValue ? data.defaultValue : ""} />
					) : data.type == "select" ? (
						data.style?.width ? (
							<div style={{ width: data.style.width }}>
								<RSelect
									name={data.name}
									onChange={data.onChange}
									style={data.style}
									// value={data.value && data.value}
									defaultValue={data.defaultValue}
									option={data.option}
									placeholder={data.placeholder}
									isMulti={data.isMulti}
									isDisabled={data.isDisabled}
									isLoading={data.isLoading}
								/>
							</div>
						) : (
							<RSelect
								name={data.name}
								onChange={data.onChange}
								style={data.style}
								// value={data.value && data.value}
								defaultValue={data.defaultValue}
								option={data.option}
								placeholder={data.placeholder}
								isMulti={data.isMulti}
								isDisabled={data.isDisabled}
								isLoading={data.isLoading}
							/>
						)
					) : data.type === "toggle" ? (
						<RToggle
							defaultValue={data.defaultValue}
							onColor={data.onColor ? data.onColor : "default"}
							offColor={data.offColor ? data.offColor : "default"}
							style={data.style}
						/>
					) : data.type === "tabPanel" ? (
						<RTabsPanel
							title={data.title}
							Tabs={data.tabs}
							SelectedIndex={data.tabIndex}
							changeHorizontalTabs={data.changeHorizontalTabs}
						></RTabsPanel>
					) : null}
					{data.feedBack && (
						<FormFeedback valid={data.feedBack.valid} invalid={!data.feedBack.valid}>
							{data.feedBack.valid ? data.feedBack.validMessage : data.feedBack.invalidMessage}
						</FormFeedback>
					)}
				</div>
			</Col>
		</Row>
	);
}
export default RFormItem;

/*
data: {
  md : gridSysAttr,
  label: formHeader,
  floating: floatingForm,
  className: form ClassName,
  type,
  readOnly,
  defaultValue,
  name,
  label,
  id
  ,disabled,required

}
*/
