import React from "react";
import RBack from "./RBack";
import RBreadCrumb from "./RBreadCrumb";
import RCompassCard from "./RCompassCard";
import { Modal, ModalBody } from "reactstrap";
import { useLocation } from "react-router-dom";
import setCurrentNodeByLink from "store/actions/navigation/setCurrentNodeByLink";
import { useDispatch } from "react-redux";
import withDirection from "hocs/withDirection";
import { handleShouldNotBeAddedModuleContent, handleShouldNotBeAddedLessonContent } from "store/actions/global/coursesManager.action";

const RNavigator = ({ node_id, siteTree, linkPrefix, history, setActiveNode, goBack, dir }) => {
	const [modalOpen, setModalOpen] = React.useState(false);
	const toggleModal = () => setModalOpen(!modalOpen);
	const [canGoBack, setCanGoback] = React.useState(true);

	let _location = useLocation().pathname;
	const dispatch = useDispatch();

	React.useEffect(() => {
		// _location = "/v6.7" + _location;
		let splitted = _location.split("/");

		const a = ["teacher", "senior-teacher", "parent", "student", "admin"];
		let index = splitted.findIndex((sp) => a.indexOf(sp) >= 0);

		splitted = splitted[splitted.length - 1] === "" ? splitted.slice(index + 1, splitted.length - 1) : splitted.slice(index + 1); //splitted[splitted.length-1] === "" ? splitted.length-1 : null)
		const link = "/" + splitted.join("/");
		dispatch(setCurrentNodeByLink(link));
		if (link === "/" || link === "/dashboard") {
			setCanGoback(false);
			return;
		}
		setCanGoback(true);
	}, [_location, siteTree]);

	return (
		<>
			<div className="R-Navigator">
				{canGoBack && (
					<RBack
						dir={dir}
						Label="back"
						onBack={() => {
							goBack();
							dispatch(handleShouldNotBeAddedModuleContent(false));
							dispatch(handleShouldNotBeAddedLessonContent(false));
						}}
					/>
				)}
				{/* 
				<Button className="R-Navigator-btn" onClick={toggleModal}>
					<i className=" nc-icon nc-compass-05  R-Navigator-icon"></i>
				</Button> */}

				{/* <RBreadCrumb node_id={node_id} siteTree={siteTree} linkPrefix={linkPrefix} setActiveNode={setActiveNode} dir={dir} /> */}
			</div>
			<Modal isOpen={modalOpen} toggle={toggleModal}>
				<ModalBody>
					<RCompassCard siteMap={siteTree} setActiveNode={setActiveNode} activeNodeId={node_id} linkPrefix={linkPrefix} dir={dir} />
				</ModalBody>
			</Modal>

			{/*    <CardHeader>
            <RBack Label="back" onBack={()=>{history.goBack; goBack() } } />
            <RBreadCrumb node_id={node_id} siteTree={siteTree} linkPrefix={linkPrefix} setActiveNode={setActiveNode} />
        </CardHeader> */}

			{/* <CardBody>
        <CardText>tree link goes here.</CardText>
    </CardBody> */}
		</>
	);
};

export default withDirection(RNavigator);
