import { Row, Col, Card, CardImg, CardBody, Table, CardTitle } from "reactstrap";
import "./RCard.css";
import styles from "./RCardHorizontal.Module.scss";
import { showOnlyDate } from "utils/HandleDate";
import { showOnlyDay } from "utils/HandleDate";
import { showOnlyTime } from "utils/HandleDate";

const RCardHorizontal = ({ cardInputs }) => {
	return (
		<Row>
			<div
				className={
					cardInputs?.length >= 3
						? styles.table_responsive + " table-full-width table-responsive"
						: styles.table_responsive_small + " table-full-width table-responsive"
				}
			>
				{cardInputs?.map((cardInput) => {
					return (
						<Col md="12" className="mt-2 pl-0">
							<Card
								style={{
									borderLeft: "0 none",
									borderTop: "0 none",
									borderRight: "0 none",
									borderBottom: "solid 1px lightgray",
									boxShadow: "0 0px 0px 0px rgb(0 0 0 / 1%)",
									borderRadius: "0",
								}}
							>
								<CardBody className={styles.card_padding}>
									<Table className={"mb-0 " + styles.line_horizantal}>
										<tbody>
											<tr>
												<td
													style={{
														borderRight: `2px solid ${cardInput?.Color}`,
													}}
													className={"img-row td-padding " + styles.border_top}
												>
													<div className={"img-wrapper " + styles.img_radius}>
														{cardInput?.icons.map((icon) => {
															return <img style={{ width: "100%" }} alt="..." src={icon.src} />;
														})}
													</div>
												</td>
												<td className={"text-left td-padding " + styles.border_top + " " + styles.td_width}>
													<div>
														<p className={"card-category " + styles.font_time}>
															{cardInput?.Titles[0]} , {cardInput?.Titles[1]}
														</p>
														<CardTitle tag="p" className={styles.font_time}>
															{" "}
															{cardInput?.details.map((d) => {
																return <> {d.value}</>;
															})}
														</CardTitle>
														<p />
													</div>
												</td>
												<td className={"text-right " + styles.border_top}>
													<div>
														<p className={"card-category second-color " + styles.font_time}>
															{showOnlyDay(cardInput?.Date)} {showOnlyDate(cardInput?.Date)}
														</p>
														<CardTitle tag="p" className={styles.font_time}>
															{showOnlyTime(cardInput?.Date)}
														</CardTitle>
														<p />
													</div>
												</td>
											</tr>
										</tbody>
									</Table>
								</CardBody>
							</Card>
						</Col>
					);
				})}
			</div>
		</Row>
	);
};
export default RCardHorizontal;
