import React from "react";
import { Row } from "reactstrap";
import loader from "assets/img/new/svg/loader.svg";

function RLoader({ onBack, className, dir, mini }) {
	const n = 18;

	const pages = [];
	for (let i = 0; i < n; i++) {
		pages.push(<li></li>);
	}
	return (
		<Row style={{ width: "100%", justifyContent: "center" }}>
			{/* {<img width={mini ? 50 : 100} height={mini ? 50 : 100} src={loading} alt="loading..." />} */}
			{<img width={mini ? 20 : 100} height={mini ? 20 : 100} src={loader} alt="loader" />}
		</Row>
	);
}
export default RLoader;
