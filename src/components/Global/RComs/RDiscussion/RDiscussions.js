import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { Row, Col, Button } from "reactstrap";
import AddNewDiscussion from "./Discussions/AddNewDiscussion";
import DiscussionSearch from "./Search/DiscussionSearch";
import AllDiscussions from "./Discussions/AllDiscussions";
import withDirection from "hocs/withDirection";
import RButton from "../RButton";
import styles from "./RDiscussion.Module.scss";
import tr from "../RTranslator";
import REmptyData from "components/RComponents/REmptyData";

const RDiscussions = ({ discussions, dir, addVisable }) => {
	const dispatch = useDispatch();
	const history = useHistory();
	const { user } = useSelector((state) => state?.auth);
	return (
		<>
			{discussions.length > 0 ? (
				<Row>
					<Col md={5} xs={12}>
						<h5 className={dir ? styles.text_style_ltr : styles.text_style_rtl}>{tr`discussions`}</h5>
					</Col>

					<Col md={7} xs={12}>
						<div style={{ display: "flex" }}>
							<DiscussionSearch />
							{!addVisable ? (
								<div hidden={addVisable} className="ml-0">
									<AddNewDiscussion />
								</div>
							) : (
								<RButton
									text={tr`back`}
									onClick={() =>
										user?.type === "school_advisor" || user?.type === "seniorteacher" || user?.type === "principal"
											? history.push(`${process.env.REACT_APP_BASE_URL}/senior-teacher/discussions`)
											: user?.type === "employee"
											? history.push(`${process.env.REACT_APP_BASE_URL}/admin/discussions`)
											: user?.type === "teacher"
											? history.push(`${process.env.REACT_APP_BASE_URL}/teacher/discussions`)
											: user?.type === "student"
											? history.push(`${process.env.REACT_APP_BASE_URL}/student/discussions`)
											: user?.type === "parent"
											? history.push(`${process.env.REACT_APP_BASE_URL}/parent/discussions`)
											: "/"
									}
									color="info"
									outline
								/>
							)}
						</div>
					</Col>

					<Col md={10} style={{ margin: "auto" }}>
						{discussions.map((discussion) => (
							<AllDiscussions discussion={discussion} />
						))}
					</Col>
				</Row>
			) : (
				<REmptyData line1={"No Posts for you !"} line2="Add new post and communicate with your colleges" dir="column" />
			)}
		</>
	);
};

export default withDirection(RDiscussions);
