import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getAccessLevels } from "store/actions/global/socialMedia.actions";
import Select from "react-select";
import tr from "components/Global/RComs/RTranslator";

const AccessibilityLevels = (props) => {
	const dispatch = useDispatch();

	const [levelId, setLevelId] = useState(0);
	const [singleSelect, setSingleSelect] = useState(null);
	const [optionsValue, setOptionsValue] = useState([]);

	const { accesslevels, accesslevelsLoading } = useSelector((state) => state.discussionsRed);

	useEffect(() => {
		dispatch(getAccessLevels("discussions"));
	}, []);

	useEffect(() => {
		getAccessLevelData();
	}, []);

	const handleSelected = (value) => {
		setSingleSelect(value);
		props.parentAccessLevelCallback(value.value);
		setLevelId(value.value);
		if (!props.accessID) {
			props.parentCheckedCallback(false);
		}
		dataToUpdate();
	};

	const getAccessLevelData = () => {
		var arrayData = [];
		accesslevels &&
			accesslevels &&
			accesslevels.map((level) => {
				arrayData.push({ value: level.id, label: level.level_name });
				if (level.id == levelId || (level.id == props.accessID && levelId == 0)) {
					setSingleSelect({ value: level.id, label: level.level_name });
				}
			});
		setOptionsValue(arrayData);
	};

	const dataToUpdate = () => {
		return (
			<>
				{accesslevelsLoading ? (
					"LAODING.."
				) : (
					<Select
						className="react-select primary"
						classNamePrefix="react-select"
						name="singleSelect"
						value={singleSelect}
						onChange={(value) => handleSelected(value)}
						options={optionsValue}
						placeholder={tr`accessibility_level`}
					/>
				)}
			</>
		);
	};

	return <span>{dataToUpdate()}</span>;
};

export default AccessibilityLevels;
