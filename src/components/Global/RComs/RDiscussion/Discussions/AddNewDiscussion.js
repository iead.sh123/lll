import React, { useState, useEffect } from "react";
import { Button, FormGroup, Form, Input, Row, Col } from "reactstrap";
import { useSelector, useDispatch } from "react-redux";
import { FetchUserTypesAsync } from "store/actions/admin/schoolEvents";
import { getCurrentDate } from "utils/CurrentDate";
import { addNewPost } from "store/actions/global/socialMedia.actions";
import AccessibilityLevels from "./AccessibilityLevels";
import ImageUpload from "components/CustomUpload/ImageUpload";
import AppModal from "components/Global/ModalCustomize/AppModal";
import RSelect from "components/Global/RComs/RSelect";
import styles from "./AddNewDiscussion.Module.scss";
import tr from "components/Global/RComs/RTranslator";

const AddNewDiscussion = () => {
	const dispatch = useDispatch();
	const { userTypes, userTypesLoading } = useSelector((state) => state.SchoolEventsRed);

	const [postForm, setPostForm] = useState({
		title: "",
		text: "",
		published_at: getCurrentDate(),
		image: null,
		access_level_id: "",
		user_types: null,
	});

	const [checked, setChecked] = useState(false);
	const [emptyfile, setEmptyfile] = useState(false);
	const [show, setShow] = useState(false);
	const [optionsValueUserTypes, setOptionsValueUserTypes] = useState([]);

	const handleClose = () => {
		setShow(false);
	};
	const handleShow = () => {
		setPostForm({
			...postForm,
			title: "",
			text: "",
			accessability_levels_id: "",
			published_at: "",
			image: null,
			user_types: null,
		});
		setShow(true);
	};

	const changeValue = (e) => {
		setPostForm({ ...postForm, [e.target.id]: e.target.value });
	};
	const accessLevelCallback = (childDataAccess) => {
		setPostForm({ ...postForm, access_level_id: childDataAccess });
		dispatch(FetchUserTypesAsync(childDataAccess));
	};
	const CheckedCallback = (childDataChecked) => {
		setChecked(childDataChecked);
	};
	const handleCallback = (childData) => {
		setPostForm({ ...postForm, image: childData });
		setEmptyfile(false);
	};

	const newPost = (e) => {
		if (postForm.access_level_id != "") {
			e.preventDefault();

			var resultObj = {
				title: postForm.title,
				text: postForm.text,
				published_at: getCurrentDate(),
				Attachments: postForm.image,
				access_level_id: postForm.access_level_id,
				user_types: postForm.user_types,
			};
			//HERE WE CALL dispatch function
			// props["addPost"](resultObj);
			dispatch(addNewPost(resultObj));

			setPostForm({ ...postForm, title: "", text: "", image: "" });
			setChecked(false);
			setEmptyfile(true);
			setShow(false);
		} else {
			e.preventDefault();
			setChecked(true);
		}
	};

	//Option UserTypes
	const getOptionsUserTypes = () => {
		const arrayDataUserTypes = [];
		userTypes &&
			userTypes.map((userType) => {
				arrayDataUserTypes.push({
					value: userType.id,
					label: userType.name,
				});
			});

		setOptionsValueUserTypes(arrayDataUserTypes);
	};

	//Handle Select UserTypes
	const handleSelectUserTypes = (e) => {
		const ArraySelected = e;
		let ArraySelectedTwo = [];
		for (let i = 0; i < ArraySelected.length; i++) {
			ArraySelectedTwo.push(ArraySelected[i].value);
		}
		setPostForm({
			...postForm,
			user_types: ArraySelectedTwo,
		});
	};

	useEffect(() => {
		getOptionsUserTypes();
	}, [userTypes]);

	return (
		<Col md={4}>
			<Button outline className={"btn-round " + styles.btn_style} color="info" onClick={handleShow}>
				<i className={"fa fa-plus " + styles.icon} aria-hidden="true"></i>
				{tr`write_new_post`}
			</Button>

			<AppModal
				show={show}
				parentHandleClose={handleClose}
				header={tr("write_new_mpdal_post")}
				classStyle={styles.add_new_post}
				headerSort={
					<Form className=" mt-4" onSubmit={newPost}>
						<Row>
							<Col xs={12}>
								<FormGroup>
									<Input
										id="title"
										type="text"
										required
										placeholder={tr("write_title")}
										name="title"
										onChange={changeValue}
										value={postForm.title}
									/>
								</FormGroup>
							</Col>
							<Col md={6} xs={12}>
								<FormGroup>
									<AccessibilityLevels
										parentAccessLevelCallback={accessLevelCallback}
										parentCheckedCallback={CheckedCallback}
										check={checked}
									/>
								</FormGroup>
							</Col>
							<Col md={6} xs={12}>
								{userTypesLoading ? (
									<h6>Loading ...</h6>
								) : (
									<RSelect
										option={optionsValueUserTypes}
										closeMenuOnSelect={true}
										placeholder={tr`select_user_types`}
										onChange={(e) => handleSelectUserTypes(e)}
										isMulti
									/>
								)}
							</Col>
						</Row>
						<Row>
							<Col xs={12}>
								<FormGroup>
									<textarea
										className="form-control"
										required
										id="text"
										placeholder={tr("write_content")}
										type="text"
										name="text"
										onChange={changeValue}
										value={postForm.text}
									/>
								</FormGroup>
							</Col>
						</Row>
						<Row>
							<Col xs={12}>
								<FormGroup check className="mt-3">
									<ImageUpload parentCallback={handleCallback} emptyFileUpload={emptyfile} id="add" />
								</FormGroup>
							</Col>
						</Row>

						<div>
							<Button type="submit" color="info" style={{ width: 150 }}>
								{tr("post")}
							</Button>
							<Button type="submit" outline className={"btn-round"} style={{ width: 150 }} color="info" onClick={handleClose}>
								{tr("cancel")}
							</Button>
						</div>
					</Form>
				}
			></AppModal>
		</Col>
	);
};

export default AddNewDiscussion;
