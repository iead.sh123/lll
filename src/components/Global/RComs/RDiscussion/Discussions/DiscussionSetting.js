import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCurrentDate } from "utils/CurrentDate";
import AccessibilityLevels from "components/SocialMedia/NewsFeed/AccessibilityLevels";
import ShowImgEditPost from "components/SocialMedia/NewsFeed/ShowImgEditPost/ShowImgEditPost";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsisH } from "@fortawesome/free-solid-svg-icons";
import { deletePost, deleteComment, updateComment } from "store/actions/global/socialMedia.actions";
import { ToastContainer, toast } from "react-toastify";
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, Button, FormGroup, Form, Row, Col } from "reactstrap";
import styles from "./AllDiscussions.Module.scss";
import { socialMediaApi } from "api/socialMedia/index";
import Loader, { SOCIALMEDIA } from "utils/Loader";

import AppModal from "components/Global/ModalCustomize/AppModal";
import RFileUploader from "components/Global/RComs/RFileUploader";
import RSelect from "components/Global/RComs/RSelect";
import { FetchUserTypesAsync } from "store/actions/admin/schoolEvents";
import trl from "../../RTranslator";
import withDirection from "hocs/withDirection";
import iconsFa6 from "variables/iconsFa6";

//array for images upload or added before
let listImgDelete = [];
let listImgAdded = [];
let allImages = [];

const DiscussionSetting = ({ discussionData, comment, showAccess, dir }) => {
	const [show, setShow] = useState(false);
	const [postForm, setPostForm] = useState({
		title: "",
		text: "",
		updated_at: getCurrentDate(),
		image: null,
		accessability_levels_id: "",
		user_types: [],
	});

	const [dropdownOpen, setOpen] = useState(false);
	const [optionsValueUserTypes, setOptionsValueUserTypes] = useState([]);

	const [showUserType, setShowUserType] = useState({ user_types: [] });

	const toggle = () => setOpen(!dropdownOpen);
	//let progressbar = useSelector((state) => state.loadingReducer.progressbar);

	const dispatch = useDispatch();

	const { userTypes, userTypesLoading } = useSelector((state) => state.SchoolEventsRed);

	const successDelete = () => {
		if (comment) {
			dispatch(deleteComment(discussionData.id, discussionData.post_id));
		} else {
			dispatch(deletePost(discussionData.id));
		}
	};

	const handelDeletePost = () => {
		successDelete();
	};

	const handleClose = () => {
		setPostForm({
			...postForm,
			title: "",
			text: "",
			accessability_levels_id: "",
			published_at: "",
			image: [],
			user_types: [],
		});
		listImgDelete.splice(0, listImgDelete.length);
		allImages.splice(0, allImages.length);
		listImgAdded.splice(0, listImgAdded.length);
		//setProgress(0);
		setShow(false);
	};

	//open Modal to edit post
	const handleShow = () => {
		const UserTypes = [];
		const ShowUserTypes = [];

		for (let i = 0; i < discussionData?.user_types?.length; i++) {
			UserTypes.push(discussionData.user_types[i]?.id);
		}

		for (let i = 0; i < discussionData?.user_types?.length; i++) {
			ShowUserTypes.push({
				value: discussionData.user_types[i]?.id,
				label: discussionData.user_types[i]?.name,
			});
		}

		setShowUserType({ user_types: ShowUserTypes });

		setPostForm({
			...postForm,
			title: discussionData.title,
			text: discussionData.content.text,
			accessability_levels_id: discussionData.accessibility_level_id,
			user_types: UserTypes,
		});
		setShow(true);
	};

	const changeValue = (e) => {
		setPostForm({ ...postForm, [e.target.id]: e.target.value });
	};

	//upload new image
	const handleCall = (childData) => {
		setPostForm({ ...postForm, image: childData });
		listImgAdded = [];
		childData.map((item) => {
			listImgAdded.push({
				url: item.url,
				file_name: item.file_name,
				type: item.type,
			});
		});
	};

	//get caccess level id
	const accessLevelCallback = (childDataAccess) => {
		setPostForm({ ...postForm, accessability_levels_id: childDataAccess });
		dispatch(FetchUserTypesAsync(childDataAccess));
	};

	//remove image added befor with post
	const handelRemoveImage = (childId) => {
		listImgDelete.push({ id: childId, to_delete: true });
	};

	//update post
	const handelUpdatePost = (e) => {
		e.preventDefault();
		//update comment
		if (comment) {
			var formUpdate = {
				text: postForm.text,
				published_at: postForm.updated_at,
				Attachments: [],
			};
			dispatch(updateComment(formUpdate, discussionData.id, discussionData.post_id));
			setPostForm({
				...postForm,
				title: "",
				text: "",
				accessability_levels_id: "",
				published_at: "",
				image: [],
				user_types: [],
			});
			setShow(false);
		} else {
			allImages = [...listImgDelete, ...listImgAdded];

			var formsUpdate = {
				title: postForm.title,
				text: postForm.text,
				published_at: postForm.updated_at,
				Attachments: allImages,
				access_level_id: postForm.accessability_levels_id,
				user_types: postForm.user_types,
			};
			socialMediaApi
				.editPost(formsUpdate, discussionData.id)

				.then((response) => {
					if (response.data.status == 1) {
						setPostForm({
							...postForm,
							title: "",
							text: "",
							accessability_levels_id: "",
							published_at: "",
							image: [],
							user_types: [],
						});
						setShow(false);
						listImgDelete.splice(0, listImgDelete.length);
						allImages.splice(0, allImages.length);
						listImgAdded.splice(0, listImgAdded.length);
						dispatch({
							type: "UPDATE_POST",
							authError: null,
							updatepost: response.data.data,
						});
					} else {
						toast.error(response.data.msg);
					}
				})
				.catch((error) => {
					toast.error(error?.message);
				});
		}
	};

	//Option UserTypes
	const getOptionsUserTypes = () => {
		const arrayDataUserTypes = [];
		userTypes &&
			userTypes.map((userType) => {
				arrayDataUserTypes.push({
					value: userType.id,
					label: userType.name,
				});
			});

		setOptionsValueUserTypes(arrayDataUserTypes);
	};

	//Handle Select UserTypes
	const handleSelectUserTypes = (e) => {
		const ArraySelected = e;
		let ArraySelectedTwo = [];
		for (let i = 0; i < ArraySelected.length; i++) {
			ArraySelectedTwo.push(ArraySelected[i].value);
		}
		setPostForm({
			...postForm,
			user_types: ArraySelectedTwo,
		});
	};

	useEffect(() => {
		getOptionsUserTypes();
	}, [userTypes]);

	return (
		<>
			<div className="col-md-12">
				<span hidden={comment} className="text-muted" style={{ fontWeight: "bold" }}>
					<i className="fa fa-eye" style={{ color: "#fd822b" }}></i> {discussionData.accessabilityLevel_name}
				</span>
				&nbsp; &nbsp;
				<ButtonDropdown isOpen={dropdownOpen} toggle={toggle}>
					{showAccess && (
						<DropdownToggle className={styles.btn_h + " btn btn-link"} id="dropdown-basic">
							<FontAwesomeIcon className={styles.icon_color} icon={faEllipsisH} />
						</DropdownToggle>
					)}

					{!showAccess && comment ? (
						<></>
					) : (
						<DropdownMenu aria-haspopup={true} caret color="default" data-toggle="dropdown" className={styles.dropdown_edit}>
							{showAccess && (
								<>
									<DropdownItem>
										<span name="Edit" onClick={handleShow} id="Edit" value="Edit" data-toggle="modal" data-target="#editPostModal">
											<i className="fa fa-pencil fa-lg" style={{ color: "#46C37E" }}></i> &nbsp; <span>{tr`edit`}</span>
										</span>
									</DropdownItem>
									<DropdownItem>
										<span
											onClick={() => {
												handelDeletePost();
											}}
										>
											<i className={iconsFa6.delete + " fa-lg"} style={{ color: "#ef8157" }}></i> &nbsp; <span>{tr`delete`}</span>
										</span>
									</DropdownItem>
								</>
							)}
						</DropdownMenu>
					)}
				</ButtonDropdown>
				<AppModal
					show={show}
					parentHandleClose={handleClose}
					header={comment ? tr`edit_comment` : tr`edit_post`}
					classStyle={styles.edit_new_post}
					headerSort={
						discussionData ? (
							<>
								<Form className=" mt-4" onSubmit={handelUpdatePost}>
									<Row>
										<Col md={12} xs={12}>
											<FormGroup>
												<textarea
													required={comment ? false : true}
													className="form-control"
													rows="1"
													id="title"
													placeholder={tr("write_title")}
													name="title"
													onChange={changeValue}
													value={postForm.title}
													hidden={comment}
												/>
											</FormGroup>
										</Col>
										<Col md={6} xs={12} hidden={comment} style={{ padding: "10px 14px" }}>
											<AccessibilityLevels
												parentAccessLevelCallback={accessLevelCallback}
												accessID={discussionData.accessibility_level_id}
											/>
										</Col>

										<Col md={6} xs={12} hidden={comment} style={{ padding: "10px 14px" }}>
											{userTypesLoading ? (
												<h6>Loading ...</h6>
											) : (
												<RSelect
													option={optionsValueUserTypes}
													closeMenuOnSelect={true}
													placeholder={tr`select_user_types`}
													onChange={(e) => handleSelectUserTypes(e)}
													defaultValue={showUserType.user_types}
													isMulti
												/>
											)}
										</Col>
									</Row>
									<Row>
										<Col xs={12}>
											<FormGroup>
												<textarea
													className="form-control"
													rows="5"
													id="text"
													placeholder={tr("write_content")}
													name="text"
													onChange={changeValue}
													value={postForm.text}
													required
												/>
											</FormGroup>
										</Col>
									</Row>
									<Row>
										<Col xs={12}>
											<ShowImgEditPost
												images={discussionData.content && discussionData.content.resource ? discussionData.content.resource : []}
												parentCallRemoveImage={handelRemoveImage}
											/>
										</Col>
									</Row>

									<Row>
										<Col xs={12}>
											<div className="col-md-3 col-sm-4 p-2" hidden={comment}>
												<RFileUploader parentCallback={handleCall} placeholder={"Replace the old selected file "} />
											</div>
										</Col>
									</Row>

									<div>
										<Button type="submit" color="info" style={{ width: 150 }}>
											{tr("post")}
										</Button>
										<Button type="submit" outline className={"btn-round"} style={{ width: 150 }} color="info" onClick={handleClose}>
											{tr("cancel")}
										</Button>
									</div>
								</Form>
								<ToastContainer
									position="top-center"
									autoClose={5000}
									hideProgressBar
									newestOnTop={false}
									closeOnClick
									rtl={false}
									pauseOnFocusLoss
									draggable
									pauseOnHover
								/>
							</>
						) : (
							Loader(SOCIALMEDIA)
						)
					}
				></AppModal>
			</div>
		</>
	);
};

export default withDirection(DiscussionSetting);
