import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
const RTextIcon = ({ text = "text", icon, iconOnRight = false, className, flexStyle, textStyle, iconStyle, onlyText = false, onClick }) => {
	return (
		<RFlex className={`align-items-center ${className}`} styleProps={{ ...flexStyle }} onClick={onClick}>
			{!iconOnRight && !onlyText && <i className={icon} style={{ ...iconStyle }}></i>}
			<span style={{ ...textStyle }}>{text}</span>
			{iconOnRight && <i className={icon} style={{ ...iconStyle }}></i>}
		</RFlex>
	);
};

export default RTextIcon;
