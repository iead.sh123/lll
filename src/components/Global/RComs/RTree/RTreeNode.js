
import React from 'react';
import { Link } from 'react-router-dom';
import { faChevronCircleRight, faChevronCircleDown } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './RTree.css';

const TreeNode = ({ name/*, marginLevel*/, url, activate, collapse, active, nodeClicked, collapsed,hasChildren }) => {

    // const mL = marginLevel * 35;
    return (
        <>
            {hasChildren && <i class="expanded" onClick={collapse}>
                <FontAwesomeIcon className='Circle-Right-Icon' icon={collapsed ? faChevronCircleDown : faChevronCircleRight} style={active ? { color: 'rgba(253, 131, 44, 1)' } : { color: 'rgba(253, 131, 44, 0.5)' }} /></i>}
            {
                (nodeClicked || url === "#") ?
                    <div style={{ display: "inline" }} onClick={url === "#" ? () => null : (e) => { activate(e); nodeClicked(e); }} >
                        <span className='Tree-Link' style={active ? { backgroundColor: 'rgba(253, 131, 44, 1)', color: 'rgba(255, 255, 255, 1)' } : { color: '#209b03' }}>
                            <i class="collapsed"></i>  {name}

                        </span>
                    </div>
                    :
                    <Link to={url} onClick={activate} replace>
                        <span className='Tree-Link' style={active ? { backgroundColor: 'rgba(253, 131, 44, 1)', color: 'rgba(255, 255, 255, 1)' } : { color: 'rgba(99, 95, 89, 1)' }}>
                            <i class="collapsed"></i>
                            {name}</span>
                    </Link>
            }
        </>
    );
};
export default TreeNode;
