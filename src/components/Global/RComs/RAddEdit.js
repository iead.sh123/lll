import React from "react";
import RForm from "./RForm";
import withDirection from "hocs/withDirection";

function RAddEdit({ oldObject, initialFormItems, onSubmit, title, type, onClick, allowEdit = true, preSetValues, addLoading }) {
	const formItems = [];

	// if (oldObject) {
	// THIS IS the Edit mode of the ADD EDIT THING
	initialFormItems.map((fi) => {
		let new_fi = fi;
		if (preSetValues) {
			let index = -1;
			preSetValues.map((v, i) => {
				if (v.field == fi.name) index = i;
			});

			if (index > -1) {
				if (new_fi.type == "select") {
					const os = new_fi.option.filter((o) => o.value == preSetValues[index].value);
					if (os.length > 0) new_fi.defaultValue = os[0];
				} else {
					new_fi.defaultValue = preSetValues[index].value;
				}

				new_fi.isDisabled = preSetValues[index].disable;
			}
		}
		formItems.push(new_fi);
	});

	return (
		<div>
			{title ? <span className="pb-2">{title}</span> : null}
			<RForm
				formItems={formItems}
				onSubmit={onSubmit}
				submitLabel={oldObject ? "Edit" : "ADD"}
				withSubmit={allowEdit}
				className={"h_btn_inline_14"}
				type={type ? type : "submit"}
				// note: use onClick for change state in RAddEdit Component to control page
				onClick={onClick ? onClick : ""}
				disabled={addLoading ? true : false}
				loading={addLoading}
			/>
		</div>
	);
}
export default withDirection(RAddEdit);
