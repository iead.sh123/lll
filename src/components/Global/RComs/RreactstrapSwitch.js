import React from "react";
import Switch from "react-bootstrap-switch";

const RreactstrapSwitch = ({ defaultValue, offText, onText, onColor, offColor, onChange }) => {
	return <Switch defaultValue={defaultValue} offText={offText} onText={onText} onColor={onColor} offColor={offColor} onChange={onChange} />;
};
export default RreactstrapSwitch;
