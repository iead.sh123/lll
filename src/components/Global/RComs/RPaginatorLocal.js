import React from "react";
import styles from "./Paginator.Module.scss";
import { CardBody, PaginationItem, PaginationLink, Pagination } from "reactstrap";

const RPaginatorLocal = ({ total, currentPage, handleNext, handlePrevious, handleLast, handleFirst }) => {
	return (
		<div className="form-group text-center page-num pr-0 mr-0">
			<CardBody className="text-center pr-0 pl-0">
				<nav aria-label="Page navigation example">
					<Pagination className={styles.pagenation_center}>
						<PaginationItem>
							<PaginationLink aria-label="Previous" onClick={handleFirst}>
								<span aria-hidden={true}>
									<i aria-hidden={true} className="fa fa-angle-double-left font-weight-bold" />
								</span>
							</PaginationLink>
						</PaginationItem>

						<PaginationItem>
							<PaginationLink aria-label="Previous" onClick={handlePrevious}>
								<span aria-hidden={true}>
									<i aria-hidden={true} className="fa fa-angle-left font-weight-bold" />
								</span>
							</PaginationLink>
						</PaginationItem>
						<PaginationItem>
							<PaginationLink className={styles.agenation_font}>
								<span className={styles.current_style}>
									{currentPage} / {total}
								</span>
							</PaginationLink>
						</PaginationItem>

						<PaginationItem>
							<PaginationLink aria-label="Next" onClick={handleNext}>
								<span aria-hidden={true}>
									<i aria-hidden={true} className="fa fa-angle-right font-weight-bold" />
								</span>
							</PaginationLink>
						</PaginationItem>
						<PaginationItem>
							<PaginationLink aria-label="Next" onClick={handleLast}>
								<span aria-hidden={true}>
									<i aria-hidden={true} className="fa fa-angle-double-right font-weight-bold" />
								</span>
							</PaginationLink>
						</PaginationItem>
					</Pagination>
				</nav>
			</CardBody>
		</div>
	);
};

export const usePaginate = (pageSize = 10, arr, resetToFirstPageOnChange = true) => {
	const [currentPage, setCurrentPage] = React.useState(1);

	React.useEffect(() => {
		true && setCurrentPage(1);
	}, [arr.length]);

	const handleNext = () => {
		if (currentPage + 1 <= Math.ceil(arr.length / pageSize)) setCurrentPage(currentPage + 1);
	};

	const handlePrevious = () => {
		if (currentPage - 1 > 0) setCurrentPage(currentPage - 1);
	};

	const handleLast = () => {
		setCurrentPage(Math.ceil(arr.length / pageSize));
	};

	const handleFirst = () => {
		setCurrentPage(1);
	};

	const pItems = arr.slice((currentPage - 1) * pageSize, currentPage * pageSize);

	return {
		pItems,
		currentPage,
		totalPages: Math.ceil(arr.length / pageSize),
		handleNext,
		handlePrevious,
		handleLast,
		handleFirst,
	};
};

export default RPaginatorLocal;
