import React from 'react';
import { useMemo } from 'react';
import { Bar, Pie } from 'react-chartjs-2';
import Helper from './Helper';

const RPieChart = ({ data,color,label ,style}) => {
  const labels = data.map((item) => item.text);
  const counts = data.map((item) => item.count);

  const getRandomColors = (length) => {
    const colors = ['rgb(102, 138, 215)', 'lightgray'];
    const selectedColors = [];

    for (let i = 0; i < length; i++) {
      //const randomIndex = Math.floor(Math.random() * colors.length);
      selectedColors.push(colors[i]);
      
      //selectedColors.push(color);
      //colors.splice(randomIndex, 1);
    }

    return selectedColors;
  };

  const backgroundColors = color??getRandomColors(data.length);

  const chartData ={
    labels: labels,
    datasets: [
      {
        label: label??'Counts',
        data: counts,
        backgroundColor: backgroundColors,
      },
    ],
  } ;
  const options = {
    animation: {
      duration: 0, // Set the duration to 0 to stop animation
    },
  };
  return (
    <div style={style}>
      <Pie data={chartData}  options={options} />
    </div>
  );
};

export default RPieChart;