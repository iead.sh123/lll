import React from "react";
import DefaultCourseImage from "assets/img/new/course-default-cover.png";
import GroupAvatar from "assets/img/new/svg/group_avatar.svg";
import UserAvatar from "assets/img/new/svg/user_avatar.svg";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { Services } from "engine/services";

const RProfileName = ({
	name,
	type = "user", //user || group || course
	img,
	imgStyle,
	className,
	flexStyleProps,
	textStyle,
	onClick,
}) => {
	const defaultImage = type == "user" ? UserAvatar : type == "group" ? GroupAvatar : DefaultCourseImage;
	return (
		<RFlex styleProps={{ ...flexStyleProps }} className={`align-items-center w-max ${className}`} onClick={() => onClick && onClick()}>
			<img
				src={img ? Services.storage.file + img : defaultImage}
				style={{ borderRadius: "100%", width: "30px", height: "30px", ...imgStyle, objectFit: "cover" }}
			/>
			<span style={{ maxWidth: "100px", ...textStyle }}>{name}</span>
		</RFlex>
	);
};

export default RProfileName;
