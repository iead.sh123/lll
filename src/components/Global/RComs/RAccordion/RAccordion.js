import SafeTooltip from "components/Global/SafeTooltip/SafeTooltip";
import withDirection from "hocs/withDirection";
import React from "react";
import {
  Row,
  Col,
  Card,
  CardBody,
  Collapse,
  ListGroup,
  UncontrolledTooltip,
} from "reactstrap";
import idsGenerator from "utils/tooltipIdsGen";
import styles from "./RAccordion.module.css";
// {
//     header:{
//     title: {caption: ,onClick: }
//     headerButtons: [{caption: ,onClick,icon}]
//     },
//     listItems:[
//         item1:{
//             LComponent: IEFunction that returns a pre-rendered component
//             collapsed: ,
//             childrenNodes:[
//                 {
//                     CComponent: IEFunction that returns a pre-rendered component
//                 }
//             ]
//         }
//     ]
// }

const RAccordion = ({ header, listItems, dir }) => {
  // const max=1000000;

  // "Tooltip"+Math.floor(Math.random() * max);
  return (
    <div className={styles["first-list"]}>
      <div
        className={[styles["first-list-title"], styles["header-bg"]].join(" ")}
      >
        {/* <Row className={`${styles["no-padding-no-margin"]}`}>
          <Col md="8" className={" mt-3"}>
            <div className={`float-${dir ? "left" : "right"}`}>
              <p className={header?.headerButtons ? "text-capitalize cursor-pointer" : "text-capitalize"} onClick={header.onClick}>
                {header.caption.toString().length > 25
                  ? header.caption.substring(0, 25) + "..."
                  : header.caption}
              </p>
            </div>
          </Col>
          <Col md="4" className={styles["no-padding-no-margin"]}>
            {header.headerButtons?.map((hb,ind) => {
              const tootipId = 'hb'+ind;//idsGenerator.next().value;

              return <div
                 id={tootipId}
                key={hb.caption}
                className={[styles["title-icon m-2"], styles["tooltipped"]].join(
                  " "
                )}
              >
                <i
                  
                  className={hb.icon + " mt-3 pl-4 ml-3 cursor-pointer "}
                  onClick={hb.onClick}
                />{" "}
                <span>{hb.caption}</span>
                <SafeTooltip
                  className=" pl-5 ml-5"
                  delay={0}
                  target={tootipId}
                >
                  {hb.tooltip}
                </SafeTooltip>
              </div>

            })}
          </Col>
        </Row> */}
      </div>
      <Card>
        <div className={styles["second-list"]}>
          {listItems?.map((it,ind) => (
            <React.Fragment key={"accord-q-"+ind}>
             {it.icon??"no icon"}
             {it.icon && <i className={`mr-2 ${it.icon}`} aria-hidden="true" style={{color: it.color??"black"}}></i>}
              {it.LComponent}
              <Collapse isOpen={true}>{/* {it.collapsed}> */}
                <ListGroup className={styles["third-list"]}>
                  {it.childrenNodes?.map((cn,ind) => <React.Fragment key={"accord-insider-"+ind}>{cn.CComponent}</React.Fragment>)}
                </ListGroup>
              </Collapse>
            </React.Fragment>
          ))}
        </div>
      </Card>
    </div>
  )
};

export default withDirection(RAccordion);
