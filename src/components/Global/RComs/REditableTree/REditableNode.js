import {
  Col,
  Row,
  UncontrolledCollapse,
  UncontrolledTooltip,
} from "reactstrap";
import "./Reditabletree.css";
const REditableNode = ({
  data,
  level,
  activeNodes,
  linkPrefix,
  nodeClicked,
  movable,
}) => {
  //set Active Node
  let active = false;
  for (let i = 0; i < activeNodes?.length; i++) {
    if (activeNodes[i] == data.id) {
      active = true;
    }
  }

  const isLeaf = !data || !data.items || data.items.length == 0;

  return (
    <UncontrolledCollapse
      style={{
        color: `${active ? "#FD832C" : "#fff"}`,
      }}
      toggler="#Toggler1"
      isOpen={true}
    >
      <div className="second-list">
        <div
          style={{ position: "relative", left: `${level * 5}px` }}
          className="second-list-head cursor-pointer "
          onClick={() => nodeClicked(data)}
        >
          <Row className="no-padding-no-margin ">
            {/* {movable ? (
              <Col lg="1" md="1" sm="1" className="no-padding-no-margin">
                <div className="up-down-icons">
                  <div>
                    <button className="p-0 m-0">
                      {" "}
                      <i className="nc-icon nc-minimal-up  p-0 m-0" />{" "}
                    </button>
                  </div>
                  <div>
                    <button className="p-0 m-0">
                      {" "}
                      <i className="nc-icon nc-minimal-down p-0 m-0" />{" "}
                    </button>
                  </div>
                </div>
              </Col>
            ) : null} */}
            {/* Question topic */}
            {data.icon ? (
              <Col
                lg="1"
                md="1"
                sm="1"
                className="no-padding-no-margin cursor-pointer "
              >
                <div className="check-circle-icon ">
                  <i className={`${data.icon} p-0 m-0 cursor-pointer`} />
                </div>
              </Col>
            ) : null}
            {/* Topic And Question Topic */}
            <Col lg="10" md="10" sm="10" className="no-padding-no-margin">
              <div id="Toggler11" style={{ width: "100%" }} className="m-2">
                {!isLeaf ? (
                  <i className="nc-icon nc-minimal-down font-weight-bold text-muted list-icon" />
                ) : null}
                <div
                  id="data-name"
                  style={{
                    display: "inline",
                  }}
                >
                  {" "}
                  <span
                    id={"tooltip4290" + data?.id}
                    style={{ width: "80%" }}
                    className={data.icon ? "ml-4 cursor-pointer" : ""}
                  >
                    {data.name.toString().length > 30
                      ? data.name.substring(0, 30) + "..."
                      : data.name}
                  </span>
                  <UncontrolledTooltip
                    delay={0}
                    target={"tooltip4290" + data?.id}
                  >
                    {data.name}
                  </UncontrolledTooltip>
                </div>
              </div>
            </Col>
            {/* {data.actions?.map((action) => (
              <Col lg="5" md="5" sm="5" className="no-padding-no-margin">
                <div className="title-icon m-2">
                  {action.name}
                  <i className={`${action.icon} ml-3 mr-1`} />
                </div>
              </Col>
            ))} */}
          </Row>
        </div>

        {data?.items?.map((item) => {
          return (
            <REditableNode
              data={item}
              level={level + 1}
              activeNodes={activeNodes}
              linkPrefix={""}
              nodeClicked={nodeClicked}
              movable={movable}
              icon={data.icon}
            ></REditableNode>
          );
        })}
      </div>
    </UncontrolledCollapse>
  );
};
export default REditableNode;
