import React from "react";
import { Col, Row } from "reactstrap";
import REditableNode from "./REditableNode";
import "./Reditabletree.css";
const REditableTree = ({
  data,
  activeNodes,
  linkPrefix,
  nodeClicked,
  movable,
}) => {
  return (
    <Row className="no-padding-no-margin ">
      <Col lg="12" md="12" sm="12" className="no-padding-no-margin ">
        <div className="main-tree">
          <div className="first-list">
            <div id="Toggler1" className={"first-list-title"}>
              <i className="nc-icon nc-minimal-down text-muted font-weight-bold list-icon " />{" "}
              {data.name}
              <div className="title-icon"></div>
            </div>
            {data?.items?.map((item) => {
              return (
                <REditableNode
                  data={item}
                  level={1}
                  activeNodes={activeNodes}
                  linkPrefix={""}
                  nodeClicked={nodeClicked}
                  movable={movable}
                  icon={data.icon}
                ></REditableNode>
              );
            })}
          </div>
        </div>
      </Col>
    </Row>
  );
};
export default REditableTree;
