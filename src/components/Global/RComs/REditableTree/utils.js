import React from "react";
import TreeNode from "./RTreeNode";

export const getActiveNodePath = (activeNodeId, currentNode) => {
	const nodePath = [];

	const _tryPath = (_activeNodeId, _currentNode, path) => {
		if (_activeNodeId == _currentNode.id) {
			return true;
		}

		if (_activeNodeId != _currentNode.id && !_currentNode.items) {
			return false;
		}

		let flag = false;
		if (_currentNode.items.length > 0)
			for (let i = 0; i < _currentNode.items.length; i++) {
				const node = _currentNode.items[i];
				if (_tryPath(_activeNodeId, node, path)) {
					path.push(node.id);
					flag = true;
					break;
				}
			}
		return flag;
	};

	_tryPath(activeNodeId, currentNode, nodePath);
	return nodePath;
};

export const buildTreeRecursively = (entryNode, level, setActiveNode, collapsedNodes, addCollapsedNode, activeNodeId, linkPrefix) => {
	const active = entryNode.id === activeNodeId;
	if (!entryNode.items) {
		return (
			<TreeNode
				active={active}
				activate={() => {
					setActiveNode(entryNode.id);
				}}
				name={entryNode.name}
				marginLevel={level}
				color={entryNode.items ? "#c3b7b7" : "#ddefdd"}
				url={`/${linkPrefix}${entryNode.link}`}
			></TreeNode>
		);
	}

	level++;
	return (
		<div>
			<TreeNode
				active={active}
				collapse={() => addCollapsedNode(entryNode.id)}
				activate={() => {
					setActiveNode(entryNode.id);
				}}
				name={entryNode.name}
				marginLevel={level}
				color={entryNode.items ? "#c3b7b7" : "#ddefdd"}
				url={`/${linkPrefix}${entryNode.link}`}
			></TreeNode>
			{(collapsedNodes.indexOf(entryNode.id) >= 0 || level < 2) && (
				<ul style={{ listStyleType: "none" }}>
					{entryNode.items.map((item) => {
						return (
							<li key={entryNode.id}>
								{buildTreeRecursively(item, level, setActiveNode, collapsedNodes, addCollapsedNode, activeNodeId, linkPrefix)}
							</li>
						);
					})}
				</ul>
			)}
		</div>
	);
};
