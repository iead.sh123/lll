import React from "react";
import styles from "./RCardUser.module.scss";
import RFlex from "../RFlex/RFlex";
import ribbon from "assets/img/new/svg/ribbon.svg";
import book from "assets/img/new/svg/book.svg";
import userFriends from "assets/img/new/svg/user-friends.svg";
import star from "assets/img/new/svg/star.svg";
import Chemistry from "assets/img/new/Chemistry.jpg";
import tr from "../RTranslator";
import { primaryColor } from "config/constants";
import { Row, Col } from "reactstrap";
import { Services } from "engine/services";

const RCardUser = ({ user, viewMode }) => {
	return (
		<div className={styles.user_card}>
			<RFlex styleProps={{ justifyContent: "space-between" }}>
				<p className={styles.about_facilitator_padding}>{tr`About the facilitator`}</p>
				<img className={styles.ribbon_padding} src={ribbon} alt="ribbon" />
			</RFlex>
			<RFlex className={styles.image_padding}>
				<img
					className={styles.image}
					src={user.image_hash_id == undefined ? Chemistry : `${Services.courses_manager.file}${user.image_hash_id}`}
					alt={user.image_hash_id}
				/>
				<h6 className="d-flex align-items-center">{user?.user_name}</h6>
			</RFlex>
			<Row className={styles.about_facilitator_padding}>
				<Col xs={1}>
					<img className={styles.icon} src={book} alt="book" />
				</Col>
				<Col xs={10}>
					<span className="text-muted">
						{user?.courses_count} <span>{tr`courses`}</span>
					</span>
				</Col>
				<Col xs={1}>
					<img className={styles.icon} src={userFriends} alt="userFriends" />
				</Col>
				<Col xs={10}>
					<span className="text-muted">
						{user?.students_count} <span>{tr`students`}</span>
					</span>
				</Col>
				<Col xs={1}>
					<img className={styles.icon} src={star} alt="star" />
				</Col>
				<Col xs={10}>
					<span className="text-muted">
						{user?.stars} <span>{tr`stars`}</span>
					</span>
				</Col>
			</Row>
		</div>
	);
};

export default RCardUser;
