import React, { useState } from 'react';
import { Button, Collapse } from 'reactstrap';

const JsonTree = ({ data,prefix="" }) => {

  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);


  const [isValueOpen, setIsValueOpen] = useState(false);

  const toggleValue = () => setIsValueOpen(!isValueOpen);



  
  const renderTreeNodes = (node, prefix="",key=0,) => {
    
    if (Array.isArray(node)) {
      return (  <div style={{display:"flex",alignItems:"flex-start",alignItems:"flex-start"}}>
        <div style={{display:"flex",alignItems:"flex-start"}} onClick={toggle}>
      <i className="fas fa-plus"> </i>
    </div>
    <Collapse isOpen={isOpen}>
        <ul>
          {node.map((item, index) => (
            <li key={index}>
                <div style={{display:"flex",alignItems:"flex-start"}}>
              <strong>{index}:</strong>
              {typeof item === 'object' ? (
                <JsonTree data={item}  prefix={prefix+"["+index+"]"}/>
              ) : (
                item.toString()
              )}
              </div>
            </li>
          ))}
        </ul>
        </Collapse>
        </div>
      );
    } else if (typeof node === 'object') {
      return (<div style={{display:"flex",alignItems:"flex-start"}}>
        <div style={{display:"flex",alignItems:"flex-start"}} onClick={toggle}>
      <i className="fas fa-plus"> </i>
    </div>
    <Collapse isOpen={isOpen}>
        <ul>
          {node?Object.entries(node).map(([childKey, childValue]) => (
            <li key={childKey}>
                <div style={{display:"flex",alignItems:"flex-start"}}>
              <strong>{childKey}:</strong>
              {typeof childValue === 'object' ? (
                <JsonTree data={childValue} prefix={prefix+"."+childKey}/>
              ) : (<div  style={{display:"flex",alignItems:"flex-start"}}> 
                <span key={key}>{childValue?.toString()}</span>
                <div  style={{display:"flex",alignItems:"flex-start"}} onClick={toggleValue}><i className="fas fa-pen"> </i></div>
    <Collapse  isOpen={isValueOpen}>  {" "+prefix+"."+childKey}      </Collapse>
      </div>
              )}
              </div>
            </li>
          )):<></>}
        </ul>
        </Collapse>
        </div>
      );
    } else {
      return <div>
                <span key={key}>{node?.toString()}</span>
                 <div style={{display:"flex",alignItems:"flex-start"}} onClick={toggle}><i className="fas fa-pen"> </i></div>
    <Collapse isOpen={isOpen}>  {prefix}      </Collapse>
      </div>
    }
  };

  return  <div style={{display:"flex",alignItems:"flex-start"}}>{renderTreeNodes(data,prefix)}</div>;
};

export default JsonTree;