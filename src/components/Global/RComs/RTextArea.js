import React from "react";
import { FormGroup, Input } from "reactstrap";

function RTextArea({ name, value, onChange, placeholder, ClassNames }) {
	return (
		<FormGroup>
			<Input className={ClassNames} type="textarea" value={value} name={name} placeholder={placeholder} onChange={onChange} rows="5" />
		</FormGroup>
	);
}
export default RTextArea;
