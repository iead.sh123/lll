import React from "react";

function RListerItemDetail({ detail, characterCount, dir }) {
  const valueToString = detail.value ? detail.value.toString() : "";
  const cUrl = valueToString.length;

  return (
    <tr>
      <td
        className={`float-${dir ? "left" : "right"} font-weight-bold`}
        hidden={detail.hidden ? detail.hidden : false}
      >
        {detail.key}:
      </td>
      <td
        className={`float-${dir ? "right" : "left"} pr-3`}
        hidden={detail.hidden ? detail.hidden : false}
      >
        {detail.icon && <i className={detail.icon + " cursor-pointer"}></i>}

        {cUrl <= characterCount || typeof detail.value == "object" ? (
          <p
            className={`float-${dir ? "right" : "left"} pr-3`}
            hidden={detail.hidden ? detail.hidden : false}
          >
            {detail.value}
          </p>
        ) : (
          <p
            className={`float-${dir ? "right" : "left"} pr-3`}
            hidden={detail.hidden ? detail.hidden : false}
          >
            {detail.value.substring(0, characterCount)}...
          </p>
        )}
      </td>
    </tr>
  );
}

export default RListerItemDetail;
