import React, { useState } from "react";
import { CardBody, Col, Row, UncontrolledTooltip } from "reactstrap";
import RListerItemDetail from "./RListerItemDetail";
import RListerItemAction from "./RListerItemAction";
import styles from "./AppCard.Module.scss";

function RListerItem({
	record,
	index,
	check,
	initialCheckValue,
	onCheckChange,
	ckeckedItem,
	checkedApprove,
	recordsArr,
	itemToShow,
	marginT,
	marginB,
	marginTStandard,
	marginBStandard,
	characterCount,
	dir,
}) {
	const [style, setStyle] = useState({ display: "none" });

	const valueToString = record.title ? record.title.toString() : "";
	const cUrl = valueToString.length;

	const toggleState = () => {
		if (checked == 0) {
			setChecked(5);
		} else {
			setChecked(0);
		}
		onCheckChange(index, checked);

		// }
	};
	// useEffect(() => {
	//   if (check && check > 0)
	//     record.actions.push({
	//       name: "check",
	//       icon: "fa fa-eye",
	//       onClick: toggleState,
	//     });
	// }, []);

	let icv = initialCheckValue ? initialCheckValue : 10;
	const [checked, setChecked] = useState(0);

	const circleChecked = () => {
		if (check && check > 0) {
			if (ckeckedItem?.length != 0) {
				let indexItem = ckeckedItem?.findIndex((item) => item.id == record.id);
				if (indexItem != -1) {
					return (
						<h6 className={styles.fixedCircle + " " + styles.Checked_check_circle + " "}>
							<i className={" nc-icon nc-check-2 mt-1 ml-1 " + styles.check_circle}></i>
						</h6>
					);
				} else {
					return (
						<h6 className={styles.fixedCircle + " " + (checked > 0 ? styles.Checked_check_circle : "") + " "}>
							<i className={" nc-icon nc-check-2 mt-1 ml-1 " + styles.check_circle}></i>
						</h6>
					);
				}
			} else {
				return (
					<h6 className={styles.fixedCircle}>
						<i className={" nc-icon nc-check-2 mt-1 ml-1 " + styles.check_circle}></i>
					</h6>
				);
			}
		} else {
			return null;
		}
	};
	const circleCheckedOnTitle = () => {
		if (check && check > 0) {
			if (ckeckedItem?.length != 0) {
				let indexItem = ckeckedItem.findIndex((item) => item.id == record.id);
				if (indexItem != -1) {
					return (
						<h6 style={{ marginTop: "-10px" }} className={styles.fixedCircle + " " + styles.Checked_check_circle + " "}>
							<i className={" nc-icon nc-check-2 mt-1 ml-1 " + styles.check_circle}></i>
						</h6>
					);
				}
			}
		} else {
			return null;
		}
	};

	// const checkCircle = () => {
	//   return (
	//     <>
	//       {check && check > 0 ? (
	//         ckeckedItem.length != 0 ? (
	//           <h6
	//             className={
	//               styles.fixedCircle +
	//               " " +
	//               (checked > 0 ? styles.Checked_check_circle : "") +
	//               " "
	//             }
	//           >
	//             <i
	//               className={
	//                 " nc-icon nc-check-2 mt-1 ml-1 " + styles.check_circle
	//               }
	//             ></i>
	//           </h6>
	//         ) : (
	//           <h6
	//             className={
	//               styles.fixedCircle +
	//               " " +
	//               (checked > 0 ? styles.Checked_check_circle : "") +
	//               " "
	//             }
	//           >
	//             <i
	//               className={
	//                 " nc-icon nc-check-2 mt-1 ml-1 " + styles.check_circle
	//               }
	//             ></i>
	//           </h6>
	//         )
	//       ) : null}
	//     </>
	//   );
	// };
	const max = 1000000;
	const tooltipid = "Tooltip" + Math.floor(Math.random() * max) + record?.id;
	return (
		<>
			<Col
				className="content pt-2"
				md={12 / itemToShow}
				sm={12 / itemToShow}
				onMouseEnter={() => {
					setStyle(1);
				}}
				onMouseLeave={() => {
					setStyle(null);
				}}
			>
				<CardBody className={"card bgTree p-0 " + styles.b_card_topic}>
					{style != 1 ? (
						<Row>
							<Col md="2" className={styles.topic_card} hidden={check && check > 0 ? true : false} dir={dir ? "ltr" : "rtl"}>
								<p
									style={{
										margin: "auto",
										padding: "auto",
										position: "absolute",
										height: "100%",
										bottom: "1px",
									}}
									className={"text-white mt-2 ml-1 " + styles.text_direction}
								>
									{cUrl <= characterCount ? record.title : record.title.substring(0, characterCount) + "..."}
								</p>
							</Col>

							<Col
								md={check & (check > 0) ? "12" : "10"}
								className={marginBStandard || marginBStandard ? marginTStandard + "  " + marginBStandard + "  " : "pt-2 pb-2"}
							>
								{circleChecked()}
								<table className={"col-md-12 "}>
									{/*col-md-12 " + styles.line_space */}
									<tbody>
										{record.details?.map((m) => (
											<RListerItemDetail key={Math.random()} detail={m} classes={""} characterCount={characterCount} dir={dir} />
										))}
									</tbody>
								</table>
							</Col>
						</Row>
					) : (
						<>
							<Col md="12" className={styles.topic_card_hover}>
								{circleCheckedOnTitle()}
								<UncontrolledTooltip delay={0} target={tooltipid}>
									{record.allTitle}
								</UncontrolledTooltip>
								<p id={tooltipid} className={"text-white mt-2 font-weight-bold mb-3 text-center "}>
									{cUrl <= 50 ? record.title : record.title.substring(0, characterCount) + "..."}
								</p>
							</Col>

							<Col md="12" className={"card-body " + marginB + "  " + marginT + "  " + styles.padding_style}>
								<Row className={styles.margin_style} style={{ textAlign: "center" }}>
									{record.actions?.map((a, index) => (
										<RListerItemAction
											key={index}
											index={index}
											checkedApprove={checkedApprove}
											action={a}
											classes={""}
											length={record.actions.length}
											recordsArr={recordsArr}
										/>
									))}
								</Row>
							</Col>
						</>
					)}
				</CardBody>

				{/* <RListerItemTitle title={record.title} classes={""}/> */}
			</Col>
		</>
	);
}
export default RListerItem;
