import React from "react";
import { Col, FormGroup, Input, Label } from "reactstrap";
import styles from "./AppCard.Module.scss";

function RListerItemAction({ action, length, recordsArr, index }) {
	return (
		<Col md={12 / length} sm={12 / length} className={length > 4 ? "ml-3 pl-3" : ""}>
			{action?.type == "checkbox" || action?.type == "select" ? (
				action?.type == "select" ? (
					<span style={{ marginLeft: "-3px" }}>
						<select
							disabled={action.disable}
							value={action.value}
							className={styles.custom_select}
							onChange={(e) => {
								action.onClick(e, recordsArr, index);
							}}
						>
							<option value="" disabled={true} selected={true}>
								{action.label}
							</option>
							{action.options?.map((level) => {
								return (
									<option key={level.value} value={level.value}>
										{level.label}
									</option>
								);
							})}
						</select>
					</span>
				) : (
					<p className="checkbox-radios" style={action.style ? { marginLeft: "-25px" } : { marginLeft: "-20px" }}>
						<FormGroup check>
							<Label className={styles.check_label}>
								<Input
									disabled={action.disable == 0 ? false : action.disable}
									type="checkbox"
									onChange={() => {
										action.onClick(recordsArr, action.checked, index);
									}}
									checked={action.checked}
								/>
								<span className="form-check-sign" />
								{action.name}
							</Label>
						</FormGroup>
					</p>
				)
			) : (
				<p
					hidden={action.permissions == false ? true : false}
					style={action.type ? { marginLeft: "-4px", lineHeight: "26px" } : { marginLeft: "0px" }}
				>
					<i
						className={action.icon + " cursor-pointer"}
						onClick={() => {
							action.onClick(recordsArr);
						}}
					></i>
					&nbsp;
					<span
						className="cursor-pointer"
						onClick={() => {
							action.onClick(recordsArr);
						}}
					>
						{action.name}
					</span>
				</p>
			)}
		</Col>
	);
}
export default RListerItemAction;
