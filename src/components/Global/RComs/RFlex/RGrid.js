const RGrid = ({ children, styleProps, ...props }) => {
  return (
    <section
      style={{
        display: "grid",
        gap: "10px",
        ...styleProps,
      }}
      {...props}
    >
      {children}
    </section>
  );
};

export default RGrid;
