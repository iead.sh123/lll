import React from "react";
import ReactMultiSelectCheckboxes from "react-multiselect-checkboxes";
import Select from "react-select";

function RFilterItem({
  Items,
  setSelectedItems,
  placeHolderText,
  isMulti,
  required,
  selected,
}) {
  const handleSelectChanged = (e) => {
    setSelectedItems(e);
  };

  return isMulti ? (
    <Select
      className="react-select primary"
      classNamePrefix="react-select"
      onChange={(e) => {
        handleSelectChanged(e);
      }}
      isMulti={false}
      options={Items ? Items : {}}
      placeholder="placeholder"
      closeMenuOnSelect={true}
      required={required}
      value={selected}
    />
  ) : (
    <ReactMultiSelectCheckboxes
      style={{ color: "red" }}
      onChange={(e) => {
        handleSelectChanged(e);
      }}
      placeholderButtonLabel={placeHolderText}
      options={Items ? Items : []}
      defaultValue={selected ? selected : []}
    />
  );
}
export default RFilterItem;
