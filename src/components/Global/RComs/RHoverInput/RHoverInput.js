import React from "react";
import iconsFa6 from "variables/iconsFa6";
import styles from "./hoverInput.module.scss";
import RFlex from "../RFlex/RFlex";
import { Input } from "reactstrap";
import * as colors from "config/constants";

const RHoverInput = ({
	saved,
	item,
	handleOnBlur,
	inputValue,
	handleInputDown,
	inputIsNotValidate,
	handleOnClick,
	handleInputChange,
	handleEditFunctionality,
	inputPlaceHolder = "your text here",
	inputWidth = "200px",
	extraInfo,
	divClassName = "justify-content-start",
	textCenter = false,
	focusOnInput = true,
	selectOnInput,
	type = "text",
	name = "",
	errorClassname = "",
	isLoading = false,
	inputClicked,
	needToHover = true,
	textClassName,
	icon,
}) => {
	const [isHovering, setIsHovering] = React.useState(false);
	const inputRef = React.useRef(null);
	React.useEffect(() => {
		if (inputRef.current) {
			focusOnInput ? inputRef.current.focus() : "";
			selectOnInput ? inputRef.current.select() : "";
		}
		return () => {};
	}, [saved]);

	return (
		<div
			onMouseEnter={() => setIsHovering(true)}
			onMouseLeave={() => setIsHovering(false)}
			className={`${styles.HoverInput} ${divClassName}`}
			style={{ width: inputWidth }}
		>
			{saved ? (
				<RFlex styleProps={{ gap: 0, alignItems: "center", textAlign: "center" }}>
					{icon && <i className={iconsFa6.starSolid} alt="starSolid" style={{ color: "#FFDE1D" }} />}
					<p
						style={{
							display: "flex",
							flexWrap: "wrap",
							alignContent: "center",
							justifyContent: textCenter ? "center" : "start",
							padding: "7px 10px",
							margin: "0px",
							minWidth: inputWidth,
							height: "30px",
							border: needToHover ? (isHovering ? "1px solid " + colors.strokeColor : "1px solid white") : "",
							borderRadius: "2px",
							// color: needToHover ? (isHovering ? primaryColor : colors.boldGreyColor) : colors.boldGreyColor,
						}}
						className={textClassName + ` ${styles.AnimateBorder}`}
						onClick={(event) => {
							event.stopPropagation();
							handleEditFunctionality && handleEditFunctionality(event, item, extraInfo);
						}}
					>
						<span style={{ color: colors.boldGreyColor }}>{inputValue}</span>
					</p>
				</RFlex>
			) : (
				<div className="position-relative">
					<Input
						style={{ padding: "7px 12px", width: inputWidth }}
						onChange={(event) => handleInputChange(event, item, extraInfo)}
						// onFocus={(event) => handleOnClick(event,item)}
						onClick={inputClicked}
						name={name}
						type={type}
						value={inputValue}
						placeholder={inputPlaceHolder}
						innerRef={inputRef}
						onBlur={(event) => {
							handleOnBlur(event, item, extraInfo);
						}}
						onKeyDown={(event) => {
							if (event.key == "Enter") inputRef.current.blur();
						}}
						className={`${inputIsNotValidate ? "input__error" : ""}  ${errorClassname}`}
					/>
					{inputIsNotValidate && (
						<i className={`${iconsFa6.errorMark} fa-md ${styles.ValidationIcon}`} style={{ color: colors.dangerColor }} />
					)}
					{isLoading && <i className={`${iconsFa6.spinner} fa-md ${styles.LoaderIcon}`} style={{ color: colors.primaryColor }} />}
				</div>
			)}
		</div>
	);
};

export default RHoverInput;
