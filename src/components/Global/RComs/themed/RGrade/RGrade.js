import React from "react";
import withTheme from "hocs/withTheme";
import withDirection from "hocs/withDirection";

const RGrade = (theme) => {
  let themedComponent = React.lazy(() =>
    (async () => {
      try {
        return await import(`./Grade_${theme}`);
      } catch (err) {
        return await import(`./Grade_default`);
      }
    })()
  );
  return themedComponent;
};

export default withDirection(withTheme(RGrade));
