import styles from "./styles/theme1.Module.scss";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircle } from '@fortawesome/free-solid-svg-icons';

const Lesson_theme1 =({
    hasNotification,
    onClick,
    isActive,
    order,
    image,
    subject,
    date,
    dir=true
  
  }) =>
    <div
      className={`col-xs-12 col-sm-6 col-md-4 ${styles["Lesson"]}`}
    // style={{
    //   position: "relative",
    //   marginTop: "2%",
    // }}
    >
      {hasNotification && (
        <span className="notipoint mt-3">
          <FontAwesomeIcon icon={faCircle} style={{ color: "red" }} />
        </span>
      )}
      <div
        onClick={onClick}
        className="bg-color card card-lesson-topic"
        style={{ background: `${isActive ? "#f6aa77" : "white"}` }}
      >
        <div className={dir ? styles["trig"] : styles["trig-right"]}></div>
        <div
          style={dir ? {
            position: "absolute",
            top: "4%",
            left: "2%",
            fontSize: "14px",
            color: "#fff",
            zIndex: "2",
          } : {
            position: "absolute",
            top: "4%",
            right: "2%",
            fontSize: "14px",
            color: "#fff",
            zIndex: "2",
          }}
        >
          {order}
        </div>
        <div
          className="row"
        >
          {dir && <div className="col-md-4 img-part">
            <img
              src={image}
              className="card-img-top"
              alt="..."
            />
          </div>}
  
          <div className="col-md-8  desc-part" style={{ textAlign: dir ? "left" : "right" }}>
            <div
              className="row my-0"
            >
              <div className="col-md-11">
                <div>{subject}</div>
              </div>
              <div className="col-md-11">
                <div className=" text-muted">{date}</div>
              </div>
            </div>
          </div>
          {!dir && <div className="col-md-4 img-part">
            <img
              src={image}
              className="card-img-top"
              alt="..."
            />
          </div>}
        </div>
      </div>
    </div >

export default Lesson_theme1;