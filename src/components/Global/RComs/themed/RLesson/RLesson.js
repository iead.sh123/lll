import React from 'react';
import withTheme from 'hocs/withTheme';
import withDirection from 'hocs/withDirection';

const RLesson = theme => {

    let themedComponent = React.lazy(() =>
        (async () => {
            try {
                return await import(`./Lesson_${theme}`);
            }
            catch (err) {
                return await import(`./Lesson_default`);;
            }
        })()
    )
    return themedComponent;
}



export default /*withDirection(*/withTheme(RLesson)//); //uncomment to apply direction