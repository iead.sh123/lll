import { Card, CardImg, CardBody, CardTitle, CardSubtitle, CardText, Button } from 'reactstrap';
import styles from "./styles/default.Module.scss";

const RCard = ({
  title,
  body,
  actions
}) =>
  <div className={styles["Card"]} >
    <div className={styles["CardBody"]}>
      {title && <>
        <div className={styles["CardHeader"]}>{title}</div>
        <hr />
      </>}
      {body && <> {body()} <hr /> </>}
      {actions?.map(act => <Button {...act}>{act.caption}</Button>)}
    </div>
  </div>

export default RCard;
