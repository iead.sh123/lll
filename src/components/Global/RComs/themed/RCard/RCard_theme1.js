import { Card, CardImg, CardBody, CardTitle, CardSubtitle, CardText, Button } from 'reactstrap';
import styles from "./styles/theme1.Module.scss";

const RCardTheme1 = ({
    title,
    body,
    actions
}) =>
    <Card className={styles["Card"]} >
        <CardBody className={styles["CardBody"]}>
            {title && <>
                <CardTitle className={styles["CardHeader"]}>{title}</CardTitle>
                <hr />
            </>}
            {body && <> {body()} <hr /> </>}
            {actions?.map(act => <Button {...act} >{act.caption}</Button>)}
        </CardBody>
    </Card>
export default RCardTheme1;