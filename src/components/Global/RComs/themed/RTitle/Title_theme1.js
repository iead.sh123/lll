import { Col, Row } from 'reactstrap';
import styles from "./styles/theme1.Module.scss";
import React from 'react';

const TitleTheme1 = ({
    text,
    dir
}) => <h4 className={styles["title-notf"]}>
        {text}
    </h4>
export default TitleTheme1;