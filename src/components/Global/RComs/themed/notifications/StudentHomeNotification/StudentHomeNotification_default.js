import React from "react";
import { Button } from "reactstrap";

const StudentHomeNotification_default = ({
  style,
  content,
  onClick,
  time,
  actions,
  dir,
}) => (
  <div className={`col-md-12 col-xs-12 list list-o`}>
    <div style={style} className={`notificationBody`}>
      <a onClick={onClick}>
        <p className={!dir ? "bubbleNotifar" : "bubbleNotif"}>{content}</p>
        <p className="bubbleInfo">{time}</p>
      </a>
      {actions.map((btn) => (
        <Button key={`hn-${btn.id}`} {...btn} link>
          {btn.caption}
        </Button>
      ))}
    </div>
  </div>
);

export default StudentHomeNotification_default;
