import React from 'react';
import withTheme from 'hocs/withTheme';
import withDirection from 'hocs/withDirection';

const RStudentHomeNotification = theme => {

    let themedComponent = React.lazy(()=>
        (async ()=>{
            try{
                return await import(`./StudentHomeNotification_${theme}`);
            }
            catch(err){
                return await import(`./StudentHomeNotification_default`);;
            }
        })()
    )
    return themedComponent;
}



export default withDirection(withTheme(RStudentHomeNotification));