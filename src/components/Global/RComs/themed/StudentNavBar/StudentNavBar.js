import React from 'react';
import withTheme from 'hocs/withTheme';
import withDirection from 'hocs/withDirection';

const StudentNavBar = theme => {

    let themedComponent = React.lazy(()=>
        (async ()=>{
            try{
                return await import(`./StudentNavBar_${theme}`);
            }
            catch(err){
                return await import(`./StudentNavBar_Defautl`);;
            }
        })()
    )
    return themedComponent;
}



export default withDirection(withTheme(StudentNavBar));