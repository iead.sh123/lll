import styles from "./styles/default.Module.scss";
import React from "react";
import {
  Navbar,
  NavLink,
  NavbarBrand,
  NavbarText,
  NavbarToggler,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownItem,
  Nav,
  Collapse,
  DropdownMenu,
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";

const StudentNavBar_default = ({
  brandHref,
  brandImage,
  handleBack,
  title,
  dropLists,
  customStyle = {},
  notifications,
}) => (
  <section className={`navbar-section`} style={customStyle}>
    <div>
      <Navbar
        color="primary"
        container
        expand
        fixed=""
        dark
        className="pb-4"
        style={{ padding: 0, margin: 0 }}
      >
        <NavbarBrand href={brandHref} className="navBrand">
          <img src={brandImage} className="d-block" />

          <div></div>

          <NavbarText>
            {handleBack ? (
              // <Link to={nav.linkBack}>
              <FontAwesomeIcon
                onClick={handleBack}
                icon={faChevronLeft}
                style={{ color: "#32353B" }}
              />
            ) : (
              // </Link>
              <div className="backPlaceholder"></div>
            )}
          </NavbarText>
        </NavbarBrand>

        <NavbarToggler onClick={() => null} />
        {title && (
          <NavbarBrand className="pageTitle greyNav">
            <div>
              <h4>{title}</h4>
            </div>
          </NavbarBrand>
        )}

        <Collapse
          navbar
          // style={{ margin: "0px", padding: "0px" }}
        >
          <Nav
            className=""
            // style={{ align: "right", margin: "0px", padding: "0px" }}
          >
            {dropLists.map((di, i) => (
              <UncontrolledDropdown
                id="iddd"
                key={"di" + i}
                nav
                style={{ margin: "0px", padding: "0px" }}
              >
                <DropdownToggle
                  nav
                  caret
                  // style={{ margin: "0px", padding: "0px" }}
                >
                  {di.toggleElement}
                </DropdownToggle>
                <DropdownMenu right>
                  {di?.menu.length > 0
                    ? di?.menu.map((it) => (
                        <DropdownItem
                          key={it.id}
                          onClick={it.onClick}
                          style={
                            it.id === di.selectedItemId
                              ? { backgroundColor: "#DDD", color: "#FFF" }
                              : {}
                          }
                        >
                          <i>{it.id}</i>
                        </DropdownItem>
                      ))
                    : di.component}
                </DropdownMenu>
              </UncontrolledDropdown>
            ))}
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  </section>
);

export default StudentNavBar_default;
