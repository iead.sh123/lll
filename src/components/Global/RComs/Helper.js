import { post } from "config/api";
import React from "react";
import Swal from "utils/Alert";
import { SUCCESS } from "utils/Alert";
import tr from "./RTranslator";
import { DANGER } from "utils/Alert";
import { get } from "config/api";
import { put, patch } from "config/api";
import JsonTree from "./JsonTree";
import { toast } from "react-toastify";

//import Snackbar from 'react-native-snackbar';
class Helper {
	static consoleObjects = [];
	static debug = true;
	static IN = false;
	static cl(s, label = "") {
		//if you don't want to use it just set debug to false .
		if (!Helper.debug) return;
		if (label !== "") {
			if (typeof s != "object") {
				console.log(label + " = " + s);
				return;
			} else {
				console.log(label);
				console.log(s);
			}
		} else {
			console.log(s);
		}
	}

	static fastPost = async (url, payload, successText, failText, success = () => {}, fail = () => {}, showSwal = true) => {
		let response1 = await post(url, payload);
		if (response1 && response1.data && response1.data.status == 1) {
			if (success) success(response1.data);
		} else {
			if (fail) fail();
			if (showSwal) toast.error(tr(failText));
		}
		//-------------------------------
	};
	static fastPut = async (url, payload, successText, failText, success = () => {}, fail = () => {}) => {
		let response1 = await put(url, payload);
		if (response1 && response1.data && response1.data.status == 1) {
			if (success) success(response1.data);
		} else {
			if (fail) fail();
			toast.error(tr(failText));
		}
		//-------------------------------
	};
	static fastPatch = async (url, payload, successText, failText, success = () => {}, fail = () => {}) => {
		let response1 = await patch(url, payload);
		if (response1 && response1.data && response1.data.status == 1) {
			if (success) success(response1.data);
		} else {
			if (fail) fail();
			toast.error(tr(failText));
		}
		//-------------------------------
	};
	//   import { useState } from 'react';
	// import { useEffect } from 'react';
	// import Helper from 'components/Global/RComs/Helper';
	// import { Services } from 'engine/services';

	// const [data,setData]=useState([]);
	// useEffect (()=>{
	//   const get_it=async()=>{ await Helper.fastGet(Services.collaboration.backend+"api/sharable-statistics","fail to get",(response)=>{setData(response.data?.data)},()=>{})}
	//   get_it();
	//   },[])

	static fastGet = async (url, failText, success = () => {}, fail = () => {}) => {
		let response1 = await get(url);
		if (response1 && response1.data && response1.data.status == 1) {
			if (success) success(response1);
		} else {
			if (fail) fail();
			//toast.error(tr(failText));
		}
		//-------------------------------
	};
	static js(s, label = "") {
		//if you don't want to use it just set debug to false .
		if (!Helper.debug) return;
		if (label !== "") {
			if (typeof s != "object") {
				return label + " = " + s;
			} else {
				return label + " = " + JSON.stringify(s);
			}
		} else {
			if (typeof s != "object") {
				return s;
			} else {
				return JSON.stringify(s);
			}
		}
	}

	static jstree(s, label = "") {
		//if you don't want to use it just set debug to false .
		if (!Helper.debug) return;
		if (label !== "") {
			if (typeof s != "object") {
				return label + " = " + JSON.stringify(s);
			} else {
				//return <></>;
				return (
					<div>
						{label}
						<div
							style={{
								zIndex: 10000000000,
								position: "fixed",
								border: "yellow 2px solid",
								left: "300px",
								top: "200px",
								background: "white",
								overflow: "shown",
								width: "fit-content",
								height: "fit-content",
							}}
						>
							{" "}
							<JsonTree data={s} />
						</div>
					</div>
				);
			}
		} else {
			//return <></>;
			return (
				<div
					style={{
						position: "fixed",
						border: "yellow 2px solid",
						left: "300px",
						top: "200px",
						background: "white",
						overflow: "shown",
						width: "fit-content",
						height: "fit-content",
						zIndex: 100000,
					}}
				>
					{" "}
					<JsonTree data={s} />
				</div>
			);
			// return JSON.stringify(s);
		}
	}

	static clp(s, label = "") {
		if (!Helper.debug) return;

		if (label !== "") {
			let typeooo = typeof s;

			if (typeof s != "object") {
				return;
			} else this.consoleObjects?.push({ name: label, data: s });
		}
	}
	static clf(s, label = "") {
		if (!Helper.debug) return;

		if (label !== "") {
			let typeooo = typeof s;
			if (typeof s != "object") {
				return;
			} else "";
		}

		("");
	}

	static cljs(s, label = "") {
		if (label !== "") {
			let typeooo = typeof s;
			if (typeof s != "object") {
				return;
			} else "";
		}

		("");
	}

	static m0(s, label = "") {
		Helper.cl("0", "m");
	}
	static m1(s, label = "") {
		Helper.cl("1", "m");
	}

	static m2(s, label = "") {
		Helper.cl("2", "m");
	}

	static m3(s, label = "") {
		Helper.cl("3", "m");
	}

	static m4(s, label = "") {
		Helper.cl("4", "m");
	}

	static m5(s, label = "") {
		Helper.cl("5", "m");
	}

	static m6(s, label = "") {
		Helper.cl("6", "m");
	}

	static m7(s, label = "") {
		Helper.cl("7", "m");
	}

	static m8(s, label = "") {
		Helper.cl("8", "m");
	}

	static m9(s, label = "") {
		Helper.cl("9", "m");
	}
}

export default Helper;
