import React, { useState } from "react";
import { Link } from "react-router-dom";
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import "./BreadCrumb.css";
import tr from "./RTranslator";

const RBreadCrumb = ({ node_id, siteTree, linkPrefix, setActiveNode, dir }) => {
  const [activeCollapsable, setActiveCollapsable] = useState(null);

  //-----------------------get the path of nodes

  const getPath = (currentNode, nid) => {
    if (nid == currentNode?.id) {
      const arr = [
        {
          id: currentNode.id,
          name: currentNode.name + " ",
          link: currentNode.link,
        },
      ];
      return arr;
    } else {
      let chosen = null;
      currentNode?.items?.map((n) => {
        const path = getPath(n, nid);
        if (path) {
          {
            //-------------------add sblings to last node
            path[path.length - 1].sblings = [];
            currentNode?.items?.map((ss) => {
              if (ss.id != path[path.length - 1].id)
                path[path.length - 1].sblings.push(ss);
            });
            //-----------------------------
            path.push({
              id: currentNode.id,
              name: currentNode.name + "",
              link: currentNode.link,
            });
            chosen = path;
            return;
          }
        }
      });

      if (chosen) {
        return chosen;
      } else {
        return null;
      }
    }
  };

  let nodes = getPath(siteTree, node_id);

  nodes = (dir ? nodes.reverse() : nodes); //reverse if ltr

  return (
    <>
      {nodes && nodes.length > 0
        ? nodes.map((node, index) => {
          return (
            <RBreadCrumbItem
              linkPrefix={linkPrefix}
              key={node.name + index}
              node={node}
              nodes={node.sblings}
              setActiveNode={setActiveNode}
              activeCollapsable={activeCollapsable}
              setActiveCollapsable={setActiveCollapsable}
            />
          );
        })
        : null}
    </>
  );
};

const RBreadCrumbItem = ({
  linkPrefix,
  node,
  nodes,
  setActiveNode
}) => {
  return (
    <>
      <UncontrolledDropdown group className="uncontrolled">
        {node.id == 0 ? (
          <DropdownToggle className="Dropdown-Toggle"></DropdownToggle>
        ) : (
          <DropdownToggle caret className="Dropdown-Toggle"></DropdownToggle>
        )}

        <DropdownMenu className="Dropdown-Menu">
        <>  {nodes?.filter(nd => nd.link !== "#").map((node1, index) => {
            return (
              <div key={"sib" + index + node1.link}>
                <DropdownItem className="Dropdown-Item">
                  <RBreadCrumbLink
                    name={node1.name}
                    link={`${linkPrefix}${node1.link}`}
                    activate={() => setActiveNode(node1.id)}
                    inCollapse
                  ></RBreadCrumbLink>
                </DropdownItem>
              </div>
            );
          })}</>
        </DropdownMenu>
      </UncontrolledDropdown>

      <RBreadCrumbLink
        id={node.id}
        name={node.name}
        link={node.link === "#" ? "#" : linkPrefix + node.link}
        activate={() => setActiveNode(node.id)}
      ></RBreadCrumbLink>
    </>
  );
};

const RBreadCrumbLink = ({ name, link, activate, inCollapse }) => {
const rname=tr(name);
return <div className={`R-Bread-Crumb-Link ${false ? "" : "bc-tooltipped"}`} 
style={{ color: '#209b03' }}>
  {link === "#" ? <>{rname} </> : <Link to={link} onClick={activate} replace>
    {rname.length > 25 ? rname.substring(0, 25) + "..." : rname}
     <i
      className="bc-tooltip"
    >
      {rname}
    </i>
    
  </Link>}
</div>
}

export default RBreadCrumb;
