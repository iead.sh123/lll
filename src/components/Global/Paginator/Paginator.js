import styles from "./Paginator.Module.scss";
const { CardBody, PaginationItem, PaginationLink, Pagination } = require("reactstrap");

const Paginator = ({ firstPageUrl, lastPageUrl, currentPage, lastPage, prevPageUrl, nextPageUrl, getData }) => {
	const handleFirstClicked = () => {
		getData(firstPageUrl);
	};

	const handleLastClicked = () => {
		getData(lastPageUrl);
	};

	const handlePrevClicked = () => {
		if (currentPage == 1) getData(lastPageUrl);
		else getData(prevPageUrl);
	};

	const handleNextClicked = () => {
		if (currentPage == lastPage) getData(firstPageUrl);
		else getData(nextPageUrl);
	};
	return (
		<div className="form-group text-center page-num" style={{ direction: "ltr" }}>
			<CardBody className="text-center">
				<nav aria-label="Page navigation example">
					<Pagination className={styles.pagenation_center}>
						<PaginationItem>
							<PaginationLink
								aria-label="Previous"
								onClick={() => {
									handleFirstClicked();
								}}
							>
								<span aria-hidden={true}>
									<i aria-hidden={true} className="fa fa-angle-double-left font-weight-bold" />
								</span>
							</PaginationLink>
						</PaginationItem>
						<PaginationItem>
							<PaginationLink
								aria-label="Previous"
								onClick={() => {
									handlePrevClicked();
								}}
							>
								<span aria-hidden={true}>
									<i aria-hidden={true} className="fa fa-angle-left font-weight-bold" />
								</span>
							</PaginationLink>
						</PaginationItem>
						<PaginationItem>
							<PaginationLink className={styles.agenation_font}>
								<span className={styles.current_style}>{currentPage}</span> {"  "} / {lastPage}
							</PaginationLink>
						</PaginationItem>
						<PaginationItem>
							<PaginationLink
								aria-label="Next"
								onClick={() => {
									handleNextClicked();
								}}
							>
								<strong aria-hidden={true}>
									<i aria-hidden={true} className="fa fa-angle-right font-weight-bold" />
								</strong>
							</PaginationLink>
						</PaginationItem>
						<PaginationItem>
							<PaginationLink
								aria-label="Next"
								onClick={() => {
									handleLastClicked();
								}}
							>
								<span aria-hidden={true}>
									<i aria-hidden={true} className="fa fa-angle-double-right font-weight-bold" />
								</span>
							</PaginationLink>
						</PaginationItem>
					</Pagination>
				</nav>
			</CardBody>
		</div>
	);
};
export default Paginator;
