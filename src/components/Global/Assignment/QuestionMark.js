import React, { useState, useEffect } from "react";
import { Input } from "reactstrap";
import { translate } from "../../../utils/translate";
import { TYPES } from "./assignmentReducer";

const QuestionMark = ({
  question,
  student,
  mode,
  questionsMarks = [],
  questionsMultiMarks = [],
  dispatch,
}) => {
  const [questionMark, setQuestionMark] = useState(0);

  useEffect(() => {
    if (mode === "student") {
      setQuestionMark(question.corrected_mark);
      return;
    }

    if (mode === "multi") {
      if (questionsMultiMarks.length > 0) {
        let targetQuestion = questionsMultiMarks.filter((q) => {
          return q.questionId === question.id && q.studentId === student[0];
        })[0];
        if (targetQuestion !== undefined) {
          setQuestionMark(targetQuestion.Mark);
          return;
        }
      }
      setQuestionMark(student[1].corrected_mark);
      return;
    }
    //check if the question is already corrected
    if (questionsMarks.length > 0) {
      let targetQuestion = questionsMarks.filter((q) => {
        return q.QuestionId == question.id;
      })[0];
      if (targetQuestion !== undefined) {
        setQuestionMark(targetQuestion.Mark);
        return;
      }
    }

    setQuestionMark(question.corrected_mark);
  }, [question.id]);

  useEffect(() => {
    if (mode === "multi") {
      dispatch({
        type: TYPES.SAVE_MULTI_QUESTION_MARK,
        payload: {
          questionId: question.id,
          Mark: questionMark,
          studentId: student[0],
          studentQuestionPatternId: student[1].student_qs_pattern_id,
        },
      });

      dispatch({
        type: TYPES.CHANGE_MULTI_TOTAL_MARKS,
        payload: {
          questionId: question.id,
          student: student[1],
          questionMark,
          studentId: student[0],
        },
      });
      return;
    }

    dispatch({
      type: TYPES.CHANGE_TOTAL_MARK,
      payload: {
        question,
        questionMark,
      },
    });

    dispatch({
      type: TYPES.SAVE_SINGLE_QUESTION__MARK,
      payload: {
        QuestionId: question.id,
        Mark: questionMark,
      },
    });
  }, [questionMark, question.id]);

  const changeQuestionMarkHandler = (e) => {
    setQuestionMark(+e.target.value);
  };

  return (
    <>
      {mode !== "multi" && (
        <div className="col-md-10 mt-4">
          <div className=" row">
            <label className="col-md-3 pt-2  gu-assignment_label21 ">
              {translate("Question mark")}
            </label>
            <p className=" gu-assignment_mark ml-3 shadow_number  p-2 ">
              {" "}
              {question.mark.toFixed(2)}
            </p>
          </div>
        </div>
      )}
      <div className="col-md-10 mt-2">
        <div className=" form-group row">
          <label className="col-sm-3 pt-2 gu-assignment_label21">
            {mode === "student"
              ? translate(" Your mark")
              : translate("Student Mark")}
          </label>
          <div className="col-sm-9">
            <Input
              value={questionMark || 0}
              type="number"
              className="form-group"
              id="group"
              disabled={mode === "student"}
              onChange={
                mode === "teacher" || mode === "multi"
                  ? changeQuestionMarkHandler
                  : undefined
              }
              min={0}
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default QuestionMark;
