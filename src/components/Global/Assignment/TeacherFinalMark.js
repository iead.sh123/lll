import React from 'react';
import { TYPES } from './assignmentReducer';
import { translate } from '../../../utils/translate';

const TeacherFinalMark = ({ dispatch, TeacherMark, mode, studentId, teacherMultiMarks }) => {
    const changeTeacherFinalMarkHandler = (e) => {
        if (mode === 'multi') {
            dispatch({
                type: TYPES.SAVE_MULTI_FINAL_TEACHER_MARK,
                payload: { finalMark: +e.target.value, studentId }
            });
            return;
        }
        dispatch({ type: TYPES.SAVE_FINAL_TEACHER_MARK, payload: +e.target.value });
    };

    return (
        <div className="form-group row topic-area m-3" /* style={{ marginTop: '0.5rem' }} */>
            <label htmlFor="topic" className="col-sm-2 col-form-label">
                {translate('Final Mark')}
            </label>
            <div className="col-sm-8">
                <input
                    value={
                        mode === 'multi'
                            ? teacherMultiMarks.filter((m) => m.studentId === studentId)[0]
                                  ?.finalMark
                            : TeacherMark
                    }
                    type="number"
                    className="form-control"
                    id="group"
                    onChange={changeTeacherFinalMarkHandler}
                    min={0}
                />
            </div>
        </div>
    );
};

export default TeacherFinalMark;
