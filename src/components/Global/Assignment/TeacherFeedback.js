import React, { memo, useEffect, useState } from 'react';
import { translate } from '../../../utils/translate';
import { TYPES } from './assignmentReducer';

const TeacherFeedback = ({
    feedback,
    mode,
    dispatch,
    teacherFeedback,
    multiFeedbacks,
    studentId
}) => {
    const [currentFeedBack, setCurrentFeedBack] = useState('');
    const changeFeedbackInputHandler = (e) => {
        setCurrentFeedBack(e.target.value);
        if (mode === 'multi') {
            dispatch({
                type: TYPES.SAVE_MULTI_TEACHER_FEEDBACK,
                payload: { feedback: e.target.value, studentId }
            });
            return;
        }
        dispatch({ type: TYPES.SAVE_TEACHER_FEEDBACK, payload: e.target.value });
    };

    useEffect(() => {
        let f = '';
        if (mode === 'multi') {
            f = multiFeedbacks.filter((f) => f.studentId === studentId)[0]?.feedback;
            setCurrentFeedBack(f);
            return;
        }

        if (feedback && feedback.length > 0) {
            setCurrentFeedBack(feedback);
            return;
        }
        setCurrentFeedBack(teacherFeedback);
    }, []);
    return (
        <div className="form-group row topic-area mt-3" >
            <label htmlFor="teacherFeedback " className=" col-sm-3 gu-assignment_label2 gu-assignment_feed mt-4">
                {translate('Teacher Feedback')}
            </label>
            <div className="col-sm-9">
                <textarea
                    value={currentFeedBack}
                    type="text"
                    className="form-control"
                    id="teacherFeedback"
                    disabled={mode === 'student'}
                    onChange={
                        mode === 'teacher' || mode === 'multi'
                            ? changeFeedbackInputHandler
                            : undefined
                    }
                />
            </div>
            <div className="col-sm-10"></div>
        </div>
    );
};

export default memo(TeacherFeedback);
