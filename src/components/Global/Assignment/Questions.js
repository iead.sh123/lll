import React from "react";
import { resourcesBaseUrl } from "config/constants";
import QuestionMark from "./QuestionMark";
import DrawQuestion from "./QuestionType/DrawQuestion";
import FreeTextQuestion from "./QuestionType/FreeTextQuestion";
import MatchDragQuestion from "./QuestionType/MatchQuestion/MatchQuestionDrag";
import MatchQuestion from "./QuestionType/MatchQuestion/MatchQuestion";
import MultiAnswerQuestion from "./QuestionType/MultiAnswerQuestion";
import SingleAnswerQuestion from "./QuestionType/SingleAnswerQuestion";
import UploadFileQuestion from "./QuestionType/UploadFileQuestion";
import MathJax from "react-mathjax-preview";
import ImageEnlarge from "utils/ImageEnlarge";
import { translate } from "../../../utils/translate";

export const QUESTION_TYPE = {
  SINGLE_ANSWER_MC: "single_answer_mc",
  MULTIPLE_ANSWER_MC: "multiple_answer_mc",
  FREE_TEXT: "free_text",
  MATCH: "match",
  DRAW: "draw",
  UPLOAD_FILE: "upload_file",
};
function toOrder(num) {
  if (num == 1) return "1st";
  if (num == 2) return "2nd";
  if (num == 3) return "3rd";
  if (num > 3) return num + "th";
}
function Counter(num, singular) {
  if (num == 1) return num + " " + singular;
  else return num + " " + singular + "s";
}
const Questions = ({
  assignmentState,
  assignmentReadMode,
  groupId,
  question,
  dispatch,
  mode,
  questionsMarks,
  currentActiveQuestion,
  topicLength,
}) => {
  const questionProps = {
    answers: assignmentState.Details,
    choices: question?.inst_choices,
    question: question,
    groupId: groupId,
    dispatch: dispatch,
    assignmentReadMode: assignmentReadMode,
    mode: mode,
  };

  const questionTypeSwitch = (question) => {
    switch (question?.question_type) {
      case QUESTION_TYPE.SINGLE_ANSWER_MC:
        return <SingleAnswerQuestion {...questionProps} />;
      case QUESTION_TYPE.MULTIPLE_ANSWER_MC:
        return <MultiAnswerQuestion {...questionProps} />;
      case QUESTION_TYPE.FREE_TEXT:
        return <FreeTextQuestion {...questionProps} />;
      case QUESTION_TYPE.MATCH:
        return assignmentReadMode || mode === "teacher" ? (
          <MatchQuestion {...questionProps} />
        ) : (
          <MatchDragQuestion {...questionProps} />
        );
      case QUESTION_TYPE.DRAW:
        return <DrawQuestion {...questionProps} />;
      case QUESTION_TYPE.UPLOAD_FILE:
        return <UploadFileQuestion {...questionProps} />;
      default:
        return <p>None</p>;
    }
  };

  return (
    <div
      className="row p-0 mt-3 d-flex  justify-content-left pl-3
      gu-assignment_question_container"
    >
      <div className="assign-topic-text col-11">
        <div className="row">
          <label
            className=" gu-assignment_label gu-assignment_question
           "
          >
            {translate("Question")}
          </label>
        </div>
        <div className=" row p-0">
          <div className="col-md-8 col-sm-8">
            {question?.question_text &&
            question?.question_text.includes("<math") ? (
              <MathJax math={question?.question_text} />
            ) : (
              <>
                <p
                  className="gu-assignment_question_text  pt-1  pl-3"
                  /*  className="assignment_question_text" */
                  dangerouslySetInnerHTML={{ __html: question?.question_text }}
                />
              </>
            )}
          </div>

          <div className="col-md-4 col-sm-4">
            <p
              className="QS-p gu-assignment_group_questionCounter
            qs-counter"
            >
              {toOrder(currentActiveQuestion + 1)} /{" "}
              {Counter(topicLength, "Question")}
            </p>
          </div>
          {question?.attachments && (
            <div className="col-sm-10" style={{ textAlign: "center" }}>
              {["png", "jpeg", "jpg"].includes(
                question?.attachments.mime_type
              ) ? (
                <ImageEnlarge
                  imageSrc={resourcesBaseUrl + question?.attachments.url}
                />
              ) : (
                <a
                  url={resourcesBaseUrl + question?.attachments.url}
                  alt="Download"
                  class="btn btn-primary"
                >
                  {translate("Download")}
                </a>
              )}
            </div>
          )}
        </div>
      </div>

      <div className="col-md-10" style={{ marginTop: "-2rem" }}>
        <div className="row">
          <label className=" gu-assignment_labe-answer">
            {translate("Answer")}
          </label>
        </div>
        <div className="row">{questionTypeSwitch(question)}</div>
      </div>
      {assignmentReadMode && (
        <QuestionMark
          question={question}
          mode={mode}
          questionsMarks={questionsMarks}
          dispatch={dispatch}
        />
      )}
    </div>
  );
};

export default Questions;
