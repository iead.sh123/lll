import { useEffect, useReducer } from "react";
import { useSelector } from "react-redux";
import Helper from "../RComs/Helper";
import {
  assignmentInitialState,
  assignmentReducer,
  TYPES,
} from "./assignmentReducer";

export const useAssignment = (mode, rabId) => {
  const { lessonGroups, isLoader, sebBrowserViolation } = useSelector(
    (state) => state.studentquestionSets
  );

  const [assignmentState, dispatch] = useReducer(
    assignmentReducer,
    assignmentInitialState
  );

  const { currentActiveGroup, currentActiveTopic, currentActiveQuestion } =
    assignmentState;

  useEffect(() => {
    if (lessonGroups && lessonGroups?.inst_pattern_groups) {
      let studentsTotalMarks = [];
      if (mode === "multi") {
        studentsTotalMarks = Object.entries(lessonGroups?.feedbacks).map(
          (studentFeedback) => {
            return {
              studentId: studentFeedback[0],
              totalMark: studentFeedback[1].total_mark,
            };
          }
        );
      }
      dispatch({
        type: TYPES.INITIALIZE_ASSIGNMENT,
        payload: {
          rabId,
          studentQsPatternId: lessonGroups?.student_qs_pattern_id,
          skip: lessonGroups?.inst_pattern_groups[currentActiveGroup].skip,
          isDone: lessonGroups?.is_done,
          totalMark: lessonGroups?.total_mark,
          teacherMark: lessonGroups?.teacher_mark,
          studentsTotalMarks,
        },
      });
    }
  }, [lessonGroups]);

  useEffect(() => {
    if (
      lessonGroups &&
      lessonGroups?.inst_pattern_groups &&
      lessonGroups?.inst_pattern_groups[currentActiveGroup] !== undefined
    ) {
      if (lessonGroups?.inst_pattern_groups[currentActiveGroup].skip > 0)
        dispatch({
          type: TYPES.TOGGLE_SKIP_BUTTON,
          payload: {
            currentSkip:
              lessonGroups?.inst_pattern_groups[currentActiveGroup].skip,
            currentGroupSkip:
              lessonGroups?.inst_pattern_groups[currentActiveGroup].skip,
            currentGroupId:
              lessonGroups?.inst_pattern_groups[currentActiveGroup].id,
          },
        });
    }
  }, [currentActiveGroup, lessonGroups]);

  useEffect(() => {
    if (
      currentActiveQuestion > 0 ||
      currentActiveGroup > 0 ||
      currentActiveTopic > 0
    ) {
      dispatch({ type: TYPES.TOGGLE_PREVIOUS_BUTTON, payload: false });
    }
  }, [currentActiveQuestion, currentActiveTopic]);

  useEffect(() => {
    if (
      lessonGroups !== undefined &&
      lessonGroups?.inst_pattern_groups !== undefined &&
      lessonGroups?.inst_pattern_groups[currentActiveGroup] !== undefined
    ) {
      dispatch({
        type: TYPES.TOGGLE_UN_SKIP_BUTTON,
        payload: {
          currentTopicId:
            lessonGroups?.inst_pattern_groups[currentActiveGroup].inst_topics[
              currentActiveTopic
            ].id,
        },
      });
    }
  }, [lessonGroups, currentActiveTopic]);

  useEffect(() => {
    if (
      currentActiveQuestion > 0 ||
      currentActiveGroup > 0 ||
      currentActiveTopic > 0
    ) {
      dispatch({ type: TYPES.TOGGLE_PREVIOUS_BUTTON, payload: false });
    }
  }, [currentActiveTopic]);

  return {
    lessonGroups,
    isLoader,
    sebBrowserViolation,
    dispatch,
    assignmentState,
    rabId,
  };
};
