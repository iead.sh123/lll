import React from "react";
import { Draggable, Droppable } from "react-beautiful-dnd";
import ImageEnlarge from "utils/ImageEnlarge";
import { resourcesBaseUrl } from "config/constants";
import { translate } from "../../../../../utils/translate";

const LeftColumn = ({ leftColumns }) => {
	const chooseBackgroundColor = (choiceId) => {
		let chosenColor;
		if (assignmentReadMode) {
			chosenColor = choicesColorRef.current.filter((choiceColor) => choiceColor.id === choiceId)[0];
		} else {
			chosenColor = choicesColor.filter((choiceColor) => choiceColor.id === choiceId)[0];
		}
		return chosenColor ? chosenColor.color : "";
	};

	return (
		<div className="col-md-6">
			<div
				style={{
					display: "flex",
					flexDirection: "column",
					alignItems: "center",
				}}
				key={32}
			>
				<h2>{translate("Drag")}</h2>
				<div style={{ margin: 8 }}> </div>

				<Droppable droppableId="left-column" key={1}>
					{(provided, snapshot) => {
						return (
							<div
								{...provided.droppableProps}
								ref={provided.innerRef}
								style={{
									background: snapshot.isDraggingOver ? "lightblue" : "lightgrey",
									padding: 4,
									width: 250,
									minHeight: 500,
								}}
							>
								{leftColumns.items &&
									leftColumns.items.map((leftChoice, index) => {
										return (
											<Draggable key={leftChoice.id} draggableId={leftChoice.id.toString()} index={index}>
												{(provided, snapshot) => {
													return (
														<div
															ref={provided.innerRef}
															{...provided.draggableProps}
															{...provided.dragHandleProps}
															style={{
																userSelect: "none",
																padding: 16,
																margin: "0 0 8px 0",
																minHeight: "50px",
																backgroundColor: snapshot.isDragging ? "#263B4A" : "#456C86",
																color: "white",
																...provided.draggableProps.style,
															}}
														>
															{leftChoice.choice && leftChoice.choice.includes("<math") ? (
																<MathJax math={leftChoice.choice} />
															) : (
																<div
																	dangerouslySetInnerHTML={{
																		__html: leftChoice.choice,
																	}}
																/>
															)}
															{leftChoice.attachments && (
																<div className="col-sm-10">
																	{["png", "jpeg"].includes(leftChoice.attachments.mime_type) ? (
																		<ImageEnlarge
																			// imageStyle={{
																			//     outline: `${chooseBackgroundColor(
																			//         leftChoice.id
																			//     )} solid 10px`
																			// }}
																			imageSrc={resourcesBaseUrl + leftChoice.attachments.url}
																		/>
																	) : (
																		<a href={resourcesBaseUrl + leftChoice.attachments.url} alt="" class="btn btn-primary">
																			{translate("Download")}
																		</a>
																	)}
																</div>
															)}
														</div>
													);
												}}
											</Draggable>
										);
									})}
								{provided.placeholder}
							</div>
						);
					}}
				</Droppable>
			</div>
		</div>
	);
};

export default LeftColumn;
