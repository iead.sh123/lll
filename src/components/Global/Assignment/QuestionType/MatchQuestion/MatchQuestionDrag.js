import React, { useEffect, useRef, useState } from 'react';
import { getRandomColor, shuffleArray } from '../../../../../utils/helper';
import { TYPES } from '../../assignmentReducer';
import { DragDropContext } from 'react-beautiful-dnd';
import LeftColumn from './LeftColumn';
import RightColumn from './RightColumn';
const MatchQuestionDrag = ({
    answers,
    choices,
    question,
    groupId,
    dispatch,
    assignmentReadMode,
    mode
}) => {
    const [selectedChoices, setSelectedChoices] = useState([]);
    const [leftColumns, setLeftColumns] = useState({});
    const [rightColumns, setRightColumns] = useState({});
    const choicesLeftRef = useRef([]);
    const choicesRightRef = useRef([]);

    useEffect(() => {
        choices.map((choice, index) => {
            if (index % 2 === 0) {
                choicesLeftRef.current.push(choice);
            } else {
                choicesRightRef.current.push(choice);
            }
        });

        choicesLeftRef.current = shuffleArray(choicesLeftRef.current);
        setLeftColumns({ ...leftColumns, items: choicesLeftRef.current,    answers,
            choices,
            question,
            groupId,
            dispatch,
            assignmentReadMode });

        choicesRightRef.current = shuffleArray(choicesRightRef.current);
        let rightColumnsValue = {};

        choicesRightRef.current.map((rChoice) => {
            rightColumnsValue[rChoice.id] = { items: [], fixedItem: rChoice };
        });
        setRightColumns(rightColumnsValue);

        if (assignmentReadMode) {
            if (question.notSolved === 1) {
                setSelectedChoices([]);
                return;
            }
            setSelectedChoices(question.student_choices.map((choice) => choice.id));

            return;
        }
        if (answers && answers.length > 0) {
            let answer = answers.filter((answer) => answer.QuestionId === question.id)[0];
            if (answer !== undefined) {
                setSelectedChoices([...answer.Choices]);
                setLeftColumns({ ...answer.leftColumns });
                setRightColumns({ ...answer.rightColumns });
            }
        }
    }, []);

    useEffect(() => {
        if (assignmentReadMode) return;
        dispatch({
            type: TYPES.SAVE_ANSWER,
            payload: {
                QuestionId: question.id,
                GroupId: groupId,
                Choices: selectedChoices,
                FreeAnswer: null,
                rightColumns,
                leftColumns
            }
        });
    }, [selectedChoices]);

    const onDragEnd = (result) => {
        if (!result.destination) return;
        const { source, destination } = result;

        if (
            destination.droppableId !== 'left-column' &&
            rightColumns[destination.droppableId].items.length === 1
        )
            return;

        if (source.droppableId === 'left-column') {
            const destColumn = rightColumns[destination.droppableId];
            const sourceItems = [...leftColumns.items];

            const destItems = [...destColumn.items];
            const [removed] = sourceItems.splice(source.index, 1);
            destItems.splice(destination.index, 0, removed);
            setLeftColumns({
                ...leftColumns,

                items: leftColumns.items.filter((item) => item.id.toString() !== result.draggableId)
            });
            setRightColumns({
                ...rightColumns,
                [destination.droppableId]: {
                    ...destColumn,
                    items: destItems
                }
            });

            setSelectedChoices([...selectedChoices, destColumn.fixedItem.id, removed.id]);
            return;
        }

        if (destination.droppableId === 'left-column') {
            const sourceColumn = rightColumns[source.droppableId];
            const sourceItems = [...sourceColumn.items];
            const destItems = [...leftColumns.items];
            const [removed] = sourceItems.splice(source.index, 1);
            destItems.splice(destination.index, 0, removed);

            setRightColumns({
                ...rightColumns,
                [source.droppableId]: {
                    ...sourceColumn,
                    items: sourceItems
                }
            });

            setLeftColumns({
                ...leftColumns,

                items: destItems
            });
            const isChoiceSelected = selectedChoices.findIndex(
                (choice) => choice === +source.droppableId
            );

            if (isChoiceSelected !== -1) {
                setSelectedChoices([
                    ...selectedChoices.slice(0, isChoiceSelected),
                    ...selectedChoices.slice(isChoiceSelected + 2)
                ]);
            }
        }
    };

    return (
        <div className="col-md-12">
            <div className="row">
                <DragDropContext onDragEnd={(result) => onDragEnd(result)}>
                    <LeftColumn leftColumns={leftColumns} />
                    <RightColumn rightColumns={rightColumns} />
                </DragDropContext>
            </div>
        </div>
    );
};

export default MatchQuestionDrag;
