import ReactMultiSelectCheckboxes from "react-multiselect-checkboxes";
import tr from "../RComs/RTranslator";

const MainCourseFilter = ({ mainCourses, setSelectedMainCourses, selectedMainCourses }) => {
	const handleSelectChanged = (e) => {
		setSelectedMainCourses(e);
	};

	return (
		<ReactMultiSelectCheckboxes
			style={{ color: "red" }}
			onChange={(e) => {
				handleSelectChanged(e);
			}}
			placeholderButtonLabel={tr`main_curricula`}
			options={mainCourses ? mainCourses : []}
			defaultValue={selectedMainCourses ? selectedMainCourses : []}
		/>
	);
};

export default MainCourseFilter;
