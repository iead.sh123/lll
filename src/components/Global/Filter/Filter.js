import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown, faFilter } from "@fortawesome/free-solid-svg-icons";
import { Form, Input, Collapse, FormGroup, Button, Col, Row } from "reactstrap";
import TagsFilter from "./TagsFilter";
import SchoolClassFilter from "./SchoolClassFilter";
import MainCourseFilter from "./MainCourseFilter";
import tr from "../RComs/RTranslator";
import withDirection from "hocs/withDirection";

const Filter = ({
	title,
	mainCourses,
	schoolClasses,
	use_tags,
	tags,
	handleApplyFilter,
	setSelectedMainCourses,
	setSelectedSchoolClasses,
	setSelectedTags,
	selectedMainCourses,
	selectedSchoolClasses,
	dir,
}) => {
	const [openedCollapses, setOpenedCollapses] = useState(false);

	const collapsesToggle = () => {
		setOpenedCollapses(!openedCollapses);
	};

	return (
		<Col md="12" style={{ top: "-18px" }}>
			<Row>
				<Col md="5" className="pt-4">
					{" "}
					<strong>{title}</strong>
				</Col>
				<Col md="3"> </Col>
				<Col md="4" className="float-right">
					<Button
						color="info"
						outline
						data-parent="#accordion"
						data-toggle="collapse"
						onClick={() => collapsesToggle(1)}
						style={{ textTransform: "capitalize" }}
					>
						<FontAwesomeIcon icon={faFilter} />
						<span className="text-dark"> {tr`filter_by`}</span> <FontAwesomeIcon icon={faChevronDown} />
					</Button>
				</Col>
			</Row>
			<Collapse isOpen={openedCollapses}>
				<Form>
					<Row>
						<Col md="3">
							<FormGroup>
								<label style={{ display: dir ? "" : "flex" }}>{tr`main_curricula`}</label>
								<MainCourseFilter
									mainCourses={mainCourses}
									setSelectedMainCourses={setSelectedMainCourses}
									selectedMainCourses={selectedMainCourses}
								/>
							</FormGroup>
						</Col>
						<Col md="3">
							<label style={{ display: dir ? "" : "flex" }}>{tr`school_classes`}</label>
							<FormGroup>
								<SchoolClassFilter
									schoolClasses={schoolClasses}
									setSelectedSchoolClasses={setSelectedSchoolClasses}
									selectedSchoolClasses={selectedSchoolClasses}
								/>
							</FormGroup>
						</Col>

						<Col md="3">
							<label style={{ display: dir ? "" : "flex" }}>{tr`tags`}</label>
							<FormGroup>
								<TagsFilter tags={tags} setSelectedTags={setSelectedTags} />
							</FormGroup>
						</Col>
						<Col md="3">
							<label></label>
							<FormGroup>
								{" "}
								<Button
									className={"btn-round ml-4 mb-1 mt-2  "}
									color="info"
									onClick={handleApplyFilter}
									id={"applyFilter"}
									style={{ textTransform: "capitalize" }}
								>
									{tr`apply`}
								</Button>
							</FormGroup>
						</Col>
					</Row>
				</Form>
			</Collapse>
		</Col>
	);
};
export default withDirection(Filter);
