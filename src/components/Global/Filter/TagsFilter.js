import RTags from "../RComs/RTags";

const TagsFilter = ({ tags, setSelectedTags }) => {
	const handleSelectChanged = (e) => {
		setSelectedTags(e);
	};
	return (
		<RTags
			defaultLabel="User Name"
			url="api/tags/getPrefixTags?prefix="
			getArrayFromResponse={(res) => (res && res.data && res.data.status ? res.data.data : null)}
			getValueFromArrayItem={(i) => i.id}
			getLabelFromArrayItem={(i) => i.name}
			getIdFromArrayItem={(i) => i.id}
			// onChange={onSelectedUsersChange}
		/>
	);
};

export default TagsFilter;
