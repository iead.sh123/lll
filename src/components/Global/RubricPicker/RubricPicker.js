import Paginator from "../Paginator/Paginator";
import { Button } from "reactstrap";
import RLister from "../RComs/RLister";
import styles from "./RubricPicker.Module.scss";
import Loader from "utils/Loader";
const RubricPicker = ({
	table_name,
	title,
	Records,
	ListerClass,
	ItemClass,
	check,
	ckeckedItem,
	onCheckChange,
	firstPageUrl,
	lastPageUrl,
	currentPage,
	lastPage,
	prevPageUrl,
	nextPageUrl,
	total,
	getData,
	parentHandleClose,
}) => {
	return (
		<div>
			{Records ? (
				Records.length == 0 ? (
					<div className="alert alert-danger text-center second-bg-color" role="alert">
						No items to show
					</div>
				) : (
					<>
						<RLister
							Records={Records}
							ListerClass={ListerClass}
							ItemClass={ItemClass}
							check={check}
							ckeckedItem={ckeckedItem}
							onCheckChange={onCheckChange}
							itemToShow={2}
							characterCount={10}
							showListerMode="tableLister"
						/>
						<Paginator
							firstPageUrl={firstPageUrl}
							lastPageUrl={lastPageUrl}
							currentPage={currentPage}
							lastPage={lastPage}
							prevPageUrl={prevPageUrl}
							nextPageUrl={nextPageUrl}
							total={total}
							getData={getData}
						/>
					</>
				)
			) : (
				Loader()
			)}
		</div>
	);
};

export default RubricPicker;
