import React from "react";

import ScrollContainer from "react-indiana-drag-scroll";

const Slider = ({ children, col, additionalStyle, additionalItemStyle }) => {
  // let width= 100/children.length;

  return (
    <ScrollContainer
      id="slider"
      vertical={false}
      className="scroll-container"
      style={Object.assign({}, additionalStyle, {
        display: "inline-block",
        width: "100%",
        height: "100%",
        whiteSpace: "nowrap",
        overflow: "hidden",
      })}
    >
      {children
        ? children.map((x, i) => {
            let borderRight1 = "5px rgb(242, 242, 242) solid";
            if (i == children.length - 1) borderRight1 = "";
            return (
              <div
                id="SliderItem"
                class={col ? "col-" + col : ""}
                style={Object.assign({}, additionalItemStyle, {
                  paddingRight: "0",
                  paddingLeft: "0",
                  display: "inline-block",
                  borderRight: `${borderRight1}`,
                })}
              >
                {x}
              </div>
            );
          })
        : null}
    </ScrollContainer>
  );
};
export default Slider;
