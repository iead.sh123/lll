import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import tr from "components/Global/RComs/RTranslator";
import { lessonPlanApi } from "api/teacher/lessonPlan";
import ReactBSAlert from "react-bootstrap-sweetalert";
import RubricPicker from "components/Global/RubricPicker/RubricPicker";
import { toast } from "react-toastify";

const CallRubricsPicker = ({ table_name, title, courseId, parentHandleClose, parentHandlePickRubric, messageConfirm }) => {
	const [records, setRecords] = useState([]);
	const [firstPageUrl, setFirstPageUrl] = useState(null);
	const [lastPageUrl, setLastPageUrl] = useState(null);
	const [nextPageUrl, setNextPageUrl] = useState(null);
	const [prevPageUrl, setPrevPageUrl] = useState(null);
	const [currentPage, setCurrentPage] = useState(null);
	const [lastPage, setLastPage] = useState(null);
	const [total, setTotal] = useState(null);
	const [ckeckedItem, setCkeckedItem] = useState([]);
	const [alert, setAlert] = useState(null);

	const dispatch = useDispatch();
	const pickedItems = useSelector((state) => state.lessonPlan.chooseItems);
	// const { courseId } = useParams();

	const getData = (url) => {
		if (!url) {
			url = `api/rubrics/get-course-rubrics/${courseId}`;
		}
		lessonPlanApi
			.getCourseRubrics(url)
			.then((response) => {
				handleResponse(response);
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};

	useEffect(() => {
		getData();
	}, []);

	useEffect(() => {
		setCkeckedItem(pickedItems);
	}, [pickedItems]);

	const handleResponse = (response) => {
		if (response.data.status === 1) {
			setCurrentPage(response.data.data.data.current_page);
			setLastPage(response.data.data.data.last_page);
			setFirstPageUrl(response.data.data.data.first_page_url);
			setLastPageUrl(response.data.data.data.last_page_url);
			setNextPageUrl(response.data.data.data.next_page_url);
			setPrevPageUrl(response.data.data.data.prev_page_url);
			setTotal(response.data.data.data.total);
			createArrayLister(response.data.data.data.data);
		} else {
			toast.error(response.data.msg);
		}
	};

	const hideAlert = () => {
		setAlert(null);
	};
	const successPickLessonPlan = (item) => {
		hideAlert();
		parentHandleClose();
		parentHandlePickRubric(item);
	};
	const chooseLessonPlan = (item) => {
		setAlert(
			<ReactBSAlert
				warning
				title={messageConfirm ? messageConfirm : "add this rubric  ?"}
				onConfirm={() => successPickLessonPlan(item)}
				onCancel={() => hideAlert()}
				confirmBtnBsStyle="info"
				cancelBtnBsStyle="danger"
				confirmBtnText="Ok, pick this rubric"
				cancelBtnText="Cancel"
				showCancel
				btnSize=""
			></ReactBSAlert>
		);
	};

	const createArrayLister = (allRecord) => {
		let processsedRecords = [];
		if (table_name === "lesson_plan_items_rubric") {
			allRecord.map((item) => {
				let record = {};
				record.id = item.id;
				record.title = item.title;
				record.initialCheckValue = true;
				record.details = [
					{ key: tr("title"), value: item.title },
					{ key: tr("creator"), value: item.creator.full_name },
					{ key: tr("logo"), value: item.school.name },
				];
				record.actions = [
					{
						name: tr("Choose"),
						icon: "fa fa-check-circle",
						onClick: () => {
							chooseLessonPlan(item);
						},
					},

					{
						name: tr("Explore"),
						icon: "fa fa-eye",
						onClick: () => {},
					},
				];

				processsedRecords.push(record);
			});
			setRecords(processsedRecords);
		}
	};

	return (
		<div className="">
			{alert}
			<RubricPicker
				table_name={table_name}
				title={title}
				Records={records}
				check={1}
				ckeckedItem={ckeckedItem}
				firstPageUrl={firstPageUrl}
				lastPageUrl={lastPageUrl}
				currentPage={currentPage}
				lastPage={lastPage}
				prevPageUrl={prevPageUrl}
				nextPageUrl={nextPageUrl}
				total={total}
				getData={getData}
				parentHandleClose={parentHandleClose}
			/>
		</div>
	);
};

export default CallRubricsPicker;
