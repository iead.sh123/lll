import React from "react";
import GCoursesFilters from "logic/landing/AllCourses/GCoursesFilters";
import withDirection from "hocs/withDirection";
import Notification from "./notification";
import GuidanceLogo from "assets/img/new/guidance.png";
import GHomeSearch from "logic/landing/Home/GHomeSearch";
import CartDrawer from "./cart/CartDrawer";
import classnames from "classnames";
import RButton from "components/Global/RComs/RButton";
import Setting from "./setting";
import styles from "./Navbar.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import Chat from "./chat";
import tr from "components/Global/RComs/RTranslator";
import { Collapse, Navbar, Nav, Container } from "reactstrap";
import { online } from "engine/config";

function LandingNavbar({
	notificationsTotalRecord,
	notificationsLoading,
	notifications,
	activeNodeId,
	chatsNumber,
	siteTree,
	color,
	dir,
	history,
	user,
	collapseOpen,
	imCollapseOpen,
	postsCollapseOpen,
	divRef,
	handleRedirectToDashboard,
	handleCloseIMCollapse,
	handleMarkChatsAsSeen,
	handleSelectedChatId,
	handlePushToAuthPage,
	togglePostsCollapse,
	toggleIMCollapse,
	toggleCollapse,
	toggleSidebar,
}) {
	return (
		<>
			<Navbar id={"refScroll"} className={classnames("navbar-absolute fixed-top", color)} expand="lg">
				<Container fluid>
					<div className={styles.navbar__wrapper}>
						<div className={styles.navbar__logo}>
							<img src={GuidanceLogo} className={styles.guidance__logo} alt="GuidanceLogo" />
							{(location.pathname.includes("/landing/all-courses") || location.pathname.includes("/landing/home")) && user && (
								<h6 className={styles.navbar__back} onClick={() => handleRedirectToDashboard()}>{tr`back_to_dashboard`}</h6>
							)}
						</div>
						{location.pathname.includes("/landing/all-courses") && <GCoursesFilters />}
						{location.pathname.includes("/landing/home") && <GHomeSearch />}

						<button
							aria-controls="navigation-index"
							aria-expanded={collapseOpen}
							aria-label="Toggle navigation"
							className="navbar-toggler nt2"
							data-toggle="collapse"
							type="button"
							onClick={toggleCollapse}
						>
							<span className="navbar-toggler-bar navbar-kebab" />
							<span className="navbar-toggler-bar navbar-kebab" />
							<span className="navbar-toggler-bar navbar-kebab" />
						</button>
					</div>
					<Collapse className="justify-content-end " navbar isOpen={collapseOpen}>
						<Nav navbar className="align-items-center">
							{!online ? <CartDrawer /> : ""}
							{user && (
								<Notification
									togglePostsCollapse={togglePostsCollapse}
									notifications={notifications}
									notificationsTotalRecord={notificationsTotalRecord}
								/>
							)}
							{user && (
								<Chat
									handleCloseIMCollapse={handleCloseIMCollapse}
									handleMarkChatsAsSeen={handleMarkChatsAsSeen}
									handleSelectedChatId={handleSelectedChatId}
									toggleIMCollapse={toggleIMCollapse}
									chatsNumber={chatsNumber}
									imCollapseOpen={imCollapseOpen}
									dir={dir}
								/>
							)}
							<Setting />

							{!user && (
								<RFlex>
									<RButton
										className={styles.auth__buttons}
										text={tr`register`}
										color="primary"
										onClick={() => handlePushToAuthPage("register")}
									/>
									<RButton
										className={styles.auth__buttons}
										text={tr`login`}
										color="primary"
										outline
										onClick={() => handlePushToAuthPage("login")}
									/>
								</RFlex>
							)}
						</Nav>
					</Collapse>
				</Container>
			</Navbar>
		</>
	);
}

export default withDirection(LandingNavbar);
