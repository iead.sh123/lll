import React from "react";
import NotificationListenToChannel from "components/Global/Notifications/NotificationListenToChannel";
import setCurrentNode from "store/actions/navigation/setCurrentNode";
import withDirection from "hocs/withDirection";
import Notification from "./notification";
import RNavigator from "components/Global/RComs/RNavigator";
import classnames from "classnames";
import CartDrawer from "./cart/CartDrawer";
import Setting from "./setting";
import Chat from "./chat";
import { Collapse, NavbarBrand, Navbar, Nav, Container, UncontrolledDropdown, NavItem, NavLink } from "reactstrap";
import { getUserTypePrefix } from "store/reducers/global/NavigationReducer";
import { online } from "engine/config";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import SearchBox from "./search/SearchBox";
import SearchResultBox from "./search/SearchResultBox";
import { removeApisNotWorkingFromLocale } from "engine/config";

function DashboardNavbar({ data, loading, handlers }) {
	return (
		<>
			<Navbar id={"refScroll"} className={classnames("navbar-absolute fixed-top", data.color)} expand="lg">
				<Container fluid>
					{/* <div className="navbar-wrapper"> */}
					{/* <div className="navbar-minimize">
							<Button className="btn-icon btn-round" color="default" id="minimizeSidebar" onClick={handlers.handleMiniClick}>
								<i className="fa fa-bars text-center visible-on-sidebar-mini" />
								<i className="nc-icon nc-simple-remove text-center visible-on-sidebar-regular" />
							</Button>
						</div> */}

					<div
						className={classnames("navbar-toggle", {
							toggled: data.sidebarOpen,
						})}
					>
						<button className="navbar-toggler nt1" type="button" onClick={handlers.handleToggleSidebar}>
							<span className="navbar-toggler-bar bar1" />
							<span className="navbar-toggler-bar bar2" />
							<span className="navbar-toggler-bar bar3" />
						</button>
					</div>

					{/* <NavbarBrand href="#" onClick={(e) => e.preventDefault()}>
							<RNavigator
								node_id={data.activeNodeId}
								siteTree={data.siteTree}
								userType={data.user?.type}
								linkPrefix={`${process.env.REACT_APP_BASE_URL}/${getUserTypePrefix(data.user?.type)}`}
								history={handlers.history}
								setActiveNode={(_nodeId) => dispatch(setCurrentNode(_nodeId))}
								goBack={() => handlers.history.goBack()}
							/>
						</NavbarBrand> */}
					{/* </div> */}

					{/* to show three dots dropdown when small devices */}
					{/* <button
						aria-controls="navigation-index"
						aria-expanded={data.collapseOpen}
						aria-label="Toggle navigation"
						className="navbar-toggler nt2"
						data-toggle="collapse"
						type="button"
						onClick={handlers.handleToggleCollapse
						<span className="navbar-toggler-bar navbar-kebab" />
						<span className="navbar-toggler-bar navbar-kebab" />
						<span className="navbar-toggler-bar navbar-kebab" />
					</button> */}

					<Collapse className="justify-content-end" navbar isOpen={data.collapseOpen}>
						{!online || removeApisNotWorkingFromLocale ? (
							<RFlex styleProps={{ justifyContent: "center", flex: "1 1 0" }}>
								<SearchBox
									setSearch={handlers.handelSetSearchQuery}
									handleOpenSearchResult={handlers.handleOpenSearchResult}
									handleCloseSearchResult={handlers.handleCloseSearchResult}
								>
									{data.searchQuery && data.openSearchResult && <SearchResultBox info={data.searchData} />}
								</SearchBox>
							</RFlex>
						) : (
							""
						)}

						{!removeApisNotWorkingFromLocale && (
							<Nav navbar>
								{!online ? <CartDrawer /> : ""}
								<Notification
									togglePostsCollapse={handlers.handleTogglePostsCollapse}
									notifications={data.notifications}
									notificationsTotalRecord={data.notificationsTotalRecord}
								/>
								<Chat
									handleCloseIMCollapse={handlers.handleCloseIMCollapse}
									handleMarkChatsAsSeen={handlers.handleMarkChatsAsSeen}
									handleSelectedChatId={handlers.handleSelectedChatId}
									toggleIMCollapse={handlers.handleToggleIMCollapse}
									chatsNumber={data.chatsNumber}
									imCollapseOpen={data.imCollapseOpen}
								/>
								<Setting />
							</Nav>
						)}
					</Collapse>
				</Container>
				{/* <NewNote /> */}
				<Collapse style={{ position: "absolute", right: "180px", top: "50px" }} isOpen={data.postsCollapseOpen} innerRef={data.divRef}>
					<UncontrolledDropdown className="btn-magnify" nav>
						<NotificationListenToChannel
							notificationList={data.notifications}
							notificationsLoading={loading.notificationsLoading}
							total={data.notificationsTotalRecord}
						/>
					</UncontrolledDropdown>
				</Collapse>
			</Navbar>
		</>
	);
}

export default withDirection(DashboardNavbar);
