import React, { useEffect, useRef, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { notificationsUnseenAsync } from "store/actions/global/notification.action";
import { useLocation, useHistory } from "react-router-dom";
import { SET_SELECTED_CHAT_ID } from "store/actions/global/globalTypes";
import { genericPath, baseURL } from "engine/config";
import { markChatsAsSeen } from "store/actions/IM/chatActions";
import DashboardNavbar from "./DashboardNavbar";
import LandingNavbar from "./LandingNavbar";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { searchApi } from "api/global/search";
import { online } from "engine/config";
import { removeApisNotWorkingFromLocale } from "engine/config";

const NavbarLayouts = (props) => {
	const location = useLocation();
	const history = useHistory();
	const divRef = useRef(null);
	const dispatch = useDispatch();

	const [imCollapseOpen, setImCollapseOpen] = useState(false);
	const [postsCollapseOpen, setPostsCollapseOpen] = useState(false);
	const [collapseOpen, setCollapseOpen] = useState(false);
	const [sidebarOpen, setSidebarOpen] = useState(false);
	const [sidebarMini, setSidebarMini] = useState(false);
	const [openSearchResult, setOpenSearchResult] = useState(false);
	const [searchQuery, setSearchQuery] = useState("");

	const [color, setColor] = useState("navbar-transparent");

	const { siteTree, activeNodeId } = useSelector((state) => state.navigation);
	const { user } = useSelector((state) => state.auth);
	const {
		chats: chatsNumber,
		notifications,
		notificationsTotalRecord,
		notificationsLoading,
	} = useSelector((state) => state.NotificationsRed);

	const searchData =
		online || removeApisNotWorkingFromLocale
			? ""
			: useFetchDataRQ({ queryKey: ["search", searchQuery], queryFn: () => searchApi.getSearch(searchQuery) });

	useEffect(() => {
		if (!removeApisNotWorkingFromLocale) {
			setTimeout(() => {
				dispatch(notificationsUnseenAsync());
			}, 4000);
		}
	}, []);

	useEffect(() => {
		window.addEventListener("resize", updateColor);
	});

	useEffect(() => {
		if (window.outerWidth < 993 && document.documentElement.className.indexOf("nav-open") !== -1) {
			document.documentElement.classList.toggle("nav-open");
		}
	}, [location]);

	useEffect(() => {
		const handleClickOutside = (event) => {
			if (divRef.current && !divRef.current.contains(event.target)) {
				setPostsCollapseOpen(false);
			}
		};
		document.addEventListener("mousedown", handleClickOutside);
		return () => {
			document.removeEventListener("mousedown", handleClickOutside);
		};
	}, []);

	// function that adds color white/transparent to the navbar on resize (this is for the collapse)
	const updateColor = () => {
		if (window.innerWidth < 993 && collapseOpen) {
			setColor("bg-white");
		} else {
			setColor("navbar-transparent");
		}
	};

	// this function opens and closes the sidebar on small devices
	const toggleSidebar = () => {
		document.documentElement.classList.toggle("nav-open");
		setSidebarOpen(!sidebarOpen);
	};

	// this function opens and closes the collapse on small devices
	// it also adds navbar-transparent class to the navbar when closed
	// ad bg-white when opened
	const toggleCollapse = () => {
		if (!collapseOpen) {
			setColor("bg-white");
		} else {
			setColor("navbar-transparent");
		}
		setCollapseOpen(!collapseOpen);
	};

	const handleRedirectToDashboard = () => {
		if (user) {
			history.push(`${baseURL}/${genericPath}/dashboard`);
		} else {
			history.push(`/landing/home`);
		}
	};
	const handlePushToAuthPage = (authName) => {
		if (authName == "register") {
			history.push(`/auth/register`);
		} else if (authName == "login") {
			history.push(`/auth/login`);
		}
	};

	//----------------------IM----------------------
	const handleMarkChatsAsSeen = () => dispatch(markChatsAsSeen());
	const toggleIMCollapse = () => {
		setImCollapseOpen(!imCollapseOpen);
	};
	const handleCloseIMCollapse = () => {
		setImCollapseOpen(false);
	};
	const handleSelectedChatId = (id) => {
		dispatch({ type: SET_SELECTED_CHAT_ID, payload: id });
	};
	//----------------------------------------------

	//----------------------Notification----------------------
	const togglePostsCollapse = () => {
		setPostsCollapseOpen(!postsCollapseOpen);
	};

	const handleMiniClick = () => {
		if (document.body.classList.contains("sidebar-mini")) {
			setSidebarMini(false);
		} else {
			setSidebarMini(true);
		}
		document.body.classList.toggle("sidebar-mini");
	};

	const handleOpenSearchResult = () => setOpenSearchResult(true);
	const handleCloseSearchResult = () => setOpenSearchResult(false);

	return (
		<>
			{props.dashboard ? (
				<DashboardNavbar
					data={{
						user: user,
						searchData: searchData,
						notificationsTotalRecord: notificationsTotalRecord,
						notifications: notifications,
						siteTree: siteTree,
						activeNodeId: activeNodeId,
						chatsNumber: chatsNumber,
						color: color,
						searchQuery: searchQuery,
						sidebarOpen: sidebarOpen,
						collapseOpen: collapseOpen,
						imCollapseOpen: imCollapseOpen,
						postsCollapseOpen: postsCollapseOpen,
						openSearchResult: openSearchResult,
						divRef: divRef,
					}}
					loading={{ notificationsLoading: notificationsLoading }}
					handlers={{
						handleCloseIMCollapse: handleCloseIMCollapse,
						handleMarkChatsAsSeen: handleMarkChatsAsSeen,
						handleSelectedChatId: handleSelectedChatId,
						handleMiniClick: handleMiniClick,
						handleTogglePostsCollapse: togglePostsCollapse,
						handleToggleIMCollapse: toggleIMCollapse,
						handleToggleCollapse: toggleCollapse,
						handleToggleSidebar: toggleSidebar,
						handelSetSearchQuery: setSearchQuery,
						handleOpenSearchResult: handleOpenSearchResult,
						handleCloseSearchResult: handleCloseSearchResult,
						history: history,
					}}
				/>
			) : (
				<LandingNavbar
					notificationsTotalRecord={notificationsTotalRecord}
					notificationsLoading={notificationsLoading}
					notifications={notifications}
					activeNodeId={activeNodeId}
					chatsNumber={chatsNumber}
					siteTree={siteTree}
					color={color}
					dir={props.dir}
					history={history}
					user={user}
					collapseOpen={collapseOpen}
					imCollapseOpen={imCollapseOpen}
					postsCollapseOpen={postsCollapseOpen}
					divRef={divRef}
					handleRedirectToDashboard={handleRedirectToDashboard}
					handleCloseIMCollapse={handleCloseIMCollapse}
					handleMarkChatsAsSeen={handleMarkChatsAsSeen}
					handleSelectedChatId={handleSelectedChatId}
					handlePushToAuthPage={handlePushToAuthPage}
					handleMiniClick={handleMiniClick}
					togglePostsCollapse={togglePostsCollapse}
					toggleIMCollapse={toggleIMCollapse}
					toggleCollapse={toggleCollapse}
					toggleSidebar={toggleSidebar}
				/>
			)}
		</>
	);
};

export default NavbarLayouts;
