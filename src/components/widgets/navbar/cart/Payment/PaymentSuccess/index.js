import React from "react";
import SuccessGif from "assets/img/new/gif/success.gif";
import RButton from "components/Global/RComs/RButton";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { baseURL, genericPath } from "engine/config";
import { useHistory, useParams } from "react-router-dom";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { paymentApi } from "api/global/payment";
import { CART_ITEMS } from "config/constants";

const PaymentSuccess = () => {
	const history = useHistory();
	const { orderId } = useParams();

	const orderSummaryData = useFetchDataRQ({
		queryKey: ["create-payment-intent"],
		queryFn: () => paymentApi.paymentOrder(orderId),
		onSuccessFn: () => {
			typeof window !== "undefined" && localStorage.removeItem(CART_ITEMS);
		},
	});

	if (orderSummaryData.isLoading) {
		return <Loader />;
	}
	return (
		<RFlex
			styleProps={{
				gap: 0,
				height: "100vh",
				background: "#F3F3F3",
				justifyContent: "center",
				alignItems: "center",
				flexDirection: "column",
			}}
		>
			<RFlex styleProps={{ gap: 0, flexDirection: "column", alignItems: "center" }}>
				<span style={{ fontSize: "16px" }}>{tr`thank_you_for_your_purchase`}</span>
				<img src={SuccessGif} alt="success-image-gif" />
				<span style={{ fontSize: "16px" }}>{orderSummaryData?.data?.data?.data?.externalOrderID}</span>
				<RButton
					text={tr`back_to_my_courses`}
					color="primary"
					style={{ width: "100%" }}
					onClick={() => history.push(`${baseURL}/${genericPath}/my-courses`)}
				/>
			</RFlex>
		</RFlex>
	);
};

export default PaymentSuccess;
