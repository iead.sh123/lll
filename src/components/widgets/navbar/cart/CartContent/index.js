import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { strokeColor, CART_ITEMS } from "config/constants";
import { specificOrganization } from "engine/config";
import { landingPageAsync } from "store/actions/global/coursesManager.action";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useMutateData } from "hocs/useMutateData";
import { paymentApi } from "api/global/payment";
import { useHistory } from "react-router-dom";
import CartGuestUser from "../CartGuestUser";
import CartCoupon from "../CartCoupon";
import CartPrices from "../CartPrices";
import CartEmpty from "../CartEmpty";
import CartItems from "../CartItems";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import styles from "../cart.module.scss";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const CartContent = ({ toggleDrawer }) => {
	const dispatch = useDispatch();
	const history = useHistory();
	const [cartItems, setCartItems] = useState(
		typeof window !== "undefined"
			? localStorage.getItem(CART_ITEMS)?.length > 0
				? localStorage.getItem(CART_ITEMS).split(",").map(Number)
				: []
			: []
	);

	const { user } = useSelector((state) => state.auth);

	// Start Apis
	const cartItemsData = useFetchDataRQ({ queryKey: ["cart-items"], queryFn: () => paymentApi.fetchCartItems() });
	const cartPricesData = useFetchDataRQ({ queryKey: ["cart-prices", cartItems], queryFn: () => paymentApi.cartPrices(cartItems) });

	const deleteItemMutation = useMutateData({
		queryFn: (itemId) => paymentApi.removeItemFromCart(itemId),
		invalidateKeys: [["cart-items"], ["cart-prices", cartItems]],
		multipleKeys: true,
	});

	const addCouponMutation = useMutateData({
		queryFn: (data) => paymentApi.applyCouponOnItems(data),
		invalidateKeys: ["cart-prices", cartItems],
	});

	const deleteCouponMutation = useMutateData({
		queryFn: () => paymentApi.deleteCouponFromItems(),
		invalidateKeys: ["cart-prices", cartItems],
	});

	const enrollItemsInCartMutation = useMutateData({
		queryFn: (data) => paymentApi.enrollCart(data),
		invalidateKeys: [["cart-items"], ["cart-prices", cartItems]],
		multipleKeys: true,
		onSuccessFn: () => {
			typeof window !== "undefined" && localStorage.removeItem(CART_ITEMS);
			dispatch(landingPageAsync(user, specificOrganization ?? user?.organization_id ?? 1));
		},
	});

	// End Apis

	// Selected Items
	const handleSelectedItems = (itemId, deleteFlag) => {
		let updatedArray = [...cartItems];

		const itemIndex = updatedArray.findIndex((existingItemId) => existingItemId == itemId);
		if (itemIndex === -1 && !deleteFlag) {
			updatedArray.push(itemId);
		} else {
			updatedArray = updatedArray.filter((existingItemId) => existingItemId !== itemId);
		}

		setCartItems(updatedArray);
		if (typeof window !== "undefined") {
			localStorage.setItem(CART_ITEMS, updatedArray);
		}
	};

	// Delete Item From Cart
	const handleDeleteItem = (itemId) => {
		deleteItemMutation.mutate(itemId);
	};

	// Delete Coupon From Cart
	const handleDeleteCoupon = () => {
		deleteCouponMutation.mutate({});
	};

	// Add Coupon
	const handleAddCouponToItems = (data) => {
		addCouponMutation.mutate(data);
	};

	// Enroll Cart
	const handleEnrollCart = () => {
		enrollItemsInCartMutation.mutate({ cartItems: cartItems });
	};

	// Push To Payment
	const handlePushToPayment = () => {
		history.push(`/cart/payment`);
	};

	// Loading
	if (cartItemsData.isLoading || cartPricesData.isLoading) {
		<Loader />;
	}

	return (
		<div>
			<aside
				className={styles.aside__content + " scroll_hidden"}
				style={{ height: `calc(100vh - ${!!cartItemsData?.data?.data?.data?.length ? "70px - 3.25rem" : "0px"})` }}
			>
				<RFlex styleProps={{ flexDirection: "column", gap: "0px" }}>
					<RFlex>
						<i className={iconsFa6.cart + " pt-1"} />
						<span>{tr`cart`}</span>
					</RFlex>
					<hr style={{ background: strokeColor, width: "100%" }} />
				</RFlex>

				{!user ? (
					<CartGuestUser />
				) : cartItemsData?.data?.data?.data?.length > 0 ? (
					<RFlex styleProps={{ flexDirection: "column" }}>
						{cartItemsData?.data?.data?.data?.map((item, index, arr) => (
							<CartItems
								key={item.id}
								item={item}
								cartItems={cartItems}
								handleSelectedItems={handleSelectedItems}
								handleDeleteItem={handleDeleteItem}
							/>
						))}

						<div className={styles.divider} />

						<CartCoupon
							prices={cartPricesData?.data?.data?.data}
							cartItems={cartItems}
							handleAddCouponToItems={handleAddCouponToItems}
							applyCouponLoading={addCouponMutation.isLoading}
							handleDeleteCoupon={handleDeleteCoupon}
							deleteCouponMutation={deleteCouponMutation}
						/>
						<div className={styles.divider} />
						<CartPrices prices={cartPricesData?.data?.data?.data} />
					</RFlex>
				) : (
					<CartEmpty />
				)}
			</aside>
			{user && cartItemsData?.data?.data?.data?.length > 0 && (
				<RFlex styleProps={{ flexDirection: "column", gap: "0px" }}>
					<RButton
						className={styles.remove__hover}
						onClick={() => toggleDrawer()}
						text={tr`continue_shopping`}
						color="link"
						style={{ margin: "5px" }}
					/>
					{cartPricesData?.data?.data?.data?.is_free && cartItems?.length > 0 ? (
						<RButton
							onClick={() => handleEnrollCart()}
							disabled={enrollItemsInCartMutation.isLoading}
							loading={enrollItemsInCartMutation.isLoading}
							text={tr`enroll_now`}
							color="primary"
							// style={{ margin: 0 }}
						/>
					) : (
						<RButton
							onClick={() => {
								handlePushToPayment();
								toggleDrawer();
							}}
							disabled={cartItems?.length == 0 || cartPricesData.isLoading}
							text={tr`proceed_to_checkout`}
							color="primary"
							// style={{ margin: 0 }}
						/>
					)}
				</RFlex>
			)}
		</div>
	);
};

export default CartContent;
