import React from "react";
import CartAdItems from "assets/img/new/cart-add-items.png";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { boldGreyColor } from "config/constants";

const CartEmpty = () => {
	return (
		<RFlex styleProps={{ flexDirection: "column", alignItems: "center", justifyContent: "center" }}>
			<p>{tr`your_cart_is_empty`}</p>
			<img src={CartAdItems} alt="cart-add-items" style={{ height: "100px" }} />
			<p style={{ color: boldGreyColor }}>{tr`keep_shopping`}&nbsp;!</p>
		</RFlex>
	);
};

export default CartEmpty;
