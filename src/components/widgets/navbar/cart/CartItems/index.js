import React from "react";
import removeDecimalIfNeeded from "utils/removeDecimalIfNeeded";
import CartAdItems from "assets/img/new/Chemistry1.jpg";
import iconsFa6 from "variables/iconsFa6";
import styles from "../cart.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { dangerColor } from "config/constants";
import { Services } from "engine/services";
import AppNewCheckbox from "components/Global/RComs/AppNewCheckbox/AppNewCheckbox";

const CartItems = ({ item, cartItems, handleSelectedItems, handleDeleteItem, viewMode }) => {
	return (
		<RFlex styleProps={{ alignItems: "center", gap: 0 }}>
			{!viewMode && (
				<RFlex>
					<AppNewCheckbox
						onChange={(event) => {
							event.stopPropagation();
							handleSelectedItems(item.id);
						}}
						checked={cartItems?.includes(+item.id)}
					/>
				</RFlex>
			)}
			<RFlex styleProps={{ justifyContent: "space-between", width: "100%" }}>
				<RFlex styleProps={{ alignItems: "center", justifyContent: "space-between", gap: 5 }}>
					<img
						className={styles.cart__image}
						src={item.image ? Services.storage.file + item.image : CartAdItems}
						alt="cart-add-items"
						width="60px"
						height="60px"
					/>
					<RFlex styleProps={{ flexDirection: "column", gap: 0 }}>
						<span className={styles.item__name}>{item?.name}</span>
						{/* {item?.course_features && (
							<span className={styles.item__type}>{Object?.values(JSON?.parse(item?.course_features))?.map((el) => el)}</span>
						)} */}
					</RFlex>
				</RFlex>
				<RFlex styleProps={{ alignItems: "center", gap: 5 }}>
					<RFlex styleProps={{ flexDirection: "column", gap: 0 }}>
						{+item?.priceAfterDiscount !== item?.subTotal && (
							<span className={styles.price_after_discount}>
								{item?.orgCurrency}&nbsp;
								{removeDecimalIfNeeded(item?.subTotal)}
							</span>
						)}
						<span className={styles.price}>
							{item?.orgCurrency}&nbsp;
							{removeDecimalIfNeeded(+item?.priceAfterDiscount !== item?.subTotal ? item?.priceAfterDiscount : item?.subTotal)}
						</span>
					</RFlex>
					{!viewMode && (
						<i
							onClick={(event) => {
								event.stopPropagation();
								handleDeleteItem(item.id);
								handleSelectedItems(item.id, true);
							}}
							className={iconsFa6.delete}
							style={{ color: dangerColor, cursor: "pointer" }}
						/>
					)}
				</RFlex>
			</RFlex>
		</RFlex>
	);
};

export default CartItems;
