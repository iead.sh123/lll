import React from "react";
import Loader from "utils/Loader";
import styles from "../Navbar.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { boldGreyColor } from "config/constants";
import tr from "components/Global/RComs/RTranslator";

const SearchResultBox = ({ info }) => {
 	return (
		<div className={styles.search__result__box}>
			{info.isLoading || info.isFetching ? (
				<Loader />
			) : info?.data?.data?.data ? (
				<div className={styles.search__result__data}>
					{Object.keys(info?.data?.data?.data)?.map((key, index) => {
						return (
							<RFlex key={index} styleProps={{ flexDirection: "column", padding: "5px" }}>
								<span style={{ fontWeight: "bold" }}>{key}</span>
								<RFlex styleProps={{ flexDirection: "column", flexWrap: "wrap", gap: 0 }}>
									{info?.data?.data?.data[key]?.items?.map((value, ind, array) => {
										return (
											<RFlex key={value.id} className={styles.search__value} styleProps={{ padding: 4 }}>
												<span style={{ color: boldGreyColor }}>{value.name}</span>
												{/* <span> {ind !== array.length - 1 ? "-" : ""}</span> */}
											</RFlex>
										);
									})}
								</RFlex>
							</RFlex>
						);
					})}
				</div>
			) : (
				<RFlex styleProps={{ justifyContent: "center", alignItems: "center", padding: "5px" }}>{tr`no_search_result`}</RFlex>
			)}
		</div>
	);
};

export default SearchResultBox;
