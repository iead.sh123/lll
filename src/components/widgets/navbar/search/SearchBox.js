import React, { useState } from "react";
import { Form, FormGroup, Input } from "reactstrap";
import iconsFa6 from "variables/iconsFa6";
import tr from "components/Global/RComs/RTranslator";

const SearchBox = ({ setSearch, handleOpenSearchResult, handleCloseSearchResult, children }) => {
	const [timeout, setT] = useState(null);

	const handleSearch = (event) => {
		if (timeout) clearTimeout(timeout);
		setT(
			setTimeout(() => {
				setSearch(event.target.value);
			}, 500)
		);
	};

	return (
		<Form className={"form__input"}>
			<FormGroup>
				<Input
					type="text"
					placeholder={tr`search`}
					className={"search__input"}
					onChange={(event) => {
						event.preventDefault();
						handleSearch(event);
					}}
					onKeyDown={(event) => {
						if (event.key === "Enter") {
							event.preventDefault();

							handleSearch(event);
						}
					}}
					onFocus={handleOpenSearchResult}
					onBlur={handleCloseSearchResult}
				/>
				<i aria-hidden="true" className={`${iconsFa6.search} search__input__icon`} />
			</FormGroup>
			{children}
		</Form>
	);
};

export default SearchBox;
