import withDirection from "hocs/withDirection";
import { Label } from "reactstrap";

const AppLabel = ({ extraStyles = "", children, dir }) => {
  return (
    <Label className={`text-${dir ? 'left' : 'right'} text-dark pl-0 mw-100 ${extraStyles}`} sm="2">
      {children}
    </Label>
  );
};

export default withDirection(AppLabel);
