import React, { useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Nav, Collapse } from "reactstrap";
import { NavLink } from "react-router-dom";
import { logout } from "store/actions/global/auth.actions";
import defaultImageSideBar from "assets/img/avatar.png";
import PerfectScrollbar from "perfect-scrollbar";
import withDirection from "hocs/withDirection";
import guidance from "assets/img/logo.png";
import Loader from "utils/Loader";
import tr from "components/Global/RComs/RTranslator";
import { Services } from "engine/services";
import { genericPath } from "engine/config";
import GStudentSwitch from "logic/General/GStudentSwitch";
import iconsFa6 from "variables/iconsFa6";
import { primaryColor } from "config/constants";
import { useLocation, useHistory } from "react-router-dom";
import Helper from "components/Global/RComs/Helper";

var ps;

function Sidebar(props) {
	const location = useLocation();
	const history = useHistory();
	const dispatch = useDispatch();
	const { sidebarLoading } = props;
	const sidebar = useRef();
	const english = localStorage.getItem("language") != "arabic";
	const float = english ? "left" : "right";

	const [openAvatar, setOpenAvatar] = useState(false);
	const [collapseStates, setCollapseStates] = useState({});
	const { user, setUserPhoto } = useSelector((state) => state.auth);
	const { chats } = useSelector((state) => state.NotificationsRed);

	const logoutCallback = () => {
		dispatch(logout(history));
	};

	// this creates the intial state of this component based on the collapse routes
	// that it gets through props.routes

	function unregisterFirebaseServiceWorker() {
		Helper.cl("unregister button1");
		if ("serviceWorker" in navigator) {
			Helper.cl("unregister button2");

			navigator.serviceWorker
				.getRegistration("/firebase-messaging-sw.js")
				.then((registration) => {
					Helper.cl("unregister button3");

					if (registration) {
						Helper.cl("unregister button4");
						Helper.cl("unregister button4");
						registration
							.unregister()
							.then(() => {
								console.log("Firebase Service Worker unregistered.");
							})
							.catch((error) => {
								console.error("Error while unregistering Firebase Service Worker:", error);
							});
					}
				})
				.catch((error) => {
					console.error("Error getting Firebase Service Worker registration:", error);
				});
		}
	}
	const getCollapseStates = (routes) => {
		let initialState = {};
		routes.map((prop) => {
			if (prop.collapse) {
				initialState = {
					[prop.state]: getCollapseInitialState(prop.items),
					...getCollapseStates(prop.items),
					...initialState,
				};
			}
			return null;
		});
		return initialState;
	};

	// this verifies if any of the collapses should be default opened on a rerender of this component
	// for example, on the refresh of the page,
	// while on the src/views/forms/RegularForms.js - route /admin/regular-forms

	const getCollapseInitialState = (routes) => {
		for (let i = 0; i < routes.length; i++) {
			if (routes[i].collapse && getCollapseInitialState(routes[i].items)) {
				return true;
			} else if (window.location.pathname.indexOf(routes[i].path) !== -1) {
				return true;
			}
		}
		return false;
	};

	// this function creates the links and collapses that appear in the sidebar (left menu)
	const createLinks = (routes) => {
		return routes.map((prop, key) => {
			if (prop.redirect) {
				return null;
			}

			if (prop.invisible) {
				return null;
			}
			if (prop.collapse) {
				var st = {};
				st[prop["state"]] = !collapseStates[prop.state];
				return (
					<li className={(getCollapseInitialState(prop.items) ? " active " : "") + (english ? " " : " rtl ")} key={key}>
						<a
							href="#"
							data-toggle="collapse"
							aria-expanded={collapseStates[prop.state]}
							onClick={(e) => {
								e.preventDefault();
								setCollapseStates(st);
							}}
							style={{ textAlign: float }}
						>
							{prop.icon !== undefined ? (
								<>
									<i className={prop.icon} style={{ float: float }} />
									<p style={{ textTransform: "capitalize" }}>
										{tr(prop.title)}
										<b className="caret" />
									</p>
								</>
							) : (
								<>
									{/* <span className="sidebar-mini-icon">{prop.mini}</span> */}
									<span
										className="sidebar-normal"
										style={{
											textTransform: "capitalize",
											paddingLeft: english ? "45px" : "",
											paddingRight: english ? "" : "45px",
										}}
									>
										{tr(prop.title)}
										<b className="caret" />
									</span>
								</>
							)}
						</a>

						<Collapse isOpen={collapseStates[prop.state]}>
							<ul className="nav">{createLinks(prop.items)}</ul>
						</Collapse>
					</li>
				);
			}

			return (
				<li className={activeRoute(prop.layout + prop.path)} key={key}>
					<NavLink
						style={{ textAlign: float }}
						to={prop.layout + prop.path}
						activeClassName=""
						className={prop.disabled ? "disabled__link" : ""}

						// onClick={() => dispatch(setCurrentNodeByLink(prop.path))}
					>
						{prop.icon !== undefined ? (
							<>
								<i className={prop.icon} style={{ float: float }} />
								<p style={{ textTransform: "capitalize" }}>
									{tr(prop.title)}
									{tr(prop.title) === "Messages" && chats !== 0 && (
										<span
											style={{
												color: "red",
												fontSize: "15px",
												backgroundColor: "white",
												border: "2.9px solid #fd822b",
												height: "18px",
												width: "24px",
												borderRadius: "50",
											}}
										>
											{chats}
										</span>
									)}
								</p>
							</>
						) : (
							<>
								{/* <span className="sidebar-mini-icon">{prop.mini}</span> */}
								<span
									className="sidebar-normal"
									style={{
										textTransform: "capitalize",
										paddingLeft: english ? "45px" : "",
										paddingRight: english ? "" : "45px",
									}}
								>
									{tr(prop.title)}
								</span>
							</>
						)}
					</NavLink>
				</li>
			);
		});
	};

	// verifies if routeName is the one active (in browser input)
	const activeRoute = (routeName) => {
		return (props.location.pathname.indexOf(routeName) > -1 ? " active " : "") + (english ? " " : " rtl ");
	};

	React.useEffect(() => {
		// if you are using a Windows Machine, the scrollbars will have a Mac look
		if (navigator.platform.indexOf("Win") > -1) {
			ps = new PerfectScrollbar(sidebar.current, {
				suppressScrollX: true,
				suppressScrollY: false,
			});
		}
		return function cleanup() {
			// we need to destroy the false scrollbar when we navigate
			// to a page that doesn't have this component rendered
			if (navigator.platform.indexOf("Win") > -1) {
				ps.destroy();
			}
		};
	});

	React.useEffect(() => {
		setCollapseStates(getCollapseStates(props.routes));
	}, []);

	return (
		<div
			className="sidebar"
			data-color={props.bgColor}
			data-active-color={props.activeColor}
			style={props.dir ? {} : { zIndex: 0, right: 0 }}
		>
			<div className="logo" style={{ display: "flex", justifyContent: "center" }}>
				<a
					href="https://testt.guidancealliance.com/"
					style={{ display: "flex", justifyContent: "center" }}
					className="simple-text"
					target="_blank"
				>
					<div className="logo-img" style={{ width: "60px", height: "60px" }}>
						<img src={guidance} alt="logo-guidance" style={{ width: "60px", height: "60px" }} />
					</div>
				</a>
			</div>
			<div className="sidebar-wrapper" ref={sidebar}>
				<div className="user">
					<div className="photo">
						<img
							src={user?.image?.hash_id !== undefined ? Services.storage.file + user?.image?.hash_id : defaultImageSideBar}
							alt="Avatar"
							style={{ height: "100%" }}
						/>
					</div>

					<div className="info">
						<a href="#" data-toggle="collapse" aria-expanded={openAvatar} onClick={() => setOpenAvatar(!openAvatar)}>
							<span style={{ color: "#333" }}>
								{user?.name}
								<b className="caret" />
							</span>
						</a>

						<Collapse
							isOpen={openAvatar}
							style={{
								width: "100%",
							}}
						>
							<ul
								className="nav"
								style={{
									paddingLeft: english ? "60px" : "",
									paddingRight: english ? "" : "60px",
									marginTop: "3px",
								}}
							>
								<li className={props.location.pathname.includes("/user/profile") ? "active" : ""}>
									<NavLink
										to={process.env.REACT_APP_BASE_URL + `/${genericPath}/user/profile`}
										activeClassName=""
										style={{
											textAlign: float,
											paddingBottom: "5px",
											margin: "0px",
										}}
									>
										<span className="sidebar-normal">{tr`my_profile`}</span>
									</NavLink>
								</li>
								<li className={props.location.pathname.includes("/user/switch-account") ? "active" : ""}>
									<NavLink
										to={process.env.REACT_APP_BASE_URL + `/${genericPath}/user/switch-account`}
										activeClassName=""
										style={{
											textAlign: float,
											paddingBottom: "5px",
											margin: "0px",
										}}
									>
										<span className="sidebar-normal">{tr`switch_account`}</span>
									</NavLink>
								</li>
								<li>
									<NavLink
										to="#"
										onClick={() => logoutCallback()}
										activeClassName=""
										style={{
											textAlign: float,
											paddingBottom: "5px",
											margin: "0px",
										}}
									>
										<span className="sidebar-normal">{tr`log_out`}</span>
									</NavLink>
								</li>
							</ul>
						</Collapse>
					</div>
				</div>
				{props.routes && props.routes.length > 0 && !sidebarLoading ? <Nav>{createLinks(props.routes)}</Nav> : Loader()}
				{user?.current_type?.toLowerCase() == "parent" ? <GStudentSwitch /> : <></>}
				{["student", "learner"].includes(user?.current_type?.toLowerCase()) && (
					<div
						style={{
							color: primaryColor,
							cursor: "pointer",
							position: "fixed",
							padding: "10px",
							margin: "auto",
							bottom: 0,
							left: 15,
						}}
						onClick={() => history.push("/landing/home")}
					>
						<i className={iconsFa6.search + " pt-1 pr-2"} />
						{location.pathname.includes("course-management") ? "" : <span>{tr`discover_all_courses`}</span>}
					</div>
				)}
			</div>
			{/* <button style={{position:"fixed",top:"300px",border:"red 2px solid",    zIndex: "100000"}} onClick={()=>{unregisterFirebaseServiceWorker()}}>handle reloading firebase</button> */}
		</div>
	);
}

export default withDirection(Sidebar);
