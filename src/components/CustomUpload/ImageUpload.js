import React, { useState } from "react";
// used for making the prop types of this component
import { Button } from "reactstrap";
import { convertBase64 } from "utils/convertToBase64";
import styles from "./ImageUpload.Module.scss";
import defaultImage from "assets/img/default-avatar.png";

function ImageUpload(props) {
	const [fileName, setFileName] = useState([]);
	const [userPhoto, setUserPhoto] = useState([]);

	const fileInput = React.useRef();
	const uploadFile = async (e) => {
		e.preventDefault();
		const files = e.target.files;
		let Attachments = [];
		for (const file of Array.from(files)) {
			setFileName(file.name, fileName);
			const base64 = await convertBase64(file);
			Attachments.push({ url: base64, file_name: file.name, type: file.type });
		}
		setUserPhoto(Attachments);
		props.parentCallback(Attachments);
	};

	const handleClick = () => {
		fileInput.current.click();
	};
	const handleRemove = () => {
		fileInput.current.value = null;
		setUserPhoto([]);
		// props.parentCallback([{}]);
	};

	const handleDeleteImage = () => {
		props.deleteImage();
	};

	return (
		<div className="fileinput text-center">
			<input type="file" multiple onChange={uploadFile} ref={fileInput} />
			{props.image ? (
				<div className={"thumbnail"}>
					<img width={200} height={200} src={process.env.REACT_APP_RESOURCE_URL + props.image} alt="User Image" />
				</div>
			) : (
				<div className={"thumbnail"}>
					<img width={200} height={200} src={defaultImage} alt="Default User Image" />
				</div>
			)}

			<div className="img-circle">
				<div className="row">
					{userPhoto.map((res, i) => {
						return res.type == "image/png" || res.type == "image/jpg" || res.type == "image/jpeg" ? (
							<div key={i} className={"col-sm-4 " + styles.P_Bootom}>
								<img src={res.url} height="50" />
								<br />
							</div>
						) : (
							<div key={i} className={"col-sm-6" + styles.P_Bootom}>
								<p>{res.file_name}</p>
							</div>
						);
					})}
				</div>
			</div>
			<div>
				{userPhoto.length === 0 ? (
					<>
						<Button className={"btn-round " + styles.h_btn} color="info" outline onClick={() => handleClick()}>
							{props.avatar ? "Add Photo" : <i className="fa fa-paperclip fa-lg" aria-hidden="true"></i>}
						</Button>
						{props.image && (
							<Button color="danger" className={"btn-round " + styles.h_btn} onClick={() => handleDeleteImage()}>
								<i className="fa fa-times fa-lg" aria-hidden="true" />
								Delete
							</Button>
						)}
					</>
				) : (
					<span>
						<Button onClick={() => handleClick()}>Change</Button>
						{props.avatar ? <br /> : null}
						<Button color="danger" onClick={() => handleRemove()}>
							<i className="fa fa-times" />
							Remove
						</Button>
					</span>
				)}
			</div>
			{props.done ? (
				<Button color="danger" onClick={props.parentClose}>
					<i className="fa fa-check" />
					Done
				</Button>
			) : (
				<></>
			)}
		</div>
	);
}

export default ImageUpload;
