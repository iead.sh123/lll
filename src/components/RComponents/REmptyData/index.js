import React from "react";
import NoData from "assets/img/new/Lightbulb.png";
import RFlex from "components/Global/RComs/RFlex/RFlex";

const REmptyData = ({ Image, width = "130px", height = "130px", removeImage, line1, line2, component }) => {
	return (
		<RFlex
			styleProps={{
				width: "100%",
				alignItems: removeImage ? "" : "center",
				justifyContent: removeImage ? "" : "center",
				flexDirection: "column",
			}}
		>
			{component ? (
				component
			) : (
				<>
					{/* {!removeImage && <img src={Image ? Image : NoData} width={width} height={height} />} */}
					{line1 && <RFlex styleProps={{ color: "#585858" }}>{line1}</RFlex>}
					{line2 && <RFlex styleProps={{ color: "#585858" }}>{line2}</RFlex>}
				</>
			)}
		</RFlex>
	);
};

export default REmptyData;
