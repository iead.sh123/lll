import tr from "components/Global/RComs/RTranslator";
import React from "react";
import { Tabs, TabsContent, TabsList, TabsTrigger } from "ShadCnComponents/ui/tabs";

const RNewTabs = ({ defaultValue = "", tabs = [], className = "", value, setActiveTab, innerContent = false }) => {
	return (
		<Tabs defaultValue={defaultValue} className={className} value={value}>
			<TabsList className="flex gap-1 w-fit bg-themeSecondary ">
				{tabs.map((tab) => (
					<TabsTrigger onClick={() => setActiveTab(tab.value)} value={tab.value} disabled={tab.disabled}>
						{tr(tab.title)}
					</TabsTrigger>
				))}
			</TabsList>
			{innerContent && tabs.map((tab) => <TabsContent value={tab.value}>{tab.content}</TabsContent>)}
		</Tabs>
	);
};

export default RNewTabs;
