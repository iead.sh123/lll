import { Button } from "ShadCnComponents/ui/button";
import { DialogContent } from "ShadCnComponents/ui/dialog";
import { DialogTitle } from "ShadCnComponents/ui/dialog";
import { DialogDescription } from "ShadCnComponents/ui/dialog";
import { DialogFooter } from "ShadCnComponents/ui/dialog";
import { DialogHeader } from "ShadCnComponents/ui/dialog";
import { DialogTrigger } from "ShadCnComponents/ui/dialog";
import { Dialog } from "ShadCnComponents/ui/dialog";
import tr from "components/Global/RComs/RTranslator";
import React from "react";

const RDialog = ({ triggerComponent, dialogHeader = null, dialogBody = null, dialogFooter = null }) => {
	return (
		<Dialog>
			<DialogTrigger asChild>
				{triggerComponent ? (
					triggerComponent
				) : (
					<Button variant="outline" className="w-fit">
						{tr("open_dialog")}
					</Button>
				)}
			</DialogTrigger>
			<DialogContent className="sm:max-w-[425px]">
				{dialogHeader && (
					<DialogHeader>
						<DialogTitle>{dialogHeader?.title}</DialogTitle>
						<DialogDescription>{dialogHeader?.description}</DialogDescription>
					</DialogHeader>
				)}
				{dialogBody && dialogBody}
				{dialogFooter && <DialogFooter>{dialogFooter}</DialogFooter>}
			</DialogContent>
		</Dialog>
	);
};

export default RDialog;
