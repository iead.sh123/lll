import { Input } from "ShadCnComponents/ui/input";
import React from "react";

const RNewInput = (props) => {
	return <Input {...props} className={`${props.inputError ? "input__error" : ""} ${props.className}`} />;
};

export default RNewInput;

// import React from "react";
// import { Input } from "ShadCnComponents/ui/input";

// const RNewInput = React.forwardRef((props, ref) => {
// 	return <Input {...props} ref={ref} className={`${props.inputError ? "input__error" : ""} ${props.className}`} />;
// });

// RNewInput.displayName = "RNewInput";

// export default RNewInput;
