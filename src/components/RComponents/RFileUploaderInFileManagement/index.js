import React, { useState, useEffect } from "react";
import { DefaultTypeFile, calculateChunkSize } from "./constants";
import { useDropzone } from "react-dropzone";
import { Services } from "engine/services";
import { toast } from "react-toastify";
import uploadFileIcon from "assets/img/svg/upload.svg";
import RFilesList from "./RFilesList";
import styles from "./RFileUploaderInFileManagement.module.scss";
import store from "store";
import tr from "components/Global/RComs/RTranslator";
import { readFile } from "utils/readFile";

function RFileUploaderInFileManagement({
	parentCallback,
	parentCallbackToFillData,
	typeFile,
	sizeFile = 5000,
	singleFile,
	placeholder,
	value = [],
	folderId,
	customAPI = false,
	customFormData = null,
	apiMethod = "POST",
	successCallback,
}) {
	const [files, setFiles] = useState([]);
	const [progress, setProgress] = useState({});
	const [abortControllers, setAbortControllers] = useState({});
	const [filesFromBackend, setFilesFromBackend] = useState([]);

	useEffect(() => {
		if (value && value.length > 0) {
			setFiles(
				value.map((val) => ({
					url: val.url,
					name: val.file_name,
					mimeType: val.mime_type,
				}))
			);
		}
	}, [value]);

	// *** Because I Need To Send upload_id To Backend If I Put It This Function In processFile And I Uploaded Multi File This Function Don't Run Correctly ***

	useEffect(() => {
		parentCallback(files);
	}, [files]);

	useEffect(() => {
		if (filesFromBackend?.length > 0) parentCallbackToFillData(filesFromBackend);
	}, [filesFromBackend]);

	// - - - - - - - - - - - - - - onDrop Function - - - - - - - - - - - - - -

	const onDrop = async (acceptedFiles) => {
		if ((singleFile && acceptedFiles.length > 1) || (singleFile && acceptedFiles.length == 1 && files.length > 0)) {
			// Display an error message or prevent further processing
			toast.warning(tr`Multiple files not allowed`);

			return;
		}
		for (const file of acceptedFiles) {
			try {
				const abortController = new AbortController();
				const signal = abortController.signal;

				// Save the abort controller for the file
				setAbortControllers((prevControllers) => ({
					...prevControllers,
					[file.name]: abortController,
				}));

				processFile(file, signal);
			} catch (error) {
				console.error("Error initiating file upload:", error);
			}
		}
	};

	// - - - - - - - - - - - - - - Progress Function - - - - - - - - - - - - - -

	const processFile = async (file, signal) => {
		// const chunkSize = calculateChunkSize(file.size);
		// const totalChunks = Math.ceil(file.size / chunkSize);
		const totalChunks = 1;
		const fileName = file.name;
		const fileMimeType = file.mime_type ?? file.type;
		const chunkProgressArray = Array(totalChunks).fill(0); // Initialize array to track progress of each chunk

		// Read file and generate preview
		const filePreview = await readFile(file);
		setFiles((prevFiles) => [
			...prevFiles,
			{
				file,
				preview: filePreview,
				mimeType: fileMimeType,
			},
		]);

		for (let i = 0; i < 1; i++) {
			if (signal.aborted) {
				return;
			}

			try {
				await uploadChunk(file, i, fileName, chunkProgressArray).then((res) => console.log("resss", res));
			} catch (error) {
				console.error("Error uploading chunk:", error);
			}
		}
	};

	// - - - - - - - - - - - - - - Upload Chunk Function - - - - - - - - - - - - - -

	const uploadChunk = (chunk, chunkNumber, fileName, chunkProgressArray) => {
		return new Promise((resolve, reject) => {
			const formData = new FormData();
			formData.append("file", chunk);
			formData.append("name", chunk.name);
			if (!customFormData) {
				formData.append("folder", folderId);
			} else {
				Object.keys(customFormData).forEach((key) => {
					formData.append(key, customFormData[key]);
				});
			}
			const token = store.getState().auth.token;
			const xhr = new XMLHttpRequest();
			const url = customAPI ? customAPI : `${Services.file_management.backend}api/file_management/files`;

			xhr.open(apiMethod, url, true);
			xhr.setRequestHeader("Authorization", `Bearer ${token}`);

			xhr.upload.addEventListener("progress", (event) => {
				const chunkProgress = Math.round((event.loaded / event.total) * 100);
				chunkProgressArray[chunkNumber] = chunkProgress; // Update progress of current chunk
				const overallProgress = calculateOverallProgress(chunkProgressArray); // Calculate overall progress

				onUploadProgress(fileName, overallProgress); // Update overall progress
			});

			xhr.onload = () => {
				if (xhr.status === 200) {
					resolve(); // Resolve the promise when the upload is successful
					if (JSON.parse(xhr.response)?.data) {
						successCallback && successCallback(JSON.parse(xhr.response));
						setFilesFromBackend((prevFiles) => [...prevFiles, JSON.parse(xhr.response)?.data]);
					}
				} else {
					reject(xhr.statusText); // Reject the promise if there's an error
				}
			};

			xhr.onerror = () => {
				reject(xhr.statusText); // Reject the promise if there's an error
			};

			xhr.send(formData);
		});
	};

	// - - - - - - - - - - - - - - Calculate Overall Progress - - - - - - - - - - - - - -

	const calculateOverallProgress = (chunkProgressArray) => {
		const totalChunks = chunkProgressArray.length;
		const totalProgress = chunkProgressArray.reduce((acc, curr) => acc + curr, 0);
		return Math.round((totalProgress / (totalChunks * 100)) * 100); // Calculate overall progress as a percentage
	};

	// - - - - - - - - - - - - - - OnUpload Progress - - - - - - - - - - - - - -

	const onUploadProgress = (fileName, overallProgress) => {
		setProgress((prevProgress) => ({
			...prevProgress,
			[fileName]: overallProgress,
		}));
	};

	// - - - - - - - - - - - - - - Remove File - - - - - - - - - - - - - -

	const removeFile = (index, fileName) => {
		setFiles((prevFiles) => prevFiles.filter((_, i) => i !== index));

		// Remove progress for the removed file
		setProgress((prevProgress) => {
			const updatedProgress = { ...prevProgress };
			delete updatedProgress[fileName];
			return updatedProgress;
		});
		// Get the abort controller for the file being removed
		const abortController = abortControllers[fileName];
		if (abortController) {
			// If an abort controller exists, abort the upload
			abortController.abort();
			// Remove the abort controller from the state
			setAbortControllers((prevControllers) => {
				const updatedControllers = { ...prevControllers };
				delete updatedControllers[fileName];
				return updatedControllers;
			});
		}
	};

	// - - - - - - - - - - - - - - onDrop Hook - - - - - - - - - - - - - -
	const baseStyle = {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		padding: "1px",
		borderWidth: 2,
		borderRadius: 2,
		borderColor: "#668AD7",
		borderStyle: "dashed",
		transition: "border .3s ease-in-out",
		padding: "24px",
	};

	const { getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject } = useDropzone({
		onDrop,
		accept: typeFile ? typeFile : DefaultTypeFile,
	});

	const style = React.useMemo(
		() => ({
			...baseStyle,
			...(isDragActive ? activeStyle : {}),
			...(isDragAccept ? acceptStyle : {}),
			...(isDragReject ? rejectStyle : {}),
		}),
		[isDragActive, isDragReject, isDragAccept]
	);
	return (
		<section>
			<div {...getRootProps({ style })} className="mb-2">
				<input {...getInputProps()} />
				<div className={styles.flex__column}>
					<img src={uploadFileIcon} width="42px" height="42px" alt="uploadFileIcon" />
					<div className={styles.flex}>
						<span className={styles.upload__file__text}>{tr`Brows files`}</span>
						<span className={styles.drag__file__text}>{tr`or Drag your file(s) to start uploading`}</span>
					</div>
					<span className={styles.max__size__text}>
						{tr`max uploading size is`} {sizeFile / 1000} GB
					</span>
				</div>
			</div>
			<RFilesList files={files} progress={progress} removeFile={removeFile} />
		</section>
	);
}

export default RFileUploaderInFileManagement;
