import React from "react";
import UploaderAsFormData from "./UploaderAsFormData";
import UploaderAsChunk from "./UploaderAsChunk";

const RUploader = ({
	parentCallback,
	singleFile,
	theme,
	fileTypes,
	fileSize,
	placeholder,
	value,
	buttonText,
	buttonIcon,
	buttonColor,
	formData,
	chunk,
	method,
	api,
}) => {
	const baseStyle = {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		padding: "1px",
		borderWidth: 2,
		borderRadius: 2,
		borderColor: "#668AD7",
		borderStyle: "dashed",
		transition: "border .3s ease-in-out",
		padding: "24px",
	};

	return formData ? (
		<UploaderAsFormData
			baseStyle={baseStyle}
			formData={formData}
			parentCallback={parentCallback}
			fileTypes={fileTypes}
			fileSize={fileSize}
			singleFile={singleFile}
			placeholder={placeholder}
			value={value}
			theme={theme}
			buttonText={buttonText}
			buttonIcon={buttonIcon}
			buttonColor={buttonColor}
		/>
	) : (
		<UploaderAsChunk chunk={chunk} method={method} api={api} />
	);
};

export default RUploader;
