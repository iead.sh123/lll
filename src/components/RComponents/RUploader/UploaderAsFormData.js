import React, { useState, useEffect } from "react";
import { DefaultFileTypes } from "./constants";
import { useDropzone } from "react-dropzone";
import { Services } from "engine/services";
import { toast } from "react-toastify";
import styles from "./RUploader.module.scss";
import store from "store";
import tr from "components/Global/RComs/RTranslator";
import { readFile } from "utils/readFile";
import FileLists from "./FileLists";
import DefaultFileUploaderTheme from "./themes/DefaultFileUploaderTheme";
import ButtonFileUploaderTheme from "./themes/ButtonFileUploaderTheme";

const UploaderAsFormData = ({
	baseStyle,
	formData,
	parentCallback,
	fileTypes,
	fileSize = 5000,
	singleFile,
	placeholder,
	value = [],
	theme,
	buttonText,
	buttonIcon,
	buttonColor,
}) => {
	const [files, setFiles] = useState(value);

	useEffect(() => {
		if (parentCallback) parentCallback(singleFile ? files[0] : files);
	}, [files]);

	// - - - - - - - - - - - - - - onDrop Function - - - - - - - - - - - - - -

	const onDrop = async (acceptedFiles) => {
		// if ((singleFile && acceptedFiles.length > 1) || (singleFile && acceptedFiles.length == 1 && files.length > 0)) {
		// 	// Display an error message or prevent further processing
		// 	toast.warning(tr`Multiple files not allowed`);
		// 	return;
		// }

		if (singleFile) {
			setFiles(acceptedFiles);
		} else {
			setFiles((prevFiles) => [...prevFiles, ...acceptedFiles]);
		}
	};

	// - - - - - - - - - - - - - - onDrop Hook - - - - - - - - - - - - - -

	const { getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject } = useDropzone({
		onDrop,
		accept: fileTypes ? fileTypes : DefaultFileTypes,
		multiple: singleFile ? false : true,
	});

	const style = React.useMemo(
		() => ({
			...baseStyle,
			...(isDragActive ? activeStyle : {}),
			...(isDragAccept ? acceptStyle : {}),
			...(isDragReject ? rejectStyle : {}),
		}),
		[isDragActive, isDragReject, isDragAccept]
	);

	return (
		<React.Fragment>
			{theme == "default" ? (
				<DefaultFileUploaderTheme
					getRootProps={getRootProps}
					getInputProps={getInputProps}
					files={files}
					formData={formData}
					fileSize={fileSize}
					progress={null}
					removeFile={null}
					style={style}
				/>
			) : theme == "button" ? (
				<ButtonFileUploaderTheme
					getRootProps={getRootProps}
					getInputProps={getInputProps}
					files={files}
					formData={formData}
					fileSize={fileSize}
					buttonText={buttonText}
					buttonIcon={buttonIcon}
					buttonColor={buttonColor}
					style={style}
				/>
			) : (
				""
			)}
		</React.Fragment>
	);
};

export default UploaderAsFormData;
