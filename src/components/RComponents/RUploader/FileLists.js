import React from "react";
import styles from "./RUploader.module.scss";
import { Progress } from "reactstrap";
import {
	imageTypes,
	fileTypes,
	fileExcel,
	fileWord,
	filePowerPoint,
	filePdf,
	fileVideo,
	fileAudio,
	fileZip,
	powerPointIcon,
	excelIcon,
	videoIcon,
	audioIcon,
	fileIcon,
	wordIcon,
	pdfIcon,
	zipIcon,
	defaultImage,
} from "config/mimeTypes";

const FileLists = ({ files, progress, removeFile, formData }) => {
	const thumbs = files?.map((fileObj, index) => {
		const type = fileObj?.type;
		const fileName = fileObj?.name;

		const fSource = fileTypes.includes(type)
			? fileIcon
			: fileWord.includes(type)
			? wordIcon
			: fileExcel.includes(type)
			? excelIcon
			: filePowerPoint.includes(type)
			? powerPointIcon
			: filePdf.includes(type)
			? pdfIcon
			: fileVideo.includes(type)
			? videoIcon
			: fileAudio.includes(type)
			? audioIcon
			: fileZip.includes(type)
			? zipIcon
			: fileObj?.image_url
			? fileObj?.image_url
			: URL.createObjectURL(fileObj);
		return (
			<div key={index}>
				<div className={styles.image__container}>
					<img className={styles.image__content} src={fSource} alt={fileName} />
					{/* <div className={styles.overlay}></div> */}

					{!formData && (
						<div className={styles.progress}>
							<Progress animated={true} barClassName={styles.progress__color} value={progress[fileObj?.file?.name]}>
								{/* {progress[fileObj?.file?.name]} % */}
							</Progress>
						</div>
					)}
				</div>

				<div className={styles.image__title}>
					{/* <i className={"fa fa-trash"} onClick={() => removeFile(index, fileName)} style={{ color: "#fb404b", cursor: "pointer" }} /> */}
					<p>{fileName?.length > 10 ? `${fileName.substring(0, 10)} ..` : fileName}</p>
				</div>
			</div>
		);
	});

	return <section className={styles.files__list}>{thumbs}</section>;
};

export default FileLists;
