import React from "react";
import FileLists from "../FileLists";
import { Button } from "ShadCnComponents/ui/button";

const ButtonFileUploaderTheme = ({ getRootProps, getInputProps, files, formData, buttonIcon, style }) => {
	return (
		<section>
			<div {...getRootProps({ ...style, display: "none" })} className="mb-2">
				<input {...getInputProps()} />
				{files && files.length > 0 ? (
					<FileLists files={files} progress={null} removeFile={null} formData={formData} />
				) : (
					<Button type="button" className="bg-themeSecondary hover:bg-themeSecondary !shadow-none" size="icon">
						<i className={buttonIcon + " text-themePrimary"} />
					</Button>
				)}
			</div>
		</section>
	);
};

export default ButtonFileUploaderTheme;
