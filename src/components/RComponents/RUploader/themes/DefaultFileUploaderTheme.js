import React from "react";
import uploadFileIcon from "assets/img/svg/upload.svg";
import styles from "../RUploader.module.scss";
import tr from "components/Global/RComs/RTranslator";
import FileLists from "../FileLists";

const DefaultFileUploaderTheme = ({ getRootProps, getInputProps, files, formData, fileSize, progress, removeFile, style }) => {
	return (
		<section>
			<div {...getRootProps({ style })} className="mb-2">
				<input {...getInputProps()} />
				<div className={styles.flex__column}>
					<img src={uploadFileIcon} width="42px" height="42px" alt="uploadFileIcon" />
					<div className={styles.flex}>
						<span className={styles.upload__file__text}>{tr`Browse files`}</span>
						<span className={styles.drag__file__text}>{tr`or Drag your file to start uploading`}</span>
					</div>
					<span className={styles.max__size__text}>
						{tr`max uploading size is`} {fileSize / 1000} GB
					</span>
				</div>
			</div>
			<FileLists files={files} progress={progress} removeFile={removeFile} formData={formData} />
		</section>
	);
};

export default DefaultFileUploaderTheme;
