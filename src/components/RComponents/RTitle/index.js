import React from "react";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import styles from "./RTitle.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RTitle = ({ text, count = 0, actionHandler, disabled }) => {
	return (
		<RFlex styleProps={{ justifyContent: "space-between", alignItems: "center" }}>
			<RFlex>
				<span className={styles.text}>{text}</span>
				<div className={styles.count}>
					<span>{count}</span>
				</div>
			</RFlex>
			{actionHandler && (
				<RButton text={actionHandler.name} onClick={actionHandler.handleClick} color="primary" faicon={iconsFa6.plus} disabled={disabled} />
			)}
		</RFlex>
	);
};

export default RTitle;
