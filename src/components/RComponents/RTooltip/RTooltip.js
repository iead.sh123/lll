import { Tooltip, TooltipContent, TooltipProvider, TooltipTrigger } from "ShadCnComponents/ui/tooltip";
import { cn } from "lib/utils";

const RTooltip = ({
	trigger = <span>Trigger Text</span>,
	tooltipText = "hoverText",
	triggerClassName = "",
	tooltipBackground = "bg-white text-black",
}) => {
	return (
		<TooltipProvider className="bg-black">
			<Tooltip className="bg-black">
				<TooltipTrigger className={triggerClassName} asChild>
					{trigger}
				</TooltipTrigger>
				<TooltipContent className={cn("border-[1px]", tooltipBackground)}>{tooltipText}</TooltipContent>
			</Tooltip>
		</TooltipProvider>
	);
};
export default RTooltip;
