import React from "react";
import { Dialog, DialogHeader, DialogFooter, DialogTitle, DialogDescription, DialogContent } from "ShadCnComponents/ui/dialog";

const RControlledDialog = ({
	isOpen = false,
	closeDialog,
	dialogHeader = null,
	dialogBody = null,
	dialogFooter = null,
	contentClassName = "max-w-fit", // or max-w-fit it depend on the designer
}) => {
	return (
		<Dialog open={isOpen}>
			<DialogContent closeDialog={closeDialog} className={contentClassName + " overflow-y-scroll max-h-screen"}>
				{dialogHeader && (
					<DialogHeader>
						<DialogTitle>{dialogHeader?.title}</DialogTitle>
						<DialogDescription>{dialogHeader?.description}</DialogDescription>
					</DialogHeader>
				)}
				{dialogBody && dialogBody}
				{dialogFooter && <DialogFooter>{dialogFooter}</DialogFooter>}
			</DialogContent>
		</Dialog>
	);
};

export default RControlledDialog;
