import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import tr from "components/Global/RComs/RTranslator";

const RReadMore = ({ numberChart = 127, text, redirect }) => {
	const history = useHistory();
	const [show, setShow] = useState(false);

	const handleShowText = () => {
		setShow(!show);
	};
	return (
		<div>
			{text?.length > numberChart ? (
				<span className="flex gap-2">
					<span>
						{text.substring(0, numberChart)}
						{"..."}&nbsp;
						<span
							className="text-themePrimary"
							onClick={() => {
								redirect ? history.push(redirect) : handleShowText();
							}}
						>{tr`read_more`}</span>
					</span>
				</span>
			) : (
				<span className="flex gap-2">
					<span>{text}</span>
					{show && !redirect && <span className="text-themePrimary" onClick={() => handleShowText()}>{tr`show_less`}</span>}
				</span>
			)}
		</div>
	);
};

export default RReadMore;
