import * as React from "react";
import { Table, TableBody, TableCell, TableHead, TableHeader, TableRow } from "ShadCnComponents/ui/table";
import { ConvertRecordsToNewForm } from "utils/ConvertRecordsToNewForm";
import {
	flexRender,
	getCoreRowModel,
	getFilteredRowModel,
	getPaginationRowModel,
	getSortedRowModel,
	useReactTable,
} from "@tanstack/react-table";
import RDropdown from "../RDropdown/RDropdown";
import RAlertDialog from "../RAlertDialog";
import RTooltip from "../RTooltip/RTooltip";

const RTable = ({ Records, convertRecordsShape = true, containerClassName = "max-h-[450px]", emptyData, callBack }) => {
	const [sorting, setSorting] = React.useState([]);
	const [columnFilters, setColumnFilters] = React.useState([]);
	const [columnVisibility, setColumnVisibility] = React.useState({});
	const [rowSelection, setRowSelection] = React.useState({});
	// const [pagination, setPagination] = React.useState({
	// 	pageIndex: 0,
	// 	pageSize: -2,
	// });
	let FormatedRecords = null;
	convertRecordsShape ? (FormatedRecords = ConvertRecordsToNewForm(Records)) : "";

	const columns = Records.columns
		?.map((columnInfo) => ({
			id: columnInfo.id ?? undefined,
			accessorKey: columnInfo.accessorKey ?? undefined,
			header: (info) => columnInfo.renderHeader(info),
			cell: (info) => columnInfo.renderCell(info),
			size: columnInfo.size ? columnInfo.size : undefined,
			minSize: columnInfo.minSize ? columnInfo.minSize : undefined,
			maxSize: columnInfo.maxSize ? columnInfo.maxSize : undefined,
		}))
		.concat(
			Records?.actions?.length > 0
				? {
						id: "actions",
						header: () => "",
						cell: (info) => (
							<div className="flex gap-3 items-center">
								{Records.actions
									.filter((action) => !action.inDropdown)
									.map((action, index) => {
										const iconComponent = (
											<RTooltip
												trigger={
													<i
														key={index}
														className={`${action.icon} ${action.actionIconClass} cursor-pointer`}
														aria-label={action.name ?? "default"}
														onClick={() => !action.inDialog && action.onClick(info)}
													/>
												}
												tooltipText={action.name}
											/>
										);
										return action.inDialog ? (
											<RAlertDialog
												key={index}
												component={iconComponent}
												title={action.dialogTitle && action.dialogTitle(info)}
												description={action.dialogDescription && action.dialogDescription(info)}
												cancel={action.cancel}
												confirm={action.confirm}
												loading={action.loading}
												confirmAction={() => action.confirmAction(info)}
												disabled={action.disabled}
												headerItemsPosition={action.headerItemsPosition}
											/>
										) : (
											iconComponent
										);
									})}
								{Records.actions.some((action) => action.inDropdown) && (
									<RDropdown
										actions={Records.actions
											.filter((action) => action.inDropdown)
											.map((action, index) => ({
												...action,
												onClick: () => action.onClick(info),
											}))}
									/>
								)}
							</div>
						),
				  }
				: []
		);

	const data = Records.data ?? [];

	const table = useReactTable({
		data: FormatedRecords ? FormatedRecords.data : data,
		columns: FormatedRecords ? FormatedRecords.columns : columns,
		onSortingChange: setSorting,
		onColumnFiltersChange: setColumnFilters,
		getCoreRowModel: getCoreRowModel(),
		getPaginationRowModel: getPaginationRowModel(),
		getSortedRowModel: getSortedRowModel(),
		getFilteredRowModel: getFilteredRowModel(),
		onColumnVisibilityChange: setColumnVisibility,
		onRowSelectionChange: setRowSelection,
		// onPaginationChange: setPagination,
		state: {
			sorting,
			columnFilters,
			columnVisibility,
			rowSelection,
			// pagination: false,
		},
		manualPagination: true,
	});
	React.useEffect(() => {
		if (callBack) {
			callBack(table);
		}
	}, [table]);
	return (
		<div className={`rounded-md border w-full`}>
			<Table containerClassName={containerClassName}>
				<TableHeader>
					{table.getHeaderGroups()?.map((headerGroup) => (
						<TableRow key={headerGroup.id}>
							{headerGroup.headers?.map((header) => {
								return (
									<TableHead key={header.id}>
										{header.isPlaceholder ? null : flexRender(header.column.columnDef.header, header.getContext())}
									</TableHead>
								);
							})}
						</TableRow>
					))}
				</TableHeader>
				<TableBody>
					{table.getRowModel().rows?.length ? (
						table.getRowModel().rows?.map((row) => (
							<TableRow key={row.id} data-state={row.getIsSelected() && "selected"}>
								{row.getVisibleCells()?.map((cell) => (
									<TableCell key={cell.id}>{flexRender(cell.column.columnDef.cell, cell.getContext())}</TableCell>
								))}
							</TableRow>
						))
					) : (
						<TableRow>
							<TableCell colSpan={columns?.length} className="h-24 text-center">
								{emptyData ? emptyData : "No results."}
							</TableCell>
						</TableRow>
					)}
				</TableBody>
			</Table>
		</div>
	);
};

export default RTable;
