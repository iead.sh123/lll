import { DotsHorizontalIcon } from "@radix-ui/react-icons";
import { Button } from "ShadCnComponents/ui/button";
import {
	DropdownMenu,
	DropdownMenuCheckboxItem,
	DropdownMenuContent,
	DropdownMenuGroup,
	DropdownMenuItem,
	DropdownMenuLabel,
	DropdownMenuPortal,
	DropdownMenuSeparator,
	DropdownMenuShortcut,
	DropdownMenuSub,
	DropdownMenuSubContent,
	DropdownMenuSubTrigger,
	DropdownMenuTrigger,
} from "ShadCnComponents/ui/dropdown-menu";
import RFlex from "components/Global/RComs/RFlex/RFlex";

const RCheckDropdown = ({ TriggerComponent = null, label = null, actions = [], setActions, multiFilter = false }) => {
	return (
		<DropdownMenu>
			<DropdownMenuTrigger asChild>
				{TriggerComponent ? (
					TriggerComponent
				) : (
					<Button variant="ghost" className="h-8 w-8 p-0">
						<span className="sr-only">Open menu</span>
						<DotsHorizontalIcon className="h-4 w-4" />
					</Button>
				)}
			</DropdownMenuTrigger>
			<DropdownMenuContent align="end">
				{label && <DropdownMenuLabel>{label}</DropdownMenuLabel>}
				{Object.keys(actions).map((key, index) => (
					<>
						<DropdownMenuCheckboxItem
							className="pr-[0.5rem]"
							key={index}
							checked={actions[key].checked}
							onCheckedChange={() => {
								actions[key].onCheckedChange(actions[key].checked);
								multiFilter
									? setActions((oldActions) => ({ ...oldActions, [key]: { ...oldActions[key], checked: !oldActions[key].checked } }))
									: setActions((oldActions) => {
											const newState = {};
											Object.keys(oldActions).forEach((innerKey, index) => {
												newState[innerKey] = { ...oldActions[innerKey], checked: innerKey == key ? !oldActions[innerKey].checked : false };
											});
											return newState;
									  });
							}}
						>
							{actions[key].component ? (
								actions[key].component
							) : (
								<RFlex className="items-center">
									{actions[key].icon && !actions[key].iconOnRight && (
										<i className={`${actions[key].icon} ${actions[key].actionIconClass}`} />
									)}
									<span className={`${actions[key].actionTextStyle}`}>{actions[key].name}</span>
									{actions[key].icon && actions[key].iconOnRight && (
										<i className={`${actions[key].icon} ${actions[key].actionIconClass}`} />
									)}
								</RFlex>
							)}
						</DropdownMenuCheckboxItem>
						{actions[key].addSeperator && <DropdownMenuSeparator />}
					</>
				))}
			</DropdownMenuContent>
		</DropdownMenu>
	);
};

export default RCheckDropdown;
