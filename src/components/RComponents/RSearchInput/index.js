import React from "react";
import iconsFa6 from "variables/iconsFa6";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Input } from "ShadCnComponents/ui/input";

const RSearchInput = ({
	searchLoading,
	searchData,
	handleSearch,
	setSearchData,
	handleChangeSearch,
	inputPlaceholder,
	disabledOnInput,
	className = "w-[270px]",
}) => {
	return (
		<RFlex className={`relative ${className}`}>
			<Input
				type="text"
				placeholder={inputPlaceholder ?? tr`search`}
				value={searchData}
				onChange={(event) => {
					event.preventDefault();
					handleChangeSearch(event.target.value);
				}}
				onKeyDown={(event) => {
					if (event.key === "Enter" && handleSearch) {
						handleSearch();
					}
				}}
				disabled={disabledOnInput ? disabledOnInput : false}
				className="w-full rounded-md border px-8"
			/>

			{searchLoading ? (
				<RFlex>
					<i className={`${iconsFa6.spinner} absolute left-2 top-[30%] text-themeLight  cursor-pointer`} />
				</RFlex>
			) : (
				<i
					className={`${iconsFa6.search} absolute left-3 top-[59%] -translate-y-1/2 h-5 w-5 text-themeLight  cursor-pointer`}
					onClick={() => handleSearch()}
				/>
			)}

			{searchData == "" ? (
				""
			) : (
				<i
					aria-hidden="true"
					className={`absolute right-3 top-[59%] -translate-y-1/2 h-5 w-5 text-themeLight cursor-pointer ${iconsFa6.close}`}
					onClick={() => {
						if (searchData !== "") {
							setSearchData && setSearchData("");
							handleSearch("");
						}
					}}
				/>
			)}
		</RFlex>
	);
};

export default RSearchInput;
