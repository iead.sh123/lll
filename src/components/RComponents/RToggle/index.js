import React from "react";
import Switch from "react-bootstrap-switch";

const RToggle = ({ value, onText = "On", offText = "Off", onColor, offColor, onChange, style }) => {
	return (
		<Switch
			bsSize={"null"}
			style={style}
			onText={onText}
			offText={offText}
			defaultValue={value}
			onColor={onColor}
			offColor={offColor}
			onChange={(e) => {
				onChange(e.state.value);
			}}
		/>
	);
};
export default RToggle;
