import React from "react";

const StaticColor = ({ data, handlers }) => {
	return (
		<div className="flex flex-col gap-3">
			{data?.values?.colors
				?.filter((color) => !data?.values?.selectedOptions?.some((option) => option.color_id === color.id))
				?.map((color) => (
					<div
						key={color.id}
						className="flex items-center cursor-pointer gap-2"
						onClick={() =>
							handlers.handleSelectStaticColor({
								value: color.id,
								label: color.name,
								color_id: color.id,
								color_hex: color.color_hex,
								static_color: true,
							})
						}
					>
						<div className={`w-[2vh] h-[2vh] rounded-full`} style={{ background: color.color_hex }}></div>
						<span style={{ color: color.color_hex }}>{color.name}</span>
					</div>
				))}
		</div>
	);
};

export default StaticColor;
