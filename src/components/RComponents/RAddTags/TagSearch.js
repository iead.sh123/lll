import React, { useCallback } from "react";
import { tagApis } from "api/tag";
import AsyncSelect from "react-select/async";
import tr from "components/Global/RComs/RTranslator";

const debounce = (fn, delay) => {
	let timer;
	return (...args) => {
		clearTimeout(timer);
		timer = setTimeout(() => fn(...args), delay);
	};
};

const TagSearch = ({
	getArrayFromResponse,
	getValueFromArrayItem,
	getLabelFromArrayItem,
	placeholder,
	disabled = false,
	onChange,
	onBlur,
	value,
}) => {
	const fetchOptions = async (inputValue) => {
		try {
			const response = await tagApis.search({ name: inputValue, organization_id: 1 });

			const dataArray = getArrayFromResponse(response);

			let options = dataArray.map((item) => ({
				value: getValueFromArrayItem(item),
				label: getLabelFromArrayItem(item),
			}));

			// Check if inputValue is already present in options
			const inputExists = options.some((option) => option.label === inputValue);

			// If inputValue is not found, add it as a new option
			if (!inputExists) {
				options.push({ value: inputValue, label: inputValue, id: null });
			}

			return options;
		} catch (error) {
			return [];
		}
	};

	const loadOptions = useCallback(
		debounce((inputValue, callback) => {
			fetchOptions(inputValue).then(callback);
		}, 1000),
		[]
	);
	const customStyles = {
		control: (baseStyles) => ({
			...baseStyles,
		}),
	};
	return (
		<AsyncSelect
			styles={customStyles}
			aria-label="Tag search select"
			isMulti
			loadOptions={loadOptions}
			placeholder={placeholder ?? tr("select") + "..."}
			isDisabled={disabled}
			onChange={onChange}
			onBlur={onBlur}
			value={value}
		/>
	);
};

export default TagSearch;
