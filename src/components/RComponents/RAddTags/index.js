import React from "react";
import StaticColor from "./StaticColor";
import TagSearch from "./TagSearch";
import iconsFa6 from "variables/iconsFa6";
import Loader from "utils/Loader";
import tr from "components/Global/RComs/RTranslator";
import { Popover, PopoverContent, PopoverTrigger } from "ShadCnComponents/ui/popover";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useFormik } from "formik";
import { lightGray } from "config/constants";
import { tagApis } from "api/tag";

const RAddTags = ({ dataFromBackend, parentCallBack, disabled = false }) => {
	// - - - - - - - - - - - - - - Formik - - - - - - - - - - - - - -
	const initialValues = {
		colors: [],
		selectedOptions:
			dataFromBackend && dataFromBackend?.length > 0
				? dataFromBackend.map((option) => ({
						label: option.name ?? option?.user_tag?.color?.name,
						value: option.id ?? option?.user_tag?.color?.name, // because if i need to remove color not remove all colors
						color_id: option.color_id ?? option?.user_tag?.color?.id,
						color_hex: option.color_hex ?? option?.user_tag?.color?.color_hex,
						static_color: !option.id && !option.name ? true : false, // because i need to set false if doesn't static color
				  }))
				: [],
		selectedOptionsToBackend: [],
	};

	const { values, setFieldValue } = useFormik({
		initialValues: initialValues,
	});

	// - - - - - - - - - - - - - Queries - - - - - - - - - - - - - -
	const colorsData = useFetchDataRQ({
		queryKey: ["colors"],
		queryFn: () => tagApis.colors(),
		onSuccessFn: ({ data }) => {
			setFieldValue("colors", data.data);
		},
		enableCondition: !disabled ? true : false,
	});

	// - - - - - - - - - - - - - Handlers - - - - - - - - - - - - - -

	const handleDataBeforeSendingToBackend = (allData) => {
		{
			/*
				This IS Format Sending Data To Backend
			 	Static Color: { id: null , name: null , color_id: 1 } 
			  	New Tag: { id: null , name: 'new tag' , color_id: 1 }
			    Tag Existing: { id: 1 , name: 'tag1' , color_id: 1 }
			 */
		}

		const formattedOptions = allData.map((option) => ({
			name: option?.static_color ? null : option.label,
			id: option.id === null || option?.static_color ? null : option.value,
			color_id: option.color_id ?? undefined,
		}));

		setFieldValue("selectedOptionsToBackend", formattedOptions);
		parentCallBack(formattedOptions);
	};

	const handleTagChange = (selectedTags) => {
		setFieldValue("selectedOptions", selectedTags);
		handleDataBeforeSendingToBackend(selectedTags);
	};

	const handleSelectStaticColor = (staticColor) => {
		const concatData = values.selectedOptions.concat(staticColor);
		setFieldValue("selectedOptions", concatData);
		handleDataBeforeSendingToBackend(concatData);
	};

	if (colorsData.isLoading && colorsData.fetchStatus !== "idle")
		return (
			<div>
				<i className={iconsFa6.spinner} />
			</div>
		);
	return (
		<div>
			{/* <Popover className="bg-slate-400  !min-h-[250px]"> */}
			<Popover>
				<PopoverTrigger>
					{values.selectedOptions?.length > 0 ? (
						<div className="flex">
							{values.selectedOptions?.map((option, ind) => (
								<div
									key={ind}
									className="position-relative w-[3vh] h-[3vh] rounded-full"
									style={{
										border: !option?.color_hex || [option?.color_hex].includes("#fff", "#ffffff", "white") ? `1px solid ${lightGray}` : "",
										background: option?.color_hex ? option?.color_hex : "#fff",
										right: `${ind * 4}px`,
									}}
								></div>
							))}
						</div>
					) : (
						<span className="flex items-center gap-2 text-themePrimary">
							<i className={iconsFa6.plus} alt="add" />
							<span>{tr`add_tag`}</span>
						</span>
					)}
				</PopoverTrigger>
				<PopoverContent className="p-3">
					<div className="flex flex-col gap-2">
						<span>{tr`assign_tag`}</span>

						<TagSearch
							getArrayFromResponse={(res) => (res && res.data && res.data.status ? res.data.data : [])}
							getValueFromArrayItem={(i) => i.id}
							getLabelFromArrayItem={(i) => i.name}
							onChange={handleTagChange}
							value={values.selectedOptions}
							disabled={disabled}
						/>

						{!disabled && <StaticColor data={{ values }} handlers={{ handleSelectStaticColor }} />}
					</div>
				</PopoverContent>
			</Popover>
		</div>
	);
};

export default RAddTags;
