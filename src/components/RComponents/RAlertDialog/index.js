import tr from "components/Global/RComs/RTranslator";
import React from "react";
import {
	AlertDialog,
	AlertDialogAction,
	AlertDialogCancel,
	AlertDialogContent,
	AlertDialogDescription,
	AlertDialogFooter,
	AlertDialogHeader,
	AlertDialogTitle,
	AlertDialogTrigger,
} from "ShadCnComponents/ui/alert-dialog";
import { Button } from "ShadCnComponents/ui/button";

const RAlertDialog = ({
	component = <Button>{tr("Open_Alert_Dialog")}</Button>,
	title = tr("Are_you_sure_you_want_to_delete"),
	description = tr("This_action_cannot_be_undone"),
	cancel = { text: tr("cancel"), className: "text-black border-input" },
	confirm = { text: tr("yes,delete it"), className: "bg-themeDanger", action: () => {} },
	confirmAction = () => {},
	loading = false,
	disabled = false,
	headerItemsPosition = "items-center",
}) => {
	return (
		<AlertDialog>
			<AlertDialogTrigger>{component}</AlertDialogTrigger>
			<AlertDialogContent>
				<AlertDialogHeader className={headerItemsPosition}>
					<AlertDialogTitle className={"font-bold text-[16px]"}>{title}</AlertDialogTitle>
					<AlertDialogDescription>{description}</AlertDialogDescription>
				</AlertDialogHeader>
				<AlertDialogFooter className="items-end">
					<AlertDialogCancel className={cancel.className} variant="link">
						{cancel.text}
					</AlertDialogCancel>
					<AlertDialogAction onClick={() => confirmAction()} className={confirm.className}>
						{confirm.text}
					</AlertDialogAction>
				</AlertDialogFooter>
			</AlertDialogContent>
		</AlertDialog>
	);
};

export default RAlertDialog;
