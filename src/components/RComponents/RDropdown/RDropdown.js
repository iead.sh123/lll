import { DotsHorizontalIcon } from "@radix-ui/react-icons";
import { Button } from "ShadCnComponents/ui/button";
import {
	DropdownMenu,
	DropdownMenuContent,
	DropdownMenuGroup,
	DropdownMenuItem,
	DropdownMenuLabel,
	DropdownMenuPortal,
	DropdownMenuSeparator,
	DropdownMenuShortcut,
	DropdownMenuSub,
	DropdownMenuSubContent,
	DropdownMenuSubTrigger,
	DropdownMenuTrigger,
} from "ShadCnComponents/ui/dropdown-menu";
import RFlex from "components/Global/RComs/RFlex/RFlex";

const RDropdown = ({ TriggerComponent = null, label = null, actions = [] }) => {
	return (
		<DropdownMenu>
			<DropdownMenuTrigger asChild>
				{TriggerComponent ? (
					TriggerComponent
				) : (
					<Button variant="ghost" className="h-8 w-8 p-0">
						<span className="sr-only">Open menu</span>
						<DotsHorizontalIcon className="h-4 w-4" />
					</Button>
				)}
			</DropdownMenuTrigger>
			<DropdownMenuContent align="end">
				{label && <DropdownMenuLabel>{label}</DropdownMenuLabel>}
				{actions.map((action, index) => (
					<>
						<DropdownMenuItem
							key={index}
							onClick={(e) => {
								e.stopPropagation();
								action.onClick();
							}}
						>
							{action.component ? (
								action.component
							) : (
								<RFlex className="items-center">
									{action.icon && !action.iconOnRight && <i className={`${action.icon} ${action.actionIconClass}`} />}
									<span className={`${action.actionTextStyle}`}>{action.name}</span>
									{action.icon && action.iconOnRight && <i className={`${action.icon} ${action.actionIconClass}`} />}
								</RFlex>
							)}
						</DropdownMenuItem>
						{action.addSeperator && <DropdownMenuSeparator />}
					</>
				))}
			</DropdownMenuContent>
		</DropdownMenu>
	);
};

export default RDropdown;
