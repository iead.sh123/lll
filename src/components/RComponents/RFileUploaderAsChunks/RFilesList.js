import React from "react";
import styles from "./RFileUploaderAsChunks.module.scss";
import { Progress } from "reactstrap";
import {
	imageTypes,
	fileTypes,
	fileExcel,
	fileWord,
	filePowerPoint,
	filePdf,
	fileVideo,
	fileAudio,
	fileZip,
	powerPointIcon,
	excelIcon,
	videoIcon,
	audioIcon,
	fileIcon,
	wordIcon,
	pdfIcon,
	zipIcon,
} from "config/mimeTypes";
import { Services } from "engine/services";

const RFilesList = ({ files, progress, removeFile }) => {
	const thumbs = files?.map((fileObj, index) => {
		const type = fileObj?.mimeType;
		const fileName = fileObj?.file?.name ?? fileObj?.name;
		const fSource = imageTypes.includes(type)
			? fileObj.url
				? Services.storage.file + fileObj.url
				: fileObj.preview
			: fileTypes.includes(type)
			? fileIcon
			: fileWord.includes(type)
			? wordIcon
			: fileExcel.includes(type)
			? excelIcon
			: filePowerPoint.includes(type)
			? powerPointIcon
			: filePdf.includes(type)
			? pdfIcon
			: fileVideo.includes(type)
			? videoIcon
			: fileAudio.includes(type)
			? audioIcon
			: fileZip.includes(type)
			? zipIcon
			: Services.storage.file + fileObj.url;
		return (
			<div key={index}>
				<div className={styles.image__container}>
					<img width="100px" height="100px" src={fSource} alt={fileName} />
					{/* <div className={styles.overlay}></div> */}

					{!fileObj?.url && (
						<div className={styles.progress}>
							<Progress animated={true} barClassName={styles.progress__color} value={progress[fileObj?.file?.name]}>
								{/* {progress[fileObj?.file?.name]} % */}
							</Progress>
						</div>
					)}
				</div>

				<div className={styles.image__title}>
					{/* <i className={"fa fa-trash"} onClick={() => removeFile(index, fileName)} style={{ color: "#fb404b", cursor: "pointer" }} /> */}
					<p>{fileName?.length > 10 ? `${fileName.substring(0, 10)} ..` : fileName}</p>
				</div>
			</div>
		);
	});

	return <section className={styles.files__list}>{thumbs}</section>;
};

export default RFilesList;
