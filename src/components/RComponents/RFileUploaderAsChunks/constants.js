export const DefaultTypeFile =
	"video/x-flv , video/mp4,application/x-mpegURL , video/MP2T , video/3gpp , video/quicktime , video/x-msvideo , video/x-ms-wmvimage/jpeg , image/* , image/png  , image/jpg ,  image/svg ,  image/svg+xml ,  .rar ,  .zip ,  application/x-zip-compressed ,   application/pdf , application/vnd.openxmlformats-officedocument ,  application/vnd.openxmlformats-officedocument.wordprocessingml.document";
export const CHUNK_SIZE = 1024 * 2048;

export function bytesToMegabytes(bytes) {
	return bytes / (1024 * 1024);
}

export function calculateChunkSize(fileSize) {
	if (fileSize > 100 * 1024 * 1024) {
		return 100 * 1024 * 1024; // 100 MB
	} else if (fileSize > 50 * 1024 * 1024) {
		return 10 * 1024 * 1024; // 10 MB
	} else if (fileSize > 5 * 1024 * 1024) {
		return 5 * 1024 * 1024; // 5 MB
	} else {
		return 2 * 1024 * 1024; // 2 MB
	}
}
