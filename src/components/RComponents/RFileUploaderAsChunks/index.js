import React, { useState, useEffect } from "react";
import { DefaultTypeFile, calculateChunkSize } from "./constants";
import { useDropzone } from "react-dropzone";
import { Services } from "engine/services";
import { readFile } from "utils/readFile";
import { toast } from "react-toastify";
import uploadFileIcon from "assets/img/svg/upload.svg";
import RFilesList from "./RFilesList";
import styles from "./RFileUploaderAsChunks.module.scss";
import store from "store";
import tr from "components/Global/RComs/RTranslator";
import ButtonFileUploaderTheme from "./themes/ButtonFileUploaderTheme";
import DefaultFileUploaderTheme from "./themes/DefaultFileUploaderTheme";
import iconsFa6 from "variables/iconsFa6";
import { newAuthToken, newAuth } from "engine/config";
function RFileUploaderAsChunks({
	parentCallbackToFillData,
	typeFile,
	sizeFile = 5000,
	singleFile = true,
	contextId = "",
	contextName = "",
	uploaderType = "default",
	buttonText = tr("Click"),
	buttonIcon = iconsFa6.paperclip,
	buttonColor = "primary",
	apiURL,
	method = "POST",
	isSync = true,
}) {
	const [files, setFiles] = useState([]);
	const [progress, setProgress] = useState({});
	const [abortControllers, setAbortControllers] = useState({});
	const [filesFromBackend, setFilesFromBackend] = useState({});

	useEffect(() => {
		if (filesFromBackend.id) parentCallbackToFillData(filesFromBackend);
	}, [filesFromBackend]);

	// - - - - - - - - - - - - - - onDrop Function - - - - - - - - - - - - - -

	const onDrop = async (acceptedFiles) => {
		if ((singleFile && acceptedFiles.length > 1) || (singleFile && acceptedFiles.length == 1 && files.length > 0)) {
			// Display an error message or prevent further processing
			toast.warning(tr`Multiple files not allowed`);

			return;
		}
		for (const file of acceptedFiles) {
			try {
				const abortController = new AbortController();
				const signal = abortController.signal;

				// Save the abort controller for the file
				setAbortControllers((prevControllers) => ({
					...prevControllers,
					[file.name]: abortController,
				}));

				processFile(file, signal);
			} catch (error) {
				console.error("Error initiating file upload:", error);
			}
		}
	};

	// - - - - - - - - - - - - - - Progress Function - - - - - - - - - - - - - -

	const processFile = async (file, signal) => {
		const chunkSize = calculateChunkSize(file.size);
		const totalChunks = Math.ceil(file.size / chunkSize);
		const fileName = file.name;
		const fileMimeType = file.mime_type ?? file.type;
		const chunkProgressArray = Array(totalChunks).fill(0); // Initialize array to track progress of each chunk

		// Read file and generate preview
		const filePreview = await readFile(file);
		setFiles((prevFiles) => [
			...prevFiles,
			{
				file,
				preview: filePreview,
				mimeType: fileMimeType,
			},
		]);

		for (let i = 0; i < totalChunks; i++) {
			if (signal.aborted) {
				return;
			}

			const start = i * chunkSize;
			const end = Math.min(start + chunkSize, file.size);
			const chunk = file.slice(start, end);

			try {
				await uploadChunk(chunk, i, totalChunks, fileName, chunkProgressArray);
			} catch (error) {
				console.error("Error uploading chunk:", error);
			}
		}
	};

	// - - - - - - - - - - - - - - Upload Chunk Function - - - - - - - - - - - - - -

	const uploadChunk = (chunk, chunkNumber, totalChunks, fileName, chunkProgressArray) => {
		return new Promise((resolve, reject) => {
			const formData = new FormData();
			formData.append("chunk", chunk);
			formData.append("totalChunks", totalChunks);
			formData.append("chunkNumber", chunkNumber + 1);
			formData.append("fileName", fileName);
			formData.append("contextname", contextName);
			formData.append("contextid", contextId);

			const token = newAuth ? newAuthToken : store.getState().auth.token;
			const xhr = new XMLHttpRequest();
			const url = apiURL;

			xhr.open(method, url, isSync);
			xhr.setRequestHeader("Authorization", `Bearer ${token}`);

			xhr.upload.addEventListener("progress", (event) => {
				const chunkProgress = Math.round((event.loaded / event.total) * 100);
				chunkProgressArray[chunkNumber] = chunkProgress; // Update progress of current chunk
				const overallProgress = calculateOverallProgress(chunkProgressArray); // Calculate overall progress

				onUploadProgress(fileName, overallProgress); // Update overall progress
			});

			xhr.onload = () => {
				if (xhr.status === 200 || xhr.status === 202) {
					resolve(); // Resolve the promise when the upload is successful
					if (JSON.parse(xhr.response)?.data?.id || JSON.parse(xhr.response)?.data?.done) {
						toast.success(tr`the_upload_was_made_successfully`);
						setFilesFromBackend(JSON.parse(xhr.response)?.data);
					}
				} else {
					reject(xhr.statusText); // Reject the promise if there's an error
				}
			};

			xhr.onerror = () => {
				reject(xhr.statusText); // Reject the promise if there's an error
			};

			xhr.send(formData);
		});
	};

	// - - - - - - - - - - - - - - Calculate Overall Progress - - - - - - - - - - - - - -

	const calculateOverallProgress = (chunkProgressArray) => {
		const totalChunks = chunkProgressArray.length;
		const totalProgress = chunkProgressArray.reduce((acc, curr) => acc + curr, 0);
		return Math.round((totalProgress / (totalChunks * 100)) * 100); // Calculate overall progress as a percentage
	};

	// - - - - - - - - - - - - - - OnUpload Progress - - - - - - - - - - - - - -

	const onUploadProgress = (fileName, overallProgress) => {
		setProgress((prevProgress) => ({
			...prevProgress,
			[fileName]: overallProgress,
		}));
	};

	// - - - - - - - - - - - - - - Remove File - - - - - - - - - - - - - -

	const removeFile = (index, fileName) => {
		setFiles((prevFiles) => prevFiles.filter((_, i) => i !== index));

		// Remove progress for the removed file
		setProgress((prevProgress) => {
			const updatedProgress = { ...prevProgress };
			delete updatedProgress[fileName];
			return updatedProgress;
		});
		// Get the abort controller for the file being removed
		const abortController = abortControllers[fileName];
		if (abortController) {
			// If an abort controller exists, abort the upload
			abortController.abort();
			// Remove the abort controller from the state
			setAbortControllers((prevControllers) => {
				const updatedControllers = { ...prevControllers };
				delete updatedControllers[fileName];
				return updatedControllers;
			});
		}
	};

	// - - - - - - - - - - - - - - onDrop Hook - - - - - - - - - - - - - -
	const baseStyle = {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		padding: "1px",
		borderWidth: 2,
		borderRadius: 2,
		borderColor: "#668AD7",
		borderStyle: "dashed",
		transition: "border .3s ease-in-out",
		padding: "24px",
	};

	const { getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject } = useDropzone({
		onDrop,
		accept: typeFile ? typeFile : DefaultTypeFile,
	});

	const style = React.useMemo(
		() => ({
			...baseStyle,
			...(isDragActive ? activeStyle : {}),
			...(isDragAccept ? acceptStyle : {}),
			...(isDragReject ? rejectStyle : {}),
		}),
		[isDragActive, isDragReject, isDragAccept]
	);

	return uploaderType == "default" ? (
		<DefaultFileUploaderTheme
			getRootProps={getRootProps}
			getInputProps={getInputProps}
			style={style}
			sizeFile={sizeFile}
			files={files}
			progress={progress}
			uploadFileIcon={uploadFileIcon}
			removeFile={removeFile}
		/>
	) : uploaderType == "button" ? (
		<ButtonFileUploaderTheme
			getRootProps={getRootProps}
			getInputProps={getInputProps}
			buttonText={buttonText}
			buttonColor={buttonColor}
			buttonIcon={buttonIcon}
			style={style}
			sizeFile={sizeFile}
		/>
	) : (
		""
	);
}

export default RFileUploaderAsChunks;
