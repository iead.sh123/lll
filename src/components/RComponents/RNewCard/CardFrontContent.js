import React from "react";
import RReadMore from "../RReadMore/RReadMore";
import { cn } from "lib/utils";
import styles from "./RCardNew.module.scss";
import { Card, CardContent } from "ShadCnComponents/ui/card";
import { AspectRatio } from "ShadCnComponents/ui/aspect-ratio";
import iconsFa6 from "variables/iconsFa6";
import tr from "components/Global/RComs/RTranslator";
import RDropdown from "../RDropdown/RDropdown";
import { Button } from "ShadCnComponents/ui/button";

const CardFrontContent = ({ cardData }) => {
	return (
		<Card
			className={cn("flex gap-[10px] flex-col rounded-md !shadow-shadowCard p-[5px] h-[266px]", styles.flip__card__front)}
			key={cardData?.id}
		>
			{/* - - - - - - - - - -  Image Section - - - - - - - - - -  */}
			<section className="h-[49%] relative overflow-hidden rounded-sm">
				{/* <AspectRatio ratio={16 / 9}> */}
				<img
					className="object-cover w-full h-full "
					src={cardData.image ? cardData.image : `https://placehold.co/800x600?text=Guidance`}
					alt={cardData.image}
				/>
				{/* </AspectRatio> */}
				<section className="flex flex-col justify-between absolute bottom-0 left-0 right-0 top-0  p-2">
					<div className="flex justify-between text-white">
						{cardData?.departmentName && (
							<span className="px-2 pt-[1px] rounded-sm bg-primary-rgba items-center">
								{cardData?.departmentName?.length > 15 ? cardData?.departmentName.substring(0, 15) + " ..." : cardData?.departmentName}
							</span>
						)}
						{cardData.actions?.length > 0 && (
							<span className="flex flex-end justify-end w-full">
								<RDropdown
									TriggerComponent={
										<Button variant="ghost" className="h-6 w-6 p-0 hover:bg-primary-rgba hover:text-white bg-primary-rgba rounded-full">
											<i className={iconsFa6.ellipsisV} alt="ellipsisV" />
										</Button>
									}
									actions={cardData.actions}
								/>
							</span>
						)}
					</div>

					<div className="flex justify-between text-white">
						{cardData.published ? (
							<span className="flex gap-2 bg-white px-2 py-[2px] items-center rounded-sm text-themeSuccess text-xs">
								<i className={iconsFa6.check} alt="check" />
								<span>{tr`published`}</span>
							</span>
						) : (
							<span className="flex gap-2 bg-white px-2 py-[2px] items-center rounded-sm text-themeWarning text-xs">
								<i className={iconsFa6.pencil} alt="pencil" />
								<span>{tr`draft`}</span>
							</span>
						)}
					</div>
				</section>
			</section>

			{/* - - - - - - - - - -  Content Section - - - - - - - - - -  */}
			<CardContent className="h-[47%] overflow-hidden !p-0 ">
				<section className="flex flex-col gap-2   ">
					<span className="flex">
						{cardData.code && <span className="text-black font-bold">{cardData.name} : </span>}
						<span className="text-black font-bold">{cardData.name}</span>
					</span>
					{cardData.description && (
						<span className="text-themeBoldGrey">
							<RReadMore text={cardData.description} redirect={cardData.link} />
						</span>
					)}
				</section>
			</CardContent>
		</Card>
	);
};

export default CardFrontContent;
