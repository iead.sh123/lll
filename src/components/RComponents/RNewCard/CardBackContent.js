import React from "react";
import { Card, CardContent } from "ShadCnComponents/ui/card";
import { cn } from "lib/utils";
import styles from "./RCardNew.module.scss";

const CardBackContent = ({ cardData }) => {
	return (
		// <Card className={cn("rounded-md !shadow-shadowCard", styles.flip__card__back)}>
		<Card className={cn("rounded-md !shadow-shadowCard")} key={cardData?.id}>
			<CardContent className="flex gap-[10px] flex-col p-[5px]">
				<section id="content__section">{cardData.name}</section>
			</CardContent>
		</Card>
	);
};

export default CardBackContent;
