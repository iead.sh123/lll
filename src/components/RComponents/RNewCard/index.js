import * as React from "react";
import CardFrontContent from "./CardFrontContent";
import CardBackContent from "./CardBackContent";
import styles from "./RCardNew.module.scss";
import { cn } from "lib/utils";
import { useLocation, useHistory } from "react-router-dom";

const RNewCard = ({ cardData, width }) => {
	const history = useHistory();
	const location = useLocation();
	const landingView = location.pathname.includes("/landing");

	return (
		<div className={cn("  ", styles.r__card)}>
			<div
				className={cn("cursor-pointer  ")}
				// className={cn("cursor-pointer", styles.flip__card__inner)}
				onClick={(event) => {
					event.stopPropagation();
					cardData.link && history.push(cardData.link);
				}}
			>
				<CardFrontContent cardData={cardData} />
				{/* <CardBackContent cardData={cardData} /> */}
			</div>
		</div>
	);
};
export default RNewCard;
