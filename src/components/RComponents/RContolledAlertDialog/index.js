import tr from "components/Global/RComs/RTranslator";
import React from "react";
import {
	AlertDialog,
	AlertDialogAction,
	AlertDialogCancel,
	AlertDialogContent,
	AlertDialogDescription,
	AlertDialogFooter,
	AlertDialogHeader,
	AlertDialogTitle,
	AlertDialogTrigger,
} from "ShadCnComponents/ui/alert-dialog";

const RControlledAlertDialog = ({
	title = tr("Are_you_sure_you_want_to_delete"),
	description = tr("This_action_cannot_be_undone"),
	cancel = { text: tr("cancel"), className: "text-black" },
	confirm = { text: tr("yes,delete it"), className: "bg-themeDanger", action: () => {} },
	confirmAction = () => {},
	isOpen = false,
	handleCloseAlert,
	closeOnConfirm = true,
	headerItemsPosition = "items-center",
}) => {
	return (
		<AlertDialog open={isOpen}>
			<AlertDialogContent>
				<AlertDialogHeader className={headerItemsPosition}>
					<AlertDialogTitle className={"font-bold text-[16px]"}>{title}</AlertDialogTitle>
					<AlertDialogDescription>{description}</AlertDialogDescription>
				</AlertDialogHeader>
				<AlertDialogFooter className="items-end">
					<AlertDialogCancel
						onClick={() => {
							handleCloseAlert();
						}}
						className={cancel.className}
						variant="link"
					>
						{cancel.text}
					</AlertDialogCancel>
					<AlertDialogAction
						onClick={() => {
							confirmAction();
							closeOnConfirm && handleCloseAlert();
						}}
						className={confirm.className}
					>
						{confirm.text}
					</AlertDialogAction>
				</AlertDialogFooter>
			</AlertDialogContent>
		</AlertDialog>
	);
};

export default RControlledAlertDialog;
