import React from "react";
import { Button } from "ShadCnComponents/ui/button";
import { Input } from "ShadCnComponents/ui/input";
import { Label } from "ShadCnComponents/ui/label";
import {
	Sheet,
	SheetClose,
	SheetContent,
	SheetDescription,
	SheetFooter,
	SheetHeader,
	SheetTitle,
	SheetTrigger,
} from "ShadCnComponents/ui/sheet";

const RDrawer = ({
	triggerComponent = <Button variant="outline">Open</Button>,
	title = { text: null, className: "" },
	description = { text: null, className: "" },
	drawerBody = null,
	drawerFooter = null,
}) => {
	return (
		<Sheet>
			<SheetTrigger asChild>{triggerComponent}</SheetTrigger>
			<SheetContent>
				<SheetHeader>
					{title.text && <SheetTitle className={title.className}>{title.text}</SheetTitle>}
					{description.text && <SheetDescription className={description.className}>{description.text}</SheetDescription>}
				</SheetHeader>
				{drawerBody && drawerBody}
				{drawerFooter && (
					<SheetFooter>
						{drawerFooter?.content && drawerFooter?.content}
						<SheetClose asChild>{drawerFooter.close}</SheetClose>
					</SheetFooter>
				)}
			</SheetContent>
		</Sheet>
	);
};

export default RDrawer;
