import React from "react";
import styles from "./RSecondarySidebar.module.scss";
import tr from "components/Global/RComs/RTranslator";
import { useLocation, NavLink } from "react-router-dom";
import { Nav, NavItem } from "reactstrap";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";

const RSecondarySidebar = (props) => {
	const { secondSideBarRoutes, bgColor, activeColor } = props;
	const location = useLocation();
	const english = localStorage.getItem("language") != "arabic";
	const float = english ? "left" : "right";
	const courseIsMaster = location.pathname.includes("master-course");
	
  	const activeRoute = (tabName) => {
		const segments = location.pathname.split("/");
		const lastWord = segments[segments.length - 1];

		return lastWord.replace(/-/g, "") == tabName.replace(/_/g, "") ? styles.active__item : styles.not__active__item;
	};

	const path = location.pathname;
	const courseIdMatch = courseIsMaster
		? path.match(/\/course-management\/master-course\/([^/]+)/) || path.match(/\/course-catalog\/master-course\/([^/]+)/)
		: path.match(/\/course-management\/([^/]+)/) || path.match(/\/course-catalog\/([^/]+)/);

	const courseId = courseIdMatch ? courseIdMatch[1] : null;

	const createLinks = (routes) => {
		return routes.map((prop, key) => {
			if (prop.redirect || prop.invisible) {
				return null;
			}

			const dynamicPath = prop.path.replace(":courseId", courseId);

			return (
				<li key={key}>
					<NavItem className="mb-3">
						<NavLink style={{ textAlign: float }} to={prop.layout + dynamicPath} className={prop.disabled ? "disabled__link" : ""}>
							<span className={activeRoute(prop.name)}>{tr(prop.name)}</span>
						</NavLink>
					</NavItem>
				</li>
			);
		});
	};

	return (
		<div className={styles.sidebar__container + " scroll_hidden"} data-color={bgColor} data-active-color={activeColor}>
			{secondSideBarRoutes && secondSideBarRoutes.length > 0 && <Nav style={{ display: "block" }}>{createLinks(secondSideBarRoutes)}</Nav>}
		</div>
	);
};

export default RSecondarySidebar;
