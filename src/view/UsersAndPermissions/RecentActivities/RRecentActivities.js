import React from "react";
import styles from "../UsersAndPermissions.module.scss";
import REmptyData from "components/RComponents/REmptyData";

const RRecentActivities = ({ usersAndPermissionsData }) => {
	return (
		<section className={`${styles.box} ${styles.recent_activities}`}>
			{Object.keys(usersAndPermissionsData.recentActivities).length == 0 ? (
				<REmptyData />
			) : (
				Object.keys(usersAndPermissionsData.recentActivities)?.map((key) => {
					return (
						<div className={"p-2"} key={key}>
							<h6>{key}</h6>
							{usersAndPermissionsData.recentActivities[key]?.map((item) => (
								<div key={item}>
									<p>{item}</p>
								</div>
							))}
						</div>
					);
				})
			)}
		</section>
	);
};

export default RRecentActivities;
