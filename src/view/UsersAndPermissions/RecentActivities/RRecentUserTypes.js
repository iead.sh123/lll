import React, { useState } from "react";
import { Services } from "engine/services";
import { Table } from "reactstrap";
import AppModal from "components/Global/ModalCustomize/AppModal";
import styles from "../UsersAndPermissions.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { useHistory } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import UserAvatar from "assets/img/new/svg/user_avatar.svg";
const RRecentUserTypes = ({ recentUserType }) => {
	const history = useHistory();
	const [openModal, setOpenModal] = useState(false);
	const handleOpenModal = () => setOpenModal(true);
	const handleCloseModal = () => setOpenModal(false);

	const handlePushToAllUsers = () => {
		history.push(`${baseURL}/${genericPath}/users-and-permissions/user-type/${recentUserType.user_type_id}`);
	};
	return (
		<>
			<AppModal
				size="md"
				show={openModal}
				parentHandleClose={handleCloseModal}
				header={tr`users`}
				headerSort={
					<Table responsive>
						<thead>
							<tr>
								<th style={{ textAlign: "left" }}>{tr`user_name`}</th>
							</tr>
						</thead>
						<tbody>
							{recentUserType.users.map((user) => (
								<tr>
									<td style={{ textAlign: "left" }} key={user.organization_user_id}>
										{user.name}
									</td>
								</tr>
							))}
						</tbody>
					</Table>
				}
			/>
			<section className={styles.recentUserType} key={recentUserType.user_type_id}>
				<RFlex
					styleProps={{
						justifyContent: "space-between",
					}}
				>
					<h6 className="text-left">{recentUserType.user_type_name}</h6>
					<div className={styles.imageDiv}>
						{recentUserType.images.map((image, index) => (
							<img
								src={`${image ? Services.auth_organization_management.file + image : UserAvatar}`}
								alt={image}
								className={`${styles.userTypeImage} `}
								style={{
									zIndex: index,
									right: index * 10,
									position: "relative",
								}}
							/>
						))}
					</div>
				</RFlex>
				<h6 className="text-muted" style={{ position: "relative", bottom: "14px" }}>
					{recentUserType.users_count} {tr`users`}
				</h6>
				<h6 className={` ${styles.seeAllText}`} onClick={() => handlePushToAllUsers()}>{tr`see_all`}</h6>
			</section>
		</>
	);
};

export default RRecentUserTypes;
