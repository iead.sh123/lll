import React from "react";
import { Row, Col, Form, FormGroup, Input, FormText } from "reactstrap";
import tr from "components/Global/RComs/RTranslator";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RSelect from "components/Global/RComs/RSelect";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import styles from "../UsersAndPermissions.module.scss";

const RAddRole = ({
  usersAndPermissionsData,
  handleChange,
  handlePermissionsByCategory,
  handleUsersByUserType,
  roleId,
}) => {
  const disabledForm = usersAndPermissionsData.roleCategoryData.app_level;
  return (
    <Form className={roleId ? `p-4 ${styles.box}` : ""}>
      <Row className="mb-4">
        <Col xs={8}>
          <FormGroup>
            <label className="d-flex">{tr("name")}</label>
            <Input
              name="name"
              required
              type="text"
              onChange={(event) =>
                handleChange(event.target.name, event.target.value)
              }
              value={usersAndPermissionsData.roleCategoryData.name}
              placeholder={tr("name")}
              disabled={disabledForm}
            />
          </FormGroup>
        </Col>
        <Col xs={4} className="d-flex justify-content-start">
          <FormGroup>
            <label className="d-flex">{tr("active")}</label>
            <AppCheckbox
              onClick={(event) => handleChange("active", event.target.checked)}
              checked={usersAndPermissionsData.roleCategoryData.active}
              disabled={disabledForm}
            />
          </FormGroup>
        </Col>
        <Col xs={12}>
          <FormGroup>
            <label className="d-flex">{tr("description")}</label>
            <Input
              name="description"
              required
              type="textarea"
              onChange={(event) =>
                handleChange(event.target.name, event.target.value)
              }
              value={usersAndPermissionsData.roleCategoryData.description}
              placeholder={tr("description")}
              disabled={disabledForm}
            />
          </FormGroup>
        </Col>
        <Col xs={12}>
          <FormGroup>
            <label className="d-flex">{tr("role_category")}</label>
            <RSelect
              option={usersAndPermissionsData.roleCategories}
              closeMenuOnSelect={true}
              placeholder={tr`select`}
              defaultValue={[
                {
                  value:
                    usersAndPermissionsData.roleCategoryData.role_category_id,
                  label:
                    usersAndPermissionsData.roleCategoryData
                      .role_category_title,
                },
              ]}
              onChange={(event) => {
                handleChange("role_category_id", event.value);
              }}
              isLoading={usersAndPermissionsData.roleCategoriesLoading}
              isDisabled={disabledForm}
            />
          </FormGroup>
        </Col>
      </Row>
      {!roleId && (
        <Row>
          <Col xs={12} sm={6}>
            <RFlex>
              <h6>{tr`Add Permissions`}</h6>
            </RFlex>
            <FormGroup>
              <label className="d-flex">
                {tr("Choose Permission Category")}{" "}
              </label>
              <RSelect
                option={usersAndPermissionsData.categoriesOrganization}
                closeMenuOnSelect={true}
                placeholder={tr`select`}
                onChange={(event) => {
                  handlePermissionsByCategory(event.value);
                }}
                isLoading={
                  usersAndPermissionsData.categoriesOrganizationLoading
                }
              />
            </FormGroup>
          </Col>
          <Col xs={12} sm={6}>
            <RFlex>
              <h6>{tr`Add Users`}</h6>
            </RFlex>
            <FormGroup>
              <label>{tr("Choose User Type")}</label>
              <RSelect
                option={usersAndPermissionsData.usersTypes}
                closeMenuOnSelect={true}
                placeholder={tr`select`}
                onChange={(event) => {
                  handleChange("organization_users", event.value);
                  handleUsersByUserType(event.value);
                }}
                isLoading={usersAndPermissionsData.usersTypesLoading}
              />
            </FormGroup>
          </Col>
        </Row>
      )}
      {!roleId && (
        <Row>
          <Col xs={12} sm={6}>
            <label>{tr("Select Permissions")}</label>
            <RSelect
              option={usersAndPermissionsData.permissionsByOrganization}
              closeMenuOnSelect={true}
              placeholder={tr`select`}
              onChange={(event) => {
                handleChange(
                  "scopes",
                  event.map((e) => e.value)
                );
              }}
              isMulti={true}
              isLoading={
                usersAndPermissionsData.permissionsByOrganizationLoading
              }
            />
          </Col>
          <Col xs={12} sm={6}>
            <label>{tr("Select Users")}</label>
            <RSelect
              option={usersAndPermissionsData.usersByOrganization}
              closeMenuOnSelect={true}
              placeholder={tr`select`}
              onChange={(event) => {
                handleChange(
                  "organization_users",
                  event.map((e) => e.value)
                );
              }}
              isMulti={true}
              isLoading={usersAndPermissionsData.usersByOrganizationLoading}
            />
          </Col>
        </Row>
      )}
    </Form>
  );
};

export default RAddRole;
