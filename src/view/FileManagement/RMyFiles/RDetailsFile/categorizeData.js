export function categorizeData(data) {
	const categorizedData = {
		Today: [],
		Yesterday: [],
		"Last Week": [],
		"Two Weeks Ago": [],
		"Last Month": [],
	};

	// Function to get the difference in days between two dates
	const getDaysDifference = (date1, date2) => {
		const utc1 = Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate());
		const utc2 = Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate());
		return Math.floor((utc2 - utc1) / (1000 * 60 * 60 * 24));
	};

	const currentDate = new Date();

	data &&
		data?.length > 0 &&
		data.forEach((item) => {
			const itemDate = new Date(item.created_at);
			const daysDiff = getDaysDifference(currentDate, itemDate);

			if (daysDiff === 0) {
				categorizedData.Today.push(item);
			} else if (daysDiff === 1) {
				categorizedData.Yesterday.push(item);
			} else if (daysDiff <= 7) {
				categorizedData["Last Week"].push(item);
			} else if (daysDiff <= 14) {
				categorizedData["Two Weeks Ago"].push(item);
			} else if (daysDiff <= 30) {
				categorizedData["Last Month"].push(item);
			} else {
				const monthYear = itemDate.toLocaleString("default", { month: "long", year: "numeric" });
				if (!categorizedData[monthYear]) {
					categorizedData[monthYear] = [];
				}
				categorizedData[monthYear].push(item);
			}
		});

	return categorizedData;
}
