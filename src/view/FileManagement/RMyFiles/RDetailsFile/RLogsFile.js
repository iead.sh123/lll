import React from "react";
import getSourceForType from "utils/getSourceForType";
import styles from "../../FileManagement.module.scss";
import moment from "moment";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { categorizeData } from "./categorizeData";

const RLogsFile = ({ data }) => {
	const categorizedData = categorizeData(data?.data?.values?.itemLogs || []);

	return (
		<RFlex styleProps={{ flexDirection: "column" }}>
			{Object.keys(categorizedData)?.map((key, index) => {
				return (
					<RFlex key={key} styleProps={{ flexDirection: "column", gap: 0 }}>
						<span className={styles.log__key}>{key}</span>
						{Object.values(categorizedData)[index]?.length > 0 ? (
							Object.values(categorizedData)[index]?.map((val, ind, array) => {
								return (
									<RFlex key={`${key}_${ind}`} styleProps={{ flexDirection: "column", gap: "5px" }}>
										<span className="text-xs pt-1">
											{tr`at`} {moment(val?.created_at).format("LT")}
										</span>
										<RFlex styleProps={{ flexWrap: "wrap", gap: "5px", alignItems: "center" }}>
											<img
												width={"30px"}
												height={"30px"}
												src={getSourceForType({
													url: val?.user?.image,
												})}
												alt={val?.name}
											/>
											<span className="text-themeBoldGrey">
												{val?.user?.first_name} {val?.user?.last_name}
											</span>
											<span className="text-themeBoldGrey">{val?.action}</span>
											<span className="font-bold">{val?.name?.length > 15 ? val?.name.substring(0, 15) + " .." : val?.name}</span>
										</RFlex>
										<span> {ind !== array.length - 1 ? <div className="divider"></div> : ""}</span>
									</RFlex>
								);
							})
						) : (
							<span className="text-themeBoldGrey text-[12px]">{tr`no_activities`}</span>
						)}
					</RFlex>
				);
			})}
		</RFlex>
	);
};

export default RLogsFile;
