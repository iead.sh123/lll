import React from "react";
import getSourceForType from "utils/getSourceForType";
import RProfileName from "components/Global/RComs/RProfileName/RProfileName";
import FolderIcon from "components/RComponents/RSvgIcons/FolderIcon";
import iconsFa6 from "variables/iconsFa6";
import styles from "../../FileManagement.module.scss";
import moment from "moment";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { primaryColor, boldGreyColor } from "config/constants";
import { formatFileSize } from "utils/formatFileSize";
import { Folder_Color } from "config/constants";
import { useParams } from "react-router-dom";

const RDetailsFile = ({ data, handlers }) => {
	const { courseId } = useParams();
	const itIsOwnerToThisItem = data?.data?.values?.item?.owner == data?.user?.id;
	const itemDetails = data?.data?.values?.itemDetails;
	const itemAccess = data?.data?.values?.itemAccess;
	const isNotFolder = itemDetails?.content_type;
	const imageBase64 = data?.data?.values?.fileBase64;
	const rootFolderId = data?.data?.values?.data?.current_folder?.id;
	const fileIds = data?.data?.values?.fileIds;
	const folderIds = data?.data?.values?.folderIds;
	const removeManageSection = courseId
		? courseId
			? true
			: false
		: data.data.type === "trashed"
		? true
		: data.data.type === "shared"
		? data.data.type === "shared" && itIsOwnerToThisItem
			? false
			: true
		: isNotFolder
		? fileIds.includes(rootFolderId)
		: folderIds.includes(rootFolderId);

	return (
		<RFlex styleProps={{ gap: "5px", flexDirection: "column" }}>
			{/* Details */}
			<RFlex styleProps={{ flexDirection: "column" }}>
				{/* Name and Image */}
				<RFlex styleProps={{ gap: "5px" }}>
					{isNotFolder ? (
						<RFlex className={styles.image__container}>
							<img
								className={styles.image__content}
								src={getSourceForType({
									fileLink: imageBase64,
									mimeType: itemDetails?.mime_type,
									fileManagement: true,
								})}
								alt={itemDetails?.file_name}
							/>
						</RFlex>
					) : (
						<FolderIcon
							tColor={Folder_Color.yellow.topColor}
							gOColor={Folder_Color.yellow.gradientColorOne}
							gTColor={Folder_Color.yellow.gradientColorTow}
							width="50"
							height="36"
						/>
					)}
					<RFlex styleProps={{ flexDirection: "column", gap: 0 }}>
						<span className={styles.item__name}>{isNotFolder ? itemDetails?.file_name : itemDetails?.folder_name}</span>
						<RFlex styleProps={{ gap: "5px", alignItems: "center", color: "#585858", fontSize: "11px" }}>
							<span>{formatFileSize(itemDetails?.Size)}</span>
							<span className={styles.dots}></span>
							<span>{moment(itemDetails.created_at).format("MMMM Do YYYY, h:mm:ss a")}</span>
							{isNotFolder && <span className={styles.dots}></span>}
							{isNotFolder && <span>{itemDetails?.content_type}</span>}
						</RFlex>
					</RFlex>
				</RFlex>
				{/* Author */}
				{itemDetails?.user?.first_name && (
					<RFlex styleProps={{ gap: "5px", flexDirection: "column" }}>
						<h1 style={{ color: boldGreyColor }}>{tr`author`}</h1>
						<RProfileName name={`${itemDetails?.user?.first_name + itemDetails?.user?.last_name}`} img={itemDetails?.user?.image} />
					</RFlex>
				)}
				{/* Path */}
				<RFlex styleProps={{ gap: "5px", flexDirection: "column" }}>
					<h1 style={{ color: boldGreyColor }}>{tr`path`}</h1>
					<span>{itemDetails?.path}</span>
				</RFlex>
			</RFlex>

			{/* Allowed Or Not */}
			<RFlex styleProps={{ gap: 0 }} className="pt-[10px]">
				<span style={{ fontSize: "12px" }}>
					{itemDetails?.download_available ? tr`download_is_allowed` : tr`download_is_not_allowed`}&nbsp;,&nbsp;
					{itemDetails?.passowrd ? tr`password_is_set` : tr`no_password_was_set`}
				</span>
			</RFlex>
			{/* Shared Users */}
			{itemAccess?.length > 0 && (
				<RFlex styleProps={{ gap: 0 }}>
					<RFlex styleProps={{ gap: 0, width: "40%" }}>
						{itemAccess.slice(0, 3)?.map((item, ind, array) => (
							<div key={ind} style={{ position: "relative", right: `${ind * 5}px` }}>
								<img
									width={"24px"}
									height={"24px"}
									src={getSourceForType({
										url: item?.file,
									})}
									alt={item?.user_name}
								/>
							</div>
						))}
					</RFlex>
					<RFlex styleProps={{ gap: "3px", flexWrap: "wrap", color: boldGreyColor }}>
						<span>{tr`this`}</span>
						<span>{isNotFolder ? tr`file` : tr`folder`}</span>
						<span>{tr`has_been_shared_with`}</span>
						{itemAccess.slice(0, 2)?.map((item, ind, array) => (
							<RFlex styleProps={{ gap: 0 }} key={ind}>
								<span className=" text-themeBlack">{item?.user_name}</span>
								<span className="text-themeBlack"> {ind !== array.length - 1 ? "," : ""}</span>
							</RFlex>
						))}
						<span>{tr`and`}</span>
						<span>{itemAccess?.length - 2}</span>
						<span>{tr`other`}</span>
					</RFlex>
				</RFlex>
			)}

			{/* Manage Access And Share */}
			{!removeManageSection && (
				<RFlex styleProps={{ gap: 0, justifyContent: "space-between", color: primaryColor }}>
					<span
						style={{ textDecoration: "underline", cursor: "pointer" }}
						onClick={() => handlers.handlers.handleOpenManageAccess()}
					>{tr`manage_access`}</span>
					<i className={iconsFa6.share + " cursor-pointer"} alt="share icon" onClick={() => handlers.handlers.handleOpenShare()} />
				</RFlex>
			)}
			<div className={"divider"} />
		</RFlex>
	);
};

export default RDetailsFile;
