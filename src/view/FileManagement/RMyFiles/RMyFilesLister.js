import React from "react";
import getSourceForType from "utils/getSourceForType";
import RHoverInput from "components/Global/RComs/RHoverInput/RHoverInput";
import FolderIcon from "components/RComponents/RSvgIcons/FolderIcon";
import iconsFa6 from "variables/iconsFa6";
import RLister from "components/Global/RComs/RLister";
import Loader from "utils/Loader";
import moment from "moment";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { DATE_FORMATE, primaryColor } from "config/constants";
import { formatFileSize } from "utils/formatFileSize";
import { Folder_Color } from "config/constants";
import { imageTypes } from "config/mimeTypes";
import AppNewCheckbox from "components/Global/RComs/AppNewCheckbox/AppNewCheckbox";
import { useParams } from "react-router-dom";

const RMyFilesLister = ({ data, handlers, loading }) => {
	const { courseId } = useParams();

	const ItemsLength = data.values.folderIds.concat(data.values.fileIds);
	//  - - - - - - - Name Component - - - - - - -
	const nameComponent = ({ id, contentType, name, type, file, index }) => {
		return (
			<RFlex styleProps={{ gap: 2, alignItems: "center" }}>
				<AppNewCheckbox
					idsOrderGenerator={id}
					onChange={(event) => {
						event.stopPropagation();
						handlers.handleSelectItems({ contentId: file.id, contentType: contentType });
						handlers.handleSetItem({ item: file });
						if (file.content_type) {
							handlers.handleSetBase64(file?.file_base64 ? file?.file_base64 : file?.file_url || null);
							handlers.handleSetFileDetail(file);
						}
					}}
					checked={contentType == "folder" ? data.values.folderIds.includes(file.id) : data.values.fileIds.includes(file.id)}
					disabled={loading.deleteItemsLoading || loading.addItemsToFavoriteListLoading}
				/>
				{type == "folder" && (
					<FolderIcon
						tColor={Folder_Color.yellow.topColor}
						gOColor={Folder_Color.yellow.gradientColorOne}
						gTColor={Folder_Color.yellow.gradientColorTow}
						width="15"
						height="15"
					/>
				)}
				{type !== "folder" && (
					<img
						width={imageTypes.includes(file.mime_type) && !file.url ? "15px" : "15px"}
						height={imageTypes.includes(file.mime_type) && !file.url ? "15px" : "15px"}
						src={getSourceForType({
							fileLink: file.file_base64 ? file.file_base64 : file.file_url,
							mimeType: file?.mime_type,
						})}
						alt={file?.file?.name ?? file?.name}
					/>
				)}

				<RHoverInput
					handleInputChange={(event) => handlers.handleChangeText(event.target.value, event.target.name)}
					handleOnBlur={(event) => handlers.handleInputSaved(event, file)}
					name={type == "folder" ? `data.sub_folders.${index}.folder_name` : `data.files.${index}.file_name_by_user`}
					inputValue={
						type == "folder"
							? data.values.data.sub_folders[index]?.folder_name?.length > 22
								? `${data.values.data.sub_folders[index]?.folder_name.substring(0, 22)} ..`
								: data.values.data.sub_folders[index]?.folder_name
							: data.values.data.files[index]?.file_name_by_user?.length > 22
							? `${data.values.data.files[index]?.file_name_by_user.substring(0, 22)} ..`
							: data.values.data.files[index]?.file_name_by_user
					}
					type={"text"}
					focusOnInput={true}
					inputPlaceHolder={tr(" ")}
					saved={type == "folder" ? data.values.data.sub_folders[index]?.saved : data.values.data.files[index]?.saved}
					// inputWidth={"150px"}
					selectOnInput={true}
					needToHover={false}
					handleEditFunctionality={(event) => {
						event.stopPropagation();
						handlers.handleShowSpecificFolder(file.id);
					}}
				/>
			</RFlex>
		);
	};
	//  - - - - - - - Generate Record Object - - - - - - -
	const generateRecordObject = (values, generateData, fileType, usersWhoMadeModified, type) => {
		return generateData.map((item, index) => ({
			id: item.id,
			details: [
				{
					key: tr`name`,
					value: nameComponent({
						id: item.content_type ? `file_${item.id}` : `folder_${item.id}`,
						contentType: item.content_type ?? "folder",
						name: item.file_name_by_user || item.folder_name,
						type: type,
						file: item,
						index: index,
					}),
					type: "component",
				},
				{ key: tr`size`, value: type == "folder" ? `${item.size ?? 0} ${tr`items`}` : formatFileSize(item.file_size) },
				{
					key: tr`file_type`,
					value: item.content_type || tr`folder`,
					type: "component",
					dropdown: true,
					dropdownData: fileType.map((item) => {
						return {
							title: item,
							name: item,
							value: values.groupingOfSearchFields.mime_type,
							onClick: () => handlers.handleFilterOnFileType(item),
						};
					}),
					itemStyle: { padding: "5px 10px" },
				},
				{
					key: tr`modified_by`,
					value: item.modified_by,
					type: "component",
					dropdown: true,
					dropdownData: usersWhoMadeModified.map((item) => {
						return {
							title: item,
							name: item,
							value: values.groupingOfSearchFields.modified_by,
							onClick: () => handlers.handleFilterOnUsers(item),
						};
					}),
					itemStyle: { padding: "5px 10px" },
				},
				{ key: tr`added_at`, value: moment(item.created_at).format(DATE_FORMATE) },
			],
			actions: [
				{
					justIcon: true,
					icon: values.ids.includes(item.id) ? iconsFa6.spinner : item?.is_favorite ? iconsFa6.starSolid : iconsFa6.star,
					disabled: values.ids.includes(item.id) ? true : false,
					loading: values.ids.includes(item.id) ? true : false,
					actionIconStyle: { color: "#FFDE1D" },
					hidden: data.type == "trashed" ? true : false,
					onClick: () =>
						handlers.handleAddSingleItemToFavorite({
							contentId: item.id,
							contentType: item.content_type ?? "folder",
							isFavorite: item.is_favorite,
						}),
				},
				{
					justIcon: true,
					icon: iconsFa6.share,
					actionIconStyle: { color: primaryColor },
					hidden: courseId || data.type == "trashed" ? true : false,
					onClick: () => {
						if (item.content_type) {
							if (!data.values.fileIds.includes(item.id)) {
								handlers.handleSelectItems({ contentId: item.id, contentType: item.content_type });
							}
						} else {
							if (!data.values.folderIds.includes(item.id)) {
								handlers.handleSelectItems({ contentId: item.id, contentType: "folder" });
							}
						}
						handlers.handleOpenShare();
					},
				},
				{
					justIcon: true,
					icon: iconsFa6.rotateRight,
					disabled: loading.restoreItemsLoading ? true : false,
					loading: loading.restoreItemsLoading && ItemsLength.includes(item.id) ? true : false,
					actionIconStyle: { color: primaryColor },
					hidden: data.type !== "trashed" ? true : false,
					onClick: () => {
						if (item.content_type) {
							if (!data.values.fileIds.includes(item.id)) {
								handlers.handleSelectItems({ contentId: item.id, contentType: item.content_type });
								handlers.handleRestoreItems(item.id, item.content_type);
							}
						} else {
							if (!data.values.folderIds.includes(item.id)) {
								handlers.handleSelectItems({ contentId: item.id, contentType: "folder" });
								handlers.handleRestoreItems(item.id);
							}
						}
					},
				},

				{ name: tr`edit`, inDropdown: true, hidden: ItemsLength?.length > 1 ? true : false, onClick: () => handlers.handleRenameItems() },
				{
					name: tr`copy_link`,
					inDropdown: true,
					hidden: courseId || ItemsLength?.length > 1 ? true : false,
					onClick: () => handlers.handleCopyLink(),
				},
				{
					name: tr`copy_to`,
					inDropdown: true,
					// hidden: ItemsLength?.length > 1 ? true : false,
					onClick: () => handlers.handleOpenCopyFile(),
				},
				{ name: tr`move_to`, inDropdown: true, onClick: () => handlers.handleOpenMoveFile() },
				{
					name: tr`download`,
					inDropdown: true,
					hidden: ItemsLength?.length > 1 ? true : false || values?.folderIds?.length > 0 ? true : false,
					onClick: () => handlers.handleDownloadFile(),
				},
				{
					name: data.type == "favorite" ? tr`un_favorite` : tr`favorite`,
					inDropdown: true,
					onClick: () => {
						data.type == "favorite" ? handlers.handleRemoveItemsFromFavorite() : handlers.handleAddItemsToFavorite();
					},
				},
				{
					name: tr`manage_access`,
					inDropdown: true,
					hidden: ItemsLength?.length > 1 ? true : false,
					onClick: () => handlers.handleOpenManageAccess(),
				},
				{
					name: tr`details`,
					inDropdown: true,
					hidden: ItemsLength?.length > 1 ? true : false,
					onClick: () => handlers.handleToggleDrawerToOpenDetail(),
				},
				{
					name: tr`move_to_trash`,
					inDropdown: true,
					onClick: () => handlers.handleDeleteItemsFromList(),
				},
			],
			rowSelected: () => {
				if (item.content_type) {
					if (!data.values.fileIds.includes(item.id)) {
						handlers.handleSelectItems({ contentId: item.id, contentType: item.content_type });
					}
				} else {
					if (!data.values.folderIds.includes(item.id)) {
						handlers.handleSelectItems({ contentId: item.id, contentType: "folder" });
					}
				}
			},
			removeDropDownActions: data?.type == "trashed" ? true : false,
		}));
	};

	const _recordFolders = generateRecordObject(
		data.values,
		data?.values?.data?.sub_folders || [],
		data?.values?.fileType,
		data?.values?.usersWhoMadeModified,
		"folder"
	);

	const _recordFiles = generateRecordObject(
		data.values,
		data?.values?.data?.files || [],
		data?.values?.fileType,
		data?.values?.usersWhoMadeModified,
		"file"
	);

	const _records = [..._recordFolders, ..._recordFiles];

	if (loading?.rootFoldersLoading) return <Loader />;
	return (
		<RFlex className="w-Full">
			<RLister Records={_records} activeScroll={true} maxHeight="70vh" />
		</RFlex>
	);
};

export default RMyFilesLister;
