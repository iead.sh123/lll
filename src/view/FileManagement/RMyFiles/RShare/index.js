import React from "react";
import RTextIcon from "components/Global/RComs/RTextIcon/RTextIcon";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import RSelect from "components/Global/RComs/RSelect";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Switch } from "ShadCnComponents/ui/switch";
import { Label } from "ShadCnComponents/ui/label";
import { Input } from "reactstrap";
import { boldGreyColor } from "config/constants";
import { primaryColor } from "config/constants";

const RShare = ({ data, handlers, loading }) => {
	return (
		<RFlex styleProps={{ flexDirection: "column", padding: "5px" }}>
			<RFlex styleProps={{ width: "100%", justifyContent: "space-between" }}>
				<span>
					{tr`share`} &nbsp;
					{data?.itemObj?.folder_name ?? data?.itemObj?.file_name_by_user}
				</span>
				<RTextIcon
					flexStyle={{ cursor: "pointer", gap: "5px" }}
					text={tr`copy_link`}
					textStyle={{ color: "#668ad7" }}
					iconStyle={{ color: "#668ad7" }}
					onClick={() => handlers.handlers.handleCopyLink()}
					icon={iconsFa6.copy}
				/>
			</RFlex>
			<RFlex className="flex-col">
				<span className="text-themeBoldGrey">
					{tr`choose_who_can_access_this`} {data?.itemObj?.folder_name ?? data?.itemObj?.file_name_by_user}
				</span>
				<RFlex className="items-center justify-between">
					<RSelect
						name="usersToShared"
						onChange={handlers.handleUsersSelected}
						value={data?.values?.usersSelected}
						option={data?.values?.usersToShared}
						isMulti={true}
						className="w-3/4"
						isLoading={loading.dataInsideSelectLoading}
					/>
					<RButton
						color="primary"
						text={tr`give_access`}
						onClick={() => handlers.handleAddUsersToShare()}
						loading={loading.addUsersToShareLoading}
						disabled={data?.values?.usersSelected?.length == 0 || loading.addUsersToShareLoading}
					/>
				</RFlex>
			</RFlex>
			<RFlex>
				<div className="flex items-center space-x-2">
					<Label htmlFor="public" className="text-themeBoldGrey">{tr`public`}</Label>
					<Switch
						checked={data.values.itemStatus}
						onCheckedChange={(event) => {
							handlers.handleChangeStatus(event);
						}}
						className="data-[state=checked]:bg-themeSuccess"
						id={"public"}
						disabled={loading?.changeStatusLoading}
					/>
					{loading?.changeStatusLoading && <i className={iconsFa6.spinner} />}
				</div>
			</RFlex>
			<RFlex>
				<RTextIcon
					flexStyle={{ cursor: "pointer", gap: "5px" }}
					className="text-themeBoldGrey"
					text={tr`sharing_privacy`}
					textStyle={{ color: boldGreyColor }}
					iconStyle={{ color: primaryColor }}
					onClick={() => handlers.handleSharingPrivacy()}
					icon={iconsFa6.lock}
				/>
			</RFlex>

			{data?.values?.sharingPrivacyToggle && (
				<div className="d-flex flex-col gap-[10px]">
					<RFlex>
						<div className="flex items-center space-x-2">
							<Label htmlFor="allow-downloads" className="text-themeBoldGrey">{tr`allow_downloads`}</Label>
							<Switch
								checked={data.values.itemDownloadStatus}
								onCheckedChange={(event) => {
									handlers.handleChangeDownloadStatus(event);
								}}
								className="data-[state=checked]:bg-themeSuccess"
								id={"allow-downloads"}
								disabled={loading?.downloadStatusLoading}
							/>
							{loading?.downloadStatusLoading && <i className={iconsFa6.spinner} />}
						</div>
					</RFlex>
					<RFlex className="flex-col gap-[7px]">
						<span className="text-themeBoldGrey">{tr`password`}</span>
						<RFlex className="align-items-center">
							<form className="flex-1">
								<div className="position-relative">
									<Input
										name="itemPassword"
										type="text"
										placeholder={tr`add_a_password`}
										value={data.values.itemPassword}
										onChange={(event) => {
											handlers.handleChange(event.target.value);
										}}
										onKeyDown={(event) => {
											if (event.key === "Enter" && handleSearch) {
												handlers.handleSetPasswordToItem();
											}
										}}
									/>
									<i
										className={iconsFa6.rotateArrow + " text-themeStroke position-absolute top-[10px] right-[10px] cursor-pointer"}
										alt="rotateArrow"
										onClick={() => handlers.handleSuggestPassword()}
									/>
								</div>
							</form>
							<RButton
								text={tr`set`}
								onClick={() => handlers.handleSetPasswordToItem()}
								color="primary"
								disabled={loading.setPasswordLoading || loading.deletePasswordLoading}
								loading={loading.setPasswordLoading || loading.deletePasswordLoading}
							/>
							<RButton
								text={tr`cancel`}
								onClick={() => handlers.handleDeletePasswordFromItems()}
								color="primary"
								outline
								disabled={!data.values.itemPassword || loading.setPasswordLoading || loading.deletePasswordLoading}
								loading={loading.setPasswordLoading || loading.deletePasswordLoading}
							/>
						</RFlex>
					</RFlex>
				</div>
			)}
		</RFlex>
	);
};

export default RShare;
