import React from "react";
import RDropdownIcon from "components/Global/RComs/RDropdownIcon/RDropdownIcon";
import RTextIcon from "components/Global/RComs/RTextIcon/RTextIcon";
import iconsFa6 from "variables/iconsFa6";
import Switch from "react-bootstrap-switch";
import styles from "../../FileManagement.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { successColor } from "config/constants";
import { dangerColor } from "config/constants";
import { primaryColor } from "config/constants";
import { useParams } from "react-router-dom";

const RFilterSection = ({ data, handlers, loading }) => {
	const { courseId } = useParams();
	const ItemsLength = data.values.folderIds.concat(data.values.fileIds);
	const DisabledItems = loading.addItemsToFavoriteListLoading || loading.deleteItemsLoading || loading.restoreItemsLoading;

	const selectFilterActions = [
		{
			label: tr`all`,
			action: () => {
				handlers.handleSelectedAllItems();
			},
		},
		{
			label: tr`clear_selection`,
			action: () => {
				handlers.handleClearSelectedItems();
			},
		},
	];

	const allActions = [
		{
			label: tr`download`,
			icon: iconsFa6.download,
			color: primaryColor,
			singleAndMulti: false,
			removeAction: data.values.folderIds.length > 0 ? true : false,
			className: DisabledItems ? styles.disabled__text : "",

			action: () => {
				handlers.handleDownloadFile();
			},
		},
		{
			label: tr`share`,
			icon: iconsFa6.share,
			color: primaryColor,
			singleAndMulti: false,
			removeAction: courseId ? true : false,
			className: DisabledItems ? styles.disabled__text : "",

			action: () => {
				handlers.handleOpenShare();
			},
		},
		{
			label: tr`copy_link`,
			color: primaryColor,
			singleAndMulti: false,
			removeAction: courseId ? true : false,
			className: DisabledItems ? styles.disabled__text : "",

			action: () => {
				handlers.handleCopyLink();
			},
		},
		{
			label: tr`rename`,
			color: primaryColor,
			singleAndMulti: false,
			className: DisabledItems ? styles.disabled__text : "",

			action: () => {
				handlers.handleRenameItems();
			},
		},
		{
			label: data.type == "favorite" ? tr`un_favorite` : tr`favorite`,
			icon: loading.addItemsToFavoriteListLoading ? iconsFa6.spinner : iconsFa6.star,
			color: primaryColor,
			singleAndMulti: true,
			className: DisabledItems ? styles.disabled__text : "",

			action: () => {
				data.type == "favorite" ? handlers.handleRemoveItemsFromFavorite() : handlers.handleAddItemsToFavorite();
			},
		},
		{
			label: tr`move_to`,
			icon: iconsFa6.chevronRight,
			color: primaryColor,
			iconOnRight: true,
			singleAndMulti: true,
			className: DisabledItems ? styles.disabled__text : "",

			action: () => {
				handlers.handleOpenMoveFile();
			},
		},
		{
			label: tr`copy_to`,
			icon: iconsFa6.chevronRight,
			color: primaryColor,
			iconOnRight: true,
			// singleAndMulti: false,
			singleAndMulti: true,
			className: DisabledItems ? styles.disabled__text : "",

			action: () => {
				handlers.handleOpenCopyFile();
			},
		},
		{
			label: tr`details`,
			icon: iconsFa6.info,
			color: primaryColor,
			className: DisabledItems ? styles.disabled__text : "",

			action: () => {
				handlers.handleToggleDrawerToOpenDetail();
			},
		},
		{
			label: tr`move_to_trash`,
			icon: loading.deleteItemsLoading ? iconsFa6.spinner : iconsFa6.delete,
			color: dangerColor,
			singleAndMulti: true,
			className: DisabledItems ? styles.disabled__text : "",

			action: () => {
				handlers.handleDeleteItemsFromList();
			},
		},
	];

	const trashedActions = [
		{
			label: tr`restore`,
			icon: loading.restoreItemsLoading ? iconsFa6.spinner : iconsFa6.rotateRight,
			color: primaryColor,
			className: DisabledItems ? styles.disabled__text : "",
			singleAndMulti: true,

			action: () => {
				handlers.handleRestoreItems();
			},
		},

		{
			label: tr`details`,
			icon: loading.deleteItemsLoading ? iconsFa6.spinner : iconsFa6.info,
			color: primaryColor,
			className: DisabledItems ? styles.disabled__text : "",

			action: () => {
				handlers.handleToggleDrawerToOpenDetail();
			},
		},
	];

	const sharedActions = [
		{
			label: tr`download`,
			icon: iconsFa6.download,
			color: primaryColor,
			singleAndMulti: false,
			removeAction: data.values.folderIds.length > 0 ? true : false,
			className: DisabledItems ? styles.disabled__text : "",

			action: () => {
				handlers.handleDownloadFile();
			},
		},

		{
			label: tr`copy_link`,
			color: primaryColor,
			singleAndMulti: false,
			removeAction: courseId ? true : false,
			className: DisabledItems ? styles.disabled__text : "",

			action: () => {
				handlers.handleCopyLink();
			},
		},

		{
			label: tr`favorite`,
			icon: loading.addItemsToFavoriteListLoading ? iconsFa6.spinner : iconsFa6.star,
			color: primaryColor,
			singleAndMulti: true,
			className: DisabledItems ? styles.disabled__text : "",

			action: () => {
				handlers.handleAddItemsToFavorite();
			},
		},

		{
			label: tr`details`,
			icon: iconsFa6.info,
			color: primaryColor,
			className: DisabledItems ? styles.disabled__text : "",

			action: () => {
				handlers.handleToggleDrawerToOpenDetail();
			},
		},
	];

	return (
		<RFlex className="flex-col">
			<RFlex styleProps={{ justifyContent: "space-between", alignItems: "center", color: "#585858" }}>
				{data?.values.breadCrumbing?.length > 0 && data?.folderId && ItemsLength?.length == 0 ? (
					<RFlex>
						{data?.values.breadCrumbing?.map((item, ind, array) => (
							<RFlex key={ind} className="flex-wrap">
								<span
									onClick={() => handlers.handleShowSpecificFolder(ind === 0 ? null : item.folder_id)}
									className={item.folder_id == data?.folderId ? "font-bold cursor-pointer" : "cursor-pointer"}
								>
									{ind === 0 ? tr`my_files` : item?.folder_name}
								</span>
								<span className="text-themePrimary">
									{ind !== array.length - 1 && <i className={iconsFa6.chevronRight} alt={`alt_${ind}`} />}
								</span>
							</RFlex>
						))}
					</RFlex>
				) : (
					<>
						{ItemsLength.length == 0 && (
							<span>
								{data?.type == "shared"
									? tr`recently_shared_files`
									: data?.type == "favorite"
									? tr`favorite_files`
									: data?.type == "trashed"
									? tr`trash_files`
									: tr`my_files`}
							</span>
						)}
					</>
				)}
				{/* - - - - - - - All Left Actions - - - - - - - */}
				{ItemsLength.length > 0 && (
					<RFlex styleProps={{ gap: "18px", alignItems: "center" }}>
						<RFlex styleProps={{ gap: "8px", alignItems: "center" }}>
							<span className="p-0 m-0 text-success">{ItemsLength.length}</span>
							<span className="p-0 m-0 text-success" style={{ fontSize: "12px", cursor: "pointer" }}>
								{tr`select`}
							</span>
							<RDropdownIcon
								actions={selectFilterActions}
								iconContainerStyle={{ border: "1px solid" + successColor, borderRadius: "100%" }}
							/>
						</RFlex>

						{(ItemsLength.length > 1
							? (data?.type == "trashed" ? trashedActions : data?.type == "shared" ? sharedActions : allActions).filter(
									(item) => item.singleAndMulti && !item.removeAction
							  )
							: (data?.type == "trashed" ? trashedActions : data?.type == "shared" ? sharedActions : allActions).filter(
									(item) => !item.removeAction
							  )
						).map((action) => (
							<RTextIcon
								icon={action.icon}
								text={action.label}
								flexStyle={{ color: action.color, cursor: "pointer" }}
								onClick={() => action.action()}
								iconOnRight={action.iconOnRight}
								className={action.className}
							/>
						))}
					</RFlex>
				)}

				{/* - - - - - - - All right Actions - - - - - - - */}
				<RFlex styleProps={{ alignItems: "center", gap: "18px" }}>
					{/* - - - - - - - Sort By - - - - - - - */}
					<RDropdownIcon
						actions={[
							{
								name: "mime_type",
								label: tr`type`,
								value: data.values.groupingOfSearchFields.sortByFiled,
								action: () => handlers.handleSortBy("mime_type"),
							},
							{
								name: "name",
								label: tr`name`,
								value: data.values.groupingOfSearchFields.sortByFiled,
								action: () => handlers.handleSortBy("name"),
							},
							{
								name: "modified_by",
								label: tr`modified_by`,
								value: data.values.groupingOfSearchFields.sortByFiled,
								action: () => handlers.handleSortBy("modified_by"),
							},
							{
								name: "file_size",
								label: tr`file_size`,
								value: data.values.groupingOfSearchFields.sortByFiled,
								action: () => handlers.handleSortBy("file_size"),
							},
							{
								name: "1",
								label: tr`ascending`,
								value: data.values.groupingOfSearchFields.sortByOrderType,
								action: () => handlers.handleOrderBy(1),
							},
							{
								name: "0",
								label: tr`descending`,
								value: data.values.groupingOfSearchFields.sortByOrderType,
								action: () => handlers.handleOrderBy(0),
							},
						]}
						component={
							<RFlex styleProps={{ alignItems: "center", color: "#585858" }}>
								<span className="p-0 m-0">{tr("sort_by")}</span>
								<RFlex className="flex-column cursor-pointer">
									<i className={iconsFa6.angleUp + " fa-sm"} />
									<i className={iconsFa6.angleDown + " fa-sm"} />
								</RFlex>
							</RFlex>
						}
						itemStyle={{ padding: "5px 10px" }}
						makeDividerAsIndexes={[4]}
					/>

					{/* - - - - - - - Details - - - - - - - */}
					{!data.removeContents && (
						<RFlex
							styleProps={{ alignItems: "center", cursor: "pointer" }}
							onClick={() => {
								handlers.handleSetRootFolder();
								handlers.handleToggleDrawerToOpenDetail();
							}}
						>
							<i className={iconsFa6.info} />
							<span>{tr`details`}</span>
						</RFlex>
					)}

					{/* - - - - - - - Switch - - - - - - - */}
					<RFlex styleProps={{ height: "100%", position: "relative", top: 3 }}>
						<Switch
							value={data.values.switchMode}
							offText={<i className={iconsFa6.list} />}
							onText={<i className={iconsFa6.list} />}
							onColor="secondary"
							offColor="secondary"
							onChange={() => handlers.handleSwitchBetweenTowMode()}
							size="small"
						/>
					</RFlex>
				</RFlex>
			</RFlex>

			{/* - - - - - - - Hint I'm on the Trashed page  - - - - - - - */}
			{data?.type == "trashed" && !data.values.trashedHint && (
				<RFlex className={styles.trashed__hint}>
					<span>{tr`moved to trash items can be restored within 15 days`}</span>
					<i className={iconsFa6.close + " text-themeDanger cursor-pointer"} alt="close" onClick={handlers.handleCloseTrashedHint} />
				</RFlex>
			)}
		</RFlex>
	);
};

export default RFilterSection;
