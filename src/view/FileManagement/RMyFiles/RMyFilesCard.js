import React from "react";
import getSourceForType from "utils/getSourceForType";
import RHoverInput from "components/Global/RComs/RHoverInput/RHoverInput";
import FolderIcon from "components/RComponents/RSvgIcons/FolderIcon";
import Loader from "utils/Loader";
import styles from "../FileManagement.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Folder_Color } from "config/constants";
import { imageTypes } from "config/mimeTypes";
import AppNewCheckbox from "components/Global/RComs/AppNewCheckbox/AppNewCheckbox";
import iconsFa6 from "variables/iconsFa6";
import REmptyData from "components/RComponents/REmptyData";

const RMyFilesCard = ({ data, handlers, loading, removeActions }) => {
	// i use removeActions to processing (copy - move)
	if (loading?.rootFoldersLoading || loading?.rootFoldersFetching) return <Loader />;
	return (
		<RFlex styleProps={{ flexWrap: "wrap" }}>
			{/* FOLDER */}
			{data?.values?.data?.sub_folders?.map((subFolder, index) => {
				return (
					<RFlex key={subFolder?.id} className={styles.div__container}>
						{!removeActions && (
							<RFlex className={styles.div__checkbox}>
								<AppNewCheckbox
									idsOrderGenerator={`folder_${subFolder?.id}`}
									onChange={(event) => {
										event.stopPropagation();
										handlers.handleSelectItems({ contentId: subFolder?.id, contentType: subFolder.content_type ?? "folder" });
										handlers.handleSetItem({ item: subFolder });
									}}
									checked={data.values.folderIds.includes(subFolder.id)}
									disabled={loading.deleteItemsLoading || loading.addItemsToFavoriteListLoading}
								/>
							</RFlex>
						)}
						<RFlex className={styles.image__container} onClick={() => handlers.handleShowSpecificFolder(subFolder.id)}>
							<FolderIcon
								tColor={Folder_Color.yellow.topColor}
								gOColor={Folder_Color.yellow.gradientColorOne}
								gTColor={Folder_Color.yellow.gradientColorTow}
								width="50"
								height="36"
							/>
						</RFlex>
						<RHoverInput
							handleInputChange={(event) => handlers.handleChangeText(event.target.value, event.target.name)}
							handleOnBlur={(event) => handlers.handleInputSaved(event, subFolder)}
							name={`data.sub_folders.${index}.folder_name`}
							inputValue={
								data.values.data.sub_folders[index]?.folder_name?.length > 6
									? `${data.values.data.sub_folders[index]?.folder_name.substring(0, 6)} ..`
									: data.values.data.sub_folders[index]?.folder_name
							}
							type={"text"}
							focusOnInput={true}
							inputPlaceHolder={tr(" ")}
							saved={data.values.data.sub_folders[index]?.saved}
							inputWidth={"90px"}
							selectOnInput={true}
							needToHover={false}
							textCenter={subFolder?.is_favorite ? false : true}
							textClassName={styles.text__class__name}
							icon={subFolder?.is_favorite && <i className={iconsFa6.starSolid} alt="starSolid" style={{ color: "#FFDE1D" }} />}
						/>
					</RFlex>
				);
			})}

			{/* FILE */}
			{data?.values?.data?.files?.map((file, index) => (
				<RFlex key={file?.id} className={styles.div__container} styleProps={{ gap: 0 }}>
					{!removeActions && (
						<RFlex className={styles.div__checkbox}>
							<AppNewCheckbox
								idsOrderGenerator={`file_${file?.id}`}
								onChange={(event) => {
									event.stopPropagation();
									handlers.handleSelectItems({ contentId: file?.id, contentType: file.content_type ?? "folder" });
									handlers.handleSetItem({ item: file });

									if (file.content_type) {
										handlers.handleSetBase64(file?.file_base64 ? file?.file_base64 : file?.file_url || null);
										handlers.handleSetFileDetail(file);
									}
								}}
								checked={data.values.fileIds.includes(file.id)}
							/>
						</RFlex>
					)}

					<RFlex className={styles.image__container}>
						<img
							className={styles.image__content}
							src={getSourceForType({
								fileLink: file.file_base64 ? file.file_base64 : file.file_url,
								mimeType: file?.mime_type,
							})}
							alt={file?.file?.name ?? file?.name}
						/>
					</RFlex>
					<RHoverInput
						handleInputChange={(event) => handlers.handleChangeText(event.target.value, event.target.name)}
						handleOnBlur={(event) => handlers.handleInputSaved(event, file)}
						name={`data.files.${index}.file_name_by_user`}
						inputValue={
							data.values.data.files[index]?.file_name_by_user?.length > 6
								? `${data.values.data.files[index]?.file_name_by_user.substring(0, 6)} ..`
								: data.values.data.files[index]?.file_name_by_user
						}
						type={"text"}
						focusOnInput={true}
						inputPlaceHolder={tr(" ")}
						saved={data.values.data.files[index]?.saved}
						inputWidth={"90px"}
						selectOnInput={true}
						needToHover={false}
						textCenter={file?.is_favorite ? false : true}
						textClassName={styles.text__class__name}
						icon={file?.is_favorite && <i className={iconsFa6.starSolid} alt="starSolid" style={{ color: "#FFDE1D" }} />}
					/>
				</RFlex>
			))}

			{data?.values?.data?.sub_folders?.length == 0 && data?.values?.data?.files?.length == 0 && <REmptyData />}
		</RFlex>
	);
};

export default RMyFilesCard;
