import React from "react";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RUploadFileSection = ({ data, handlers, loading, removeUploadFile }) => {
	return (
		<RFlex>
			{!data.removeContents && (
				<RFlex>
					{!removeUploadFile && (
						<RButton
							className={"mt-0"}
							text={tr`upload_file`}
							onClick={() => {
								handlers.handleOpenUploadFile();
							}}
							color="primary"
						/>
					)}

					<RButton
						className={"ml-0 mr-0 mt-0"}
						text={tr`new_folder`}
						onClick={() => handlers.handleAddNewFolder()}
						color="primary"
						outline
						disabled={loading.createSubFolderLoading || loading.updateSubFolderLoading || loading.updateFileLoading}
						loading={loading.createSubFolderLoading || loading.updateSubFolderLoading || loading.updateFileLoading}
					/>
				</RFlex>
			)}

			<RSearchHeader
				widthInput="275px"
				searchLoading={loading.allItemsLoading}
				searchData={data?.values?.groupingOfSearchFields?.name}
				handleSearch={() => handlers.setFieldValue("groupingOfSearchFields.name", "")}
				setSearchData={(value) => handlers.setFieldValue("groupingOfSearchFields.name", value)}
				handleChangeSearch={(value) => handlers.setFieldValue("groupingOfSearchFields.name", value)}
			/>
		</RFlex>
	);
};

export default RUploadFileSection;
