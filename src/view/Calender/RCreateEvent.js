import { Button } from "ShadCnComponents/ui/button";
import { Checkbox } from "ShadCnComponents/ui/checkbox";
import { Label } from "ShadCnComponents/ui/label";
import { RadioGroupItem } from "ShadCnComponents/ui/radio-group";
import { RadioGroup } from "ShadCnComponents/ui/radio-group";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RSelect from "components/Global/RComs/RSelect";
import RTextArea from "components/Global/RComs/RTextArea";
import tr from "components/Global/RComs/RTranslator";
import RNewInput from "components/RComponents/RNewInput/RNewInput";
import React from "react";

const RCreateEvent = (values, touched, errors, handleChange, handleBlur) => {
	return (
		<RFlex className="flex-col" styleProps={{ gap: "15px" }}>
			<RFlex className="flex-col">
				<Label>{tr("Event_name")}</Label>
				<RNewInput name="eventName" value={values.eventName} onChange={handleChange} onBlur={handleBlur} placeholder={tr("Event_Name")} />
			</RFlex>
			<RFlex className="flex-col">
				<Label>{tr("Event_Type")}</Label>
				<div className="w-full">
					<RSelect />
				</div>
			</RFlex>
			<RFlex className="flex-col">
				<Label>{tr("Summary_or_description")}</Label>
				<RTextArea />
			</RFlex>
			<RFlex>
				<Label>{tr("All_day")}</Label>
				<Checkbox className="border-themePrimary data-[state=checked]:bg-themePrimary" />
			</RFlex>
			<RFlex className="flex-col">
				<Label>{tr("Start_date_and_time")}</Label>
				<RNewInput />
			</RFlex>
			<RFlex className="flex-col">
				<Label>{tr("End_date_and_time")}</Label>
				<RNewInput />
			</RFlex>

			<RFlex className="flex-col" id="gender" styleProps={{ gap: "0px" }}>
				<span>{tr("Repeat")}</span>
				<RFlex>
					<RadioGroup
						// value={"ad"}
						className="flex flex-row"
						onValueChange={(e) => {
							// setGender(e);
						}}
					>
						<RFlex className="gap-2 items-center">
							<RadioGroupItem
								value="none"
								id="none"
								iconClassName="h-2.5 w-2.5 bg-themePrimary rounded-full"
								className="border-[1px] border-themePrimary"
							/>
							<Label htmlFor="none">{tr("None")}</Label>
						</RFlex>
						<RFlex className="gap-2 items-center">
							<RadioGroupItem
								value="every"
								id="every"
								iconClassName="h-2.5 w-2.5 bg-themePrimary rounded-full"
								className="border-[1px] border-themePrimary "
							/>
							<Label htmlFor="every">{tr("Every")}</Label>
						</RFlex>
					</RadioGroup>
				</RFlex>
			</RFlex>
			<RFlex className="flex-col">
				<Label>{tr("Event_Type")}</Label>
				<div className="w-full">
					<RSelect />
				</div>
			</RFlex>
			<RFlex>
				<Button>{tr("Create")}</Button>
				<Button variant="secondry">{tr("Cancel")}</Button>
			</RFlex>
		</RFlex>
	);
};

export default RCreateEvent;
