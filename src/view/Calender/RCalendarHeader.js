import RFlex from "components/Global/RComs/RFlex/RFlex";
import React from "react";
import calendarIcon from "../../assets/img/calendar.svg";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import filterIcon from "../../assets/img/filter.svg";
import printIcon from "../../assets/img/print.svg";
import settingsIcon from "../../assets/img/settings.svg";
import { DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown } from "reactstrap";
import RDropdown from "components/RComponents/RDropdown/RDropdown";

const RCalendarHeader = ({
	advanced = true,
	title = "Current Date",
	currentDay = 31,
	next,
	previous,
	today,
	currentFilter = "Month",
	toggleWorkDays,
	handleViewChange,
	handlePrint,
}) => {
	const actions = [
		{ name: "Day", onClick: () => handleViewChange("Day") },
		{ name: "Week", onClick: () => handleViewChange("Week") },
		{ name: "Month", onClick: () => handleViewChange("Month") },
	];
	return (
		<RFlex id="header" className="custom-header">
			<h1 style={{ margin: "0px", padding: "0px", fontSize: "25px", width: "250px" }}>{title}</h1>
			<RFlex className="align-items-center">
				<div style={{ position: "relative" }} onClick={() => today()}>
					<img src={calendarIcon} width={25} height={25} style={{ cursor: "pointer" }} />
					<span className="current-day">{currentDay}</span>
				</div>
				<RFlex id="buttons group" styleProps={{ width: "153px", gap: "2px" }} className="align-items-center">
					<button className="calander-button" onClick={() => previous()}>
						<i className="fa-solid fa-angle-left text-primary" />
					</button>
					<RDropdown
						actions={actions}
						TriggerComponent={
							<button className="calander-button" style={{ width: "52px" }}>
								{currentFilter}
							</button>
						}
					/>
					{/* <UncontrolledDropdown direction="right">
						<DropdownToggle aria-haspopup={true} color="default" data-toggle="dropdown" nav style={{ padding: "0px" }}>
							<button className="calander-button" style={{ width: "52px" }}>
								{currentFilter}
							</button>
						</DropdownToggle>
						<DropdownMenu persist aria-labelledby="navbarDropdownMenuLink" right className="mb-4">
							{["Day", "Week", "Month"]?.map((data, index) => (
								<DropdownItem
									key={data}
									onClick={() => {
										handleViewChange(data);
									}}
								>
									<span>{data}</span>
								</DropdownItem>
							))}
						</DropdownMenu>
					</UncontrolledDropdown> */}

					<button className="calander-button" onClick={() => next()}>
						<i className="fa-solid fa-angle-right text-primary" />
					</button>
				</RFlex>
			</RFlex>
			{advanced ? (
				<AppCheckbox
					onClick={() => {
						toggleWorkDays();
					}}
					label={"Only Show Work Days"}
					spanStyle={{ marginTop: "7px" }}
					paragraphStyle={{ marginTop: "9px", marginBottom: "0px" }}
				/>
			) : (
				<></>
			)}

			{advanced ? (
				<RFlex id="Right Section" styleProps={{ gap: "15px" }}>
					<img src={settingsIcon} style={{ cursor: "pointer" }} width={20} height={20} />
					<img src={printIcon} onClick={handlePrint} style={{ cursor: "pointer" }} width={20} height={20} />
				</RFlex>
			) : (
				<></>
			)}
		</RFlex>
	);
};

export default RCalendarHeader;
