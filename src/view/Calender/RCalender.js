import React from "react";
import "./cutsom.scss";
import FullCalendar from "@fullcalendar/react"; // must go before plugins
import dayGridPlugin from "@fullcalendar/daygrid"; // a plugin!
import timegridPlugin from "@fullcalendar/timegrid";
import listPlugin from "@fullcalendar/list";
import interactionPlugin from "@fullcalendar/interaction";
import RCalendarHeader from "./RCalendarHeader";
import { Checkbox } from "ShadCnComponents/ui/checkbox";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { Button } from "ShadCnComponents/ui/button";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";
import { Separator } from "ShadCnComponents/ui/separator";
import RHoverInput from "components/Global/RComs/RHoverInput/RHoverInput";
import { tagsColor } from "config/constants";

const RCalender = ({
	advanced = true,
	config = { initialView: "dayGridMonth", plugins: [dayGridPlugin, timegridPlugin, listPlugin, interactionPlugin] },
	handlePrint,
	eventTypes,
	handleAddingEventType,
	addingEventType,
	values,
	touched,
	errors,
	setFieldValue,
	setFieldTouched,
	setFieldError,
	handleChange,
	handleBlur,
	handleInputBlur,
	handleEditFunctionality,
	handlePickColor,
	setOpenCreateEventModal,
}) => {
	const [workDays, setWorkDays] = React.useState(false);
	const [title, setTitle] = React.useState("CurrentDate");
	const [currentDay, setCurrentDay] = React.useState(0);
	const [currentFilter, setCurrentFilter] = React.useState("Month");
	const calendarRef = React.useRef(null);

	React.useEffect(() => {
		calendarRef.current.getApi().changeView("dayGridMonth");
		setTitle(calendarRef.current.getApi().view.title);
		setCurrentDay(getDayNumber());
		return () => {};
	}, []);

	const toggleWorkDays = () => {
		setWorkDays(!workDays);
	};
	const today = () => {
		calendarRef.current.getApi().today();
		updateTitle();
	};
	const next = () => {
		calendarRef.current.getApi().next();
		updateTitle();
	};
	const previous = () => {
		calendarRef.current.getApi().prev();
		updateTitle();
	};
	const updateTitle = () => {
		const currentTitle = calendarRef.current.getApi().view.title;
		setTitle(currentTitle);
	};

	const getDayNumber = () => {
		const currentDate = calendarRef.current.getApi().getDate();
		const dayNumber = new Intl.DateTimeFormat("en", { day: "numeric" }).format(currentDate);
		return dayNumber;
	};
	const changeView = (viewName) => {
		calendarRef.current.getApi().changeView(viewName);
		updateTitle();
	};
	const handleViewChange = (viewType) => {
		switch (viewType) {
			case "Day":
				changeView("timeGridDay");
				setCurrentFilter("Day");
				break;
			case "Week":
				changeView("timeGridWeek");
				setCurrentFilter("Week");

				break;
			case "Month":
				changeView("dayGridMonth");
				setCurrentFilter("Month");

				break;
			default:
				console.error("Invalid viewType:", viewType);
				break;
		}
	};
	return (
		<RFlex id="containerrr" className="h-full" styleProps={{ gap: "10px" }}>
			<RFlex id="left Part" className="w-[23.5%] flex-col " styleProps={{ gap: "5px" }}>
				<Button onClick={() => setOpenCreateEventModal(true)} className="w-fit">
					{tr("New_Event")}
				</Button>
				<RFlex className="flex-col" styleProps={{ gap: "5px" }}>
					<RFlex className="flex-col items-start">
						<RFlex className="items-center justify-between w-full">
							<span className="font-bold">🗓️ {tr("Event_types")}</span>
							<i className={`${iconsFa6.plus} text-themePrimary cursor-pointer`} onClick={handleAddingEventType} />
						</RFlex>
						{values?.eventTypes?.map((eventType, index) => {
							console.log(`colors${index}`, eventType);
							return (
								<RFlex className="items-center relative">
									<Checkbox className="border-[1px]" style={{ borderColor: eventType.color, backgroundColor: eventType.color }} />
									<RHoverInput
										name={`eventTypes[${index}].name`}
										inputValue={values?.eventTypes?.[index]?.name}
										inputWidth={"120px"}
										type={"text"}
										focusOnInput={false}
										handleInputChange={handleChange}
										inputIsNotValidate={errors?.[index]?.name && touched?.[index]?.name}
										item={eventType}
										handleOnBlur={(event, item, extraInfo) => {
											handleInputBlur(event, item, extraInfo);
											handleBlur(event);
										}}
										extraInfo={{ index }}
										handleEditFunctionality={handleEditFunctionality}
										saved={values?.eventTypes?.[index]?.saved}
									/>
									{!values?.eventTypes?.[index]?.saved && (
										<div className="colorDivPicker flex gap-1">
											{tagsColor.map((tagColor) => (
												<div
													className="specificColor cursor-pointer"
													style={{ backgroundColor: tagColor }}
													onClick={() => handlePickColor(tagColor, index)}
												></div>
											))}
										</div>
									)}
								</RFlex>
							);
						})}
					</RFlex>
				</RFlex>
			</RFlex>
			<Separator orientation="vertical" className="h-auto" />

			<RFlex id="rightPart" className="flex-col w-[76.5%]" styleProps={{ gap: "0px" }}>
				<RCalendarHeader
					advanced={advanced}
					next={next}
					previous={previous}
					title={title}
					currentDay={currentDay}
					toggleWorkDays={toggleWorkDays}
					today={today}
					currentFilter={currentFilter}
					handleViewChange={handleViewChange}
					handlePrint={handlePrint}
				/>
				<FullCalendar
					ref={calendarRef}
					dateClick={(args) => {
						console.log("this is Args", args);
					}}
					plugins={config.plugins}
					initialView={config.initialView}
					weekends={!workDays}
					stickyHeaderDates
					stickyFooterScrollbar
					// events={events}
					// eventContent={renderEvent}
					// eventDataTransform={eventDataTransform}
				/>
			</RFlex>
		</RFlex>
	);
};

export default RCalender;

// plugins={[ dayGridPlugin, timeGridPlugin ]}
// initialView="dayGridMonth" // set the default view to monthly
