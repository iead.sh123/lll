import React, { useContext } from "react";
import { UnitPlanContext } from "logic/Courses/CourseManagement/UnitPlans/UnitPlanEditor/GUnitPlanEditor";
import { primaryColor } from "config/constants";
import styles from "../../RUnitPlan.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import iconsFa6 from "variables/iconsFa6";

const RUnitPlanItem = ({ sectionId, items }) => {
	const UnitPlanContextData = useContext(UnitPlanContext);

	return (
		<>
			{items.map((item) => (
				<div
					className={styles.card_add_item}
					key={item.id ? item.id : item.fakeId}
					ref={(ref) => {
						UnitPlanContextData.targetRefs.current[item.id ? item.id : item.fakeId] = ref;
					}}
				>
					<div
						onClick={() => {
							UnitPlanContextData.handleAddItemsToSection(sectionId, item?.id ? item?.id : item?.fakeId, "visible", true, "parent", true);
						}}
					>
						<RFlex
							styleProps={{
								flexDirection: "column",
								alignItems: "center",
								position: "absolute",
								top: "50%",
								left: "50%",
								transform: " translate(-50%, -50%)",
								width: "100%",
							}}
						>
							<p className="text-capitalize" style={{ color: primaryColor }}>
								{item?.text}
							</p>
							<p className="text-capitalize">{item?.items?.length > 0 ? `${item?.items?.length} items` : "no items"} </p>
						</RFlex>
					</div>

					<div className={styles.remove_icon_item}>
						<i
							className={iconsFa6.delete}
							style={{ color: "#DD0000", cursor: "pointer" }}
							onClick={() => UnitPlanContextData.handleRemoveItemFromSection(sectionId, item?.id ? item?.id : item?.fakeId)}
						/>
					</div>
				</div>
			))}
		</>
	);
};

export default RUnitPlanItem;
