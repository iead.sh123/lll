import React, { useContext } from "react";
import { UnitPlanContext } from "logic/Courses/CourseManagement/UnitPlans/UnitPlanEditor/GUnitPlanEditor";
import RUnitPlanSections from "./RUnitPlanSections/RUnitPlanSections";
import RUnitPlanHeader from "./RUnitPlanHeader";
import RUnitPlanViewer from "./RUnitPlanViewer";
import RUnitPlanForm from "./RUnitPlanForm";

const RUnitPlanEditor = () => {
	const UnitPlanContextData = useContext(UnitPlanContext);

	return (
		<>
			{UnitPlanContextData.previewMode ? (
				<RUnitPlanViewer
					unitPlan={UnitPlanContextData.unitPlan}
					handleBackToUnitPlanEditor={() => {}}
					previewMode={UnitPlanContextData.previewMode}
					setPreviewMode={UnitPlanContextData.setPreviewMode}
				/>
			) : (
				<>
					<RUnitPlanHeader />
					<RUnitPlanForm />
					<RUnitPlanSections />
				</>
			)}
		</>
	);
};

export default RUnitPlanEditor;
