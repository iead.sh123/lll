import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RUnitPlanItem from "./RUnitPlanItem";

const RUnitPlanItems = ({ items, sectionIndex }) => {
	return items.map((item, itemIndex) => (
		<RFlex styleProps={{ flexDirection: "column" }}>
			<RUnitPlanItem item={item} itemIndex={itemIndex + 1} sectionIndex={sectionIndex} />
			{item?.items && item?.items?.length > 0 && <RUnitPlanItems items={item?.items} sectionIndex={sectionIndex} />}
		</RFlex>
	));
};

export default RUnitPlanItems;
