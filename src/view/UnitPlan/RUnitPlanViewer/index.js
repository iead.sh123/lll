import React from "react";
import RUnitPlanMainData from "./RUnitPlanMainData";
import RUnitPlanSections from "./RUnitPlanSections";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import styles from "../RUnitPlan.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RUnitPlanViewer = ({
	unitPlan,
	handleBackToUnitPlanEditor,
	previewMode,
	setPreviewMode,
	exportUnitPlanAsPdfLoading,
	handleExportUnitPlanAsPdf,
	importCollaboration,
	removeAllActions,
}) => {
	return (
		<RFlex className={styles.up__viewer__container}>
			<RFlex className={styles.up__viewer__buttons}>
				{removeAllActions ? (
					""
				) : importCollaboration ? (
					<RButton text={tr`import`} onClick={() => importCollaboration()} color="primary" outline faicon={"fas fa-file-import"} />
				) : !previewMode ? (
					<>
						<RButton
							text={tr`export_as_pdf`}
							onClick={() => handleExportUnitPlanAsPdf(unitPlan.id)}
							color="primary"
							outline
							faicon={iconsFa6.pdf}
							loading={exportUnitPlanAsPdfLoading}
							disabled={exportUnitPlanAsPdfLoading}
						/>
						<RButton
							text={tr`edit`}
							onClick={() => handleBackToUnitPlanEditor && handleBackToUnitPlanEditor()}
							color="primary"
							outline
							faicon={"fa fa-pencil"}
						/>
					</>
				) : (
					<RButton text={tr`back_to_editor`} color="primary" faicon={"fa fa-pencil"} outline onClick={() => setPreviewMode(!previewMode)} />
				)}
			</RFlex>
			<RFlex className={styles.up__viewer__main}>
				<RUnitPlanMainData unitPlan={unitPlan} />
				<RFlex className={styles.up__viewer__main__data__description}>
					<span>
						<i className={iconsFa6.alignLeft} />
						&nbsp;
						<span className="font-weight-bold">{tr`description`} :</span>
					</span>
					<span>{unitPlan?.description}</span>
				</RFlex>
			</RFlex>

			<RUnitPlanSections unitPlanSections={unitPlan?.uplsections ? unitPlan?.uplsections : []} />
		</RFlex>
	);
};

export default RUnitPlanViewer;
