import React, { useState } from "react";
import { Button, Row, Col, Form, FormGroup, Input, Label } from "reactstrap";
import RSelect from "components/Global/RComs/RSelect";
import tr from "components/Global/RComs/RTranslator";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import styles from "./RStateMachine.Module.scss";
import { useDispatch, useSelector } from "react-redux";
import {
  addConstraintToNode,
  removeConstraintFromNode,
  addDataToConstraint,
  getInteractionConstraintType,
} from "store/actions/global/learningObjects";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";

const FormByConstraintType = ({
  constraintData,
  nodeId,
  constraintId,
  statuses,
  responses,
  handleAddDataToConstraint,
  handleAddResponseConstraintTypeToConstraint,
  handleAddResponseConstraint,
  handleRemoveAutoCorrectAnswer,
}) => {
  const [autoCorrectAnswer, serAutoCorrectAnswer] = useState("");
  return (
    <>
      {constraintData.type == "time" && (
        <RFlex>
          <Col xs={6}>
            <FormGroup>
              <Label>{tr`max_time`}</Label>
              <Input
                type="number"
                name="max_time_allowed_in_seconds"
                placeholder={tr`max_time`}
                defaultValue={constraintData.max_time_allowed_in_seconds}
                onChange={(e) => {
                  handleAddDataToConstraint(
                    e.target.name,
                    e.target.value,
                    nodeId,
                    constraintId
                  );
                }}
              />
            </FormGroup>
          </Col>
          <Col xs={6}>
            <FormGroup>
              <Label>{tr`min_time`}</Label>
              <Input
                type="number"
                name="min_time_allowed_in_seconds"
                placeholder={tr`min_time`}
                defaultValue={constraintData.min_time_allowed_in_seconds}
                onChange={(e) => {
                  handleAddDataToConstraint(
                    e.target.name,
                    e.target.value,
                    nodeId,
                    constraintId
                  );
                }}
              />
            </FormGroup>
          </Col>
        </RFlex>
      )}

      <RFlex>
        <Col xs={6}>
          <FormGroup>
            <Label>{tr`fulfilled_next_status`}</Label>
            <RSelect
              option={statuses}
              closeMenuOnSelect={true}
              placeholder={tr`fulfilled_next_status`}
              onChange={(e) => {
                handleAddDataToConstraint(
                  "fulfilled_next_status_name",
                  e.value,
                  nodeId,
                  constraintId
                );
              }}
              defaultValue={[
                {
                  label: constraintData.fulfilled_next_status_name,
                  value: constraintData.fulfilled_next_status_name,
                },
              ]}
            />
          </FormGroup>
        </Col>
        {constraintData.type !== "nothing" && (
          <Col xs={6}>
            <FormGroup>
              <Label>{tr`rejected_next_status`}</Label>
              <RSelect
                option={statuses}
                closeMenuOnSelect={true}
                placeholder={tr`rejected_next_status`}
                onChange={(e) => {
                  handleAddDataToConstraint(
                    "rejected_next_status_name",
                    e.value,
                    nodeId,
                    constraintId
                  );
                }}
                defaultValue={[
                  {
                    label: constraintData.rejected_next_status_name,
                    value: constraintData.rejected_next_status_name,
                  },
                ]}
              />{" "}
            </FormGroup>
          </Col>
        )}
      </RFlex>

      {constraintData.type == "response" && (
        <>
          <RFlex>
            <Col xs={6}>
              <FormGroup>
                <Label>{tr`response_types`}</Label>
                <RSelect
                  option={responses}
                  closeMenuOnSelect={true}
                  placeholder={tr`response_types`}
                  onChange={(e) => {
                    handleAddResponseConstraintTypeToConstraint(
                      "response_type",
                      e.value,
                      nodeId,
                      constraintId
                    );
                  }}
                  defaultValue={[
                    {
                      label: constraintData.response_constraint?.response_type,
                      value: constraintData.response_constraint?.response_type,
                    },
                  ]}
                />
              </FormGroup>
            </Col>
          </RFlex>
          {constraintData.response_constraint.response_type == "score" && (
            <RFlex>
              <Col xs={6}>
                <FormGroup>
                  <Label>{tr`score`}</Label>
                  <Input
                    type="number"
                    name="score"
                    placeholder={tr`score`}
                    defaultValue={
                      constraintData.response_constraint.correct_responses.score
                    }
                    onChange={(e) => {
                      handleAddResponseConstraint(
                        e.target.name,
                        e.target.value,
                        nodeId,
                        constraintId,
                        true
                      );
                    }}
                  />
                </FormGroup>
              </Col>
              <Col xs={6} className={styles.is_percentage}>
                <AppCheckbox
                  checked={
                    constraintData.response_constraint.correct_responses
                      ?.is_percentage
                  }
                  onChange={(event) =>
                    handleAddResponseConstraint(
                      "is_percentage",
                      event.target.checked,
                      nodeId,
                      constraintId,
                      true
                    )
                  }
                  label={tr`is_percentage`}
                />
              </Col>
            </RFlex>
          )}
          {constraintData.response_constraint.response_type ==
            "autocorrect" && (
            <>
              <RFlex>
                <Col xs={6}>
                  <FormGroup>
                    <Label>{tr`answers`}</Label>
                    <Input
                      type="text"
                      name="answers"
                      placeholder={tr`answers`}
                      defaultValue={autoCorrectAnswer}
                      value={autoCorrectAnswer}
                      onChange={(e) => serAutoCorrectAnswer(e.target.value)}
                    />
                  </FormGroup>
                </Col>
                <Col xs={2} className={styles.delete_button}>
                  <Button
                    className={"btn-icon " + styles.action_button}
                    color="success"
                    size="sm"
                    onClick={() => {
                      handleAddResponseConstraint(
                        "answers",
                        autoCorrectAnswer,
                        nodeId,
                        constraintId,
                        false
                      );
                      serAutoCorrectAnswer("");
                    }}
                    disabled={autoCorrectAnswer == "" ? true : false}
                  >
                    <i className="fa fa-plus text-white" />
                  </Button>
                </Col>
              </RFlex>

              <Col xs={12}>
                <div className={`mt-2 ${styles.autoCorrectAnswers}`}>
                  {constraintData.response_constraint.correct_responses &&
                    constraintData.response_constraint.correct_responses
                      .answers &&
                    constraintData.response_constraint.correct_responses.answers
                      .length > 0 &&
                    constraintData.response_constraint.correct_responses.answers.map(
                      (answer, ind) => (
                        <h6 style={{ position: "relative" }}>
                          {answer}
                          <p
                            className={styles.removeAutoCorrectAnswer}
                            onClick={() =>
                              handleRemoveAutoCorrectAnswer(
                                ind,
                                nodeId,
                                constraintId
                              )
                            }
                          >
                            X
                          </p>
                        </h6>
                      )
                    )}
                </div>
              </Col>
            </>
          )}
        </>
      )}
    </>
  );
};

export default FormByConstraintType;
