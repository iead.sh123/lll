import React from "react";
import AllNodes from "./AllNodes";
import styles from "./RStateMachine.Module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Row, Col, Form } from "reactstrap";

const RStateMachine = ({
  stateMachine,
  statuses,
  interactions,
  responses,
  interactionConstraintType,
  handleAddValueToStateMachine,
  handleAddNewNode,
  handleRemoveNode,
  handleAddStatusToNode,
}) => {
  return (
    <Form>
      <Row>
        <Col xs={12}>
          <RFlex className={styles.add_status} onClick={handleAddNewNode}>
            <i className="fa fa-plus"></i>
            <h6>{tr`add_new_status`}</h6>
          </RFlex>
        </Col>
        <Col xs={12}>
          {stateMachine.nodes && stateMachine.nodes.length > 0 && (
            <AllNodes
              stateMachine={stateMachine}
              statuses={statuses}
              interactions={interactions}
              responses={responses}
              handleAddValueToStateMachine={handleAddValueToStateMachine}
              handleRemoveNode={handleRemoveNode}
              handleAddStatusToNode={handleAddStatusToNode}
            />
          )}
        </Col>
      </Row>
    </Form>
  );
};

export default RStateMachine;
