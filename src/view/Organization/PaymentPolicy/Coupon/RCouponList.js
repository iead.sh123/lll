import React, { useContext, useState } from "react";
import { primaryColor, boldGreyColor, warningColor, successColor, dangerColor, lightGray } from "config/constants";
import { PaymentPolicyContext } from "logic/Organization/PaymentPolicy/GPaymentPolicy";
import { relativeDate } from "utils/dateUtil";
import { RLabel } from "components/Global/RComs/RLabel";
import RQPaginator from "components/Global/RComs/RQPaginator/RQPaginator";
import Hands_Give from "assets/img/new/Hands_Give.png";
import RTextIcon from "components/Global/RComs/RTextIcon/RTextIcon";
import iconsFa6 from "variables/iconsFa6";
import moment from "moment";
import Loader from "utils/Loader";
import styles from "../../ROrganization.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import REmptyData from "components/RComponents/REmptyData";

const RCouponList = () => {
	const PaymentPolicyData = useContext(PaymentPolicyContext);
	const newDate = moment();
	const [page, setPage] = useState(1);
	const [open, setOpen] = useState(false);

	const handleChangePage = (pageNumber) => {
		setPage(pageNumber);
	};
	const handleTooltipOpen = (text) => {
		setOpen(true);
		navigator.clipboard.writeText(text);
		setOpen(false);
	};

	if (PaymentPolicyData.couponLoading) return <Loader />;

	return (
		<RFlex className="mt-3 mb-3" styleProps={{ flexDirection: "column" }}>
			<RFlex className={styles.div__scroll + " scroll_hidden"} styleProps={{ flexDirection: "column" }}>
				{PaymentPolicyData.couponData?.data?.data?.data?.data?.length > 0 ? (
					PaymentPolicyData.couponData?.data?.data?.data?.data?.map((item) => (
						<RFlex
							key={item.id}
							className={+PaymentPolicyData.values.couponIdSelected == +item.id ? styles.card__list__selected : styles.card__list}
							onClick={(event) => {
								event.stopPropagation();
								PaymentPolicyData.tabType == "archive" ? {} : PaymentPolicyData.handleSelectCouponId(item.id, item);
							}}
						>
							<RFlex styleProps={{ gap: 0, width: "100%" }}>
								<RFlex className={styles.card__discount}>
									<span className={styles.card__discount__amount}>
										<RFlex styleProps={{ gap: 3 }}>
											<RFlex styleProps={{ gap: 0 }}>
												<span>
													{item.value}
													{item?.couponType == "Percent" ? "%" : "$"}
												</span>
											</RFlex>
										</RFlex>
									</span>
								</RFlex>
								<RFlex styleProps={{ flexDirection: "column", width: "85%", padding: "7px 10px" }}>
									<RFlex styleProps={{ justifyContent: "space-between" }}>
										<RFlex styleProps={{ gap: 3, color: boldGreyColor, fontSize: "14px", fontWeight: 500, fontWeight: "bold" }}>
											<span>
												{item.value}
												{item?.couponType == "Percent" ? "%" : "$"}
											</span>
											<span>{tr`off`} </span>
											<span>|</span>
											<span className={styles.heighLight__hover}>{item.code}</span>{" "}
											<div
												className="cursor-pointer"
												onClick={(event) => {
													event.stopPropagation();
													handleTooltipOpen(item.code);
												}}
											>
												<RLabel value={open ? tr`copied!` : tr`copy`} icon={iconsFa6.clone} lettersToShow={0} />
											</div>
										</RFlex>
									</RFlex>
									<RFlex styleProps={{ justifyContent: "space-between" }}>
										<RFlex className={styles.date}>
											<span style={{ color: lightGray }}>
												{tr`expired_at`}&nbsp;
												{moment(item.expireDate).format("L")}
											</span>
											<span style={{ fontSize: "12px" }}>
												{newDate.isAfter(moment(item.expireDate), "day") ? (
													<span style={{ color: dangerColor }}>{tr`expired`}</span>
												) : moment.duration(moment(item.expireDate).diff(newDate)).asHours() < 24 ? (
													<span style={{ color: warningColor }}>24 {tr`hours_left`}</span>
												) : (
													<span style={{ color: successColor }}> {relativeDate(item.expireDate)}</span>
												)}
											</span>
										</RFlex>
										<RTextIcon
											text={item?.maxNumber == -1 ? item?.numberOfUsers : `${item?.numberOfUsers + "/" + item?.maxNumber}`}
											icon={item?.maxNumber == -1 ? iconsFa6.earthAsia : iconsFa6.user}
											flexStyle={{
												color: item?.numberOfUsers !== item?.maxNumber && item?.maxNumber == -1 ? primaryColor : boldGreyColor,
											}}
										/>{" "}
									</RFlex>
								</RFlex>
							</RFlex>
						</RFlex>
					))
				) : (
					<REmptyData Image={Hands_Give} line1={tr`no_coupons_yet`} />
				)}
			</RFlex>

			{/* RQPaginator */}
			{PaymentPolicyData.couponData?.data?.data?.data?.data && PaymentPolicyData.couponData?.data?.data?.data?.paginate.last_page !== 1 && (
				<RQPaginator info={PaymentPolicyData.couponData} handleChangePage={handleChangePage} page={page} />
			)}
		</RFlex>
	);
};

export default RCouponList;
