import React, { useContext, useState, useMemo } from "react";
import { primaryColor, boldGreyColor, warningColor, successColor, dangerColor } from "config/constants";
import { PaymentPolicyContext } from "logic/Organization/PaymentPolicy/GPaymentPolicy";
import { relativeDate } from "utils/dateUtil";
import { RLabel } from "components/Global/RComs/RLabel";
import Hands_Give from "assets/img/new/Hands_Give.png";
import RTextIcon from "components/Global/RComs/RTextIcon/RTextIcon";
import iconsFa6 from "variables/iconsFa6";
import RLister from "components/Global/RComs/RLister";
import Loader from "utils/Loader";
import moment from "moment";
import styles from "../../ROrganization.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RCouponLister = () => {
	const PaymentPolicyData = useContext(PaymentPolicyContext);

	const [page, setPage] = useState(1);
	const [open, setOpen] = useState(false);

	const handleChangePage = (pageNumber) => {
		setPage(pageNumber);
	};

	const handleTooltipOpen = (text) => {
		setOpen(true);
		navigator.clipboard.writeText(text);
		setOpen(false);
	};

	const codeComponent = ({ item }) => {
		return (
			<RFlex
				className={`align-items-center`}
				styleProps={{ gap: 5 }}
				onClick={(event) => {
					event.stopPropagation();
					PaymentPolicyData.tabType == "archive" ? {} : PaymentPolicyData.handleSelectCouponId(item.id, item);
				}}
			>
				<div className="cursor-pointer" onClick={() => handleTooltipOpen(item?.code)}>
					<RLabel value={open ? tr`copied!` : tr`copy`} icon={iconsFa6.clone} lettersToShow={0} />
				</div>
				<RFlex styleProps={{ gap: 3 }} className={styles.heighLight__hover}>
					<span>{item?.code}</span>
				</RFlex>
			</RFlex>
		);
	};

	const valueComponent = ({ item }) => {
		return (
			<RFlex className={`align-items-center`}>
				<RFlex styleProps={{ gap: 3 }} className={styles.heighLight__hover}>
					<span>
						{item?.value}
						{item?.couponType == "Percent" ? "%" : "$"}
					</span>
				</RFlex>
			</RFlex>
		);
	};

	const dateComponent = ({ expiredAt }) => {
		const newDate = moment();
		const expiredDate = moment(expiredAt);
		const duration = moment.duration(expiredDate.diff(newDate));
		const hours = duration.asHours();
		const equalDates = newDate.isAfter(expiredDate, "day");
		return (
			<RFlex className={styles.date}>
				<span>
					{tr`expired_at`}&nbsp;
					{moment(expiredAt).format("L")}
				</span>
				<span style={{ fontSize: "12px" }}>
					{equalDates ? (
						<span style={{ color: dangerColor }}>{tr`expired`}</span>
					) : hours < 24 ? (
						<span style={{ color: warningColor }}>24 {tr`hours_left`}</span>
					) : (
						<span style={{ color: successColor }}> {relativeDate(expiredAt)}</span>
					)}
				</span>
			</RFlex>
		);
	};

	const usedByComponent = ({ item }) => {
		return (
			<RTextIcon
				text={item?.maxNumber == -1 ? item?.numberOfUsers : `${item?.numberOfUsers + "/" + item?.maxNumber}`}
				icon={item?.maxNumber == -1 ? iconsFa6.earthAsia : iconsFa6.user}
				flexStyle={{ gap: 5, color: item?.numberOfUsers !== item?.maxNumber && item?.maxNumber == -1 ? primaryColor : boldGreyColor }}
			/>
		);
	};

	const _records = useMemo(
		() =>
			PaymentPolicyData.couponData?.data?.data?.data?.data?.map((item) => ({
				details: [
					{
						key: tr`code`,
						value: codeComponent({ item: item }),
						type: "component",
					},
					{
						key: tr`amount`,
						value: valueComponent({ item: item }),
						type: "component",
					},
					{
						key: tr`expiry_date`,
						value: dateComponent({ expiredAt: item?.expireDate }),
						type: "component",
					},
					{
						key: tr`used_by`,
						value: usedByComponent({ item: item }),
						type: "component",
					},
				],
			})),
		[PaymentPolicyData.couponData?.data?.data?.data?.data]
	);

	if (PaymentPolicyData.couponLoading) return <Loader />;
	return (
		<RFlex className="mt-3" styleProps={{ flexDirection: "column" }}>
			<RFlex className={styles.div__scroll + " scroll_hidden"} styleProps={{ flexDirection: "column" }}>
				<RLister
					Records={_records}
					info={PaymentPolicyData.couponData}
					withPagination={true}
					handleChangePage={handleChangePage}
					page={page}
					Image={Hands_Give}
					line1={tr`no_coupons_yet`}
				/>
			</RFlex>
		</RFlex>
	);
};

export default RCouponLister;
