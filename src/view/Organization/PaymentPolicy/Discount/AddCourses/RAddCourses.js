import React from "react";
import { Row, Col } from "reactstrap";
import styles from "../../../ROrganization.module.scss";
import RCategoryTree from "./RCategoryTree";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RCoursesContent from "./CoursesContent/RCoursesContent";
import { warningColor } from "config/constants";
import tr from "components/Global/RComs/RTranslator";

const RAddCourses = ({ formProperties, data, loading, handlers }) => {
	return (
		<Row>
			<Col lg={2} md={12} className="p-0 m-0">
				<div className={styles.category__tree + " scroll_hidden"}>
					<RCategoryTree categoriesTree={data.categoriesTree} formProperties={formProperties} handlers={handlers} />
				</div>

				{/* uncategorized */}
				{data.categoriesTree
					.filter((el) => el.name.toLowerCase() === "uncategorized")
					.map((category) => (
						<RFlex className={styles.category__un__categorized} key={category.id}>
							<p
								className={`m-0 cursor-pointer ${formProperties.values.categoryId == "uncategorized" ? styles.heightLight : ""}`}
								onClick={() => handlers.handleChangeCAtegory("uncategorized")}
							>
								{category?.name?.length > 25 ? category.name?.substring(0, 25) : category.name}
							</p>
							<div className={styles.courses__count}>{category.courses}</div>
						</RFlex>
					))}
			</Col>

			<Col lg={10} md={12} className={styles.courses__content + " scroll_hidden"}>
				{data.checkIfIHaveGeneralDiscount && (
					<span style={{ color: warningColor }}>
						<span>{data?.generalDiscountData?.value}% &nbsp;</span>
						{tr`general discount is already applied`}
					</span>
				)}
				<RCoursesContent data={data} formProperties={formProperties} handlers={handlers} loading={loading} />
			</Col>
		</Row>
	);
};

export default RAddCourses;
