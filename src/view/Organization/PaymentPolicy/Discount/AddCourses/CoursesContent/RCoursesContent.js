import React from "react";
import RCoursesContentSearch from "./RCoursesContentSearch";
import RCoursesContentFilter from "./RCoursesContentFilter";
import RTableCourses from "./RTableCourses";
import RListCourses from "./RListCourses";
import RFlex from "components/Global/RComs/RFlex/RFlex";

const RCoursesContent = ({ data, formProperties, handlers, loading }) => {
	return (
		<RFlex styleProps={{ flexDirection: "column" }}>
			<RCoursesContentSearch creatorsData={data.creators} formProperties={formProperties} loading={loading} handlers={handlers} />
			<RCoursesContentFilter formProperties={formProperties} handlers={handlers} loading={loading} data={data} />
			{formProperties.values.switchMode ? (
				<RTableCourses coursesData={data.courses} loading={loading} formProperties={formProperties} handlers={handlers} />
			) : (
				<RListCourses coursesData={data.courses} loading={loading} formProperties={formProperties} handlers={handlers} />
			)}
		</RFlex>
	);
};

export default RCoursesContent;
