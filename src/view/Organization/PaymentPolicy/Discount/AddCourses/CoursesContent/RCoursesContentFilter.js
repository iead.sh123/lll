import React from "react";
import AppNewCheckbox from "components/Global/RComs/AppNewCheckbox/AppNewCheckbox";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import Switch from "react-bootstrap-switch";
import Slider from "rc-slider";
import styles from "../../../../ROrganization.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { primaryColor, boldGreyColor } from "config/constants";
import "rc-slider/assets/index.css";

const RCoursesContentFilter = ({ formProperties, handlers, loading, data }) => {
	const mPrice = formProperties.values.maxPrice;
	const style = {
		width: 250,
	};

	const marks = {
		1: <strong>1</strong>,
		1000000: <strong>{mPrice}</strong>,
	};
	return (
		<RFlex styleProps={{ justifyContent: "space-between", minHeight: "35px" }}>
			<RFlex>
				<RFlex
					styleProps={{
						cursor: "pointer",
						alignItems: "center",
						color: formProperties.values.showFilter ? primaryColor : boldGreyColor,
						height: "100%",
					}}
					onClick={() => handlers.handleShowFilter()}
				>
					<i className={iconsFa6.filter} />
					{tr`filter`}
				</RFlex>
				{formProperties.values.showFilter && (
					<RFlex styleProps={{ height: "100%", justifyContent: "flex-start", alignItems: "center", gap: 20, flexWrap: "wrap" }}>
						<div style={style}>
							<Slider
								range={3}
								step={5}
								min={1}
								max={1000000}
								marks={marks}
								onChange={(e) => formProperties.setFieldValue("filterValue", e)}
								defaultValue={formProperties.values.filterValue}
							/>
						</div>
						<RFlex styleProps={{ gap: 0 }}>
							<RButton
								text={tr`apply`}
								onClick={() => handlers.handleApplyFilter()}
								color="primary"
								className={styles.apply__button + " p-1"}
								loading={loading.coursesLoading}
								disabled={loading.coursesLoading}
							/>
							<RButton
								text={tr`reset`}
								onClick={() => handlers.handleResetFilter()}
								color="link"
								className={styles.reset__button + " p-1"}
								disabled={loading.coursesLoading}
							/>
						</RFlex>
					</RFlex>
				)}
			</RFlex>

			<RFlex styleProps={{ height: "100%", position: "relative", top: 5 }}>
				<RFlex styleProps={{ height: "100%", position: "relative", top: 2 }}>
					<AppNewCheckbox
						label={tr`select_all`}
						onChange={(event) => handlers.handleSelectAndUnSelectAllCourses({ categoryId: formProperties.values.categoryId })}
						// checked={
						// 	formProperties?.values?.allCourses?.filter((courseId) => {
						// 		return data?.courses.some((course) => course.id === courseId);
						// 	})
						// 		? ""
						// 		: formProperties.values.categoryIds.includes(formProperties.values.categoryId)
						// }
						checked={formProperties.values.categoryIds.includes(formProperties.values.categoryId)}
						disabled={data?.courses?.length == 0}
					/>
				</RFlex>
				<Switch
					value={formProperties.values.switchMode}
					offText={<i className={iconsFa6.list} />}
					onText={<i className={iconsFa6.list} />}
					onColor="success"
					offColor="success"
					onChange={() => handlers.handleSwitchMode()}
					size="small"
				/>
			</RFlex>
		</RFlex>
	);
};

export default RCoursesContentFilter;
