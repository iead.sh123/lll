import React from "react";
import removeDecimalIfNeeded from "utils/removeDecimalIfNeeded";
import AppNewCheckbox from "components/Global/RComs/AppNewCheckbox/AppNewCheckbox";
import DefaultImage from "assets/img/new/course-default-cover.png";
import RProfileName from "components/Global/RComs/RProfileName/RProfileName";
import styles from "../../ROrganization.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { successColor } from "config/constants";
import { Services } from "engine/services";
import { isConstructorDeclaration } from "typescript";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";

const RDiscountCourses = ({ course, states, handlers, discountDetails }) => {
	const idsOrderGenerator = (function* () {
		let i = 1;
		while (true) yield i++;
	})();
	return (
		<RFlex className={styles.course__discount}>
			<div className={styles.course__discount__image__container}>
				<img
					src={course?.image_id == undefined ? DefaultImage : `${Services.storage.file + course.image_id}`}
					className={styles.course__discount__image}
					alt={`${course.id}`}
					width={"75px"}
					height={"85px"}
				/>
				<RFlex className={styles.course__discount__prices}>
					<RFlex className={styles.course__discount__price} styleProps={{ color: successColor }}>
						{course?.paymentInfo?.priceAfterDiscount !== course?.paymentInfo?.price && (
							<span>
								{course?.paymentInfo?.orgCurrency}&nbsp;
								{removeDecimalIfNeeded(
									+course?.paymentInfo?.priceAfterDiscount !== course?.paymentInfo?.price
										? course?.paymentInfo?.priceAfterDiscount
										: course?.paymentInfo?.price
								)}
							</span>
						)}
					</RFlex>
					<RFlex className={styles.course__discount__price__after__discount}>
						<span>
							{course?.paymentInfo?.orgCurrency}&nbsp;
							{removeDecimalIfNeeded(course?.paymentInfo?.price)}
						</span>
					</RFlex>
				</RFlex>
			</div>
			<RFlex styleProps={{ width: "100%", flexDirection: "column", justifyContent: "center" }}>
				<span>{course?.name}</span>
				<RProfileName key={1} img={course.creator.image_hash_id} name={course.creator.full_name} />
			</RFlex>
			{states?.showActions ? (
				states?.showActions == discountDetails?.id && (
					<div className={styles.course__discount__checkbox}>
						<AppNewCheckbox
							onChange={(e) => handlers.handleSelectCourses(course.id)}
							checked={states.values.allCourses.includes(+course.id)}
						/>
					</div>
				)
			) : (
				<div className={styles.course__discount__checkbox}>
					<AppNewCheckbox
						onChange={(e) => {
							e.stopPropagation();
							handlers.handleSelectCourses({ courseId: course.id });
						}}
						checked={states.values.allCourses.includes(+course.id)}
						idsOrderGenerator={course.id}
					/>
				</div>
			)}
		</RFlex>
	);
};

export default RDiscountCourses;
