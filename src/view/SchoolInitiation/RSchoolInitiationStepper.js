import React from "react";
import useWindowDimensions from "components/Global/useWindowDimensions";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import styles from "./RSchoolInitiation.module.scss";
import tr from "components/Global/RComs/RTranslator";
import { primaryColor } from "config/constants";
import { useParams } from "react-router-dom";

const RSchoolInitiationStepper = () => {
  const { sIType, sITypeId } = useParams();
  const { width } = useWindowDimensions();
  const mobile = width < 1000;

  return (
    <RFlex
      styleProps={{
        flexDirection: mobile ? "column" : "row",
        justifyContent: mobile ? "center" : "space-between",
        margin: mobile ? "auto" : "",
      }}
      className="mb-4"
    >
      <RFlex>
        <div
          className={styles.stepper_circle}
          style={{
            backgroundColor: !sIType ? primaryColor : "#dddddd",
            color: !sIType ? "white" : "black",
          }}
        >
          1
        </div>
        <p className={styles.stepper_text}>{tr`create_education_stage`}</p>
        {!mobile && <p className={styles.stepper_border}></p>}
      </RFlex>
      <RFlex>
        <div
          className={styles.stepper_circle}
          style={{
            backgroundColor:
              sIType == "education_stage" ? primaryColor : "#dddddd",
            color: sIType == "education_stage" ? "white" : "black",
          }}
        >
          2
        </div>
        <p className={styles.stepper_text}>{tr`create_grade_level`}</p>
        {!mobile && <p className={styles.stepper_border}></p>}
      </RFlex>
      <RFlex>
        <div
          className={styles.stepper_circle}
          style={{
            backgroundColor:
              sIType == "grade_levels" ? primaryColor : "#dddddd",
            color: sIType == "grade_levels" ? "white" : "black",
          }}
        >
          3
        </div>
        <p className={styles.stepper_text}>{tr`create_curricula`}</p>
      </RFlex>
    </RFlex>
  );
};

export default RSchoolInitiationStepper;
