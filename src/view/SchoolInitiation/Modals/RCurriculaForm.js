import React, { useContext } from "react";
import { Row, Col, Form, FormGroup, Input } from "reactstrap";
import { SchoolInitiationContext } from "logic/SchoolInitiation/GSchoolInitiation";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useParams } from "react-router-dom";
import RSchoolInitiationButtons from "./RSchoolInitiationButtons";
import RSelect from "components/Global/RComs/RSelect";
import styles from "../RSchoolInitiation.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RCurriculaForm = () => {
  const SchoolInitiationData = useContext(SchoolInitiationContext);
  const { tabTitle, semesterId, sITypeId } = useParams();

  return (
    <Form className={tabTitle == "properties" ? styles.add_form : ""}>
      <Row>
        <Col xs={12}>
          <RFlex>
            <label>{tr("main_curriculum")}</label>
            <FontAwesomeIcon icon={faQuestionCircle} className="text-muted" />
          </RFlex>
          <FormGroup>
            <RSelect
              option={SchoolInitiationData.mainCourses}
              closeMenuOnSelect={true}
              placeholder={tr`main_curriculum`}
              value={
                SchoolInitiationData.addCurricula &&
                SchoolInitiationData.addCurricula.main_course
              }
              onChange={(event) => {
                SchoolInitiationData.handleChangeCurricula(
                  "main_course",
                  event
                );
              }}
              isLoading={SchoolInitiationData.mainCoursesLoading}
            />
          </FormGroup>
        </Col>

        <Col xs={12}>
          <RFlex>
            <label>{tr("name")}</label>
            <FontAwesomeIcon icon={faQuestionCircle} className="text-muted" />
          </RFlex>
          <FormGroup>
            <Input
              name="name"
              type="text"
              placeholder={tr`name`}
              value={SchoolInitiationData.addCurricula.name}
              onChange={(event) => {
                SchoolInitiationData.handleChangeCurricula(
                  event.target.name,
                  event.target.value
                );
              }}
            />
          </FormGroup>
        </Col>
      </Row>
      {tabTitle && <RSchoolInitiationButtons />}
    </Form>
  );
};

export default RCurriculaForm;
