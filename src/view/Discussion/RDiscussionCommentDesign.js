import React, { useContext } from "react";
import { Row, Col, FormGroup, Input, Collapse } from "reactstrap";
import { DiscussionFilterContext } from "logic/Discussion/GDiscussionFilter";
import { DiscussionContext } from "logic/Discussion/GDiscussion";
import { primaryColor } from "config/constants";
import { useParams } from "react-router-dom";
import RDiscussionPostDesign from "./RDiscussionPostDesign";
import RFileUploader from "components/Global/RComs/RFileUploader";
import RButton from "components/Global/RComs/RButton";
import styles from "./RDiscussion.module.scss";
import tr from "components/Global/RComs/RTranslator";

const RDiscussionCommentDesign = ({ post, mB, ml, community, typeContent, specificMandatoryPost, indRec, seeMoreComment, postId }) => {
	const { cohortId, categoryName, sectionId } = useParams();
	const DiscussionData = useContext(categoryName ? DiscussionFilterContext : DiscussionContext);
	return (
		<>
			<FormGroup className={ml}>
				<div style={{ position: "relative" }}>
					<Input
						name="text"
						required
						type="textarea"
						placeholder={indRec == 1 ? tr`write a replay` : tr`write a comment`}
						onChange={(event) => {
							DiscussionData.handleChangeCreateComment(event.target.name, event.target.value);
							DiscussionData.handleChangeCreateComment("reply_id", post?.content_id);
							DiscussionData.handleChangeCreateComment("post_id", typeContent == "post" ? post?.id : null);
							DiscussionData.handleChangeCreateComment("comment_id", typeContent == "comment" ? post?.id : null);
							DiscussionData.handleChangeCreateComment("for_community", cohortId ? false : true);
							DiscussionData.handleChangeCreateComment("typeContent", typeContent);
						}}
						style={{ width: "100%" }}
					/>

					<RButton
						text={indRec ? tr`replay` : tr`comment`}
						color="primary"
						className={styles.submit_button}
						onClick={() => DiscussionData.handleSaveComment()}
						loading={DiscussionData.addCommentLoading}
						disabled={DiscussionData.addCommentLoading ? true : false}
					/>
				</div>
			</FormGroup>

			{post?.last_comment_id !== null && (
				<h6
					onClick={() => {
						if (DiscussionData.specificContentLoad) {
							DiscussionData.handleLoadPreviousComments(post.content_id, post.last_comment_id, typeContent, indRec ? "replay" : "comment");
						}
					}}
					className={`mb-4 mt-4 ${ml}`}
					style={{
						textDecoration: "underline",
						color: primaryColor,
						cursor: "pointer",
					}}
				>
					{tr`see more`}
				</h6>
			)}

			{post?.comments &&
				post.comments?.length > 0 &&
				post.comments?.map((comment) => (
					<div key={comment.id}>
						<RDiscussionPostDesign
							post={comment}
							mB={"mb-4"}
							ml={indRec == 1 ? "ml-4" : "ml-0"}
							community={community}
							typeContent={"comment"}
							specificMandatoryPost={""}
							indRec={indRec + 1}
							seeMoreComment={false}
						/>
					</div>
				))}
		</>
	);
};

export default RDiscussionCommentDesign;
