import React, { useContext } from "react";
import { Row, Col, Form, Label, FormGroup, Input, FormText } from "reactstrap";
import { DiscussionContext } from "logic/Discussion/GDiscussion";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPaperclip } from "@fortawesome/free-solid-svg-icons";
import { useParams } from "react-router-dom";
import RFileUploader from "components/Global/RComs/RFileUploader";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import AppModal from "components/Global/ModalCustomize/AppModal";
import RButton from "components/Global/RComs/RButton";
import RSelect from "components/Global/RComs/RSelect";
import Loader from "utils/Loader";
import styles from "./RDiscussion.module.scss";
import style from "./RDiscussion.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Services } from "engine/services";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";

const RCreatePost = ({ cohorts, cohortsLoading }) => {
	const DiscussionData = useContext(DiscussionContext);
	const { cohortId } = useParams();
	return (
		<AppModal
			show={DiscussionData.createPost}
			parentHandleClose={DiscussionData.handleCloseAddPost}
			header={tr`create_new_post`}
			size={"xl"}
			headerSort={
				<Form>
					{cohortsLoading ? (
						<Loader />
					) : (
						<Row className="mb-3">
							<Col lg={12} xs={12} className="mb-2">
								<RFlex>
									<Label className="text-muted">{tr`Item name`}</Label>
									<FontAwesomeIcon icon={faQuestionCircle} className="text-muted" />
								</RFlex>
								<FormGroup>
									<Input
										name="title"
										required
										type="text"
										placeholder={tr`what is this post about ?`}
										value={DiscussionData.cPost.title}
										onChange={(event) => DiscussionData.handleChangeCreatePost(event.target.name, event.target.value)}
									/>
								</FormGroup>
							</Col>

							<Col lg={12} xs={12} className="mb-2">
								<RFlex>
									<Label className="text-muted">{tr`content`}</Label>
									<FontAwesomeIcon icon={faQuestionCircle} className="text-muted" />
								</RFlex>
								<FormGroup>
									<Input
										name="text"
										required
										type="textarea"
										placeholder={tr`describe or ask something`}
										value={DiscussionData.cPost.text}
										onChange={(event) => DiscussionData.handleChangeCreatePost(event.target.name, event.target.value)}
									/>

									{DiscussionData.createPostValidation.text && (
										<FormText color="danger">{DiscussionData.createPostValidation.text}</FormText>
									)}
								</FormGroup>
							</Col>
							{!cohortId && (
								<Col lg={12} xs={12} className="mb-2">
									<RFlex>
										<Label className="text-muted">{tr`who can see this ?`}</Label>
										<FontAwesomeIcon icon={faQuestionCircle} className="text-muted" />
									</RFlex>
									<FormGroup>
										<RSelect
											name="cohort_id"
											option={cohorts}
											closeMenuOnSelect={true}
											placeholder={tr`select`}
											// value={{
											//   label: DiscussionData.cPost.cohort_name,
											//   value: DiscussionData.cPost.cohort_id,
											// }}
											value={
												DiscussionData?.postId
													? {
															label: DiscussionData.cPost.cohort_name,
															value: DiscussionData.cPost.cohort_id,
													  }
													: DiscussionData.cPost.cohort_id_obj
											}
											onChange={(event) => {
												DiscussionData.handleChangeCreatePost("cohort_id", event.value);
												DiscussionData.handleChangeCreatePost("cohort_id_obj", event);
											}}
										/>
										{DiscussionData.createPostValidation.cohort_id && (
											<FormText color="danger">{DiscussionData.createPostValidation.cohort_id}</FormText>
										)}
									</FormGroup>
								</Col>
							)}
							<Col xs={12} lg={6} className="mb-2">
								<RFlex>
									<FontAwesomeIcon icon={faPaperclip} />
									<label>{tr("attachments")}</label>
								</RFlex>
								<div className={style.attachment}>
									<RFileSuite
										parentCallback={(event) => DiscussionData.handleChangeCreatePost("attachments", event)}
										singleFile={false}
										binary={true}
										value={DiscussionData.cPost.attachments}
									/>
								</div>
							</Col>
							<Col xs={12} lg={6} className="mb-2">
								<RFlex>
									<label>{tr("comments settings")}</label>
								</RFlex>
								<RFlex>
									<AppCheckbox
										checked={DiscussionData.cPost.prevent_comments}
										onChange={(event) => DiscussionData.handleChangeCreatePost("prevent_comments", event.target.checked)}
										disabled={
											DiscussionData.cPost.private_comments ||
											DiscussionData.cPost.mandatory_comments ||
											DiscussionData.cPost.mandatory_replies_check ||
											DiscussionData.cPost.replies_must_be_written
												? true
												: false
										}
										label={tr`prevent_comments`}
									/>
									<i className={`fa fa-question-circle-o ${styles.circle_icon}`} aria-hidden="true"></i>
								</RFlex>
								<RFlex>
									<AppCheckbox
										checked={DiscussionData.cPost.private_comments}
										onChange={(event) => DiscussionData.handleChangeCreatePost("private_comments", event.target.checked)}
										disabled={DiscussionData.cPost.prevent_comments ? true : false}
										label={tr`private_comments`}
									/>
									<i className={`fa fa-question-circle-o ${styles.circle_icon}`} aria-hidden="true"></i>
								</RFlex>
								<RFlex>
									<AppCheckbox
										checked={DiscussionData.cPost.mandatory_comments}
										onChange={(event) => DiscussionData.handleChangeCreatePost("mandatory_comments", event.target.checked)}
										disabled={DiscussionData.cPost.prevent_comments || DiscussionData.cPost.private_comments ? true : false}
										label={tr`mandatory_comments`}
									/>
									<i className={`fa fa-question-circle-o ${styles.circle_icon}`} aria-hidden="true"></i>
								</RFlex>
								<RFlex>
									<AppCheckbox
										checked={DiscussionData.cPost.mandatory_replies_check}
										onChange={(event) => DiscussionData.handleChangeCreatePost("mandatory_replies_check", event.target.checked)}
										disabled={DiscussionData.cPost.private_comments ? true : false}
										label={tr`replies_must_be_written :`}
									/>
									<FormGroup>
										<Input
											style={{ width: "60px" }}
											name="mandatory_replies"
											required
											type="number"
											value={DiscussionData.cPost.mandatory_replies}
											onChange={(event) => DiscussionData.handleChangeCreatePost(event.target.name, event.target.value)}
										/>
									</FormGroup>
									<i className={`fa fa-question-circle-o ${styles.circle_icon}`} aria-hidden="true"></i>
								</RFlex>
							</Col>
							<RFlex styleProps={{ justifyContent: "end", width: "100%" }}>
								<RButton
									text={DiscussionData.postId ? tr`save` : tr`publish`}
									color="primary"
									onClick={DiscussionData.postId ? DiscussionData.handleUpdatePost : DiscussionData.handleCreatePost}
									loading={DiscussionData.addPostLoading}
								/>
							</RFlex>
						</Row>
					)}
				</Form>
			}
		/>
	);
};

export default RCreatePost;
