import React, { useContext } from "react";
import AppModal from "components/Global/ModalCustomize/AppModal";
import { Table } from "reactstrap";

const RShowAllPeople = () => {
  const { cohortId, categoryName } = useParams();

  const DiscussionData = useContext(
    categoryName ? DiscussionFilterContext : DiscussionContext
  );

  return (
    <AppModal
      show={DiscussionData.showAllPeople}
      parentHandleClose={DiscussionData.handleCloseSeeAllPeople}
      header={tr`show_all_people`}
      size={"lg"}
      headerSort={
        <Table>
          <thead>
            <tr>
              <th>Full Name</th>
            </tr>
          </thead>
          <tbody>
            {DiscussionData.seeAllPeople.map((person) => (
              <tr key={person.id}>
                <td>{person.user.full_name}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      }
    />
  );
};

export default RShowAllPeople;
