import tr from "components/Global/RComs/RTranslator";
import React from "react";
import { Row, Col, Button } from "reactstrap";

const RScorm = React.forwardRef(({ src, width, height, title, sco, hasNextSco, hasPrevSco, handleNextSco, handlePrevSco, onLoad }, ref) => {
	return (
		<Row>
			<Col xs="12">
				<iframe
					ref={ref}
					id="scormFrame"
					height="800"
					width="100%"
					sandbox="allow-same-origin allow-modals allow-scripts allow-popups allow-forms allow-top-navigation"
					src={src}
					title={title ?? sco?.get("cmi.core.lesson_location")}
				></iframe>
			</Col>

			<Col>
				<Button disabled={!hasPrevSco} onClick={handlePrevSco}>
					{tr`Previous`}
				</Button>
				<Button disabled={!hasNextSco} onClick={handleNextSco}>
					{tr`Next`}
				</Button>
			</Col>
		</Row>
	);
});

export default RScorm;

/**            props.sco **/

/*{
    // status of the connection to the SCORM API
    apiConnected: Bool,
 
    // cmi.core.student_name (SCORM 1.2) || cmi.learner_name (SCORM 2004)
    learnerName: String,
 
    // indication of course status
    completionStatus: String,
 
    // cmi.suspend_data parsed as an object (all suspend_data must be a JSON.stringify'd object for the suspend_data to work properly with RSP)
    suspendData: Object,
 
   
 
    // calling this function will update props.sco.suspendData with the current suspend_data from the LMS
    getSuspendData: Function () returns a Promise,
 
    // this function takes the required key and value arguments and merges them into the suspendData Object, overwriting the value if the key already exists. It then stringifies the object and saves it to the LMS as suspend_data
    setSuspendData: Function (key, val) returns a Promise,
 
    // resets the suspend_data to an empty object, clearing any existing key:value pairs
    clearSuspendData: Function () returns a Promise,
 
    // sends an updated course status to the LMS, accepts one of: "passed", "completed", "failed", "incomplete", "browsed", "not attempted"
    setStatus: Function (string) returns a Promise,
 
    // sends a score to the LMS via an object argument -- { value: Number - score (required), min: Number - min score (optional), max: Number - max score (optional), status: String - same as setStatus method (optional) }
    setScore: Function ({ value, min, max, status }) returns a Promise,
 
    // sets a SCORM value, ex: props.sco.set('cmi.score.scaled', 100)
    set: Function (string, val) returns a Promise,
 
    // gets a SCORM value from the LMS, ex: props.sco.get('cmi.score.scaled')
    get: Function (string) returns the LMS value
  } */
