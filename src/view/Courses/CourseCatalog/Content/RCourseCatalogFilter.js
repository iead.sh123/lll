import React from "react";
import iconsFa6 from "variables/iconsFa6";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Switch } from "ShadCnComponents/ui/switch";
import { Button } from "ShadCnComponents/ui/button";
import { Input } from "reactstrap";

const RCourseCatalogFilter = ({ data, handlers }) => {
	const colors = {
		textColor: data.values.showFilter ? "text-themePrimary" : "text-themeBoldGrey",
	};

	return (
		<RFlex className="align-items-center justify-between w-[100%] min-h-[35px]">
			<RFlex className="h-[100%]">
				<RFlex className={`${colors.textColor} cursor-pointer align-items-center h-[100%]`} onClick={() => handlers.handleShowFilter()}>
					<i className={`${iconsFa6.filter} text-themeBoldGrey`} />
					{tr`filter`}
				</RFlex>
				{data.values.showFilter && (
					<RFlex className="items-center">
						<div className="w-20">
							<Input
								name="credit"
								type="number"
								placeholder={tr`credit`}
								onChange={(event) => handlers.setFieldValue("credit", event.target.value)}
								min={0}
								className={data.errors?.credit ? "input__error" : ""}
							/>
							{/* {data.errors.credit && <FormText color="danger">{data.errors.credit}</FormText>} */}
						</div>

						<RFlex className="gap-1 text-themeThird">
							<Button onClick={() => handlers.setFieldValue("groupingOfSearchFields.credit", data.values.credit ?? "")}>{tr`apply`}</Button>
							<Button onClick={() => handlers.handleResetFilter()} variant="ghost">{tr`reset`}</Button>
						</RFlex>
					</RFlex>
				)}
			</RFlex>
			{/* - - - - - - - Switch - - - - - - - */}

			<Switch
				checked={data.values.switchMode}
				onCheckedChange={(event) => {
					handlers.handleSwitchBetweenTowMode();
				}}
				className="data-[state=checked]:bg-themeSuccess"
			/>
		</RFlex>
	);
};

export default RCourseCatalogFilter;
