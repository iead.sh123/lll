import React from "react";
import RUploader from "components/RComponents/RUploader";
import iconsFa6 from "variables/iconsFa6";
import RSelect from "components/Global/RComs/RSelect";
import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { FormGroup, FormText, Input } from "reactstrap";
import { getInputClass } from "utils/getInputClass";
import RTooltip from "components/RComponents/RTooltip/RTooltip";
import RAddTags from "components/RComponents/RAddTags";

const RCreateCourseCatalog = ({ data, formProperties, handlers, loading }) => {
	return (
		<form onSubmit={handlers.handleSubmit}>
			<RFlex styleProps={{ width: "100%", flexDirection: "column", paddingTop: "10px" }}>
				<RFlex styleProps={{ alignItems: "center" }}>
					<span>{data?.masterCourseId ? tr`edit_course` : tr`create_new_course`}</span>
				</RFlex>
				<RFlex className="flex-wrap justify-between gap-[5px]">
					<RFlex className="flex-col w-[48%] gap-0">
						<FormGroup style={{ width: "100%" }}>
							<label>{tr("course_name")}</label>
							<Input
								autoFocus
								name="name"
								type="text"
								value={formProperties.values.name}
								placeholder={tr("course_name")}
								onBlur={formProperties.handleBlur}
								onChange={formProperties.handleChange}
								className={getInputClass(formProperties.touched, formProperties.errors, "name")}
							/>
							{formProperties.touched.name && formProperties.errors.name && (
								<FormText color="danger">{formProperties.errors.name}</FormText>
							)}
						</FormGroup>
						<RAddTags dataFromBackend={formProperties.values.tags} parentCallBack={(data) => formProperties.setFieldValue("tags", data)} />
					</RFlex>
					<FormGroup style={{ width: "48%" }}>
						<label>{tr("course_code")}</label>
						<Input
							name="code"
							type="text"
							value={formProperties.values.code}
							placeholder={tr("course_code")}
							onBlur={formProperties.handleBlur}
							onChange={formProperties.handleChange}
							className={getInputClass(formProperties.touched, formProperties.errors, "code")}
						/>
						{formProperties.touched.code && formProperties.errors.code && <FormText color="danger">{formProperties.errors.code}</FormText>}
					</FormGroup>

					<FormGroup style={{ width: "48%" }}>
						<RFlex className="items-center">
							<label>{tr("credit")}</label>
							<RTooltip
								trigger={<i className={iconsFa6.questionCircleReq + " cursor-pointer"} />}
								tooltipText={"Measure of a course's workload and value"}
							/>
						</RFlex>
						<Input
							name="credit"
							type="number"
							min={0}
							value={formProperties.values.credit}
							placeholder={tr("credit")}
							onBlur={formProperties.handleBlur}
							onChange={formProperties.handleChange}
							className={getInputClass(formProperties.touched, formProperties.errors, "credit")}
						/>
						{formProperties.touched.credit && formProperties.errors.credit && (
							<FormText color="danger">{formProperties.errors.credit}</FormText>
						)}
					</FormGroup>
					<FormGroup style={{ width: "48%" }}>
						<label>{tr("hours")}</label>
						<Input
							name="hour"
							type="number"
							min={0}
							value={formProperties.values.hour}
							placeholder={tr("hours")}
							onBlur={formProperties.handleBlur}
							onChange={formProperties.handleChange}
							className={getInputClass(formProperties.touched, formProperties.errors, "hour")}
						/>
						{formProperties.touched.hour && formProperties.errors.hour && <FormText color="danger">{formProperties.errors.hour}</FormText>}
					</FormGroup>

					<FormGroup style={{ width: "48%" }}>
						<label>{tr("semester_length")}</label>
						<Input
							name="semester_length"
							value={formProperties.values.semester_length}
							placeholder={tr("semester_length")}
							onBlur={formProperties.handleBlur}
							onChange={formProperties.handleChange}
						/>
					</FormGroup>
					<FormGroup style={{ width: "48%" }}>
						<label>{tr("max_class_size")}</label>
						<Input
							name="max_class_size"
							type="number"
							min={0}
							value={formProperties.values.max_class_size}
							placeholder={tr("max_class_size")}
							onBlur={formProperties.handleBlur}
							onChange={formProperties.handleChange}
						/>
					</FormGroup>
					<FormGroup style={{ width: "48%" }}>
						<label>{tr("methods_of_instruction")}</label>
						<Input
							name="methods_of_instruction"
							value={formProperties.values.methods_of_instruction}
							placeholder={tr("methods_of_instruction")}
							onBlur={formProperties.handleBlur}
							onChange={formProperties.handleChange}
						/>
					</FormGroup>
					<FormGroup style={{ width: "48%" }}>
						<label>{tr("course_designation")}</label>
						<Input
							name="course_designation"
							value={formProperties.values.course_designation}
							placeholder={tr("course_designation")}
							onBlur={formProperties.handleBlur}
							onChange={formProperties.handleChange}
						/>
					</FormGroup>

					<FormGroup style={{ width: "48%" }}>
						<label>{tr("industry_designation")}</label>
						<Input
							name="industry_designation"
							value={formProperties.values.industry_designation}
							placeholder={tr("industry_designation")}
							onBlur={formProperties.handleBlur}
							onChange={formProperties.handleChange}
						/>
					</FormGroup>
					<FormGroup style={{ width: "48%" }}>
						<label>{tr("typically_offered")}</label>
						<Input
							name="typically_offered"
							value={formProperties.values.typically_offered}
							placeholder={tr("typically_offered")}
							onBlur={formProperties.handleBlur}
							onChange={formProperties.handleChange}
						/>
					</FormGroup>

					<FormGroup style={{ width: "100%" }}>
						<label>{tr("department")}</label>
						<RSelect
							option={data.courseDepartments?.map((department) => {
								return { label: department.name, value: department.id };
							})}
							closeMenuOnSelect={true}
							placeholder={tr`choose`}
							onChange={(event) => formProperties.setFieldValue("department", event)}
							value={formProperties.values.department}
						/>
					</FormGroup>

					<FormGroup style={{ width: "100%" }}>
						<label>{tr("description")}</label>
						<Input
							name="description"
							type="textarea"
							row={5}
							value={formProperties.values.description}
							placeholder={tr("description")}
							onBlur={formProperties.handleBlur}
							onChange={formProperties.handleChange}
							className={getInputClass(formProperties.touched, formProperties.errors, "description")}
						/>
						{formProperties.touched.description && formProperties.errors.description && (
							<FormText color="danger">{formProperties.errors.description}</FormText>
						)}
					</FormGroup>

					<FormGroup style={{ width: "100%" }}>
						<RFlex className="items-center">
							<label>{tr("course_thumbnail")}</label>
							<RUploader
								parentCallback={(event) => {
									formProperties.setFieldValue("image", event);
								}}
								singleFile={true}
								theme="button"
								formData={true}
								value={formProperties.values.image}
								buttonIcon={iconsFa6.paperclip}
							/>
						</RFlex>
					</FormGroup>
				</RFlex>

				{/* actions */}
				<RFlex>
					<RButton
						type="submit"
						text={data?.masterCourseId ? tr`save` : tr`create`}
						color="primary"
						loading={loading.createCourseLoading}
						disabled={loading.createCourseLoading}
					/>
					<RButton
						text={tr`cancel`}
						color="link"
						className="text-themePrimary"
						onClick={() => {
							formProperties.resetForm();
							handlers.handleCloseCreateCourse(null);
						}}
					/>
				</RFlex>
			</RFlex>
		</form>
	);
};

export default RCreateCourseCatalog;
