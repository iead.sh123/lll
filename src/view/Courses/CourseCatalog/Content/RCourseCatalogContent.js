import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import Loader from "utils/Loader";
import RCourseCatalogTable from "./RCourseCatalogTable";
import RCourseCatalogCard from "./RCourseCatalogCard";

const RCourseCatalogContent = ({ data, handlers, loading }) => {
	if (loading.masterCoursesLoading) return <Loader />;
	return (
		<RFlex>
			{data.values.switchMode ? (
				<RCourseCatalogCard data={{ data }} handlers={{ handlers }} loading={{ loading }} />
			) : (
				<RCourseCatalogTable data={{ data }} handlers={{ handlers }} loading={{ loading }} />
			)}
		</RFlex>
	);
};

export default RCourseCatalogContent;
