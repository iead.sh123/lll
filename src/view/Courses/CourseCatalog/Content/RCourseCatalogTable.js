import React from "react";
import CourseImage from "assets/img/EmptyData/course.png";
import RDropdown from "components/RComponents/RDropdown/RDropdown";
import iconsFa6 from "variables/iconsFa6";
import RLister from "components/Global/RComs/RLister";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { useHistory, useParams } from "react-router-dom/cjs/react-router-dom.min";
import { genericPath } from "engine/config";
import { dangerColor } from "config/constants";
import { baseURL } from "engine/config";
import { successColor } from "config/constants";

const RCourseCatalogTable = ({ data, handlers, loading }) => {
	const { departmentId } = useParams();
	const history = useHistory();

	const _recordCourses1 = {
		data: data.data.values?.masterCourses?.data,
		columns: [
			{
				accessorKey: "name",
				renderHeader: () => <span>{tr`course_name`}</span>,
				renderCell: ({ row }) => (
					<span
						className="hover:text-themePrimary hover:underline cursor-pointer"
						onClick={() =>
							history.push(
								row.original.is_master
									? `${baseURL}/${genericPath}/course-catalog/master-course/${row.original.id}/syllabus`
									: `${baseURL}/${genericPath}/course-catalog/${row.original.id}/syllabus`
							)
						}
					>
						{row.original.name}
					</span>
				),
			},

			{
				accessorKey: "code",
				renderHeader: () => <span>{tr`course_code`}</span>,
				renderCell: ({ row }) => <span>{row.original.code}</span>,
			},
			{
				accessorKey: "credit",
				renderHeader: () => <span>{tr`credit`}</span>,
				renderCell: ({ row }) => <span>{row.original.credit}</span>,
			},
			!departmentId
				? {
						accessorKey: "department_name",
						renderHeader: () => <span>{tr`department_name`}</span>,
						renderCell: ({ row }) => <span>{row.original.department_name ? row.original.department_name : "No Department"}</span>,
				  }
				: null,

			{
				accessorKey: "is_published",
				renderHeader: (info) => (
					<RFlex className="items-center">
						<span>{tr`status`}</span>
						<RDropdown
							actions={[
								{
									name: "all",
									onClick: () => {
										handlers.handlers.setFieldValue("groupingOfSearchFields.is_published", "");
									},
								},
								{
									name: "publish",
									onClick: () => {
										handlers.handlers.setFieldValue("groupingOfSearchFields.is_published", 1);
									},
								},
								{
									name: "draft",
									onClick: () => {
										handlers.handlers.setFieldValue("groupingOfSearchFields.is_published", "0");
									},
								},
							]}
							TriggerComponent={
								<button>
									<i className={iconsFa6.filter + " text-xs"} aria-hidden="true" />
								</button>
							}
						/>
					</RFlex>
				),
				renderCell: ({ row }) => (
					<span>
						{loading.loading.changeCourseStatusLoading && data.data.changeCourseStatusMutate.variables.masterCourseId == row.original.id ? (
							<i className={iconsFa6.spinner} alt="spinner" />
						) : (
							<span
								className={"cursor-pointer"}
								style={{ color: row.original.is_published ? successColor : dangerColor }}
								onClick={() =>
									handlers.handlers.handleChangeStatus({ masterCourseId: row.original.id, status: !row.original.is_published })
								}
							>
								{row.original.is_published ? tr`active` : tr`draft`}
							</span>
						)}
					</span>
				),
			},
		].filter((detail) => detail !== null),
		actions: [
			{
				icon: iconsFa6.pen,
				actionIconClass: "text-themeBoldGrey hover:text-themePrimary",
				onClick: ({ row }) => {
					handlers.handlers.handleOpenCreateCourse(row?.original?.id);
				},
			},
			{
				icon: iconsFa6.delete,
				actionIconClass: "text-themeDanger",

				// loading: loading.loading.deleteMasterCourseLoading,
				// disabled: loading.loading.deleteMasterCourseLoading,
				confirmAction: ({ row }) => {
					handlers.handlers.handleRemoveCourse(row?.original?.id);
				},
				inDialog: true,
				dialogTitle: ({ row }) => `${tr`are_you_sure_you_want_to_delete`} ${row?.original?.name}`,
				dialogDescription: ({ row }) => `${tr`this_action_cannot_be_undone`}`,
				headerItemsPosition: "items-start",
			},
		],
	};
	return (
		<div className="w-[100%]">
			<RLister
				Records={_recordCourses1}
				convertRecordsShape={false}
				info={data.data.info}
				withPagination={true}
				handleChangePage={(event) => handlers.handlers.setFieldValue("groupingOfSearchFields.page", event)}
				page={data.data.values.groupingOfSearchFields.page}
				activeScroll={false}
				containerClassName="max-h-[65vh]"
				Image={CourseImage}
				line1={tr`not_course_yet`}
			/>
		</div>
	);
};

export default RCourseCatalogTable;
