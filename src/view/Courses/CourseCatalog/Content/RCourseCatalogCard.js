import moment from "moment";
import React from "react";
import { baseURL, genericPath } from "engine/config";
import RQPaginator from "components/Global/RComs/RQPaginator/RQPaginator";
import CourseImage from "assets/img/EmptyData/course.png";
import REmptyData from "components/RComponents/REmptyData";
import iconsFa6 from "variables/iconsFa6";
import RNewCard from "components/RComponents/RNewCard";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RCourseCatalogCard = ({ data, handlers, loading }) => {
	const generateRecordObject = (dataCourses) => {
		return dataCourses?.map((item, index) => ({
			id: item.id,
			departmentName: item.department_name,
			description: item.description,
			image: item.image_url,
			name: item.name,
			published: item.is_published,
			startedAt: moment(item.created_at).format("YYYY-MM-DD"),
			actions: [
				{
					name: item.is_published ? tr`unpublish` : tr`publish`,
					icon: item.is_published ? iconsFa6.pen : iconsFa6.check,
					onClick: (event) => {
						// event.stopPropagation();
						handlers.handlers.handleChangeStatus({ masterCourseId: item.id, status: !item.is_published });
					},
				},
				{
					name: tr`edit`,
					icon: iconsFa6.edit,
					onClick: (event) => {
						handlers.handlers.handleOpenCreateCourse(item.id);
					},
				},
				{
					name: tr`delete`,
					icon: iconsFa6.delete,
					actionTextStyle: "text-themeDanger",
					actionIconClass: "text-themeDanger",
					onClick: (event) => {
						handlers.handlers.handleOpenDialogToDeleteCourse(item);
					},
				},
			],
			link: item.is_master
				? `${baseURL}/${genericPath}/course-catalog/master-course/${item.id}/syllabus`
				: `${baseURL}/${genericPath}/course-catalog/${courseId}/syllabus`,
		}));
	};

	const _recordCourses = generateRecordObject(data.data.values?.masterCourses?.data);

	return (
		<RFlex className="flex-wrap gap-x-[1.3%] gap-y-[15px] w-[100%]">
			{_recordCourses && _recordCourses?.length > 0 ? (
				_recordCourses.map((course) => <RNewCard cardData={course} key={course.id} />)
			) : (
				<RFlex className="justify-center align-items-center w-[100%]">
					<REmptyData Image={CourseImage} line1={tr`not_course_yet`} />
				</RFlex>
			)}

			{data.data.values?.masterCourses?.paginate && (
				<RQPaginator
					info={data.data.info}
					handleChangePage={(event) => handlers.handlers.setFieldValue("groupingOfSearchFields.page", event)}
					page={data.data.values.groupingOfSearchFields.page}
				/>
			)}
		</RFlex>
	);
};

export default RCourseCatalogCard;
