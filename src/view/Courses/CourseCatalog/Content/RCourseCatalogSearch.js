import React from "react";
import RSearchInput from "components/RComponents/RSearchInput";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Button } from "ShadCnComponents/ui/button";

const RCourseCatalogSearch = ({ data, handlers, loading }) => {
	return (
		<RFlex className="items-center">
			<Button
				onClick={() => {
					handlers.handleOpenCreateCourse();
				}}
			>{tr`new_course`}</Button>
			<RSearchInput
				searchLoading={loading?.masterCoursesLoading}
				searchData={data?.values?.name}
				handleSearch={(emptyData) => handlers.handleSearch(emptyData)}
				setSearchData={(value) => handlers.setFieldValue("name", value)}
				handleChangeSearch={(value) => handlers.setFieldValue("name", value)}
			/>
		</RFlex>
	);
};

export default RCourseCatalogSearch;
