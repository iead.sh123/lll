import React, { useEffect, useState, useRef } from "react";
import { primaryColor, dangerColor } from "config/constants";
import { useHistory, useParams } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import RNewInput from "components/RComponents/RNewInput/RNewInput";
import iconsFa6 from "variables/iconsFa6";
import styles from "../../Courses.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RBill from "components/Global/RComs/RBill";
import tr from "components/Global/RComs/RTranslator";

const RCourseDepartmentsList = ({
	inputValue,
	courseDepartments,
	level,
	handleAddDepartment,
	handleChange,
	addCategory,
	loading,
	handleRemoveDepartment,
}) => {
	const history = useHistory();
	const inputRef = useRef(null);
	const { departmentId } = useParams();

	const [isHovered, setIsHovered] = useState(null);
	const [isEdited, setIsEdited] = useState(null);

	// - - - - - - - - Focus the input field when the button is clicked - - - - - - - -
	const handleFocusOnInput = () => {
		if (inputRef.current !== null) {
			inputRef.current && inputRef.current.focus();
		}
	};

	useEffect(() => {
		if (isEdited?.length > 4) {
			if (inputRef?.current !== null) {
				inputRef?.current && inputRef?.current?.focus();
			}
		}
	}, [isEdited]);

	const actionsOnHover = (id) => {
		const isOpen = "hover" + id;
		if (isOpen == isHovered) {
			setIsHovered("hover");
		} else {
			setIsHovered("hover" + id);
		}
	};

	const editDepartment = (id) => {
		const isEdit = "edit" + id;
		if (isEdit == isEdited) {
			setIsEdited("edit");
		} else {
			setIsEdited("edit" + id);
		}
	};

	return (
		<RFlex className="flex-col h-[88vh] overflow-auto no-scrollbar">
			{courseDepartments && courseDepartments.length > 0 ? (
				courseDepartments.map((courseDepartment) => {
					return (
						<RFlex
							className="gap-2"
							key={courseDepartment.id}
							onMouseEnter={() => actionsOnHover(courseDepartment.id + level)}
							onMouseLeave={() => actionsOnHover(courseDepartment.id + level)}
						>
							{/* Input And Text (Edit Show Element) */}
							{isEdited == "edit" + (courseDepartment.id + level) ? (
								<div className="position-relative w-[95%]">
									<RNewInput
										autoFocus
										name="name"
										type="text"
										placeholder={tr("department_name")}
										defaultValue={courseDepartment.name}
										onChange={(event) => handleChange({ id: courseDepartment?.id, value: event.target.value })}
										onBlur={() => {
											handleAddDepartment(courseDepartment?.id);
											handleChange({ id: null, value: "" });
											setIsEdited(null);
										}}
										onKeyDown={(event) => {
											if (event.key == "Enter") {
												handleAddDepartment(courseDepartment?.id);
												handleChange({ id: null, value: "" });
												setIsEdited(null);
											}
										}}
									/>

									{loading.addEditDepartmentLoading && (
										<div className="position-absolute top-[6px] right-2">
											<i aria-hidden="true" className={`${iconsFa6.spinner}`} alt="spinner" />
										</div>
									)}
								</div>
							) : (
								<div
									className="flex gap-2 cursor-pointer mb-0 pb-0  "
									onClick={() => {
										history.push(`${baseURL}/${genericPath}/courses-catalog/department/${courseDepartment.id}`);
									}}
									style={{
										color: courseDepartment.id == departmentId ? primaryColor : "black",
										fontWeight: courseDepartment.id == departmentId ? "bold" : "normal",
									}}
								>
									{isHovered === "hover" + (courseDepartment?.id + level) && !isEdited ? (
										<span>
											{courseDepartment.name?.length > 18 ? courseDepartment.name.substring(0, 18) + ".." : courseDepartment.name}
										</span>
									) : (
										<span>{courseDepartment.name}</span>
									)}
									<RBill count={courseDepartment?.count ?? 0} fontColor="#668AD7" color="#F3F3F3" />
								</div>
							)}

							{/* Actions On Hover */}
							{isHovered === "hover" + (courseDepartment?.id + level) && !isEdited && (
								<React.Fragment>
									<i
										className={`fa fa-pen ${styles.category_icon}`}
										aria-hidden="true"
										style={{ color: primaryColor }}
										onClick={() => {
											editDepartment(courseDepartment?.id + level);
											handleChange({ id: courseDepartment?.id, value: courseDepartment?.name });
											handleFocusOnInput();
										}}
									/>
									<div className={"position-relative bottom-[6px]"}>
										<i
											className={`${loading?.deleteDepartmentLoading ? iconsFa6.spinner : iconsFa6.delete} ${styles.category_icon}`}
											aria-hidden="true"
											style={{ color: dangerColor }}
											onClick={() => handleRemoveDepartment(courseDepartment?.id)}
										/>
									</div>
								</React.Fragment>
							)}
						</RFlex>
					);
				})
			) : (
				<span className={"text-themeBoldGrey text-sm"}> {tr`no_department_yet`}</span>
			)}

			{/* - - - - - - - - Add department  - - - - - - - - */}
			{addCategory && (
				<div className="position-relative w-[95%]">
					<RNewInput
						autoFocus
						name="name"
						type="text"
						placeholder={tr("department_name")}
						value={inputValue}
						onChange={(event) => handleChange({ id: null, value: event.target.value })}
						onBlur={() => {
							handleAddDepartment();
						}}
						onKeyDown={(event) => {
							if (event.key == "Enter") handleAddDepartment();
						}}
					/>
					{loading.addEditDepartmentLoading && (
						<div className="position-absolute top-[6px] right-2">
							<i aria-hidden="true" className={`${iconsFa6.spinner}`} alt="spinner" />
						</div>
					)}
				</div>
			)}
		</RFlex>
	);
};

export default RCourseDepartmentsList;
