import React from "react";
import RCourseDepartmentsList from "./RCourseDepartmentsList";
import iconsFa6 from "variables/iconsFa6";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RBill from "components/Global/RComs/RBill";
import tr from "components/Global/RComs/RTranslator";
import { useParams, useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { baseURL, genericPath } from "engine/config";

const RCourseDepartments = ({ data, handlers, loading }) => {
	const { departmentId } = useParams();
	const history = useHistory();

	const colors = {
		textColor: "uncategorized" == departmentId ? "text-themePrimary" : "text-themeBlack",
		textFontWeight: "uncategorized" == departmentId ? "font-bold" : "font-normal",
	};
	const allTextColors = {
		textColor: "uncategorized" !== departmentId && !departmentId ? "text-themePrimary" : "text-themeBlack",
		textFontWeight: "uncategorized" !== departmentId && !departmentId ? "font-bold" : "font-normal",
	};

	if (loading.courseDepartmentsLoading)
		return (
			<RFlex className="flex-col h-[88vh] w-[15%] border-r-[1px] border-themeStroke">
				<Loader />
			</RFlex>
		);
	return (
		<RFlex className="flex-col h-[88vh] w-[15%] border-r-[1px] border-themeStroke gap-[15px]">
			<RFlex className="align-items-center cursor-pointer text-themePrimary gap-2" onClick={() => handlers.handleOpenAddDepartment()}>
				<i className={iconsFa6.plus} alt="plus" />
				<p className={`m-0 text-xs`}>{tr`new_department`}</p>
			</RFlex>

			<RFlex className="border-b-[1px] border-themeStroke gap-2">
				<div className={`${allTextColors.textColor} ${allTextColors.textFontWeight} flex gap-2 mb-2`}>
					<span
						className={`cursor-pointer`}
						onClick={() => {
							history.push(`${baseURL}/${genericPath}/courses-catalog/department`);
						}}
					>{tr`all`}</span>
					<RBill count={data.values.allCourseCount} color="#F3F3F3" fontColor="#668AD7" />
				</div>
			</RFlex>

			<RCourseDepartmentsList
				unCategorizedCount={data.values.nonDepartmentCount}
				courseDepartments={data.values.courseDepartments}
				addCategory={data.values.addDepartment}
				level={data.values.level + 1}
				inputValue={data.values.department.name}
				loading={{ addEditDepartmentLoading: loading.addEditDepartmentLoading, deleteDepartmentLoading: loading.deleteDepartmentLoading }}
				handleAddDepartment={handlers.handleAddDepartment}
				handleCloseAddCategory={handlers.handleCloseAddCategory}
				handleChange={handlers.handleChangeDepartment}
				handleRemoveDepartment={handlers.handleRemoveDepartment}
			/>

			{/* - - - - - - - - No department courses - - - - - - - - */}
			<div
				className={`${colors.textColor} ${colors.textFontWeight} gap-2 flex items-center border-t-[1px] border-themeStroke pt-[14px] px-[0px]`}
				onClick={() => {
					history.push(`${baseURL}/${genericPath}/courses-catalog/department/uncategorized`);
				}}
			>
				<span className={`cursor-pointer`}>{tr`no_department_courses`}</span>
				<RBill count={data.values.nonDepartmentCount ?? 0} color="#F3F3F3" fontColor="#668AD7" />
			</div>
		</RFlex>
	);
};

export default RCourseDepartments;
