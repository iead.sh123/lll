import React from "react";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Button } from "ShadCnComponents/ui/button";
import iconsFa6 from "variables/iconsFa6";
const RDiscussionHeader = ({ data, handlers }) => {
	return (
		<RFlex className="align-items-center">
			<Button
				variant="defaultWithoutShadow"
				onClick={() => {
					handlers.handleOpenCreateDiscussion();
				}}
			>
				<i className={iconsFa6.plus} style={{ marginRight: "5px" }} />
				{tr`new_discussion`}
			</Button>
			<RSearchHeader
				searchLoading={false}
				searchData={data?.values?.title}
				handleSearch={(emptyData) => handlers.handleSearch(emptyData)}
				setSearchData={(value) => handlers.setFieldValue("title", value)}
				handleChangeSearch={(value) => handlers.setFieldValue("title", value)}
			/>
		</RFlex>
	);
};

export default RDiscussionHeader;
