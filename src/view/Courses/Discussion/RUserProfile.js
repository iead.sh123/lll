import React from "react";
import UserAvatar from "assets/img/new/svg/user_avatar.svg";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { Services } from "engine/services";

const RUserProfile = ({ userName, userType, img, imgStyle, className, flexStyleProps, nameStyle, userTypeStyle, onClick }) => {
	const defaultImage = UserAvatar;
	return (
		<RFlex styleProps={{ ...flexStyleProps }} className={`align-items-center ${className}`} onClick={() => onClick && onClick()}>
			<img
				src={img ? Services.storage.file + img : defaultImage}
				style={{ borderRadius: "100%", width: "30px", height: "30px", ...imgStyle, objectFit: "cover" }}
			/>
			<div className="flex flex-col">
				<span style={{ ...nameStyle }}>{userName}</span>
				<span style={{ ...userTypeStyle }}>{userType}</span>
			</div>
		</RFlex>
	);
};

export default RUserProfile;
