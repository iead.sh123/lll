import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RUserProfile from "./RUserProfile";
import styles from "./Discussion.module.scss";
import iconsFa6 from "variables/iconsFa6";
import ReactionIcons from "./ReactionIcons";

const CommentAndReplay = ({ userName, userType, discussionContent, likeCounts, replayCounts, isReplay, time }) => {
	return (
		<div className={isReplay ? `${styles.replay_align}` : `${styles.comment_align}`}>
			<RFlex styleProps={{ marginTop: "10px", justifyContent: "space-between" }}>
				<RUserProfile
					userName={userName}
					userType={userType}
					nameStyle={{ fontSize: "12px", fontWeight: "bold" }}
					userTypeStyle={{ fontSize: "10px" }}
					imgStyle={{ width: "40px", height: "40px" }}
				/>
				<span className="text-themeLight" style={{ fontSize: "12px" }}>{time}</span>
			</RFlex>
			<RFlex style={{ flexDirection: "column", marginTop: "10px" }}>
				<article>{discussionContent}</article>
			</RFlex>
			<RFlex styleProps={{ gap: "20px", marginTop: "10px" }}>
				<ReactionIcons iconName={iconsFa6.like} count={likeCounts} onClick={() => console.log("like", likeCounts)} />
				<ReactionIcons iconName={iconsFa6.replay} count={replayCounts} onClick={() => console.log("replay", replayCounts)} />
			</RFlex>
		</div>
	);
};

export default CommentAndReplay;
