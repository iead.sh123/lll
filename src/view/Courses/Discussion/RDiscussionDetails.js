import React from "react";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import Post from "./Post";
import CommentAndReplay from "./CommentAndReplay";
import { FormGroup, Input } from "reactstrap";
import iconsFa6 from "variables/iconsFa6";
import { RightBar } from "./RightBar";
import { Button } from "ShadCnComponents/ui/button";
import styles from "./Discussion.module.scss";
import tr from "components/Global/RComs/RTranslator";
import RUploader from "components/RComponents/RUploader";

const RDiscussionDetails = ({ data, handlers }) => {
	console.log(data);
	const userName = "author name";
	const userType = "user type";
	const discussionTitle = "This is the title of discussion";
	const discussionContent =
		"Lorem ipsum dolor sit amet, consectetur ajdipiscing elit, sed do eiusmod iempor incididunt ut laborare et dolore magna aliqua. Ut enim ad minim veniam empor incididunt ut laborare Lorem ipsum dolor sit amet, consectetur ajdipiscing elit, sed do eiusmod iempor incididunt ut laborare et dolore magna aliqua. Ut enim ad minim veniam empor incididunt ut laborareLorem ipsum dolor sit amet, consectetur ajdipiscing elit, sed do eiusmod iempor incididunt ut laborare et dolore magna aliqua. Ut enim ad minim veniam empor incididunt ut laborare";
	const likeCounts = 60;
	const commentCounts = 23;
	const replayCounts = 1;
	const postTime = "Today, 8 hour ago";
	const commentTime = "Today, 5 hour ago";
	const replayTime = "Today, 2 hour ago";
	
	const normalOptions = [
		{
			title: "Name",
			value: "Discussion Title",
		},
		{
			title: "Post First",
			value: "On",
		},
		{
			title: "Max Comments",
			value: "5",
		},
		{
			title: "Max Replies",
			value: "2",
		},
		{
			title: "Comments",
			value: "Private Comments",
		},
		{
			title: "Allow Comments",
			value: "Allowed",
		},
		{
			title: "Required Comments",
			value: "On",
		},
	];

	return (
		<>
			<RFlex className="align-items-center">
				<RSearchHeader
					searchLoading={false}
					searchData={data?.values?.title}
					handleSearch={(emptyData) => handlers.handleSearch(emptyData)}
					setSearchData={(value) => handlers.setFieldValue("title", value)}
					handleChangeSearch={(value) => handlers.setFieldValue("title", value)}
				/>
			</RFlex> 
			<RFlex className="">
				<div className="basis-full md:basis-3/4 lg:basis-[70%]">
					<Post
						userName={userName}
						userType={userType}
						discussionTitle={discussionTitle}
						discussionContent={discussionContent}
						likeCounts={likeCounts}
						commentCounts={commentCounts}
						time={postTime}
					/>

					<FormGroup style={{ marginTop: "10px" }}>
						<div style={{ position: "relative" }}>
							<Input name="text" required type="textarea" placeholder={tr`write a comment`} />
							<div className={styles.file_button}>
								<RUploader singleFile={true} theme="button" formData={true} buttonIcon={iconsFa6.paperclip} />
							</div>
							<Button variant="link" className={styles.submit_button} onClick={() => console.log("Submit clicked")}>
								<i className={iconsFa6.send} style={{ color: "themePrimary", fontSize: "15px" }} />
							</Button>
						</div>
					</FormGroup>

					<CommentAndReplay
						userName={userName}
						userType={userType}
						discussionTitle={discussionTitle}
						discussionContent={discussionContent}
						likeCounts={likeCounts}
						replayCounts={replayCounts}
						isReplay={false}
						time={commentTime}
					/>

					<CommentAndReplay
						userName={userName}
						userType={userType}
						discussionTitle={discussionTitle}
						discussionContent={discussionContent}
						likeCounts={likeCounts}
						replayCounts={replayCounts}
						isReplay={true}
						time={replayTime}
					/>
				</div>
				<div className="basis-full md:basis-1/4 lg:basis-[30%]">
					<RightBar normalOptions={normalOptions} />
				</div>
			</RFlex>
		</>
	);
};

export default RDiscussionDetails;
