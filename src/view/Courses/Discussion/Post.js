import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RUserProfile from "./RUserProfile";
import styles from "./Discussion.module.scss";
import iconsFa6 from "variables/iconsFa6";
import ReactionIcons from "./ReactionIcons";
import RDropdown from "components/RComponents/RDropdown/RDropdown";
import RAlertDialog from "components/RComponents/RAlertDialog";

const Post = ({ userName, userType, discussionTitle, discussionContent, likeCounts, commentCounts, time }) => {
	const actions = [
		{
			name: "Edit",
			onClick: () => console.log("edit"),
			component: (
				<button type="button" className="flex items-center gap-2 text-themeBoldGrey">
					<i className={iconsFa6.pencil} />
					<span>Edit</span>
				</button>
			),
		},
		{
			name: "Delete",
			onClick: () => console.log("delete"),
			component: (
				<RAlertDialog
				component={
					<button
						type="button"
						className="flex items-center gap-2 text-themeDanger"
						onClick={(e) => e.stopPropagation()}
					>
						<i className={iconsFa6.delete} />
						<span>Delete</span>
					</button>
				}
				headerItemsPosition={"items-start"}
				confirmAction={() => console.log("confirm delete")}
			/>
			),
		},
	];
	
	return (
		<div style={{ marginLeft: "10px" }}>
			<RFlex styleProps={{ marginTop: "10px", justifyContent: "space-between" }}>
				<RUserProfile
					userName={userName}
					userType={userType}
					nameStyle={{ fontWeight: "bold" }}
					userTypeStyle={{ fontSize: "12px" }}
					imgStyle={{ width: "40px", height: "40px" }}
				/>
				<div className="flex items-end flex-col">
					<RDropdown
						actions={actions}
						TriggerComponent={
							<button>
								<i className={iconsFa6.horizontalOptions} />
							</button>
						}
					/>
					<span className="text-themeLight" style={{ fontSize: "12px" }}>{time}</span>
				</div>
			</RFlex>
			<RFlex styleProps={{ flexDirection: "column", marginTop: "20px" }}>
				<h3 className="font-bold">{discussionTitle}</h3>
				<article>{discussionContent}</article>
			</RFlex>
			<div className={styles.divider}></div>
			<RFlex styleProps={{ gap: "20px", marginTop: "10px" }}>
				<ReactionIcons iconName={iconsFa6.like} count={likeCounts} onClick={() => console.log("like", likeCounts)} />
				<ReactionIcons iconName={iconsFa6.commentsRegular} count={commentCounts} onClick={() => console.log("comment", commentCounts)} />
			</RFlex>
		</div>
	);
};

export default Post;
