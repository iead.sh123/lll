import React, { useState } from "react";
import iconsFa6 from "variables/iconsFa6";
import RTable from "components/RComponents/RTable/RTable";
import RLister from "components/Global/RComs/RLister";
import tr from "components/Global/RComs/RTranslator";
import { genericPath } from "engine/config";
import { useHistory, useParams } from "react-router-dom/cjs/react-router-dom.min";
import { baseURL } from "engine/config";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RCheckDropdown from "components/RComponents/RCheckDropdown/RCheckDropdown";
import { Button } from "ShadCnComponents/ui/button";

const RDiscussionTable = ({ data, handlers, loading }) => {
	const param = useParams();

	const [availabililtyStateFilter, setAvailabililtyStateFilter] = useState({
		1: {
			checked: false,
			name: "Available",
			onCheckedChange: (checkedFlag) => {
				console.log(checkedFlag);
				// const newFilter = checkedFlag ? { ...allMembersFilters } : { ...allMembersFilters, is_active: true };
				// setAllMembersFilters(newFilter);
			},
		},
		2: {
			checked: false,
			name: "Future",
			onCheckedChange: (checkedFlag) => {
				console.log(checkedFlag);
				// const newFilter = checkedFlag ? { ...allMembersFilters } : { ...allMembersFilters, is_active: true };
				// setAllMembersFilters(newFilter);
			},
		},
		3: {
			checked: false,
			name: "Closed",
			onCheckedChange: (checkedFlag) => {
				console.log(checkedFlag);
				// const newFilter = checkedFlag ? { ...allMembersFilters } : { ...allMembersFilters, is_active: true };
				// setAllMembersFilters(newFilter);
			},
		},
	});

	const [gradedFilter, setGradedFilter] = useState({
		1: {
			name: "Graded",
		},
		2: {
			name: "Not Graded",
		},
	});

	const [statusFilter, setStatusFilter] = useState({
		1: {
			name: "Active",
		},
		2: {
			name: "Inactive",
		},
	});

	const history = useHistory();

	const discussionNameComponent = ({ row }) => {
		console.log(row);
		// history.push(`${baseURL}/${genericPath}/course-catalog/master-course/${param.courseId}/discussion/${discussionId}`)
		// return (
		// 	<span
		// 		className="hover:text-themePrimary hover:underline cursor-pointer"
		// 		// onClick={() =>
		// 		// 	history.push(
		// 		// 		isMasterCourse
		// 		// 			? `${baseURL}/${genericPath}/course-catalog/master-course/${discussionId}/discussion`
		// 		// 			: `${baseURL}/${genericPath}/course-catalog/${discussionId}/discussion`
		// 		// 	)
		// 		// }
		// 		onClick={() => }
		// 	>
		// 	</span>
		// );
	};

	const columns = [
		{
			accessorKey: "id",
			renderHeader: () => <span className="min-w-max">{tr("ID")}</span>,
			renderCell: ({ row }) => <span className="text-themeBoldGrey">{row.getValue("id")}</span>,
		},
		{
			accessorKey: "title",
			renderHeader: () => <span className="min-w-max">{tr("title")}</span>,
			renderCell: ({ row }) => (
				<span
					className="text-themeBoldGrey hover:text-themePrimary hover:underline cursor-pointer"
					onClick={() =>
						history.push(`${baseURL}/${genericPath}/course-catalog/master-course/${param.courseId}/discussion/${row.getValue("id")}`)
					}
				>
					{row.getValue("title")}
				</span>
			),
		},
		{
			accessorKey: "availabililty_state",
			renderHeader: () => (
				<RFlex className="items-baseline">
					<span>{tr("availabililty_state")}</span>
					<RCheckDropdown
						TriggerComponent={
							<Button variant="ghost" className="flex flex-col gap-2 p-0 h-fit">
								<i className={`${iconsFa6.filter}`} />
							</Button>
						}
						actions={availabililtyStateFilter}
						setActions={setAvailabililtyStateFilter}
					/>
				</RFlex>
			),
			renderCell: ({ row }) => <span className="p-0 m-0 flex gap-2 items-center">{row.getValue("availabililty_state")}</span>,
		},
		{
			accessorKey: "graded",
			renderHeader: () => (
				<RFlex className="items-baseline">
					<span>{tr("graded")}</span>
					<RCheckDropdown
						TriggerComponent={
							<Button variant="ghost" className="flex flex-col gap-2 p-0 h-fit">
								<i className={`${iconsFa6.filter}`} />
							</Button>
						}
						actions={gradedFilter}
						setActions={setGradedFilter}
					/>
				</RFlex>
			),
			renderCell: ({ row }) => <span className="p-0 m-0 flex gap-2 items-center">{row.getValue("graded")}</span>,
		},
		{
			accessorKey: "status",
			renderHeader: () => (
				<RFlex className="items-baseline">
					<span>{tr("status")}</span>
					<RCheckDropdown
						TriggerComponent={
							<Button variant="ghost" className="flex flex-col gap-2 p-0 h-fit">
								<i className={`${iconsFa6.filter}`} />
							</Button>
						}
						actions={statusFilter}
						setActions={setStatusFilter}
					/>
				</RFlex>
			),
			renderCell: ({ row }) => <span className="p-0 m-0 flex gap-2 items-center">{row.getValue("status")}</span>,
		},
		{
			accessorKey: "attach_to",
			renderHeader: () => <span className="min-w-max">{tr("attach_to")}</span>,
			renderCell: ({ row }) => <span className="text-themeBoldGrey ">{row.getValue("attach_to") ?? "-"}</span>,
		},
	];

	const _recordDiscussion = { columns, data: data?.info?.data };

	if (loading.discussionsLoading) return <Loader />;

	return (
		<div className="w-[100%]">
			<RTable Records={_recordDiscussion} convertRecordsShape={false} containerClassName="overflow-visible" />
		</div>
	);
};

export default RDiscussionTable;
