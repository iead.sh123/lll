import React from "react";

const ReactionIcon = ({ iconName, count, onClick }) => {
	return (
		<button type="button" className="flex items-center gap-2" onClick={() => onClick && onClick()}>
			<i className={iconName} />
			<span>{count}</span>
		</button>
	);
};

export default ReactionIcon;
