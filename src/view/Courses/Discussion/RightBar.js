import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import styles from "./Discussion.module.scss";
import iconsFa6 from "variables/iconsFa6";
import ReactionIcons from "./ReactionIcons";
import RDropdown from "components/RComponents/RDropdown/RDropdown";

export const RightBar = ({ normalOptions }) => {
	return (
		<>
			<RFlex className="flex-col">
				{normalOptions.map((item, index) => (
					<div className="flex justify-between" key={index}>
						<span className="text-themeBoldGrey">
							{item.title}
						</span>
						<span className="font-semibold"> {item.value}</span>
					</div>
				))}
			</RFlex>
			<div className={styles.divider}></div>
		</>
	);
};
