import React, { useState } from "react";
import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { FormGroup, FormText, Input } from "reactstrap";
import { getInputClass } from "utils/getInputClass";
import RUploader from "components/RComponents/RUploader";
import iconsFa6 from "variables/iconsFa6";
import { Checkbox } from "ShadCnComponents/ui/checkbox";
import { Switch } from "ShadCnComponents/ui/switch";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";

const RCreateDiscussion = ({ formProperties, handlers, loading }) => {
	// console.log("formProperties.values.image 520025", formProperties.values.image);
	return (
		<form onSubmit={handlers.handleSubmit}>
			<RFlex styleProps={{ width: "100%", flexDirection: "column", paddingTop: "10px" }}>
				<RFlex styleProps={{ alignItems: "center" }}>
					<span>{tr`create_new_discussion`}</span>
				</RFlex>
				<RFlex className="flex-wrap justify-between gap-0">
					{/* title */}
					<FormGroup style={{ width: "100%" }}>
						<label>{tr("title")}</label>
						<Input
							name="title"
							type="text"
							value={formProperties.values.title}
							placeholder={tr("what_is_this_discussion_about")}
							onBlur={formProperties.handleBlur}
							onChange={formProperties.handleChange}
							className={getInputClass(formProperties.touched, formProperties.errors, "title")}
						/>
						{formProperties.touched.title && formProperties.errors.title && (
							<FormText color="danger">{formProperties.errors.title}</FormText>
						)}
					</FormGroup>

					{/* content */}
					<FormGroup style={{ width: "100%" }}>
						<label>{tr("content")}</label>
						<Input
							name="content"
							type="textarea"
							row={5}
							value={formProperties.values.content}
							onBlur={formProperties.handleBlur}
							onChange={formProperties.handleChange}
							className={getInputClass(formProperties.touched, formProperties.errors, "content")}
						/>
						{formProperties.touched.content && formProperties.errors.content && (
							<FormText color="danger">{formProperties.errors.content}</FormText>
						)}
					</FormGroup>

					{/* file */}
					<FormGroup style={{ width: "100%" }}>
						<RFlex className="items-center">
							<label>{tr("upload_file")}</label>
							{/* <RUploader
								parentCallback={(event) => {
									formProperties.setFieldValue("image", event);
								}}
								singleFile={true}
								theme="button"
								formData={true}
								value={formProperties.values.image}
								buttonIcon={iconsFa6.paperclip}
							/> */}

							<RFileSuite
								parentCallback={(event) => {
									console.log(event);
								}}
								singleFile={true}
								// value={[{ hash_id: hashId ? hashId : null }]}
								theme="button"
								binary={true}
							/>
						</RFlex>
					</FormGroup>

					{/* allow comments */}
					<FormGroup style={{ width: "100%" }}>
						<RFlex>
							<Checkbox
								onCheckedChange={(event) => {
									console.log(event);
									formProperties.setFieldValue("allow_comments", event);
								}}
								checked={formProperties.values.allow_comments}
							/>
							<span>{tr`allow_comments`}</span>
						</RFlex>
					</FormGroup>

					{formProperties.values.allow_comments && (
						<div className="row" style={{ width: "100%" }}>
							{/* max comments */}
							<FormGroup style={{ width: "35%" }}>
								<label>{tr("max_comments")}</label>
								<Input
									name="max_comments"
									type="number"
									value={formProperties.values.max_comments}
									onBlur={formProperties.handleBlur}
									onChange={formProperties.handleChange}
									className={getInputClass(formProperties.touched, formProperties.errors, "max_comments")}
									style={{ width: "60px" }}
								/>
								{formProperties.touched.max_comments && formProperties.errors.max_comments && (
									<FormText color="danger">{formProperties.errors.max_comments}</FormText>
								)}
							</FormGroup>

							{/* max replies */}
							<FormGroup style={{ width: "65%" }}>
								<label>{tr("max_replies")}</label>
								<Input
									name="max_replies"
									type="number"
									value={formProperties.values.max_replies}
									onBlur={formProperties.handleBlur}
									onChange={formProperties.handleChange}
									className={getInputClass(formProperties.touched, formProperties.errors, "max_replies")}
									style={{ width: "60px" }}
								/>
								{formProperties.touched.max_replies && formProperties.errors.max_replies && (
									<FormText color="danger">{formProperties.errors.max_replies}</FormText>
								)}
							</FormGroup>
						</div>
					)}

					{/* post first */}
					{formProperties.values.allow_comments && (
						<FormGroup style={{ width: "100%" }} className="mt-2">
							<RFlex>
								<Checkbox
									onCheckedChange={(event) => {
										console.log(event);
										formProperties.setFieldValue("post_first", event);
									}}
									checked={formProperties.values.post_first}
								/>
								<span>{tr`post_first`}</span>
							</RFlex>
						</FormGroup>
					)}

					{/* private comments */}
					{formProperties.values.allow_comments && (
						<FormGroup style={{ width: "100%" }} className="mt-2">
							<RFlex>
								<Checkbox
									onCheckedChange={(event) => {
										console.log(event);
										formProperties.setFieldValue("private_comments", event);
									}}
									checked={formProperties.values.private_comments}
								/>
								<span>{tr`private_comments`}</span>
							</RFlex>
						</FormGroup>
					)}

					{/* required comments */}
					{formProperties.values.allow_comments && (
						<FormGroup style={{ width: "100%" }} className="mt-2">
							<RFlex className="items-center">
								<Checkbox
									onCheckedChange={(event) => {
										console.log(event);
										formProperties.setFieldValue("required_comments", event);
									}}
									checked={formProperties.values.required_comments}
								/>
								<span>{tr`required_comments`}</span>
								{formProperties.values.allow_comments && formProperties.values.required_comments && (
									<div>
										<Input
											name="comments_number"
											type="number"
											value={formProperties.values.comments_number}
											onBlur={formProperties.handleBlur}
											onChange={formProperties.handleChange}
											className={getInputClass(formProperties.touched, formProperties.errors, "comments_number")}
											style={{ width: "60px" }}
										/>
										{formProperties.touched.comments_number && formProperties.errors.comments_number && (
											<FormText color="danger">{formProperties.errors.comments_number}</FormText>
										)}
									</div>
								)}
							</RFlex>
						</FormGroup>
					)}

					{/* published */}
					{formProperties.values.allow_comments && (
						<FormGroup style={{ width: "100%" }} className="mt-2">
							<RFlex>
								<label>{tr("published")}</label>
								<Switch
									checked={formProperties.values.published}
									onCheckedChange={(event) => {
										formProperties.setFieldValue("published", event);
									}}
									id={"published"}
								/>
							</RFlex>
						</FormGroup>
					)}
				</RFlex>

				{/* actions */}
				<RFlex>
					<RButton
						type="submit"
						text={tr`create`}
						color="primary"
						loading={loading.createDiscussionLoading}
						disabled={loading.createDiscussionLoading}
					/>
					<RButton
						text={tr`cancel`}
						color="link"
						className="text-themePrimary"
						onClick={() => {
							formProperties.resetForm();
							handlers.handleCloseCreateDiscussion(null);
						}}
					/>
				</RFlex>
			</RFlex>
		</form>
	);
};

export default RCreateDiscussion;
