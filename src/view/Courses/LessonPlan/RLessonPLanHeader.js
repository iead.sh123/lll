import React from "react";
import iconsFa6 from "variables/iconsFa6";
import tr from "components/Global/RComs/RTranslator";
import { ReloadIcon } from "@radix-ui/react-icons";
import { Button } from "ShadCnComponents/ui/button";
import RFlex from "components/Global/RComs/RFlex/RFlex";

const RLessonPLanHeader = ({ handlers, loading }) => {
	return (
		<RFlex>
			<Button className="flex gap-[5px]" variant={"outline"} onClick={() => handlers.handlePushToLessonPlanEditor()}>
				<i className={iconsFa6.pencil} alt="pencil" />
				{tr`edit`}
			</Button>

			<Button
				className="flex gap-[5px]"
				variant={"outline"}
				onClick={() => handlers.handleExportLessonPlanAsPdf()}
				disabled={loading.exportLessonPlanAsPdfLoading}
			>
				{loading.exportLessonPlanAsPdfLoading ? (
					<RFlex>
						<i className={iconsFa6.spinner} alt="spinner" />
					</RFlex>
				) : (
					<i className={iconsFa6.pdf} alt="pdf" />
				)}
				{tr`export_as_pdf`}
			</Button>
		</RFlex>
	);
};

export default RLessonPLanHeader;
