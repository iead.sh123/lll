import React from "react";
import RNewInput from "components/RComponents/RNewInput/RNewInput";
import RCkEditor from "components/Global/RComs/RCkEditor";
import RAddTags from "components/RComponents/RAddTags";
import RSelect from "components/Global/RComs/RSelect";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { ReloadIcon } from "@radix-ui/react-icons";
import { Button } from "ShadCnComponents/ui/button";
import { Label } from "ShadCnComponents/ui/label";
import iconsFa6 from "variables/iconsFa6";

const RLessonPLanEditor = ({ formProperties, loading }) => {
	return (
		<form onSubmit={formProperties.handleSubmit}>
			<RFlex className="flex-col" styleProps={{ gap: "15px" }}>
				<RFlex className="gap-2 items-end ">
					<RFlex className="flex-col gap-2">
						<Label>{tr("plan_title")}</Label>
						<RNewInput
							name="name"
							value={formProperties.values.name}
							onChange={formProperties.handleChange}
							onBlur={formProperties.handleBlur}
							placeholder={tr("name")}
							className={formProperties?.errors?.name ? "input__error" : ""}
						/>
					</RFlex>
					<RAddTags dataFromBackend={formProperties.values.tags} parentCallBack={(data) => formProperties.setFieldValue("tags", data)} />
				</RFlex>

				{/* <RFlex className="flex-col gap-0">
					<Label>{tr("attach")}</Label>
					<RSelect
						option={[{}]}
						closeMenuOnSelect={true}
						value={formProperties.values.attach}
						onChange={(e) => formProperties.setFieldValue("attach", [e.value])}
					/>
				</RFlex> */}

				<RCkEditor data={formProperties.values.description} handleChange={(data) => formProperties.setFieldValue("description", data)} />

				<RFlex className="gap-2">
					<Button disabled={loading.addOrEditLessonPLanLoading}>
						<RFlex className="gap-1">
							<div>{loading.addOrEditLessonPLanLoading && <i className={iconsFa6.spinner} />}</div>
							{tr`save`}
						</RFlex>
					</Button>
				</RFlex>
			</RFlex>
		</form>
	);
};

export default RLessonPLanEditor;
