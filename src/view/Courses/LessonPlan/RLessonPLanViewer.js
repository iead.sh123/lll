import RFlex from "components/Global/RComs/RFlex/RFlex";
import React from "react";

const RLessonPLanViewer = ({ lessonPlanData }) => {
	return (
		<RFlex className="flex-col">
			<span>{lessonPlanData?.name}</span>
			<span dangerouslySetInnerHTML={{ __html: lessonPlanData?.description }} />
		</RFlex>
	);
};

export default RLessonPLanViewer;
