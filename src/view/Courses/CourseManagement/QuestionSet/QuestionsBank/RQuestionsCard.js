import React, { useContext } from "react";
import { QuestionsBankContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionsBank";
import styles from "../QuestionSet.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { relativeDate } from "utils/HandleDate";
  import { dangerColor } from "config/constants";
import iconsFa6 from "variables/iconsFa6";

const RQuestionsCard = () => {
	const QuestionsBankData = useContext(QuestionsBankContext);

	return (
		<div className={styles.question__bank__container}>
			{QuestionsBankData.questionsBank?.map((questionBank) => (
				<section className={styles.question__bank}>
					<section className={styles.question__bank__column}>
						<section className={styles.question__bank__info}>
							<RFlex
								styleProps={{
									alignItems: "middle",
									justifyContent: "space-between",
								}}
							>
								<p style={{ fontSize: "12px" }}>
									{questionBank?.isGroup ? (
										tr`question_group` + ` (${questionBank.questions.length}) questions`
									) : (
										<div>
											<i
												className={
													QuestionsBankData?.questionTypesEnum.find((item) => item.value == questionBank.questions?.[0]?.type)?.icon
												}
											/>
											<span className="pl-2">
												{questionBank.questions?.[0]?.type == "MULTIPLE_CHOICES_SINGLE_ANSWER"
													? "seq"
													: questionBank.questions?.[0]?.type == "MULTIPLE_CHOICES_MULTIPLE_ANSWERS"
													? "mcq"
													: questionBank.questions?.[0]?.type}
											</span>
										</div>
									)}
								</p>
								{/* {QuestionsBankData.ChooseFromQuestionBank && (
									<UncontrolledDropdown direction="end">
										<DropdownToggle
											aria-haspopup={true}
											// caret
											color="default"
											data-toggle="dropdown"
											nav
										>
											<i class="fa fa-ellipsis-v" aria-hidden="true"></i>
										</DropdownToggle>
										<DropdownMenu persist aria-labelledby="navbarDropdownMenuLink" right>
											<DropdownItem key={"edit"} onClick={() => QuestionsBankData.handleOpeModalToEdit(questionBank)}>
												<RFlex
													styleProps={{
														alignItems: "center",
														justifyContent: "space-between",
													}}
												>
													<i className={"far fa-file"} aria-hidden="true" />
													<span>{tr`edit`}</span>
												</RFlex>
											</DropdownItem>
											<DropdownItem key={"delete"} onClick={() => QuestionsBankData.handleRemoveQuestionBank(questionBank.id)}>
												<RFlex
													styleProps={{
														alignItems: "center",
														justifyContent: "space-between",
													}}
												>
													<i style={{ color: dangerColor }} className={iconsFa6.delete} aria-hidden="true" />
													<span style={{ color: dangerColor }}>{tr`delete`}</span>
												</RFlex>
											</DropdownItem>
										</DropdownMenu>
									</UncontrolledDropdown>
								)} */}
								{QuestionsBankData.ChooseFromQuestionBank && (
									<i
										onClick={(event) => {
											event.stopPropagation();
											QuestionsBankData.handleRemoveQuestionBank(questionBank.id);
										}}
										style={{ color: dangerColor, cursor: "pointer" }}
										className={iconsFa6.delete}
										aria-hidden="true"
									/>
								)}
							</RFlex>

							<div
								style={{
									width: "100%",
									wordWrap: "break-word",
								}}
							>
								<h6
									onClick={(event) => {
										event.stopPropagation();
										QuestionsBankData.ChooseFromQuestionBank ? QuestionsBankData.handleOpeModalToEdit(questionBank) : {};
									}}
									style={{
										fontSize: "16px",
										cursor: QuestionsBankData.ChooseFromQuestionBank ? "pointer" : "auto",
									}}
									className="font-weight-bold"
									dangerouslySetInnerHTML={{
										__html: questionBank?.isGroup
											? questionBank?.title
												? questionBank?.title + "?"
												: ""
											: questionBank?.questions?.[0]?.text
											? questionBank?.questions?.[0]?.text.length > 30
												? questionBank?.questions?.[0]?.text.slice(0, 30) + "..."
												: questionBank?.questions?.[0]?.text
											: "",
									}}
								/>
							</div>

							<span className="text-muted">
								{tr`last_edited`} : {relativeDate(questionBank?.updated_at)}
							</span>
							<RFlex styleProps={{ alignItems: "center" }}>
								{/* <img
                  className={styles.answer__image}
                  src={Services.storage.file + questionBank?.creator?.image}
                  alt={questionBank?.creator?.image}
                  width="40px"
                  height="40px"
                />
                <h6>{questionBank?.creator?.full_name}</h6> */}
							</RFlex>
						</section>
						<section className={styles.question__bank__use}>
							<span onClick={() => QuestionsBankData.handleOpenQuestionModal(questionBank)}>{tr`view`}</span>
							{QuestionsBankData.ChooseFromQuestionBank && (
								<span onClick={() => QuestionsBankData.handleOpenModal(questionBank)}>{tr`use`}</span>
							)}
						</section>
					</section>
				</section>
			))}
		</div>
	);
};

export default RQuestionsCard;
