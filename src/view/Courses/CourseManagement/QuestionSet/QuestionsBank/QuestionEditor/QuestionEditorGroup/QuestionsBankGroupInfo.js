import React, { useContext } from "react";
import { Row, Col, FormGroup, Label, Input } from "reactstrap";
import { QuestionsBankContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionsBank";
import { Services } from "engine/services";
import { RLabel } from "components/Global/RComs/RLabel";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import RTags from "components/Global/RComs/RTags";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const QuestionsBankGroupInfo = ({ group, questionGroupId }) => {
  const QuestionsBankData = useContext(QuestionsBankContext);

  return (
    <Row className="m-3">
      <Col xs={12} md={4} className="pl-0">
        <RFlex styleProps={{ flexDirection: "column" }}>
          <RFlex>
            <Label className="p-0 m-0">{tr("name")}</Label>
            <RLabel
              value={tr(" ")}
              icon="fa-regular fa-circle-question pt-1"
              lettersToShow={0}
            />
          </RFlex>
          <FormGroup>
            <Input
              name="title"
              type="text"
              placeholder={tr`title`}
              value={group?.title}
              onChange={(event) =>
                QuestionsBankData.handleChangeQuestionsGroup({
                  questionGroupId: questionGroupId,
                  key: event.target.name,
                  value: event.target.value,
                })
              }
            />
          </FormGroup>
        </RFlex>
      </Col>
      <Col xs={12} md={4}>
        <RFlex styleProps={{ flexDirection: "column" }}>
          <RFlex>
            <Label className="p-0 m-0">{tr("tags")}</Label>
            <RLabel
              value={tr(" ")}
              icon="fa-regular fa-circle-question pt-1"
              lettersToShow={0}
            />
          </RFlex>
          <FormGroup>
            <RTags
              url={`${Services.tag_search.backend}api/v1/tag/search?prefix=`}
              getArrayFromResponse={(res) =>
                res && res.data && res.data.status ? res.data.data : null
              }
              getValueFromArrayItem={(i) => i.id}
              getLabelFromArrayItem={(i) => i.name}
              getIdFromArrayItem={(i) => i.id}
              defaultValues={group?.tags}
              onChange={(data) =>
                QuestionsBankData.handleChangeQuestionsGroup({
                  questionGroupId: questionGroupId,
                  key: "tags",
                  value: data,
                })
              }
            />
          </FormGroup>
        </RFlex>
      </Col>

      <Col xs={12}>
        <RFileSuite
          parentCallback={(values) => {
            QuestionsBankData.handleChangeQuestionsGroup({
              questionGroupId: questionGroupId,
              key: "files",
              value: values,
            });
          }}
          singleFile={true}
          binary={true}
          value={group?.files}
          fileType="image/*"
        />
      </Col>
    </Row>
  );
};

export default QuestionsBankGroupInfo;
