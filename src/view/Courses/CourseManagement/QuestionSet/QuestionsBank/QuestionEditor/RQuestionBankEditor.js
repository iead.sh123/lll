import React, { useContext } from "react";
import { QuestionsBankContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionsBank";
import EditorMultipleChoiceSingleAnswer from "./QuestionType/EditorMultipleChoiceSingleAnswer";
import EditorEssay from "./QuestionType/EditorEssay";
import EditorTrueOrFalse from "./QuestionType/EditorTrueOrFalse";
import EditorOrdering from "./QuestionType/EditorOrdering";
import EditorMultipleChoice from "./QuestionType/EditorMultipleChoice";
import QuestionsBankGroupInfo from "./QuestionEditorGroup/QuestionsBankGroupInfo";
import EditorMatch from "./QuestionType/EditorMatch";
import RQuestionTypes from "../../RQuestionTypes";
import styles from "../../QuestionSet.module.scss";
import EditorDraw from "./QuestionType/EditorDraw";
import EditorUploadFile from "./QuestionType/EditorUploadFile";
import EditorFillInBlank from "./QuestionType/EditorFillInBlank";

const RQuestionBankEditor = () => {
  const QuestionsBankData = useContext(QuestionsBankContext);

  const questionTypeEditorComponents = {
    MULTIPLE_CHOICES_MULTIPLE_ANSWERS: EditorMultipleChoice,
    MULTIPLE_CHOICES_SINGLE_ANSWER: EditorMultipleChoiceSingleAnswer,
    ESSAY: EditorEssay,
    FILL_IN: EditorFillInBlank,
    UPLOAD_FILE: EditorUploadFile,
    TRUEFALSE: EditorTrueOrFalse,
    SEQUENCING: EditorOrdering,
    DRAW: EditorDraw,
    MATCH: EditorMatch,
  };
  return (
    <div>
      {QuestionsBankData.questionSet.groups?.length > 0 &&
        QuestionsBankData.questionSet.groups.map((group, ind) => {
          return (
            <section>
              <section
                className={styles.section__group}
                key={group?.id ? group?.id : group?.fakeId}
              >
                <>
                  {group.isGroup && (
                    <>
                      <QuestionsBankGroupInfo
                        group={group}
                        questionGroupId={group?.id ? group?.id : group?.fakeId}
                      />
                      <RQuestionTypes
                        questionSetIndex={null}
                        type={"group"}
                        groupId={group?.id ? group?.id : group?.fakeId}
                        questionBank={true}
                      />
                    </>
                  )}
                  {group?.questions?.map((question, index) => {
                    const QuestionBankEditorComponent =
                      questionTypeEditorComponents[question.type];

                    if (QuestionBankEditorComponent) {
                      return (
                        <>
                          <QuestionBankEditorComponent
                            questionGroupId={
                              group?.id ? group?.id : group?.fakeId
                            }
                            question={question}
                            questionNumber={index + 1}
                            group={group}
                          />
                          {group.isGroup && (
                            <RQuestionTypes
                              questionSetIndex={index + 1}
                              type={"group"}
                              groupId={group?.id ? group?.id : group?.fakeId}
                              questionBank={true}
                            />
                          )}
                        </>
                      );
                    }

                    return null;
                  })}
                </>
              </section>
            </section>
          );
        })}
    </div>
  );
};

export default RQuestionBankEditor;
