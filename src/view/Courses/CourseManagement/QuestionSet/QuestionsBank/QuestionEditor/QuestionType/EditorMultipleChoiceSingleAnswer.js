import React, { useContext } from "react";
import { Row, Col, FormGroup, Label, Input } from "reactstrap";
import { QuestionsBankContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionsBank";
import { dangerColor, primaryColor } from "config/constants";
import ChoicesQuestions from "../../../QuestionsTypes/ChoicesQuestions/ChoicesQuestions";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";

const EditorMultipleChoiceSingleAnswer = ({ questionGroupId, question, questionNumber = 0, group }) => {
	const QuestionsBankData = useContext(QuestionsBankContext);

	return (
		<div>
			<Row className="mt-4 d-flex align-items-center">
				<Col md={8} xs={12}>
					<RFlex styleProps={{ flexDirection: "column" }}>
						<RFlex>
							<i className="fas fa-tasks pt-1" />
							<RFlex>
								{group.isGroup && (
									<Label className="p-0 m-0">
										{tr`question`} &nbsp;
										{questionNumber}
									</Label>
								)}
								<Label className="p-0 m-0">
									{group.isGroup ? `(${tr("multiple_choice_single_answer")})` : tr("multiple_choice_single_answer")}
								</Label>
							</RFlex>
						</RFlex>
						<FormGroup>
							<Input
								name="text"
								type="text"
								value={question?.text}
								onChange={(event) =>
									QuestionsBankData.handleFillDataToQuestionType({
										questionGroupId: questionGroupId,
										questionTypeId: question?.id ? question?.id : question?.fakeId,
										key: event.target.name,
										value: event.target.value,
									})
								}
							/>
						</FormGroup>
					</RFlex>
				</Col>
				<Col md={4} xs={12}>
					<RFlex
						styleProps={{
							alignItems: "center",
							justifyContent: "space-between",
						}}
					>
						<RFlex styleProps={{ alignItems: "center", paddingTop: "32px" }}>
							<RFileSuite
								parentCallback={(values) => {
									QuestionsBankData.handleFillDataToQuestionType({
										questionGroupId: questionGroupId,
										questionTypeId: question?.id ? question?.id : question?.fakeId,
										key: "files",
										value: values,
									});
								}}
								singleFile={true}
								binary={true}
								value={question?.files}
								theme="button_with_replace"
								fileType="image/*"
							/>
							{group?.isGroup && (
								<i
									className={iconsFa6.delete}
									style={{ color: dangerColor, cursor: "pointer" }}
									onClick={() =>
										QuestionsBankData.handleRemoveQuestionGroup({
											questionGroupId: questionGroupId,
											questionTypeId: question?.id ? question?.id : question?.fakeId,
											isGroup: group?.isGroup,
										})
									}
								/>
							)}
						</RFlex>
					</RFlex>
				</Col>
			</Row>
			{/* start choices */}
			<Row className="mt-4 d-flex align-items-center ">
				{question?.choices?.length > 0 &&
					question?.choices.map((choice, index) => (
						<ChoicesQuestions
							questionGroupId={questionGroupId}
							question={question}
							choice={choice}
							questionType={question.type}
							qType={"choice"}
							index={index}
							questionBank={true}
						/>
					))}
			</Row>
			{/* end choices */}
			<Row className="mb-4">
				<Col xs={12} className="mt-2">
					<RFlex
						styleProps={{
							justifyContent: "space-between",
						}}
					>
						<AppCheckbox
							onChange={(event) => {
								QuestionsBankData.handleFillDataToQuestionType({
									questionGroupId: questionGroupId,
									questionTypeId: question?.id ? question?.id : question?.fakeId,
									key: "shuffleAnswers",
									value: event.target.checked,
								});
							}}
							label={tr`shuffle_answer`}
						/>
						<div
							style={{ color: primaryColor, cursor: "pointer" }}
							onClick={() =>
								QuestionsBankData.handleAddChoicesToQuestion({
									questionGroupId: questionGroupId,
									questionTypeId: question?.id ? question?.id : question?.fakeId,
									choicesLength: question?.choices.length,
									questionType: "choice",
								})
							}
						>
							<i className="fa fa-plus pt-1 pr-2" />
							<span>{tr`add_option`}</span>
						</div>
					</RFlex>
				</Col>
			</Row>
		</div>
	);
};

export default EditorMultipleChoiceSingleAnswer;
