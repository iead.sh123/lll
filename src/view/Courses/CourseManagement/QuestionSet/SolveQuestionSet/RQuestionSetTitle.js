import React, { useContext } from "react";
import { QuestionSetSolveContext } from "logic/Courses/CourseManagement/QuestionSet/GSolveQuestionSet";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import AppRadioButton from "components/UI/AppRadioButton/AppRadioButton";
import tr from "components/Global/RComs/RTranslator";

const RQuestionSetTitle = ({ isGroup, groupInd, questionGroup }) => {
  const QuestionSetSolveData = useContext(QuestionSetSolveContext);

  return (
    <>
      {questionGroup.questions.map((question, index) => {
        return (
          <section key={question.id} className={isGroup ? "pl-4" : ""}>
            <RFlex>
              <AppRadioButton
                name={`questionGroup_${
                  isGroup ? question.id : questionGroup.id
                }`}
                disabled={true}
                checked={
                  QuestionSetSolveData.submitted == "solving"
                    ? question.isSolved
                    : question.isGraded
                }
              />
              <p
                className="pt-2 cursor-pointer"
                onClick={(e) => {
                  QuestionSetSolveData._tree(groupInd);
                }}
                style={{
                  fontWeight:
                    QuestionSetSolveData.step == groupInd ? "bolder" : "normal",
                }}
              >
                {tr`question`}&nbsp;
                {isGroup ? index + 1 : groupInd + 1}
              </p>
            </RFlex>
          </section>
        );
      })}
    </>
  );
};

export default RQuestionSetTitle;
