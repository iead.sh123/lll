import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import styles from "../../SolveQuestionSet.module.scss";
import { Services } from "engine/services";

const CorrectOrReviewOrdering = ({ question }) => {
  const studentAnswers =
    question.answer !== ""
      ? question.answer
          ?.split(",")
          ?.map((el) => (el === "" ? null : Number(el)))
      : [];
  const correctAnswer =
    question.correctAnswer && question.correctAnswer !== ""
      ? question.correctAnswer?.split(",").map(Number)
      : [];

  const updatedStudentAnswers =
    studentAnswers &&
    studentAnswers.length > 0 &&
    studentAnswers.map((answer, ind) => {
      return {
        order: answer,
        isInCorrectAnswer: [answer] == correctAnswer[ind],
      };
    });

  return (
    <RFlex styleProps={{ justifyContent: "space-between" }} className="mt-4">
      <RFlex
        styleProps={{
          flexDirection: "column",
        }}
      >
        {updatedStudentAnswers &&
          updatedStudentAnswers.length > 0 &&
          updatedStudentAnswers.map((item1, ind) => {
            const { order, isInCorrectAnswer } = item1;
            const matchedItem = question?.sequencingStatements.find(
              (item2) => +item2.order === +order
            );

            if (matchedItem) {
              const { text, files } = matchedItem;

              return (
                <RFlex styleProps={{ alignItems: "center" }}>
                  <span>{ind + 1}. </span>

                  <RFlex
                    key={order}
                    className={
                      isInCorrectAnswer
                        ? styles.correct__boxes
                        : styles.wrong__boxes
                    }
                    style={{
                      width: files && files.length > 0 ? "" : "130px",
                    }}
                  >
                    <i
                      className={`fa-solid ${
                        isInCorrectAnswer
                          ? "fa-circle-check"
                          : "fa-circle-xmark"
                      }  ${styles.circle__check}`}
                    />
                    <span>{text}</span>
                    {files && files.length > 0 && (
                      <img
                        src={Services.storage.file + files[0]?.hash_id}
                        alt={order}
                        className={styles.image}
                      />
                    )}
                  </RFlex>
                </RFlex>
              );
            }

            return null;
          })}
      </RFlex>
      <RFlex
        styleProps={{
          flexDirection: "column",
        }}
      >
        {correctAnswer && correctAnswer.length > 0 && (
          <>
            {question?.sequencingStatements &&
              question?.sequencingStatements
                .filter((item) => correctAnswer.includes(item.order))
                .map((el, ind) => (
                  <div key={el.id}>
                    <RFlex styleProps={{ alignItems: "center" }}>
                      <div
                        className={styles.correct__boxes}
                        style={{
                          width:
                            el?.files && el?.files.length > 0 ? "" : "130px",
                        }}
                      >
                        <i
                          className={`fa-solid fa-circle-check ${styles.circle__check}`}
                        />
                        {el.text}
                        {el?.files && el?.files.length > 0 && (
                          <img
                            src={Services.storage.file + el?.files[0]?.hash_id}
                            alt={el?.id}
                            className={styles.image}
                          />
                        )}
                      </div>
                    </RFlex>
                  </div>
                ))}
          </>
        )}
      </RFlex>
    </RFlex>
  );
};

export default CorrectOrReviewOrdering;
