import React, { useContext } from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { QuestionSetSolveContext } from "logic/Courses/CourseManagement/QuestionSet/GSolveQuestionSet";
import SaveAnswersButtons from "../SaveAnswersButtons";
import Feedbacks from "../feedbacks";
import styles from "../../SolveQuestionSet.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import Headers from "../Headers";
import CorrectOrReviewOrdering from "./CorrectOrReviewOrdering";

const SolveOrdering = ({
  key,
  questionGroup,
  question,
  questionNumber,
  readMode = false,
  correctionMode = false,
}) => {
  const QuestionSetSolveData = useContext(QuestionSetSolveContext);

  const SelectedValue =
    QuestionSetSolveData.questionAnswers?.questionAnswers.find(
      (item) => +item.questionContextId == +question.module_context_question_id
    );

  const onDragEnd = (result) => {
    const DropOtSideInItemsAbove = result.draggableId.includes("__");

    if (!result.destination && DropOtSideInItemsAbove) {
      if (result.draggableId && result.draggableId !== null) {
        const DraggableIdAfterFormate = result.draggableId?.split("__")[0];
        QuestionSetSolveData.handleRemoveOrderingQuestion({
          questionGroupId: questionGroup.id,
          questionId: question.id,
          groupContextId: questionGroup.module_context_group_id,
          questionContextId: question.module_context_question_id,
          type: question.type.name,
          isCorrect: true,
          draggableOrder: DraggableIdAfterFormate,
        });
      }
    } else if (!result.destination) {
      return;
    } else if (result.destination.droppableId == "droppableId") {
      return;
    } else {
      QuestionSetSolveData.handleFillDataToSolveInOrderingQuestion({
        questionGroupId: questionGroup.id,
        questionId: question.id,
        groupContextId: questionGroup.module_context_group_id,
        questionContextId: question.module_context_question_id,
        type: question.type.name,
        isCorrect: true,
        droppableOrder: result.destination.droppableId,
        draggableOrder: result.draggableId,
      });
    }
  };

  const DroppableArea = () => {
    return (
      <div>
        {question?.sequencingStatements?.map((sequencingStatement, ind) => (
          <Droppable
            droppableId={`${sequencingStatement.order}`}
            key={sequencingStatement.order}
            // type={`111`}
          >
            {(provided, snapshot) => (
              <div
                {...provided.droppableProps}
                ref={provided.innerRef}
                backgroundColor={snapshot.isDraggingOver ? "red" : "red"}
                style={{ width: "130px" }}
              >
                <RFlex styleProps={{ alignItems: "center" }}>
                  <span>{ind + 1}. </span>
                  <div className={styles.droppable__boxes}>
                    <Draggable
                      key={`${sequencingStatement.order}`}
                      draggableId={`${sequencingStatement.order}__items_are_above`}
                      index={ind}
                    >
                      {(provided, snapshot) => (
                        <div
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                          // className={styles.draggable__div}
                          key={sequencingStatement.id}
                          style={{
                            // backgroundColor: snapshot.isDragging
                            //   ? "#263B4A"
                            //   : "#456C86",

                            ...provided.draggableProps.style,
                          }}
                        >
                          {
                            question?.sequencingStatements.find(
                              (el) =>
                                +el.order == +sequencingStatement.orderSelected
                            )?.text
                          }
                        </div>
                      )}
                    </Draggable>
                  </div>
                </RFlex>

                {provided.placeholder}
              </div>
            )}
          </Droppable>
        ))}
      </div>
    );
  };

  const DragAndDropArea = () => {
    return (
      <Droppable droppableId={`droppableId`}>
        {(provided, snapshot) => (
          <section
            {...provided.droppableProps}
            ref={provided.innerRef}
            backgroundColor={snapshot.isDraggingOver ? "lightblue" : "white"}
            style={{ display: "flex", gap: "10px" }}
          >
            {question?.sequencingStatements?.map(
              (sequencingStatement, ind) =>
                !SelectedValue?.answer
                  ?.split(",")
                  .map(Number)
                  .includes(+sequencingStatement.order) && (
                  <Draggable
                    key={`${sequencingStatement.order}_items_are_belows`}
                    draggableId={`${sequencingStatement.order}`}
                    index={ind}
                  >
                    {(provided, snapshot) => (
                      <div
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                        className={styles.draggable__div}
                        key={sequencingStatement.id}
                        style={{
                          // backgroundColor: snapshot.isDragging
                          //   ? "#263B4A"
                          //   : "#456C86",
                          ...provided.draggableProps.style,
                        }}
                      >
                        {sequencingStatement.text}
                      </div>
                    )}
                  </Draggable>
                )
            )}
            {provided.placeholder}
          </section>
        )}
      </Droppable>
    );
  };

  return (
    <div>
      <Headers questionNumber={questionNumber} question={question} />
      {QuestionSetSolveData?.correctOrReviewMode ? (
        <CorrectOrReviewOrdering question={question} />
      ) : (
        <div className={styles.true__false__container}>
          <DragDropContext onDragEnd={onDragEnd}>
            <DroppableArea />
            <DragAndDropArea />
          </DragDropContext>
        </div>
      )}
      {!QuestionSetSolveData.correctOrReviewMode && (
        <SaveAnswersButtons questionGroup={questionGroup} question={question} />
      )}
      {correctionMode && <Feedbacks question={question} />}
    </div>
  );
};

export default SolveOrdering;
