import React, { useContext } from "react";
import { QuestionSetSolveContext } from "logic/Courses/CourseManagement/QuestionSet/GSolveQuestionSet";
import { primaryColor } from "config/constants";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import RTextArea from "components/Global/RComs/RTextArea";

const Feedbacks = ({ question }) => {
  const QuestionSetSolveData = useContext(QuestionSetSolveContext);

  return (
    <div className="pt-3">
      <RFlex>
        <span
          className="mb-4"
          style={{ color: primaryColor, cursor: "pointer" }}
          onClick={QuestionSetSolveData.handleOpenAndCloseFeedback}
        >{tr`add_feedback`}</span>
      </RFlex>
      {QuestionSetSolveData.openFeedback ? (
        <RTextArea
          name="feedback"
          onChange={(event) =>
            QuestionSetSolveData.handleAddFeedbackToQuestion({
              learnerContextQuestionId: question.learner_context_question_id,
              key: event.target.name,
              value: event.target.value,
            })
          }
        />
      ) : (
        ""
      )}
    </div>
  );
};

export default Feedbacks;
