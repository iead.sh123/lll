import React, { useContext } from "react";
import { QuestionSetSolveContext } from "logic/Courses/CourseManagement/QuestionSet/GSolveQuestionSet";
import DrawPlace from "../../QuestionsTypes/DrawPlace/DrawPlace";
import SaveAnswersButtons from "./SaveAnswersButtons";
import Feedbacks from "./feedbacks";
import Headers from "./Headers";

const SolveDraw = ({
  key,
  questionGroup,
  question,
  questionNumber,
  readMode = false,
  correctionMode = false,
}) => {
  const QuestionSetSolveData = useContext(QuestionSetSolveContext);

  return (
    <div key={key}>
      <Headers questionNumber={questionNumber} question={question} />

      <div>
        <DrawPlace
          QuestionSetSolveData={QuestionSetSolveData}
          questionGroupId={questionGroup.id}
          groupContextId={questionGroup.module_context_group_id}
          questionContextId={question.module_context_question_id}
          type={question.type.name}
          hashId={null}
          readMode={readMode || !correctionMode}
        />
      </div>

      {!QuestionSetSolveData.correctOrReviewMode && (
        <SaveAnswersButtons questionGroup={questionGroup} question={question} />
      )}
      {correctionMode && <Feedbacks question={question} />}
    </div>
  );
};

export default SolveDraw;
