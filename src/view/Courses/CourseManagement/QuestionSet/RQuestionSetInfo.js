import React, { useContext } from "react";
import { Row, Col, FormGroup, Label, Input } from "reactstrap";
import { QuestionSetContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionSetEditor";
import { Services } from "engine/services";
import { RLabel } from "components/Global/RComs/RLabel";
import RCkEditor from "components/Global/RComs/RCkEditor";
import RButton from "components/Global/RComs/RButton";
import RTags from "components/Global/RComs/RTags";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RQuestionSetInfo = () => {
	const QuestionSetData = useContext(QuestionSetContext);
	return (
		<Row>
			<Col xs={12} md={4} className="pl-0">
				<RFlex styleProps={{ flexDirection: "column" }}>
					<RFlex>
						<Label className="p-0 m-0">{tr("name")}</Label>
						<RLabel value={tr(" ")} icon="fa-regular fa-circle-question pt-1" lettersToShow={0} />
					</RFlex>
					<FormGroup>
						<Input
							name="name"
							type="text"
							placeholder={tr`name`}
							value={QuestionSetData?.questionSet?.name}
							onChange={(event) =>
								QuestionSetData.handleChangeQuestionSetInfo({
									key: event.target.name,
									value: event.target.value,
								})
							}
						/>
					</FormGroup>
				</RFlex>
			</Col>
			<Col xs={12} md={4}>
				<RFlex styleProps={{ flexDirection: "column" }}>
					<RFlex>
						<Label className="p-0 m-0">{tr("tags")}</Label>
						<RLabel value={tr(" ")} icon="fa-regular fa-circle-question pt-1" lettersToShow={0} />
					</RFlex>
					<FormGroup>
						<RTags
							url={`${Services.tag_search.backend}api/v1/tag/search?prefix=`}
							getArrayFromResponse={(res) => (res && res.data && res.data.status ? res.data.data : null)}
							getValueFromArrayItem={(i) => i.id}
							getLabelFromArrayItem={(i) => i.name}
							getIdFromArrayItem={(i) => i.id}
							defaultValues={QuestionSetData?.questionSet?.tags}
							onChange={(data) =>
								QuestionSetData.handleChangeQuestionSetInfo({
									key: "tags",
									value: data,
								})
							}
						/>
					</FormGroup>
				</RFlex>
			</Col>
			<Col xs={12} md={2} className="pr-0">
				<RFlex styleProps={{ flexDirection: "column" }}>
					<RFlex>
						<Label className="p-0 m-0">{tr("points")}</Label>
						<RLabel value={tr(" ")} icon="fa-regular fa-circle-question pt-1" lettersToShow={0} />
					</RFlex>
					<FormGroup>
						<Input name="points" type="text" placeholder={tr`points`} value={QuestionSetData?.questionSet?.points} disabled={true} />
					</FormGroup>
				</RFlex>
			</Col>
			<Col xs={12} md={2}>
				<Label className="p-0 m-0"></Label>

				<RButton text={tr`view_as_learner`} faicon="fa fa-graduation-cap" outline={true} color={"primary"} />
			</Col>

			<Col xs={12} md={10} className="pl-0 pt-3">
				<RFlex styleProps={{ flexDirection: "column" }}>
					<RFlex>
						<Label className="p-0 m-0">{tr("description")}</Label>
						<RLabel value={tr("visible to learners")} icon="fa-regular fa-circle-question pt-1" lettersToShow={0} />
					</RFlex>
					<FormGroup>
						<RCkEditor
							data={QuestionSetData?.questionSet?.description}
							handleChange={(data) =>
								QuestionSetData.handleChangeQuestionSetInfo({
									key: "description",
									value: data,
								})
							}
						/>
					</FormGroup>
				</RFlex>
			</Col>
		</Row>
	);
};

export default RQuestionSetInfo;
