import React, { useContext } from "react";
import { QuestionSetContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionSetEditor";
import { primaryColor } from "config/constants";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import styles from "./QuestionSet.module.scss";

const RHideCollapse = ({ group, groupId, questionTitle, questionPoint }) => {
	const QuestionSetData = useContext(QuestionSetContext);
	const questionTypesEnum = [
		{
			value: "MULTIPLE_CHOICES_MULTIPLE_ANSWERS",
			icon: "fas fa-tasks pt-1",
		},
		{
			value: "MULTIPLE_CHOICES_SINGLE_ANSWER",
			icon: "fas fa-tasks pt-1",
		},
		{
			value: "ESSAY",
			icon: "fas fa-align-left pt-1",
		},
		{
			value: "MATCH",
			icon: "fas fa-grip-lines-vertical pt-1",
		},
		{
			value: "DRAW",
			icon: "fa-solid fa-pen pt-1",
		},

		{
			value: "UPLOAD_FILE",
			icon: "fa-regular fa-file pt-1",
		},

		{
			value: "TRUEFALSE",
			icon: "fa-solid fa-clipboard-check pt-1",
		},
		{
			value: "SEQUENCING",
			icon: "fa-solid fa-arrow-down-1-9 pt-1",
		},
	];

	return (
		<RFlex
			styleProps={{ justifyContent: "space-between", cursor: "pointer" }}
			onClick={() => QuestionSetData.collapsesQuestionSetGroupToggle(groupId)}
		>
			<RFlex styleProps={{ alignItems: "center" }} className="pl-3">
				<span>{questionTypesEnum.map((el) => el.value == group?.questions[0]?.type && <i className={el.icon} />)}</span>
				<span
					className={styles.padding}
					dangerouslySetInnerHTML={{ __html: `<span>${group?.isGroup ? questionTitle : group?.questions[0]?.text}</span>` }}
				/>
			</RFlex>
			<RFlex>
				<span className={styles.padding}>
					{group?.isGroup ? questionPoint : group?.questions[0]?.points}
					&nbsp;
					{tr`points`}
				</span>
				<div className={styles.show__collapse}>
					<i className="fa-solid fa-chevron-down pt-1" style={{ color: primaryColor }} />
					<span>{tr`show`}</span>
				</div>
			</RFlex>
		</RFlex>
	);
};

export default RHideCollapse;
