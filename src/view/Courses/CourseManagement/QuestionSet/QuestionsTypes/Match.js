import React, { useContext } from "react";
import { Row, Col, FormGroup, Label, Input } from "reactstrap";
import { QuestionSetContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionSetEditor";
import { dangerColor, primaryColor, warningColor } from "config/constants";
import ChoicesMatchQuestion from "./ChoicesQuestions/ChoicesMatchQuestion";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";

const Match = ({ questionGroupId, question, questionNumber = 0, group }) => {
	const QuestionSetData = useContext(QuestionSetContext);
	const requiredQuestionsToQuestionGroup = group?.requiredQuestionsToAnswer && group?.points ? true : false;

	return (
		<>
			<Row className="mt-4 d-flex align-items-center">
				<Col md={8} xs={12}>
					<RFlex styleProps={{ flexDirection: "column" }}>
						<RFlex>
							<i className="fas fa-grip-lines-vertical pt-1" />
							<RFlex>
								<Label className="p-0 m-0">
									{tr`question`} &nbsp;
									{questionNumber}
								</Label>
								<Label className="p-0 m-0">({tr("match")})</Label>
							</RFlex>
						</RFlex>
						<FormGroup>
							<Input
								autoFocus
								name="text"
								type="text"
								value={question?.text}
								onChange={(event) =>
									QuestionSetData.handleFillDataToQuestionType({
										questionGroupId: questionGroupId,
										questionTypeId: question?.id ? question?.id : question?.fakeId,
										key: event.target.name,
										value: event.target.value,
									})
								}
							/>
						</FormGroup>
					</RFlex>
				</Col>

				<Col md={4} xs={12}>
					<RFlex
						styleProps={{
							alignItems: "center",
							justifyContent: "space-between",
						}}
					>
						<RFlex styleProps={{ flexDirection: "column" }}>
							<RFlex>
								<Label className="p-0 m-0">{tr("points")}</Label>
							</RFlex>
							<FormGroup>
								<Input
									name="points"
									type="number"
									placeholder={tr`points`}
									min={1}
									value={requiredQuestionsToQuestionGroup ? group.points : question?.points}
									disabled={requiredQuestionsToQuestionGroup}
									onChange={(event) => {
										QuestionSetData.handleFillDataToQuestionType({
											questionGroupId: questionGroupId,
											questionTypeId: question?.id ? question?.id : question?.fakeId,
											key: event.target.name,
											value: event.target.value,
										});
									}}
								/>
							</FormGroup>
						</RFlex>

						<RFlex styleProps={{ alignItems: "center", paddingTop: "20px" }}>
							<RFileSuite
								parentCallback={(values) => {
									QuestionSetData.handleFillDataToQuestionType({
										questionGroupId: questionGroupId,
										questionTypeId: question?.id ? question?.id : question?.fakeId,
										key: "files",
										value: values,
									});
								}}
								singleFile={true}
								binary={true}
								value={question?.files}
								theme="button_with_replace"
								fileType="image/*"
							/>
							<i
								className={iconsFa6.delete}
								style={{ color: dangerColor, cursor: "pointer" }}
								onClick={() =>
									QuestionSetData.handleRemoveQuestionGroup({
										questionGroupId: questionGroupId,
										questionTypeId: question?.id ? question?.id : question?.fakeId,
										isGroup: group?.isGroup,
									})
								}
							></i>
						</RFlex>
					</RFlex>
				</Col>
			</Row>

			{/* start choices */}
			<ChoicesMatchQuestion questionGroupId={questionGroupId} question={question} questionType={question.type} choices={question?.pairs} />
			{/* end choices */}

			<Row>
				<Col xs={12} className="mt-2">
					<RFlex
						styleProps={{
							justifyContent: "space-between",
						}}
					>
						<AppCheckbox
							onChange={(event) => {
								QuestionSetData.handleFillDataToQuestionType({
									questionGroupId: questionGroupId,
									questionTypeId: question?.id ? question?.id : question?.fakeId,
									key: "shuffleAnswers",
									value: event.target.checked,
								});
							}}
							label={tr`shuffle_answer`}
						/>
						<div
							style={{ color: primaryColor, cursor: "pointer" }}
							onClick={() => {
								QuestionSetData.handleAddPairsToMatchQuestion({
									questionGroupId: questionGroupId,
									questionTypeId: question?.id ? question?.id : question?.fakeId,
									parisLength: question?.pairs?.length,
								});
							}}
						>
							<i className="fa fa-plus pr-2" />
							<span>{tr`add_option`}</span>
						</div>
					</RFlex>
				</Col>
				{!requiredQuestionsToQuestionGroup && (
					<Col xs={12} className="mt-2">
						<AppCheckbox
							onChange={(event) => {
								QuestionSetData.handleFillDataToQuestionType({
									questionGroupId: questionGroupId,
									questionTypeId: question?.id ? question?.id : question?.fakeId,
									key: "isRequired",
									value: event.target.checked,
								});
							}}
							label={<span>{tr`required_question`}</span>}
							checked={question?.isRequired}
						/>
					</Col>
				)}
			</Row>
		</>
	);
};

export default Match;
