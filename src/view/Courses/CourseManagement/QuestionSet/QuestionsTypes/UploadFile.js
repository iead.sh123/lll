import React, { useContext } from "react";
import { QuestionSetContext } from "logic/Courses/CourseManagement/QuestionSet/GQuestionSetEditor";
import { Row, Col, FormGroup, Label, Input } from "reactstrap";
import { dangerColor, primaryColor, warningColor } from "config/constants";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import Lamp from "assets/img/new/svg/lamp.svg";
import tr from "components/Global/RComs/RTranslator";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import iconsFa6 from "variables/iconsFa6";

const UploadFile = ({ questionGroupId, question, questionNumber = 0, group }) => {
	const QuestionSetData = useContext(QuestionSetContext);
	const requiredQuestionsToQuestionGroup = group?.requiredQuestionsToAnswer && group?.points ? true : false;

	return (
		<Row className="mt-4 mb-4">
			<Col md={8} xs={12}>
				<RFlex styleProps={{ flexDirection: "column" }}>
					<RFlex>
						<i className="far fa-file pt-1" />
						<RFlex>
							<Label className="p-0 m-0">
								{tr`question`} &nbsp;
								{questionNumber}
							</Label>
							<Label className="p-0 m-0">({tr("upload_file")})</Label>
						</RFlex>
					</RFlex>
					<FormGroup>
						<Input
							autoFocus
							name="text"
							type="text"
							placeholder={tr`write_something_about_the_uploaded_file`}
							value={question?.text}
							onChange={(event) =>
								QuestionSetData.handleFillDataToQuestionType({
									questionGroupId: questionGroupId,
									questionTypeId: question?.id ? question?.id : question?.fakeId,
									key: event.target.name,
									value: event.target.value,
								})
							}
						/>
					</FormGroup>
				</RFlex>
			</Col>

			<Col md={4} xs={12}>
				<RFlex
					styleProps={{
						alignItems: "center",
						justifyContent: "space-between",
					}}
				>
					<RFlex styleProps={{ flexDirection: "column" }}>
						<RFlex>
							<Label className="p-0 m-0">{tr("points")}</Label>
						</RFlex>
						<FormGroup>
							<Input
								name="points"
								type="number"
								placeholder={tr`points`}
								min={1}
								value={requiredQuestionsToQuestionGroup ? group.points : question?.points}
								disabled={requiredQuestionsToQuestionGroup}
								onChange={(event) => {
									QuestionSetData.handleFillDataToQuestionType({
										questionGroupId: questionGroupId,
										questionTypeId: question?.id ? question?.id : question?.fakeId,
										key: event.target.name,
										value: event.target.value,
									});
								}}
							/>
						</FormGroup>
					</RFlex>

					<RFlex styleProps={{ alignItems: "center", paddingTop: "20px" }}>
						<RFileSuite
							parentCallback={(values) => {
								QuestionSetData.handleFillDataToQuestionType({
									questionGroupId: questionGroupId,
									questionTypeId: question?.id ? question?.id : question?.fakeId,
									key: "files",
									value: values,
								});
							}}
							singleFile={true}
							binary={true}
							value={question?.files}
							theme="button_with_replace"
							fileType="image/* , video/* , application/* , text/*"
						/>
						<i
							className={iconsFa6.delete}
							style={{ color: dangerColor, cursor: "pointer" }}
							onClick={() =>
								QuestionSetData.handleRemoveQuestionGroup({
									questionGroupId: questionGroupId,
									questionTypeId: question?.id ? question?.id : question?.fakeId,
									isGroup: group?.isGroup,
								})
							}
						></i>
					</RFlex>
				</RFlex>
			</Col>

			{/* <Col xs={12} className="mt-2">
				<RFlex>
					<img src={Lamp} />
					<span className="text-muted">
						{tr`Any uploaded file will be visible to students to download then upload another one`}
						&nbsp;
						<span style={{ color: primaryColor, textDecoration: "underline" }}>{tr`View as students`}</span>
					</span>
				</RFlex>
			</Col> */}

			{!requiredQuestionsToQuestionGroup && (
				<Col xs={12} className="mt-2">
					<AppCheckbox
						onChange={(event) => {
							QuestionSetData.handleFillDataToQuestionType({
								questionGroupId: questionGroupId,
								questionTypeId: question?.id ? question?.id : question?.fakeId,
								key: "isRequired",
								value: event.target.checked,
							});
						}}
						label={<span>{tr`required_question`}</span>}
						checked={question?.isRequired}
					/>
				</Col>
			)}
		</Row>
	);
};

export default UploadFile;
