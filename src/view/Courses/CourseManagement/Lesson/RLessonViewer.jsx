import React from "react";
import RLessonContentViewer from "view/CoursesManager/RCourseModule/RLessonContentViewer";
import RLessonPlanMainData from "../LessonPlan/RLessonPlanMainData";
import RLessonPlanFullData from "../LessonPlan/RLessonPlanFullData";
import GLiveSession from "logic/GCoursesManager/GLiveSession/GLiveSession";
import RFilterTabs from "components/Global/RComs/RFilterTabs/RFilterTabs";
import styles from "./RLessonViewer.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { primaryColor } from "config/constants";
import { Row } from "reactstrap";
import REmptyData from "components/RComponents/REmptyData";

const RLessonViewer = ({ lessonContentByLessonId, lessonPlan, history, Tabs, tabActive, setTabActive }) => {
	return (
		<div className={styles.lp__viewer__container}>
			<div className={styles.lp__viewer__main}>
				<section className={styles.lp__viewer__main__data}>
					<RFlex style={{ color: primaryColor, cursor: "pointer" }} onClick={() => history.goBack()}>
						<i class="fa-solid fa-arrow-left pt-1 pr-1"></i>
						<span>{tr`back_to_all_course_content`}</span>
					</RFlex>
					<RFilterTabs tabs={Tabs} setTabActive={setTabActive} />

					{tabActive == "lesson_plan" ? (
						lessonPlan ? (
							<RLessonPlanMainData lessonPlan={lessonPlan} width={"100%"} />
						) : (
							<REmptyData />
						)
					) : tabActive == "lesson_content" ? (
						<Row>
							{lessonContentByLessonId?.contents &&
								lessonContentByLessonId?.contents.length > 0 &&
								lessonContentByLessonId?.contents?.map((lesson_content) => <RLessonContentViewer lessonContent={lesson_content} />)}
						</Row>
					) : tabActive == "attendance" ? (
						"attendance"
					) : (
						""
					)}
				</section>
				<section className={styles.lp__viewer__main__data__live__session}>
					<GLiveSession />
				</section>
			</div>
			{tabActive == "lesson_plan" &&
				lessonPlan?.lesson_plan_items.map((lesson_plan_item) => <RLessonPlanFullData lesson_plan_item={lesson_plan_item} />)}
		</div>
	);
};

export default RLessonViewer;
