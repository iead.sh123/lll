import React from "react";
import iconsFa6 from "variables/iconsFa6";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { ReloadIcon } from "@radix-ui/react-icons";
import { Button } from "ShadCnComponents/ui/button";

const RRubricHeader = ({ handlers, loading }) => {
	return (
		<RFlex>
			<Button className="flex gap-[5px]" variant={"outline"} onClick={() => handlers.handlePushToRubricEditor()}>
				<i className={iconsFa6.pencil} alt="pencil" />
				{tr`edit`}
			</Button>

			<Button
				className="flex gap-[5px]"
				variant={"outline"}
				onClick={() => handlers.handleExportRubricAsPdf()}
				disabled={loading.exportRubricAsPdfLoading}
			>
				{loading.exportRubricAsPdfLoading ? (
					<RFlex>
						<i className={iconsFa6.spinner} alt="spinner" />
					</RFlex>
				) : (
					<i className={iconsFa6.pdf} alt="pdf" />
				)}
				{tr`export_as_pdf`}
			</Button>
		</RFlex>
	);
};

export default RRubricHeader;
