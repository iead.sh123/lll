import React, { useContext } from "react";
import { RubricContext } from "logic/Courses/Rubrics/GRubricEditor";
import { primaryColor } from "config/constants";
import { FieldArray } from "formik";
import { FormText } from "reactstrap";
import RCategoryCollapses from "./RCategoryCollapses";
import RFormBasic from "./RFormBasic";
import styles from "./Rubric.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RRubricEditor = () => {
	const RubricData = useContext(RubricContext);
	return (
		<RFlex className={styles.rubric__editor__container}>
			<RFormBasic />

			<FieldArray name="categories">
				{(arrayHelpers) => (
					<RFlex className="flex-col gap-0">
						{/* Create Category */}
						<RFlex
							styleProps={{ color: primaryColor, cursor: "pointer", width: "fit-content" }}
							onClick={() => arrayHelpers.push({ fakeId: Math.random(), subject: "", standards: [], saved: false })}
						>
							<i className="fa fa-plus pt-1" />
							<p>{tr`create_new_category`}</p>
						</RFlex>
						{/* At least one category is required [ validation ] */}
						{RubricData?.formProperties?.errors?.categories && typeof RubricData?.formProperties?.errors?.categories == "string" && (
							<FormText color="danger">{RubricData?.formProperties?.errors?.categories}</FormText>
						)}

						<RFlex className={styles.rubric__editor__categories}>
							{RubricData.values.categories &&
								RubricData.values.categories.length > 0 &&
								RubricData.values.categories.map((category, index) => (
									<RCategoryCollapses
										key={category.id ? category.id : category.fakeId}
										arrayHelpers={arrayHelpers}
										category={category}
										categoryIndex={index}
									/>
								))}
						</RFlex>
					</RFlex>
				)}
			</FieldArray>
		</RFlex>
	);
};

export default RRubricEditor;
