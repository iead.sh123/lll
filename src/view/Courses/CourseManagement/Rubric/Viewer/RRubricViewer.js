import React from "react";
import styles from "./../Rubric.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Table } from "reactstrap";

const RRubricViewer = ({ rubricData }) => {
	return (
		<RFlex className={styles.r_viewer}>
			<RFlex className={styles.r_viewer_basic_info}>
				<RFlex styleProps={{ alignItems: "center" }}>
					<p style={{ fontSize: "20px", fontWeight: "300" }}>{rubricData.title}</p>
					<RFlex styleProps={{ gap: 2 }}>
						<h6>{rubricData.total_points}</h6>
						<h6>{tr`points`}</h6>
					</RFlex>
				</RFlex>
				<p dangerouslySetInnerHTML={{ __html: rubricData.description }} />
			</RFlex>
			<RFlex className={styles.rubric__editor__categories}>
				{rubricData.categories &&
					rubricData.categories.length > 0 &&
					rubricData.categories.map((category, index) => (
						<RFlex styleProps={{ flexDirection: "column" }} key={category?.id ? category?.id : category?.fakeId}>
							<div>
								<section className={styles.r__collapse}>{category?.subject}</section>
								<Table bordered className="mb-0">
									<thead>
										<tr>
											<th style={{ width: "320px" }}>
												{tr`standards`} ({category?.standards?.length})
											</th>
											<th style={{ textAlign: "center" }}>{tr`ratings`}</th>
										</tr>
									</thead>

									<tbody>
										{category &&
											category.standards &&
											category.standards.length > 0 &&
											category.standards.map((standard, standardIndex) => {
												return (
													<tr key={standard.id ? standard.id : standard.fakeId}>
														<td style={{ fontWeight: "bold", width: "320px" }}>
															<RFlex styleProps={{ alignItems: "center", justifyContent: "space-between" }}>
																<span style={{ color: "#818181" }}>
																	{standard.subject?.length > 30 ? standard.subject.substring(0, 30) + "..." : standard.subject}
																</span>
															</RFlex>
														</td>
														<td style={{ padding: "0px" }}>
															<Table className="mb-0">
																<tbody>
																	<tr>
																		{standard?.ratings.map((rate, ind, array) => (
																			<td
																				key={rate.id ? rate.id : rate.fakeId}
																				style={{
																					color: "#818181",
																					border: "0px",
																					borderRight: ind !== array.length - 1 ? "1px solid #dee2e6" : "0px",
																				}}
																			>
																				<div style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
																					{rate.from_score ? <span>{rate.from_score}</span> : ""}
																					{rate.to_score ? <span>{rate.to_score}</span> : ""}
																					<span>{rate.abbreviation}</span>
																				</div>
																			</td>
																		))}
																	</tr>
																</tbody>
															</Table>
														</td>
													</tr>
												);
											})}
									</tbody>
								</Table>
							</div>
							<RFlex styleProps={{ gap: 100 }}>
								{category &&
									category.standards &&
									category.standards.length > 0 &&
									category.standards.map((standard, standardIndex) => (
										<RFlex styleProps={{ flexDirection: "column", gap: 0 }}>
											{standard.ratings.map((rate) => (
												<RFlex styleProps={{ gap: 5, alignItems: "center" }} key={rate.id}>
													<p style={{ fontWeight: "bolder", fontSize: "16px" }}>{rate.abbreviation}: </p>
													<p style={{ fontSize: "16px" }}>{rate.title}</p>
												</RFlex>
											))}
										</RFlex>
									))}
							</RFlex>
						</RFlex>
					))}
			</RFlex>
		</RFlex>
	);
};

export default RRubricViewer;
