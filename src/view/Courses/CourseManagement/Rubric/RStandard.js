import React, { useContext } from "react";
import { Input, FormGroup, FormText } from "reactstrap";
import { RubricContext } from "logic/Courses/Rubrics/GRubricEditor";
import { Textarea } from "ShadCnComponents/ui/textarea";
import { Checkbox } from "ShadCnComponents/ui/checkbox";
import RButton from "components/Global/RComs/RButton";
import RRates from "./RRates";
import styles from "./Rubric.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RStandard = ({ categoryData, handleCloseStandardModal, setFieldValue, handleRemoveStandard }) => {
	const RubricData = useContext(RubricContext);
	const pathVal = RubricData?.values?.categories?.[categoryData?.categoryIndex]?.standards?.[categoryData?.standardIndex];

	const pathErr = RubricData?.errors?.categories?.[categoryData?.categoryIndex]?.standards?.[categoryData?.standardIndex];
	const pathTouch = RubricData?.touched?.categories?.[categoryData?.categoryIndex]?.standards?.[categoryData?.standardIndex];

	// console.log("pathErr", pathErr);
	return (
		<RFlex className="flex-col">
			<span className={styles.rubric__editor__standard__title}>{tr`create_new_standard`}</span>
			<FormGroup className="mb-0">
				<label>{tr("standard_title")}</label>
				<Input
					type="text"
					name={`categories.${categoryData.categoryIndex}.standards.${categoryData.standardIndex}.subject`}
					value={pathVal?.["subject"]}
					onChange={RubricData.handleChange}
					placeholder={tr`standard_title`}
					className={pathErr?.["subject"] ? "input__error" : ""}
				/>
				<FormText color="danger">{pathErr?.["subject"]}</FormText>
			</FormGroup>

			{/* max_points  [ pointed ]*/}
			{RubricData.values.pointed && (
				<FormGroup className="mb-0">
					<label>{tr("max_points")}</label>
					<Input
						type="number"
						name={`categories.${categoryData.categoryIndex}.standards.${categoryData.standardIndex}.max_points`}
						value={pathVal?.["max_points"]}
						onChange={RubricData.handleChange}
						placeholder={tr`max_points`}
						className={pathErr?.["max_points"] ? "input__error" : ""}
						min={0}
					/>
					<FormText color="danger">{pathErr?.["max_points"]}</FormText>
				</FormGroup>
			)}

			<FormGroup className="mb-0">
				<label>{tr("description")}</label>

				<Textarea
					name={`categories.${categoryData.categoryIndex}.standards.${categoryData.standardIndex}.description`}
					onChange={RubricData.handleChange}
					value={pathVal?.["description"]}
					placeholder={tr`description`}
				/>
			</FormGroup>

			{RubricData?.values?.pointed && (
				<RFlex className="items-center">
					<Checkbox
						onCheckedChange={(event) => {
							setFieldValue(`categories.${categoryData.categoryIndex}.standards.${categoryData.standardIndex}.is_range`, event);
							console.log("eventeventevent outside", !event);
							if (!event) {
								console.log("eventeventevent", event);
								setFieldValue(`categories.${categoryData.categoryIndex}.standards.${categoryData.standardIndex}.to_score`, null);
							}
						}}
						checked={pathVal?.is_range}
					/>
					<span>{tr`range`}</span>
				</RFlex>
			)}

			{/* RRates */}
			<RRates categoryData={categoryData} />

			<RFlex styleProps={{ justifyContent: "flex-end" }}>
				<RButton text={tr`save`} color="primary" onClick={handleCloseStandardModal} disabled={pathErr} />
				<RButton
					text={tr`cancel`}
					color="link"
					onClick={() => {
						if (pathErr !== undefined) {
							handleRemoveStandard(setFieldValue, RubricData.values);
						}
						handleCloseStandardModal();
					}}
					// disabled={pathErr}
				/>
			</RFlex>
		</RFlex>
	);
};

export default RStandard;
