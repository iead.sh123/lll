import React, { useContext } from "react";
import { Popover, PopoverTrigger } from "ShadCnComponents/ui/popover";
import { PopoverContent } from "ShadCnComponents/ui/popover";
import { RubricContext } from "logic/Courses/Rubrics/GRubricEditor";
import { FieldArray } from "formik";
import { FormText } from "reactstrap";
import RAllStandards from "./RAllStandards";
import iconsFa6 from "variables/iconsFa6";
import tr from "components/Global/RComs/RTranslator";

const RNewStandards = ({ categoryIndex }) => {
	const RubricData = useContext(RubricContext);

	return (
		<FieldArray name={`categories[${categoryIndex}].standards`}>
			{(arrayHelpers) => (
				<Popover className="pt-1">
					<PopoverTrigger>
						<div className="flex gap-2 items-center">
							<i className={iconsFa6.plus} />
							<p>{tr`new_standards`}</p>
						</div>
						{/* At Least One Standard Is Required [ Validation ] */}
						{RubricData.touched.categories?.[categoryIndex]?.["standards"] &&
							RubricData.errors.categories?.[categoryIndex]?.["standards"] &&
							typeof RubricData.errors.categories?.[categoryIndex]?.["standards"] == "string" && (
								<FormText color="danger">{RubricData.errors.categories?.[categoryIndex]?.["standards"]}</FormText>
							)}
					</PopoverTrigger>
					<PopoverContent className="p-3 w-[237px]">
						<div className="flex flex-col gap-[7px]">
							<div
								className="flex gap-[5px] cursor-pointer text-themePrimary text-xs items-center"
								onClick={() => {
									// arrayHelpers.push({
									// 	fakeId: Math.random(),
									// 	subject: "",
									// 	max_points: null,
									// 	description: "",
									// 	is_range: false,
									// 	ratings: [{ fakeId: Math.random(), description: "", title: "", from_score: null, to_score: null }],
									// });
									RubricData.handleOpenStandardModal({
										categoryIndex: categoryIndex,
										standardIndex: RubricData.values.categories[categoryIndex]?.standards?.length,
										// arrayHelpers: arrayHelpers,
									});
								}}
							>
								<i className="fa fa-plus" />
								<span>{tr`new_standards`}</span>{" "}
							</div>
							<div className="flex flex-col gap-[5px]">
								<div className="flex gap-[5px] items-center">
									<i className="fa-solid fa-copy" />
									<span>{tr`duplicate_standard`} :</span>
								</div>
								<RAllStandards arrayHelpers={arrayHelpers} />
							</div>
						</div>
					</PopoverContent>
				</Popover>
			)}
		</FieldArray>
	);
};

export default RNewStandards;
