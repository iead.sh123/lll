import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import styles from "../Rubric.module.scss";
import RButton from "components/Global/RComs/RButton";
import iconsFa6 from "variables/iconsFa6";
import tr from "components/Global/RComs/RTranslator";
import { RLabel } from "components/Global/RComs/RLabel";
import { Table } from "reactstrap";
import RUsers from "./RUsers";

const RGradeStudentByStudent = ({ data, users, handleExportStudentAsPdf, loadingPdf, userIndex, setUserIndex }) => {
	return (
		<RFlex className={styles.grade__student}>
			<RFlex className={styles.grade__student__header}>
				<RFlex className={styles.grade__student__top__header}>
					<p>{data.title}</p>
					<RButton
						text={tr`export_as_pdf`}
						onClick={() => handleExportStudentAsPdf()}
						color="primary"
						faicon={iconsFa6.pdf}
						loading={loadingPdf}
						disabled={loadingPdf}
					/>
				</RFlex>
				<RFlex className={styles.grade__students}>
					<RUsers users={users} userIndex={userIndex} setUserIndex={setUserIndex} />
				</RFlex>
			</RFlex>

			{data.categories &&
				data.categories.length > 0 &&
				data.categories.map((category, index) => (
					<section key={category.id}>
						<section className={styles.r__collapse}>{category?.subject}</section>{" "}
						<Table bordered hover className="mb-0 cursor-pointer">
							<thead>
								<tr>
									<th style={{ width: "320px" }}>
										{tr`standards`} ({category?.standards?.length})
									</th>
									<th style={{ textAlign: "center" }}>{tr`ratings`}</th>
								</tr>
							</thead>

							<tbody>
								{category &&
									category.standards &&
									category.standards.length > 0 &&
									category.standards.map((standard, standardIndex) => {
										return (
											<tr key={standard.id ? standard.id : standard.fakeId}>
												<td style={{ fontWeight: "bold", width: "320px" }}>
													<RFlex styleProps={{ alignItems: "center", color: "#818181" }}>
														<span>{standard.subject?.length > 30 ? standard.subject.substring(0, 30) + "..." : standard.subject}</span>
														{standard.description && (
															<RLabel key={standard.id} value={standard.description} icon={iconsFa6.info} lettersToShow={0} />
														)}
													</RFlex>
												</td>
												<td style={{ padding: "0px" }}>
													<Table className="mb-0">
														<tbody>
															<tr>
																{standard?.ratings.map((rate, ind, array) => (
																	<td
																		key={rate.id}
																		style={{
																			color: "#818181",
																			border: "0px",
																			borderRight: ind !== array.length - 1 ? "1px solid #dee2e6" : "0px",
																		}}
																	>
																		<div style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
																			{rate.from_score ? <span>{rate.from_score}</span> : ""}
																			{rate.to_score ? <span>{rate.to_score}</span> : ""}
																			<span>{rate.title}</span>
																		</div>
																	</td>
																))}
															</tr>
														</tbody>
													</Table>
												</td>
											</tr>
										);
									})}
							</tbody>
						</Table>
					</section>
				))}
		</RFlex>
	);
};

export default RGradeStudentByStudent;
