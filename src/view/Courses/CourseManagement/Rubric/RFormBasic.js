import React, { useContext, useEffect, useRef } from "react";
import { RubricContext } from "logic/Courses/Rubrics/GRubricEditor";
import { getInputClass } from "utils/getInputClass";
import RDescription from "components/Global/RComs/RDescription/RDescription";
import RNewInput from "components/RComponents/RNewInput/RNewInput";
import RAddTags from "components/RComponents/RAddTags";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RFormBasic = () => {
	const inputRef = useRef(null);
	const RubricData = useContext(RubricContext);
	const sumPoints = RubricData.formProperties?.values?.categories
		?.flatMap((category) => category.standards)
		?.map((standard) => +standard?.max_points)
		?.reduce((total, points) => +total + +points, 0);

	// useEffect(() => {
	// 	if (inputRef.current) {
	// 		inputRef.current.focus();
	// 	}
	// }, []);
	// console.log("RubricData.inputRef", inputRef?.current);

	return (
		<RFlex className={"flex-col w-[75%]"}>
			<RFlex className="gap-[66px]">
				<RFlex className="gap-2 items-end ">
					<RFlex className="w-[80%] flex-col gap-1">
						<RNewInput
							// ref={inputRef}
							name="title"
							value={RubricData.formProperties?.values?.title}
							onChange={RubricData.formProperties?.handleChange}
							onBlur={RubricData.formProperties?.handleBlur}
							placeholder={tr("rubric_title")}
							className={getInputClass(RubricData?.formProperties.touched, RubricData?.formProperties.errors, "title")}
						/>
						{/* <FormText color="danger">{formProperties?.errors?.description}</FormText> */}
					</RFlex>

					<div className="w-[30%]">
						<RAddTags
							dataFromBackend={RubricData.formProperties.values.tags}
							parentCallBack={(data) => RubricData.formProperties.setFieldValue("tags", data)}
						/>
					</div>
				</RFlex>

				{RubricData.formProperties?.values?.pointed && (
					<RFlex className="items-end gap-2">
						<label className="text-themeBoldGrey">{tr("total_points")}</label>
						<span>{sumPoints ? sumPoints : 0}</span>{" "}
					</RFlex>
				)}
				<RFlex className="w-[30%] items-end gap-2">
					<RFlex className="items-center">
						<label className="text-themeBoldGrey">{RubricData.values.pointed ? tr`pointed` : tr`unpointed`}</label>
						<span
							className="text-themePrimary cursor-pointer text-xs"
							onClick={(event) => RubricData.handleChangePointStatus(event, RubricData.setFieldValue, RubricData.values)}
						>
							{RubricData.values.pointed ? tr`change_to_unpointed` : tr`change_to_pointed`}
						</span>
					</RFlex>
				</RFlex>
			</RFlex>

			<RDescription
				value={RubricData.values?.description}
				handleChange={(data) => RubricData.setFieldValue("description", data)}
				error={RubricData.errors.description}
				openCollapseValue={RubricData.values?.openDescriptionCollapse}
				collapseToggle={() => RubricData.setFieldValue("openDescriptionCollapse", !RubricData.values.openDescriptionCollapse)}
			/>
		</RFlex>
	);
};

export default RFormBasic;
