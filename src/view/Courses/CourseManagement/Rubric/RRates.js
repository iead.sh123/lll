import React, { useContext } from "react";
import { Input, FormGroup, FormText } from "reactstrap";
import { RubricContext } from "logic/Courses/Rubrics/GRubricEditor";
import { primaryColor } from "config/constants";
import { dangerColor } from "config/constants";
import { FieldArray } from "formik";
import { Textarea } from "ShadCnComponents/ui/textarea";
import iconsFa6 from "variables/iconsFa6";
import styles from "./Rubric.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RRates = ({ categoryData }) => {
	const RubricData = useContext(RubricContext);
	const pathVal = RubricData.values.categories[categoryData.categoryIndex]?.standards?.[categoryData.standardIndex];
	const pathErr = RubricData?.errors?.categories?.[categoryData?.categoryIndex]?.standards?.[categoryData?.standardIndex];

	return (
		<FieldArray name={`categories.${categoryData.categoryIndex}.standards.${categoryData.standardIndex}.ratings`}>
			{(arrayHelpers) => {
				return (
					<RFlex className="flex-col">
						<label>{tr("rates")}</label>
						<RFlex className={styles.rubric__editor__rate}>
							{pathVal?.ratings?.map((rate, ratingIndex) => {
								return (
									<RFlex key={ratingIndex} className="flex-col">
										<RFlex className="w-full items-baseline">
											<RFlex className="w-full">
												<FormGroup className="mb-0 w-[30%]">
													<Input
														name={`categories.${categoryData.categoryIndex}.standards.${categoryData.standardIndex}.ratings.${ratingIndex}.title`}
														value={rate?.title}
														onChange={RubricData.handleChange}
														className={pathErr?.ratings?.[ratingIndex]?.title ? "input__error" : ""}
													/>
													<FormText color="danger">{pathErr?.ratings?.[ratingIndex]?.title}</FormText>
												</FormGroup>

												{/* range  [ pointed ] */}
												{RubricData.values.pointed && (
													<FormGroup className="mb-0">
														<Input
															type="number"
															name={`categories.${categoryData.categoryIndex}.standards.${categoryData.standardIndex}.ratings.${ratingIndex}.from_score`}
															value={rate?.from_score ?? null}
															onChange={RubricData.handleChange}
															placeholder={tr`from_score`}
															min={0}
															className={pathErr?.ratings?.[ratingIndex]?.from_score ? "input__error" : ""}
														/>
														<FormText color="danger">{pathErr?.ratings?.[ratingIndex]?.from_score}</FormText>
													</FormGroup>
												)}

												{RubricData?.values?.pointed && pathVal?.is_range && (
													<FormGroup className="mb-0">
														<Input
															type="number"
															name={`categories.${categoryData.categoryIndex}.standards.${categoryData.standardIndex}.ratings.${ratingIndex}.to_score`}
															value={rate?.to_score ?? null}
															onChange={RubricData.handleChange}
															placeholder={tr`to_score`}
															min={0}
															className={pathErr?.ratings?.[ratingIndex]?.to_score ? "input__error" : ""}
														/>
														<FormText color="danger">{pathErr?.ratings?.[ratingIndex]?.to_score}</FormText>
													</FormGroup>
												)}
											</RFlex>
											<i
												className={iconsFa6.delete + " cursor-pointer"}
												style={{ color: dangerColor }}
												onClick={() => arrayHelpers.remove(ratingIndex)}
											/>
										</RFlex>

										<Textarea
											name={`categories.${categoryData.categoryIndex}.standards.${categoryData.standardIndex}.ratings.${ratingIndex}.description`}
											onChange={RubricData.handleChange}
											value={rate?.description}
											placeholder="Type your message here."
										/>
									</RFlex>
								);
							})}
						</RFlex>
						<RFlex
							styleProps={{ color: primaryColor, alignItems: "center", width: "fit-content", cursor: "pointer" }}
							onClick={() => {
								// arrayHelpers.push({
								// 	fakeId: Math.random(),
								// 	title: "",
								// 	score: null,
								// });
								arrayHelpers.push({
									fakeId: Math.random(),
									description: "",
									title: "",
									from_score: null,
									to_score: null,
								});
							}}
						>
							<i className={iconsFa6.plus} />
							<span>{tr`new_rate`}</span>
						</RFlex>
					</RFlex>
				);
			}}
		</FieldArray>
	);
};

export default RRates;
