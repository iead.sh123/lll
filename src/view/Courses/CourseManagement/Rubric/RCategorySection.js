import React, { useContext } from "react";
import { RubricContext } from "logic/Courses/Rubrics/GRubricEditor";
import { dangerColor } from "config/constants";
import RHoverInput from "components/Global/RComs/RHoverInput/RHoverInput";
import iconsFa6 from "variables/iconsFa6";
import styles from "./Rubric.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RCategorySection = ({ arrayHelpers, categoryIndex, isOpen, CategoryId }) => {
	const RubricData = useContext(RubricContext);

	return (
		<section
			className={styles.r__collapse + " cursor-pointer"}
			onClick={(event) => {
				event.stopPropagation();
				RubricData.collapsesCategoryToggle(CategoryId);
			}}
		>
			<div className="flex gap-6 items-center">
				<RHoverInput
					handleInputChange={RubricData.handleChange}
					handleOnBlur={(event) => {
						event.stopPropagation();
						!RubricData.formProperties?.errors.categories?.[categoryIndex]?.["subject"] &&
							RubricData.formProperties.setFieldValue(
								`categories.${categoryIndex}.${"saved"}`,
								!RubricData.values.categories[categoryIndex]["saved"]
							);
					}}
					handleEditFunctionality={(event) => {
						event.stopPropagation();
						RubricData.formProperties.setFieldValue(
							`categories.${categoryIndex}.${"saved"}`,
							!RubricData.values.categories[categoryIndex]["saved"]
						);
					}}
					name={`categories.${categoryIndex}.${"subject"}`}
					inputValue={RubricData.values.categories[categoryIndex]["subject"]}
					type={"text"}
					focusOnInput={true}
					saved={RubricData.values.categories[categoryIndex]["saved"]}
					selectOnInput={true}
					needToHover={false}
					inputIsNotValidate={RubricData.formProperties?.errors.categories?.[categoryIndex]?.["subject"]}
				/>
				{!isOpen && (
					<span className="flex gap-1 text-themeWarning font-bold">
						{RubricData.values.categories[categoryIndex]?.standards?.length}
						<span>{tr`standards`}</span>
					</span>
				)}
			</div>
			<RFlex>
				{RubricData.isHovered == "hover" + CategoryId ? (
					<i
						className={iconsFa6.delete + " cursor-pointer"}
						style={{ color: dangerColor }}
						onClick={() => arrayHelpers.remove(categoryIndex)}
					/>
				) : (
					""
				)}
				<i className={`fa ${isOpen ? iconsFa6.chevronDown : iconsFa6.chevronRight} cursor-pointer`} />
			</RFlex>
		</section>
	);
};

export default RCategorySection;
