import React from "react";
import RRates_1 from "./RRates_1";
import styles from "./Rubric.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Input, FormGroup, FormText } from "reactstrap";
import { Textarea } from "ShadCnComponents/ui/textarea";
import { Checkbox } from "ShadCnComponents/ui/checkbox";

const RStandard_1 = ({ formProperties }) => {
	const pathVal = formProperties?.values;
	const pathErr = formProperties?.errors;
	const pathTouch = formProperties;

	return (
		<RFlex className="flex-col">
			<span className={styles.rubric__editor__standard__title}>{formProperties?.isEdit ? tr`edit_standard` : tr`create_new_standard`}</span>
			<FormGroup className="mb-0">
				<label>{tr("standard_title")}</label>
				<Input
					type="text"
					name={"subject"}
					value={pathVal?.["subject"]}
					onChange={formProperties.handleChange}
					placeholder={tr`standard_title`}
					className={pathErr?.["subject"] ? "input__error" : ""}
				/>
				<FormText color="danger">{pathErr?.["subject"]}</FormText>
			</FormGroup>
			{/* max_points  [ pointed ]*/}
			{formProperties.values.pointed && (
				<FormGroup className="mb-0">
					<label>{tr("max_point")}</label>
					<Input
						type="number"
						name={`max_points`}
						value={pathVal?.["max_points"]}
						onChange={formProperties.handleChange}
						placeholder={tr`max_point`}
						className={pathErr?.["max_points"] ? "input__error" : ""}
						min={0}
					/>
					{pathErr?.["max_points"] && <FormText color="danger">{pathErr?.["max_points"]}</FormText>}
				</FormGroup>
			)}
			{/* Description */}
			<FormGroup className="mb-0">
				<label>{tr("description")}</label>
				<Textarea
					name={"description"}
					onChange={formProperties.handleChange}
					value={pathVal?.["description"]}
					placeholder={tr`description`}
				/>
			</FormGroup>

			{/* Is Range */}
			{formProperties?.values?.pointed && (
				<RFlex className="items-center">
					<Checkbox
						onCheckedChange={(event) => {
							formProperties.setFieldValue(`is_range`, event);
							if (!event) {
								formProperties.setFieldValue(
									`ratings`,
									formProperties?.values.ratings.map((rating) => ({
										...rating,
										to_score: null,
									}))
								);
							}
						}}
						checked={pathVal?.is_range}
					/>
					<span>{tr`range`}</span>
				</RFlex>
			)}

			{/* RRates */}
			<RRates_1 formProperties={{ formProperties }} />
		</RFlex>
	);
};

export default RStandard_1;
