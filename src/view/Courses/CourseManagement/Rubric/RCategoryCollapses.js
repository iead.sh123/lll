import React, { useContext } from "react";
import { Table, Collapse, FormText } from "reactstrap";
import { RubricContext } from "logic/Courses/Rubrics/GRubricEditor";
import { FieldArray } from "formik";
import RCategorySection from "./RCategorySection";
import RNewStandards from "./RNewStandards";
import iconsFa6 from "variables/iconsFa6";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const RCategoryCollapses = ({ arrayHelpers, category, categoryIndex }) => {
	const RubricData = useContext(RubricContext);
	const isOpen = !RubricData.openedCollapses.includes(category?.id ? category?.id : category?.fakeId);
	const CategoryId = category.id ? category.id : category.fakeId;

	return (
		<section
			key={CategoryId}
			onMouseEnter={() => RubricData.actionsOnHover(CategoryId)}
			onMouseLeave={() => RubricData.actionsOnHover(CategoryId)}
		>
			<RCategorySection arrayHelpers={arrayHelpers} categoryIndex={categoryIndex} isOpen={isOpen} CategoryId={CategoryId} />

			<Collapse isOpen={isOpen}>
				<Table bordered className="mb-0">
					<thead>
						<tr>
							<th style={{ background: "white", width: "320px" }}>
								{tr`standards`} ({category?.standards?.length})
							</th>
							<th className="text-themeWarning bg-white items-center">{tr`ratings`}</th>
						</tr>
					</thead>

					<tbody>
						{category &&
							category.standards &&
							category.standards.length > 0 &&
							category.standards.map((standard, standardIndex) => {
								return (
									<tr key={standard.id ? standard.id : standard.fakeId}>
										<td
											className="font-bold w-[320px]"
											onMouseEnter={() => RubricData.actionsStandardOnHover(standard.id ? standard.id : standard.fakeId)}
											onMouseLeave={() => RubricData.actionsStandardOnHover(standard.id ? standard.id : standard.fakeId)}
										>
											<RFlex styleProps={{ alignItems: "center", justifyContent: "space-between" }}>
												<div className="flex flex-col">
													<span className="text-themeBoldGrey">
														{standard.subject?.length > 30 ? standard.subject.substring(0, 30) + "..." : standard.subject}
													</span>
													{/* subject must be unique" [ validation ] */}
													{RubricData?.formProperties?.errors?.categories?.[categoryIndex]?.standards &&
														RubricData?.formProperties?.errors?.categories?.[categoryIndex]?.standards?.[standardIndex] &&
														RubricData?.formProperties?.errors?.categories?.[categoryIndex]?.standards?.[standardIndex]?.subject && (
															<FormText color="danger">
																{RubricData?.formProperties?.errors?.categories?.[categoryIndex]?.standards?.[standardIndex]?.subject}
															</FormText>
														)}
												</div>
												{RubricData.isStandardHovered == "standardHover" + (standard.id ? standard.id : standard.fakeId) && (
													<FieldArray name={`categories[${categoryIndex}].standards`}>
														{(arrayHelpers) => {
															return (
																<RFlex>
																	<i
																		className={iconsFa6.delete + " cursor-pointer text-themeDanger"}
																		onClick={() => arrayHelpers.remove(standardIndex)}
																	/>
																	<i
																		className={iconsFa6.pen + " cursor-pointer text-themePrimary"}
																		onClick={() => {
																			RubricData.formProperties.setFieldValue("standardChildForm", standard);
																			RubricData.handleOpenStandardModal({
																				categoryIndex: categoryIndex,
																				standardIndex: standardIndex,
																			});
																		}}
																	/>
																</RFlex>
															);
														}}
													</FieldArray>
												)}
											</RFlex>
										</td>
										<td style={{ padding: "0px" }}>
											<Table className="mb-0">
												<tbody>
													<tr>
														{RubricData.values.pointed && !RubricData.standardModal ? (
															<>
																{standard?.ratings
																	.sort((a, b) => Number(a.from_score) - Number(b.from_score))
																	.map((rate, ind, array) => (
																		<td
																			key={rate.id ? rate.id : rate.fakeId}
																			className="text-themeBoldGrey"
																			style={{
																				border: "0px",
																				borderRight: ind !== array.length - 1 ? "1px solid #dee2e6" : "0px",
																			}}
																		>
																			<RFlex className="flex-col gap-0 justify-center items-center">
																				<RFlex>
																					{rate.from_score && <span>{rate.from_score}</span>}
																					{rate.to_score && <span>-</span>}
																					{rate.to_score && <span>{rate.to_score}</span>}
																				</RFlex>
																				<div className="flex gap-2 items-center">
																					<span>{rate.title}</span>
																					{RubricData?.values.pointed &&
																					RubricData?.errors?.categories?.[categoryIndex]?.standards?.[standardIndex]?.ratings?.length >
																						0 ? (
																						<i className={iconsFa6.info + " text-themeDanger"} alt="info" />
																					) : (
																						""
																					)}
																				</div>
																			</RFlex>
																		</td>
																	))}
															</>
														) : (
															<>
																{standard?.ratings.map((rate, ind, array) => (
																	<td
																		key={rate.id ? rate.id : rate.fakeId}
																		className="text-themeBoldGrey"
																		style={{
																			border: "0px",
																			borderRight: ind !== array.length - 1 ? "1px solid #dee2e6" : "0px",
																		}}
																	>
																		<div className="flex justify-center items-center">
																			<span>{rate.title}</span>
																		</div>
																	</td>
																))}
															</>
														)}
													</tr>
												</tbody>
											</Table>
										</td>
									</tr>
								);
							})}
					</tbody>
				</Table>
			</Collapse>

			<section style={{ width: "fit-content" }}>
				<RNewStandards categoryIndex={categoryIndex} />
			</section>
		</section>
	);
};

export default RCategoryCollapses;
