import React, { useContext } from "react";
import { RubricContext } from "logic/Courses/Rubrics/GRubricEditor";
import RFlex from "components/Global/RComs/RFlex/RFlex";

const RAllStandards = ({ arrayHelpers }) => {
	const RubricData = useContext(RubricContext);

	return (
		<ul style={{ padding: "5px 0px 0px 22px" }}>
			{RubricData.values.categories &&
				RubricData.values.categories.length > 0 &&
				RubricData.values.categories.map((category, categoryIndex) =>
					category.standards.map((standard, standardIndex) => {
						return (
							<RFlex key={standard.id} className="cursor-pointer items-center justify-between">
								<li
									style={{ listStyleType: "square", color: "#818181" }}
									// onClick={() =>
									// 	arrayHelpers.push({
									// 		fakeId: Math.random(),
									// 		subject: standard.subject,
									// 		max_points: standard.max_points,
									// 		is_range: standard.is_range,
									// 		description: standard.description,
									// 		ratings: standard.ratings.map((rate) => {
									// 			return {
									// 				fakeId: Math.random(),
									// 				title: rate.title,
									// 				description: rate.description,
									// 				from_score: rate.from_score,
									// 				to_score: rate.to_score,
									// 			};
									// 		}),
									// 	})
									// }
									onClick={() => {
										RubricData.formProperties.setFieldValue("standardChildForm", standard);
										RubricData.handleOpenStandardModal({
											categoryIndex: categoryIndex,
											standardIndex: standardIndex,
										});
									}}
								>
									{standard.subject?.length > 30 ? standard.subject.substring(0, 30) + "..." : standard.subject}
								</li>
							</RFlex>
						);
					})
				)}
		</ul>
	);
};

export default RAllStandards;
