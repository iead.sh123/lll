import React, { useState } from "react";
import iconsFa6 from "variables/iconsFa6";
import tr from "components/Global/RComs/RTranslator";
import { genericPath } from "engine/config";
import { useHistory, useParams } from "react-router-dom/cjs/react-router-dom.min";
import { baseURL } from "engine/config";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RDropdown from "components/RComponents/RDropdown/RDropdown";
import { Checkbox } from "ShadCnComponents/ui/checkbox";
import styles from "./Lesson.module.scss";
import RControlledAlertDialog from "components/RComponents/RContolledAlertDialog";

const Record = ({ course, record, inListerPage = true, inRightBar = false, handleToggleStatus }) => {
	// console.log(course)
	const { courseId, lessonId } = useParams();
	const [isAlertDialogOpen, setIsAlertDialogOpen] = useState(false);
	const actions = [
		{
			name: "Edit",
			onClick: () => console.log("edit"),
			component: (
				<button type="button" className="flex items-center gap-2 text-themeBoldGrey">
					<i className={iconsFa6.pencil} />
					<span>Edit</span>
				</button>
			),
		},

		{
			name: "Duplicate",
			onClick: () => console.log("Duplicate"),
			component: (
				<button type="button" className="flex items-center gap-2 text-themeBoldGrey">
					<i className={iconsFa6.copy} />
					<span>Duplicate</span>
				</button>
			),
		},
		{
			name: "Delete",
			onClick: () => setIsAlertDialogOpen(true),
			component: (
				<button type="button" className="flex items-center gap-2 text-themeDanger">
					<i className={iconsFa6.delete} />
					<span>Delete</span>
				</button>
			),
		},
	];
	const history = useHistory();

	return (
		<>
			<RFlex className="justify-between items-start">
				<RFlex className="flex-col">
					{inListerPage ? (
						<h4
							className="hover:text-themePrimary hover:underline cursor-pointer"
							onClick={() => {
								if (course) history.push(`${baseURL}/${genericPath}/course-catalog/${courseId}/course lessons/add/${record.id}`);
								else history.push(`${baseURL}/${genericPath}/course-catalog/master-course/${courseId}/lessons/add/${record.id}`);
							}}
						>
							{record.title}, {record.id}
						</h4>
					) : (
						<h4 className={`${record.id == lessonId && inRightBar ? "text-themePrimary" : ""}`}>
							{record.title}, {record.id}
						</h4>
					)}
					{!inRightBar && (
						<RFlex>
							<Checkbox checked={record.checked} className="shadow-none" disabled />
							<span className="text-xs text-themeBoldGrey">{tr`After_the_previous_lesson_is_completed`}</span>
						</RFlex>
					)}
				</RFlex>
				<RFlex className="items-center">
					{!inRightBar && (
						<button
							onClick={handleToggleStatus}
							className={`text-xs ${record.status === "Draft" ? "text-themeWarning" : "text-themeSuccess"}`}
						>
							{record.status}
						</button>
					)}
					{course && (
						<RFlex className="items-baseline text-themeBoldGrey">
							{!inRightBar && <i className={`${iconsFa6.clock} text-black`} />}
							<div>available at {record.available_at}</div>
						</RFlex>
					)}
					{(inListerPage || inRightBar) && (
						<RDropdown
							actions={actions}
							TriggerComponent={
								<button>
									<i className={iconsFa6.horizontalOptions} />
								</button>
							}
						/>
					)}
				</RFlex>
			</RFlex>
			<RControlledAlertDialog
				isOpen={isAlertDialogOpen}
				handleCloseAlert={() => setIsAlertDialogOpen(false)}
				confirmAction={() => console.log("confirm delete")}
				headerItemsPosition={"items-start"}
				cancel={{ text: tr("cancel"), className: "text-black border-themeStroke bg-background shadow-sm hover:text-black" }}
			/>
			{!inRightBar && <div className={styles.divider}></div>}
		</>
	);
};

export default Record;
