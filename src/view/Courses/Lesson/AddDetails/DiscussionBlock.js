import React, { useState } from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { Accordion, AccordionContent, AccordionItem, AccordionTrigger } from "ShadCnComponents/ui/accordion";
import iconsFa6 from "variables/iconsFa6";
import RDropdown from "components/RComponents/RDropdown/RDropdown";

const DiscussionBlock = ({ index, setComponents }) => {
	const [discussion, setDiscussion] = useState("");
	const [openItem, setOpenItem] = useState("discussion");

	const discussionOptions = [
		{
			id: 1,
			name: "discussion 1",
			onClick: () => setDiscussion("discussion 1"),
		},
		{
			id: 2,
			name: "discussion 2",
			onClick: () => setDiscussion("discussion 2"),
		},
		{
			id: 3,
			name: "discussion 3",
			onClick: () => setDiscussion("discussion 3"),
		},
	];

	const handleSave = () => {
		console.log(openItem);
		setOpenItem("");
	};
	const handleDelete = () => {
		setComponents((component) => component.filter((_, i) => i !== index));
	};

	return (
		<RFlex className="mt-3">
			<Accordion type="single" collapsible className="w-[750px]" value={openItem} onValueChange={setOpenItem}>
				<AccordionItem value="discussion">
					<AccordionTrigger
						actions
						className={`hover:no-underline pl-2 border border-themeStroke h-[44px] rounded-t-sm ${
							openItem ? "bg-themeSecondary text-themePrimary" : "bg-themeWhite text-black"
						}`}
					>
						<RFlex className="items-center">
							<i className={`${iconsFa6.users}`} />
							{discussion ? <div>{discussion}</div> : <div style={{ fontSize: "15px" }}>Discussion</div>}
						</RFlex>
					</AccordionTrigger>
					<AccordionContent className="flex flex-col p-[10px] rounded-b-sm border border-themeStroke text-themeBoldGrey">
						<div className="flex-grow">
							<RFlex className="items-center">
								<span>Discussion</span>
								<RDropdown
									TriggerComponent={
										<div className="border border-themeStroke text-themeBoldGrey rounded-sm p-2">
											{discussion ? (
												<div className="text-black">
													{discussion} <i className={iconsFa6.chevronDown} style={{ marginLeft: "10px" }} />
												</div>
											) : (
												<div className="text-themeLight">
													select a discussion
													<i className={iconsFa6.chevronDown} style={{ marginLeft: "10px" }} />
												</div>
											)}
										</div>
									}
									actions={discussionOptions}
								/>
							</RFlex>

							<div className="flex justify-end mt-auto">
								<button
									disabled={!discussion}
									className={`${discussion ? "text-themeSuccess" : "text-themeBoldGrey"} mr-4`}
									onClick={handleSave}
								>
									save
								</button>
								<button onClick={handleDelete}>
									<i className={`${iconsFa6.delete} text-themeDanger`} style={{ fontSize: "15px" }} />
								</button>
							</div>
						</div>
					</AccordionContent>
				</AccordionItem>
			</Accordion>
		</RFlex>
	);
};

export default DiscussionBlock;
