import React, { useState } from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { Accordion, AccordionContent, AccordionItem, AccordionTrigger } from "ShadCnComponents/ui/accordion";
import iconsFa6 from "variables/iconsFa6";
import RDropdown from "components/RComponents/RDropdown/RDropdown";
import { Switch } from "ShadCnComponents/ui/switch";
import { Label } from "ShadCnComponents/ui/label";

const AssignmentBlock = ({ index, setComponents }) => {
	const [assignment, setAssignment] = useState("");
	const [required, setRequired] = useState(false);
	const [openItem, setOpenItem] = useState("assignment");

	const assignmentOptions = [
		{
			id: 1,
			name: "assignment 1",
			onClick: () => setAssignment("assignment 1"),
		},
		{
			id: 2,
			name: "assignment 2",
			onClick: () => setAssignment("assignment 2"),
		},
		{
			id: 3,
			name: "assignment 3",
			onClick: () => setAssignment("assignment 3"),
		},
	];

	const handleSave = () => {
		console.log(openItem);
		setOpenItem("");
	};
	const handleDelete = () => {
		setComponents((component) => component.filter((_, i) => i !== index));
	};

	return (
		<RFlex className="mt-3">
			<Accordion type="single" collapsible className="w-[750px] rounded-sm" value={openItem} onValueChange={setOpenItem}>
				<AccordionItem value="assignment">
					<AccordionTrigger
						actions
						className={`hover:no-underline pl-2 border border-themeStroke h-[44px] rounded-t-sm ${
							openItem ? "bg-themeSecondary text-themePrimary" : "bg-themeWhite text-black"
						}`}
					>
						<RFlex className="items-center">
							<i className={`${iconsFa6.squarePen}`} />
							{assignment ? <div>{assignment}</div> : <div style={{ fontSize: "15px" }}>Assignment</div>}
						</RFlex>
					</AccordionTrigger>
					<AccordionContent className="flex flex-col p-[10px] rounded-b-sm border border-themeStroke text-themeBoldGrey">
						<div className="flex-grow">
							<div className="flex gap-[20px] flex-col">
								<RFlex className="items-center">
									<span>Assignment</span>
									<RDropdown
										TriggerComponent={
											<div className="border border-themeStroke text-themeBoldGrey rounded-sm p-2">
												{assignment ? (
													<div className="text-black">
														{assignment} <i className={iconsFa6.chevronDown} style={{ marginLeft: "10px" }} />
													</div>
												) : (
													<div className="text-themeLight">
														select a assignment
														<i className={iconsFa6.chevronDown} style={{ marginLeft: "10px" }} />
													</div>
												)}
											</div>
										}
										actions={assignmentOptions}
									/>
								</RFlex>

								<RFlex className="items-center">
									<Label>Required from students</Label>
									<Switch
										checked={required}
										onCheckedChange={(event) => {
											setRequired(event);
										}}
										className="data-[state=unchecked]:bg-themeBoldGrey data-[state=checked]:bg-themeSuccess"
									/>
								</RFlex>
							</div>
							<div className="flex justify-end mt-auto">
								<button
									disabled={!assignment}
									className={`${assignment ? "text-themeSuccess" : "text-themeBoldGrey"} mr-4`}
									onClick={handleSave}
								>
									save
								</button>
								<button onClick={handleDelete}>
									<i className={`${iconsFa6.delete} text-themeDanger`} style={{ fontSize: "15px" }} />
								</button>
							</div>
						</div>
					</AccordionContent>
				</AccordionItem>
			</Accordion>
		</RFlex>
	);
};

export default AssignmentBlock;
