import React, { useState } from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { Accordion, AccordionContent, AccordionItem, AccordionTrigger } from "ShadCnComponents/ui/accordion";
import iconsFa6 from "variables/iconsFa6";
import RDropdown from "components/RComponents/RDropdown/RDropdown";

const LessonPlanBlock = ({ index, setComponents }) => {
	const [lessonPlan, setLessonPlan] = useState("");
	const [openItem, setOpenItem] = useState("lessonPlan");

	const lessonPlanOptions = [
		{
			id: 1,
			name: "lesson plan 1",
			onClick: () => setLessonPlan("lesson plan 1"),
		},
		{
			id: 2,
			name: "lesson plan 2",
			onClick: () => setLessonPlan("lesson plan 2"),
		},
		{
			id: 3,
			name: "lesson plan 3",
			onClick: () => setLessonPlan("lesson plan 3"),
		},
	];

	const handleSave = () => {
		console.log(openItem);
		setOpenItem("");
	};
	const handleDelete = () => {
		setComponents((component) => component.filter((_, i) => i !== index));
	};

	return (
		<RFlex className="mt-3">
			<Accordion type="single" collapsible className="w-[750px]" value={openItem} onValueChange={setOpenItem}>
				<AccordionItem value="lessonPlan">
					<AccordionTrigger
						actions
						className={`hover:no-underline pl-2 border border-themeStroke h-[44px] rounded-t-sm ${
							openItem ? "bg-themeSecondary text-themePrimary" : "bg-themeWhite text-black"
						}`}
					>
						<RFlex className="items-center">
							<i className={`${iconsFa6.eyeSlashSolid}`} />
							{lessonPlan ? <div>{lessonPlan}</div> : <div style={{ fontSize: "15px" }}>Lesson Plan</div>}
						</RFlex>
					</AccordionTrigger>
					<AccordionContent className="flex flex-col p-[10px] rounded-b-sm border border-themeStroke text-themeBoldGrey">
						<div className="flex-grow">
							<RFlex className="items-center">
								<span>Lesson Plan</span>
								<RDropdown
									TriggerComponent={
										<div className="border border-themeStroke text-themeBoldGrey rounded-sm p-2">
											{lessonPlan ? (
												<div className="text-black">
													{lessonPlan} <i className={iconsFa6.chevronDown} style={{ marginLeft: "10px" }} />
												</div>
											) : (
												<div className="text-themeLight">
													select a lesson plan
													<i className={iconsFa6.chevronDown} style={{ marginLeft: "10px" }} />
												</div>
											)}
										</div>
									}
									actions={lessonPlanOptions}
								/>
							</RFlex>
							<div className="flex justify-end mt-auto">
								<button
									disabled={!lessonPlan}
									className={`${lessonPlan ? "text-themeSuccess" : "text-themeBoldGrey"} mr-4`}
									onClick={handleSave}
								>
									save
								</button>
								<button onClick={handleDelete}>
									<i className={`${iconsFa6.delete} text-themeDanger`} style={{ fontSize: "15px" }} />
								</button>
							</div>
						</div>
					</AccordionContent>
				</AccordionItem>
			</Accordion>
		</RFlex>
	);
};

export default LessonPlanBlock;

// bg-themeWhite text-black
