import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import iconsFa6 from "variables/iconsFa6";
import { Button } from "ShadCnComponents/ui/button";
import tr from "components/Global/RComs/RTranslator";

const ButtonTabs = ({ components, handleAddComponent }) => {
	let preventCrerateLessonPlan = components.includes("LessonPlan");
	return (
		<RFlex className="mt-3">
			<button onClick={() => handleAddComponent("Heading")}>Heading</button>|
			<button onClick={() => handleAddComponent("Content")}>Content</button>|
			<button onClick={() => handleAddComponent("Assignment")}>Assignment</button>|
			<button onClick={() => handleAddComponent("Discussion")}>Discussion</button>|
			<button disabled={preventCrerateLessonPlan} onClick={() => handleAddComponent("LessonPlan")}>
				Lesson plan
			</button>
		</RFlex>
	);
};

export default ButtonTabs;
