import React, { useState } from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RightBar from "./RightBar";
import Record from "../Record";
import ButtonTabs from "./ButtonTabs";
import HeadingBlock from "./HeadingBlock";
import ContentBlock from "./ContentBlock";
import AssignmentBlock from "./AssignmentBlock";
import DiscussionBlock from "./DiscussionBlock";
import LessonPlanBlock from "./LessonPlanBlock";
import iconsFa6 from "variables/iconsFa6";
import PerviousNextButtons from "../PerviousNextButtons";
const GAddLessonDetails = ({ type, lessonRecords, lesson }) => {
	const course = type == "course"
	const [components, setComponents] = useState([]);

	const handleAddComponent = (component) => {
		setComponents([...components, component]);
	};

	const renderComponent = (component, index) => {
		switch (component) {
			case "Heading":
				return <HeadingBlock index={index} />;
			case "Content":
				return <ContentBlock index={index} setComponents={setComponents} />;
			case "Assignment":
				return <AssignmentBlock index={index} setComponents={setComponents} />;
			case "Discussion":
				return <DiscussionBlock index={index} setComponents={setComponents} />;
			case "LessonPlan":
				return <LessonPlanBlock index={index} setComponents={setComponents} />;
			default:
				return null;
		}
	};
	return (
		<RFlex className="">
			<div className="basis-full md:basis-3/4 lg:basis-[70%]">
				<Record course={course} record={lesson} inListerPage={false} />
				{components.map((component, index) => renderComponent(component, index))}
				<ButtonTabs components={components} handleAddComponent={handleAddComponent} />
				<PerviousNextButtons />
			</div>
			<div className="basis-full md:basis-1/4 lg:basis-[30%]">
				<RightBar course={course} lessonRecords={lessonRecords} />
			</div>
		</RFlex>
	);
};

export default GAddLessonDetails;
