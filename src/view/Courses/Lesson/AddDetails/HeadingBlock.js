import React, { useState, useRef, useEffect } from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { Input } from "ShadCnComponents/ui/input";
import tr from "components/Global/RComs/RTranslator";

const HeadingBlock = () => {
	const [heading, setHeading] = useState("");
	const [close, setClose] = useState(false);
	const headingInputRef = useRef(null);

	const handleKeyDown = (event) => {
		if (event.key === "Enter" && heading != "") {
			event.preventDefault();
			setClose(true);
		}
	};

	useEffect(() => {
		if (headingInputRef.current) {
			headingInputRef.current.focus();
		}
	}, [heading]);

	return (
		<RFlex className="mt-3 w-[750px]">
			{close ? (
				<div className="font-semibold" style={{ fontSize: "16px" }}>{heading}</div>
			) : (
				<Input 
					ref={headingInputRef}
					name="title"
					value={heading}
					onChange={(event) => setHeading(event.target.value)}
					placeholder={tr("heading")}
					onKeyDown={handleKeyDown}
				/>
			)}
		</RFlex>
	);
};

export default HeadingBlock;
