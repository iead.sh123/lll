import React, { useState } from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { Accordion, AccordionContent, AccordionItem, AccordionTrigger } from "ShadCnComponents/ui/accordion";
import iconsFa6 from "variables/iconsFa6";
import { Switch } from "ShadCnComponents/ui/switch";
import { Label } from "ShadCnComponents/ui/label";
import RCkEditor from "components/Global/RComs/RCkEditor";

const ContentBlock = ({ index, setComponents }) => {
	const [openItem, setOpenItem] = useState("content");
	const [content, setContent] = useState("");
	const [visible, setVisible] = useState(false);
	const [openLink, setOpenLink] = useState(false);
	const [showEmbedded, setShowEmbedded] = useState(false);

	const handleSave = () => {
		console.log(openItem);
		setOpenItem("");
	};
	const handleDelete = () => {
		setComponents((component) => component.filter((_, i) => i !== index));
	};
	return (
		<RFlex className="mt-3">
			<Accordion type="single" collapsible className="w-[750px]" value={openItem} onValueChange={setOpenItem}>
				<AccordionItem value="content">
					<AccordionTrigger
						actions
						className={`hover:no-underline pl-2 border border-themeStroke h-[44px] rounded-t-sm ${
							openItem ? "bg-themeSecondary text-themePrimary" : "bg-themeWhite text-black"
						}`}
					>
						<RFlex className="items-center">
							<i className={`${iconsFa6.alignLeft}`} />
							<div style={{ fontSize: "15px" }}>Content</div>
						</RFlex>
					</AccordionTrigger>
					<AccordionContent className="flex flex-col p-[10px] rounded-b-sm border border-themeStroke text-themeBoldGrey">
						<div className="flex-grow">
							<div className="flex gap-[20px] flex-col">
								<RFlex className="items-center">
									<RCkEditor data={content} handleChange={(data) => setContent(data)} />
								</RFlex>
								<RFlex className="items-center">
									<Label>Visible to students</Label>
									<Switch
										checked={visible}
										onCheckedChange={(event) => {
											setVisible(event);
										}}
										className="data-[state=unchecked]:bg-themeBoldGrey data-[state=checked]:bg-themeSuccess"
									/>
								</RFlex>
								<RFlex className="items-center">
									<Label>Open links in new tab</Label>
									<Switch
										checked={openLink}
										onCheckedChange={(event) => {
											setOpenLink(event);
										}}
										className="data-[state=unchecked]:bg-themeBoldGrey data-[state=checked]:bg-themeSuccess"
									/>
								</RFlex>
								<RFlex className="items-center">
									<Label>Show embedded preview or media player if available</Label>
									<Switch
										checked={showEmbedded}
										onCheckedChange={(event) => {
											setShowEmbedded(event);
										}}
										className="data-[state=unchecked]:bg-themeBoldGrey data-[state=checked]:bg-themeSuccess"
									/>
								</RFlex>
							</div>
						</div>
						<div className="flex justify-end mt-auto">
							<button disabled={!content} className={`${content ? "text-themeSuccess" : "text-themeBoldGrey"} mr-4`} onClick={handleSave}>
								save
							</button>
							<button onClick={handleDelete}>
								<i className={`${iconsFa6.delete} text-themeDanger`} style={{ fontSize: "15px" }} />
							</button>
						</div>
					</AccordionContent>
				</AccordionItem>
			</Accordion>
		</RFlex>
	);
};
export default ContentBlock;
