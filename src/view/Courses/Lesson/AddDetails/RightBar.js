import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import iconsFa6 from "variables/iconsFa6";
import Record from "../Record";
import styles from "../Lesson.module.scss";
 
const RightBar = ({ course, lessonRecords }) => {
    return (
        <div style={{ marginLeft: "10px" }}>
            <RFlex className="flex justify-between">
                <RFlex className="items-center">
                    <i className={iconsFa6.book} />
                    <div style={{ fontSize: "15px" }}>Lessons</div>
                </RFlex>
                <button>Add new</button>
            </RFlex>
            <RFlex className="flex-col mt-3">
                {lessonRecords.map((record, index) => {
                    return (
                        <div key={index}>
                            <Record course={course} record={record} inListerPage={false} inRightBar={true}/>
                        </div>
                    );
                })}
            </RFlex>
            <div className={styles.divider}></div>
            <div className="mt-3 mb-3">
                <RFlex className="items-center">
                    <i className={iconsFa6.users} />
                    <div style={{ fontSize: "15px" }}>Discussions</div>
                </RFlex>
                <div className="text-themeBoldGrey mt-1">no discussions yet</div>
            </div>
            <div className={styles.divider}></div>
            <div className="mt-3 mb-3">
                <RFlex className="items-center">
                    <i className={iconsFa6.squarePen} />
                    <div style={{ fontSize: "15px" }}>Assignments</div>
                </RFlex>
                <div className="text-themeBoldGrey mt-1">no assignments yet</div>
            </div>
            <div className={styles.divider}></div>
        </div>
    );
};
 
export default RightBar;