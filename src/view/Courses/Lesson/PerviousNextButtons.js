import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import iconsFa6 from "variables/iconsFa6";
import { Button } from "ShadCnComponents/ui/button";
const PerviousNextButtons = () => {
	return (
		<RFlex className="justify-between mb-5">
			<button className="flex flex-col mt-4 text-themeBoldGrey text-xs hover:text-themePrimary group">
				<div className="flex items-baseline gap-[10px]">
					<i className={`${iconsFa6.chevronLeft}`} />
					<div className="flex items-start flex-col transition-transform duration-300 group-hover:translate-x-2">
						<span>Previous lesson</span>
						<span className="mt-1 text-black group-hover:text-themePrimary" style={{ fontSize: "10px" }}>
							[Lesson title]
						</span>
					</div>
				</div>
			</button>

			<button className="flex flex-col mt-4 text-themeBoldGrey text-xs hover:text-themePrimary group">
				<div className="flex items-baseline gap-[10px]">
					<div className="flex flex-col items-start transition-transform duration-300 group-hover:-translate-x-2">
						<span>Next lesson</span>
						<span className="mt-1 text-black group-hover:text-themePrimary" style={{ fontSize: "10px" }}>
							[Lesson title]
						</span>
					</div>
					<i className={`${iconsFa6.chevronRight}`} />
				</div>
			</button>
		</RFlex>
	);
};

export default PerviousNextButtons;
