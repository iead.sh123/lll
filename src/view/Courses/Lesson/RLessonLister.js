import React from "react";
import Record from "./Record";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";

const RLessonLister = ({ data, handlers, loading = false }) => {
	const course = data.type == "course";
	const onDragEnd = (result) => {
		const { destination, source } = result;

		if (!destination) {
			return;
		}

		if (destination.index === source.index && destination.droppableId === source.droppableId) {
			return;
		}

		const updatedRecords = Array.from(data.lessonRecords);
		const [movedRecord] = updatedRecords.splice(source.index, 1);
		updatedRecords.splice(destination.index, 0, movedRecord);

		handlers.updateLessonRecords(updatedRecords);
	};

	return (
		<DragDropContext onDragEnd={onDragEnd}>
			<Droppable droppableId="lessonRecords">
				{(provided) => (
					<div {...provided.droppableProps} ref={provided.innerRef}>
						{data.lessonRecords.map((record, index) => (
							<Draggable key={record.id} draggableId={record.id.toString()} index={index}>
								{(provided) => (
									<div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
										<Record course={course} record={record} handleToggleStatus={handlers.handleToggleStatus} />
										{/* <div className="bg-backgroundTertiary mt-3">{record.title}, {record.id}</div> */}
									</div>
								)}
							</Draggable>
						))}
						{provided.placeholder}
					</div>
				)}
			</Droppable>
		</DragDropContext>
	);
};

export default RLessonLister;
