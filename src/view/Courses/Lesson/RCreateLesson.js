import React, { useState, useRef, useEffect } from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Button } from "ShadCnComponents/ui/button";
import iconsFa6 from "variables/iconsFa6";
import { Checkbox } from "ShadCnComponents/ui/checkbox";
import { Input } from "ShadCnComponents/ui/input";

const RCreateLesson = ({ formProperties }) => {
	// const [showAddTitle, setShowAddTitle] = useState(false);
	const titleInputRef = useRef(null);
	const course = formProperties.values.type == "course";
	// console.log(course);

	const handleKeyDown = (event) => {
		if (event.key === "Enter") {
			event.preventDefault();
			formProperties.handleSubmit();
		}
	};

	const handleNewLessonClick = () => {
		formProperties.setFieldValue("title", "");
		formProperties.setFieldValue("checked", false);
		formProperties.setFieldValue("showAddTitle", true);
	};

	useEffect(() => {
		if (formProperties.values.showAddTitle && titleInputRef.current) {
			titleInputRef.current.focus();
		}
	}, [formProperties.values.showAddTitle]);

	return (
		<>
			{/* <form > */}
			<RFlex className="align-items-center">
				<Button variant="defaultWithoutShadow" onClick={handleNewLessonClick}>
					<i className={iconsFa6.plus} style={{ marginRight: "5px" }} />
					{tr`new_lesson_`}
				</Button>

				<span className="text-themeBoldGrey ml-3">{tr`order_lessons_as_you_want_students_to_see_them`}</span>
			</RFlex>
			<div
				className={`transition-transform transform ${
					formProperties.values.showAddTitle ? "translate-y-0" : "-translate-y-3"
				} duration-500 ease-out mb-3`}
			>
				{formProperties.values.showAddTitle && (
					<RFlex className="justify-between">
						<RFlex className="flex-col">
							<RFlex className="items-center">
								<Input
									ref={titleInputRef}
									name="title"
									value={formProperties.values.title}
									onChange={formProperties.handleChange}
									placeholder={tr("lesson_title")}
									className={formProperties.touched.title && formProperties?.errors?.title ? "input__error" : ""}
									style={{ width: "300px" }}
									onKeyDown={handleKeyDown}
								/>
								{formProperties.touched.title && formProperties.errors.title && (
									<span className="text-themeDanger">{formProperties.errors.title}</span>
								)}
							</RFlex>

							<RFlex>
								<Checkbox
									onCheckedChange={(event) => {
										formProperties.setFieldValue("checked", event);
									}}
									checked={formProperties.values.checked}
									onKeyDown={handleKeyDown}
									className="shadow-none"
								/>
								<span className="text-xs text-themeBoldGrey">{tr`After_the_previous_lesson_is_completed`}</span>
							</RFlex>
						</RFlex>
						{course && (
							<RFlex className="items-baseline text-themeBoldGrey">
								<i className={`${iconsFa6.clock} text-black`} />
								<div>available at</div>
							</RFlex>
						)}
					</RFlex>
				)}
			</div>
			{/* </form> */}
		</>
	);
};

export default RCreateLesson;
