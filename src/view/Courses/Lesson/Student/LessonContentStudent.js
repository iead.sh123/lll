import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import Heading from "./ContentBlock/Heading";
import Assignment from "./ContentBlock/Assignment"
import Discussion from "./ContentBlock/Discussion" 
import Content from "./ContentBlock/Content";
const LessonContentStudent = ({ lesson }) => {
	return (
		<RFlex className="flex-col w-[72%] gap-[20px] mt-3">
			<Heading heading={lesson.heading} />
            <Assignment assignment={lesson.assignment} />
            <Heading heading="Share your opinion in the discussion bellow" />
            <Discussion discussion={lesson.discussion} />
            <Content content={{}} />
            <Heading heading="End of today’s lesson. good luck!" />
		</RFlex>
	);
};

export default LessonContentStudent;
