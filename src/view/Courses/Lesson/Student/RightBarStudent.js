import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import iconsFa6 from "variables/iconsFa6";
import RecordLessonStudent from "./RecordLessonStudent";
import styles from "../Lesson.module.scss";

const RightBarStudent = ({ lessonRecords }) => {
	return (
		<div style={{ marginLeft: "10px" }}>
			<RFlex className="flex-col">
				<div style={{ fontSize: "15px" }}>My progress</div>
				<div className="flex gap-[3px] text-themeBoldGrey">
					6 hours left to
					<span className="font-bold !normal-case">
						[discussion]  
					</span>
					<span className="text-themeDanger !normal-case"> to overdue</span>
				</div>
				<div className="flex gap-[3px] text-themeBoldGrey">
					you didn’t submit <span className="font-bold !normal-case"> [assignment title]</span> yet
				</div>
			</RFlex>
			<div className={styles.divider}></div>
			<RFlex className="items-center mt-3">
				<i className={iconsFa6.book} />
				<div style={{ fontSize: "15px" }}>Lessons</div>
			</RFlex>
			<RFlex className="flex-col mt-2">
				{lessonRecords.map((record, index) => {
					return (
						<div key={index}>
							<RecordLessonStudent record={record} inListerPage={false} />
						</div>
					);
				})}
			</RFlex>
			<div className={styles.divider}></div>
			<div className="mt-3 mb-3">
				<RFlex className="items-center">
					<i className={iconsFa6.users} />
					<div style={{ fontSize: "15px" }}>Discussions</div>
				</RFlex>
				<div className="text-themeBoldGrey mt-1">[discussion name here]</div>
			</div>
			<div className={styles.divider}></div>
			<div className="mt-3 mb-3">
				<RFlex className="items-center">
					<i className={iconsFa6.squarePen} />
					<div style={{ fontSize: "15px" }}>Assignments</div>
				</RFlex>
				<div className="text-themeBoldGrey mt-1">[assignment name here]</div>
			</div>
			<div className={styles.divider}></div>
		</div>
	);
};

export default RightBarStudent;
