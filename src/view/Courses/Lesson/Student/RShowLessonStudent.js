import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RightBarStudent from "./RightBarStudent";
// import HeadingBlock from "./HeadingBlock";
// import ContentBlock from "./ContentBlock";
// import AssignmentBlock from "./AssignmentBlock";
// import DiscussionBlock from "./DiscussionBlock";
// import LessonPlanBlock from "./LessonPlanBlock";
import PerviousNextButtons from "../PerviousNextButtons";
import LessonContentStudent from "./LessonContentStudent"
const RShowLessonStudent = ({ lessonRecords, lesson }) => {
	return (
		<RFlex className="">
			<div className="basis-full md:basis-3/4 lg:basis-[70%]">
				<RFlex className="justify-between items-start text-themeBoldGrey">
					<div className="flex gap-[15px] flex-col">
						<h4>{lesson.title}</h4>
					</div>
					<div>{lesson.available_at}</div>
				</RFlex>
                <LessonContentStudent lesson={lesson}/>
				<PerviousNextButtons />
			</div>
			<div className="basis-full md:basis-1/4 lg:basis-[30%]">
				<RightBarStudent lessonRecords={lessonRecords} />
			</div>
		</RFlex>
	);
};

export default RShowLessonStudent;
