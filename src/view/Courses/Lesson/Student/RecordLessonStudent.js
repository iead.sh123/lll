import React, { useState } from "react";
import { genericPath } from "engine/config";
import { useHistory, useParams } from "react-router-dom/cjs/react-router-dom.min";
import { baseURL } from "engine/config";
import RFlex from "components/Global/RComs/RFlex/RFlex";

const RecordLessonStudent = ({ record, inListerPage = true }) => {
	const { courseId, lessonId } = useParams();
	const history = useHistory();
	return (
		<>
			<RFlex className="justify-between items-start text-themeBoldGrey width-[70%]">
				<div className="flex gap-[15px] flex-col">
					{inListerPage ? (
						<h4
							className="hover:text-themePrimary hover:underline cursor-pointer"
							onClick={() => history.push(`${baseURL}/${genericPath}/course-catalog/${courseId}/student lessons/show/${record.id}`)}
						>
							{record.title}
						</h4>
					) : (
                        <h4 className={`${record.id == lessonId ? "text-themePrimary" : ""}`}>{record.title}</h4>
					)}
					
				</div>
                <div>
                    {record.available_at}
                </div>
			</RFlex>
		</>
	);
};

export default RecordLessonStudent;
