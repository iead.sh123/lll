import React from "react";
import RecordLessonStudent from "./RecordLessonStudent";

const RLessonListerStudent = ({ data, loading = false }) => {
	return (
		<>
			{data.map((record) => (
				<div key={record.id}>
					<RecordLessonStudent record={record} />
				</div>
			))}
		</>
	);
};

export default RLessonListerStudent;
