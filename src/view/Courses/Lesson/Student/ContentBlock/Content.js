import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import iconsFa6 from "variables/iconsFa6";

const Content = ({ content }) => {
	return (
		<RFlex className="flex-col gap-[5px] p-[10px] border border-themeStroke rounded-sm w-[600px] h-[320px] overflow-auto">
			<div className="text-[17px] font-semibold">History</div>
			<div>
				History User experience design is a conceptual design discipline rooted in human factors and ergonomics. This field, since the late
				1940s, has focused on the interaction between human users, machines, and contextual environments to design systems that address the
				user's experience. User experience became a positive insight for designers in the early 1990s with the proliferation of workplace
				computers. Aseel Farraj, a professor and researcher in design, usability, and cognitive science, coined the term "user experience"
				and brought it to a wider audience. Aseel Farraj, a professor and researcher in design, usability, and cognitive science, coined the
				term "user experience" and brought it to a wider audience. I invented the term because I thought human interface and usability were
				too narrow. I wanted to cover all aspects of the person's experience with the system including industrial design graphics, the
				interface, the physical interaction and the manual. Since then the term has spread widely, so much so that it is starting to lose
				its meaning. I invented the term because I thought human interface and usability were too narrow. I wanted to cover all aspects of
				the person's experience with the system including industrial design graphics, the interface, the physical interaction and the
				manual. Since then the term has spread widely, so much so that it is starting to lose its meaning. -Donald Norman
			</div>
		</RFlex>
	);
};

export default Content;
