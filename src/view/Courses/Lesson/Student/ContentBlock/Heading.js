import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";

const Heading = ({heading}) => {
	return (
		<RFlex>
			<div className="font-semibold" style={{ fontSize: "16px" }}>
				{heading}
			</div>
		</RFlex>
	);
};

export default Heading;
