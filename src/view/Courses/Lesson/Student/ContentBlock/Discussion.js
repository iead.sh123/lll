import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import iconsFa6 from "variables/iconsFa6";

const Discussion = ({ discussion }) => {
	return (
		<RFlex className="flex-col gap-[5px] p-[10px] border border-themeStroke rounded-sm">
			<RFlex className="items-center">
				<i className={`${iconsFa6.users}`} />
				<div>{discussion.title}</div>
				<div className="flex  py-[2px] px-[9px] justify-center items-center gap-[10px] rounded-[9px] bg-themeSuccess text-themeWhite">
					<span className="text-[9px]">{discussion.status}</span>
				</div>
			</RFlex>
			<span className="text-themeBoldGrey text-[10px] ml-[30px]">{discussion.availablity}</span>
		</RFlex>
	);
};

export default Discussion;
