import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import iconsFa6 from "variables/iconsFa6";

const Assignment = ({ assignment }) => {
	return (
		<RFlex className="flex-col gap-[5px] p-[10px] border border-themeStroke rounded-sm">
			<RFlex className="items-center">
				<i className={`${iconsFa6.squarePen}`} />
				<div>{assignment.title}</div>
                <span className="text-themeDanger text-[13px]">
                    {assignment.status}
                </span>
			</RFlex>
            <span className="text-themeBoldGrey text-[10px] ml-[22px]">
                {assignment.availablity}
            </span>
		</RFlex>
	);
};

export default Assignment;
