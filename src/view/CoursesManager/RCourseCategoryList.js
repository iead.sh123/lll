import React, { useState, useRef } from "react";
import { primaryColor, dangerColor } from "config/constants";
import { Form, FormGroup, Input } from "reactstrap";
import styles from "./CoursesManager.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { useHistory, useParams } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import iconsFa6 from "variables/iconsFa6";
import GCategoryAdd from "logic/CourseCatalog/GCategoryAdd";
import RBill from "components/Global/RComs/RBill";
import Helper from "components/Global/RComs/Helper";
import { useEffect } from "react";
import { Services } from "engine/services";
import { useDispatch } from "react-redux";
import { getCourseCategoriesTreeAsync } from "store/actions/global/coursesManager.action";
import RLoader from "components/Global/RComs/RLoader";
// import styles from './RCourseCategoryList.module.scss'
const RCourseCategoryList = ({
	parentId,
	courseCategories,
	level,
	handleAddCategoryToTree,
	handleRemoveCategoryFromTree,
	addEditCategoryToTreeLoading,
	formGroup,
	handleChange,
	editable = true,
	courseOnClick,
	categoryOnClick,
	addCategory,
	handleCloseAddCategory,
	categorySilentloader,
	setCategorySilentloader,
	uncategorizedCount,
}) => {
	const history = useHistory();
	const { categoryId } = useParams();
	const [openedCollapses, setOpenedCollapses] = useState(null);
	const [isHovered, setIsHovered] = useState(null);
	const [isEdited, setIsEdited] = useState(null);
	const [isAdded, setIsAdded] = useState(null);

	const [changeLoading, setChangeLoading] = useState(null);

	const inputRef = useRef(null);
	const emtpy = [];
	const addRef = useRef(null);
	const dispatch = useDispatch();
	useEffect(() => {
		if (addCategory) {
			//Helper.cl("c1111");
			//handleChange("name","")
			addRef.current.focus();
			//Helper.cl("c2222");
		}
	}, [addCategory]);

	const collapsesToggle = (id) => {
		const isOpen = "collapse" + id;
		if (isOpen == openedCollapses) {
			setOpenedCollapses("collapse");
		} else {
			setOpenedCollapses("collapse" + id);
		}
	};

	const [oldLength, setOldLength] = useState(-1);

	useEffect(() => {
		setChangeLoading(false);
		Helper.cl("tree length");
		if (oldLength == -2) {
			Helper.cl(courseCategories, "tree length 2");
			if (courseCategories[courseCategories.length - 2]?.id) {
				Helper.cl(courseCategories, "tree length 3");
				history.push(`${baseURL}/${genericPath}/courses-catalog/category/${courseCategories[courseCategories.length - 2]?.id}`);
			}
		}
		setOldLength(courseCategories.length);
	}, [courseCategories]);

	const actionsOnHover = (id) => {
		if (!editable) return;
		const isOpen = "hover" + id;
		if (isOpen == isHovered) {
			setIsHovered("hover");
		} else {
			setIsHovered("hover" + id);
		}
	};

	const editCategory = (id) => {
		const isEdit = "edit" + id;
		if (isEdit == isEdited) {
			setIsEdited("edit");
		} else {
			setIsEdited("edit" + id);
		}
	};

	const addCategory1 = (id) => {
		const isAdd = "add" + id;
		if (isAdd == isAdded) {
			setIsAdded("add");
		} else {
			setIsAdded("add" + id);
		}
	};
	useEffect(() => {
		if (isEdited?.length > 4) {
			if (inputRef?.current !== null) {
				inputRef?.current && inputRef?.current?.focus();
			}
		}
	}, [isEdited]);

	const handleFocusOnInput = () => {
		// Focus the input field when the button is clicked

		if (inputRef.current !== null) {
			inputRef.current && inputRef.current.focus();
		}
	};
	const handleKeyPress = (event) => {
		// Check if the pressed key is Enter (key code 13)

		//alert("here " +event.key);
		if (event.key === "Enter") {
			leftInput();
			event.preventDefault();
		}
	};

	const leftInput = () => {
		if (!changeLoading) {
			setChangeLoading(true);
			setTimeout(() => {
				setChangeLoading(false);
			}, 4000);
			setOldLength(-2);
			handleAddCategoryToTree();
			setIsEdited(null);
		}
	};
	return (
		<React.Fragment>
			<RFlex style={{ display: "flex", height: "84vh", flexDirection: "column", justifyContent: "space-between" }}>
				<RFlex style={{ display: "flex", flexDirection: "column" }}>
					{courseCategories && courseCategories.length > 0 ? (
						courseCategories.map((courseCategory) => {
							return (
								<>
									{courseCategory?.id ? (
										<div
											key={courseCategory.id}
											onMouseEnter={() => actionsOnHover(courseCategory.id + level)}
											onMouseLeave={() => actionsOnHover(courseCategory.id + level)}
										>
											<RFlex className={level == 1 ? `mb-2` : styles.category_name_border_vertical}>
												{/* Start another collapse style after level 1 */}
												{level !== 1 && (
													<i
														onClick={() => collapsesToggle(courseCategory.id + level)}
														className={`${
															openedCollapses === "collapse" + (courseCategory.id + level) ? iconsFa6.minusSolid : iconsFa6.plusSquare
														} ${styles.category_another_level_icon}`}
														aria-hidden="true"
														style={{ color: primaryColor }}
													/>
												)}
												{/* End another collapse style after level 1 */}
												{/* Start Edit Actions */}
												{isEdited == "edit" + (courseCategory.id + level) ? (
													<RFlex>
														{editable && (
															<Form>
																<FormGroup>
																	<Input
																		// style={{height:"32px",left:"12px"}}
																		innerRef={inputRef}
																		name="name"
																		required
																		className={styles.input__category}
																		type="text"
																		defaultValue={courseCategory.name}
																		onChange={(event) => handleChange(event.target.name, event.target.value)}
																		onBlur={leftInput} //{() => handleChange("parent_id", parentId)}
																		placeholder={tr("Program name")}
																		onKeyPress={handleKeyPress}
																	/>
																</FormGroup>
															</Form>
														)}

														{formGroup?.name == "" ? (
															<></>
														) : (
															// <i className={`${styles.close_icon} ${iconsFa6.close}`} aria-hidden="true" onClick={() => setIsEdited(null)} />
															<div>
																{editable && addEditCategoryToTreeLoading ? (
																	<i aria-hidden="true" className={`${styles.close_icon} ${iconsFa6.spinner}`} />
																) : (
																	<></>
																	// editable && (
																	// 	<RFlex
																	// 		styleProps={{
																	// 			flexDirection: "column",
																	// 			alignItems: "center",
																	// 		}}
																	// 	>
																	// 		<i className={`${styles.edit_icon} fa fa-check`} aria-hidden="true" onClick={handleAddCategoryToTree} />
																	// 		<i className={`${styles.edit_icon} fa fa-close`} aria-hidden="true" onClick={() => setIsEdited(null)} />
																	// 	</RFlex>
																	//)
																)}
															</div>
														)}
													</RFlex>
												) : (
													<div
														className="m-0 cursor-pointer"
														onClick={() => {
															if (categoryOnClick) categoryOnClick(courseCategory.id);
															else history.push(`${baseURL}/${genericPath}/courses-catalog/category/${courseCategory.id}`);
														}}
														style={{
															display: "flex",
															color: courseCategory.id == categoryId ? primaryColor : "black",
															fontWeight: courseCategory.id == categoryId ? "bold" : "normal",
															textWrap: "pretty",
															maxWidth: "166px",
														}}
													>
														{/* {courseCategory?.name?.length > 25 ? courseCategory.name?.substring(0, 25) : */}
														{courseCategory.name}
														<div style={{ marginLeft: "8px" }}>
															<RBill
																count={courseCategory?.courses?.filter((c) => c.is_master)?.length}
																fontColor="#668AD7"
																color="#F3F3F3"
															/>
														</div>
													</div>
												)}
												{/* End Edit Actions */}
												{/* Start Collapse Icon first Level */}
												{/* {level == 1 && !isEdited && (
								<React.Fragment>
									<i
										onClick={() => collapsesToggle(courseCategory.id + level)}
										className={`${
											openedCollapses === "collapse" + (courseCategory.id + level)
												? "fa-solid fa-chevron-down"
												: "fa-solid fa-chevron-right"
										} ${styles.category_icon}`}
										aria-hidden="true"
										style={{
											color: courseCategory.id == categoryId ? primaryColor : "black",
											// fontWeight:
											//   courseCategory.id == categoryId ? "bold" : "normal",
										}}
									></i>
								</React.Fragment>
							)} */}
												{/* End Collapse Icon first Level */}
												{/* Start Actions On Hover */}
												{isHovered === "hover" + (courseCategory?.id + level) && !isEdited && (
													<>
														<i
															className={`fa fa-pen ${styles.category_icon}`}
															aria-hidden="true"
															style={{ color: primaryColor }}
															onClick={() => {
																editCategory(courseCategory?.id + level);
																handleChange("id", courseCategory?.id);
																handleFocusOnInput();
															}}
														/>
														{/* <i
										className={`fa fa-plus ${styles.category_icon}`}
										aria-hidden="true"
										style={{ color: primaryColor }}
										onClick={() => {
											addCategory(courseCategory.id + level);
											openedCollapses === "collapse" + (courseCategory.id + level) ? null : collapsesToggle(courseCategory.id + level);
										}}
									/> */}
														<i
															className={`fa fa-trash-alt ${styles.category_icon}`}
															aria-hidden="true"
															style={{ color: dangerColor }}
															onClick={() => handleRemoveCategoryFromTree(courseCategory?.id, courseCategory?.can_delete)}
														/>
													</>
												)}
												{/* End Actions On Hover */}
											</RFlex>
										</div>
									) : (
										<></>
									)}
								</>
							);
						})
					) : (
						<div
							style={{
								color: "#585858",
								fontFamily: "Cairo",
								fontSize: "12px",
								fontStyle: "normal",
								fontWeight: "400",
								lineHeight: "normal",
								textTransform: "capitalize",
							}}
						>
							No program added
						</div>
					)}

					{/* // <GCategoryAdd
								// 	handleCloseAddCategory={handleCloseAddCategory}
								// 	handleAddCategoryToTree={handleAddCategoryToTree}
								// 	parentId={null}
								// 	addEditCategoryToTreeLoading={addEditCategoryToTreeLoading}
								// 	handleChange={handleChange}
								// /> */}
					{addCategory ? (
						<FormGroup>
							<Input
								style={{ height: "32px", left: "12px", width: "140px" }}
								name="name"
								required
								className={styles.input__category}
								type="text"
								// style={{visibility:?'visible':'hidden'}}
								onChange={(event) => handleChange(event.target.name, event.target.value)}
								// placeholder={parentId ? tr`sub_category_name` : tr`category_name`}
								placeholder={tr`Program name`}
								onBlur={(e) => {
									setOldLength(-2);
									handleAddCategoryToTree(e);
								}}
								innerRef={addRef}
								onKeyPress={handleKeyPress}
							/>
						</FormGroup>
					) : (
						<></>
					)}
					{changeLoading ? <RLoader mini /> : <></>}
				</RFlex>
				<RFlex
					style={{
						display: "flex",
						width: "197px",
						height: "54px",
						paddingLeft: "8px",
						cursor: "pointer",
						paddingTop: "14px",
						paddingBottom: "14px",
						alignItems: "center",
						// gap: "8px",
						borderTop: "#D7E3FD 1px solid",
						flexShrink: "0",
					}}
					onClick={() => {
						history.push(`${baseURL}/${genericPath}/courses-catalog/category/uncategorized`);
					}}
				>
					<div
						style={{
							display: "flex",
							color: "uncategorized" == categoryId ? primaryColor : "black",
							fontWeight: "uncategorized" == categoryId ? "bold" : "normal",
							textWrap: "pretty",
							maxWidth: "166px",
						}}
					>
						Courses with no program
					</div>
					{/* {Helper.js(courseCategories?.[courseCategories?.length-1],"courseCategories?.[courseCategories?.length-1]")} */}
					<div style={{ marginLeft: "8px", marginTop: "14px", marginBottom: "14px" }}>
						<RBill count={uncategorizedCount ?? "0"} color="#F3F3F3" />
					</div>
				</RFlex>
			</RFlex>
		</React.Fragment>
	);
};

export default RCourseCategoryList;
