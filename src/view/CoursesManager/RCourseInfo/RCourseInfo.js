import React, { useState, useEffect } from "react";
import { Row, Col, FormGroup, Form, Input, Label } from "reactstrap";
import { primaryColor, boldGreyColor } from "config/constants";
import { useParams, useHistory } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import { useSelector } from "react-redux";
import { greyColor } from "config/constants";
import { Services } from "engine/services";
import AppRadioButton from "components/UI/AppRadioButton/AppRadioButton";
import RDotedTitle from "components/Global/RComs/RDotedTitle/RDotedTitle";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import RSelect from "components/Global/RComs/RSelect";
import styles from "../CoursesManager.module.scss";
import moment from "moment";
import RCard from "components/Global/RComs/RCards/RCard";
import RTags from "components/Global/RComs/RTags";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import AppNewCheckbox from "components/Global/RComs/AppNewCheckbox/AppNewCheckbox";

const RCourseInfo = ({ handelCourseChange, handleCreateCourse }) => {
	const history = useHistory();
	const { categoryId, courseId } = useParams();
	const [categoryName, setCategoryName] = useState({ label: "", value: 1 });

	const {
		courseManagerTree,
		categoryAncestors,
		courseById,
		levels,
		facilitators,
		administrators,
		directors,
		courseByIdLoading,
		saveCourseLoading,
		categoryAncestorsLoading,
		courseManagerTreeLoading,
		levelsLoading,
		facilitatorsLoading,
	} = useSelector((state) => state.coursesManagerRed);

	const customFacilitatorsOption = ({ innerProps, label, data }) => {
		return (
			<div {...innerProps}>
				<img className={styles.imageFacilitators} src={data.image_hash_id} alt={data.label} width="55px" height="55px" />
				{label}
			</div>
		);
	};

	useEffect(() => {
		if (categoryId && courseManagerTree) {
			let ind = courseManagerTree.findIndex((el) => el.value == categoryId);
			if (ind !== -1) {
				setCategoryName({
					label: courseManagerTree[ind].label,
					value: courseManagerTree[ind].value,
				});
			}
		}
	}, [categoryId, courseManagerTree]);

	const levelOptions = ({ innerProps, label, data }) => {
		return (
			<RFlex styleProps={{ alignItems: "center" }} key={data.value} className="pl-3 pr-3 pt-3 cursor-pointer" {...innerProps}>
				<img src={data.image} alt={data.label} width="17px" height="20px" />
				<p>{label}</p>
			</RFlex>
		);
	};

	return (
		<React.Fragment>
			<Row>
				<Col xs={12} sm={8} className="pl-0">
					<Form>
						<Row>
							<Col xs={12} className="pl-0">
								<RDotedTitle title={tr`basic information`} />{" "}
								<Row>
									<Col xs={12} sm={9} className="mt-3 ">
										<FormGroup>
											<Label>{tr`course title`}</Label>
											<Input
												name="name"
												type="text"
												required
												placeholder={tr("course title")}
												onChange={(e) => handelCourseChange(e.target.name, e.target.value)}
												value={courseById.name}
											/>
										</FormGroup>
									</Col>

									<Col xs={12} sm={9} className="mt-3">
										<FormGroup>
											<label>{tr("tags")}</label>
											<RTags
												url={`${Services.tag_search.backend}api/v1/tag/search?prefix=`}
												getArrayFromResponse={(res) => (res && res.data && res.data.status ? res.data.data : null)}
												getValueFromArrayItem={(i) => i.id}
												getLabelFromArrayItem={(i) => i.name}
												getIdFromArrayItem={(i) => i.id}
												value={courseById?.tags && courseById?.tags.length > 0 ? courseById?.tags : []}
												onChange={(data) => handelCourseChange("tags", data)}
											/>
										</FormGroup>
									</Col>

									<Col xs={12} sm={9} className="mt-3">
										<FormGroup>
											<label>{tr("category")}</label>
											<RSelect
												option={[{ label: `--${tr`no_category`}--`, value: null }].concat(courseManagerTree)}
												closeMenuOnSelect={true}
												placeholder={tr`select_category`}
												onChange={(e) => {
													history.push(
														`${baseURL}/${genericPath}/courses-manager/editor${courseId ? `/course/${courseId}` : ""}${
															e.value !== null ? `/category/${e.value}` : ""
														}/course-information`
													);
													handelCourseChange("category_id", e.value);
												}}
												defaultValue={categoryName}
												loading={courseManagerTreeLoading}
											/>
										</FormGroup>
									</Col>

									<Col xs={12} md={6} sm={9} className="mt-3">
										<FormGroup>
											<Label>{tr`color`}</Label>
											<Input
												type="color"
												name="color"
												onChange={(e) => handelCourseChange(e.target.name, e.target.value)}
												value={courseById.color}
												defaultValue={primaryColor}
											/>
										</FormGroup>
									</Col>

									<Col xs={12} md={2} sm={9} className="mt-3" style={{ width: "20px", height: "20px" }}>
										<FormGroup>
											<Label>{tr`icon`}</Label>
											<RFileSuite
												parentCallback={(values) => {
													handelCourseChange("icon", values);
												}}
												singleFile={true}
												binary={true}
												value={courseById?.icon}
												theme="button_with_replace"
												fileType="image/*"
											/>
										</FormGroup>
									</Col>

									<Col xs={12} md={6} sm={9} className="mt-3">
										<FormGroup>
											<Label>{tr`image`}</Label>

											<RFileSuite
												parentCallback={(values) => {
													handelCourseChange("image", values);
												}}
												singleFile={true}
												binary={true}
												value={courseById?.image}
												fileType="image/*"
												widthHeight={{ checkingWHImage: true, minWidth: 308, minHeight: 160 }}
											/>
										</FormGroup>
										<p
											style={{ color: boldGreyColor }}
										>{tr`Go for something original and high quality (generic stock photos suck). You need something memorable that will resonate with your target audience.`}</p>
									</Col>
								</Row>
							</Col>
							<Col xs={12} className="mt-3 pl-0">
								<RDotedTitle title={"details"} icon={iconsFa6.calendarDay} />
								<Row>
									<Col lg={2} xs={12} className="mt-3">
										<AppNewCheckbox
											onChange={(e) => handelCourseChange("isOnline", e.target.checked)}
											label={tr`online`}
											checked={courseById?.isOnline ? true : false}
										/>
									</Col>

									{courseById.isOnline ? (
										<>
											<Col lg={5} xs={12} className="mt-3">
												<FormGroup>
													<Label>{tr`start_date`}</Label>
													<Input
														name="start_date"
														type="date"
														required
														placeholder={tr("start_date")}
														onChange={(e) => handelCourseChange(e.target.name, e.target.value)}
														value={moment(courseById.start_date).format("YYYY-MM-DD")}
													/>
												</FormGroup>
											</Col>
											<Col lg={5} xs={12} className="mt-3">
												<FormGroup>
													<Label>{tr`end_date`}</Label>
													<Input
														name="end_date"
														type="date"
														required
														placeholder={tr("end_date")}
														onChange={(e) => handelCourseChange(e.target.name, e.target.value)}
														value={moment(courseById.end_date).format("YYYY-MM-DD")}
													/>
												</FormGroup>
											</Col>
										</>
									) : (
										""
									)}
								</Row>
								<Row>
									<Col lg={3} md={6} sm={9}>
										<RFlex>
											<i className={iconsFa6.clock + " pt-1"} /> <span>{tr`number_of_hours`}</span>
										</RFlex>
									</Col>

									<Col lg={6} sm={9}>
										<FormGroup>
											<Input
												name="numberOfHours"
												type="number"
												required
												placeholder={tr("number_of_hours")}
												onChange={(e) => handelCourseChange(e.target.name, e.target.value)}
												min={1}
												value={courseById.numberOfHours}
											/>
										</FormGroup>
									</Col>

									<Col xs={12} sm={9} className="mt-3">
										<FormGroup>
											<label>{tr("languages")}</label>
											<RTags
												value={courseById?.language ?? []}
												onChange={(data) => handelCourseChange("language", data)}
												withOutSearch={true}
											/>
										</FormGroup>
									</Col>

									<Col xs={12} sm={9} className="mt-3">
										<FormGroup>
											<label>{tr("levels")}</label>
											<RSelect
												option={levels}
												closeMenuOnSelect={false}
												placeholder={tr`levels`}
												onChange={(e) =>
													handelCourseChange(
														"level_ids",
														e.map((e) => e.value)
													)
												}
												loading={levelsLoading}
												isMulti={true}
												defaultValue={
													courseId &&
													courseById?.levels?.map((level) => ({
														label: level.name,
														value: level.id,
													}))
												}
												customOption={levelOptions}
											/>
										</FormGroup>
									</Col>

									<Col xs={12} sm={9} className="mt-3">
										<FormGroup>
											<label>{tr("facilitators")}</label>
											<RSelect
												option={facilitators}
												closeMenuOnSelect={false}
												placeholder={tr`facilitators`}
												onChange={(e) =>
													handelCourseChange(
														"teachers",
														e.map((el) => {
															return {
																user_id: el.value,
																user_name: el.label,
																// user_type_id: el.userTypeId,
																image_hash_id: el.image_hash_id,
															};
														})
													)
												}
												// onChange={(e) => handelCourseChange("teachers", e)}
												loading={facilitatorsLoading}
												isMulti={true}
												defaultValue={
													courseId &&
													courseById?.teachers?.map((teacher) => ({
														label: teacher.user_name,
														value: teacher.user_id,
													}))
												}
												// customOption={customFacilitatorsOption}
											/>
										</FormGroup>
									</Col>
									<Col xs={12} sm={9} className="mt-3">
										<FormGroup>
											<label>{tr("course_administrators")}</label>
											<RSelect
												option={administrators}
												closeMenuOnSelect={false}
												placeholder={tr`course_administrators`}
												// onChange={(e) => handelCourseChange("course_administrators", e)}
												onChange={(e) =>
													handelCourseChange(
														"course_administrators",
														e.map((el) => {
															return {
																user_id: el.value,
																user_name: el.label,
																// user_type_id: el.userTypeId,
																image_hash_id: el.image_hash_id,
															};
														})
													)
												}
												loading={facilitatorsLoading}
												isMulti={true}
												defaultValue={
													courseId &&
													courseById?.course_administrators?.map((administrator) => ({
														label: administrator.user_name,
														value: administrator.user_id,
													}))
												}
												// customOption={customFacilitatorsOption}
											/>
										</FormGroup>
									</Col>
									<Col xs={12} sm={9} className="mt-3">
										<FormGroup>
											<label>{tr("curriculum_directors")}</label>
											<RSelect
												option={directors}
												closeMenuOnSelect={false}
												placeholder={tr`curriculum_directors`}
												// onChange={(e) => handelCourseChange("curriculum_directors", e)}
												onChange={(e) =>
													handelCourseChange(
														"curriculum_directors",
														e.map((el) => {
															return {
																user_id: el.value,
																user_name: el.label,
																// user_type_id: el.userTypeId,
																image_hash_id: el.image_hash_id,
															};
														})
													)
												}
												loading={facilitatorsLoading}
												isMulti={true}
												defaultValue={
													courseId &&
													courseById?.curriculum_directors?.map((director) => ({
														label: director.user_name,
														value: director.user_id,
													}))
												}
												// customOption={customFacilitatorsOption}
											/>
										</FormGroup>
									</Col>
								</Row>
							</Col>
							<Col xs={12} className="mt-3 pl-0">
								<RDotedTitle title={"cost"} icon={iconsFa6.handDollar} />
								<Row style={{ display: "flex", alignItems: "center" }} className="mt-3">
									<Col md={2} xs={6}>
										<AppRadioButton
											name="free"
											label={tr`free`}
											checked={courseById.isFree ? true : false}
											onClick={(e) => handelCourseChange("isFree", true)}
										/>
									</Col>
									<Col md={2} xs={6}>
										<AppRadioButton
											name="paid"
											label={tr`paid`}
											checked={courseById.isFree ? false : true}
											onClick={(e) => handelCourseChange("isFree", false)}
										/>
									</Col>
									{!courseById.isFree && (
										<Col md={4} xs={12}>
											<FormGroup>
												<Input
													name="prices"
													type="number"
													required
													placeholder={tr("cost")}
													onChange={(e) => handelCourseChange(e.target.name, [e.target.value])}
													value={courseById.prices?.[0]}
												/>
											</FormGroup>
										</Col>
									)}
								</Row>
								{/* {!courseById.isFree && (
									<Row>
										<Col md={8} xs={12} className="mt-3">
											<FormGroup>
												<Input
													name="discounts"
													type="number"
													required
													placeholder={tr("discount")}
													onChange={(e) => handelCourseChange(e.target.name, [e.target.value])}
													value={courseById.discounts?.[0]}
												/>
											</FormGroup>
										</Col>
									</Row>
								)} */}
							</Col>
							<Col xs={12} className="mt-3 pl-0">
								<RDotedTitle title={"course status"} icon={iconsFa6.calendarDay} />
								<Col xs={12} className="mt-3">
									<RFlex styleProps={{ flexDirection: "column" }}>
										<RFlex styleProps={{ alignItems: "center" }}>
											<AppNewCheckbox
												onChange={(e) => handelCourseChange("comingSoon", e.target.checked)}
												label={tr`coming_soon`}
												checked={courseById?.comingSoon ? courseById?.comingSoon : 0}
												idsOrderGenerator={1}
											/>
											<span
												style={{ color: greyColor }}
											>{tr`learners can see this course but can’t enroll in (no specific time to start at)`}</span>
										</RFlex>
										<RFlex styleProps={{ alignItems: "center" }}>
											<AppNewCheckbox
												onChange={(e) => handelCourseChange("isOneDayCourse", e.target.checked)}
												label={tr`one_day_course`}
												checked={courseById?.isOneDayCourse ? courseById?.isOneDayCourse : 0}
												idsOrderGenerator={2}
											/>
											<span style={{ color: greyColor }}>{tr`Accelerate learning with one-day courses that takes only some hours`}</span>
										</RFlex>
										<RFlex styleProps={{ alignItems: "center" }}>
											<AppNewCheckbox
												onChange={(e) => handelCourseChange("isPrivate", e.target.checked)}
												label={tr`private`}
												checked={courseById?.isPrivate ? courseById?.isPrivate : 0}
												idsOrderGenerator={3}
											/>
											<span style={{ color: greyColor }}>{tr`restricted-access course exclusively for organization members`}</span>
										</RFlex>
									</RFlex>
								</Col>
							</Col>
						</Row>
					</Form>

					<Col xs={12}>
						<RButton
							text={courseId ? tr`save` : tr`create`}
							onClick={handleCreateCourse}
							color="primary"
							loading={saveCourseLoading}
							disabled={saveCourseLoading ? true : false}
						/>
					</Col>
				</Col>

				<Col xs={12} md={6} lg={4}>
					<div style={{ position: "fixed", right: 25, bottom: -10 }}>
						<RCard
							course={{
								name: courseById.name,
								categoryName: categoryId ? categoryAncestors?.name : "",
								color: courseById.color,
								image: courseById.image_id == undefined ? null : `${Services.courses_manager.file}${courseById.image_id}`,
								icon: courseById.icon_id == undefined ? null : `${Services.courses_manager.file}${courseById.icon_id}`,
								startedAt: courseById.start_date ? moment(courseById.start_date).format("YYYY-MM-DD") : null,
								users: courseById.teachers,
								isOnline: courseById.isOnline,
							}}
							hiddenFlag={true}
						/>
					</div>
				</Col>
			</Row>
		</React.Fragment>
	);
};

export default RCourseInfo;
