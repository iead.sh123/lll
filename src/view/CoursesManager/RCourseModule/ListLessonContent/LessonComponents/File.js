import React, { useState, useContext } from "react";
import { CourseModuleContext } from "logic/GCoursesManager/GCourseModule/GCourseModule";
import { primaryColor } from "config/constants";
import { Row, Col } from "reactstrap";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";

const File = ({ viewMode, lessonContent, content }) => {
	const CourseModuleData = useContext(CourseModuleContext);
	const [showUploadImages, setShowUploadImages] = useState(false);

	return (
		<>
			{viewMode ? (
				<></>
			) : (
				<>
					{!showUploadImages ? (
						<Row className="text-center mt-4" style={{ color: primaryColor }}>
							<Col xs={12} sm={6}>
								<div>
									<i className="fa fa-globe fa-2x mb-3" />
									<p>{tr`pick_from_collaboration`}</p>
								</div>
							</Col>
							<Col xs={12} sm={6}>
								<div className="cursor-pointer" onClick={() => setShowUploadImages(true)}>
									<i className="fa fa-list-ul fa-2x mb-3" />
									<p>{tr`upload_from_my_device`}</p>
								</div>
							</Col>
						</Row>
					) : (
						<Row className="text-center" style={{ color: primaryColor }}>
							<Col xs={12}>
								<RFileSuite
									parentCallback={(values) => {
										CourseModuleData.handleAddContentToLessonContent(content, lessonContent, "files", values, "attachment");
									}}
									singleFile={false}
									binary={true}
									value={lessonContent?.files}
									// fileType={"*"}
									fileType={["image/*", ".docx", ".doc", ".pptx", "video/*", "text/*", "application/*", "*"]}
									fileSize={10}
								/>
							</Col>
							<Col xs={12} className="d-flex justify-content-end">
								{/* <RButton
									onClick={() => CourseModuleData.handleAddLessonContent(content, content.content_id, lessonContent, true, "attachment")}
									color="primary"
									text={tr`save_and_publish`}
									outline
									loading={CourseModuleData.lessonFileLoading}
									disabled={CourseModuleData.lessonFileLoading || lessonContent?.files?.length == 0}
								/> */}
								<RButton
									onClick={() => CourseModuleData.handleAddLessonContent(content, content.content_id, lessonContent, false, "attachment")}
									color="success"
									text={tr`save`}
									loading={CourseModuleData.lessonFileLoading}
									disabled={CourseModuleData.lessonFileLoading || lessonContent?.files?.length == 0}
								/>
							</Col>
						</Row>
					)}
				</>
			)}
		</>
	);
};

export default File;
