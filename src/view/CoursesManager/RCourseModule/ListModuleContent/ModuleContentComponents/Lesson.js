import React, { useContext } from "react";
import { CourseModuleContext } from "logic/GCoursesManager/GCourseModule/GCourseModule";
import { Form, FormGroup, Input, Row, Col, Label } from "reactstrap";
import RButton from "components/Global/RComs/RButton";
import moment from "moment";
import tr from "components/Global/RComs/RTranslator";
import DateUtcWithTz from "utils/dateUtcWithTz";

const Lesson = ({ type, handleCloseLessonModal, editMode }) => {
	const CourseModuleData = useContext(CourseModuleContext);

	return (
		<Form>
			<Row>
				<Col xs={12} className="pl-0">
					<Label md="12" className="pl-0">{tr`lesson_title`}</Label>{" "}
					<FormGroup>
						<Input
							type="text"
							name="subject"
							defaultValue={CourseModuleData.moduleContentLesson.subject}
							onChange={(e) => {
								CourseModuleData.setModuleContentLesson({
									...CourseModuleData.moduleContentLesson,
									subject: e.target.value,
								});
							}}
							autoFocus
						/>
					</FormGroup>
				</Col>
				<Col xs={12} className="pl-0">
					<Label md="12" className="pl-0">{tr`lesson_schedule`}</Label>
				</Col>
				<Col xs={6} className="pl-0">
					<FormGroup>
						<Input
							type="date"
							name="lesson_date"
							value={moment(CourseModuleData.moduleContentLesson.lesson_date).format("YYYY-MM-DD")}
							onChange={(e) => {
								CourseModuleData.setModuleContentLesson({
									...CourseModuleData.moduleContentLesson,
									lesson_date: e.target.value,
								});
							}}
						/>
					</FormGroup>
				</Col>
				<Col xs={6} className="pr-0">
					<FormGroup>
						<Input
							type="time"
							name="lesson_time"
							value={CourseModuleData.moduleContentLesson.lesson_time}
							onChange={(e) => {
								CourseModuleData.setModuleContentLesson({
									...CourseModuleData.moduleContentLesson,
									lesson_time: e.target.value,
								});
							}}
						/>
					</FormGroup>
				</Col>
				<Col xs={12} className="pl-0">
					<Label md="12" className="pl-0">{tr`end_date`}</Label>
				</Col>
				<Col xs={12} className="pl-0">
					<FormGroup>
						<Input
							type="datetime-local"
							name="end_date"
							value={moment(CourseModuleData.moduleContentLesson.end_date).format("YYYY-MM-DDTHH:mm")}
							// value={DateUtcWithTz({
							// 	dateTime: CourseModuleData.moduleContentLesson.end_date,
							// 	// dateFormate: "llll",
							// })}
							onChange={(e) => {
								CourseModuleData.setModuleContentLesson({
									...CourseModuleData.moduleContentLesson,
									end_date: e.target.value,
								});
							}}
						/>
					</FormGroup>
				</Col>

				<Col xs={12} className="d-flex justify-content-end">
					{/* <RButton
						text={tr`save_and_publish`}
						color="primary"
						onClick={() => CourseModuleData.handleCreateModuleContentLesson(type, true)}
						disabled={CourseModuleData.createModuleContentLoading}
						loading={CourseModuleData.createModuleContentLoading}
						outline
					/> */}
					<RButton
						text={editMode ? tr`save` : tr`create`}
						color="primary"
						onClick={() => CourseModuleData.handleCreateModuleContentLesson(type, false)}
						disabled={CourseModuleData.createModuleContentLoading}
						loading={CourseModuleData.createModuleContentLoading}
					/>
					<RButton text={tr`cancel`} color="primary" onClick={() => handleCloseLessonModal()} outline />
				</Col>
			</Row>
		</Form>
	);
};

export default Lesson;
