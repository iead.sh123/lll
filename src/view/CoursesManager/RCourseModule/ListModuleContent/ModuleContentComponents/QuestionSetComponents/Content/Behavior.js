import React, { useContext } from "react";
import { CourseModuleContext } from "logic/GCoursesManager/GCourseModule/GCourseModule";
import tr from "components/Global/RComs/RTranslator";
import Quizzes from "./BehaviorComponents/Quizzes";
import Polls from "./BehaviorComponents/Polls";
import Assignments from "./BehaviorComponents/Assignments";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RButton from "components/Global/RComs/RButton";

const Behavior = ({ content }) => {
	const CourseModuleData = useContext(CourseModuleContext);

	return (
		<div>
			{content?.type.toLowerCase() == "quizzes" || content?.type.toLowerCase() == "exams" ? (
				<Quizzes content={content} />
			) : content?.type.toLowerCase() == "polls" || content?.type.toLowerCase() == "surveys" ? (
				<Polls content={content} />
			) : (
				<Assignments content={content} />
			)}
			<RFlex styleProps={{ width: "100%", justifyContent: "end" }}>
				<RButton
					text={tr`save`}
					color="success"
					loading={CourseModuleData.saveBehaviorLoading}
					disabled={CourseModuleData.saveBehaviorLoading}
					onClick={() =>
						CourseModuleData.handleAddBehaviorToQuestionSetFromModuleContentAsync({
							moduleId: CourseModuleData.specificModule.id,
							moduleContentId: content.id,
							questionSetType: content?.type.toLowerCase(),
							contentId: content.content_id,
							data: content.behavior,
						})
					}
				/>
				{!content.is_published && (
					<RButton
						text={tr`publish`}
						color="link"
						onClick={() =>
							CourseModuleData.handlePublishAndUnPublishModuleContent(
								CourseModuleData.specificModule.id,
								content.id,
								content.is_published ? false : true
							)
						}
					/>
				)}
			</RFlex>
		</div>
	);
};

export default Behavior;
