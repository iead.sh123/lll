import React, { useState, useContext } from "react";
import { LiveSessionContext } from "logic/GCoursesManager/GLiveSession/GLiveSession";
import styles from "./RLiveSession.module.scss";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";
import Counter from "utils/Counter";

const RLiveSession = () => {
  const LiveSessionData = useContext(LiveSessionContext);

  return (
    <div className={styles.schedule__liveSession}>
      <RButton
        faicon={"fa-solid fa-rocket"}
        text={tr`start_the_live_session`}
        color={"success"}
        outline
        onClick={() => {
          //   LiveSessionData.startCounter();
          LiveSessionData.handleStartScheduleLiveSession();
        }}
        loading={LiveSessionData.startLiveSessionLoading}
        disabled={LiveSessionData.startLiveSessionLoading}
      />
      <p>{tr`you_have_started_at`}</p>

      <Counter
        isRunning={LiveSessionData.isRunning}
        time={LiveSessionData.time}
        setTime={LiveSessionData.setTime}
      />

      <RButton
        faicon={"fa-solid fa-rocket"}
        text={tr`end_the_recorded_session`}
        color={"danger"}
        outline
        onClick={() => {
          //   LiveSessionData.resetCounter();
          LiveSessionData.handleEndScheduleLiveSession();
        }}
        loading={LiveSessionData.endLiveSessionLoading}
      />
    </div>
  );
};

export default RLiveSession;
