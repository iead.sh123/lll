import React from "react";
import RLessonPlanHeader from "./RLessonPlanHeader";
import RLessonPlanForm from "./RLessonPlanForm";
import RLessonPlanItem from "./RLessonPlanItem/RLessonPlanItem";
import withDirection from "hocs/withDirection";
import RLessonPlanViewer from "view/Courses/CourseManagement/LessonPlan/RLessonPlanViewer";

const RLessonPlan = ({
  lessonPlan,
  lessonPlanErrors,
  isEdit,
  previewMode,
  setPreviewMode,
  handelLessonPlanChange,
  handelLessonPlanItemChange,
  handleAddNewLessonPlanItems,
  handleRemoveLessonPlanItem,
  handleClearLessonPlanItem,
  setActiveTab,
  activeTab,
  handleOpenPickARubric,
  setLessonPlanItemId,
  handleRemoveRubricFromLessonPlanItem,
  dir,
}) => {
  return (
    <>
      {!previewMode && (
        <RLessonPlanHeader
          previewMode={previewMode}
          setPreviewMode={setPreviewMode}
          lessonPlan={lessonPlan}
        />
      )}
      {previewMode ? (
        <RLessonPlanViewer
          lessonPlan={lessonPlan}
          handleBackToLessonPlanEditor={() => {}}
          previewMode={previewMode}
          setPreviewMode={setPreviewMode}
        />
      ) : (
        <>
          <RLessonPlanForm
            lessonPlan={lessonPlan}
            lessonPlanErrors={lessonPlanErrors}
            handelLessonPlanChange={handelLessonPlanChange}
            dir={dir}
          />

          <RLessonPlanItem
            lessonPlan={lessonPlan}
            lessonPlanErrors={lessonPlanErrors}
            handelLessonPlanItemChange={handelLessonPlanItemChange}
            handleAddNewLessonPlanItems={handleAddNewLessonPlanItems}
            handleRemoveLessonPlanItem={handleRemoveLessonPlanItem}
            handleClearLessonPlanItem={handleClearLessonPlanItem}
            setActiveTab={setActiveTab}
            activeTab={activeTab}
            handleOpenPickARubric={handleOpenPickARubric}
            setLessonPlanItemId={setLessonPlanItemId}
            handleRemoveRubricFromLessonPlanItem={
              handleRemoveRubricFromLessonPlanItem
            }
            dir={dir}
          />
        </>
      )}
    </>
  );
};

export default withDirection(RLessonPlan);
