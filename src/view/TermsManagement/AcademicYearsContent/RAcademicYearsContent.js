import React, { useContext } from "react";
import { TermsManagementContext } from "logic/TermsManagement/GTermsManagement";
import { Collapse } from "reactstrap";
import RAcademicYearsContentCollapses from "./RAcademicYearsContentCollapses";
import styles from "../TermsManagement.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown } from "reactstrap";
import { primaryColor, greyColor, boldGreyColor } from "config/constants";
import RTextIcon from "components/Global/RComs/RTextIcon/RTextIcon";
import iconsFa6 from "variables/iconsFa6";
import { CharactersToRandomize } from "config/constants";
import { generateRandomString } from "utils/GenerateRandomString";
import RDropdown from "components/RComponents/RDropdown/RDropdown";
import { Button } from "ShadCnComponents/ui/button";

const RAcademicYearsContent = () => {
	const TermsManagementData = useContext(TermsManagementContext);

	return (
		<RFlex className={styles.academic__years} styleProps={{ width: "100%" }}>
			{TermsManagementData?.academicYears?.map((academicYear) => (
				<RFlex key={academicYear.id} styleProps={{ flexDirection: "column" }}>
					<RAcademicYearsContentCollapses
						data={academicYear}
						type="academicYear"
						iD={academicYear.id}
						aYCollapse={TermsManagementData.openedAcademicYearsCollapses == "collapse" + `academicYear_${academicYear.id}`}
						tCollapse={false}
						gLCollapse={false}
						academicYearId={academicYear.id}
					/>
					<Collapse isOpen={TermsManagementData.openedAcademicYearsCollapses == "collapse" + `academicYear_${academicYear.id}`}>
						<RFlex styleProps={{ flexDirection: "column" }}>
							{academicYear?.terms &&
								academicYear?.terms?.length > 0 &&
								academicYear?.terms?.map((term) => (
									<RFlex key={`${academicYear.id}_${term.id}`} styleProps={{ flexDirection: "column" }}>
										<RAcademicYearsContentCollapses
											data={term}
											type="term"
											iD={`${academicYear.id}_${term.id}`}
											aYCollapse={TermsManagementData.openedAcademicYearsCollapses == "collapse" + `academicYear_${academicYear.id}`}
											tCollapse={TermsManagementData.openedTermsCollapses == "collapse" + `term_${academicYear.id}_${term.id}`}
											gLCollapse={false}
											academicYearId={academicYear.id}
											termId={term.id}
											mLeft={"20px"}
										/>

										<Collapse isOpen={TermsManagementData.openedTermsCollapses == "collapse" + `term_${academicYear.id}_${term.id}`}>
											<RFlex styleProps={{ flexDirection: "column" }}>
												<RFlex styleProps={{ justifyContent: "flex-end" }}>
													<RDropdown
														actions={[{ name: tr`from_course_catalogue`, onClick: () => TermsManagementData.handleOpenCoursesModal() }]}
														TriggerComponent={
															<button className="text-themePrimary flex gap-2 items-center">
																<span>{tr`add_courses`}</span>
																<i className={iconsFa6.chevronDown + " text-xs"} aria-hidden="true" />
															</button>
														}
													/>
												</RFlex>

												{term?.courses &&
													term?.courses?.length > 0 &&
													term?.courses?.map((course) => (
														<RFlex key={`${academicYear.id}_${term.id}_${course.id}`} styleProps={{ flexDirection: "column" }}>
															<RAcademicYearsContentCollapses
																data={course}
																type="course"
																iD={`${academicYear.id}_${term.id}_${course.id}`}
																aYCollapse={
																	TermsManagementData.openedAcademicYearsCollapses == "collapse" + `academicYear_${academicYear.id}`
																}
																tCollapse={TermsManagementData.openedTermsCollapses == "collapse" + `term_${academicYear.id}_${term.id}`}
																gLCollapse={
																	TermsManagementData.openedGradeLevelsCollapses ==
																	"collapse" + `course_${academicYear.id}_${term.id}_${course.id}`
																}
																academicYearId={academicYear.id}
																termId={term.id}
																mLeft={"40px"}
															/>
														</RFlex>
													))}
											</RFlex>
										</Collapse>
									</RFlex>
								))}

							{/* term label */}
							<RFlex styleProps={{ marginLeft: "20px" }}>
								{academicYear?.terms?.length > 0 ? (
									<span
										className={styles.add__academic__year}
										onClick={() => TermsManagementData.handleOpenTermModal(null, academicYear.id)}
									>{tr`add_term`}</span>
								) : (
									<RFlex>
										<span style={{ color: greyColor }}>{tr`no_terms_yet`}</span>
										<span
											className={styles.add__academic__year}
											onClick={() => TermsManagementData.handleOpenTermModal(null, academicYear.id)}
										>
											{tr`add_term`}
										</span>
									</RFlex>
								)}
							</RFlex>
						</RFlex>
					</Collapse>
				</RFlex>
			))}

			<RFlex className={styles.add__academic__year} onClick={() => TermsManagementData.handleOpenAcademicYearModal()}>
				<span>{tr`add_academic_year`}</span>
			</RFlex>
		</RFlex>
	);
};

export default RAcademicYearsContent;
