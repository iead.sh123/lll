import React from "react";
import RTermManagementContent from "./AcademicYearsContent/RTermManagementContent";
import RAcademicYearsContent from "./AcademicYearsContent/RAcademicYearsContent";
import RRightSideContent from "./RightSide/RRightSideContent";
import styles from "./TermsManagement.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { useWindowWidth } from "hocs/useWindowWidth";

const RTermsManagement = ({ academicPath }) => {
	const screenWidth = useWindowWidth();

	return (
		<RFlex className={styles.term__container}>
			<RFlex className={styles.term__left__side}>{academicPath ? <RAcademicYearsContent /> : <RTermManagementContent />}</RFlex>
			<RFlex className={styles.term__right__side}>
				<RRightSideContent academicPath={academicPath} />
			</RFlex>
		</RFlex>
	);
};

export default RTermsManagement;
