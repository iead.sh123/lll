import React, { useContext } from "react";
import { TermsManagementContext } from "logic/TermsManagement/GTermsManagement";
import HandsPoint from "assets/img/new/Hands_Point.png";
import styles from "../TermsManagement.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import RButton from "components/Global/RComs/RButton";
import iconsFa6 from "variables/iconsFa6";

const RTermWizard = ({ academicPath }) => {
	const TermsManagementData = useContext(TermsManagementContext);

	const ClassNameAcademicYear = TermsManagementData.academicYears?.length > 0;
	const ClassNameTerm = TermsManagementData.academicYears?.length > 0 && TermsManagementData.academicYears[0]?.terms?.length > 0;
	const ClassNameGradeLevel = TermsManagementData.activeGradeLevelInWizard;
	return (
		<RFlex styleProps={{ flexDirection: "column" }}>
			<RFlex styleProps={{ justifyContent: "center" }}>
				<img src={HandsPoint} alt="HandsPoint" width={"168px"} height={"200px"} />
			</RFlex>
			<RFlex styleProps={{ justifyContent: "center" }}>
				<RFlex className={styles.steps}>
					<div
						className={
							ClassNameAcademicYear
								? `${styles.circle__status} ${styles.circle__status__success__color}`
								: `${styles.circle__status} ${styles.circle__status__color}`
						}
					>
						{ClassNameAcademicYear ? <i className={iconsFa6.check} /> : 1}
					</div>
					<div
						className={
							ClassNameTerm
								? `${styles.circle__status} ${styles.circle__status__success__color}`
								: `${styles.circle__status} ${styles.circle__status__color}`
						}
					>
						{ClassNameTerm ? <i className={iconsFa6.check} /> : 2}
					</div>
					<div
						className={
							ClassNameGradeLevel
								? `${styles.circle__status} ${styles.circle__status__success__color}`
								: `${styles.circle__status} ${styles.circle__status__color}`
						}
					>
						{ClassNameGradeLevel ? <i className={iconsFa6.check} /> : 3}
					</div>
					<div className={styles.line}></div>
				</RFlex>
				<RFlex styleProps={{ flexDirection: "column", justifyContent: "center", gap: 20 }}>
					<div
						className={ClassNameAcademicYear ? styles.text__status__success : styles.text__status}
					>{tr`start by adding your first academic year`}</div>
					<div className={ClassNameTerm ? styles.text__status__success : styles.text__status}>{tr`add terms`}</div>
					<div className={ClassNameGradeLevel ? styles.text__status__success : styles.text__status}>
						{academicPath ? tr`add_courses` : tr`all your grade levels are here. go ahead !`}
					</div>
				</RFlex>
			</RFlex>
			{/* <RFlex styleProps={{ justifyContent: "center" }}>
				<RButton text={tr`got_it`} color="primary" outline />
			</RFlex> */}
		</RFlex>
	);
};

export default RTermWizard;
