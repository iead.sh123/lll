import React from "react";
import styles from "./RSectionMapStyle.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { faChevronRight, faChevronUp } from "@fortawesome/free-solid-svg-icons";
import { Collapse, Row, Col, Progress } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import RFlexBetween from "components/Global/RComs/RFlex/RFlexBetween";

const RSectionMapRec = ({
  items,
  openedCollapses,
  collapsesToggle,
  progress,
  lOTree,
  ind = 1,
}) => {
  return items?.map((item) => (
    <>
      <Col
        className={styles.section_map_record}
        style={{ paddingLeft: `${25 * ind}px`, display: "flex" }}
        xs={12}
        key={item.id}
        onClick={() => collapsesToggle(item?.id)}
      >
        {progress ? (
          <RFlex className={styles.r_flex}>
            {item.children?.length > 0 ? (
              <Progress
                animated
                color="success"
                value={`${
                  item?.progress[lOTree?.learner_names.map((el) => el.id)] * 100
                }`}
                style={{ borderRadius: "50%", height: "25px", width: "25px" }}
              />
            ) : (
              <>
                {item?.done &&
                item?.done[
                  lOTree?.learner_names &&
                    lOTree?.learner_names.map((el) => el.id)
                ] == true ? (
                  <div className={styles.circle_check}>
                    <i className={`fa fa-check ${styles.icon_check}`}></i>
                  </div>
                ) : (
                  <div className={styles.circle_un_check}>
                    <i className={`fa fa-close ${styles.icon_un_check}`}></i>
                  </div>
                )}
              </>
            )}
            <RFlexBetween>
              <span>
                <h6 className={styles.content_name}>{item?.content_name} </h6>
              </span>

              <div></div>
              {item?.children?.length > 0 && (
                <FontAwesomeIcon
                  icon={
                    openedCollapses === "collapse" + item?.id
                      ? faChevronUp
                      : faChevronRight
                  }
                />
              )}
            </RFlexBetween>
          </RFlex>
        ) : (
          <RFlex className={styles.r_flex}>
            <RFlexBetween>
              <span>
                <h6 className={styles.content_name}>{item?.content_name} </h6>
              </span>

              {item?.children?.length > 0 && (
                <FontAwesomeIcon
                  icon={
                    openedCollapses === "collapse" + item?.id
                      ? faChevronUp
                      : faChevronRight
                  }
                />
              )}
            </RFlexBetween>
          </RFlex>
        )}
      </Col>
      {item?.children?.length > 0 && (
        <Collapse
          key={item.id}
          isOpen={openedCollapses === "collapse" + item?.id}
          style={{ width: "100%" }}
        >
          <RSectionMapRec
            items={item.children}
            openedCollapses={openedCollapses}
            collapsesToggle={collapsesToggle}
            progress={progress}
            lOTree={lOTree}
            ind={ind + 1}
          />
        </Collapse>
      )}
    </>
  ));
};

export default RSectionMapRec;

//  lOTree?.learner_names && lOTree?.learner_names.map((el) => el.id);
