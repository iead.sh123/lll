import React from "react";
import RSectionMapRec from "./RSectionMapRec";
import styles from "./RSectionMapStyle.module.scss";
import tr from "components/Global/RComs/RTranslator";
import { Progress, Row, Col } from "reactstrap";

const RSectionMap = ({
  lOTree,
  openedCollapses,
  collapsesToggle,
  progress,
}) => {
  return (
    <Row>
      <Col md={12}>
        <section className={styles.learning_object}>
          <h6 className="pb-2">{lOTree?.content_name}</h6>
          {progress && lOTree.learner_names && (
            <>
              <Progress
                value={`${
                  lOTree?.progress[lOTree?.learner_names.map((el) => el.id)] *
                  100
                }`}
                color="success"
              />
              <h6 className="pt-2">
                {lOTree?.progress[lOTree?.learner_names.map((el) => el.id)] *
                  100}
                % {tr`is_complete`}
              </h6>
            </>
          )}
        </section>
      </Col>

      {lOTree.children?.length > 0 && (
        <RSectionMapRec
          items={lOTree.children}
          openedCollapses={openedCollapses}
          collapsesToggle={collapsesToggle}
          progress={progress}
          lOTree={lOTree}
        />
      )}
    </Row>
  );
};

export default RSectionMap;
