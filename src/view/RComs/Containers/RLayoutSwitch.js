import RTabsPanel from "components/Global/RComs/RTabsPanel";
import RColumn from "./RColumn";
import React, { useEffect } from "react";
import RRow from "./RRow";
import { SET_SECONDARY_SIDEBAR } from "store/actions/global/globalTypes";
import { SET_THIRD_SIDEBAR } from "store/actions/global/globalTypes";
import { useDispatch } from "react-redux";
import { SET_THIRD_SIDEBAR_SHOWN } from "store/actions/global/globalTypes";
import { SET_SECONDARY_SIDEBAR_SHOWN } from "store/actions/global/globalTypes";
import { Route, Switch, useParams } from "react-router-dom/cjs/react-router-dom.min";
import { routes as secondary_sidebar_routes } from "routes/secondarySidebarRoutes/withoutCurriculum";
import { online } from "engine/config";
import { SecondarySidebarRoutes } from "routes/secondarySidebarRoutes";

function RLayoutSwitch({ children, layout = "column", title = "", titles = [] }) {
	const dispatch = useDispatch();

	const tabs = () => {
		const tabs = [];

		// titles.map((t, i) => {
		//   tabs.push({ title: t, content: () => children[i] });
		// });
		children.map((c, i) => {
			tabs.push({
				title: titles && titles.length > i ? titles[i]?.title : "emptyTitle" + i,
				content: () => c,
			});
		});

		return (
			<RTabsPanel
				title={title}
				Tabs={tabs}
				SelectedIndex={0}
				changeHorizontalTabs={() => {}}
				// Url={
				//   process.env.REACT_APP_BASE_URL +
				//   `/admin/school-event/manage/${schoolEventID}/`
				// }
			/>
		);
	};

	//   const tabs = [];
	//   children.map((c, i) => {
	//     tabs.push({
	//       title:
	//         titles && titles.length > i ? titles[i]?.title : "emptyTitle" + i,
	//       content: () => c,
	//     });
	//   });

	//   return (
	//     <RSidebarPanel
	//       title={title}
	//       Tabs={tabs}
	//       SelectedIndex={0}
	//       changeHorizontalTabs={() => {}}
	//       // Url={
	//       //   process.env.REACT_APP_BASE_URL +
	//       //   `/admin/school-event/manage/${schoolEventID}/`
	//       // }
	//     />
	//   );
	const params = useParams();

	useEffect(() => {
		const ParamsArr = Object.entries(params);

		const secondary_sidebar = () => {
			dispatch({
				type: SET_SECONDARY_SIDEBAR,
				payload: {
					sidebar: titles,
					key: ParamsArr[0][0],
					value: ParamsArr[0][1],
				},
			});
			dispatch({ type: SET_SECONDARY_SIDEBAR_SHOWN, payload: { show: true } });
		};

		const third_sidebar = () => {
			dispatch({
				type: SET_THIRD_SIDEBAR,
				payload: {
					sidebar: titles,
					key: ParamsArr[1][0],
					value: ParamsArr[1][1],
				},
			});

			dispatch({ type: SET_SECONDARY_SIDEBAR_SHOWN, payload: { show: false } });
			dispatch({ type: SET_THIRD_SIDEBAR_SHOWN, payload: { show: true } });
		};
		if (layout == "secondary_sidebar") secondary_sidebar();
		else if (layout == "third_sidebar") third_sidebar();
	}, []);

	const getRoutes = (routes) => {
		return routes.map((prop, key) => {
			return <Route path={prop.layout + prop.path} component={prop.component} key={key} exact />;
		});
	};

	return (
		<div className={"RLayoutSwitch"}>
			{/* {"layout switch=" + layout} */}
			{layout == "column" ? (
				<RColumn children={children}></RColumn>
			) : layout == "row" ? (
				<RRow children={children}></RRow>
			) : layout == "tabs" ? (
				tabs()
			) : layout == "secondary_sidebar" ? (
				<Switch>{getRoutes(online ? secondary_sidebar_routes : SecondarySidebarRoutes)}</Switch>
			) : (
				<></>
			)}
		</div>
	);
}
export default RLayoutSwitch;
