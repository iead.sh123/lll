import React from "react";
function REnd({ children }) {
	return <div class={"Rend"}>{children && children.length > 0 ? children?.map((c) => <div>{c}</div>) : <div>{children}</div>}</div>;
}
export default REnd;
