import React from "react";
function RColumn({ children, style }) {
	return (
		<div className={"RColumn"} style={style}>
			{children && children.length > 0 ? children?.map((c, i) => <div key={i}>{c}</div>) : <div>{children}</div>}
		</div>
	);
}
export default RColumn;
