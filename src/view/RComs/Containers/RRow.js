import React from "react";

function RRow({ children, extraClasses, extraStyles }) {
	return (
		<div id="rrow" class={"RRow" + " " + extraClasses} style={extraStyles}>
			{children && children.length > 0 ? children?.map((c) => <div>{c}</div>) : <div style={{ width: "100%" }}>{children}</div>}
		</div>
	);
}
export default RRow;
