import React from "react";

const RContentPage = ({ children }) => {
  return (
    <div id="content-page" className="content">
      {children && children.length > 0 ? (
        children?.map((c) => <div>{c}</div>)
      ) : (
        <div>{children}</div>
      )}
    </div>
  );
};
export default RContentPage;
