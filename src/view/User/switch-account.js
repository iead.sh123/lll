import React from "react";
import tr from "components/Global/RComs/RTranslator";
import styles from "./user.module.scss";
import { Row, Col } from "reactstrap";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import hands from "assets/img/new/hands.png";
import { primaryColor } from "config/constants";
import { useDispatch, useSelector } from "react-redux";
import Helper from "components/Global/RComs/Helper";
import { Services } from "engine/services";
import { switchAccountType } from "store/actions/global/auth.actions";

import { refresh } from "store/actions/global/auth.actions";
import store from "store";

const SwitchAccount = () => {
	const dispatch = useDispatch();
	const types = useSelector((s) => s.auth.schoolsAndTypes);

	const switchType = (s) => {
		Helper.cl("switchType ");

		dispatch(switchAccountType(s.user_type__organization.user_type_id, s.organization_id));
	};
	return (
		<section>
			{/* <button onClick={()=>{dispatch(refresh({refresh_token:store.getState().auth.refresh_token}),()=>{})}}>refresh test</button> */}
			<h6 className={styles.switch__account__title}>{tr`switch_account`}</h6>
			<section className={styles.switch__account__note}>
				<RFlex styleProps={{ position: "relative" }}>
					<span className={styles.switch__account__description}>
						Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus architecto explicabo dolorem. Accusamus eaque dolorem,
						deserunt non eligendi recusandae velit laborum cumque facere! Nihil, minus. Qui delectus ut sint voluptates?
					</span>
					<img src={hands} alt="hands" className={styles.switch__account__image} />
				</RFlex>
			</section>

			<RFlex styleProps={{ flexWrap: "wrap", justifyContents: "flex-start", alignItems: "center" }}>
				{types.map((s) => (
					<section
						onClick={() => {
							switchType(s);
						}}
						className={styles.switch__account__organization__section}
					>
						<div className={styles.switch__account__organization}>
							<RFlex className={styles.switch__account__organization__content}>
								<img src={s.organization_image} alt="hands" className={styles.switch__account__organization__image} />
								<h6>{s.type}</h6>
								<h6 className="text-muted">{s.organization_name}</h6>
								<RFlex styleProps={{ color: primaryColor }}>
									<i className="fa-solid fa-clock pt-1"></i>
									<span>
										{tr`last_access`} :{s.lastAccess}
									</span>
								</RFlex>
							</RFlex>
						</div>
					</section>
				))}
			</RFlex>
		</section>
	);
};

export default SwitchAccount;
