import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { shadeRGBColor } from "utils/Colors";
import { Draggable } from "react-beautiful-dnd";

const EventCard = ({ id, text, actions, rgbColor, index, disableActions }) => {
	return (
		<Draggable key={"event_" + id} draggableId={"event_" + id} index={index} isDragDisabled={disableActions}>
			{(provided, snapshot) => {
				return (
					<div
						ref={provided.innerRef}
						{...provided.draggableProps}
						{...provided.dragHandleProps}
						style={{
							display: "flex",
							gap: "10px",
							backgroundColor: shadeRGBColor(rgbColor),
							color: rgbColor,
							justifyContent: "space-between",
							padding: "5px",
							margin: "5px",
							borderRadius: "7px",
							...provided.draggableProps.style,
						}}
					>
						<p className="mt-2">{text}</p>
						{!disableActions && (
							<div className="mt-2">
								{actions?.map((ac) => (
									<FontAwesomeIcon key={"eventcard-" + id + text + ac.id} icon={ac.icon} onClick={() => ac.onClick()} className="ml-2" />
								))}
							</div>
						)}
					</div>
				);
			}}
		</Draggable>
	);
};

export default EventCard;
