import React from 'react';
import styles from './TowLevelsAccordion.module.scss';
import AccordionCard from './AccordionCard';

import { UncontrolledCollapse } from 'reactstrap';

const TowLevelsAccordion = ({ items, level = 1}) => {
    if (!items?.length)
        return <></>;

    return <div className={styles['Accordion']}>
        {
            items.map((it,index) => <div key={'accordion-div' + it.id}>
                <AccordionCard level={level} icon={it.icon} component={it.component} collapseId={it.collapseId} style={it.style} onClick={it.onClick} draggableId={it.draggableId} index={index}/>
                {
                    it.collapseId && <UncontrolledCollapse
                        toggler={it.collapseId}
                    // isOpen={true}
                    >
                        {
                            <TowLevelsAccordion items={it?.items} level={level + 1} />
                            // it?.items?.map?.(child=>
                            // <AccordionCard level={2} key={'accordion-ch-' + child.text} icon={child.icon} text={child.text} onClick={child.onClick}/>
                            // )
                        }
                    </UncontrolledCollapse>
                }
            </div>)
        }
    </div>
}


export default TowLevelsAccordion;