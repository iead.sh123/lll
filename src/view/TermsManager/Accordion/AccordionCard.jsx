import RFlex from 'components/Global/RComs/RFlex/RFlex';
import React from 'react';
import styles from './AccordionCard.module.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronRight, faPeace } from '@fortawesome/free-solid-svg-icons';
import { Draggable, Droppable } from 'react-beautiful-dnd';


const AccordionCard = ({ level, icon, component, onClick, collapseId, style = {}, draggableId, index }) => {


    return draggableId ? <Droppable droppableId={"drop" + draggableId} isDropDisabled>
        {provided => (<div ref={provided.innerRef} {...provided.droppableProps}>
            <Draggable
                key={draggableId}
                draggableId={draggableId}
                // style={getStyle(provided.draggableProps.style, snapshot)}
                index={index}>
                {(provided, snapshot) => {
                    return (
                        <div
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                            className={styles['AccordionCardLvl' + level]}
                            style={{ ...style, ...provided.draggableProps.style }}
                            onClick={() => onClick?.()}>
                            {icon && <FontAwesomeIcon style={{ color: '#668AD7' }} icon={icon} />}
                            {component}
                            {collapseId && <FontAwesomeIcon id={collapseId} icon={faChevronRight} />}
                        </div>
                    );
                }}
            </Draggable>
            {provided.placeholder}
        </div>)}
    </Droppable> :


        <div className={styles['AccordionCardLvl' + level]} style={style} onClick={() => onClick?.()}>

            {icon && <FontAwesomeIcon style={{ color: '#668AD7' }} icon={icon} />}
            {component}
            {collapseId && <FontAwesomeIcon id={collapseId} icon={faChevronRight} />}

        </div>
}



export default AccordionCard;