import React from "react";
import iconsFa6 from "variables/iconsFa6";
import RSwitch from "components/RComponents/RSwitch";
import styles from "../../Collaboration.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { successColor } from "config/constants";

const CollaborateNow = ({ item, handlers, data }) => {
	return (
		<div>
			{Object.keys(data?.values.contentShareableIds)?.map(Number)?.includes(item.id) ? (
				<RFlex styleProps={{ justifyContent: "space-between" }}>
					<RFlex styleProps={{ color: successColor, alignItems: "center", gap: "5px" }}>
						<span style={{ fontSize: "12px" }}>{tr`shareable`}</span>
						<i className={iconsFa6.check} />
					</RFlex>
					<RFlex styleProps={{ alignItems: "center", gap: "5px" }}>
						<span style={{ fontSize: "12px" }}>{tr`require_approve`}?</span>

						<RSwitch
							onChange={() => {
								handlers.handleMakingContentShareable({ contentId: item.id });
								handlers.handleApprovalContent({ contentId: item.id });
							}}
							checked={data?.values.contentShareableIds[item.id].need_approval}
							width={32}
							height={15}
							onColor="#46c37e"
							onHandleColor="#fff"
						/>
					</RFlex>
				</RFlex>
			) : (
				<RFlex
					className={styles.collaborate}
					onClick={() => {
						handlers.handleMakingContentShareable({ contentId: item.id });
						handlers.handleMutateShareableContent({ contentId: item.id });
					}}
				>
					<span className={styles.rectangle__card__collaborate}>{tr`collaborate_now`}</span>
				</RFlex>
			)}
		</div>
	);
};

export default CollaborateNow;
