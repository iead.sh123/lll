import RDropdown from "components/RComponents/RDropdown/RDropdown";
export const ConvertRecordsToNewForm = (records) => {
	const newDataForm = [];
	const tableColumns = getColumnsFromRecords(records);
	records.forEach((record) => {
		newDataForm.push(transformRecordShape(record));
	});
	return { columns: tableColumns, data: newDataForm };
};

const transformRecordShape = (record) => {
	const newRecordForm = {};
	record.details.forEach((record, index) => {
		const finalKey = typeof record.key == "string" ? record.key : `id${index}`;
		newRecordForm[finalKey] = record.value;
	});
	record.id ? (newRecordForm["id"] = record.id) : "";
	return newRecordForm;
};
const getColumnsFromRecords = (records) => {
	if (records?.length < 0) return;
	const columns = records[0].details?.map((record, index) => ({
		accessorKey: typeof record.key == "string" ? record.key : `id${index}`,
		header: () => record.key,
		cell: ({ row }) => row.getValue(typeof record.key == "string" ? record.key : `id${index}`),
	}));
	if (records[0].actions < 0 || !records[0].actions) return columns;

	const actions = {
		id: "actions",
		header: () => "",
		cell: (info) => {
			const cell = (
				<div className="flex gap-1 items-center">
					{records[0].actions
						.filter((action) => !action.inDropdown)
						.map((action, index) => (
							<i
								key={index}
								className={`${action.icon} ${action.actionIconClass} cursor-pointer`}
								aria-label={action.name ?? "default"}
								onClick={() => action.onClick(info)}
							/>
						))}
					{records[0].actions.some((action) => action.inDropdown) && (
						<RDropdown
							actions={records[0].actions
								.filter((action) => action.inDropdown)
								.map((action, index) => ({ ...action, onClick: () => action.onClick(info) }))}
						/>
					)}
				</div>
			);
			return cell;
		},
	};
	return [...columns, actions];
};
