export const generateRandomString = (length, characterSet) => {
	let result = "";
	const charactersLength = characterSet.length;

	for (let i = 0; i < length; i++) {
		const randomIndex = Math.floor(Math.random() * charactersLength);
		result += characterSet.charAt(randomIndex);
	}

	return result;
};
