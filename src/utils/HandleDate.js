import moment from "moment";

export const showOnlyDate = (date) => {
	return format(new Date(date), "dd-MM-yyyy");
};

export const showOnlyTime = (date) => {
	return format(new Date(date), "hh:mm aaaaa'm'");
};

export const showFullDate = (date) => {
	return format(new Date(date), "yyyy-MM-dd hh:mm aaaaa'm'");
};

export const showOnlyDay = (date) => {
	return format(new Date(date), "eeee");
};

export const relativeDate = (date) => {
	const now = moment();
	const then = moment(date);

	if (
		now.diff(then, "days") < 7 ||
		(now.date() === then.date() && now.diff(then, "years") === 0) ||
		(now.date() === then.date() && now.month() === then.month() && now.diff(then, "years") > 0)
	) {
		return then.fromNow();
	}
	return then.format("YYYY/MM/DD HH:mm");
};

// export default pSBC;
