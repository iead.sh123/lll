export const shadeRGBColor = (color) => {
    let rgbaShaded  = color.split('');
    rgbaShaded.splice(rgbaShaded.indexOf('(') , 0 , 'a');

   
    rgbaShaded.splice(rgbaShaded.indexOf(')') , 0 , ',0.1');

  
    return rgbaShaded.join('');
}