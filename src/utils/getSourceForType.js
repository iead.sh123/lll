import {
	imageTypes,
	fileTypes,
	fileExcel,
	fileWord,
	filePowerPoint,
	filePdf,
	fileVideo,
	fileAudio,
	fileZip,
	powerPointIcon,
	excelIcon,
	videoIcon,
	audioIcon,
	fileIcon,
	wordIcon,
	pdfIcon,
	zipIcon,
	defaultImage,
} from "config/mimeTypes";
import { Services } from "engine/services";

const getSourceForType = ({ fileLink, url, mimeType }) => {
	console.log("fileLink_url_521251", fileLink, url);
	if (imageTypes.includes(mimeType)) {
		return fileLink ? fileLink : url ? Services.storage.file + url : defaultImage;
	} else if (fileTypes.includes(mimeType)) {
		return fileIcon;
	} else if (fileWord.includes(mimeType)) {
		return wordIcon;
	} else if (fileExcel.includes(mimeType)) {
		return excelIcon;
	} else if (filePowerPoint.includes(mimeType)) {
		return powerPointIcon;
	} else if (filePdf.includes(mimeType)) {
		return pdfIcon;
	} else if (fileVideo.includes(mimeType)) {
		return videoIcon;
	} else if (fileAudio.includes(mimeType)) {
		return audioIcon;
	} else if (fileZip.includes(mimeType)) {
		return zipIcon;
	} else {
		return fileLink ? fileLink : url ? Services.storage.file + url : defaultImage;
	}
};

export default getSourceForType;
