import SweetAlert from "react-bootstrap-sweetalert";
import tr from "../components/Global/RComs/RTranslator";
import { render, unmountComponentAtNode } from "react-dom";

let alertDiv = document.getElementById("sweetAlert-98iop76524");
let observer = null;

const waitAndRender = (elt) => {
	if (alertDiv) {
		render(elt, alertDiv);
	} else {
		alertDiv = document.getElementById("sweetAlert-98iop76524");
		if (alertDiv) {
			render(elt, alertDiv);
			return;
		}

		if (!observer)
			observer = new MutationObserver(() => {
				alertDiv = document.getElementById("sweetAlert-98iop76524");
				if (alertDiv) {
					observer.disconnect();
					observer = null;
					render(elt, alertDiv);
				}
			});

		observer.observe(document, {
			subtree: true,
			childList: true,
			characterData: true,
		});
	}
};

//inject notify

/**
 * @param {string} title .
 * @param {string} message .
 * @param { SUCCESS|WARNING|PRIMARY|DANGER|INFO } type .
 * @param {BigInteger} timeout .
 * @return {void}
 */

const notify = (title, message, type, timeout = 3000) =>
	waitAndRender(
		<SweetAlert
			success={type === SUCCESS}
			warning={type === WARNING}
			info={type === INFO}
			primary={type === PRIMARY}
			danger={type === DANGER}
			title={title}
			onConfirm={() => unmountComponentAtNode(alertDiv)}
			onCancel={() => unmountComponentAtNode(alertDiv)}
			timeout={timeout}
		>
			{message}
		</SweetAlert>
	);

//inject confirm
/**
 * @param {string} title .
 * @param {string} message .
 * @param { SUCCESS|WARNING|PRIMARY|DANGER|INFO } type .
 * @param {string} confirmButonText .
 * @param {function } onConfirm.
 * @param {function } onCancel.
 * @return {void}
 */
const confirm = (
	title,
	message,
	type,
	confirmButonText = tr("ok"),
	onConfirm,
	onCancel = () => null,
	cancelButtonText = tr("cancel"),
	confirmBtnBsStyle
) =>
	waitAndRender(
		<SweetAlert
			success={type === SUCCESS}
			warning={type === WARNING}
			info={type === INFO}
			primary={type === PRIMARY}
			danger={type === DANGER}
			// customButtons=
			showCancel
			confirmBtnText={confirmButonText}
			cancelBtnText={cancelButtonText}
			confirmBtnBsStyle={confirmBtnBsStyle ?? "primary"}
			title={title}
			onConfirm={() => {
				onConfirm();
				unmountComponentAtNode(alertDiv);
			}}
			onCancel={() => {
				onCancel();
				unmountComponentAtNode(alertDiv);
			}}
			focusCancelBtn
		>
			{message}
		</SweetAlert>
	);

//inject input
/**
 * @param {string} title .
 * @param {string} message .
 * @param { SUCCESS|WARNING|PRIMARY|DANGER|INFO } type .
 * @param {string} confirmBtnText .
 * @param {function } onConfirm.
 * @param {function } onCancel.
 * @return {void}
 */
const input = ({
	title: title,
	message: message,
	type: type,
	placeHolder: placeHolder,
	onConfirm: onConfirm,
	onCancel: onCancel = () => null,
	confirmBtnText: confirmBtnText,
	cancelBtnText: cancelBtnText,
}) =>
	waitAndRender(
		<SweetAlert
			input
			showCancel
			cancelBtnBsStyle="link"
			confirmBtnBsStyle="danger"
			confirmBtnText={confirmBtnText}
			cancelBtnText={cancelBtnText}
			placeHolder={placeHolder}
			title={title}
			onConfirm={(event) => {
				if (event.toLowerCase() == "delete") {
					onConfirm();
					unmountComponentAtNode(alertDiv);
				} else {
				}
			}}
			onCancel={() => {
				onCancel();
				unmountComponentAtNode(alertDiv);
			}}
		>
			{message}
		</SweetAlert>
	);

export const SUCCESS = "SUCCESS";
export const WARNING = "WARNING";
export const PRIMARY = "PRIMARY";
export const DANGER = "DANGER";
export const INFO = "INFO";

const Swal = { notify: notify, confirm: confirm, input: input };

export default Swal;
