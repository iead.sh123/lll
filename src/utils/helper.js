import { backendUrl, baseUrl, resourcesBaseUrl } from "config/constants";
import axios from "./Http";
import api from "../utils/Http";
import { useSelector } from "react-redux";
import Swal, { DANGER } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";
export const goToQuestionSet = () => {
	// Add an event listener
};
export const getPlaceHolderContent = async (e, rab_id, user) => {
	const response = await post(`std-api/question-set/place-holder/` + e.detail.qid + "/" + rab_id);

	if (response.data.status == 1) {
		let type = response.data.data["type"];
		switch (type) {
			case "qs-linked":
				let patternId = response.data.data.models["id"];
				if (user === "teacher") {
					window.open(`${process.env.REACT_APP_BASE_URL}/teacher/courses/pattern/${patternId}`);
				} else {
					if (user === "student") {
						window.open(`${process.env.REACT_APP_BASE_URL}/questionset/course/${rab_id}/pattern/${patternId}`);
					} else {
						toast.error(tr`You Are Not Allowed !`);
					}
				}

				break;
			case "qs":
				let qsPatternId = response.data.data.models["id"];
				if (user === "teacher") {
					window.open(`${process.env.REACT_APP_BASE_URL}/teacher/courses/pattern/${qsPatternId}`);
				} else {
					if (user === "student") {
						window.open(`${process.env.REACT_APP_BASE_URL}/student/courses/pattern/${qsPatternId}`);
					} else {
						toast.error(tr`You Are Not Allowed !`);
					}
				}
				break;

			case "file":
				let url = response.data.data.models;
				window.open(`${resourcesBaseUrl}/${url}`);
				break;
			case "link":
				url = response.data.data.models;
				window.open(`${url}`);
				break;
			default:
				break;
		}
	} else {
		toast.error(response.data.msg);
	}
};

export const switchLang = (lang) => {
	if (lang === "switch") {
		if (localStorage.getItem("lang") === "en") {
			localStorage.setItem("lang", "ar");
			axios.defaults.headers.common["lang"] = "ar";
		} else {
			localStorage.setItem("lang", "en");
			axios.defaults.headers.common["lang"] = "en";
		}
	} else {
		localStorage.setItem("lang", lang);
		axios.defaults.headers.common["lang"] = lang;
	}
	window.location.reload();
};

export const accordion = (accordion) => {
	let acc = document.getElementsByClassName(accordion);
	let i;
	for (i = 0; i < acc.length; i++) {
		acc[i].addEventListener("click", function () {
			this.classList.toggle("active");
			let panel = this.nextElementSibling;
			if (panel.style.display === "block") {
				panel.style.display = "none";
			} else {
				panel.style.display = "block";
			}
		});
	}
};

export const shuffleArray = (array) => {
	let i = array.length;
	while (i--) {
		const ri = Math.floor(Math.random() * (i + 1));
		[array[i], array[ri]] = [array[ri], array[i]];
	}
	return array;
};

export const getRandomColor = () => {
	const randomColor = Math.floor(Math.random() * 16777215).toString(16);

	return `#${randomColor}`;
};
export const getTypeName = (val) => {
	return {}.toString.call(val).slice(8, -1);
};

export const removeTags = (str) => {
	if (str === null || str === "") return false;
	else str = str.toString();

	return str.replace(/(<([^>]+)>)/gi, "");
};
