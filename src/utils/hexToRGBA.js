export function hexToRGBA(hex, alpha) {
	hex = hex.replace(/^#/, "");

	const r = parseInt(hex.slice(0, 2), 16);
	const g = parseInt(hex.slice(2, 4), 16);
	const b = parseInt(hex.slice(4, 6), 16);

	alpha = Math.min(1, Math.max(0, alpha));

	return `rgba(${r}, ${g}, ${b}, ${alpha})`;
}
