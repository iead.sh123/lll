export const getInputClass = (touched, errors, fieldName) => {
	return touched[fieldName] && errors[fieldName] ? "input__error" : "";
};
