import { useInfiniteQuery } from "@tanstack/react-query";
import { toast } from "react-toastify";

export const useInfinitFetchDataRQ = ({
    queryKey,
    queryFn,
    enableCondition = true,
    getNextPageParam,
    onSuccessFn,
    onErrorFn,
    selectFn,
    dontFetchOnMount,
    keepPreviousData = true,
}) => {
    const data = useInfiniteQuery({
        queryKey,
        queryFn,
        getNextPageParam,
        refetchOnWindowFocus: false,
        refetchOnMount: dontFetchOnMount ? false : true,
        keepPreviousData: keepPreviousData ? true : false,
        retry: 1,
        enabled: enableCondition,
        onError: (error) => {
            console.log(error)
            toast.error(error?.message);
        },
        onSuccess: (data) => {
            console.log(data)
            // Because the response from the backend adopts this method, I don't know why
            // if (data?.pages[0]?.status === 0 || data?.pages[0]?.code === 500) {
            //     onErrorFn && onErrorFn(data.pages[0].msg);
            // } else {
            //     onSuccessFn && onSuccessFn(data);
            // }
        },
        select: (data) => {
            console.log(data)
            if (selectFn) {
                return selectFn(data);
            }
            const FetchData = data.pages.flatMap((page) => page.data);
            return { data: FetchData };
        },
    });

    return data;
};