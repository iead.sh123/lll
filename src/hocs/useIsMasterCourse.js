import { useLocation } from "react-router-dom";

const useIsMasterCourse = () => {
	// course catalog => Master Course
	// course management => Master Course | Clone Course
	const location = useLocation();
	const courseIsMaster = location.pathname.includes("master-course") && location.pathname.includes("course-catalog");

	return courseIsMaster;
};

export default useIsMasterCourse;
