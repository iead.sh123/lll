import { useSelector } from "react-redux";
import React from "react";

/**
 * @description should applied directly as hoc to the intended (themed) component withTheme(Com) not withTheme(anotherHoc(Com)).
 * @author eng-mostafa.
 */
const withTheme = (mapping) => (props) => {
  const theme = useSelector((state) => state?.theme?.selectedTheme);
  const ComponentToRender = mapping(theme);

  return (
    <React.Suspense fallback={() => {}}>
      <ComponentToRender {...props} />
    </React.Suspense>
  );
};

export default withTheme;
