import { useEffect, useMemo, useState } from 'react';
 
export const useIsInViewport = (ref) => {
    const [isIntersecting, setIsIntersecting] = useState(false);
 
    const observer = useMemo(
        () => (typeof window !== 'undefined' ? new IntersectionObserver(([entry]) => setIsIntersecting(entry.isIntersecting)) : null),
        []
    );
 
    useEffect(() => {
        if (ref && ref.current && observer) {
            observer.observe(ref.current);
        }
 
        return () => {
            if (observer) {
                observer.disconnect();
            }
        };
    }, [ref.current, observer]);
 
    return isIntersecting;
};