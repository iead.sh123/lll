import { appDomain } from "./config";

export const ServicesOnline = {
	courses_manager: {
		backend: "https://coursemanagement.graydesert-424caa95.northeurope.azurecontainerapps.io/",

		file: "https://storage.graydesert-424caa95.northeurope.azurecontainerapps.io/api/files/get-by-id/",
	},

	lesson: {
		backend: "https://lessons.graydesert-424caa95.northeurope.azurecontainerapps.io/",

		file: "https://storage.graydesert-424caa95.northeurope.azurecontainerapps.io/api/files/get-by-id/",
	},

	certificate: {
		backend: "https://certificate.graydesert-424caa95.northeurope.azurecontainerapps.io/",
		file: "https://storage.graydesert-424caa95.northeurope.azurecontainerapps.io/api/files/get-by-id/",
	},

	unit_plan: {
		backend: "https://unit-plan.graydesert-424caa95.northeurope.azurecontainerapps.io/",
		file: "https://storage.graydesert-424caa95.northeurope.azurecontainerapps.io/api/files/get-by-id/",
	},
	rubric: {
		backend: "https://rubric.graydesert-424caa95.northeurope.azurecontainerapps.io/",
		file: "https://storage.graydesert-424caa95.northeurope.azurecontainerapps.io/api/files/get-by-id/",
	},

	badge: {
		backend: "https://badges.graydesert-424caa95.northeurope.azurecontainerapps.io/",
		file: "https://storage.graydesert-424caa95.northeurope.azurecontainerapps.io/api/files/get-by-id/",
	},

	lesson_plan: {
		backend: "https://lesson-plan.graydesert-424caa95.northeurope.azurecontainerapps.io/",
		print: "https://print-service.lemonsea-40df4a8d.northeurope.azurecontainerapps.io/",
		file: "https://storage.graydesert-424caa95.northeurope.azurecontainerapps.io/api/files/get-by-id/",
	},

	tag_search: {
		backend: "https://tagservice.lemonsea-40df4a8d.northeurope.azurecontainerapps.io/",
		file: "https://storageservice.lemonsea-40df4a8d.northeurope.azurecontainerapps.io/",
	},

	logs: {
		backend: "https://tags.graydesert-424caa95.northeurope.azurecontainerapps.io/",
		file: "https://storage.graydesert-424caa95.northeurope.azurecontainerapps.io/api/files/get-by-id/",
	},

	lesson_content: {
		backend: "https://lesson-content.graydesert-424caa95.northeurope.azurecontainerapps.io/",
		file: "https://storage.graydesert-424caa95.northeurope.azurecontainerapps.io/api/files/get-by-id/",
	},

	authentication: {
		backend: "https://guidance-app-service.graydesert-424caa95.northeurope.azurecontainerapps.io/",
		file: "https://storage.graydesert-424caa95.northeurope.azurecontainerapps.io/api/files/get-by-id/",
	},

	auth_organization_management: {
		backend: "https://oauth.graydesert-424caa95.northeurope.azurecontainerapps.io/",
		file: "https://storage.graydesert-424caa95.northeurope.azurecontainerapps.io/api/files/get-by-id/",
	},
	notficiation: {
		backend: "https://notification.graydesert-424caa95.northeurope.azurecontainerapps.io/",
		file: "https://storage.graydesert-424caa95.northeurope.azurecontainerapps.io/api/files/get-by-id/",
	},
	storage: {
		backend: "https://storage.graydesert-424caa95.northeurope.azurecontainerapps.io/",
		file: "https://storage.graydesert-424caa95.northeurope.azurecontainerapps.io/api/files/get-by-id/",
	},
	im: {
		backend: "https://chat.graydesert-424caa95.northeurope.azurecontainerapps.io/",
		file: "https://storage.graydesert-424caa95.northeurope.azurecontainerapps.io/api/files/get-by-id/",
	},

	calender: {
		backend: "https://calendar.graydesert-424caa95.northeurope.azurecontainerapps.io/",
		file: "https://storage.graydesert-424caa95.northeurope.azurecontainerapps.io/api/files/get-by-id/",
	},

	collaboration: {
		backend: "https://collaboration.graydesert-424caa95.northeurope.azurecontainerapps.io/",
		file: "https://storage.graydesert-424caa95.northeurope.azurecontainerapps.io/api/files/get-by-id/",
	},

	discussion: {
		backend: "https://discussions.graydesert-424caa95.northeurope.azurecontainerapps.io/",
		file: "https://storage.graydesert-424caa95.northeurope.azurecontainerapps.io/api/files/get-by-id/",
	},

	meeting: {
		backend: "https://meeting.graydesert-424caa95.northeurope.azurecontainerapps.io/",
		file: "https://storage.graydesert-424caa95.northeurope.azurecontainerapps.io/api/files/get-by-id/",
	},

	print: {
		backend: "https://print.graydesert-424caa95.northeurope.azurecontainerapps.io/",
		file: "https://storage.graydesert-424caa95.northeurope.azurecontainerapps.io/api/files/get-by-id/",
	},

	question_set: {
		backend: "https://question-set.graydesert-424caa95.northeurope.azurecontainerapps.io/",
		file: "https://storage.graydesert-424caa95.northeurope.azurecontainerapps.io/api/files/get-by-id/",
	},

	grading: {
		backend: "https://grading.graydesert-424caa95.northeurope.azurecontainerapps.io/",
		file: "",
	},

	attendance: {
		backend: "https://grading.graydesert-424caa95.northeurope.azurecontainerapps.io/",
		file: "",
	},
	terms: {
		backend: "https://terms-management.graydesert-424caa95.northeurope.azurecontainerapps.io/",
		file: "https://storage.graydesert-424caa95.northeurope.azurecontainerapps.io/api/files/get-by-id/",
	},

	payment: {
		backend: "https://payment.graydesert-424caa95.northeurope.azurecontainerapps.io/",
		file: "https://storage.graydesert-424caa95.northeurope.azurecontainerapps.io/api/files/get-by-id/",
	},
	search: {
		backend: "http://192.168.1.72:8009/",
	},

	file_management: {
		backend: "https://filemanagement.graydesert-424caa95.northeurope.azurecontainerapps.io/",
		file: "http://192.168.1.73:8012/api/file_management/files/download/",
	},

	users_management: {
		backend: "https://newoauth.graydesert-424caa95.northeurope.azurecontainerapps.io/",
	},

	departments_programs: {
		backend: "http://192.168.1.70:8015/",
	},

	course: {
		backend: "http://192.168.1.73:8013/",
	},

	scorm: {
		backend: "https://scorm.graydesert-424caa95.northeurope.azurecontainerapps.io/",
		file: appDomain + "scorm-reader.php/https://scorm.graydesert-424caa95.northeurope.azurecontainerapps.io/storage/",
	},

	tag: {
		backend: "http://192.168.1.72:8000/",
	},
};
