import { connectWithGetAway, getAwayOnline, online } from "./config";
import { ServicesGetawayOnline } from "./ServicesGetawayOnline";
import { ServicesGetawayLocal } from "./ServicesGetawayLocal";
import { ServicesOnline } from "./ServicesOnline";
import { ServicesLocal } from "./ServicesLocal";

const ConnectWithService = online ? ServicesOnline : ServicesLocal;
const ConnectWithGetAway = getAwayOnline ? ServicesGetawayOnline : ServicesGetawayLocal;

export const Services = connectWithGetAway ? ConnectWithGetAway : ConnectWithService;
