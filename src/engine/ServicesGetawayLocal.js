import { appDomain, backend } from "./config";

export const ServicesGetawayLocal = {
	courses_manager: {
		backend: "http://192.168.1.71:3500/",
	},

	lesson: {
		backend: "http://192.168.1.71:3500/",
	},

	question_set: {
		backend: "http://192.168.1.71:3500/",
	},

	grading: {
		backend: "http://192.168.1.71:3500/",
	},

	scorm: {
		backend: "http://192.168.1.71:3500/",
		file: appDomain + "scorm-reader.php/http://192.168.1.71:8020/storage/",
	},

	certificate: {
		backend: "http://192.168.1.71:3500/",
	},

	unit_plan: {
		backend: "http://192.168.1.71:3500/",
	},
	rubric: {
		backend: "http://192.168.1.71:3500/",
	},

	badge: {
		backend: "http://192.168.1.71:3500/",
	},

	lesson_plan: {
		backend: "http://192.168.1.71:3500/",
	},

	tag_search: {
		backend: "http://192.168.1.71:3500/",
	},

	logs: {
		backend: "http://192.168.1.71:3500/",
	},

	lesson_content: {
		backend: "http://192.168.1.71:3500/",
	},

	authentication: {
		backend: "http://192.168.1.71:3500/",
	},

	auth_organization_management: {
		backend: "http://192.168.1.71:3500/",
	},

	discussion: {
		backend: "http://192.168.1.71:3500/",
	},
	cohorts: {
		backend: "http://192.168.1.71:3500/",
	},

	notficiation: {
		backend: "http://192.168.1.71:3500/",
	},

	storage: {
		backend: "http://192.168.1.71:3500/",
		file: "http://192.168.1.71:3500/api/files/get-by-id/",
	},

	im: {
		backend: "http://192.168.1.71:3500/",
	},

	terms: {
		backend: "http://192.168.1.71:3500/",
	},

	calender: {
		backend: "http://192.168.1.71:3500/",
	},

	collaboration: {
		backend: "http://192.168.1.71:3500/",
	},

	meeting: {
		backend: "http://192.168.1.71:3500/",
	},

	attendance: {
		backend: "http://192.168.1.71:3500/",
	},

	print: {
		backend: "http://192.168.1.71:3500/",
	},

	payment: {
		backend: "http://192.168.1.71:3500/",
	},

	organizations: {
		backend: "http://192.168.1.71:3500/",
	},

	search: {
		backend: "http://192.168.1.71:3500/",
	},

	file_management: {
		backend: "http://192.168.1.71:3500/",
	},

	users_management: {
		backend: "http://192.168.1.71:3500/",
	},
	departments_programs: {
		backend: "http://192.168.1.71:3500/",
	},

	course: {
		backend: "http://192.168.1.71:3500/",
	},

	tag: {
		backend: "http://192.168.1.71:3500/",
	},
};
