export const backend = process.env.REACT_APP_BACKEND_URL;
export const versionServiceComponent = process.env.REACT_APP_VERSION_SERVICE_COMPONENT;
export const versionServiceUnitPlan = process.env.REACT_APP_VERSION_SERVICE_UNIT_PLAN;
export const versionServiceRubric = process.env.REACT_APP_VERSION_SERVICE_RUBRIC;
export const LoginService = process.env.LoginService;
export const genericPath = process.env.REACT_APP_GENERIC_PATH;
export const files = backend + "api/files/get/";
export const baseURL = "";

// export const appDomain = "http://localhost/scorm/";
export const appDomain = `${window !== "undefined" ? window?.location.origin : ""}/`;
export const pusher_key = "6d0ef7c934860c779f49";

export const connectWithGetAway = false;
export const getAwayOnline = false;
export const online = false;

export const sidebarOnline = false;
export const specificOrganization = 307;
export const newAuth = false;
export const removeApisNotWorkingFromLocale = true;
export const newAuthToken =
	"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNWM5NjViMTczOTQzNDc5ODVhZDRiOTU1YTgyNzFkYjQwZjYwMGQyMjhjMjA1YWRjNTE0OWIxNGUwYmU4NmUwYzY0NTdlMjVlMzdlZDlkMWYiLCJpYXQiOjE3MTY4MjM3MTQuMzcwNjgsIm5iZiI6MTcxNjgyMzcxNC4zNzA2ODgsImV4cCI6MTczMjcyMTMxNC4zNjE4NDUsInN1YiI6IjE1Iiwic2NvcGVzIjpbXX0.hWei8zbE3vst7R6bFWsy179YeZSR3haSU703wT4MTPYgjU5UiiTD_d49saTDskc2_dQsstBpIA7IHd0HTBA9ti54t290FLitdc5l7M1djSFyaJUkgfyp5MAZA7WOkzUdWKFnzlvNfHMqby10WiVu4lHbXh3d5zy1Eg50EgK__K1Qz-oKRmutTMKD8M-m5T8bvN4N-qvpf92cR3PQE-t-AOQnQctIXskyUVtViJOGyfnnvacf8dtsUOYxejlm8GU8uvBf9mTd2fb3TQq8FPYHwo2ju-oRVh9ybLZUz4yWWEv6NkLDQ2mC6qiifLq2FWo02lesEbce4g7F8KacYWqiX3EM8OSTk_2rup1OE_dsGUQ_m_uKQtGrDr_quWkzvK26yp5GrnfFNhEoGeoMJMtpWbAfmwj4-U0IsX3FZMKNvkZWY54YP3LqWR8X9I8pnFBoQ1mt1SAwhYatHYZyRLkTmTwbWtW9B5-mByG9t1nWoOAT3BJbRhKHkUJ67pRFRIFwXyLpGschS_nzUt6soxIS1j4y8soiUrj8vBYftIsQ-HpbMONJ4zj6Ey-iE265by3CYGU4pZ1W684eBxPZyKq98IwiHbkZSbhnfuLU6gkucHRolunHIMS6ZljizTInz_dF7RYp1P2t8y2aWRMrTMQTYMnKY9JJklN-sbUBumESr6Y";
