import { appDomain, backend } from "./config";

export const ServicesGetawayOnline = {
	courses_manager: {
		backend: "",
	},

	lesson: {
		backend: "",
	},

	question_set: {
		backend: "",
	},

	grading: {
		backend: "",
	},

	scorm: {
		backend: "",
		file: appDomain + "scorm-reader.php/http://192.168.1.71:8020/storage/",
	},

	certificate: {
		backend: "",
	},

	unit_plan: {
		backend: "",
	},
	rubric: {
		backend: "",
	},

	badge: {
		backend: "",
	},

	lesson_plan: {
		backend: "",
	},

	tag_search: {
		backend: "",
	},

	logs: {
		backend: "",
	},

	lesson_content: {
		backend: "",
	},

	authentication: {
		backend: "",
	},

	auth_organization_management: {
		backend: "",
	},

	discussion: {
		backend: "",
	},
	cohorts: {
		backend: "",
	},

	notficiation: {
		backend: "",
	},

	storage: {
		backend: "",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
	},

	im: {
		backend: "",
	},

	terms: {
		backend: "",
	},

	calender: {
		backend: "",
	},

	collaboration: {
		backend: "",
	},

	meeting: {
		backend: "",
	},

	attendance: {
		backend: "",
	},

	print: {
		backend: "",
	},

	payment: {
		backend: "",
	},

	organizations: {
		backend: "",
	},

	search: {
		backend: "",
	},

	file_management: {
		backend: "",
	},

	users_management: {
		backend: "",
	},
	departments_programs: {
		backend: "",
	},

	course: {
		backend: "",
	},

	tag: {
		backend: "",
	},
};
