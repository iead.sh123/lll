import { appDomain, backend } from "./config";

export const ServicesLocal = {
	courses_manager: {
		backend: "http://192.168.1.71:8008/",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
	},

	lesson: {
		backend: "http://192.168.1.72:8042/",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
	},

	question_set: {
		backend: "http://192.168.1.71:3002/",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
	},

	grading: {
		backend: "http://192.168.1.71:3005/",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
	},

	scorm: {
		backend: "http://192.168.1.71:8020/",
		file: appDomain + "scorm-reader.php/http://192.168.1.71:8020/storage/",
	},

	certificate: {
		backend: "http://192.168.1.71:8010/",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
	},

	unit_plan: {
		backend: "http://192.168.1.70:8010/",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
	},
	rubric: {
		backend: "http://192.168.1.71:8009/",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
	},

	badge: {
		// backend: "http://192.168.1.70:8008/",
		backend: "http://192.168.1.72:8005/",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
	},

	lesson_plan: {
		backend: "http://192.168.1.71:8011/",
		print: "http://192.168.1.70:8012/",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
	},

	tag_search: {
		backend: "http://192.168.1.70:8007/",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
	},

	logs: {
		backend: "http://192.168.1.72:8009/",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
	},

	lesson_content: {
		backend: "http://192.168.1.71:8034/",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
	},

	authentication: {
		backend: "http://192.168.1.71:7001/",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
	},

	auth_organization_management: {
		backend: "http://192.168.1.71:7000/",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
	},

	discussion: {
		backend: "http://192.168.1.72:8022/",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
	},
	cohorts: {
		backend: "http://192.168.1.70:8006/",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
	},

	// cohort: {
	//   backend: "http://192.168.1.70:8006/",
	//   file: "http://192.168.1.73:8001/api/files/get-by-id/",
	// },

	notficiation: {
		backend: "http://192.168.1.73:8002/",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
	},

	storage: {
		backend: "http://192.168.1.73:8001/",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
		flipBookFile: "http://192.168.1.73:8001/api/files/get/",
	},

	im: {
		backend: "http://192.168.1.73:7002/",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
	},

	terms: {
		backend: "http://192.168.1.71:3003/",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
	},

	calender: {
		backend: "http://192.168.1.71:3001/",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
	},

	collaboration: {
		backend: "http://192.168.1.73:7003/",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
	},

	meeting: {
		backend: "http://192.168.1.73:8004/",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
	},

	attendance: {
		backend: "http://192.168.1.70:8013/",
	},

	print: {
		backend: "http://192.168.1.70:8012/",
		file: "http://192.168.1.73:8001/api/files/get-by-id/",
	},

	payment: {
		backend: "http://192.168.1.73:8010/",
	},

	organizations: {
		backend: "http://192.168.1.70:8014/",
	},

	search: {
		backend: "http://192.168.1.72:8009/",
	},

	file_management: {
		backend: "http://192.168.1.73:8012/",
		file: "http://192.168.1.73:8012/api/file_management/files/download/",
	},
	users_management: {
		backend: "http://192.168.1.71:8067/",
	},
	departments_programs: {
		backend: "http://192.168.1.70:8015/",
	},

	course: {
		backend: "http://192.168.1.73:8013/",
	},

	tag: {
		backend: "http://192.168.1.72:8062/",
	},
};
