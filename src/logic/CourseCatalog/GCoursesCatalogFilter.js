import React, { useState } from 'react';
import { Button, Input, InputGroup, InputGroupAddon, InputGroupText, Label, Form, FormGroup, CustomInput } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFilter } from '@fortawesome/free-solid-svg-icons';
import RFlex from 'components/Global/RComs/RFlex/RFlex';
import RButton from 'components/Global/RComs/RButton';
import RButtonIcon from 'components/Global/RComs/RButtonIcon';
import RLiveAction from 'components/Global/RComs/Collaboration/RLiveAction';
import Helper from 'components/Global/RComs/Helper';
import RSelect from 'components/Global/RComs/RSelect';
import tr from 'components/Global/RComs/RTranslator';

const GCoursesCatalogFilter = ({filterCredits,filterStatus,setFilterCredits,setFilterStatus,filter,filterreset}) => {
  const [toggle, setToggle] = useState(false);

  const handleToggle = () => {
    setToggle(!toggle);
  };
  const reset = () => {
    setFilterCredits(0);
	  setFilterStatus(null);
    setToggle(false);
   
  
    //setToggle(true);
  };

  const options = [
    { label: 'Active', value: "published" , selected:filterStatus=="published"  },
    { label: 'All', value: "all" ,selected:!filterStatus||filterStatus=="all"||filterStatus=="" },
    { label: "Draft", value: "unpublished" ,selected:filterStatus=="unpublished"  } 
  ]


  return (
    <div>
     <RFlex style={{display:"flex",alignItems:"center"}}> 
     {/* {filterStatus} */}
     <RLiveAction 
        disabled={false}
        color={toggle?"#668AD7":"#585858"}
        //</RFlex>}
        style={toggle?{backgroundColor:"white",paddingLeft:"0px",color:"#585858"}:
        {backgroundColor:"white",color:"#668AD7",paddingLeft:"0px",marginRight:"30px"}}  
        iconStyle={{left:"20px"}}
        text={<div style={{marginLeft:"-5px",marginRight:"15px"}}><FontAwesomeIcon icon={faFilter} 
                className="mr-2" />
              Filter</div>} 
              onClick={handleToggle} 
              className="mb-3">
     </RLiveAction>
      {toggle && (
        <>
        {/* {filterCredits} */}
        <RFlex style={{display:"flex",alignItems:"center" ,gap:"15px"}}>
      {/* {filterCredits} */}
            <input style={{
              height:"38px",
              width:"80px",
              fontSize:"12px",
              padding:"2px",
              gap:"10px",
              border: "1px solid #ced4da",
              borderRadius: "0.25rem"
              }}
              
              type="number" 
              placeholder="Credit" 
              onChange={(e)=>{setFilterCredits(e.target.value)}} 
              value={filterCredits===0?null:filterCredits} 
              //defaultValue={filterCredits} 
              defaultValue={(filterCredits&&filterCredits>0)?filterCredits:null} 
              
          
              
              />
         
 

            
            {/* <CustomInput style={{fontSize:"12px",marginLeft:"10px"}} type="select"  placeholder='Stauts' onChange={(e)=>{ setFilterStatus(e.target.value)}}>
              <option value="published" selected={filterStatus=="published" }>Published</option>
              <option value="all"selected={filterStatus=="all" }>All</option>
              <option value="unpublished"selected={filterStatus=="unpublished" }>Not Published</option>
            </CustomInput>

            {Helper.js(filterStatus,"filterStatus")} */}
            <div style={{fontSize:"12px",minWidth:"143px",width:"143px", height:"38px",flexGrow:true}} >
            <RSelect
                  style={{fontSize:"12px",minWidth:"143px !important",width:"143px !important", height:"38px",flexGrow:true}} 
                  option={options}
                  closeMenuOnSelect={true}
                  placeholder={tr`Status`}
                  value={
                    options.filter(o=>o.selected)?options.filter(o=>o.selected)[0]:
                    options[1]
                  }
                  onChange={(e)=>{ setFilterStatus(e.value)}}
                  required
            />
          </div>
            <RLiveAction style=
                        {{
                          borderRadius:"2px",
                      
                          padding:"5px",
                          color:"white",
                         
                          paddingLeft:"10px",
                          paddingRight:"10px"
                        }}
             border={true} backgroundColor="#668AD7" color="white" onClick={filter}  text={"Apply"} />
            <RLiveAction color="orange" 
            onClick={
                ()=>{
                reset();
                setTimeout(
                  ()=>{setToggle(true); filterreset();}
                    ,200
                    );
              }
                } text={"Reset"} />
		  </RFlex>
      </>
      )}
	  </RFlex> 
    </div>
  );
};

export default GCoursesCatalogFilter;