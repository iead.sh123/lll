import React, { useState, useEffect } from "react";

import { baseURL, genericPath } from "engine/config";

import tr from "components/Global/RComs/RTranslator";

import RFlex from "components/Global/RComs/RFlex/RFlex";
import { Services } from "engine/services";
import moment from "moment";
import iconsFa6 from "variables/iconsFa6";
import { successColor } from "config/constants";
import { warningColor } from "config/constants";
import RCard from "components/Global/RComs/RCards/RCard";
import RLister from "components/Global/RComs/RLister";
import Helper from "components/Global/RComs/Helper";
import { Link } from "react-router-dom/cjs/react-router-dom.min";
import { faPen, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import RLiveAction from "components/Global/RComs/Collaboration/RLiveAction";
import REmptyData from "components/RComponents/REmptyData";
const GCoursetable = ({
	showCategory,
	courses,
	setloading,
	publishAndUnPublishCourseAsync,
	editCourseInformation,
	handleRemoveCourse,
	newCourseCallback,
}) => {
	const [transferedCourses, setTransferedCourses] = useState([]);

	useEffect(() => {
		buildCourses(courses);
	}, [courses]);

	const buildCourses = (courses2) => {
		if (courses2 && courses2.length)
			setTransferedCourses(
				courses2?.map((course, index) => {
					const condition = showCategory;
					return {
						id: course.id,
						table_name: "GAttendanceDetailsLister",
						details: [
							{ key: "Course Code", value: course.code, type: "component" },
							{
								key: "Course Name",
								value: (
									<a
										href={
											course.category_id
												? `${baseURL}/${genericPath}/course-management/course/${course.id}/cohort/${course.cohort_id}/category/${course.category_id}/modules`
												: `${baseURL}/${genericPath}/course-management/course/${course.id}/cohort/${course.cohort_id}/modules`
										}
									>
										{course.name}
									</a>
								),
								type: "component",
								keyStyle: { color: "#668AD7" },
								color: "#46C37E",
							},
							...(condition ? [{ key: "Program", value: course.category_name, color: "#585858" }] : []),
							{ key: "Credit", value: course.credits ?? "-", color: "#585858" },
							{
								key: "Status",
								value: (
									<RLiveAction
										disabled={false}
										style={{ paddingLeft: "0px" }}
										text={
											<span style={{ color: course.is_published ? "#46C37E" : "#DD0000" }}>{course.is_published ? "Active" : "Draft"}</span>
										}
										onClick={() => {
											setloading(course.id);
											publishAndUnPublishCourseAsync(course.id, course.category_id, course.is_published);
										}}
										className="mb-3"
										loading={course?.loading}
									></RLiveAction>
								),
								type: "component",
								color: "#585858",
							},
							{
								key: "Actions",
								value: (
									<div id="tableActions">
										<RFlex>
											<RLiveAction
												disabled={false}
												color={"#585858"}
												style={{ paddingLeft: "0px", color: "#585858" }}
												text={
													<div>
														{" "}
														<FontAwesomeIcon icon={faPen} className="mr-2" />
													</div>
												}
												onClick={() => {
													editCourseInformation(course.id);
												}}
												className="mb-3"
											></RLiveAction>

											<RLiveAction
												disabled={false}
												color={"red"}
												style={{ paddingLeft: "0px", color: "red" }}
												text={
													<div>
														{" "}
														<FontAwesomeIcon icon={faTrashAlt} className="mr-2" />
													</div>
												}
												onClick={() => {
													handleRemoveCourse(course.id);
												}}
												className="mb-3"
											></RLiveAction>
										</RFlex>
									</div>
								),

								color: "#585858",
								type: "component",
							},
						],
						actions: null,
						// [
						// 	{
						// 		id: 0,
						// 		name: course.is_published ? tr`deactivate` : tr`activate`,
						// 		icon: course.is_published ? iconsFa6.pen : iconsFa6.check,
						// 		color: course.is_published ? warningColor : successColor,
						// 		//hidden: course.is_master,
						// 		onClick: () => {
						// 			publishAndUnPublishCourseAsync(course.id, course.category_id, course.is_published);
						// 		},
						// 	},
						// 	{
						// 		id: 1,
						// 		name: tr`Edit information`,
						// 		icon: iconsFa6.pen,

						// 		onClick: () => {
						// 		editCourseInformation(course.id)
						// 		},
						// 	},
						// 	{
						// 		id: 4,
						// 		name: tr`delete`,
						// 		icon: iconsFa6.delete,
						// 		color: "red",
						// 		onClick: () => {
						// 			handleRemoveCourse(course.id);
						// 		},
						// 	},
						// ]
					};
				})
			);
		else setTransferedCourses([]);
	};

	return (
		<>
			{transferedCourses && transferedCourses?.length > 0 ? (
				<RLister Records={transferedCourses} center={false} tableStyle={{ maxHeight: "70vh", overflowY: "auto" }} />
			) : (
				<RFlex styleProps={{ justifyContent: "center", alignItems: "center", width: "100%" }}>
					<REmptyData
						line1={<div style={{ weight: "400", fontSize: "12px" }}>No Courses Yet</div>}
						line2={
							<div style={{ weight: "400", fontSize: "14px" }}>
								Click On <Link onClick={newCourseCallback}>New Course</Link> To Start{" "}
							</div>
						}
					/>
				</RFlex>
			)}
		</>
	);
};

export default GCoursetable;
