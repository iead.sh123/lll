import React, { useEffect, useState } from "react";
import Swal, { DANGER } from "utils/Alert";
import { useDispatch, useSelector } from "react-redux";
import {
	getCourseCategoriesTreeAsync,
	addCategoryToTreeAsync,
	removeCategoryFromTreeAsync,
} from "store/actions/global/coursesManager.action";
import { deleteSweetAlert } from "components/Global/RComs/RAlert2";
import { primaryColor } from "config/constants";
import { Row, Col } from "reactstrap";
import GCourseCatalogContent from "./GCourseCatalogContent";

import GCategoryAdd from "./GCategoryAdd";
import Loader from "utils/Loader";
import styles from "./GCoursesCatalog.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import RCourseCategoryList from "view/CoursesManager/RCourseCategoryList";
import { useHistory, useParams } from "react-router-dom/cjs/react-router-dom";
import Helper from "components/Global/RComs/Helper";
import { nodeleteSweetAlert } from "components/Global/RComs/RAlert2";
import { Services } from "engine/services";

const GCoursesCatalog = () => {
	const dispatch = useDispatch();
	const params = useParams();
	const { categoryId } = useParams();
	const history = useHistory();
	const [addCategory, setAddCategory] = useState(false);
	const [alert, setAlert] = useState(false);
	const [Uncategorized, setUncategorized] = useState([]);

	const get_uncated_count = async () => {
		await Helper.fastGet(
			Services.courses_manager.backend +
				"api/course-catalogue/filter?query={%22category_id%22:%22uncategorized%22,%22noPagination%22:true}",
			"fail to get",
			(response) => {
				//alert(response?.data?.data?.length);
				setUncategorized(response?.data?.data?.filter((c) => c.is_master)?.length);
			},
			() => {}
		);
	};

	const [formGroup, setFormGroup] = useState({
		id: null,
		name: "",
		parent_id: null,
	});
	const [silentLoading, setSilentLoading] = useState(false);

	const [courseSilentLoading, setCourseSilentLoading] = useState(false);
	const [categorySilentloader, setCategorySilentloader] = useState(false);

	const { courseManagerTree, courseManagerTreeLoading, addEditCategoryToTreeLoading, removeCategoryFromTreeLoading } = useSelector(
		(state) => state.coursesManagerRed
	);
	useEffect(() => {
		get_uncated_count();
	}, []);
	useEffect(() => {
		get_uncated_count();
	}, [courseManagerTree]);

	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);

	const handleOpenAddCategory = () => setAddCategory(true);
	const handleCloseAddCategory = () => setAddCategory(false);

	const reloadCategories = () => {
		setCategorySilentloader(true);
		dispatch(getCourseCategoriesTreeAsync({}));
	};

	useEffect(() => {
		dispatch(getCourseCategoriesTreeAsync({}));
	}, []);

	const handleChange = (name, value) => {
		// Helper.cl(name,"handleChange 1 1 1 ");
		setFormGroup({
			...formGroup,
			[name]: value,
		});
	};

	const handleAddCategoryToTree = () => {
		//if(formGroup)
		Helper.cl(formGroup, "formGroup11");

		if (formGroup?.name) {
			setSilentLoading(true);
			setCategorySilentloader(true);
			dispatch(addCategoryToTreeAsync(formGroup, handleCloseAddCategory, setFormGroup));
		} else {
			Helper.cl(formGroup, "here here here");
			setAddCategory(false);
		}
	};

	const successDelete = async (prameters) => {
		if (!prameters.hasCourses) {
			setSilentLoading(true);
			dispatch(removeCategoryFromTreeAsync(prameters.categoryId, hideAlert));
			reloadCategories();
		}
	};

	const handleRemoveCategoryFromTree = (categoryId, canDelete) => {
		const prameters = {
			categoryId,
			canDelete,
		};
		const message = tr`Are you Sure ? This can't be undone!`;
		const confirm = tr`Yes, delete it`;
		prameters.canDelete
			? deleteSweetAlert(showAlerts, hideAlert, successDelete, prameters, message, confirm)
			: nodeleteSweetAlert(showAlerts, tr`This program have courses , and can't be deleted!`, hideAlert);
	};

	let level = 0;
	return (
		<React.Fragment>
			<Row style={{ height: " 84vh" }}>
				{alert}
				<Col xs={12} sm={2} className={styles.tree + " scroll_hidden p-0"} style={{ height: "90vh" }}>
					{courseManagerTreeLoading && !silentLoading ? (
						<Loader />
					) : (
						<RFlex style={{ flexDirection: "column", height: "100%" }}>
							<RFlex
								className={styles.new_category}
								styleProps={{
									color: primaryColor,
								}}
								onClick={() => handleOpenAddCategory()}
							>
								<i className={`fa fa-plus`} />
								<p className={`m-0 ${styles.new_category_text}`}>{tr`Create Program`}</p>
							</RFlex>

							<RCourseCategoryList
								parentId={null}
								courseCategories={courseManagerTree}
								level={level + 1}
								handleAddCategoryToTree={handleAddCategoryToTree}
								handleRemoveCategoryFromTree={handleRemoveCategoryFromTree}
								addEditCategoryToTreeLoading={addEditCategoryToTreeLoading}
								formGroup={formGroup}
								handleChange={handleChange}
								addCategory={addCategory}
								handleCloseAddCategory={handleCloseAddCategory}
								categorySilentloader={categorySilentloader}
								setCategorySilentloader={setCategorySilentloader}
								uncategorizedCount={Uncategorized}
							/>
						</RFlex>
					)}
				</Col>

				<Col xs={12} sm={10} className={styles.content + " scroll_hidden"}>
					<GCourseCatalogContent
						categoryId={categoryId}
						history={history}
						courseCategories={courseManagerTree}
						reloadCategories={reloadCategories}
						courseSilentLoading={courseSilentLoading}
						setCourseSilentLoading={setCourseSilentLoading}
					/>
				</Col>
			</Row>
		</React.Fragment>
	);
};

export default GCoursesCatalog;
