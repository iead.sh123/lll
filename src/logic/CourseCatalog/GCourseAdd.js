import React, { useEffect, useState } from "react";
import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Row, Col, Form, FormGroup, Input } from "reactstrap";
import RFormModal from "components/Global/RComs/RFormModal";
import Helper from "components/Global/RComs/Helper";
import { coursesManagerApi } from "api/global/coursesManager";
import Swal from "utils/Alert";
import { DANGER } from "utils/Alert";
import { toast } from "react-toastify";

const GCourseAdd = ({ categories ,
	courseName,setCourseName,
	courseCode,setCourseCode,
	courseCredits,setCourseCredits,
	courseDescription,setCourseDescription,
	courseCategory,setCourseCategory,
	courseImage,setCourseImage,
	courseId,
    openAdd,setOpenAdd,mode="add",
	successCallback
}) => {
	const [form1, setForm1] = useState(); //agreement.;
	const [requesting,setRequesting]=useState(false);
	useEffect(()=>{
		setRequesting(false)
	},[openAdd])
	const addSubmit=async (data)=>
	{
		setRequesting(true);
		Helper.cl(data,"add submit data = ");
		Helper.cl(courseDescription,"courseDescription ");
	
		//"image":courseId?[courseImage]:null};

		const data1=
		{
		"id":courseId,
		"isOnline":false,
		"isFree":true,
		"prices":[],
		"discounts":[],
		"category_id":courseCategory=="uncategorized"?null:courseCategory??null,
		"name":courseName,
		"code":courseCode,
		"credits":courseCredits,
		"color":"#a5bcee",

		"image":(courseImage&&courseImage?.url)?[courseImage]:null};
		Helper.cl(data1,"add submit data = ");

		const response =(mode=="add")?await coursesManagerApi.saveCourse(data1, false):

		await coursesManagerApi.updateCourse(data1, courseId,false) ;
		Helper.cl(response.data," first call response.data");	
		if(response?.data?.status&&response.data?.data){
			//----------------------------now should update description in the overview
			const cid=courseId??response?.data?.data?.id;
			Helper.cl(response.data,"response.data");
		//	const response1 =await coursesManagerApi.saveCourse(data1, false);
			Helper.cl(cid,"cid");
			const overviewData={"description":courseDescription}
			const response1=await coursesManagerApi.saveCourseOverview(cid, overviewData)
			Helper.cl(response1.data.status," second call response.data.status");	
			Helper.cl(successCallback ,"second call response.data.status");
			successCallback();
		}
		else
		{
			toast.error(response?.data?.msg)
		}
		setRequesting(false);
	}

	const handleFileChanged = (childData) => {
		if (childData) {
			//alert(JSON.stringify(childData));
			
			setCourseImage({
					url: childData?.[0]?.url,
					file_name: childData?.[0]?.file_name,
					type: childData?.[0]?.type,
				});
		}
	};

	const test =(e)=>{
		alert(JSON.stringify(e));
	}
	const constructForm = () => {
		let form2 = [];
		const labelStyle={
            fontWeight: "400",
            marginBottom: "0px",
           
          }

		const rowStyle={fontWeight: "400",
            margin: "0px",
            padding: "0px",
			marginBottom:"15px"
          }
		  

		const col1Style={
			fontWeight: "400",
	        margin: "0px",
            padding: "0px",

          }

		  const col2Style={
			fontWeight: "400",
	        margin: "0px",
            padding: "0px",
			marginRight:"15px"
          }
		  const containingDivStyle={
				margin: "0px",
				padding: "0px",
			}
		  const styles={labelStyle:labelStyle,
		  rowStyle:rowStyle,
		  col1Style:col1Style,
		  col2Style:col2Style,
		  containingDivStyle:containingDivStyle
		}
		//alert(courseName);
		let l1=(categories?.[categories?.length-1]?.name=="uncategorized")?"no program":categories?.[categories?.length-1]?.name;
		Helper.cl(l1,"l111");
		let orderedCategories=[{
			label: l1 ,
			value: categories?.[categories?.length-1]?.id,
		  },
		...categories?.filter((c,i)=>i<(categories?.length-1))?.map((c,i)=>
			({
				label: c.name=="uncategorized"?"no program":c.name,
				value: c.id,
			  }))];

		const cat1=categories.filter(ca=>ca.id==courseCategory)?.[0];
		const cat=
		cat1?{
			label: cat1.name=="uncategorized"?"no program":cat1.name,//JSON.stringify(c),
			value: cat1.id,
		  }:orderedCategories[0];


		Helper.cl(courseName,"courseName");	
		form2.push({...styles,
			col: 6,
			beginning: true,
			type: "text",
			label: tr`Course Name`,
			name: "course_name",
			
			defaultValue: courseName,

			 style:{ width:"220px"},
			//value:courseName,
			onChange:(e)=> setCourseName(e.target.value),
		});

		form2.push({
			...styles,
			col: 6,
			beginning: true,
			type: "text",
			label: tr`Course code`,
			name: "course_code",
			defaultValue: courseCode,
			labelStyle:labelStyle,
			  style:{ width:"232px"},
			//value: courseCode,
			onChange:(e)=> setCourseCode(e.target.value),
		});

		//-----------------------------------------------line 2
		form2.push({
			...styles,
			col: 6,
			beginning: true,
			type: "number",
			label: tr`Course Credit`,
			min:1,
			name: "course_credits",
			defaultValue: courseCredits,
			labelinfo:"Measure of a courses workload and value",
			labelStyle:labelStyle,
			  style:{ width:"220px"},
			//value: courseCredits,
			onChange:(e)=> setCourseCredits(+e.target.value),
		});

		form2.push({
			...styles,
			col: 6,
			beginning: true,
			type: "select",
			label: tr`Course program`,
			name: "course_category",
			  style:{ width:"232px"},
			labelStyle:labelStyle,
			defaultValue:  cat??{
				label: categories?.[0]?.name,//JSON.stringify(c),
				value: categories?.[0]?.id,
			  },
			//value:cat,
			option:orderedCategories
			,
			  onChange:(e)=> setCourseCategory(e.value),
	
		});

		form2.push({
			...styles,
			col: 12,
			beginning: true,
			//handleData: (file) => handleFileChanged(file),
			type: "text_editor",
			label: tr`Course Description`,
			// labelinfo:"Some Label INfo 22",
			name: "course_description",
			labelStyle:labelStyle,
			defaultValue: courseDescription??"",
			value: courseDescription??"",
			items:[
				"heading",
				"MathType",
				"ChemType",
				"|",
				"bold",
				"italic",
				"link",
				"bulletedList",
				"numberedList",
				"imageUpload",
				"mediaEmbed",
				"insertTable",
				"blockQuote",
				"undo",
				"redo",
			  ],
			
			data:"",
			onChange:(event, editor) => {
				const data = editor.getData();
				Helper.cl(data,"data");
				setCourseDescription(data);
			  }
			
			
		,
		});

				//-----------------------------------------------line 3
				form2.push({...styles,
					col: 12,
					beginning: true,
					handleData: (file) => handleFileChanged(file),
					type: "file",
					typeFile: "image/jpeg, image/* , image/png ,image/jpg",
					binary: true,
					value:courseImage?[{hash_id:courseImage}]:null,
					labelStyle:labelStyle,
					label: tr`Course Thumbnail`,
					singleFile:true,
					name: "course_thumbnail",
					defaultValue: "",
					//onChange: changeDestinationOrganization,
				});
		
		//Helper.cl(contents,"contents");
		// contents?.map((c) => {
		// 	//------------------search for existence in details
		// 	const matches = ag?.contents?.filter((d) => d.id == c.id);
		// 	// Helper.cl(matches,c.id+"matches");//.length
		// 	// Helper.cl(ag.contents,c.id+"ag.details");//.length
		// 	//
		// 	form2.push({
		// 		beginning: true,
		// 		col: 5,
		// 		type: "checkbox",
		// 		checked: matches && matches.length,
		// 		label: c.name,
		// 		name: c.id + "_" + c.name,
		// 		onChange: (e) => {
		// 			changeContentType(e, c.id);
		// 		},
		// 	});
		// 	// form2.push({
		// 	//   col:5,
		// 	//   type: "select",
		// 	//   label: tr`level` ,
		// 	//   name: c.id+"_"+"level" ,
		// 	//   option: lvls?.map((e) => {
		// 	//     return { label: e.name, value: e.id };
		// 	//   }),
		// 	//   isDisabled:!(matches&&matches.length),
		// 	//   onChange:(e)=>{ changeLevel(e,c.id)},
		// 	// });
		// 	form2.push({
		// 		col: 5,
		// 		type: "multicheckbox_dropdown",
		// 		label: tr`level`,
		// 		name: c.id + "_" + "level",
		// 		options: lvls?.map((e) => {
		// 			// Helper.cl(e.id,"al level");
		// 			// if(matches?.[0]?.levels)
		// 			//   Helper.cl(matches?.[0]?.levels,"al matches?.[0]?.levels");
		// 			return { label: e.name, value: e.id, checked: matches?.[0]?.levels?.filter((l) => l.id == e.id).length };
		// 		}),
		// 		isDisabled: !(matches && matches.length),
		// 		onChange: (e) => {
		// 			changeLevels(e, c.id);
		// 		},
		// 	});
		// 	// RMulticheckboxDropdown
		// 	return;
		// });
		//   agreement.details.map((d)=>{

		//Helper.cl(form2,"form2")
		return form2;

		  //});
	};
	useEffect(() => {
		//Helper.cl("constructForm");

		setForm1(constructForm());
	}, [
		courseCategory,
		courseName,
		courseCode,
		courseCredits,
	]);

	return (
		<>
		 {/* {courseImage?"course image not null":"no course Image"}
		 {courseName?"ss="+courseName:"Course Name null"} */}
		 {/* {Helper.js(categories,"cats")}  */}
		 {/* {Helper.js(courseCategory,"courseCategory")} 
		 {Helper.js(courseName,"courseName")} 
		 {Helper.js(courseCredits,"courseCredits")}  */}
		 {/* {Helper.js(courseImage,"course Image")} */}
		 {/* {Helper.js(courseDescription)} */}
		  {/* {Helper.js(courseCategory)}  */}
		<RFormModal
				title={mode=="add"?"Create New Course":"Edit course info"}
				open={openAdd}
				setOpen={setOpenAdd}
				actionButtonText={mode=="add"?"Create":"ٍSave"}
				alignActionButtons="left"
				initialformItems={form1}
				withCloseIcon={false}
				actionButtonDisabled={requesting}
				//submitLabel={}
				onFormSubmit={(e) => {
					addSubmit();
				}}
				modalSize="m"
		/>
		</>
	);
};

export default GCourseAdd;
