import React, { useState, useEffect } from "react";

import { baseURL, genericPath } from "engine/config";

import tr from "components/Global/RComs/RTranslator";

import RFlex from "components/Global/RComs/RFlex/RFlex";
import { Services } from "engine/services";
import moment from "moment";
import DefaultImage from "assets/img/new/course-default-cover.png";
import iconsFa6 from "variables/iconsFa6";
import { successColor } from "config/constants";
import { warningColor } from "config/constants";
import RCard from "components/Global/RComs/RCards/RCard";
import { Link } from "react-router-dom/cjs/react-router-dom.min";
import RCatalogCard from "components/Global/RComs/RCards/RCatalogCard";
import REmptyData from "components/RComponents/REmptyData";
const GCourseCards = ({ courses, newCourseCallback, publishAndUnPublishCourseAsync, editCourseInformation, handleRemoveCourse }) => {
	const [transferedCourses, setTransferedCourses] = useState([]);

	useEffect(() => {
		buildCourses(courses);
	}, [courses]);

	const buildCourses = (courses2) => {
		let courses1 = [];
		courses2 &&
			courses2.length &&
			courses2?.map((course, index) => {
				let data = {};

				data.id = course.id;
				data.image = course.image_id == undefined ? DefaultImage : `${Services.courses_manager.file + course.image_id}`;
				data.icon = course.icon_id == undefined ? undefined : `${Services.courses_manager.file + course.icon_id}`;
				data.categoryName = course.category_name;
				data.color = course.color;
				data.name = course.name;
				data.users = course.teachers ? course.teachers : course.teachers;
				data.isFree = course?.extra?.isFree;
				data.startedAt = moment(course.extra.start_date).format("YYYY-MM-DD");
				data.isNew = course?.isNew;
				data.isPublished = course?.is_published;
				data.isCloned = course?.is_cloned;
				data.isMaster = course?.is_master;
				data.isOnline = course?.extra?.isOnline;
				data.isOneDayCourse = course?.isOneDayCourse;
				data.comingSoon = course?.comingSoon;
				data.discount = course?.discount;
				data.newPrice = course?.newPrice;
				data.oldPrice = course?.oldPrice;
				data.isInstance = course?.isInstance;
				data.description = course?.overview?.description;
				data.actions = [
					{
						id: 0,
						name: course.is_published ? tr`deactivate` : tr`activate`,
						icon: course.is_published ? iconsFa6.pen : iconsFa6.check,
						color: course.is_published ? warningColor : successColor,
						//hidden: course.is_master,
						onClick: () => {
							publishAndUnPublishCourseAsync(course.id, course.category_id, course.is_published);
						},
					},
					{
						id: 1,
						name: tr`Edit information`,
						icon: iconsFa6.pen,

						onClick: () => {
							editCourseInformation(course.id);
						},
					},
					{
						id: 4,
						name: tr`delete`,
						icon: iconsFa6.delete,
						color: "red",
						onClick: () => {
							handleRemoveCourse(course.id);
						},
					},
				];
				data.link = course.category_id
					? `${baseURL}/${genericPath}/course-management/course/${course.id}/cohort/${course.cohort_id}/category/${course.category_id}/modules`
					: `${baseURL}/${genericPath}/course-management/course/${course.id}/cohort/${course.cohort_id}/modules`;
				courses1.push(data);
			});
		setTransferedCourses(courses1);
	};

	return (
		<>
			{/* {Helper.js(courses)} */}
			<RFlex styleProps={{ flexWrap: "wrap", gap: 30 }}>
				{/* {alert} */}
				{transferedCourses && transferedCourses?.length > 0 ? (
					transferedCourses.map((course) => (
						<RCatalogCard course={course} key={course.id} forceStyle={{ width: "214px", height: "294px" }} catalogMode={true} />
					))
				) : (
					<RFlex styleProps={{ justifyContent: "center", alignItems: "center", width: "100%" }}>
						<REmptyData
							line1={<div style={{ weight: "400", fontSize: "12px" }}>No Courses Yet</div>}
							line2={
								<div style={{ weight: "400", fontSize: "14px" }}>
									Click On <Link onClick={newCourseCallback}>New Course</Link> To Start{" "}
								</div>
							}
						/>
					</RFlex>
				)}
			</RFlex>
		</>
	);
};

export default GCourseCards;
