import RFlex from 'components/Global/RComs/RFlex/RFlex'
import React, { useState } from 'react'
import styles from './SchoolLister.module.scss'
import RSchoolInfoV2 from './RSchoolInfoV2'
import GEducationStageV2 from './GEducationStageV2'
import RTextIcon from 'components/Global/RComs/RTextIcon/RTextIcon'
import { useDispatch, useSelector } from 'react-redux'
import * as actions from 'store/actions/global/schoolManagement.action'
import Loader from 'utils/Loader'
import { types, organizationTypes } from './constants'
import Swal, { DANGER } from 'utils/Alert'
import tr from "components/Global/RComs/RTranslator";
import moment from 'moment'
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import GDetailsListerV2 from './GDetailsListerV2'
import { useFetchDataRQ } from 'hocs/useFetchDataRQ'
import { schoolManagementV2API } from 'api/SchoolManagementV2'
import { useHistory } from 'react-router-dom'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import { baseURL } from 'engine/config'
import { genericPath } from 'engine/config'
import RLoader from 'components/Global/RComs/RLoader'
import { useMutateData } from 'hocs/useMutateData'
import { useCUDToQueryKey } from 'hocs/useCUDToQueryKey'
const GSchoolListerV2 = () => {
    const { CUDToQueryKey, operations } = useCUDToQueryKey()
    const user = useSelector((state) => state?.auth);
    const history = useHistory()
    // const organization_id = user && user?.user && user?.user?.organization_id;
    const organization_id = 1
    // const { school, stages, loading, error, info } = useSelector((state) => state?.schoolManagementRed)
    const dispatch = useDispatch()
    //---------------------------State management and vaidation-----------------------------------
    const validationSchema = Yup.object({
        educationStages: Yup.array().of(
            Yup.object({
                name: Yup.string().required("name is required")
            })
        ),
        gradeLevels: Yup.array().of(
            Yup.object({
                name: Yup.string().required("name is required")
            })
        ),
        curriculas: Yup.array().of(
            Yup.object({
                name: Yup.string().required("name is required")
            })
        ),
        periods: Yup.array().of(
            Yup.object({
                title: Yup.string().required("Title is Required"),
                from: Yup.string()
                    .required('From Date is required')
                    .matches(/^([01]?[0-9]|2[0-3]):[0-5][0-9]$/, 'From time must be in the format HH:mm (24-hour format)'),
                to: Yup.string()
                    .required("To Date is Required")
                    .matches(/^([01]?[0-9]|2[0-3]):[0-5][0-9]$/, 'From time must be in the format HH:mm (24-hour format)')
                    .test('is-greater-than-from-time', 'End time must be after from time', (value, context) => {
                        const { from } = context.parent;
                        const startTime = new Date(`1970-01-01T${from}:00`).getTime();
                        const endTime = new Date(`1970-01-01T${value}:00`).getTime();
                        return endTime > startTime;
                    }),

            })
        )
    })
    const initialStateValues = {
        schoolData: {},
        educationStages: [],
        gradeLevels: [],
        curriculas: [],
        periods: [],
        assessments: [],
        students: [],
        principals: [],
        seniorTeachers: [],
        courses: [],
        levelsProperites: {
            openStageId: null, openGradeLevelId: null, openCurriculaId: null,
            openStageIndex: null, openGradeLevelIndex: null, openCurriculaIndex: null
            // activeStageId: null, activeGradeLevelId: null, activeCurriculaId: null
        }

        // organizationTypes: {},
    }
    const { values: schoolState, touched, errors, setFieldValue, setFieldTouched,
        setFieldError, handleBlur, handleChange } = useFormik({
            initialValues: initialStateValues,
            validationSchema: validationSchema
        })


    //---------------------------------Get School Tree-------------------------------------------
    // const { data: organizationTypes, isLoading: isLoadingOrgnanizationTypes } = useFetchDataRQ({
    //     queryKey: [types.OrgnaizaitonTypes, organization_id],
    //     queryFn: () => schoolManagementV2API.getOrganizationTypes(),
    //     onSuccessFn: (data) => {
    //         data.data.data.level_types.reduce((result,item)=>{

    //             return resultg
    //         },{})
    //     }
    // })

    const { data: schoolTree, isLoading: isLoadingSchoolTree, isError, error } = useFetchDataRQ({
        queryKey: [types.Tree, organization_id],
        queryFn: () => schoolManagementV2API.getOrganization(organization_id),
        onSuccessFn: (data) => {
            if (!data?.data?.data?.checked) {
                history.replace(`${baseURL}/${genericPath}/school-management/create`)
            }
            storeSchoolData(data?.data?.data)
            storeLevelData(data?.data?.data?.org_levels)
        }
    })

    const storeSchoolData = (data) => {
        const schoolData = {
            id: data.id, name: data.name, type: data.organization_type_id,
            logo: data.logo, firstTime: data.checked
        }
        setFieldValue(types.SchoolData, schoolData)
    }
    const storeLevelData = (levels) => {
        let educationStages = []
        let gradeLevels = []
        let curriculas = []
        let courses = []
        let periods = []
        let assessments = []
        let principals = []
        let seniorTeachers = []
        let students = []
        educationStages = levels?.map((educationStage, educationIndex) => {
            gradeLevels = gradeLevels.concat(educationStage?.levels?.map((gradeLevel, gradeIndex) => {
                curriculas = curriculas.concat(gradeLevel?.levels?.map((curricula, curriculaIndex) => {
                    courses = courses.concat(curricula?.levels?.map((course, index) => {
                        const { levels, ...other } = course
                        return other
                    }))
                    const { levels, ...other } = curricula
                    curricula?.contents?.period ?
                        periods = periods.concat(curricula?.contents?.period) : ""
                    return other
                }))
                const { levels, ...other } = gradeLevel
                gradeLevel?.contents?.period ?
                    periods = periods.concat(gradeLevel?.contents?.period) : ""
                gradeLevel?.contents?.assessment_scheduler ?
                    assessments = assessments.concat(gradeLevel?.contents?.assessment_scheduler) : ""
                gradeLevel?.users?.senior_teacher ?
                    seniorTeachers = seniorTeachers.concat(
                        gradeLevel?.users?.senior_teacher.map((teacher) => ({ ...teacher, level_id: gradeLevel.id }))
                    ) : ":"
                gradeLevel?.users?.student ?
                    students = students.concat(
                        gradeLevel?.users?.student.map((student) => ({ ...student, level_id: gradeLevel.id }))
                    ) : ""
                return other
            }))
            const { levels, ...other } = educationStage
            educationStage?.contents?.period ?
                periods = periods.concat(educationStage?.contents?.period) : ""
            educationStage?.contents?.assessment_scheduler ?
                assessments = assessments.concat(educationStage?.contents?.assessment_scheduler) : ""
            educationStage?.users?.principle ?
                principals = principals.concat(
                    educationStage?.users?.principle.map((principal) => ({ ...principal, level_id: educationStage.id }))
                ) : ""
            return other
        })
        const sortedEducations = educationStages.filter((item) => item)
            .sort((firstEd, secondEd) => firstEd.order - secondEd.order)
        const sortedGrades = gradeLevels.filter((item) => item)
            .sort((firstGr, secondGr) => firstGr.order - secondGr.order)
        const sortedCurr = curriculas.filter((item) => item)
            .sort((firstCurr, secondCurr) => firstCurr.order - secondCurr.order)
        const sortedCourses = courses.filter((item) => item)
            .sort((firstCourse, secondCourse) => firstCourse.order - secondCourse.order)
        const formatedPeriods = periods.map((period, index) => ({
            ...period,
            from: moment(period.from).format("HH:mm"),
            to: moment(period.to).format("HH:mm"),
            saved: true,
            index
        }))
        const formatedAssessments = assessments.map((assessment, index) => ({
            ...assessment,
            saved: true,
            index
        }))
        setFieldValue(types.EducationStages, sortedEducations)
        setFieldValue(types.GradeLevels, sortedGrades)
        setFieldValue(types.Curriculas, sortedCurr)
        setFieldValue(types.Courses, sortedCourses)
        setFieldValue(types.Periods, formatedPeriods)
        setFieldValue(types.Assessments, formatedAssessments)
        setFieldValue(types.Principals, principals)
        setFieldValue(types.SeniorTeacher, seniorTeachers)
        setFieldValue(types.Students, students)
    }
    console.log("StatesStatesValues", schoolState)
    console.log("StatesStatesTouched", touched)
    console.log("StatesStatesErrors", errors)
    const handleChangingLevelsProperties = (newProperties) => {
        setFieldValue(types.LevelsProperites,
            {
                ...schoolState.levelsProperites,
                ...newProperties
            })
    }
    const currentStage = schoolState.educationStages?.[schoolState?.levelsProperites?.openStageIndex]
    const currentGrade = schoolState.gradeLevels?.[schoolState?.levelsProperites?.openGradeLevelIndex]

    const deleteEducationStageMutaion = useMutateData({
        queryFn: ({ levelId }) => schoolManagementV2API.deleteLevel(levelId),
        // invalidateKeys: [types.Tree, organization_id],
        onSuccessFn: ({ variables }) => {
            const filteredArray = schoolState.educationStages.filter((stage) => stage.id != variables.levelId)
            setFieldValue(types.EducationStages, filteredArray)
        }
    })
    const handleDeleteEducationStage = ({ stageId, order }) => {
        if (stageId > 0) {
            deleteEducationStageMutaion.mutate({ levelId: stageId })
        } else {
            const filteredArray = schoolState.educationStages.filter((stage) => stage.fakeId ? stage.fakeId != stageId : true)
            setFieldValue(types.EducationStages, filteredArray)
        }

    };


    const generateEducatoinComponent = () => {
        console.log("hereherehere")
        const heighstOrder = schoolState.educationStages.reduce((max, stage) => stage.order > max ? stage.order : max, 0)
        const lastId = schoolState.educationStages?.length + 1
        setFieldValue(types.EducationStages, [
            ...schoolState.educationStages,
            { fakeId: -lastId, order: heighstOrder + 1, name: '', content_type_id: organizationTypes[types.EducationStages] }
        ])
    };

    const reOrderLevelsMutation = useMutateData({
        queryFn: ({ payload }) => schoolManagementV2API.reOrderLevels(payload),
        // invalidateKeys: [types.Tree, organization_id],
    })

    const getRightIndexes = (parentId, sourceIndex, destinationIndex, type) => {
        const levelsForParent = schoolState?.[type].filter((level, index) => level.parent_level_id == parentId)
        const sourceLevel = levelsForParent[sourceIndex]
        const destinationLevel = levelsForParent[destinationIndex]
        const sourceLevelRealIndex = schoolState?.[type]?.findIndex((level) => {
            if (sourceLevel.fakeId)
                return sourceLevel.fakeId == level.fakeId
            else if (sourceLevel.id)
                return sourceLevel.id == level.id
        })
        const destinationLevelRealIndex = schoolState?.[type]?.findIndex((level) => {
            if (destinationLevel.fakeId)
                return destinationLevel.fakeId == level.fakeId
            else if (destinationLevel.id)
                return destinationLevel.id == level.id
        })
        return { a: sourceLevelRealIndex, b: destinationLevelRealIndex }
    }
    const handleLevelsOrder = (parentId, fakeSourceIndex, fakeDestinationIndex, type) => {
        let sourceIndex = fakeSourceIndex
        let destinationIndex = fakeDestinationIndex
        if (type != types.EducationStages) {
            const { a, b } = getRightIndexes(parentId, fakeSourceIndex, fakeDestinationIndex, type)
            sourceIndex = a
            destinationIndex = b
        }
        const educationArray = JSON.parse(JSON.stringify(schoolState[type]))
        const [element] = educationArray.splice(sourceIndex, 1);
        educationArray.splice(destinationIndex, 0, element);
        let sortedEducations = educationArray.map((stage, index) => ({ ...stage, order: index + 1 }))
        let backSortedEducations = JSON.parse(JSON.stringify(sortedEducations))
        if (schoolState[type].filter((stage) => stage.fakeId).length > 0)
            backSortedEducations = backSortedEducations.filter((stage) => stage.id)
        setFieldValue(type, sortedEducations)
        const payload = {
            levels: backSortedEducations
        }

        reOrderLevelsMutation.mutate({ payload })
    }


    const onDragEnd = (result) => {
        console.log(result)
        const sourceIndex = result?.source?.index
        const destinationIndex = result?.destination?.index
        if (sourceIndex == destinationIndex || !result.source || !result.destination) {
            return
        }
        const matchForType = result.type.match(/level:(\w+)/)
        const matchForId = result.type.match(/type:(\d+)/);
        const type = matchForType ? matchForType[1] : null;
        const id = matchForId ? parseInt(matchForId[1], 10) : null;
        console.log("id isaa", id)
        switch (type) {
            case types.School:
                handleLevelsOrder(id, sourceIndex, destinationIndex, types.EducationStages)
                return
            case types.EducationStages:
                handleLevelsOrder(id, sourceIndex, destinationIndex, types.GradeLevels)
                return
            case types.GradeLevels:
                handleLevelsOrder(id, sourceIndex, destinationIndex, types.Curriculas)
                return
            default:
                console.log("No Matched Type")
        }
    }
    if (isLoadingSchoolTree) {
        return <RLoader />
    }
    console.log("LevelLevel Stage", currentStage)
    console.log("LevelLevel Grade", currentGrade)
    return <RFlex id="Big Container" styleProps={{ gap: '12px' }}>
        <RFlex id="Left Container" styleProps={{ gap: "10px" }} className={styles.LeftContainer} >
            <RFlex id="Collapsses Container" styleProps={{ gap: "15px", flexDirection: "column" }} >
                <DragDropContext onDragEnd={onDragEnd} >
                    <Droppable droppableId={`level:${types.School},id:${organization_id}`} type={`level:${types.School},type:${organization_id}`}>
                        {(provided, snapshot) => (
                            <div className='d-flex flex-column' style={{ gap: '10px' }} ref={provided.innerRef} >
                                {schoolState?.educationStages?.map((educationStage, index) => {
                                    console.log("schoolState?.educationStages", schoolState?.educationStages)
                                    const currentStageGrades = schoolState.gradeLevels.filter((grade) => grade.parent_level_id == educationStage.id)
                                    return (
                                        <Draggable key={`item:${types.EducationStages},key:${educationStage.fakeId ? String(educationStage.fakeId) : String(educationStage.id)}`} draggableId={`item:${types.EducationStages},id:${educationStage.fakeId ? String(educationStage.fakeId) : String(educationStage.id)}`} index={index}>
                                            {(provided, snapshot) => (
                                                <div>
                                                    <div ref={provided.innerRef}
                                                        {...provided.draggableProps}
                                                        {...provided.dragHandleProps}>
                                                        <GEducationStageV2
                                                            id={educationStage.fakeId ?? parseInt(educationStage.id)}
                                                            currentStage={educationStage}
                                                            index={index}
                                                            gradeLevels={currentStageGrades}
                                                            levelsProperites={schoolState?.levelsProperites}
                                                            openedEducationStageId={schoolState?.levelsProperites?.openStageId}
                                                            openedGradeLevelId={schoolState?.levelsProperites?.openGradeLevelId}
                                                            setFieldValue={setFieldValue}
                                                            educationStages={schoolState.educationStages}
                                                            allGradeLevels={schoolState.gradeLevels}
                                                            allCurriculas={schoolState.curriculas}
                                                            courses={schoolState.courses}
                                                            handleChangingLevelsProperties={handleChangingLevelsProperties}
                                                            reOrderEducationStageMutation={reOrderLevelsMutation}
                                                            reOrderGradeLevelMutation={reOrderLevelsMutation}
                                                            reOrderCurriculaMutation={reOrderLevelsMutation}
                                                            onDelete={handleDeleteEducationStage}

                                                        />
                                                    </div>
                                                    {provided.placeholder}

                                                </div>
                                            )}

                                        </Draggable>
                                    );
                                })}
                                {provided.placeholder}

                            </div>
                        )}

                    </Droppable>
                </DragDropContext>
            </RFlex>
            <RTextIcon
                flexStyle={{ paddingLeft: "10px", cursor: 'pointer', gap: '5px', width: 'fit-content' }}
                text={tr("Add_Education_Stage")}
                onlyText
                textStyle={{ color: '#668ad7' }}
                onClick={generateEducatoinComponent}
            />
        </RFlex>
        <RFlex id="RightContainer" className={styles.RightContainer} styleProps={{ gap: '8px' }}>
            <RSchoolInfoV2 info={{}} />
            <RFlex id="Details Conteiner" className={styles.DetailsConteiner} styleProps={{ gap: '25px' }}>
                <GDetailsListerV2
                    periods={schoolState.periods}
                    users={{
                        [types.Principals]: schoolState[types.Principals],
                        [types.SeniorTeacher]: schoolState[types.SeniorTeacher],
                        [types.Students]: schoolState[types.Students]
                    }}
                    errors={errors}
                    touched={touched}
                    activeLevel={currentGrade ?? currentStage}
                    currentType={currentStage == undefined ? types.School : currentGrade == undefined ? types.EducationStages : types.GradeLevels}
                    handleBlur={handleBlur}
                    handleChange={handleChange}
                    setFieldValue={setFieldValue}
                    setFieldTouched={setFieldTouched}
                    setFieldError={setFieldError}
                    assessments={schoolState.assessments}
                />
            </RFlex>
        </RFlex>
    </RFlex >

}

export default GSchoolListerV2