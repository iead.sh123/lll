export const types = {
    Tree: 'tree',
    School: 'school',
    EducationStages: 'educationStages',
    GradeLevels: 'gradeLevels',
    Curriculas: 'curriculas',
    OrgnaizaitonTypes: 'organizationTypes',
    Courses: 'courses',
    Periods: 'periods',
    Assessments: 'assessments',
    Attendance: 'attendance',
    Curriculums: 'curriculums',
    Levels: "levels",
    SchoolData: 'schoolData',
    LevelsProperites: "levelsProperites",
    OpenStageId: "openStageId",
    OpenGradeLevelId: "openGradeLevelId",
    OpenStageIndex: "openStageIndex",
    OpenGradeLevelIndex: "openGradeLevelIndex",
    OpenCurriculaIndex: "openCurriculaIndex",
    OpenCurriculaId: "openCurriculaId",
    ActiveStageId: "activeStageId",
    ActiveGradeLevelId: "activeGradeLevelId",
    ActiveCurriculumId: "activeCurriculumId",
    Principals: 'principals',
    SeniorTeacher: 'seniorTeachers',
    Students: 'students',
}

export const dataTypes = {
    Principals: 'principals',
    SeniorTeacher: 'seniorTeachers',
    Students: 'students',
}

export const scaleTypes = {
    Letter: 'letter',
    Min: 'min',
    Max: 'max',
    GPA: 'GPA'
}
export const assessmentsTypes = {
    Name: 'name',
    Timestamp: 'timestamp'
}
export const fieldProperties = [
    { label: "Name", value: 'name' },
    { label: "Description", value: 'description' },
]
export const organizationTypes = {
    [types.EducationStages]: 1,
    [types.GradeLevels]: 2,
    [types.Curriculas]: 6,
    [types.Periods]: 4,
    [types.Attendance]: 5,
    [types.Courses]: 3,
    [types.Curriculums]: 7
}
export const userTypes = {
    [types.Principals]: 1,
    [types.SeniorTeacher]: 3,
    [types.Students]: 2
}
export const allTypes = {
    [types.School]: 1
}