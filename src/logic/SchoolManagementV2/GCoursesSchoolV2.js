import React, { useEffect, useState } from 'react'
import RFlex from 'components/Global/RComs/RFlex/RFlex'
import RSearchHeader from 'components/Global/RComs/RSearchHeader/RSearchHeader'
import RSelect from 'components/Global/RComs/RSelect'
import RLister from 'components/Global/RComs/RLister'
import * as actions from 'store/actions/global/schoolManagement.action'
import { useDispatch, useSelector } from 'react-redux'
import tr from 'components/Global/RComs/RTranslator'
import iconsFa6 from 'variables/iconsFa6'
import RProfileName from 'components/Global/RComs/RProfileName/RProfileName'
import NewPaginator from 'components/Global/NewPaginator/NewPaginator'
import Loader from 'utils/Loader'
import { useFetchDataRQ } from 'hocs/useFetchDataRQ'
import { schoolManagementV2API } from 'api/SchoolManagementV2'
import RLoader from 'components/Global/RComs/RLoader'
import { useMutateData } from 'hocs/useMutateData'
import { organizationTypes, types } from './constants'
const GCoursesSchoolV2 = ({
    curriculaId,
    handleCloseCoursesModal,
    isModal,
    allCurriculasCourses = [],
    currentCurriculaCourses,
    setFieldValue
}) => {
    console.log("jjjj", currentCurriculaCourses)
    const [searchData, setSearchData] = useState('')
    const [backSearchData, setBackSearchData] = useState('')
    const [currentCategoryId, setCurrentCategoryId] = useState(null)
    const [page, setPage] = useState(1)
    const organization_id = 1
    const handleSearch = (clearData) => {
        const data = clearData == "" ? clearData : searchData
        setBackSearchData(data)
    }

    const handleSelectCategory = (categoryId) => {
        setCurrentCategoryId(categoryId)
    }
    const handleChangeCoursePage = (page) => {
        setPage(page)
    }
    const addCourseToCurriculaMutation = useMutateData({
        queryFn: ({ payload }) => schoolManagementV2API.createUpdateLevel(payload, organization_id),
        invalidateKeys: ["Courses", backSearchData, currentCategoryId?.id, page],
        onSuccessFn: (data) => {
            setFieldValue(`${types.Courses}`, [...allCurriculasCourses, { ...data?.data?.data }])
        }
    })
    const handleCourseClicked = (course) => {
        const payload = {
            name: course.name,
            order: currentCurriculaCourses.length <= 0 ? 1 : currentCurriculaCourses.length,
            entity_id: course.id,
            content_type_id: organizationTypes[types.Courses],
            parent_level_id: curriculaId
        }
        addCourseToCurriculaMutation.mutate({ payload })
        // isModal && handleCloseCoursesModal()
        // console.log(courseId)
    }
    const { data: courses, isLoading: isLoadingCourses } = useFetchDataRQ({
        queryKey: ["Courses", backSearchData, currentCategoryId?.id, page],
        queryFn: () => schoolManagementV2API.getCoursesByCategoryId({
            curriculaId,
            name: backSearchData,
            category_id: currentCategoryId?.id,
            page
        })
    })
    const { data: categories, isLoading: isLoadingCategories } = useFetchDataRQ({
        queryKey: ['categories'],
        queryFn: () => schoolManagementV2API.getAllCategories(),

    })

    const _records = courses?.data?.data?.data?.map((course) => {
        const couresNameComponent = <RProfileName
            name={course.name}
            img={course.image_id ? course.image_id : null}
            imgStyle={{ width: '18px', height: '18px' }}
        />
        return {
            details: [
                { key: tr`course_name`, value: couresNameComponent, type: 'component' },
                { key: tr`course_code`, value: course?.code ?? "-" },
                { key: tr`credit`, value: course?.credits },
            ],
            actions: [
                {
                    name: " ",
                    icon: iconsFa6.plus,
                    color: "primary",
                    outline: true,
                    loading: addCourseToCurriculaMutation.isLoading,
                    onClick: () => handleCourseClicked(course),
                },
            ],
        }
    })
    return (
        isLoadingCourses ? < Loader /> :
            <RFlex styleProps={{ flexDirection: "column" }}>
                <RFlex styleProps={{ justifyContent: "space-between" }}>
                    <RSearchHeader
                        // searchLoading={isLoadingCourses}
                        searchData={searchData}
                        handleSearch={handleSearch}
                        setSearchData={setSearchData}
                        handleChangeSearch={(value) => setSearchData(value)}
                    />
                    {isLoadingCategories ? <RLoader mini={true} /> :
                        <div style={{ width: "250px" }}>
                            <RSelect
                                option={[{ label: `--${tr`no_category`}--`, id: null }].concat(categories?.data?.data)}
                                closeMenuOnSelect={true}
                                value={currentCategoryId}
                                placeholder={tr`category`}
                                onChange={(e) => handleSelectCategory(e)}
                            />
                        </div>
                    }
                </RFlex>
                <RLister
                    Records={_records}
                    withPagination={true}
                    info={courses}
                    handleChangePage={handleChangeCoursePage}
                    page={page} />
            </RFlex>
    )
}

export default GCoursesSchoolV2