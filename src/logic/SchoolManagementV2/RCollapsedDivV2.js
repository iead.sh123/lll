import React, { useState, useEffect, useRef } from 'react'
import { Collapse, Input } from 'reactstrap';
import RFlex from 'components/Global/RComs/RFlex/RFlex';
import RTextIcon from 'components/Global/RComs/RTextIcon/RTextIcon'
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import * as colors from 'config/constants'
import styles from './SchoolLister.module.scss'
import { types } from './constants';
import GCurriculaCourseV2 from './GCurriculaCourseV2';
import Loader from 'utils/Loader';
import RDropdownIcon from 'components/Global/RComs/RDropdownIcon/RDropdownIcon';
import iconsFa6 from 'variables/iconsFa6';
import { primaryColor, greyColor, boldGreyColor } from "config/constants";
import tr from 'components/Global/RComs/RTranslator';
import RHoverInput from 'components/Global/RComs/RHoverInput/RHoverInput';
const RCollapsedDiv = (
    {
        handleInputChange,
        saveClicked,
        handleInputDown,
        collapseChilds,
        number,
        currentType,
        childType,
        inputValue,
        inputPlaceHolder,
        saved,
        handleDivClicked,
        handleEditFunctionality,
        handleDeleteFunctionality,
        handleSaveButton,
        generateChildComponent,
        buttonText,
        isOpen = false,
        setOpenCurriculaCategories,
        id,
        index,
        order,
        addClicked,
        isItActive,
        openCourseCataloug,
        isLoading
    }
) => {

    let inputIsNotValidate = false
    let addButtonDisable = false
    inputIsNotValidate = (saveClicked && inputValue == "") ? true : false
    addButtonDisable = (id <= 0 && addClicked) ? true : false
    const leftMarginDiv = currentType === types.EducationStages ? '0px' : currentType === types.GradeLevels ? '20px' : '40px'
    const leftMarginAddingButton = currentType === types.EducationStages ? '20px' : currentType === types.GradeLevels ? '40px' : '60px'
    let openIndexType = ''

    if (currentType == types.EducationStages) {
        openIndexType = "gradeLevelOpenIndex"
    }

    else if (currentType === types.Curriculas) {
        openIndexType = "curricluaOpenIndex"
    }
    const [isHovering, setIsHovering] = useState(false)
    return (
        <RFlex id="InEducation"
            className="flex-column"
            styleProps={{ width: '100%' }}
        >
            <RFlex
                id="padding div"
                // className={currentType === types.Curriculas ? "flex-column" : "flex-row"}
                styleProps={{
                    gap: '0px', height: "42px", padding: !isHovering ? "7px 20px" : '0px 20px',
                    cursor: "pointer", boxShadow: "0px 0px 8px 0px #E4E4E4",
                    marginLeft: leftMarginDiv, position: 'relative'
                }}
                className={isItActive ? styles.ActiveCollapse : ""}
                onClick={() => { handleDivClicked(id) }}
                onMouseEnter={() => { setIsHovering(true) }}
                onMouseLeave={() => { setIsHovering(false) }}
            >

                <RFlex
                    id="Spacebetween div"
                    styleProps={{ minHeight: isHovering ? "38px" : '100%', width: '100%' }}
                    className="justify-content-between align-items-center">
                    {/* between input and save button */}
                    <RFlex
                        id="input and save div"
                        className=" align-items-center"
                        styleProps={{ gap: '18px', height: "100%", width: "320px" }} >
                        {/* just input field or (flex between value and edit and delete icon) */}
                        <RFlex
                            id="input or value and icons"
                            styleProps={{ height: '100%' }}
                            className=" align-items-center position-relative"
                        >
                            <RHoverInput
                                // name={`attendanceSettings.${index}.${header.value}`}
                                inputValue={`${inputValue}`}
                                inputClicked={(event) => { event.stopPropagation() }}
                                inputWidth={'250px'}
                                type={currentType}
                                focusOnInput={true}
                                inputPlaceHolder={inputPlaceHolder}
                                handleInputChange={handleInputChange}
                                inputIsNotValidate={inputIsNotValidate || addButtonDisable}
                                item={{}}
                                isLoading={isLoading}
                                handleOnBlur={handleSaveButton}
                                handleEditFunctionality={handleEditFunctionality}
                                saved={saved}
                            />
                            {isHovering &&
                                <i className={`${iconsFa6.delete} text-danger ${styles.deleteIcon}`} onClick={(event) => { event.stopPropagation(); handleDeleteFunctionality(id, order) }} />}
                        </RFlex>

                    </RFlex>
                    <span style={{ padding: '0px', margin: '0px', color: colors.greyColor, fontSize: '12px' }}>
                        {number > 0 ? `${number} ${childType}` : `no ${childType} Yet`}
                    </span>

                    <i
                        className={`${!isOpen ? "fa-solid fa-angle-right" : "fa-solid fa-angle-down"} ${isItActive ? 'text-primary' : ""}`}
                        style={{ width: "10px" }} />
                </RFlex>

            </RFlex>
            {

                currentType === types.Curriculas &&
                <Collapse isOpen={isOpen}>
                        <RFlex className="flex-column">
                            {collapseChilds?.length > 0 &&
                                <RFlex className="flex-column">{
                                    collapseChilds.map((course, id) => {
                                        return (
                                            <GCurriculaCourseV2 course={course} />
                                        )
                                    })}
                                </RFlex>}
                            <span
                                style={{ margin: '0px 0px 5px 60px', padding: '0px 15px', cursor: 'pointer' }}
                                className='text-primary'
                                onClick={openCourseCataloug}
                            >
                                {tr("Pick_From_Course_catalogue")}
                            </span>
                        </RFlex>
                    
                </Collapse>

            }
            {currentType != types.Curriculas &&
                <Collapse isOpen={isOpen}>
                    <RFlex id="collapse flex " className="flex-column" styleProps={{ gap: '5px' }}>
                        <Droppable droppableId={`level:${currentType},id:${id}`} type={`level:${currentType},type:${id}`}>
                            {(provided, snapshot) => (
                                <div
                                    ref={provided.innerRef}
                                    id='under Collapse Div'
                                    style={{ display: 'flex', flexDirection: 'column', gap: '5px' }}
                                >
                                    {console.log("!! ---------------------------------------------------------------------------------")}
                                    {collapseChilds.map((child, index) => {
                                        console.log('!!', `item:${currentType},key:${String(child.id)}`)
                                        return (
                                            <Draggable key={`item:${childType},key:${String(child.id)}`} draggableId={`item:${childType},id:${String(child.id)}`} index={index}>
                                                {(provided, snapshot) => (
                                                    <div>
                                                        <div ref={provided.innerRef}
                                                            {...provided.draggableProps}
                                                            {...provided.dragHandleProps}>
                                                            {child.component}
                                                        </div>
                                                        {provided.placeholder}

                                                    </div>
                                                )}
                                            </Draggable>

                                        )
                                    }
                                    )}
                                    {provided.placeholder}

                                </div>
                            )}

                        </Droppable>


                        {collapseChilds?.length === 0 ? (
                            <RFlex styleProps={{ marginLeft: leftMarginAddingButton }}>
                                <span
                                    style={{ padding: '0px', margin: '0px', color: "color: #5e5f63" }}>
                                    {currentType === types.EducationStages ? "No Grage Level Yet" : "No Curriculum Yet"}
                                </span>
                                {types.GradeLevels == currentType ? <RDropdownIcon
                                    dropdownClassname="btn-magnify"
                                    menuClassname={styles.CurriculaDropDown}
                                    dropdownStyle={{ width: 'fit-content' }}
                                    actions={[{ label: 'Create new', action: generateChildComponent }, { label: "Pick a category ", action: () => setOpenCurriculaCategories(true) }]}
                                    component={
                                        <RTextIcon
                                            icon={iconsFa6.chevronDown}
                                            text={tr`add_curriculum`}
                                            iconOnRight={true}
                                            flexStyle={{ color: primaryColor, cursor: "pointer", width: "fit-content" }}
                                        />}
                                // menuClassname={styles.SelectDropDown}
                                // iconContainerStyle={{ border: '1px solid' + colors.successColor, borderRadius: '100%' }}
                                /> :
                                    <RTextIcon
                                        flexStyle={{ cursor: 'pointer', gap: '5px' }}
                                        text={buttonText}
                                        onlyText
                                        textStyle={{ color: '#668ad7' }}
                                        onClick={generateChildComponent} />}
                            </RFlex>
                        ) : types.GradeLevels == currentType ? <RDropdownIcon
                            dropdownClassname="btn-magnify"
                            menuClassname={styles.CurriculaDropDown}
                            dropdownStyle={{ width: 'fit-content', marginLeft: leftMarginAddingButton }}
                            actions={[{ label: 'Create new', action: generateChildComponent }, { label: "Pick A category ", action: () => setOpenCurriculaCategories(true) }]}
                            component={
                                <RTextIcon
                                    icon={iconsFa6.chevronDown}
                                    text={tr`add_curriculum`}
                                    iconOnRight={true}
                                    flexStyle={{ color: primaryColor, cursor: "pointer", width: "fit-content" }}
                                />}
                        // menuClassname={styles.SelectDropDown}
                        // iconContainerStyle={{ border: '1px solid' + colors.successColor, borderRadius: '100%' }}
                        /> : <RTextIcon
                            flexStyle={{ cursor: 'pointer', gap: '5px', marginLeft: leftMarginAddingButton, width: 'fit-content' }}
                            text={buttonText}
                            onlyText
                            textStyle={{ color: '#668ad7' }}
                            onClick={generateChildComponent} />

                        }
                    </RFlex>

                </Collapse>
            }
        </RFlex >
    )
}

export default RCollapsedDiv




