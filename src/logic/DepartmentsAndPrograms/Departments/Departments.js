import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import tr from "components/Global/RComs/RTranslator";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import React, { useState } from "react";
import { Terms } from "../constants";
import { departmentsProgramsApi } from "api/DepartmetnsAndPrograms";
import RCollapseDiv from "components/RComponents/RCollapseDiv/RCollapseDiv";
import RLoader from "components/Global/RComs/RLoader";
import { Accordion, AccordionContent, AccordionItem, AccordionTrigger } from "ShadCnComponents/ui/accordion";

const Departments = () => {
	//--------------------------------Fetching Departments-------------------------------------
	const [frontDepartmentsSearchTerm, setFrontDepartmentsSearchTerm] = useState("");
	const [backDepartmentsSearchTerm, setBackDepartmentsSearchTerm] = useState("");
	const handleSearch = (clearData) => {
		let data = frontDepartmentsSearchTerm;
		if (clearData == "") {
			data = clearData;
		}
		setBackDepartmentsSearchTerm(data);
	};
	const {
		data: departments,
		isLoading: isLoadingDepartments,
		isFetching: isFetchingDepartments,
	} = useFetchDataRQ({
		queryKey: [Terms.Departments, backDepartmentsSearchTerm],
		queryFn: () => departmentsProgramsApi.getAllDepartemnts(),
	});
	if (isLoadingDepartments) {
		return <RLoader />;
	}
	return (
		<RFlex className="flex-col" styleProps={{ gap: "15px" }}>
			<span>{tr`All_Departments`}</span>
			<RFlex className="w-[430px]">
				<RButton text={tr("New_Department")} color="primary" className="m-0" onClick={() => {}} />
				<RSearchHeader
					// searchLoading={isFetching}
					inputPlaceholder={tr("search...")}
					searchData={frontDepartmentsSearchTerm}
					handleChangeSearch={setFrontDepartmentsSearchTerm}
					setSearchData={setFrontDepartmentsSearchTerm}
					handleSearch={handleSearch}
				/>
			</RFlex>
			<Accordion type="single" collapsible className="w-[700px]">
				{departments?.data?.data.map((department) => (
					<AccordionItem value={`item-${department.id}`}>
						<AccordionTrigger className="hover:text-themePrimary p-[10px]" actions>
							<RFlex className=" font-bold">{department.name}</RFlex>
						</AccordionTrigger>
						<AccordionContent className="p-[5px]">Yes. It adheres to the WAI-ARIA design pattern.</AccordionContent>
					</AccordionItem>
				))}
			</Accordion>

			{/* <RNewDropdown /> */}
		</RFlex>
	);
};

export default Departments;
