import React, { useContext, useState } from "react";
import { FormGroup, Input, FormText } from "reactstrap";
import { TermsManagementContext } from "logic/TermsManagement/GTermsManagement";
import { primaryColor } from "config/constants";
import { useFormik } from "formik";
import RRenderUserInfo from "components/Global/RComs/RRenderUserInfo";
import GEnrollUsers from "./GEnrollUsers";
import iconsFa6 from "variables/iconsFa6";
import AppModal from "components/Global/ModalCustomize/AppModal";
import RButton from "components/Global/RComs/RButton";
import styles from "./TermsManagement.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import * as yup from "yup";

const GHomeRoomEditor = () => {
	const TermsManagementData = useContext(TermsManagementContext);

	const [enrollUsers, setEnrollUsers] = useState(false);
	const [userType, setUserType] = useState("");

	const initialValues = {
		name: TermsManagementData.itemObj?.dataObj?.name || "",
		gradeLevelId: TermsManagementData.itemObj?.gradeLevelId || null,
		learners: TermsManagementData.valuesUsers?.studentsById || [],
		teachers: TermsManagementData.valuesUsers?.teachersById || [],
		searchText: "",
	};

	const formSchema = yup.object().shape({
		name: yup.string().required(tr`name is required`),
	});

	const handleFormSubmit = (values) => {
		const Values = { homerooms: [{ ...values }] };

		if (TermsManagementData.itemObj?.homeRoomId) {
			delete values.gradeLevelId;
			TermsManagementData.handleEditHomeRoom(values, TermsManagementData.itemObj?.homeRoomId);
		} else {
			TermsManagementData.handleAddHomeRoom(Values, TermsManagementData.itemObj.termId);
		}
	};

	const { values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue, resetForm } = useFormik({
		initialValues,
		onSubmit: handleFormSubmit,
		validationSchema: formSchema,
	});

	const getInputClass = (touched, errors, fieldName) => {
		return touched[fieldName] && errors[fieldName] ? "input__error" : "";
	};

	const handleOpenEnrollUsers = (uType) => {
		setEnrollUsers(true);
		setUserType(uType);
	};
	const handleCloseEnrollUsers = () => {
		setEnrollUsers(false);
		setUserType("");
		setFieldValue("searchText", "");
	};

	const handleSelectedUser = (userType, userId, hashId, userName) => {
		let updatedArray = [...values[userType]];

		const userIndex = updatedArray.findIndex((existingUserId) => existingUserId.user_id == userId);
		if (userIndex === -1) {
			updatedArray.push({ user_id: userId, hash_id: hashId, name: userName });
		} else {
			updatedArray = updatedArray.filter((existingUserId) => existingUserId.user_id !== userId);
		}
		setFieldValue(userType, updatedArray);
	};

	const RenderUsers = ({ users, userType }) => {
		return (
			<RFlex styleProps={{ flexWrap: "wrap", width: "100%" }}>
				{users?.map((user, index) => {
					return (
						<div className={styles.user} key={index}>
							<RRenderUserInfo
								name={user.name ?? user.full_name}
								image={user.hash_id ?? user.image}
								showAction={true}
								clickOnAction={() => handleSelectedUser(userType, user.user_id, user.hash_id, user.name)}
							/>
						</div>
					);
				})}
			</RFlex>
		);
	};

	return (
		<div>
			<AppModal
				size={"md"}
				show={enrollUsers}
				header={true}
				parentHandleClose={handleCloseEnrollUsers}
				headerSort={
					<GEnrollUsers
						enrollUsers={false}
						userType={userType}
						values={values}
						handleSelectedUser={handleSelectedUser}
						handleCloseEnrollUsers={handleCloseEnrollUsers}
						setFieldValue={setFieldValue}
					/>
				}
			/>

			<form onSubmit={handleSubmit}>
				<RFlex styleProps={{ width: "100%", flexDirection: "column", gap: "15px" }}>
					<RFlex styleProps={{ alignItems: "center" }}>
						<div className={styles.icon}>
							<i className={iconsFa6.userPlus + " fa-lg"} style={{ color: "white" }} />
						</div>
						<span>{TermsManagementData.itemObj?.homeRoomId ? tr`edit_home_room` : tr`create_new_home_room`}</span>
					</RFlex>
					<RFlex styleProps={{ width: "100%" }}>
						<FormGroup style={{ width: "100%" }}>
							<label>{tr("home_room")}</label>
							<Input
								name="name"
								type="text"
								value={values.name}
								placeholder={tr("home_room")}
								onBlur={handleBlur}
								onChange={handleChange}
								className={getInputClass(touched, errors, "name")}
							/>
							{touched.name && errors.name && <FormText color="danger">{errors.name}</FormText>}
						</FormGroup>
					</RFlex>

					{/* teachers */}
					<RFlex styleProps={{ width: "100%" }}>
						<span>{tr`teachers`}</span>
						<span
							onClick={() => handleOpenEnrollUsers("teachers")}
							style={{ color: primaryColor, cursor: "pointer" }}
						>{tr`enroll_teacher`}</span>
					</RFlex>
					<RFlex styleProps={{ width: "100%" }}>
						{values?.teachers?.length > 0 && <RenderUsers users={values?.teachers} userType={"teachers"} />}
					</RFlex>
					{/* students */}
					<RFlex styleProps={{ width: "100%" }}>
						<span>{tr`students`}</span>
						<span
							onClick={() => handleOpenEnrollUsers("learners")}
							style={{ color: primaryColor, cursor: "pointer" }}
						>{tr`enroll_students`}</span>
					</RFlex>
					<RFlex styleProps={{ width: "100%" }}>
						{values?.learners?.length > 0 && <RenderUsers users={values?.learners} userType={"learners"} />}
					</RFlex>

					{/* actions */}
					<RFlex styleProps={{ justifyContent: "end", width: "100%" }}>
						<RButton
							type="submit"
							text={TermsManagementData.itemObj?.homeRoomId ? tr`save` : tr`create`}
							color="primary"
							loading={
								TermsManagementData.itemObj?.homeRoomId ? TermsManagementData.editHomeRoomLoading : TermsManagementData.addHomeRoomLoading
							}
							disabled={
								TermsManagementData.itemObj?.homeRoomId ? TermsManagementData.editHomeRoomLoading : TermsManagementData.addHomeRoomLoading
							}
						/>
						<RButton
							text={tr`cancel`}
							color="link"
							onClick={() => {
								TermsManagementData.handleCloseHomeRoomModal();
								resetForm();
							}}
						/>
					</RFlex>
				</RFlex>
			</form>
		</div>
	);
};

export default GHomeRoomEditor;
