import React, { useContext } from "react";
import { TermsManagementContext } from "logic/TermsManagement/GTermsManagement";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useFormik } from "formik";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import iconsFa6 from "variables/iconsFa6";
import RSelect from "components/Global/RComs/RSelect";
import RLister from "components/Global/RComs/RLister";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const GCourses = ({ academicPath }) => {
	const TermsManagementData = useContext(TermsManagementContext);

	// - - - - - - - - - - - - - Formik - - - - - - - - - - - - - -
	const initialValues = {
		courseDepartments: [],
		searchText: "",
		groupingOfSearchFields: { name: "", department: undefined, page: 1 },
	};

	const { values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue, resetForm } = useFormik({
		initialValues,
	});

	// - - - - - - - - - - - - - Queries - - - - - - - - - - - - - -
	const masterCoursesData = useFetchDataRQ({
		queryKey: ["masterCourses", departmentId, values.groupingOfSearchFields],
		queryFn: () => courseApi.masterCourses(values.groupingOfSearchFields),

		onSuccessFn: ({ data }) => {
			setFieldValue("masterCourses", data?.data);
		},
	});

	const courseDepartmentsData = useFetchDataRQ({
		queryKey: ["courseDepartments"],
		queryFn: () => courseApi.courseDepartments(),
		onSuccessFn: ({ data }) => {
			setFieldValue(
				"courseDepartments",
				[
					{ label: tr`all`, value: undefined },
					{ label: tr`no_department`, value: "uncategorized" },
				].concat(
					data?.data?.department.map((item) => ({
						label: item.name,
						value: item.id,
					}))
				)
			);
		},
	});

	// - - - - - - - - - - - - - Data - - - - - - - - - - - - - -
	const _recordCMasterCourses = {
		data: masterCoursesData.data?.data,

		columns: [
			{
				accessorKey: "name",
				renderHeader: () => <span>{tr`course_name`}</span>,
				renderCell: ({ row }) => (
					<span className="cursor-pointer hover:text-themePrimary" onClick={() => handlePushToRubricEditor(row.original?.id)}>
						{row.original?.name}
					</span>
				),
			},
			{
				accessorKey: "code",
				renderHeader: () => <span>{tr`course_code`}</span>,
				renderCell: ({ row }) => <span>{`${row.original.code}`}</span>,
			},
			{
				accessorKey: "credits",
				renderHeader: () => <span>{tr`credit`}</span>,
				renderCell: ({ row }) => <span>{`${row.original.credits}`}</span>,
			},
		],
		actions: [
			{
				icon: iconsFa6.plus,
				actionIconClass: "text-themePrimary",
				onClick: ({ row }) => {
					TermsManagementData.handleSelectedCourseToLoading({ courseId: row.original.id });

					TermsManagementData.handleMakeCourseCloned({
						courseId: row.original.id,
						termId: TermsManagementData?.itemObj?.termId,
						curriculumId: TermsManagementData?.itemObj?.firstCurriculumID,
					});
				},
			},
		],
	};

	// - - - - - - - - - - - - - Handlers - - - - - - - - - - - - - -
	const handleSearch = (emptyData) => {
		setFieldValue("groupingOfSearchFields", { ...values.groupingOfSearchFields, name: emptyData !== undefined ? "" : values.searchText });
	};

	const handleSelectDepartment = (departmentId) => {
		setFieldValue("groupingOfSearchFields", {
			...values.groupingOfSearchFields,
			department: `${departmentId == "uncategorized" ? 0 : departmentId}`,
		});
	};

	const handleChangePage = (page) => {
		setFieldValue("page", page);
	};

	if (masterCoursesData?.isLoading) return <Loader />;
	return (
		<RFlex styleProps={{ flexDirection: "column" }}>
			<RFlex styleProps={{ justifyContent: "space-between" }}>
				<RSearchHeader
					searchLoading={masterCoursesData.isLoading}
					searchData={values.searchText}
					handleSearch={handleSearch}
					setSearchData={(value) => setFieldValue("searchText", value)}
					handleChangeSearch={(value) => setFieldValue("searchText", value)}
				/>

				<div style={{ width: "48%" }}>
					<RSelect
						option={values.courseDepartments}
						closeMenuOnSelect={true}
						placeholder={academicPath ? tr`program` : tr`category`}
						onChange={(e) => handleSelectDepartment(e.value)}
						isLoading={courseDepartmentsData.isLoading}
					/>
				</div>
			</RFlex>
			{/* - - - - - - - - - - - - - RLister - - - - - - - - - - - - - - */}
			<RLister
				Records={_recordCMasterCourses}
				convertRecordsShape={false}
				info={masterCoursesData}
				withPagination={true}
				handleChangePage={(event) => handleChangePage(event)}
				page={values.groupingOfSearchFields.page}
			/>{" "}
		</RFlex>
	);
};

export default GCourses;
