import React, { useState } from "react";
import Swal, { DANGER } from "utils/Alert";
import { genericPath, baseURL } from "engine/config";
import { termsManagementApi } from "api/global/termsManagement";
import { coursesManagerApi } from "api/global/coursesManager";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useMutateData } from "hocs/useMutateData";
import { useHistory, useLocation } from "react-router-dom/cjs/react-router-dom.min";
import { useFormik } from "formik";
import GMoveUsersFromHomeRoomToAnother from "./GMoveUsersFromHomeRoomToAnother";
import GCoursesFromSchoolCatalog from "./GCoursesFromSchoolCatalog";
import GCoursesByCurricula from "./GCoursesByCurricula";
import RTermsManagement from "view/TermsManagement/RTermsManagement";
import TermManagement_1 from "logic/TermManagement/TermManagement_1";
import GHomeRoomEditor from "./GHomeRoomEditor";
import GAcademicYear from "./GAcademicYear";
import GEnrollUsers from "./GEnrollUsers";
import GCourses from "./GCourses";
import AppModal from "components/Global/ModalCustomize/AppModal";
import Loader from "utils/Loader";
import GTerm from "./GTerm";
import tr from "components/Global/RComs/RTranslator";
import RControlledDialog from "components/RComponents/RControlledDialog";
import { courseApi } from "api/course";

export const TermsManagementContext = React.createContext();
const GTermsManagement = () => {
	const history = useHistory();
	const location = useLocation();
	const AcademicPath = location.pathname.includes("academics");

	const [isHovering, setIsHovering] = useState(null);
	const [openMoveUsersFromHomeRoomToAnother, setOpenMoveUsersFromHomeRoomToAnother] = useState(false);
	const [openedAcademicYearsCollapses, setOpenedAcademicYearsCollapses] = useState(null);
	const [openCoursesFromSchoolCatalog, setOpenCoursesFromSchoolCatalog] = useState(false);
	const [openedGradeLevelsCollapses, setOpenedGradeLevelsCollapses] = useState(null);
	const [openedTermsCollapses, setOpenedTermsCollapses] = useState(null);
	const [openUsersToEnroll, setOpenUsersToEnroll] = useState(false);
	const [openAcademicYear, setOpenAcademicYear] = useState(false);
	const [openHomeRoom, setOpenHomeRoom] = useState(false);
	const [openCourses, setOpenCourses] = useState(false);
	const [openCoursesCurricula, setOpenCoursesCurricula] = useState(false);
	const [openTerm, setOpenTerm] = useState(false);
	const [openWeeklySchedule, setOpenWeeklySchedule] = useState(false);

	const [showAllStudentsInGradeLevel, setShowAllStudentsInGradeLevel] = useState(false);
	const [showAllStudentsInHomeRoom, setShowAllStudentsInHomeRoom] = useState(false);
	const [showAllTeachersInHomeRoom, setShowAllTeachersInHomeRoom] = useState(false);

	const [activeGradeLevelInWizard, setActiveGradeLevelInWizard] = useState(false);
	const [highlightCollapse, setHighlightCollapse] = useState(null);
	const [showActions, setShowActions] = useState(null);
	const [activeNode, setActiveNode] = useState("");

	const [userType, setUserType] = useState(null);
	const [dirtyFlag, setDirtyFlag] = useState(false);

	const initialValues = {
		academicYearId: null,
		termId: null,
		gradeLevelId: null,
		homeRoomId: null,
		firstCurriculumID: null,
		allHomeRoomsToGradeLevel: [],
		dataObj: {},
		termActive: null,
		coursesByCurricula: [],
		idsLoading: [],
	};

	const initialValuesUsers = {
		students: [],
		teachers: [],
		studentsById: [],
		teachersById: [],
	};

	const { values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue, resetForm } = useFormik({
		initialValues,
	});

	const {
		values: valuesUsers,
		setFieldValue: setFieldValueUsers,
		resetForm: resetFormUsers,
	} = useFormik({
		initialValues: initialValuesUsers,
	});

	const { data, isLoading, isFetching, isError } = useFetchDataRQ({
		queryKey: ["academicYears"],
		queryFn: () => termsManagementApi.getAcademicYears(AcademicPath),
	});

	const addAcademicYearMutation = useMutateData({
		queryFn: (data) => termsManagementApi.addAcademicYear(data),
		invalidateKeys: ["academicYears"],
		closeDialog: () => setOpenAcademicYear(false),
	});

	const editAcademicYearMutation = useMutateData({
		queryFn: ({ data, id }) => termsManagementApi.editAcademicYear({ data, id }),
		invalidateKeys: ["academicYears"],
		closeDialog: () => {
			setOpenAcademicYear(false);
			resetForm();
		},
	});

	const deleteAcademicYearMutation = useMutateData({
		queryFn: (id) => termsManagementApi.deleteAcademicYear(id),
		invalidateKeys: ["academicYears"],
	});

	const addTermMutation = useMutateData({
		queryFn: ({ data, academicYearId }) => termsManagementApi.addTerm({ data, academicYearId }),
		invalidateKeys: ["academicYears"],
		closeDialog: () => {
			setOpenTerm(false);
			// resetForm();
		},
		onSuccessFn: ({ data }) => {
			handleChangeActiveNode({
				nodeName: "term",
				academicYearId: values.academicYearId,
				termId: data.data.id,
				gradeLevelId: null,
				homeRoomId: null,
				firstCurriculumID: null,
				allHomeRoomsToGradeLevel: null,
				data: data.data,
			});
			termsCollapses(`term_${values.academicYearId}_${data.data.id}`);
		},
	});

	const editTermMutation = useMutateData({
		queryFn: ({ data, id }) => termsManagementApi.editTerm({ data, id }),
		invalidateKeys: ["academicYears"],
		closeDialog: () => {
			setOpenTerm(false);
			resetForm();
		},
	});

	const deleteTermMutation = useMutateData({
		queryFn: (id) => termsManagementApi.deleteTerm(id),
		invalidateKeys: ["academicYears"],
		onSuccessFn: ({ data }) => {
			handleChangeActiveNode({
				nodeName: "academicYear",
				academicYearId: values.academicYearId,
				termId: null,
				gradeLevelId: null,
				homeRoomId: null,
				firstCurriculumID: null,
				allHomeRoomsToGradeLevel: null,
				data: data.data,
			});
			// academicYearsCollapses(`academicYear_${values.academicYearId}`);
			termsCollapses(`collapse`);
		},
	});
	const changeTermStatusMutation = useMutateData({
		queryFn: ({ status, termId }) => termsManagementApi.changeTermStatus({ status, termId }),
		invalidateKeys: ["academicYears"],
		onSuccessFn: ({ data }) => {
			setFieldValue("dataObj.isActive", data.data.isActive);
			setFieldValue("dataObj.isFinalized", data.data.isFinalized);
			setDirtyFlag(false);
		},
	});

	const addHomeRoomMutation = useMutateData({
		queryFn: ({ data, termId }) => termsManagementApi.addHomeRoom({ data, termId }),
		invalidateKeys: ["academicYears"],
		closeDialog: () => {
			setOpenHomeRoom(false);
			// resetForm();
		},

		onSuccessFn: ({ data }) => {
			handleChangeActiveNode({
				nodeName: "homeRoom",
				academicYearId: values.academicYearId,
				termId: values.termId,
				gradeLevelId: values.gradeLevelId,
				homeRoomId: data.data.id,
				firstCurriculumID: null,
				allHomeRoomsToGradeLevel: null,
				data: data.data[0],
			});
			// gradeLevelsCollapses(`gradeLevel_${values.academicYearId}_${values.termId}_${values.gradeLevelId}`);
		},
	});
	const key1 = `"student-users", ${values.dataObj.id}, "alphabiticallyDesc"`;
	const key2 = `"teacher-users", ${values.dataObj.id}, "alphabiticallyDesc"`;
	const editHomeRoomMutation = useMutateData({
		queryFn: ({ data, id }) => termsManagementApi.editHomeRoom({ data, id }),
		multipleKeys: true,
		invalidateKeys: [key1, key2],
		closeDialog: () => {
			setOpenHomeRoom(false);
			// resetForm();
		},
	});

	const deleteHomeRoomMutation = useMutateData({
		queryFn: (id) => termsManagementApi.deleteHomeRoom(id),
		invalidateKeys: ["academicYears"],
		onSuccessFn: ({ data }) => setActiveNode(""),
	});

	// const makeCourseClonedMutation = useMutateData({
	// 	queryFn: (data) => coursesManagerApi.clonedCourse(data),
	// 	invalidateKeys: ["academicYears"],
	// 	onSuccessFn: ({ data, variables }) => handleSelectedCourseToLoading({ courseId: variables.id }),
	// });

	const makeCourseClonedMutation = useMutateData({
		queryFn: ({ courseId, data }) => courseApi.clonedCourse({ courseId, data }),
		invalidateKeys: ["academicYears"],
		onSuccessFn: ({ data, variables }) => {
			handleSelectedCourseToLoading({ courseId: variables.courseId });
		},
		displaySuccess: true,
	});

	const unEnrollUsersFromHomeRoomMutation = useMutateData({
		queryFn: ({ data, homeRoomId }) => termsManagementApi.unEnrollUsersInHomeRoom({ data, homeRoomId }),
		// invalidateKeys: ["student-users", "teacher-users"],
		invalidateKeys: [" "],
	});

	const sendNotificationsToUsersSelectedInHomeRoomMutation = useMutateData({
		queryFn: ({ data }) => termsManagementApi.sendNotificationToUsers({ data }),
		invalidateKeys: [" "],
	});

	const moveUsersFromHomeRoomToAnotherOneMutation = useMutateData({
		queryFn: ({ data, sourceHomeRoomId, targetHomeRoomId }) =>
			termsManagementApi.changeUsersFromHomeRoomToAnotherOne({ data, sourceHomeRoomId, targetHomeRoomId }),
		// invalidateKeys: ["academicYears", "student-users", "teacher-users"],
		invalidateKeys: [" "],
		closeDialog: () => {
			setOpenMoveUsersFromHomeRoomToAnother(false);
			setFieldValueUsers("students", []);
			setFieldValueUsers("teachers", []);
		},
	});
	const teacher1 = ["teacher-users-to-course", values.dataObj.id, "alphabiticallyDesc"];
	const student1 = ["student-users-to-course", values.dataObj.id, "alphabiticallyDesc"];

	const unEnrollUsersFromCourseMutation = useMutateData({
		queryFn: ({ courseId, data }) => coursesManagerApi.unEnrollUsersFromCourse({ courseId, data }),
		invalidateKeys: [teacher1, student1],
		closeDialog: () => {
			setFieldValueUsers("students", []);
			setFieldValueUsers("teachers", []);
		},
		multipleKeys: true,
	});

	const enrollUsersToCourseMutation = useMutateData({
		queryFn: ({ courseId, data }) => coursesManagerApi.enrollUsersInCourse({ courseId, data }),
		invalidateKeys:
			userType == "teachers"
				? ["teacher-users-to-course", values.dataObj.id, "alphabiticallyDesc"]
				: ["student-users-to-course", values.dataObj.id, "alphabiticallyDesc"],

		closeDialog: () => {
			// **start old enroll
			// setOpenUsersToEnroll(false);
			// setUserType(null);
			// **end old enroll

			setFieldValueUsers("students", []);
			setFieldValueUsers("teachers", []);
		},
		// onSuccessFn: ({ data, variables }) =>
		// 	handleSelectedCourseToLoading({
		// 		courseId: userType == "teachers" ? variables.data?.teachers[0] : variables.data?.learner[0],
		// 	}),
	});

	const enrollUsersToHomeRoomMutation = useMutateData({
		queryFn: ({ homeRoomId, data }) => termsManagementApi.enrollUsersToHomeRoom({ homeRoomId, data }),
		invalidateKeys:
			userType == "teachers"
				? ["teacher-users", values.dataObj.id, "alphabiticallyDesc"]
				: ["student-users", values.dataObj.id, "alphabiticallyDesc"],

		closeDialog: () => {
			setOpenUsersToEnroll(false);
			setUserType(null);
			setFieldValueUsers("students", []);
			setFieldValueUsers("teachers", []);
		},
	});

	const copyBulkCoursesMutation = useMutateData({
		queryFn: ({ data }) => coursesManagerApi.copyBulkCourses({ data }),
		invalidateKeys: ["academicYears"],
		closeDialog: () => {
			setOpenCoursesCurricula(false);
			setFieldValue("coursesByCurricula", []);
		},
	});

	const publishCourseMutation = useMutateData({
		queryFn: (courseId) => coursesManagerApi.publishCourse(courseId),
		invalidateKeys: ["academicYears"],
	});

	//--------------------------------------------------------------------------------------

	//--------- Start Hovering ---------
	const handleIconHovering = (id) => {
		setIsHovering(id);
	};
	//--------- End Hovering ---------

	//--------- Start Open Collapse ---------

	const academicYearsCollapses = (id) => {
		const isOpen = "collapse" + id;
		if (isOpen == openedAcademicYearsCollapses) {
			setOpenedAcademicYearsCollapses("collapse");
		} else {
			setOpenedAcademicYearsCollapses("collapse" + id);
		}
	};

	const termsCollapses = (id) => {
		const isOpen = "collapse" + id;
		if (isOpen == openedTermsCollapses) {
			setOpenedTermsCollapses("collapse");
		} else {
			setOpenedTermsCollapses("collapse" + id);
		}
	};

	const gradeLevelsCollapses = (id) => {
		const isOpen = "collapse" + id;
		if (isOpen == openedGradeLevelsCollapses) {
			setOpenedGradeLevelsCollapses("collapse");
		} else {
			setOpenedGradeLevelsCollapses("collapse" + id);
		}
	};

	const highlightSpecificCollapse = (id) => {
		// const isOpen = "collapse" + id;
		// if (isOpen == highlightCollapse) {
		// 	setHighlightCollapse("collapse");
		// } else {
		// 	setHighlightCollapse("collapse" + id);
		// }
		setHighlightCollapse(id);
	};

	const handleOpenCollapse = ({ id, type }) => {
		if (type == "academicYear") {
			academicYearsCollapses(id);
			highlightSpecificCollapse(id);
		} else if (type == "term") {
			termsCollapses(id);
			highlightSpecificCollapse(id);
		} else if (type == "gradeLevel") {
			gradeLevelsCollapses(id);
			highlightSpecificCollapse(id);
		}
	};

	const handelDelete = (id, type) => {
		Swal.input({
			title: tr`write "delete" to confirm deleting`,
			message: tr`this can't be undone`,
			type: DANGER,
			onConfirm: () =>
				type == "academicYear" ? handleDeleteAcademicYear(id) : type == "homeRoom" ? handleDeleteHomeRoom(id) : handleDeleteTerm(id),
			onCancel: () => null,
		});
	};

	//--------- End Open Collapse ---------

	//--------- Start Modal ---------
	//AcademicYearObj
	const handleOpenAcademicYearModal = (data) => {
		setOpenAcademicYear(true);
		setFieldValue("dataObj", {});

		if (data) {
			setFieldValue("dataObj", data);
		}
	};
	const handleCloseAcademicYearModal = () => {
		setOpenAcademicYear(false);
		resetForm();
	};

	//Term
	const handleOpenTermModal = (data, academicYearId) => {
		setOpenTerm(true);
		setFieldValue("academicYearId", academicYearId);
		setFieldValue("dataObj", {});

		if (data) {
			setFieldValue("academicYearId", academicYearId);
			setFieldValue("dataObj", data);
		}
	};
	const handleCloseTermModal = () => {
		setOpenTerm(false);
		resetForm();
	};

	//Grade Level Right Side
	const handleShowAllStudentsInGradeLevel = () => {
		setShowAllStudentsInGradeLevel(!showAllStudentsInGradeLevel);
	};

	//Home Room
	const handleOpenHomeRoomModal = ({ data, termId, gradeLevelId }) => {
		setOpenHomeRoom(true);

		if (!data) {
			setFieldValue("termId", termId);
			setFieldValue("gradeLevelId", gradeLevelId);
			setFieldValue("allHomeRoomsToGradeLevel", []);
			setFieldValueUsers("students", []);
			setFieldValueUsers("teachers", []);
		}
	};

	const handleCloseHomeRoomModal = () => {
		setOpenHomeRoom(false);
		// resetForm();
	};

	//Home Room RightSide

	const handleShowAllStudentsInHomeRoom = () => {
		setShowAllStudentsInHomeRoom(!showAllStudentsInHomeRoom);
	};
	const handleShowAllTeachersInHomeRoom = () => {
		setShowAllTeachersInHomeRoom(!showAllTeachersInHomeRoom);
	};

	const handleShowActions = (homeroomId) => {
		setShowActions(homeroomId);
	};

	const handleHideActions = () => {
		setShowActions(null);
		// resetFormUsers();
		setFieldValueUsers("students", []);
		setFieldValueUsers("teachers", []);
	};

	const handleOpenMoveUsersFromHomeRoomToAnother = () => {
		setOpenMoveUsersFromHomeRoomToAnother(true);
	};

	const handleCloseMoveUsersFromHomeRoomToAnother = () => {
		setOpenMoveUsersFromHomeRoomToAnother(false);
	};

	//Courses Curricula
	const handleOpenCoursesByCurriculaModal = () => {
		setOpenCoursesCurricula(true);
	};
	const handleCloseCoursesByCurriculaModal = () => {
		setOpenCoursesCurricula(false);
	};

	//Courses Catalog
	const handleOpenCoursesModal = () => {
		setOpenCourses(true);
	};
	const handleCloseCoursesModal = () => {
		setOpenCourses(false);
		setFieldValue("idsLoading", []);
	};

	//Courses School Catalog
	const handleOpenCoursesFromSchoolCatalogModal = ({ termId, gradeLevelId }) => {
		setOpenCoursesFromSchoolCatalog(true);
		setFieldValue("termId", termId);
		setFieldValue("gradeLevelId", gradeLevelId);
	};
	const handleCloseCoursesFromSchoolCatalogModal = () => {
		setOpenCoursesFromSchoolCatalog(false);
	};
	const handleOpenUsersToEnrollModal = (type) => {
		setOpenUsersToEnroll(true);
		setUserType(type);
	};
	const handleCloseUsersToEnrollModal = () => {
		setOpenUsersToEnroll(false);
		setUserType(null);
	};

	//Term
	const handleOpenWeeklyScheduleModal = () => {
		setOpenWeeklySchedule(true);
	};
	const handleCloseWeeklyScheduleModal = () => {
		setOpenWeeklySchedule(false);
	};

	//--------- End Modal ---------

	//--------- Start Actions ---------
	const handleAddAcademicYear = (data) => {
		addAcademicYearMutation.mutate(data);
	};

	const handleEditAcademicYear = (data, id) => {
		editAcademicYearMutation.mutate({ data, id });
	};

	const handleDeleteAcademicYear = (id) => {
		deleteAcademicYearMutation.mutate(id);
	};

	const handleAddTerm = (data, academicYearId) => {
		addTermMutation.mutate({ data, academicYearId });
	};

	const handleEditTerm = (data, id) => {
		editTermMutation.mutate({ data, id });
	};

	const handleDeleteTerm = (id) => {
		deleteTermMutation.mutate(id);
	};

	const handleChangeTermStatus = ({ status, termId }) => {
		changeTermStatusMutation.mutate({ status, termId });
	};

	const handleAddHomeRoom = (data, termId) => {
		addHomeRoomMutation.mutate({ data, termId });
	};

	const handleEditHomeRoom = (data, id) => {
		editHomeRoomMutation.mutate({ data, id });
	};

	const handleDeleteHomeRoom = (id) => {
		deleteHomeRoomMutation.mutate(id);
	};

	const handleMakeCourseCloned = ({ courseId: courseId, termId: termId, curriculumId: curriculumId }) => {
		// makeCourseClonedMutation.mutate({
		// 	destination_id: termId,
		// 	destination_table: "terms",
		// 	destination_sub_table: AcademicPath ? null : "curricula",
		// 	destination_sub_id: AcademicPath ? null : curriculumId,
		// 	id: courseId,
		// });

		makeCourseClonedMutation.mutate({
			courseId,
			data: { term: termId },
		});

		// setFieldValue("idsLoading",);
		// handleCloseCoursesModal();
		handleCloseCoursesFromSchoolCatalogModal();
	};

	const handleUnEnrollUsersFromHomeRoom = ({ type }) => {
		const allUsers = [...valuesUsers.students, ...valuesUsers.teachers];
		const studentIds = valuesUsers.students.map((student) => student.user_id);
		const teacherIds = valuesUsers.teachers.map((teacher) => teacher.user_id);
		const allUserIds = [...studentIds, ...teacherIds];

		if (type == "homeRoom") {
			unEnrollUsersFromHomeRoomMutation.mutate({ data: { users: allUsers }, homeRoomId: values.dataObj?.id });
		} else {
			unEnrollUsersFromCourseMutation.mutate({ courseId: values.dataObj?.id, data: { users: allUserIds } });
		}
	};
	const handleEnrollUsers = () => {
		const studentIds = valuesUsers.students.map((student) => student.user_id);
		const teacherIds = valuesUsers.teachers.map((teacher) => teacher.user_id);
		let enrollUsersToCourse = {};
		if (userType == "students") {
			enrollUsersToCourse = { learners: studentIds, teachers: [] };
		} else {
			enrollUsersToCourse = { learners: [], teachers: teacherIds };
		}

		let enrollUsersToHomeRoom = {};
		if ((userType == "students" || userType == "learners") && values?.homeRoomId) {
			enrollUsersToHomeRoom = { learners: valuesUsers.students, teachers: [] };
		} else {
			enrollUsersToHomeRoom = { learners: [], teachers: valuesUsers.teachers };
		}

		if (values?.homeRoomId) {
			enrollUsersToHomeRoomMutation.mutate({ homeRoomId: values.dataObj?.id, data: enrollUsersToHomeRoom });
		} else {
			enrollUsersToCourseMutation.mutate({ courseId: values.dataObj?.id, data: enrollUsersToCourse });
		}
	};

	// **start old enroll
	const handleEnrollSingleUser = ({ userType, userId }) => {
		let enrollUsersToCourse = {};
		if (userType == "students") {
			enrollUsersToCourse = { learners: [userId], teachers: [] };
		} else {
			enrollUsersToCourse = { learners: [], teachers: [userId] };
		}

		// let enrollUsersToHomeRoom = {};
		// if ((userType == "students" || userType == "learners") && values?.homeRoomId) {
		// 	enrollUsersToHomeRoom = { learners: valuesUsers.students, teachers: [] };
		// } else {
		// 	enrollUsersToHomeRoom = { learners: [], teachers: valuesUsers.teachers };
		// }

		if (values?.homeRoomId) {
			// enrollUsersToHomeRoomMutation.mutate({ homeRoomId: values.dataObj?.id, data: enrollUsersToHomeRoom });
		} else {
			enrollUsersToCourseMutation.mutate({ courseId: values.dataObj?.id, data: enrollUsersToCourse });
		}
	};
	// ** end old enroll

	const handleSendNotificationsToUsersSelectedInHomeRoom = ({ type }) => {
		const studentIds = valuesUsers.students.map((student) => student.user_id);
		const teacherIds = valuesUsers.teachers.map((teacher) => teacher.user_id);

		const allUserIds = [...studentIds, ...teacherIds];
		sendNotificationsToUsersSelectedInHomeRoomMutation.mutate({
			data: {
				user_ids: allUserIds,
				title: type == "homeRoom" ? "New Home Room" : "New Course",
				body: type == "homeRoom" ? "You have been enrolled to home room" : "You have been enrolled to course",
				table_name: type == "homeRoom" ? "home_room" : "courses",
				row_id: values.dataObj?.id,
			},
		});
	};

	const handleMoveUsersFromHomeRoomToAnotherOne = (values) => {
		moveUsersFromHomeRoomToAnotherOneMutation.mutate({
			data: { learners: [...valuesUsers.students], teachers: [...valuesUsers.teachers] },
			sourceHomeRoomId: values.sourceHomeRoomId,
			targetHomeRoomId: values.targetHomeRoomId,
		});
	};

	const handleCopyBulkCoursesMutation = () => {
		copyBulkCoursesMutation.mutate({
			data: values.coursesByCurricula,
		});
	};

	const handlePublishCourseMutation = (courseId) => {
		publishCourseMutation.mutate(courseId);
	};

	const handleActiveTerm = (academicYearId) => {
		const academic = data.data.data.find((item) => item.id == academicYearId);
		const termActive = academic.terms.find((term) => term.isActive && !term.isFinalized);

		setFieldValue("termActive", termActive);
	};

	//--------- End Actions ---------

	const handleChangeActiveNode = ({
		nodeName,
		academicYearId,
		termId,
		gradeLevelId,
		homeRoomId,
		firstCurriculumID,
		allHomeRoomsToGradeLevel,
		data,
	}) => {
		if (nodeName == "academicYear") {
			setActiveNode("academicYear");
			handleActiveTerm(academicYearId);
		} else if (nodeName == "term") {
			setActiveNode("term");
			if (data?.gradelevels?.length > 0) {
				setActiveGradeLevelInWizard(true);
			}
			// Use this in Academic
			if (data?.courses?.length > 0) {
				setActiveGradeLevelInWizard(true);
			}

			// setFieldValue("itemObj", data);
			setFieldValue("dataObj", data);
			setFieldValue("academicYearId", academicYearId);
			setFieldValue("termId", termId);

			handleActiveTerm(academicYearId);
		} else if (nodeName == "gradeLevel") {
			setActiveNode("gradeLevel");

			setFieldValue("termId", termId);
			setFieldValue("gradeLevelId", gradeLevelId);
			setFieldValue("firstCurriculumID", firstCurriculumID);
			setFieldValue("allHomeRoomsToGradeLevel", allHomeRoomsToGradeLevel);
			setFieldValue("dataObj", data);
		} else if (nodeName == "homeRoom") {
			setActiveNode("homeRoom");

			setFieldValue("termId", termId);
			setFieldValue("gradeLevelId", gradeLevelId);
			setFieldValue("homeRoomId", homeRoomId);
			setFieldValue("dataObj", data);

			// handleHideActions();
		} else if (nodeName == "course") {
			setActiveNode("course");

			setFieldValue("termId", termId);
			setFieldValue("gradeLevelId", gradeLevelId);
			setFieldValue("homeRoomId", null);

			setFieldValue("dataObj", data);
			handleHideActions();
		} else if (nodeName == "weeklySchedule") {
			setActiveNode("weeklySchedule");
		}
	};

	const handleSelectedHomeRoomEnrolledUsers = (userType, userId, hashId, userName) => {
		let updatedArray = [...valuesUsers[userType]];

		const userIndex = updatedArray.findIndex((existingUserId) => existingUserId.user_id == userId);
		if (userIndex === -1) {
			updatedArray.push({ user_id: userId, hash_id: hashId ?? null, name: userName ?? "" });
		} else {
			updatedArray = updatedArray.filter((existingUserId) => existingUserId.user_id !== userId);
		}

		setFieldValueUsers(userType, updatedArray);
	};

	const handleSelectedCoursesByCurricula = ({ courseId, termId, curriculumId }) => {
		let updatedArray = [...values.coursesByCurricula];

		const courseIdIndex = updatedArray.findIndex((course) => course.id === courseId);
		if (courseIdIndex === -1) {
			updatedArray.push({
				destination_id: termId,
				destination_table: "terms",
				destination_sub_table: AcademicPath ? null : "curricula",
				destination_sub_id: AcademicPath ? null : curriculumId,
				id: courseId,
			});
		} else {
			updatedArray = updatedArray.filter((existingCourseId) => existingCourseId.id !== courseId);
		}

		setFieldValue("coursesByCurricula", updatedArray);
	};

	const handleSelectedCourseToLoading = ({ courseId }) => {
		let updatedArray = [...values.idsLoading];

		const courseIdIndex = updatedArray.findIndex((id) => id === courseId);
		if (courseIdIndex === -1) {
			updatedArray.push(courseId);
		} else {
			updatedArray = updatedArray.filter((existingCourseId) => existingCourseId !== courseId);
		}

		setFieldValue("idsLoading", updatedArray);
	};

	const handlePushToSchoolProfile = () => {
		history.push(`${baseURL}/${genericPath}/school-management/create`);
	};

	const handlePushToModules = (courseId, cohortId) => {
		history.push(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/modules`);
	};

	const handleSelectedAllCourses = (coursesData) => {
		let allCourses = [];
		allCourses = coursesData?.map((course) => ({
			destination_id: values?.termId,
			destination_table: "terms",
			destination_sub_table: AcademicPath ? null : "curricula",
			destination_sub_id: AcademicPath ? null : values.firstCurriculumID,
			id: course.id,
		}));

		setFieldValue("coursesByCurricula", allCourses);
	};

	if (isLoading || isError) return <Loader />;

	return (
		<TermsManagementContext.Provider
			value={{
				academicYears: data.data.data,
				itemObj: values,
				valuesUsers,

				openedAcademicYearsCollapses,
				openedTermsCollapses,
				openedGradeLevelsCollapses,
				isHovering,
				highlightCollapse,
				activeNode,
				activeGradeLevelInWizard,
				showAllStudentsInGradeLevel,
				showAllStudentsInHomeRoom,
				showAllTeachersInHomeRoom,
				showActions,
				setFieldValueUsers,

				dirtyFlag,
				setDirtyFlag,

				handleIconHovering,
				handleOpenCollapse,
				handleOpenAcademicYearModal,
				handleOpenTermModal,
				handleCloseAcademicYearModal,
				handleCloseTermModal,
				handleAddAcademicYear,
				handleEditAcademicYear,
				handelDelete,
				handleAddTerm,
				handleEditTerm,
				handleChangeTermStatus,
				handleChangeActiveNode,
				handleOpenHomeRoomModal,
				handleCloseHomeRoomModal,
				handleAddHomeRoom,
				handleEditHomeRoom,
				handleDeleteHomeRoom,
				handleOpenCoursesModal,
				handleMakeCourseCloned,
				handleOpenCoursesFromSchoolCatalogModal,
				handleOpenCoursesByCurriculaModal,
				handleShowAllStudentsInGradeLevel,
				handleShowAllStudentsInHomeRoom,
				handleShowAllTeachersInHomeRoom,
				handleShowActions,
				handleHideActions,
				handleSelectedHomeRoomEnrolledUsers,
				handleUnEnrollUsersFromHomeRoom,
				handleSendNotificationsToUsersSelectedInHomeRoom,
				handleMoveUsersFromHomeRoomToAnotherOne,
				handleOpenMoveUsersFromHomeRoomToAnother,
				handleCloseMoveUsersFromHomeRoomToAnother,
				handleOpenUsersToEnrollModal,
				handleEnrollUsers,
				handleEnrollSingleUser,
				handleOpenWeeklyScheduleModal,
				handleCloseWeeklyScheduleModal,
				handlePushToSchoolProfile,
				handlePushToModules,
				handleSelectedCoursesByCurricula,
				handleCopyBulkCoursesMutation,
				handleSelectedAllCourses,
				handlePublishCourseMutation,
				handleSelectedCourseToLoading,

				addAcademicLoading: addAcademicYearMutation.isLoading,
				editAcademicLoading: editAcademicYearMutation.isLoading,
				addTermLoading: addTermMutation.isLoading,
				editTermLoading: editTermMutation.isLoading,
				termStatusLoading: changeTermStatusMutation.isLoading,
				addHomeRoomLoading: addHomeRoomMutation.isLoading,
				editHomeRoomLoading: editHomeRoomMutation.isLoading,
				makeCourseClonedLoading: makeCourseClonedMutation.isLoading,
				unEnrollUsersFromHomeRoomLoading: unEnrollUsersFromHomeRoomMutation.isLoading,
				unEnrollUsersFromCourseLoading: unEnrollUsersFromCourseMutation.isLoading,
				sendNotificationsToUsersSelectedInHomeRoomLoading: sendNotificationsToUsersSelectedInHomeRoomMutation.isLoading,
				moveUsersFromHomeRoomToAnotherOneLoading: moveUsersFromHomeRoomToAnotherOneMutation.isLoading,
				enrollUsersToCourseLoading: enrollUsersToCourseMutation.isLoading,
				enrollUsersToHomeRoomLoading: enrollUsersToHomeRoomMutation.isLoading,
				copyBulkCoursesMutationLoading: copyBulkCoursesMutation.isLoading,
			}}
		>
			<AppModal size={"md"} show={openAcademicYear} parentHandleClose={handleCloseAcademicYearModal} headerSort={<GAcademicYear />} />
			<AppModal size={"md"} show={openTerm} parentHandleClose={handleCloseTermModal} headerSort={<GTerm />} />
			<AppModal size={"md"} show={openHomeRoom} parentHandleClose={handleCloseHomeRoomModal} headerSort={<GHomeRoomEditor />} />
			<AppModal
				size={"lg"}
				show={openCoursesCurricula}
				header={true}
				parentHandleClose={handleCloseCoursesByCurriculaModal}
				headerSort={<GCoursesByCurricula academicPath={AcademicPath} />}
			/>

			<RControlledDialog
				isOpen={openCourses}
				closeDialog={handleCloseCoursesModal}
				dialogBody={<GCourses />}
				contentClassName="max-w-[550px]"
			/>
			<AppModal
				size={"lg"}
				show={openCoursesFromSchoolCatalog}
				header={true}
				parentHandleClose={handleCloseCoursesFromSchoolCatalogModal}
				headerSort={<GCoursesFromSchoolCatalog />}
			/>
			<AppModal
				size={"md"}
				show={openMoveUsersFromHomeRoomToAnother}
				parentHandleClose={handleCloseMoveUsersFromHomeRoomToAnother}
				headerSort={<GMoveUsersFromHomeRoomToAnother />}
			/>
			<AppModal
				size={"md"}
				show={openUsersToEnroll}
				parentHandleClose={handleCloseUsersToEnrollModal}
				headerSort={
					<GEnrollUsers
						enrollUsers={true}
						userType={userType}
						values={valuesUsers}
						handleSelectedUser={handleSelectedHomeRoomEnrolledUsers}
						handleCloseEnrollUsers={handleCloseUsersToEnrollModal}
						setFieldValue={setFieldValueUsers}
						handleSelectedCourseToLoading={handleSelectedCourseToLoading}
					/>
				}
			/>

			<AppModal
				size={"xl"}
				show={openWeeklySchedule}
				parentHandleClose={handleCloseWeeklyScheduleModal}
				headerSort={
					<TermManagement_1
						academicYearId={values?.academicYearId}
						termId={values?.termId}
						gradeLevelId={values?.gradeLevelId}
						show={false}
						closeModal={() => handleCloseWeeklyScheduleModal()}
					/>
				}
			/>
			<RTermsManagement academicPath={AcademicPath} />
		</TermsManagementContext.Provider>
	);
};

export default GTermsManagement;
