import React, { useContext, useEffect } from "react";
import { TermsManagementContext } from "logic/TermsManagement/GTermsManagement";
import { boldGreyColor } from "config/constants";
import { primaryColor } from "config/constants";
import { useDispatch, useSelector } from "react-redux";
import TermManagement_1 from "logic/TermManagement/TermManagement_1";
import weeklySchedule from "assets/img/new/weekly_schedule.png";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { loadWeeklyScheduleEvents } from "logic/TermManagement/state/terms-manager.actions";

const GWeeklySchedule = () => {
	const TermsManagementData = useContext(TermsManagementContext);

	const { weeklyScheduleHaveEvents } = useSelector((state) => state.schoolManagementRed);
	const mounted = React.useRef();
	const dispatch = useDispatch();

	// React.useEffect(() => {
	// 	mounted.current = true;

	// 	return () => {
	// 		mounted.current = false;
	// 	};
	// }, []);

	// useEffect(() => {
	// 	loadWeeklyScheduleEvents(dispatch, mounted, TermsManagementData.itemObj?.termId, TermsManagementData.itemObj?.gradeLevelId);
	// }, []);

	return (
		<RFlex styleProps={{ flexDirection: "column" }}>
			<RFlex styleProps={{ justifyContent: "space-between", width: "100%" }}>
				<h6>{tr`weekly_schedule`}</h6>
				{weeklyScheduleHaveEvents && (
					<p
						style={{ color: primaryColor, cursor: "pointer" }}
						onClick={() => TermsManagementData.handleOpenWeeklyScheduleModal()}
					>{tr`edit`}</p>
				)}
			</RFlex>
			<TermManagement_1
				academicYearId={TermsManagementData.itemObj?.academicYearId}
				termId={TermsManagementData.itemObj?.termId}
				gradeLevelId={TermsManagementData.itemObj?.gradeLevelId}
				show={true}
				closeModal={() => TermsManagementData.handleCloseWeeklyScheduleModal()}
			/>
			{!weeklyScheduleHaveEvents && (
				<RFlex styleProps={{ justifyContent: "center", alignItems: "center", flexDirection: "column" }}>
					<img src={weeklySchedule} width={"130px"} height={"120px"} />
					<p style={{ color: boldGreyColor }}>{tr`No weekly schedule yet`}</p>
					<RFlex>
						<p style={{ color: boldGreyColor }}>{tr`click_on`}</p>
						<p
							style={{ color: primaryColor, cursor: "pointer" }}
							onClick={() => TermsManagementData.handleOpenWeeklyScheduleModal()}
						>{tr`add_new`}</p>
						<p style={{ color: boldGreyColor }}>{tr`to_start`}</p>
					</RFlex>
				</RFlex>
			)}
		</RFlex>
	);
};

export default GWeeklySchedule;
