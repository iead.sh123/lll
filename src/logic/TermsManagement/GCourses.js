import React, { useContext } from "react";
import { TermsManagementContext } from "logic/TermsManagement/GTermsManagement";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useFormik } from "formik";
import { courseApi } from "api/course";
import RSearchInput from "components/RComponents/RSearchInput";
import iconsFa6 from "variables/iconsFa6";
import RSelect from "components/Global/RComs/RSelect";
import RLister from "components/Global/RComs/RLister";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const GCourses = () => {
	const TermsManagementData = useContext(TermsManagementContext);

	// - - - - - - - - - - - - - formik - - - - - - - - - - - - - -
	const initialValues = {
		masterCourses: {},
		courseDepartments: [],
		groupingOfSearchFields: { name: "", department: undefined, page: 1 },
		name: "",
	};

	const { values, setFieldValue } = useFormik({
		initialValues,
	});

	// - - - - - - - - - - - - - Queries - - - - - - - - - - - - - -
	const masterCoursesData = useFetchDataRQ({
		queryKey: ["masterCourses", values.groupingOfSearchFields],
		queryFn: () => courseApi.masterCourses(values.groupingOfSearchFields),

		onSuccessFn: ({ data }) => {
			setFieldValue("masterCourses", data?.data);
		},
	});

	const courseDepartmentsData = useFetchDataRQ({
		queryKey: ["courseDepartments"],
		queryFn: () => courseApi.courseDepartments(),
		onSuccessFn: ({ data }) => {
			setFieldValue(
				"courseDepartments",
				[
					{ label: tr`all`, value: undefined },
					{ label: tr`no_department`, value: "uncategorized" },
				].concat(data?.data?.department?.map((dep) => ({ label: dep.name, value: dep.id })))
			);
		},
	});

	// - - - - - - - - - - - - - Actions - - - - - - - - - - - - - -
	const handleSearch = (emptyData) => {
		setFieldValue("groupingOfSearchFields.name", emptyData !== undefined ? "" : values.name);
	};

	const handleSelectDepartment = (departmentId) => {
		setFieldValue("groupingOfSearchFields.department", departmentId == "uncategorized" ? 0 : departmentId);
	};

	const _recordMasterCourses = {
		data: values?.masterCourses?.data,
		columns: [
			{
				accessorKey: "name",
				renderHeader: () => <span>{tr`course_name`}</span>,
				renderCell: ({ row }) => <span> {row.original?.name}</span>,
			},
			{
				accessorKey: "code",
				renderHeader: () => <span>{tr`course_code`}</span>,
				renderCell: ({ row }) => <span> {row.original?.code}</span>,
			},
			{
				accessorKey: "credits",
				renderHeader: () => <span>{tr`credits`}</span>,
				renderCell: ({ row }) => <span> {row.original?.credits}</span>,
			},
		],

		actions: [
			{
				// icon: TermsManagementData.itemObj.idsLoading.includes(row.original.id) ? iconsFa6.spinner : iconsFa6.plus,
				// disabled:
				// 	TermsManagementData?.itemObj?.idsLoading?.length == 0
				// 		? false
				// 		: TermsManagementData.itemObj.idsLoading.includes(row.original.id)
				// 		? false
				// 		: true,

				icon: iconsFa6.plus,
				onClick: ({ row }) => {
					TermsManagementData.handleSelectedCourseToLoading({ courseId: row.original.id });

					TermsManagementData.handleMakeCourseCloned({
						courseId: row.original.id,
						termId: TermsManagementData?.itemObj?.termId,
					});
				},
			},
		],
	};

	return (
		<RFlex styleProps={{ flexDirection: "column" }}>
			<RFlex styleProps={{ justifyContent: "space-between" }}>
				<RSearchInput
					searchLoading={masterCoursesData.isLoading}
					searchData={values.name}
					handleSearch={handleSearch}
					setSearchData={(value) => setFieldValue("name", value)}
					handleChangeSearch={(value) => setFieldValue("name", value)}
				/>

				<div style={{ width: "48%" }}>
					<RSelect
						option={values.courseDepartments}
						closeMenuOnSelect={true}
						placeholder={tr`department`}
						onChange={(e) => handleSelectDepartment(e.value)}
						isLoading={courseDepartmentsData.isLoading}
					/>
				</div>
			</RFlex>
			{masterCoursesData.isLoading ? (
				<Loader />
			) : (
				<RLister
					Records={_recordMasterCourses}
					convertRecordsShape={false}
					info={masterCoursesData}
					withPagination={true}
					handleChangePage={(event) => setFieldValue("groupingOfSearchFields.page", event)}
					page={values.groupingOfSearchFields.page}
				/>
			)}
		</RFlex>
	);
};

export default GCourses;
