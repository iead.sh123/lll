import React, { useState } from "react";
import tr from "components/Global/RComs/RTranslator";
import styles from "./SchoolInitiation.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { Collapse } from "reactstrap";
import { useHistory } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import { useParams } from "react-router-dom";
import { primaryColor } from "config/constants";

const Tree = ({ data }) => {
  const { sIType, sITypeId, semesterId, tabTitle } = useParams();

  const history = useHistory();
  const [openedCollapse, setOpenedCollapse] = useState(false);
  const [openedGradeLevelCollapse, setOpenedGradeLevelCollapse] =
    useState(false);
  const [openedSemesterCollapse, setOpenedSemesterCollapse] = useState(false);
  const [openedCourseCollapse, setOpenedCourseCollapse] = useState(false);

  const collapseToggle = (id, node) => {
    const isOpen = "collapse" + id + node;
    if (isOpen == openedCollapse) {
      setOpenedCollapse("collapse");
    } else {
      setOpenedCollapse("collapse" + id + node);
    }
  };

  const collapseGradeLevelToggle = (id, node) => {
    const isOpen = "collapse" + id + node;
    if (isOpen == openedGradeLevelCollapse) {
      setOpenedGradeLevelCollapse("collapse");
    } else {
      setOpenedGradeLevelCollapse("collapse" + id + node);
    }
  };

  const collapseSemesterToggle = (id, node) => {
    const isOpen = "collapse" + id + node;
    if (isOpen == openedSemesterCollapse) {
      setOpenedSemesterCollapse("collapse");
    } else {
      setOpenedSemesterCollapse("collapse" + id + node);
    }
  };

  const collapseCourseToggle = (id, node) => {
    const isOpen = "collapse" + id + node;
    if (isOpen == openedCourseCollapse) {
      setOpenedCourseCollapse("collapse");
    } else {
      setOpenedCourseCollapse("collapse" + id + node);
    }
  };

  return (
    <div className="m-3">
      {data?.map((ed_s, index) => (
        <div key={ed_s.id}>
          <h6 style={{ fontSize: "16px", cursor: "pointer" }}>
            <RFlex>
              <span
                onClick={() =>
                  history.push(
                    `${baseURL}/${genericPath}/school-initiation/education_stage/${ed_s?.id}/properties`
                  )
                }
                style={{ color: sITypeId == ed_s.id ? primaryColor : "#333" }}
              >
                {ed_s.name}
              </span>
              {openedCollapse === "collapse" + ed_s?.id + "education_stage" ? (
                <i
                  className="fa fa-arrow-down"
                  onClick={() => collapseToggle(ed_s?.id, "education_stage")}
                  style={{ color: sITypeId == ed_s.id ? primaryColor : "#333" }}
                />
              ) : (
                <i
                  className="fa fa-arrow-right"
                  onClick={() => collapseToggle(ed_s?.id, "education_stage")}
                  style={{ color: sITypeId == ed_s.id ? primaryColor : "#333" }}
                />
              )}
            </RFlex>
          </h6>
          <Collapse
            isOpen={
              openedCollapse === "collapse" + ed_s?.id + "education_stage"
            }
          >
            <h6
              className="pl-2 cursor-pointer"
              onClick={() =>
                history.push(
                  `${baseURL}/${genericPath}/school-initiation/education_stage/${ed_s?.id}/principal`
                )
              }
            >{tr`Principals`}</h6>

            {ed_s.gradeLevels.map((gradeLevel, index) => (
              <div key={gradeLevel.id}>
                <h6 className="pl-2 cursor-pointer">
                  <RFlex>
                    <span
                      onClick={() =>
                        history.push(
                          `${baseURL}/${genericPath}/school-initiation/education_stage/${ed_s?.id}/grade_levels`
                        )
                      }
                      style={{
                        color: sITypeId == ed_s.id ? primaryColor : "#333",
                      }}
                    >
                      {gradeLevel.title}
                    </span>
                    {openedGradeLevelCollapse ===
                    "collapse" + gradeLevel?.id + "grade_level" ? (
                      <i
                        className="fa fa-arrow-down"
                        onClick={() =>
                          collapseGradeLevelToggle(
                            gradeLevel?.id,
                            "grade_level"
                          )
                        }
                        style={{
                          color: sITypeId == ed_s.id ? primaryColor : "#333",
                        }}
                      />
                    ) : (
                      <i
                        className="fa fa-arrow-right"
                        onClick={() =>
                          collapseGradeLevelToggle(
                            gradeLevel?.id,
                            "grade_level"
                          )
                        }
                        style={{
                          color: sITypeId == ed_s.id ? primaryColor : "#333",
                        }}
                      />
                    )}
                  </RFlex>
                </h6>

                <Collapse
                  isOpen={
                    openedGradeLevelCollapse ===
                    "collapse" + gradeLevel?.id + "grade_level"
                  }
                >
                  <h6
                    className="pl-4 cursor-pointer"
                    onClick={() =>
                      history.push(
                        `${baseURL}/${genericPath}/school-initiation/grade_levels/${gradeLevel?.id}/student`
                      )
                    }
                  >{tr`students`}</h6>
                  {gradeLevel.semesters.map((semester) => (
                    <div key={semester.id}>
                      <h6 className="pl-4 cursor-pointer">
                        <RFlex>
                          <span
                            onClick={() =>
                              history.push(
                                `${baseURL}/${genericPath}/school-initiation/grade_levels/${gradeLevel?.id}/curricula/${semester.id}`
                              )
                            }
                            style={{
                              color:
                                semesterId == semester.id
                                  ? primaryColor
                                  : "#333",
                            }}
                          >
                            {semester.name}
                          </span>
                          {openedSemesterCollapse ===
                          "collapse" + semester?.id + "semester" ? (
                            <i
                              className="fa fa-arrow-down"
                              onClick={() =>
                                collapseSemesterToggle(semester?.id, "semester")
                              }
                              style={{
                                color:
                                  semesterId == semester.id
                                    ? primaryColor
                                    : "#333",
                              }}
                            />
                          ) : (
                            <i
                              className="fa fa-arrow-right"
                              onClick={() =>
                                collapseSemesterToggle(semester?.id, "semester")
                              }
                              style={{
                                color:
                                  semesterId == semester.id
                                    ? primaryColor
                                    : "#333",
                              }}
                            />
                          )}
                        </RFlex>
                      </h6>
                      <Collapse
                        isOpen={
                          openedSemesterCollapse ===
                          "collapse" + semester?.id + "semester"
                        }
                      >
                        {semester.courses.map((course) => (
                          <div key={course.id}>
                            <h6 className={styles.paddingRem2}>
                              <RFlex>
                                <span
                                  onClick={() =>
                                    history.push(
                                      `${baseURL}/${genericPath}/school-initiation/curricula/${course?.id}/properties`
                                    )
                                  }
                                  style={{
                                    color:
                                      sITypeId == course.id
                                        ? primaryColor
                                        : "#333",
                                  }}
                                  className="cursor-pointer"
                                >
                                  {course.name}
                                </span>
                                {openedCourseCollapse ===
                                "collapse" + course?.id + "course" ? (
                                  <i
                                    className="fa fa-arrow-down cursor-pointer"
                                    onClick={() =>
                                      collapseCourseToggle(course?.id, "course")
                                    }
                                  />
                                ) : (
                                  <i
                                    className="fa fa-arrow-right cursor-pointer"
                                    onClick={() =>
                                      collapseCourseToggle(course?.id, "course")
                                    }
                                  />
                                )}
                              </RFlex>
                            </h6>

                            <Collapse
                              isOpen={
                                openedCourseCollapse ===
                                "collapse" + course?.id + "course"
                              }
                            >
                              <h6
                                className={
                                  styles.paddingRem2 + " cursor-pointer"
                                }
                                onClick={() =>
                                  history.push(
                                    `${baseURL}/${genericPath}/school-initiation/curricula/${course?.id}/senior_teacher`
                                  )
                                }
                              >{tr`senior_teacher`}</h6>

                              <RFlex
                                className={
                                  styles.paddingRem2 + " cursor-pointer"
                                }
                              >
                                <h6
                                  onClick={() =>
                                    history.push(
                                      `${baseURL}/${genericPath}/school-initiation/curricula/${course?.id}/course_books`
                                    )
                                  }
                                >{tr`books`}</h6>
                                <div className={styles.tree_book}>
                                  {course.books.length}
                                </div>
                              </RFlex>
                            </Collapse>
                          </div>
                        ))}
                      </Collapse>
                    </div>
                  ))}
                </Collapse>
              </div>
            ))}
          </Collapse>
        </div>
      ))}
    </div>
  );
};

export default Tree;
