import React, { useEffect, useContext } from "react";
import { useDispatch } from "react-redux";
import RGradeLevelForm from "view/SchoolInitiation/Modals/RGradeLevelForm";
import {
  getGradeScalesAsync,
  getAppGradeLevelsAsync,
  educationStageAsync,
  getSemestersByOrganizationAsync,
} from "store/actions/global/schoolInitiation.actions";
import { useParams } from "react-router-dom";
import { gradeLevelsByIdAsync } from "store/actions/global/schoolInitiation.actions";
import Loader from "utils/Loader";
import { SchoolInitiationContext } from "logic/SchoolInitiation/GSchoolInitiation";

const GGradeLevelForm = () => {
  const { sIType, sITypeId } = useParams();
  const dispatch = useDispatch();
  const SchoolInitiationData = useContext(SchoolInitiationContext);

  useEffect(() => {
    if (sIType == "grade_levels" && sITypeId) {
      dispatch(
        gradeLevelsByIdAsync(sITypeId, SchoolInitiationData.setAddGradeLevels)
      );
      dispatch(getSemestersByOrganizationAsync(true));
    }
    dispatch(getGradeScalesAsync(true));
    dispatch(getAppGradeLevelsAsync(true));
    dispatch(educationStageAsync(true));
  }, [sIType]);

  return (
    <>
      {SchoolInitiationData.egcLoading ? (
        <Loader />
      ) : (
        <>
          <RGradeLevelForm />
        </>
      )}
    </>
  );
};

export default GGradeLevelForm;
