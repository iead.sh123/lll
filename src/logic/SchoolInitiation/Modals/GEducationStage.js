import React, { useEffect, useContext } from "react";
import { useParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import { SchoolInitiationContext } from "logic/SchoolInitiation/GSchoolInitiation";
import { educationStageByIdAsync } from "store/actions/global/schoolInitiation.actions";
import Loader from "utils/Loader";

import REducationStageForm from "view/SchoolInitiation/Modals/REducationStageForm";
const GEducationStage = () => {
  const { sIType, sITypeId } = useParams();
  const dispatch = useDispatch();
  const SchoolInitiationData = useContext(SchoolInitiationContext);

  useEffect(() => {
    if (sIType == "education_stage" && sITypeId) {
      dispatch(
        educationStageByIdAsync(
          sITypeId,
          SchoolInitiationData.setAddEducationStage
        )
      );
    }
  }, [sITypeId]);

  return (
    <>
      {SchoolInitiationData.egcLoading ? (
        <Loader />
      ) : (
        <>
          <REducationStageForm />
        </>
      )}
    </>
  );
};

export default GEducationStage;
