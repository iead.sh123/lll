import React, { useEffect, useState } from "react";
import Swal, { DANGER } from "utils/Alert";
import { useDispatch, useSelector } from "react-redux";
import {
	getCourseCategoriesTreeAsync,
	addCategoryToTreeAsync,
	removeCategoryFromTreeAsync,
} from "store/actions/global/coursesManager.action";
import { deleteSweetAlert } from "components/Global/RComs/RAlert";
import { primaryColor } from "config/constants";
import { Row, Col } from "reactstrap";
import GCourseManagerContent from "./GCourseManagerContent";
import RCoursesTree from "view/CoursesManager/RCoursesTree";
import GAddCategory from "./GAddCategory";
import Loader from "utils/Loader";
import styles from "./GCoursesManager.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

const GCoursesManager = () => {
	const dispatch = useDispatch();
	const [addCategory, setAddCategory] = useState(false);
	const [alert, setAlert] = useState(false);
	const [formGroup, setFormGroup] = useState({
		id: null,
		name: "",
		parent_id: null,
	});

	const { courseManagerTree, courseManagerTreeLoading, addEditCategoryToTreeLoading, removeCategoryFromTreeLoading } = useSelector(
		(state) => state.coursesManagerRed
	);

	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);

	const handleOpenAddCategory = () => setAddCategory(true);
	const handleCloseAddCategory = () => setAddCategory(false);

	useEffect(() => {
		dispatch(getCourseCategoriesTreeAsync({}));
	}, []);

	const handleChange = (name, value) => {
		setFormGroup({
			...formGroup,
			[name]: value,
		});
	};

	const handleAddCategoryToTree = () => dispatch(addCategoryToTreeAsync(formGroup, handleCloseAddCategory, setFormGroup));

	const successDelete = async (prameters) => {
		if (!prameters.hasCourses) {
			dispatch(removeCategoryFromTreeAsync(prameters.categoryId, hideAlert));
		}
	};

	const handleRemoveCategoryFromTree = (categoryId, canDelete) => {
		const prameters = {
			categoryId,
			canDelete,
		};
		const message = tr`Are you sure?`;
		const confirm = tr`Yes, delete it`;
		prameters.canDelete
			? deleteSweetAlert(showAlerts, hideAlert, successDelete, prameters, message, confirm)
			: toast.error(tr`you can't delete this category`);
	};

	let level = 0;
	return (
		<React.Fragment>
			<Row>
				{alert}

				<Col xs={12} sm={2} className={styles.tree + " scroll_hidden p-0"}>
					{courseManagerTreeLoading ? (
						<Loader />
					) : (
						<article>
							<RFlex
								className={styles.new_category}
								styleProps={{
									color: primaryColor,
								}}
								onClick={() => handleOpenAddCategory()}
							>
								<i className={`fa fa-plus`} />
								<p className={`m-0 ${styles.new_category_text}`}>{tr`create_new_category`}</p>
							</RFlex>

							<RCoursesTree
								parentId={null}
								courseCategories={courseManagerTree}
								level={level + 1}
								handleAddCategoryToTree={handleAddCategoryToTree}
								handleRemoveCategoryFromTree={handleRemoveCategoryFromTree}
								addEditCategoryToTreeLoading={addEditCategoryToTreeLoading}
								formGroup={formGroup}
								handleChange={handleChange}
							/>

							{addCategory && level == 0 && (
								<GAddCategory
									handleCloseAddCategory={handleCloseAddCategory}
									handleAddCategoryToTree={handleAddCategoryToTree}
									parentId={null}
									addEditCategoryToTreeLoading={addEditCategoryToTreeLoading}
									handleChange={handleChange}
								/>
							)}
						</article>
					)}
				</Col>

				<Col xs={12} sm={10} className={styles.content + " scroll_hidden"}>
					<GCourseManagerContent />
				</Col>
			</Row>
		</React.Fragment>
	);
};

export default GCoursesManager;
