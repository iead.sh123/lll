import React from "react";
import iconsFa6 from "variables/iconsFa6";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import styles from "../GCourseMap.module.scss";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import RImageViewer from "components/Global/RComs/RImageViewer/RImageViewer";
import { Collapse } from "reactstrap";

const LessonContent = ({ content, searchResult, itemsSelected, collapsesContentToggle, openedCollapses, handleUserInteraction }) => {
	return (
		<RFlex styleProps={{ flexDirection: "column", gap: 0 }} onClick={() => collapsesContentToggle(content.id)}>
			<RFlex styleProps={{ alignItems: "center", justifyContent: "space-between", padding: "5px 0px 0px 0px", cursor: "pointer" }}>
				<RFlex id={content.id} styleProps={{ alignItems: "center", gap: "8px" }}>
					<i className={iconsFa6.playCircle + " fa-md"} />
					<span
						className={`${Object.keys(searchResult).map(Number).includes(+content.id) ? styles.highlighted : ""}`}
						style={{ background: Object.keys(searchResult)[itemsSelected - 1] == +content.id ? "#f58b1f" : "" }}
					>
						{content.title}
					</span>
				</RFlex>
				<i className={openedCollapses.includes(content?.id) ? iconsFa6.chevronDown : iconsFa6.chevronRight} />
			</RFlex>
			<Collapse isOpen={openedCollapses.includes(content?.id)}>
				<RFlex styleProps={{ flexDirection: "column", gap: 0 }}>
					{content.contents?.map((lessonContent) => {
						return (
							<RFlex key={lessonContent.id} className={`${styles.content}`} id={lessonContent.id}>
								{/* This method needs to be adjusted  marginTop: "-25px" }*/}
								<div style={{ marginTop: "-25px" }}>
									<AppCheckbox
										onChange={(event) => handleUserInteraction(lessonContent.learning_object_id)}
										className={styles.checkbox__design}
										checked={lessonContent.done}
									/>
								</div>
								<RFlex styleProps={{ justifyContent: "space-between", alignItems: "baseline" }}>
									{lessonContent?.type?.toLowerCase() == "attachment" ? (
										<RImageViewer
											mimeType={lessonContent?.mime_type}
											url={lessonContent?.attachment?.url}
											imageName={lessonContent?.attachment?.file_name}
											showFullAttachment={false}
											width={"16px"}
											height={"16px"}
										/>
									) : (
										<i
											className={
												lessonContent?.type?.toLowerCase() == "link"
													? iconsFa6.link + " pt-1 fa-md"
													: lessonContent?.type?.toLowerCase() == "text"
													? iconsFa6.text + " pt-1 fa-md"
													: lessonContent?.type?.toLowerCase() == "live_session"
													? iconsFa6.headphones + " pt-1 fa-md"
													: lessonContent?.type?.toLowerCase() == "lesson_plan"
													? iconsFa6.plus + " pt-1 fa-md"
													: ""
											}
										/>
									)}
									{/* styles.title__selected */}
									<span
										className={`${Object.keys(searchResult).map(Number).includes(+lessonContent.id) ? styles.highlighted : ""}`}
										style={{ background: Object.keys(searchResult)[itemsSelected - 1] == +lessonContent.id ? "#f58b1f" : "" }}
									>
										{lessonContent.title}
									</span>
								</RFlex>
							</RFlex>
						);
					})}
				</RFlex>
			</Collapse>
		</RFlex>
	);
};

export default LessonContent;
