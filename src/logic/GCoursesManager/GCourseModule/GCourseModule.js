import React, { useEffect, useState, useRef } from "react";
import Swal, { WARNING } from "utils/Alert";
import {
	getCourseModuleAsync,
	categoryAncestorsAsync,
	fillDataToSpecificModule,
	updateModuleAsync,
	createModuleAsync,
	deleteModuleAsync,
	selectLessonContent,
	addItemsToLessonContent,
	selectModuleContent,
	addItemsToModuleContent,
	addContentToLessonContent,
	deleteModuleContentAsync,
	addLinksToLessonModule,
	publishAndUnPublishModuleContentAsync,
	deleteNewLessonContent,
	removeLinkFromLessonContentLocale,
	updateQuestionSetNameFromModuleContentAsync,
	updateTitleToQuestionSet,
	changeBehaviorInModuleContent,
	deleteNewModuleContent,
} from "store/actions/global/coursesManager.action";
import { lessonByIdAsync } from "store/actions/global/lesson.actions";
import {
	addLessonTextAsync,
	addLessonFileAsync,
	addLessonLinkAsync,
	addLiveSessionAsync,
	deleteLessonContentAsync,
	editLessonAttachmentAsync,
	editLessonLinkAsync,
	editLessonTextAsync,
	liveSessionByIdAsync,
} from "store/actions/teacher/lessonContent.action";
import { startLiveSessionAsync, joinMeetingToGetAttendLinkAsync } from "store/actions/global/liveSession.actions";
import { addBehaviorToQuestionSetFromModuleContentAsync, initiateQuestionSetFormate } from "store/actions/global/questionSet.actions";
import { createOrEditModuleContentLessonAsync } from "store/actions/global/lesson.actions";
import { importLessonPlanToLessonContent } from "store/actions/teacher/lessonPlan.actions";
import { useDispatch, useSelector } from "react-redux";
import { baseURL, genericPath } from "engine/config";
import { deleteSweetAlert } from "components/Global/RComs/RAlert";
import { useHistory, useParams, useLocation } from "react-router-dom";
import GPickLessonPlanLister from "logic/Courses/CourseManagement/LessonPlans/GPickLessonPlanLister";
import LiveSessionModal from "view/CoursesManager/RCourseModule/ListLessonContent/LessonComponents/LiveSession/LiveSessionModal";
import RCourseModule from "view/CoursesManager/RCourseModule/RCourseModule";
import AppModal from "components/Global/ModalCustomize/AppModal";
import Lesson from "view/CoursesManager/RCourseModule/ListModuleContent/ModuleContentComponents/Lesson";
import Loader from "utils/Loader";
import tr from "components/Global/RComs/RTranslator";
import QuestionSetModal from "view/CoursesManager/RCourseModule/ListModuleContent/ModuleContentComponents/QuestionSetComponents/QuestionSetModal";
import GCourseMap from "./GCourseMap";
import Drawer from "react-modern-drawer";
import "react-modern-drawer/dist/index.css";
import styles from "./GCourseMap.module.scss";
import { getLearningObjectMyProgressAsync } from "store/actions/global/learningObjects";
import GScorm from "logic/Scorm/GScorm";
import GScormViewable from "logic/Scorm/GScormViewable";
import { pushDataToCourseModule } from "store/actions/global/coursesManager.action";

export const CourseModuleContext = React.createContext();
const GCourseModule = () => {
	const history = useHistory();
	const dispatch = useDispatch();
	const { courseId, curriculumId, cohortId, categoryId, viewAsStudent } = useParams();

	const location = useLocation();

	const moduleContentRef = useRef(null);

	const queryParams = new URLSearchParams(location.search);
	const moduleIdParams = queryParams.get("moduleId");
	const moduleContentIdParams = queryParams.get("moduleContentId");
	const lessonContentIdParams = queryParams.get("lessonContentId");

	const [moduleData, setModuleData] = useState({
		title: "",
		order: 1,
	});
	const [moduleContentLesson, setModuleContentLesson] = useState({
		subject: "",
		lesson_date: "",
		lesson_time: "",
		end_date: "",
	});

	const [moduleContentState, setModuleContentState] = useState(null);

	const [openedModuleCollapses, setOpenedModuleCollapses] = useState(null);
	const [openedLessonContentCollapses, setOpenedLessonContentCollapses] = useState(null);
	const [openedContentCollapses, setOpenedContentCollapses] = useState(moduleContentIdParams ? "collapse" + moduleContentIdParams : null);
	const [selectedButton, setSelectedButton] = useState(0);
	const [createModule, setCreateModule] = useState(false);
	const [lessonModal, setLessonModal] = useState(false);
	const [scormModal, setScormModal] = useState(false);
	const [scormToViewable, setScormToViewable] = useState(false);
	const [liveSessionModal, setLiveSessionModal] = useState(false);
	const [lessonPlanModal, setLessonPlanModal] = useState(false);
	const [editModule, setEditModule] = useState(false);
	const [addContent, setAddContent] = useState(false);
	const [isHovered, setIsHovered] = useState(null);
	const [alert, setAlert] = useState(false);
	const [lessonId, setLessonId] = useState(null);
	const [lessonContentId, setLessonContentId] = useState(null);
	const [meetingId, setMeetingId] = useState(null);
	const [questionSetType, setQuestionSetType] = useState(null);
	const [pickFromCourseModal, setPickFromCourseModal] = useState(false);
	const [isOpenDrawer, setIsOpenDrawer] = useState(false);
	const [scormId, setScormId] = useState(null);

	const [fileNameLessonAttachmentState, setFileNameLessonAttachmentState] = useState({
		name: "",
	});
	const [editLessonLinkState, setEditLessonLinkState] = useState({
		link_url: "",
		link_title: "",
	});
	const [editLessonTextState, setEditLessonTextState] = useState({
		name: "",
		text: "",
	});
	const [editAttachment, setEditAttachment] = useState(false);

	const {
		courseModule,
		specificModule,
		moduleContent,
		categoryAncestors,
		lessonContentType,
		moduleContentType,
		shouldNotBeAddedLessonContent,
		shouldNotBeAddedModuleContent,
		courseModuleLoading,
		moduleContentLoading,
		createModuleLoading,
		createModuleContentLoading,
		updateTitleLoading,
		saveBehaviorLoading,
	} = useSelector((state) => state.coursesManagerRed);

	const { learningObjectMyProgress, learningObjectMyProgressLoading } = useSelector((state) => state.learningObjectRed);

	const { user } = useSelector((state) => state.auth);
	const { lessonByIdLoading } = useSelector((state) => state.lessonRed);
	const {
		lessonTextLoading,
		lessonFileLoading,
		lessonLinkLoading,
		liveSessionLoading,
		editLessonAttachmentLoading,
		reloadLiveSessionLoading,
	} = useSelector((state) => state.lessonContentRed);
	const { importLessonPlanToLessonContentLoading } = useSelector((state) => state.lessonPlan);

	let studentView = viewAsStudent || user?.type == "student" || user?.type == "learner" || location?.pathname?.includes("student");

	useEffect(() => {
		if (studentView && user?.type == "Course Administrator") {
			dispatch(getCourseModuleAsync(courseId, true));
		} else {
			dispatch(getCourseModuleAsync(courseId));
		}
	}, [studentView]);

	useEffect(() => {
		if (categoryId) {
			dispatch(categoryAncestorsAsync(categoryId));
		}
	}, [categoryId]);

	const handleCloseAllCollapses = () => {
		setOpenedModuleCollapses(null);
		setOpenedLessonContentCollapses(null);
		setOpenedContentCollapses(null);
		setIsHovered(null);
	};

	const collapsesModuleToggle = (moduleId) => {
		const isOpen = "collapse" + moduleId;
		if (isOpen == openedModuleCollapses) {
			setOpenedModuleCollapses("collapse");
		} else {
			setOpenedModuleCollapses("collapse" + moduleId);
		}
	};

	const collapsesContentToggle = (moduleContentId) => {
		const isOpen = "collapse" + moduleContentId;

		if (isOpen == openedContentCollapses) {
			if (moduleContentId == 0) {
				setOpenedContentCollapses("collapse0");
			} else {
				setOpenedContentCollapses("collapse");
			}
		} else {
			if (moduleContentId == 0) {
				setOpenedContentCollapses("collapse0");
			} else {
				setOpenedContentCollapses("collapse" + moduleContentId);
			}
		}
	};

	const showActionsOnHover = (id) => {
		const isOpen = "hover" + id;
		if (isOpen == isHovered) {
			setIsHovered("hover");
		} else {
			setIsHovered("hover" + id);
		}
	};

	const collapsesLessonContentToggle = (id) => {
		// i use this condition id == 0 to open collapse when click on lesson content to added
		const isOpen = "collapse" + id;
		if (isOpen == openedLessonContentCollapses) {
			if (id == 0) {
				setOpenedLessonContentCollapses("collapse0");
			} else {
				setOpenedLessonContentCollapses("collapse");
			}
		} else {
			if (id == 0) {
				setOpenedLessonContentCollapses("collapse0");
			} else {
				setOpenedLessonContentCollapses("collapse" + id);
			}
		}
	};

	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);

	const emptyState = () => {
		setModuleData({ title: "", order: 1 });
	};

	const emptyStateModuleContentLesson = () => {
		setModuleContentLesson({ subject: "", lesson_date: "", lesson_time: "", end_date: "" });
	};

	const getParentNames = (obj) => {
		const names = [];

		while (obj && obj.name) {
			names.push(obj.name);
			obj = obj.parent;
		}

		return names;
	};

	const handleShowCreateModuleForm = () => {
		setCreateModule(true);
		setEditModule(false);
		emptyState();
	};

	const handleCloseCreateModuleForm = () => {
		setCreateModule(false);
		emptyState();
	};

	const handleShowEditModuleForm = () => {
		setEditModule(true);
		setCreateModule(false);
	};

	const handleCloseEditModuleForm = () => {
		setEditModule(false);
	};

	const handleChangeCourseModule = ({ value, order }) => {
		setModuleData({ title: value, order: order ?? 1 });
	};

	const handleRedirectToSpecificModule = (module) => {
		setSelectedButton(module.id);
		handleFillDataToSpecificModule(module);
		setModuleData({
			title: module.title,
			order: module.order_id,
		});
		handleCloseAllCollapses();
		handlePushToAnotherRoute({
			ModuleId: module.id,
		});
	};

	const handleCreateModule = () => {
		dispatch(createModuleAsync(courseId, moduleData, false, handleCloseCreateModuleForm, handleRedirectToSpecificModule));
	};

	const handleEditModule = () => {
		dispatch(updateModuleAsync(specificModule.id, moduleData, true, handleCloseEditModuleForm));
	};

	const resultCategoryAncestors = getParentNames(categoryAncestors?.parent);

	useEffect(() => {
		if (courseModule.modules && courseModule.modules.length > 0 && moduleIdParams) {
			const data = courseModule.modules.find((module) => module.id === +moduleIdParams);
			if (data) {
				dispatch(fillDataToSpecificModule(data));
			}
		}
	}, [courseModule]);

	const handleFillDataToSpecificModule = (data) => {
		dispatch(fillDataToSpecificModule(data));
	};

	const handleAddContent = () => {
		setAddContent(!addContent);
	};

	const handleSelectLessonContentType = (lessonContentType) => {
		dispatch(selectLessonContent(lessonContentType));
	};

	const handleAddItemsToStore = (content, type, title) => {
		dispatch(addItemsToLessonContent(content, type, title));
	};

	//remove this (i don't using this)
	const handleSelectModuleContentType = (moduleContentType) => {
		dispatch(selectModuleContent(moduleContentType));
	};

	const handleAddItemsToModule = (content, type, title) => {
		dispatch(addItemsToModuleContent(content, type, title));
	};

	const handleOpenLessonModal = () => {
		setLessonModal(true);
	};
	const handleCloseLessonModal = () => {
		setLessonModal(false);
		emptyStateModuleContentLesson();
		setLessonId(null);
		collapsesContentToggle(0);
	};

	// - - - - - - SCORM - - - - - -
	const handleOpenScormModal = () => {
		setScormModal(true);
	};
	const handleCloseScormModal = () => {
		setScormModal(false);
		emptyStateModuleContentLesson();
		setLessonId(null);
		collapsesContentToggle(0);
	};

	const handleOpenScormToViewable = (contentId) => {
		setScormId(contentId);
		setScormToViewable(true);
	};
	const handleCloseScormToViewable = () => {
		setScormId(null);
		setScormToViewable(false);
	};

	// - - - - - - Live Session - - - - - -
	const handleOpenLiveSessionModal = () => {
		setLiveSessionModal(true);
	};
	const handleCloseLiveSessionModal = () => {
		setLiveSessionModal(false);
	};

	const handleOpenLessonPlanModal = (lessonContentId, moduleContent) => {
		setLessonPlanModal(true);
		setLessonContentId(lessonContentId);
		setModuleContentState(moduleContent);
	};
	const handleCloseLessonPlanModal = () => {
		setLessonPlanModal(false);
	};

	const handleCreateModuleContentLesson = (type, publish) => {
		dispatch(
			createOrEditModuleContentLessonAsync(
				specificModule.id,
				moduleContentLesson.id,
				moduleContentLesson,
				type,
				handleCloseLessonModal,
				lessonId,
				publish
			)
		);
	};

	const handleAddContentToLessonContent = (content, lessonContentId, name, value, type, linkIndex) => {
		dispatch(addContentToLessonContent(content, lessonContentId, name, value, type, linkIndex));
	};

	const handleDelete = (moduleId, moduleContentId, lessonContentId, lessonContentType, newLessonContent) => {
		const prameters = {
			moduleId,
			moduleContentId,
			lessonContentId,
			lessonContentType,
			newLessonContent,
		};
		const message = tr`Are you sure`;
		const confirm = tr`Yes, delete it`;
		if (prameters.lessonContentId == 0 || prameters.lessonContentId) {
			if (prameters.newLessonContent) {
				dispatch(
					deleteNewLessonContent(
						prameters.moduleId,
						prameters.moduleContentId,
						prameters.lessonContentId,
						prameters.lessonContentType,
						hideAlert
					)
				);
			} else {
				dispatch(
					deleteLessonContentAsync(
						prameters.moduleId,
						prameters.moduleContentId,
						prameters.lessonContentId,
						prameters.lessonContentType,
						hideAlert
					)
				);
			}
		} else if (prameters.moduleContentId == 0 || prameters.moduleContentId) {
			if (prameters.newLessonContent) {
				dispatch(deleteNewModuleContent(prameters.moduleId, prameters.moduleContentId, hideAlert));
			} else {
				dispatch(deleteModuleContentAsync(prameters.moduleId, prameters.moduleContentId, hideAlert));
			}
		} else {
			deleteSweetAlert(showAlerts, hideAlert, successDelete, prameters, message, confirm);
		}
	};

	const successDelete = (prameters) => {
		dispatch(deleteModuleAsync(prameters.moduleId, courseId, hideAlert));

		// i updated on this because we need to remove confirmation when delete lesson - lesson content

		// if (prameters.lessonContentId == 0 || prameters.lessonContentId) {
		// 	if (prameters.newLessonContent) {
		// 		dispatch(
		// 			deleteNewLessonContent(
		// 				prameters.moduleId,
		// 				prameters.moduleContentId,
		// 				prameters.lessonContentId,
		// 				prameters.lessonContentType,
		// 				hideAlert
		// 			)
		// 		);
		// 	} else {
		// 		dispatch(
		// 			deleteLessonContentAsync(
		// 				prameters.moduleId,
		// 				prameters.moduleContentId,
		// 				prameters.lessonContentId,
		// 				prameters.lessonContentType,
		// 				hideAlert
		// 			)
		// 		);
		// 	}
		// } else if (prameters.moduleContentId == 0 || prameters.moduleContentId) {
		// 	if (prameters.newLessonContent) {
		// 		dispatch(deleteNewModuleContent(prameters.moduleId, prameters.moduleContentId, hideAlert));
		// 	} else {
		// 		dispatch(deleteModuleContentAsync(prameters.moduleId, prameters.moduleContentId, hideAlert));
		// 	}
		// } else {
		// 	dispatch(deleteModuleAsync(prameters.moduleId, courseId, hideAlert));
		// }
	};
	useEffect(() => {
		if (lessonId) {
			dispatch(lessonByIdAsync(lessonId, setModuleContentLesson));
		}
	}, [lessonId]);

	const handleAddLessonContent = (moduleContent, lessonId, data, publish, lessonContentType) => {
		if (lessonContentType == "text") {
			dispatch(
				addLessonTextAsync(
					lessonId,
					{
						name: data.title,
						text: data.text.text,
						order: data.order,
						is_published: publish ? true : false,
					},
					specificModule,
					moduleContent
				)
			);
		} else if (lessonContentType == "attachment") {
			dispatch(
				addLessonFileAsync(
					lessonId,
					{
						...data,
						order: data.order,
						is_published: publish ? true : false,
					},
					specificModule,
					moduleContent
				)
			);
		} else if (lessonContentType == "link") {
			dispatch(
				addLessonLinkAsync(
					lessonId,
					{
						links: data.links,
						order: data.order,
						is_published: publish ? true : false,
					},
					specificModule,
					moduleContent
				)
			);
		} else if (lessonContentType == "live_session") {
			dispatch(addLiveSessionAsync(lessonId, specificModule, moduleContent, handleOpenLiveSessionModal, setMeetingId, setLessonId));
		}
	};

	const handleAddLinksToLessonModule = (content, lessonContent) => {
		dispatch(addLinksToLessonModule(content, lessonContent));
	};

	const handlePublishAndUnPublishModuleContent = (moduleId, moduleContentId, publish) => {
		dispatch(publishAndUnPublishModuleContentAsync(moduleId, moduleContentId, publish));
	};

	const handleShowWarningMessage = () => {
		if (shouldNotBeAddedLessonContent) {
			toast.warning(tr`you can’t add new content if you didn’t save the previous one`);
		}
	};

	const handleShowWarningMessageInModuleContent = () => {
		if (shouldNotBeAddedModuleContent) {
			toast.warning(tr`you can’t add new content if you didn’t save the previous one`);
		}
	};

	const handleRemoveLinkFromLessonContent = (content, lessonContent, fakeId) => {
		dispatch(removeLinkFromLessonContentLocale(content, lessonContent, fakeId));
	};

	const handleAddLessonPlanToLessonContent = (data, emptyState) => {
		dispatch(importLessonPlanToLessonContent(data, specificModule, moduleContentState, handleCloseLessonPlanModal, emptyState));
	};

	const handleEditLessonContent = (moduleId, moduleContentId, lessonContentId, type) => {
		if (type == "attachment") {
			dispatch(
				editLessonAttachmentAsync(moduleId, moduleContentId, lessonContentId, type, fileNameLessonAttachmentState, setEditAttachment)
			);
		} else if (type == "link") {
			dispatch(editLessonLinkAsync(moduleId, moduleContentId, lessonContentId, type, editLessonLinkState));
		} else if (type == "text") {
			dispatch(editLessonTextAsync(moduleId, moduleContentId, lessonContentId, type, editLessonTextState));
		}
	};

	const handleViewAsStudent = () => {
		if (categoryId) {
			history.push(
				`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/category/${categoryId}/modules/view/student`
			);
		} else {
			history.push(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/modules/view/student`);
		}
	};

	const handleExitViewAsStudent = () => {
		if (categoryId) {
			history.push(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/category/${categoryId}/modules`);
		} else {
			history.push(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/modules`);
		}
	};
	// let currentScrollY = window && window.scrollY;

	const handlePushToAnotherRoute = ({ ModuleId, ModuleContentID, LessonContentID }) => {
		let queryParams = "";

		if (ModuleId && !ModuleContentID) {
			queryParams = `?moduleId=${ModuleId}`;
		} else if (ModuleId && ModuleContentID && !LessonContentID) {
			queryParams = `?moduleId=${ModuleId}&moduleContentId=${ModuleContentID}`;
		} else if (ModuleId && ModuleContentID && LessonContentID) {
			queryParams = `?moduleId=${ModuleId}&moduleContentId=${ModuleContentID}&lessonContentId=${LessonContentID}`;
		} else if (!ModuleId && !ModuleContentID && LessonContentID) {
			queryParams = `?lessonContentId=${LessonContentID}`;
		} else if (!ModuleId && ModuleContentID && LessonContentID) {
			queryParams = `?moduleContentId=${ModuleContentID}&lessonContentId=${LessonContentID}`;
		} else if (!ModuleId && ModuleContentID && !LessonContentID) {
			queryParams = "";
		} else {
			queryParams = "";
		}

		if (categoryId) {
			if (studentView) {
				history.push(
					`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/category/${categoryId}/modules/view/student${queryParams}`
				);
			} else {
				history.push(
					`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/category/${categoryId}/modules${queryParams}`
				);
			}
		} else {
			if (studentView) {
				history.push(
					`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/modules/view/student${queryParams}`
				);
			} else {
				history.push(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/modules${queryParams}`);
			}
		}
	};

	// const handleViewLiveSession = (MeetingId) => {
	//   history.push(
	//     `${baseURL}/${genericPath}/view/live-session/${MeetingId ?? meetingId}`
	//   );
	// };

	const handleViewLessonContent = ({ LessonId, MeetingId }) => {
		if (categoryId) {
			history.push(
				`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/category/${categoryId}/lesson/${
					LessonId ?? lessonId
				}/meeting/${MeetingId ?? meetingId}`
			);
		} else {
			history.push(
				`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/lesson/${LessonId ?? lessonId}/meeting/${
					MeetingId ?? meetingId
				}`
			);
		}
	};

	const handleJoinStudentAndTeacherToMeeting = (MeetingId) => {
		if (user?.type == "student" || user?.type == "learner") {
			dispatch(joinMeetingToGetAttendLinkAsync(MeetingId, user?.type));
		} else {
			dispatch(startLiveSessionAsync(MeetingId, null, null, user?.type));
		}
	};

	const handleReloadLiveSession = (content, lessonContent) => {
		dispatch(liveSessionByIdAsync(content, lessonContent));
	};

	const handlePickFromCollaboration = (questionSetType) => {
		history.push(`${baseURL}/${genericPath}/collaboration_area_contents/modules/${moduleIdParams}/${questionSetType}`);
	};
	// const handlePickFromCollaboration = () => {
	// 	history.push(`${baseURL}/${genericPath}/collaboration_area_contents/${courseId ? `courses/${courseId}` : curriculumId ? `curricula/${curriculumId}` : ""}/lesson_plans`
	// 	);
	// };
	const handleCreateQuestionSet = (questionSetType) => {
		if (categoryId) {
			history.push(
				`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/category/${categoryId}/module/${moduleIdParams}/editor/${questionSetType}`
			);
		} else {
			history.push(
				`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/module/${moduleIdParams}/editor/${questionSetType}`
			);
		}
		dispatch(initiateQuestionSetFormate(courseId, curriculumId));
	};

	const handleOpenPickFromCourseModal = (questionSetType) => {
		setQuestionSetType(questionSetType);
		setPickFromCourseModal(true);
	};
	const handleClosePickFromCourseModal = () => {
		setQuestionSetType(null);
		setPickFromCourseModal(false);
	};

	const handleChangeQuestionSetTitle = ({ moduleId, moduleContentId, key, value }) => {
		dispatch(updateTitleToQuestionSet(moduleId, moduleContentId, key, value));
	};

	const handleUpdateQuestionSetNameFromModuleContentAsync = ({ type, contentId, title }) => {
		dispatch(
			updateQuestionSetNameFromModuleContentAsync(type, contentId, {
				title: title,
			})
		);
	};

	const handlePushToSpecificQuestionSet = ({ type, questionSetId }) => {
		if (categoryId) {
			history.push(
				`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/category/${categoryId}/module/${moduleIdParams}/editor/${type}/${questionSetId}`
			);
		} else {
			history.push(
				`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/module/${moduleIdParams}/editor/${type}/${questionSetId}`
			);
		}
	};

	const handleAddBehaviorToQuestionSetFromModuleContentAsync = ({ moduleId, moduleContentId, questionSetType, contentId, data }) => {
		dispatch(addBehaviorToQuestionSetFromModuleContentAsync(moduleId, moduleContentId, questionSetType, contentId, data));
	};

	const handleChangeBehaviorInModuleContent = ({ moduleId, moduleContentId, key, value }) => {
		dispatch(changeBehaviorInModuleContent(moduleId, moduleContentId, key, value));
	};

	const handlePushToStartSolveQuestionSet = (moduleContextId, questionSetId, questionSetType, isReview) => {
		if (categoryId) {
			history.push(
				`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/category/${categoryId}/module/${moduleContextId}/${questionSetType}/${questionSetId}/${
					isReview ? "review" : "solving"
				}/start`
			);
		} else {
			history.push(
				`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/module/${moduleContextId}/${questionSetType}/${questionSetId}/${
					isReview ? "review" : "solving"
				}/start`
			);
		}
	};

	const handlePushToLessonPlanViewer = (lessonPlanId) => {
		history.push(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/lesson-plan/${lessonPlanId}`);
	};

	const handlePushToSpecificModule = (moduleId) => {
		history.push(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/modules?moduleId=${moduleId}`);
	};

	const handleOpenCourseMapDrawer = () => {
		setIsOpenDrawer(!isOpenDrawer);
		if (!isOpenDrawer) dispatch(getLearningObjectMyProgressAsync(courseModule.course.learning_object_id));
	};

	const handlePushScormToModule = (data) => {
		dispatch(pushDataToCourseModule(data, moduleIdParams));
		handleCloseScormModal();
	};

	return (
		<React.Fragment>
			{courseModuleLoading ? (
				<Loader />
			) : (
				<CourseModuleContext.Provider
					value={{
						user,
						categoryAncestors: resultCategoryAncestors,
						courseModule,
						specificModule,
						shouldNotBeAddedLessonContent,
						shouldNotBeAddedModuleContent,
						studentView,
						questionSetType,

						moduleIdParams,
						moduleContentIdParams,
						lessonContentIdParams,
						location,

						handleShowCreateModuleForm,
						handleCloseCreateModuleForm,
						handleCreateModule,
						handleChangeCourseModule,
						handleFillDataToSpecificModule,
						handleShowEditModuleForm,
						handleCloseEditModuleForm,
						handleEditModule,
						handleAddContent,
						handleDelete,
						handleSelectLessonContentType,
						handleAddItemsToStore,
						handleSelectModuleContentType,
						handleAddItemsToModule,
						handleOpenLessonModal,
						handleOpenScormModal,
						handleOpenScormToViewable,
						handleCreateModuleContentLesson,
						handleAddContentToLessonContent,
						handleCloseAllCollapses,
						collapsesModuleToggle,
						collapsesContentToggle,
						collapsesLessonContentToggle,
						showActionsOnHover,
						handleAddLessonContent,
						handleAddLinksToLessonModule,
						handlePublishAndUnPublishModuleContent,
						handleOpenLessonPlanModal,
						handleShowWarningMessage,
						handleShowWarningMessageInModuleContent,
						handleRemoveLinkFromLessonContent,
						handleEditLessonContent,
						handleViewAsStudent,
						handleExitViewAsStudent,
						// handleViewLiveSession,
						handleViewLessonContent,
						handleJoinStudentAndTeacherToMeeting,
						handleReloadLiveSession,
						handlePushToAnotherRoute,
						handleCreateQuestionSet,
						handleOpenPickFromCourseModal,
						handlePickFromCollaboration,
						handleChangeQuestionSetTitle,
						handleUpdateQuestionSetNameFromModuleContentAsync,
						handlePushToSpecificQuestionSet,
						handleAddBehaviorToQuestionSetFromModuleContentAsync,
						handleChangeBehaviorInModuleContent,
						handlePushToStartSolveQuestionSet,
						handlePushToLessonPlanViewer,
						handlePushToSpecificModule,
						handleOpenCourseMapDrawer,

						createModule,
						moduleData,
						selectedButton,
						editModule,
						addContent,
						lessonContentType,
						moduleContentLesson,
						moduleContentType,
						openedModuleCollapses,
						openedContentCollapses,
						openedLessonContentCollapses,
						isHovered,
						editAttachment,
						fileNameLessonAttachmentState,
						lessonContentId,
						viewAsStudent,
						moduleContentRef,

						setCreateModule,
						setSelectedButton,
						setModuleData,
						setModuleContentLesson,
						setLessonId,
						setEditAttachment,
						setFileNameLessonAttachmentState,
						setLessonContentId,
						setEditLessonLinkState,
						setEditLessonTextState,

						createModuleLoading,
						createModuleContentLoading,
						lessonTextLoading,
						lessonFileLoading,
						lessonLinkLoading,
						liveSessionLoading,
						editLessonAttachmentLoading,
						editLessonLinkState,
						editLessonTextState,
						reloadLiveSessionLoading,
						updateTitleLoading,
						saveBehaviorLoading,
					}}
				>
					{/* <Drawer open={true} onClose={handleOpenCourseMapDrawer} className={styles.drawer__width} direction="right"> */}
					<Drawer open={isOpenDrawer} onClose={handleOpenCourseMapDrawer} className={styles.drawer__width} direction="right">
						<GCourseMap
							courseModule={learningObjectMyProgress}
							myProgress={learningObjectMyProgress}
							loading={learningObjectMyProgressLoading}
						/>
					</Drawer>
					{lessonPlanModal && (
						<GPickLessonPlanLister
							handleAddItems={handleAddLessonPlanToLessonContent}
							handleClosePickALessonPlan={handleCloseLessonPlanModal}
							openPickALessonPlan={lessonPlanModal}
							sectionId={null}
							itemId={null}
							typeToPick={"lessonContent"}
							lessonContentId={lessonContentId}
							loading={importLessonPlanToLessonContentLoading}
						/>
					)}

					<AppModal
						size="md"
						show={lessonModal}
						parentHandleClose={handleCloseLessonModal}
						header={moduleContentLesson.id ? tr`edit_the_lesson` : tr`create_a_lesson`}
						headerSort={
							lessonByIdLoading ? (
								<Loader />
							) : (
								<Lesson type="LESSON" handleCloseLessonModal={handleCloseLessonModal} editMode={moduleContentLesson.id} />
							)
						}
					/>

					<AppModal
						size="md"
						show={scormModal}
						parentHandleClose={handleCloseScormModal}
						header={tr`upload_scorm_package`}
						headerSort={<GScorm moduleId={moduleIdParams} handlePushScormToModule={handlePushScormToModule} />}
					/>

					<AppModal
						size="lg"
						show={scormToViewable}
						parentHandleClose={handleCloseScormToViewable}
						headerSort={<GScormViewable scormId={scormId} />}
					/>

					<AppModal
						size="lg"
						show={liveSessionModal}
						parentHandleClose={handleCloseLiveSessionModal}
						// header={tr`create_a_lesson`}
						headerSort={<LiveSessionModal handleCloseLiveSessionModal={handleCloseLiveSessionModal} />}
					/>

					<AppModal
						size="lg"
						show={pickFromCourseModal}
						parentHandleClose={handleClosePickFromCourseModal}
						headerSort={
							<QuestionSetModal questionSetType={questionSetType} handleClosePickFromCourseModal={handleClosePickFromCourseModal} />
						}
						// header={true}
					/>

					{alert}
					<RCourseModule />
				</CourseModuleContext.Provider>
			)}
		</React.Fragment>
	);
};

export default GCourseModule;
