import React, { useState, useEffect } from "react";
import { getCoursesByCategoryIdAsync } from "store/actions/global/coursesManager.action";
import { useDispatch, useSelector } from "react-redux";
import { baseURL, genericPath } from "engine/config";
import { useHistory } from "react-router-dom";
import { useParams } from "react-router-dom";
import { Row, Col } from "reactstrap";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import GCourseCard from "./GCourseCard";
import RFilterTabs from "components/Global/RComs/RFilterTabs/RFilterTabs";
import RSelect from "components/Global/RComs/RSelect";
import RButton from "components/Global/RComs/RButton";
import Loader from "utils/Loader";
import tr from "components/Global/RComs/RTranslator";

const GCourseManagerContent = () => {
	const dispatch = useDispatch();
	const history = useHistory();
	const { categoryId } = useParams();
	const [searchData, setSearchData] = useState("");
	const [courseType, setCourseType] = useState([]);
	const [tabTitle, setTabTitle] = useState("");

	const { coursesByCategory, coursesStatistics, coursesByCategoryLoading } = useSelector((state) => state.coursesManagerRed);

	const handleChangeSearch = (text) => {
		setSearchData(text);
	};

	const searchRadioButtonArray = [
		{
			id: 1,
			name: tr`search_in_this_category`,
			categoryId: categoryId ? true : false,
		},
		{
			id: 2,
			name: tr`search_in_all_categories`,
			categoryId: categoryId ? false : true,
		},
	];

	useEffect(() => {
		if (categoryId && categoryId != "undefined") {
			dispatch(
				getCoursesByCategoryIdAsync({
					category_id: categoryId,
					noPagination: true,
				})
			);
		} else {
			dispatch(
				getCoursesByCategoryIdAsync({
					category_id: null,
					noPagination: true,
				})
			);
		}
	}, [categoryId]);

	const handleSearchOnRadioButton = (id) => {
		if (id == 2) {
			history.push(`${baseURL}/${genericPath}/courses-manager`);
		}
	};

	const handleSearch = (emptySearch) => {
		const payload = {
			category_id: categoryId,
			name: emptySearch ?? searchData,
			noPagination: true,
			course_types: courseType,
			isPublished: tabTitle.toLocaleLowerCase() == "published" ? true : tabTitle.toLocaleLowerCase() == "draft" ? false : null,
		};
		tabTitle.toLocaleLowerCase() == "all" && delete payload.isPublished;

		dispatch(getCoursesByCategoryIdAsync(payload));
	};

	const handleSearchOnCourseType = (data) => {
		setCourseType(data);
		const payload = {
			category_id: categoryId ? categoryId : null,
			noPagination: true,
			course_types: data,
			isPublished: tabTitle.toLocaleLowerCase() == "published" ? true : tabTitle.toLocaleLowerCase() == "draft" ? false : null,
			name: searchData,
		};
		tabTitle.toLocaleLowerCase() == "all" && delete payload.isPublished;

		dispatch(getCoursesByCategoryIdAsync(payload));
	};

	const handleSearchOnTabs = (tabTitle) => {
		setTabTitle(tabTitle);
		const payload = {
			category_id: categoryId ? categoryId : null,
			noPagination: true,
			course_types: courseType,
			isPublished: tabTitle.toLocaleLowerCase() == "published" ? true : tabTitle.toLocaleLowerCase() == "draft" ? false : null,
			name: searchData,
		};
		tabTitle.toLocaleLowerCase() == "all" && delete payload.isPublished;
		dispatch(getCoursesByCategoryIdAsync(payload));
	};

	return (
		<section>
			<React.Fragment>
				<React.Fragment>
					<RSearchHeader
						searchLoading={coursesByCategoryLoading}
						searchData={searchData}
						handleSearch={handleSearch}
						setSearchData={setSearchData}
						handleChangeSearch={handleChangeSearch}
						inputPlaceholder={tr`search`}
						addNew={false}
						searchRadioButton={searchRadioButtonArray}
						handleSearchOnRadioButton={handleSearchOnRadioButton}
					/>

					<Row className="mt-4 " style={{ display: "flex", alignItems: "center" }}>
						<Col md={3} sm={12} className="m-0 p-0">
							<RSelect
								name="course_type"
								option={[
									{ label: "Master Courses", value: "isMaster" },
									{ label: "Instance Courses", value: "isInstance" },
									{ label: "Private Courses", value: "isPrivate" },
								]}
								onChange={(e) => handleSearchOnCourseType(e.map((el) => el.value))}
								isMulti={true}
							/>
						</Col>

						<Col md={5} sm={12}>
							<RFilterTabs
								tabs={coursesStatistics && coursesStatistics.length > 0 && coursesStatistics}
								handleSearchOnTabs={handleSearchOnTabs}
								firstSelectedTab="All"
							/>
						</Col>

						<Col md={4} sm={12} style={{ display: "flex", justifyContent: "end" }}>
							<RButton
								text={tr`create_new_course`}
								faicon="fa fa-plus"
								onClick={() =>
									history.push({
										pathname: `${baseURL}/${genericPath}/courses-manager/editor${
											categoryId ? `/category/${categoryId}` : ""
										}/course-information`,
										state: { emptyStore: true },
									})
								}
								color="primary"
							/>
						</Col>
					</Row>
					{coursesByCategoryLoading ? (
						<Loader />
					) : (
						<React.Fragment>
							<GCourseCard
								payloadData={{
									category_id: categoryId ? categoryId : null,
									noPagination: true,
									course_types: courseType,
									isPublished: tabTitle == "published" ? true : tabTitle == "draft" ? false : null,
									name: searchData,
								}}
							/>
						</React.Fragment>
					)}
				</React.Fragment>
			</React.Fragment>
		</section>
	);
};

export default GCourseManagerContent;
