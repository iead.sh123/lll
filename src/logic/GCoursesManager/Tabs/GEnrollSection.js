import React from "react";
import DefaultImage from "assets/img/new/course-default-cover.png";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import styles from "../GCoursesManager.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { useDispatch, useSelector } from "react-redux";
import { enrollInCourseAsync } from "store/actions/global/coursesManager.action";
import { dangerColor } from "config/constants";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import { Services } from "engine/services";
import { useMutateData } from "hocs/useMutateData";
import { paymentApi } from "api/global/payment";
import { boldGreyColor } from "config/constants";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import Loader from "utils/Loader";
import { online } from "engine/config";

const GEnrollSection = () => {
	const dispatch = useDispatch();
	const { courseId } = useParams();
	const { courseById, enrollInCourseLoading } = useSelector((state) => state.coursesManagerRed);
	const { user } = useSelector((state) => state.auth);

	const cartItemsData = useFetchDataRQ({
		queryKey: ["cart-items"],
		queryFn: () => paymentApi.fetchCartItems(),
		enableCondition: !online ? true : false,
	});

	const addToCartMutation = useMutateData({
		queryFn: (data) => paymentApi.addItemToCart(data),
		invalidateKeys: [["cart-items"], ["cart-prices", []]],
		multipleKeys: true,
	});

	const handleAddToCart = () => {
		addToCartMutation.mutate({ productID: courseById?.id, productType: courseById?.productType });
	};

	const handleEnrollInCourse = () => {
		dispatch(enrollInCourseAsync(courseId));
	};

	const scrollToSection = (sectionName) => {
		const section = document.getElementById(sectionName);
		if (section) {
			section.scrollIntoView({ behavior: "smooth" });
		}
	};
	if (enrollInCourseLoading) {
		<Loader />;
	}
	const isUserEnrolledOrLoading = courseById.isEnrolled || enrollInCourseLoading || addToCartMutation?.isLoading;
	const isUserValid = user && ["student", "learner"].includes(user.type);
	const isCourseInCart = cartItemsData?.data?.data?.data.some((item) => +item.ProductID === courseById?.id);
	const isDisabled = !isUserValid || isUserEnrolledOrLoading || isCourseInCart;

	return (
		<RFlex className={styles.enrolled__section}>
			<RFlex styleProps={{ alignItems: "baseline", justifyContent: "space-between", width: "100%" }}>
				<RFlex styleProps={{ alignItems: "center" }}>
					<RFlex
						styleProps={{
							justifyContent: "space-between",
							alignItems: "center",
						}}
					>
						<div className={styles.imageDiv}>
							{courseById?.teachers?.slice(0, 3).map((user, index) => {
								return (
									<div key={index}>
										<img
											src={user.image_hash_id == undefined ? DefaultImage : `${Services.storage.file}${user.image_hash_id}`}
											alt={user.image_hash_id}
											className={`${styles.userTypeImage} `}
											style={{
												zIndex: index,
												right: index * 20,
												position: "relative",
											}}
										/>
									</div>
								);
							})}
						</div>
					</RFlex>

					<RFlex styleProps={{ flexWrap: "wrap", cursor: "pointer" }} onClick={() => scrollToSection("facilitator")}>
						{courseById?.teachers?.slice(0, 1)?.map((user, index, array) => (
							<span key={index}>
								{user.user_name}
								{index !== array.length - 1 && ","}
							</span>
						))}
						<span>{courseById.teachers?.length > 2 && `+${courseById.teachers?.length - 1}more`}</span>
					</RFlex>
				</RFlex>
			</RFlex>

			<RFlex>
				<span style={{ fontWeight: 700 }}>
					{courseById?.paymentInfo?.price} {courseById?.paymentInfo?.orgCurrency}
				</span>
				{+courseById?.paymentInfo?.priceAfterDiscount == +courseById?.paymentInfo?.price ? (
					""
				) : (
					<span style={{ fontWeight: 400, color: boldGreyColor, textDecoration: "line-through" }}>
						{courseById?.paymentInfo?.priceAfterDiscount} {courseById?.paymentInfo?.orgCurrency}
					</span>
				)}
			</RFlex>
			<RFlex styleProps={{ width: "100%", justifyContent: "space-between", alignItems: "center" }}>
				<RButton
					style={{ width: "300px", fontSize: "16px" }}
					text={courseById?.isFree ? tr`enroll` : isCourseInCart ? tr`added` : tr`add_to_cart`}
					onClick={() => {
						if (courseById?.isFree) {
							handleEnrollInCourse();
						} else {
							handleAddToCart();
						}
					}}
					color={isCourseInCart ? "success" : "primary"}
					loading={enrollInCourseLoading}
					disabled={isDisabled}
				/>
				{user && <i className={iconsFa6.heart} style={{ fontSize: "20px", color: dangerColor, paddingTop: "3px", cursor: "pointer" }} />}
			</RFlex>
		</RFlex>
	);
};

export default GEnrollSection;
