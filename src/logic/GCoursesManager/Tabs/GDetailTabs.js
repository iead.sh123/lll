import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams, useHistory, useLocation } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import { viewCourseMode, emptyDataWhenAddNewCourse } from "store/actions/global/coursesManager.action";
import { Row, Col } from "reactstrap";
import { Services } from "engine/services";
import GCourseOverview from "../GCourseOverview/GCourseOverview";
import GCourseInfo from "../CourseInfo/GCourseInfo";
import RFilterTabs from "components/Global/RComs/RFilterTabs/RFilterTabs";
import DefaultImage from "assets/img/new/course-default-cover.png";
import RButton from "components/Global/RComs/RButton";
import styles from "../GCoursesManager.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import GEnrollSection from "./GEnrollSection";

const GDetailTabs = () => {
	const location = useLocation();
	const dispatch = useDispatch();
	const history = useHistory();
	const pathName = location.pathname.split("/");
	const courseStatus = pathName[pathName.length - 1];

	const { categoryId, courseId, status } = useParams();
	const { courseById, courseStudentMode, categoryAncestors } = useSelector((state) => state.coursesManagerRed);

	const { user } = useSelector((state) => state.auth);

	useEffect(() => {
		if (
			["student", "learner"].includes(user?.type) ||
			["school_advisor", "senior_teacher", "principal", "Curriculum Director"].includes(user?.type) ||
			["teacher", "facilitator"].includes(user?.type)
		) {
			dispatch(viewCourseMode(true));
		} else if (user?.type == "Course Administrator") {
			dispatch(viewCourseMode(false));
		}
	}, [user]);

	useEffect(() => {
		if (courseId) {
		} else if (status == "course-information" || courseStatus == "course-information") {
			dispatch(emptyDataWhenAddNewCourse());
		}
	}, [status, courseStatus]);

	const coursesInfoTabs = [
		{
			title: tr`course-information`,
			url: `courses-manager/editor${courseId ?? courseById?.course_id ? `/course/${courseId ?? courseById?.course_id}` : ""}${
				categoryId ? `/category/${courseById?.category?.category_id ?? categoryId}` : ""
			}/course-information`,
			disable: false,
		},
		{
			title: tr`course-overview`,
			url: `courses-manager/editor${courseId ?? courseById?.course_id ? `/course/${courseId ?? courseById?.course_id}` : ""}${
				categoryId ? `/category/${courseById?.category?.category_id ?? categoryId}` : ""
			}/course-overview`,
			disable: courseById?.course_id ? false : true,
		},
	];

	const handlePushToAnotherRoute = (url) => {
		history.push(`${baseURL}/${genericPath}/${url}`);
	};

	const replaceMode = () => {
		dispatch(viewCourseMode(!courseStudentMode));
	};

	const getParentNames = (obj) => {
		const names = [];
		while (obj && obj.name) {
			names.push(obj.name);
			obj = obj.parent;
		}
		return names;
	};

	const result = getParentNames(categoryAncestors.parent);

	return (
		<React.Fragment>
			<Row>
				<Col xs={12} className="p-0 m-0">
					{/* <img
						className={styles.course_image}
						src={courseById.image_id == undefined ? DefaultImage : `${Services.courses_manager.file}${courseById.image_id}`}
						alt="image_cover"
					/>
					<div className={styles.opacity}></div>*/}

					<div className={styles.image__container}>
						<div
							className={styles.bg__image}
							style={{
								backgroundImage: `url(${
									courseById.image_id == undefined ? DefaultImage : `${Services.courses_manager.file}${courseById.image_id}`
								})`,
							}}
						>
							<div className={styles.blur}></div>
						</div>
						{!courseStudentMode && (
							<div className={styles.image__content}>
								<div className={styles.image__contain}>
									<img
										className={styles.image__banner}
										src={courseById.image_id == undefined ? DefaultImage : `${Services.courses_manager.file}${courseById.image_id}`}
										alt="image_cover"
										draggable={false}
									/>
								</div>
							</div>
						)}
					</div>

					{!courseStudentMode &&
						(categoryAncestors.parent == null ? (
							""
						) : (
							<RFlex styleProps={{ color: "white", fontSize: "18px", padding: " 10px 20px" }} className={styles.breadCrumb}>
								<span>{tr`create new course in`} : </span>
								<span>
									{result.map((name, index) => (
										<React.Fragment key={name}>
											{name}
											{index !== result.length - 1 && " > "}
										</React.Fragment>
									))}
								</span>
							</RFlex>
						))}
					{((status == "course-overview" && courseStudentMode) || (courseStatus == "course-overview" && courseStudentMode)) && (
						<GEnrollSection />
					)}
				</Col>
			</Row>

			<Row className={`${courseStudentMode ? styles.border : ""} mb-4 `}>
				{user ? (
					<Col xs={12} lg={4} className="pl-0">
						{courseStudentMode ? (
							<>
								{categoryAncestors.parent == null ? (
									""
								) : (
									<div style={{ color: "black" }} className={styles.breadCrumb}>
										{tr`create new course in`} : {result.map((name) => name + " _ ")}
									</div>
								)}
							</>
						) : ["school_advisor", "senior_teacher", "principal", "Curriculum Director"].includes(user?.type) ||
						  ["learner", "student"].includes(user?.type) ||
						  ["teacher", "facilitator"].includes(user?.type) ? (
							""
						) : (
							<RFilterTabs
								tabs={coursesInfoTabs}
								handleSearchOnTabs={null}
								handlePushToAnotherRoute={handlePushToAnotherRoute}
								activeT={courseStatus}
								firstSelectedTab={"Course Information"}
							/>
						)}
					</Col>
				) : (
					""
				)}

				{user ? (
					<>
						{courseStudentMode && user?.type !== "Course Administrator"
							? ""
							: (status == "course-overview" || courseStatus == "course-overview") && (
									<Col xs={12} lg={8} className="d-flex justify-content-end pl-0">
										<RButton
											text={courseStudentMode ? tr`exit_view_as_learner` : tr`view_as_learner`}
											color="primary"
											outline={true}
											faicon={"fa fa-graduation-cap"}
											onClick={replaceMode}
										/>
									</Col>
							  )}
					</>
				) : (
					""
				)}
			</Row>

			{status == "course-information" || courseStatus == "course-information" ? (
				<GCourseInfo />
			) : status == "course-overview" || courseStatus == "course-overview" ? (
				<GCourseOverview />
			) : null}
		</React.Fragment>
	);
};

export default GDetailTabs;
