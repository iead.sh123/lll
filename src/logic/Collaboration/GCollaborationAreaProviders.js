import React, { useState } from "react";
import { genericPath } from "engine/config";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { Services } from "engine/services";
import { baseURL } from "engine/config";
import { get } from "config/api";
import RCollaborationCard from "components/Global/RComs/Collaboration/RCollaborationCard";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";

export const GCollaborationAreaProviders = ({ setNoData = (x) => {}, setCount = () => {} }) => {
	const history = useHistory();
	const [records, setRecords] = useState([]);
	const getDataFromBackend = async (specific_url) => {
		const url = Services.collaboration.backend + `api/collaboration-area/providers`;
		let response1 = await get(specific_url ? specific_url : url);

		if (response1) {
			return response1;
		}
	};
	const setData = (response) => {
		if (!(response && response.data && response.data.data && response.data.data.length))
			if (typeof setNoData === "function" && setNoData !== null) setNoData(true);

		if (typeof setCount === "function" && setCount !== null) setCount(response?.data?.data?.length);
		setRecords(
			response?.data?.data?.map((r, index) => {
				return {
					specialProps: {
						onClick1: () => {
							history.push(`${baseURL}/${genericPath}/collaboration_area_contents/${r.agreement_id}`);
						},
						title: { text: r.sharable_contents_counts + "Content types" },
						Author: { image: r.image, text: r.name },
					},
				};
			})
		);
	};

	return (
		<div>
			<RAdvancedLister
				hideTableHeader={false}
				colorEveryOtherRow={false}
				getDataFromBackend={getDataFromBackend}
				setData={setData}
				records={records}
				getDataObject={(response) => response.data?.data?.data}
				characterCount={15}
				marginT={"mt-3"}
				marginB={"mb-2"}
				align={"left"}
				SpecialCard={RCollaborationCard}
				showListerMode={"cardLister"}
				swiper
				perLine={4}
			/>
		</div>
	);
};
