import RTabsPanel from 'components/Global/RComs/RTabsPanel';
import RTabsPanel_Horizontal from 'components/Global/RComs/RTabsPanel_Horizontal';
import RTabsPanel_Vertical from 'components/Global/RComs/RTabsPanel_Vertical';
import { genericPath } from 'engine/config';
import { baseURL } from 'engine/config';
import React, { useState } from 'react';
import { GCollaborationSettingContents } from './GCollaborationSettingContents';
import { GCollaborationSettingLevels } from './GCollaborationSettingLevels';
import RFilterTabs from 'components/Global/RComs/RFilterTabs/RFilterTabs';
export const GCollaborationSetting=()=>{



  const [contentsCount,setContentCount]=useState (0);
  const [levelsCount,setLevelsCount]=useState(0);
    const Tabs = [
        {
          icon:"fas fa-layer-group",
          name: "contents",
          title: "Contents",
          count:contentsCount,
          url: `${baseURL}/${genericPath}/collaboration_setting/contents`,
          content: () => {
            return <div><GCollaborationSettingContents setContentCount={setContentCount}/></div>;
          },
        },
        {
          icon:"fas fa-grip-horizontal",
          name: "",
          title: "Levels",
          count:levelsCount,
          url: `${baseURL}/${genericPath}/collaboration_setting/levels`,
          content: () => {
            return <div><GCollaborationSettingLevels setLevelsCount={setLevelsCount}/></div>;
          },
        },
       
        
      ];




      return (<RTabsPanel_Horizontal
          title={null}
          Tabs={Tabs}
          changeHorizontalTabs={() => {}}
        />)

};