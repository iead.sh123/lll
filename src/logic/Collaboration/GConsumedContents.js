
import RAdvancedLister from "components/Global/RComs/RAdvancedLister"; 
import { Services } from "engine/services";
import Helper from "components/Global/RComs/Helper";
import { get } from "config/api";
import RCollaborationCard from "components/Global/RComs/Collaboration/RCollaborationCard";
import React,{useState} from 'react';
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import RRow from "view/RComs/Containers/RRow";
import RColumn from "view/RComs/Containers/RColumn";
  //   import { useState } from 'react';
 import { useEffect } from 'react';
import RTagsViewer from "components/Global/RComs/Collaboration/RTagsViewer";

import RBieChart from "components/Global/RComs/RBieChart";
import RRecentActivities from "components/Global/RComs/Collaboration/RRecentActivities";
import RTitle from "components/Global/RComs/Collaboration/RTitle";
import { GCollaborationAreaProviders } from "./GCollaborationAreaProviders";

import { GCollaborationAreaConsumedContents } from "./GCollaborationAreaConsumedContents";
import { GCollaborationAreaClients } from "./GCollaborationAreaClients";
// import Helper from 'components/Global/RComs/Helper';
// import { Services } from 'engine/services';
export const GConsumedContents=({setNoData=(x)=>{}})=>{

//---------------------------------------------------------------------
const [providerConsumedContents,setProviderConsumedContents]=useState([]);
useEffect (()=>{
  const get_it=async()=>{ await Helper.fastGet(Services.collaboration.backend+"api/collaboration-area/most-consumed-contents/provider-side/","fail to get",(response)=>{setProviderConsumedContents(response.data?.data)},()=>{})}
  get_it();
  },[])

  const [transformedProviderConsumedContents,settransformedProviderConsumedContents]=useState([]);
 
 
  useEffect(()=>{
    
    settransformedProviderConsumedContents(providerConsumedContents.map(t=>({text:t.sharable_content_name,count:t.count})));

    Helper.cl(providerConsumedContents,"providerConsumedContents")
},[providerConsumedContents])
  
//---------------------------------------------------------------------------
//---------------------------------------------------------------------
const [consumerConsumedContents,setconsumerConsumedContents]=useState([]);
useEffect (()=>{
  const get_it=async()=>{ await Helper.fastGet(Services.collaboration.backend+"api/collaboration-area/most-consumed-contents/client-side/","fail to get",(response)=>{setconsumerConsumedContents(response.data?.data)},()=>{})}
  get_it();
  },[])
  

  const [transformedconsumerConsumedContents,settransformedconsumerConsumedContents]=useState([]);
 
 
  useEffect(()=>{


    if(!(consumerConsumedContents&&consumerConsumedContents.length>0)) if(typeof setNoData === 'function' && setNoData !== null) setNoData(true);
    settransformedconsumerConsumedContents(consumerConsumedContents.map(t=>({text:t.sharable_content_name,count:t.count})));

    Helper.cl(consumerConsumedContents,"consumerConsumedContents")
},[consumerConsumedContents])
  
//---------------------------------------------------------------------------


     return <div>
        
     


      <RBieChart data={transformedconsumerConsumedContents} />

</div> 
}