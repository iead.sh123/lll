import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import { Services } from "engine/services";
import Helper from "components/Global/RComs/Helper";
import { get } from "config/api";
import RCollaborationCard from "components/Global/RComs/Collaboration/RCollaborationCard";
import React, { useState } from "react";
import { useEffect } from "react";
import { put } from "config/api";
import produce from "immer";
import moment from "moment";
import { AgreementViewer } from "./AgreementViewer";
import RRow from "view/RComs/Containers/RRow";
import RLiveAction from "components/Global/RComs/Collaboration/RLiveAction";
import Swal from "utils/Alert";
import { DANGER } from "utils/Alert";
import { SUCCESS } from "utils/Alert";

import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";
export const GCollaborationAgreementsClients = ({ setOpenModal, setAgreementId, setClientsCount, refresh, setRefresh }) => {
	const [data1, setData1] = useState([]);
	const [records, setRecords] = useState([]);
	const [agreementIdToView, setAgreementIdToView] = useState();

	const getDataFromBackend = async (specific_url) => {
		const url = Services.collaboration.backend + `api/collaboration-agreement/clients`;
		let response1 = await get(specific_url ? specific_url : url);

		if (response1) {
			return response1;
		}
	};
	const activate = async (agreement_id, active) => {
		if (!r.approved_by_destination && !r.active) {
			alert("should be approved by destination to be active");
			return;
		}
		const url = Services.collaboration.backend + `api/collaboration-agreement/activate/${agreement_id}`;

		const toSend = { active: active };
		let response1 = await put(url, toSend);
		if (response1 && response1.data && response1.data.status == 1) {
			const data2 = produce(data1, (tempdata) => {
				const rec = tempdata.filter((r) => r.id == agreement_id)[0];
				rec.active = active;
			});

			Helper.cl(data2, "data2");
			setData1(data2);
			toast.error((active ? tr` Activated ` : tr`Deactivated`) + `Successfully`);

			setRefresh(!refresh);
		} else {
			toast.error(tr`Something went wrong during activation / deactivation`);
		}
	};
	const view = async (agreement_id) => {
		setAgreementIdToView(agreement_id);
	};

	const remove = async (agreement_id) => {
		Helper.fastPost(
			Services.collaboration.backend + `api/collaboration-agreement/remove/${agreement_id}`,
			{},
			"deletted successfully",
			"error while deleting",
			() => {
				Helper.cl("agreement delte suceess");
				setRefresh(!refresh);
			},
			() => {
				Helper.cl("agreement delete fail");
			}
		);
	};
	useEffect(() => {
		setRecords(
			data1.map((r, index) => {
				return {
					specialProps: {
						title: { text: r.details?.length + "Content types" },
						approveStatus: {
							icon: r.approved_by_destination ? "fa fa-check" : "fa fa-history",
							title: r.approved_by_destination ? "approved by destination" : "not approved yet",
							color: r.approved_by_destination ? "green" : "blue",
						},
						UpLeftIcon: "All:86",
						Actions: [
							{
								title: r.active ? "Deactivate" : "Activate",
								icon: r.active ? "fa fa-power-off" : "fa fa-power-off",
								color: r.active ? "red" : "green",
								onClick: () => {
									activate(r.id, !r.active);
									esh(!refresh);
								},
								withBorder: false,
								disabled: !r.approved_by_destination,
								disabledNote: "Not Yet Approved By destination",
							},
						],

						bottomBorderColor: r.active ? "green" : "blue",
						Author: {
							text: r.destinaton_organization.name,
							image: r.destinaton_organization.image,
						},
						Description: "date:" + moment(r.updated_at).format("YYYY-MM-DD HH:mm A"),
						menuActions: [
							{
								title: "View",
								icon: "	fa fa-eye",
								color: "red",
								onClick: () => {
									view(r.id);
								},
								withBorder: true,
							},
							{
								title: "Edit",
								icon: "	fa fa-pencil",
								color: "gray",
								onClick: () => {
									setAgreementId(r.id);
									setOpenModal(true);
								},
								withBorder: true,
							},
							{
								title: r.active ? "Deactivate" : "Activate",
								icon: r.active ? "fa fa-power-off" : "fa fa-power-off",
								color: r.active ? "red" : "green",
								onClick: () => {
									activate(r.id, !r.active);
								},
								withBorder: false,
								disabled: !r.approved_by_destination,
								disabledNote: "Not Yet Approved By destination",
							},
							{
								title: "Delete",
								icon: "fa fa-remove",
								color: "red",
								onClick: "",
								withBorder: true,
								onClick: () => {
									////Helper.cl("delete");
									remove(r.id, !r.active);
								},
							},
						],
					},
					// active:1
					// approved_by_destination:0
					// created_at:"2023-07-20T06:27:28.000000Z"
					// destination_org_id:8
					// id:12
					// source_org_id:1
				};
			})
		);
		setClientsCount(data1?.length);
	}, [data1]);

	const setData = (response) => {
		if (response?.data?.data) setData1(response?.data?.data);
	};

	return (
		<div style={{ height: "50vh" }}>
			{agreementIdToView && agreementIdToView > 0 ? (
				<>
					<RRow>
						<RLiveAction
							title=""
							icon="fas fa-arrow-left"
							color="black"
							text=""
							// color="red"
							loading={false}
							type="button"
							onClick={() => {
								setAgreementIdToView(null);
							}}
						/>
					</RRow>
					<AgreementViewer agreement_id={agreementIdToView} />
				</>
			) : (
				<RAdvancedLister
					hideTableHeader={false}
					colorEveryOtherRow={false}
					//firstCellImageProperty={"image"}
					getDataFromBackend={getDataFromBackend}
					setData={setData}
					records={records}
					getDataObject={(response) => response.data?.data?.data}
					SpecialCard={RCollaborationCard}
					showListerMode={"cardLister"}
					refresh={refresh}
					perLine={3}
					overflowX={"visible"}
					overflowY={"visible"}
				/>
			)}
		</div>
	);
};
