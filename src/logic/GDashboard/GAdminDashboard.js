import React, { useEffect, useState } from "react";

import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";

export const DiscussionContext = React.createContext();
import { Container, Row, Col } from "reactstrap";

import quoteimg from "assets/img/quote.png";
import nodata from "assets/img/no-data.png";
import GCalender from "logic/calender/GCalender";

import { usersAndPermissionsReducer, initialState } from "logic/UsersAndPermissions/State/UsersAndPermissions.reducer";
import { useReducer } from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import RecentAddedUser from "logic/UsersAndPermissions/RecentActivities/RecentAddedUser";
import RRole from "view/UsersAndPermissions/RRole";
import Helper from "components/Global/RComs/Helper";
import { getRecentAddedRolesAsync } from "logic/UsersAndPermissions/State/UsersAndPermissions.action";
import { getRecentUserTypesAsync } from "logic/UsersAndPermissions/State/UsersAndPermissions.action";
import { getRecentActivitiesAsync } from "logic/UsersAndPermissions/State/UsersAndPermissions.action";
import Loader from "utils/Loader";
import DashboardStatisticsCard from "./DashboardStatisticsCard";
import EventCard from "./EventCard";
import { event } from "jquery";
import RTitle from "components/Global/RComs/themed/RTitle/RTitle";
import { GCollaborationAreaProviders } from "logic/Collaboration/GCollaborationAreaProviders";
import { GCollaborationAreaConsumedContents } from "logic/Collaboration/GCollaborationAreaConsumedContents";
import { GCollaborationAreaClients } from "logic/Collaboration/GCollaborationAreaClients";
import { GConsumedContents } from "logic/Collaboration/GConsumedContents";
import RColumn from "view/RComs/Containers/RColumn";
import GGreetingSearch from "./GGreetingSearch";
import GStatisticsRow from "./GStatisticsRow";
import { Services } from "engine/services";
import GReportedContent from "./GReportedContent";
import RHeader from "components/Global/RComs/RHeader";
import RDiscover from "./rc/RDiscover";
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";
import REmptyData from "components/RComponents/REmptyData";

const GAdminDashboard = () => {
	// const dispatch = useDispatch();
	const [reportedContentNoData, setreportedContentNoData] = useState(true);
	const [reportedContentsMd, SetreportedContentsMd] = useState(4);
	const [quoteMd, setQuoteMd] = useState(4);
	const [mostConsumedCollaborationMd, setMostConsumedCollaborationMd] = useState(4);

	const [mostConsumedCollaborationNoData, setmostConsumedCollaborationNoData] = useState(false);

	const [colAreaClientsNoData, setcolAreaClientsNoData] = useState(true);
	const [colAreaProvidersNoData, setcolAreaProvidersNoData] = useState(true);
	const [colAreaConsumedContentsNoData, setcolAreaConsumedContentsNoData] = useState(true);

	const [statistics, setStatistics] = useState([]);

	const [usersAndPermissionsData, dispatch] = useReducer(usersAndPermissionsReducer, initialState);

	useEffect(() => {
		getRecentAddedRolesAsync(dispatch);
		getRecentUserTypesAsync(dispatch);
		getRecentActivitiesAsync(dispatch);
	}, []);

	const history = useHistory();

	const { user } = useSelector((state) => state.auth.user);
	const userData = {
		username: "John Doe",
	};

	const quotes = [
		{ text: "Don't count the days, make the days count.", image: "quote1.png" },
		// Add more quotes as needed
	];

	const cards = [
		{ title: "Card 1", description: "5 new posts", icon: "icon1.png" },
		{ title: "Card 2", description: "10 new messages", icon: "icon2.png" },
		{ title: "Card 3", description: "3 new notifications", icon: "icon3.png" },
		{ title: "Card 4", description: "7 new tasks", icon: "icon4.png" },
	];

	const events = [
		{
			icon: "fas fa-pen",
			color: "green",
			date: "2 days ago",
			description: "2 New Home Rooms Created",
			images: ["quote", "nodata", "quote", "nodata"],
			link: "http://google.com",
		},
		{
			icon: "fas fa-camera",
			color: "blue",
			date: "5 days ago",
			description: "New Photos Uploaded",
			images: ["quote", "nodata"],
			link: "http://example.com",
		},
		{
			icon: "fas fa-bell",
			color: "orange",
			date: "10 days ago",
			description: "10 New Notifications",
			images: ["5.png", "6.png"],
			link: "http://example.com",
		},
		{
			icon: "fas fa-tasks",
			color: "purple",
			date: "1 day ago",
			description: "Task Completed",
			images: ["7.png", "8.png"],
			link: "http://example.com",
		},
	];

	const [data, setData] = useState([]);

	useEffect(() => {
		//setData(dd.data);
		const get_it = async () => {
			await Helper.fastGet(
				Services.courses_manager.backend + "api/dashboard",
				"fail to get api/dashboard",
				(response) => {
					Helper.cl(response, "response");
					setData(response.data?.data);
				},
				() => {}
			);
		};
		get_it();
	}, []);

	useEffect(() => {
		if (!reportedContentNoData) {
			SetreportedContentsMd(8);
			if (!mostConsumedCollaborationNoData) {
				setMostConsumedCollaborationMd(4);
				setQuoteMd(0);
			} else {
				setMostConsumedCollaborationMd(0);
				setQuoteMd(4);
			}
		} else {
			SetreportedContentsMd(0);
			if (!mostConsumedCollaborationNoData) {
				setMostConsumedCollaborationMd(4);
				setQuoteMd(8);
			} else {
				setMostConsumedCollaborationMd(0);
				setQuoteMd(12);
			}
		}
		// ,mostConsumedCollaborationNoData

		//     setQuoteMd

		//     setMostConsumedCollaborationMd
	}, [reportedContentNoData, mostConsumedCollaborationNoData]);

	useEffect(() => {
		//setData(dd.data);
		const get_it = async () => {
			await Helper.fastGet(
				Services.courses_manager.backend + "api/dashboard/statistics",
				"fail to get api/dashboard/statistics",
				(response) => {
					Helper.cl(response, "response");
					setStatistics(response.data?.data);
				},
				() => {}
			);
		};
		get_it();
	}, []);

	const reportedContents = () => (
		<GReportedContent
			setNoData={() => {
				SetreportedContentsMd(0);
			}}
		/>
	);

	const [quoteData, setQuote] = useState([]);
	useEffect(() => {
		const get_it = async () => {
			await Helper.fastGet(
				Services.auth_organization_management.backend + "api/dashboard/quote",
				"fail to get quotes",
				(response) => {
					Helper.cl(response, "quote response");
					setQuote(response?.data?.data);
				},
				() => {}
			);
		};
		get_it();
	}, []);

	const quote = () => (
		<Col xs="12" md="12" style={{ background: "#F9F9F9" }}>
			{/* {Helper.js(quoteData,"quote data")} */}
			<div
				style={{
					display: "flex",
					flexDirection: "column",
					justifyContent: "center",
					fontSize: "12px",
					fontWeight: 400,
					alignItems: "flex-start",
					height: "100px",
					marginTop: "40px",
				}}
			>
				<div style={{ paddingRight: "10px", marginRight: "10px" }}>{quoteData?.quote}</div>
				<div style={{ fontWeight: 700 }}>- {quoteData?.author}</div>
			</div>
			<img src={quoteimg} alt="Quote" style={{ position: "absolute", right: "0px", top: "-50%", height: "75px", width: "auto" }} />
		</Col>
	);

	const [newOrg, setNewOrg] = useState(true);
	//-------------------
	//   import { useState } from 'react';
	// import { useEffect } from 'react';
	// import Helper from 'components/Global/RComs/Helper';
	// import { Services } from 'engine/services';

	useEffect(() => {
		const get_it = async () => {
			await Helper.fastPost(
				Services.auth_organization_management.backend + "api/generic/getTableData",
				{ payload: { TableName: "organizations", id: 247 } },
				"fail to get",
				(response) => {
					setNewOrg(!response.data?.records?.[0]?.firstEdited);
				},
				() => {},
				false
			);
		};
		get_it();
	}, []);

	// (url, payload, successText, failText, success = () => {}, fail = () => {}, showSwal = true)
	//----------------------------

	const mostConsumedCollaboration = () => (
		<GConsumedContents
			setNoData={(x) => {
				setmostConsumedCollaborationNoData(x);
			}}
		/>
	);

	if (!newOrg)
		return (
			<Container>
				<Row>
					<RDiscover
						link={`/school-management/create`}
						lines={[
							<p style={{ color: "white", fontSize: "17px" }}>
								Seems you're <span style={{ color: "#F58B1F" }}>new</span> here
							</p>,
							<p style={{ color: "white", fontSize: "26px" }}>Start By Setting up your school</p>,
						]}
					/>
				</Row>

				<Row>
					<Col>Video</Col>
					<Col>
						<GCalender advanced={false} />
					</Col>
				</Row>
			</Container>
		);

	return (
		<Container>
			<GGreetingSearch />
			{/* <Row className="" style={{marginTop:"50px"}}>

{reportedContentsMd?
  <Col xs="12" md={reportedContentsMd}>
    {reportedContents()}
</Col>:<></>}
{quoteMd?<Col xs="12" md={quoteMd}>
    {quote()}
</Col>:<></>}
{mostConsumedCollaborationMd?
<Col xs="12" md={mostConsumedCollaborationMd}>
    {mostConsumedCollaboration()}
</Col>:<></>}

</Row> */}

			<Row className="" style={{ marginTop: "50px" }}>
				<Col xs="12" md={6}>
					{reportedContents()}
				</Col>
				<Col xs="12" md={6}>
					{mostConsumedCollaboration()}
				</Col>
			</Row>

			<Row>
				<Col xs="12" md={12}>
					{quote()}
				</Col>
			</Row>
			<GStatisticsRow data={statistics.statistics} />
			{/* <Row className="mt-4">
        {cards.map((card, index) => (
          <Col key={index} xs="12" md="3">
            <Card>
              <CardBody>
                <CardTitle className="font-weight-bold text-gray font-size-12">{card.title}</CardTitle>
                <CardText className="font-size-14 font-weight-bold">{card.description}</CardText>
                <FontAwesomeIcon icon={card.icon} />
              </CardBody>
            </Card>
          </Col>
        ))}
      </Row> */}
			<Row className="mt-4">
				{/* First column */}
				<Col xs="12" md="7">
					{/* Table placeholder (75% height) */}
					<div style={{}}>
						<RFlex>
							<h6>{tr`Recently added users`}</h6>
						</RFlex>
						<RecentAddedUser />
					</div>
					{/* Swiper of cards placeholder (25% height) */}
					<div style={{}}>
						<Col xs={12} md={6} style={{ marginBottom: "55px" }}>
							<h6 className="mb-3">{tr`Recent Added Roles`}</h6>
							<RFlex styleProps={{ flexWrap: "wrap", height: "300px" }}>
								{usersAndPermissionsData.recentAddedRolesLoading ? (
									<Loader />
								) : (
									<div style={{ height: "300px" }}>
										{/* {Helper.js(usersAndPermissionsData?.recentAddedRoles,"usersAndPermissionsData?.recentAddedRoles")} */}
										{usersAndPermissionsData?.recentAddedRoles?.length ? (
											<>
												{usersAndPermissionsData?.recentAddedRoles?.map((role) => (
													<RRole role={role} lister={true} />
												))}
											</>
										) : (
											<>
												<REmptyData />
											</>
										)}
									</div>
								)}
							</RFlex>
						</Col>
					</div>
				</Col>

				{/* Second column */}
				<Col xs="12" md="5" className="d-flex align-items-center justify-content-center">
					<div>
						<GCalender advanced={false} />
					</div>
				</Col>
			</Row>

			<Row className="mt-4">
				<Col xs={12} md={12}>
					{
						// (!(colAreaProvidersNoData&&colAreaClientsNoData&&colAreaConsumedContentsNoData))?
						<RColumn>
							{/* {(!colAreaProvidersNoData)?<> */}

							<RHeader title="Providers" />
							<GCollaborationAreaProviders
								setNoData={(x) => {
									setcolAreaClientsNoData(x);
								}}
							/>

							{/* {(!colAreaClientsNoData)?<>  */}

							<RHeader title="Clients" />
							<GCollaborationAreaClients
								setNoData={(x) => {
									setcolAreaProvidersNoData(x);
								}}
							/>

							<RHeader title="consumed contents" />
							<GCollaborationAreaConsumedContents
								setNoData={(x) => {
									setcolAreaConsumedContentsNoData(x);
								}}
							/>

							{/* </>:<></>} */}
						</RColumn>
						// :<></>
					}
				</Col>

				<Col xs={12} md={6}>
					<div
						style={{
							display: "flex",
							borderStartStartRadius: "150px",
							left: "20vw",
							background: "#F9F9F9",
							padding: "33px",
							paddingLeft: "100px",
						}}
					>
						{events.map((e) => (
							<EventCard {...e} />
						))}
					</div>
				</Col>
			</Row>
			{/* //#hazem add agreements from collaboration. */}
		</Container>
	);
};

export default GAdminDashboard;
