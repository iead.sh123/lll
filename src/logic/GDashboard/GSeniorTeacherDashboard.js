import React, { useEffect, useState } from "react";

import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";

export const DiscussionContext = React.createContext();
import { Container, Row, Col, Card, CardBody, CardTitle, CardText, InputGroup, InputGroupAddon, Input } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import quote from "assets/img/quote.png";
import nodata from "assets/img/no-data.png";
import GCalender from "logic/calender/GCalender";

import { usersAndPermissionsReducer, initialState } from "logic/UsersAndPermissions/State/UsersAndPermissions.reducer";
import { useReducer } from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import RecentAddedUser from "logic/UsersAndPermissions/RecentActivities/RecentAddedUser";
import RRole from "view/UsersAndPermissions/RRole";
import Helper from "components/Global/RComs/Helper";
import { getRecentAddedRolesAsync } from "logic/UsersAndPermissions/State/UsersAndPermissions.action";
import { getRecentUserTypesAsync } from "logic/UsersAndPermissions/State/UsersAndPermissions.action";
import { getRecentActivitiesAsync } from "logic/UsersAndPermissions/State/UsersAndPermissions.action";
import Loader from "utils/Loader";
import DashboardStatisticsCard from "./DashboardStatisticsCard";
import EventCard from "./EventCard";
import { event } from "jquery";
import RTitle from "components/Global/RComs/themed/RTitle/RTitle";
import { GCollaborationAreaProviders } from "logic/Collaboration/GCollaborationAreaProviders";
import { GCollaborationAreaConsumedContents } from "logic/Collaboration/GCollaborationAreaConsumedContents";
import { GCollaborationAreaClients } from "logic/Collaboration/GCollaborationAreaClients";
import { GConsumedContents } from "logic/Collaboration/GConsumedContents";
import RColumn from "view/RComs/Containers/RColumn";
import RTimeLine from "./RTimeLine";
import GGreetingSearch from "./GGreetingSearch";
import GLessonLister from "logic/Courses/CourseManagement/Lessons/SectionLessonLister/GLessonLister";
import GStatisticsRow from "./GStatisticsRow";
import GMyCourses from "logic/Courses/MyCourses/GMyCourses";
import GCurricula from "logic/Courses/MyCurricula/GMyCurricula";
import GReportedContent from "./GReportedContent";
import { Services } from "engine/services";
import RLister from "components/Global/RComs/RLister";
import moment from "moment";
import GTodayLessons from "./GTodayLessons";
import GRecentlyAddedContents from "./GRecentlyAddedContents";

export const GSeniorTeacherDashboard = () => {
	const [data, setData] = useState([]);
	const [recentLessons, setRecentLessons] = useState([]);
	const [recentTasks, setRecentTasks] = useState([]);
	const [recentlyAddedContents, setRecentlyAddedContents] = useState([]);
	const [statistics, setStatistics] = useState([]);
	const [todayActivities, setTodayActivities] = useState([]);

	const [reportedContentsMd, SetreportedContentsMd] = useState(4);
	const [reportedContentNoData, setreportedContentNoData] = useState(true);
	const [mostConsumedCollaborationNoData, setmostConsumedCollaborationNoData] = useState(false);

	useEffect(() => {
		//setData(dd.data);
		const get_it = async () => {
			await Helper.fastGet(
				Services.courses_manager.backend + "api/dashboard",
				"fail to get api/dashboard",
				(response) => {
					Helper.cl(response, "response");
					setData(response.data?.data);
				},
				() => {}
			);
			await Helper.fastGet(
				Services.courses_manager.backend + "api/dashboard/recent-lessons",
				"fail to get api/dashboard/recent-lessons",
				(response) => {
					Helper.cl(response, "response");
					setRecentLessons(response.data?.data);
				},
				() => {}
			);
			await Helper.fastGet(
				Services.courses_manager.backend + "api/dashboard/recent-tasks",
				"fail to get api/dashboard/recent-tasks",
				(response) => {
					Helper.cl(response, "response");
					setRecentTasks(response.data?.data);
				},
				() => {}
			);
			await Helper.fastGet(
				Services.courses_manager.backend + "api/dashboard/recently-added-contents",
				"fail to get api/dashboard/recently-added-contents",
				(response) => {
					Helper.cl(response, "response");
					setRecentlyAddedContents(response.data?.data);
				},
				() => {}
			);
			await Helper.fastGet(
				Services.courses_manager.backend + "api/dashboard/statistics",
				"fail to get api/dashboard/statistics",
				(response) => {
					Helper.cl(response, "response");
					setStatistics(response.data?.data);
				},
				() => {}
			);
			await Helper.fastGet(
				Services.courses_manager.backend + "api/dashboard/today",
				"fail to get api/dashboard/today",
				(response) => {
					Helper.cl(response, "response");
					setTodayActivities(response.data?.data);
				},
				() => {}
			);
		};
		get_it();
	}, []);

	const [recentlyCreated, setRecentlyCreated] = useState([]);
	useEffect(() => {
		const get_it = async () => {
			await Helper.fastGet(
				Services.auth_organization_management.backend + "api/dashboard/recently-created",
				"fail to get",
				(response) => {
					setRecentlyCreated(response.data?.data);
				},
				() => {}
			);
		};
		get_it();
	}, []);

	const viewContent = (type, id) => {
		alert(type + " " + id);
	};
	const items = [
		{ icon: "fa fa-pen", title: "First Event", description: "This is the first event", image: null },
		{ icon: "fa fa-camera", title: "Second Event", description: "This is the second event", image: null },
		{ icon: null, title: "Third Event", description: "This is the third event", image: "image.png" },
	];

	const _records =
		recentlyCreated.items && recentlyCreated.items.length > 0
			? recentlyCreated.items?.map((rc) => ({
					title: "REcently",
					details: [
						{
							key: "Creator",
							value: () => (
								<span>
									<img src={Services.storage.files + rc?.creator_image?.hash_id} style={{ height: "30px", borderRadius: "50%" }}>
										{" "}
									</img>{" "}
									{rc?.creator_full_name}
								</span>
							),
						},
						{ key: "Type", value: rc?.content_type },
						// { key: "content_image", value:  },
						{ key: "Curriculm", value: rc?.curriculum_name },
						{ key: "date", value: moment(rc?.created_at).format("DD MMM") },
					],

					actions: [
						{
							name: tr("view"),
							icon: "fa fa-eye",
							color: "white",
							onClick: () => {
								viewContent(rc?.content_type, rc?.id);
							},
						},
					],
			  }))
			: [];

	// In your component...
	return (
		<Container>
			<GGreetingSearch />

			<Row className="mt-4">
				<Col xs="6" md={mostConsumedCollaborationNoData ? "12" : "7"} style={{ background: "#F9F9F9" }}>
					{/* < GLessonLister today={true}/> */}
					{/* < GLessonLister today={true}/>  */}
					{/* <GTodayLessons lessons={recentLessons}/> */}
					<GTodayLessons lessons={data?.recentLessons} mode="student" />
				</Col>

				{mostConsumedCollaborationNoData ? (
					<Col xs="6" md={mostConsumedCollaborationNoData ? "0" : "5"}>
						<GConsumedContents
							setNoData={(x) => {
								setmostConsumedCollaborationNoData(x);
							}}
						/>
					</Col>
				) : (
					<></>
				)}
			</Row>
			<GStatisticsRow />

			<Row className="mt-4">
				{/* First column */}
				<Col xs="12" md="7">
					<div style={{ height: "100%" }}>
						Recently Created Contents
						{/* {Helper.jstree(recentlyCreated,"recentlyCreated : ")} */}
						<RLister Records={_records}></RLister>
						<GRecentlyAddedContents />
					</div>
				</Col>

				<Col xs="12" md="5" className="d-flex align-items-center justify-content-center">
					<div>
						<GCalender advanced={false} />
					</div>
				</Col>
			</Row>

			<Row>
				<GCurricula advanced={false} />
			</Row>
		</Container>
	);
};

export default GSeniorTeacherDashboard;
