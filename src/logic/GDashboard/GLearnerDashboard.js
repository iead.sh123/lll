import React, { useEffect, useState } from "react";

import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";

export const DiscussionContext = React.createContext();
import { Container, Row, Col, Card, CardBody, CardTitle, CardText, InputGroup, InputGroupAddon, Input } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import quote from "assets/img/quote.png";
import nodata from "assets/img/no-data.png";
import GCalender from "logic/calender/GCalender";

import { usersAndPermissionsReducer, initialState } from "logic/UsersAndPermissions/State/UsersAndPermissions.reducer";
import { useReducer } from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import RecentAddedUser from "logic/UsersAndPermissions/RecentActivities/RecentAddedUser";
import RRole from "view/UsersAndPermissions/RRole";
import Helper from "components/Global/RComs/Helper";
import { getRecentAddedRolesAsync } from "logic/UsersAndPermissions/State/UsersAndPermissions.action";
import { getRecentUserTypesAsync } from "logic/UsersAndPermissions/State/UsersAndPermissions.action";
import { getRecentActivitiesAsync } from "logic/UsersAndPermissions/State/UsersAndPermissions.action";
import Loader from "utils/Loader";
import DashboardStatisticsCard from "./DashboardStatisticsCard";
import EventCard from "./EventCard";
import { event } from "jquery";

import { GCollaborationAreaProviders } from "logic/Collaboration/GCollaborationAreaProviders";
import { GCollaborationAreaConsumedContents } from "logic/Collaboration/GCollaborationAreaConsumedContents";
import { GCollaborationAreaClients } from "logic/Collaboration/GCollaborationAreaClients";
import { GConsumedContents } from "logic/Collaboration/GConsumedContents";
import RColumn from "view/RComs/Containers/RColumn";
import RTimeLine from "./RTimeLine";
import GGreetingSearch from "./GGreetingSearch";
import GLessonLister from "logic/Courses/CourseManagement/Lessons/SectionLessonLister/GLessonLister";
import GStatisticsRow from "./GStatisticsRow";
import GMyCourses from "logic/Courses/MyCourses/GMyCourses";
import { fileImage } from "components/Global/RComs/RResourceViewer/RFile";
import { relativeDate } from "utils/dateUtil";
import RHeader from "components/Global/RComs/RHeader";
import GStudentSwitch from "logic/General/GStudentSwitch";
import GRecentlyAddedCourses from "./GRecentlyAddedCourses";
import GTodayLiveSessions from "./GTodayLiveSessions";
//import RRecentlyVisitedCourses from "./RRecentlyVisitedCourses";
import GCoursesLine from "./GCoursesLine";
import { Services } from "engine/services";
import RLearningProgress from "./RLearningProgress";
import GLiveSessions from "./rc/GLiveSessions";
import GRecentlyVisited from "./rc/GRecentlyVisited";
import RDiscover from "./rc/RDiscover";
import { GHome } from "logic/landing/Home/GHome";
import RTitle from "components/Global/RComs/Collaboration/RTitle";
import { getFileExtension } from "utils/handelText";
import { getFileNameWithoutExtension } from "utils/handelText";
import { getRelativeDate } from "utils/dateUtil";
import moment from "moment";
import RHidable from "./RHidable";
import RNoDataLine from "components/Global/RComs/RNoDataLine";

const GLearnerDashboard = () => {
	const [data, setData] = useState([]);
	// const [recentLessons,setRecentLessons]=useState([]);
	// const [recentTasks,setRecentTasks]=useState([]);
	//const [recentlyAddedContents,setRecentlyAddedContents]=useState([]);
	const [statistics, setStatistics] = useState([]);
	const [todayActivities, setTodayActivities] = useState([]);
	const [recentCourses, setRecentCourses] = useState([]);

	const link = (cid, mid) => `/g/course-management/course/${cid}/cohort/320/modules/view/student?moduleId=` + mid;
	const getTimeLineItemFromTask = (t, outdated = false) =>
		t.type == "quizzes"
			? {
					link: link(t.module?.course?.id, t?.module?.id),
					icon: outdated ? "fa fa-pen" : "fa fa-pen",
					title: "Quiz : " + t.quiz.name,
					description: getRelativeDate(t.endDate),
					image: null,
			  }
			: t.type == "assignments"
			? {
					link: link(t.module?.course?.id, t?.module?.id),
					icon: outdated ? "fa fa-pen" : "fa fa-pen",
					title: "Assignment :" + t.assignment.name,
					description: getRelativeDate(t.dueDate),
					image: null,
			  }
			: t.type == "exams"
			? {
					link: link(t.module?.course?.id, t?.module?.id),
					icon: outdated ? "fa fa-pen" : "fa fa-question-mark",
					title: "Prepare for " + t.exam.name,
					description: getRelativeDate(t.closeDate),
					image: null,
			  }
			: t.type == "polls"
			? {
					link: link(t.module?.course?.id, t?.module?.id),
					icon: outdated ? "fa fa-pen" : "fa fa-question-mark",
					title: "Prepare for " + t.poll.name,
					description: getRelativeDate(t.closeDate),
					image: null,
			  }
			: t.type == "projects"
			? {
					link: link(t.module?.course?.id, t?.module?.id),
					icon: outdated ? "fa fa-pen" : "fa fa-question-mark",
					title: "Prepare for " + t.project.name,
					description: getRelativeDate(t.closeDate),
					image: null,
			  }
			: t.type == "surveys"
			? {
					link: link(t.module?.course?.id, t?.module?.id),
					icon: outdated ? "fa fa-pen" : "fa fa-question-mark",
					title: "Prepare for " + t.survey.name,
					description: getRelativeDate(t.closeDate),
					image: null,
			  }
			: { icon: outdated ? "fa fa-pen" : "fa fa-pen", title: t?.type + "", description: "This is the first event", image: null };

	const items1 = data?.recentTasks?.activeTasks?.map((t) => getTimeLineItemFromTask(t));
	const overDueItems = data?.recentTasks?.outDatedTasks?.map((t) => getTimeLineItemFromTask(t, true));

	useEffect(() => {
		//setData(dd.data);
		const get_it = async () => {
			await Helper.fastGet(
				Services.courses_manager.backend + "api/dashboard",
				"fail to get api/dashboard",
				(response) => {
					Helper.cl(response, "dashboard response");
					setData(response.data?.data);
				},
				() => {}
			);
			//  await Helper.fastGet(Services.courses_manager.backend+"api/dashboard/recent-lessons","fail to get api/dashboard/recent-lessons",(response)=>{Helper.cl(response,"response");  setRecentLessons(response.data?.data)  },()=>{});

			await Helper.fastPost(
				Services.courses_manager.backend + "api/courses/search",
				{ isNew: true, isMaster: false },
				"success",
				"fail",
				(response) => {
					Helper.cl(response, "response");
					setRecentCourses(response?.data?.courses?.data);
				},
				() => {},
				false
			);

			//  await Helper.fastGet(Services.courses_manager.backend+"api/dashboard/recent-tasks","fail to get api/dashboard/recent-tasks",(response)=>{Helper.cl(response,"response");  setRecentTasks(response.data?.data?.recentTaks)  },()=>{});
			//await Helper.fastGet(Services.courses_manager.backend+"api/dashboard/recently-added-contents","fail to get api/dashboard/recently-added-contents",(response)=>{Helper.cl(response,"response");  setRecentlyAddedContents(response.data?.data?.recentlyAddedContent)},()=>{});
			await Helper.fastGet(
				Services.courses_manager.backend + "api/dashboard/statistics",
				"fail to get api/dashboard/statistics",
				(response) => {
					Helper.cl(response, "response");
					setStatistics(response.data?.data);
				},
				() => {}
			);
			await Helper.fastGet(
				Services.courses_manager.backend + "api/dashboard/today",
				"fail to get api/dashboard/today",
				(response) => {
					Helper.cl(response, "response");
					setTodayActivities(response.data?.data);
				},
				() => {}
			);

			//----------------------------Courses
			// await Helper.fastPost(
			//   Services.courses_manager.backend+"api/courses/search",
			//   { 'new':true ,'isMaster':false},
			//   "success","fail",
			//   (response)=>{

			//   setCourses1(response?.data?.courses?.data)  },
			//   ()=>{},
			//   false);

			//   await Helper.fastPost(
			//   Services.courses_manager.backend+"api/courses/search",
			//   { 'new':true ,'isInstance':true},
			//   "success","fail",
			//   (response)=>{Helper.cl(response,"response");  setCourses2(response?.data?.courses?.data)  },
			//   ()=>{},
			//   false);

			await Helper.fastPost(
				Services.courses_manager.backend + "api/courses/search",
				{ new: true, isPublished: true },
				"success",
				"fail",
				(response) => {
					Helper.cl(response, "response");
					setCourses3(response?.data?.courses?.data);
				},
				() => {},
				false
			);
		};
		get_it();
	}, []);

	// const [courses1, setCourses1] = useState([]);
	// const [courses2, setCourses2] = useState([]);
	const [courses3, setCourses3] = useState([]);

	//const items= recentTasks?.activeTasks?.map(t=>getTimeLineItemFromTask(t))

	const reddivStyle = {
		display: "flex",
		flexDirection: "column",
		alignItems: "flex-start",
		color: "red",
	};

	const greendivStyle = {
		display: "flex",
		flexDirection: "column",
		alignItems: "flex-start",
		color: "green",
	};

	const orangedivStyle = {
		display: "flex",
		flexDirection: "column",
		alignItems: "flex-start",
		color: "orange",
	};

	const smileyStyle = {
		fontSize: "12px",
		marginRight: "3px",
	};

	const textStyle = {
		fontSize: "12px",
		marginRight: "3px",
	};

	const [myCoursesCount, setMyCoursesCount] = useState(0);

	const fileItem = (title, filename, size, time, courseName, type, link, icon) => {
		//alert(type);
		return {
			link: link,
			icon: icon,
			title: title,
			description: (
				<>
					{" "}
					<div style={{ fontSize: "14px", color: "black" }}>
						{filename}
						<span style={{ fontSize: "11px", color: "gray", marginLeft: "10px" }}>{size}</span>
						<span style={{ fontSize: "20px", color: "black", marginLeft: "10px" }}>•</span>
						<i classname={"fa-clock"} style={{ fontSize: "11px", color: "#668AD7", marginLeft: "10px" }} />
						<span style={{ fontSize: "11px", color: "#668AD7", marginLeft: "5px" }}>{time}</span>
					</div>
					<div style={{ fontSize: "11px", fontWeight: "bold", color: "gray" }}>{courseName}</div>
				</>
			),

			image: fileImage(type),
		};
	};
	// const s1=data.recentLessons[0]?.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.lessonDate?.split(' ')[0]
	// const s2=data.recentLessons[0]?.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.lessonTime
	// Helper.cl( s1,"df1 s1");
	// Helper.cl( s2,"df1 s2");
	// Helper.cl( getRelativeDate(s1+'T'+s2 ),"df1 s2");

	const sessions1 = data?.recentLessons?.map((e) => {
		return (
			// {"_id":"653129a529b5b125ef0c57d7",
			//"name":"Laravel 10 , RC Tests Organization",
			//date_Synchronized":"2023-10-19 11:54:36","
			//"color":"#d90d0d",oo"nextLesson":{"id":519,"subject":"What is Python?","lesson_date":"2023-10-30 00:00:00","end_date":null,"lesson_order":1,"lesson_time":"09:00:00","published":1,"module_id":177,"lesson_plan_id":null,"creator_id":1,"created_at":"2023-10-24T11:50:20.000000Z","updated_at":"2023-10-24T11:50:20.000000Z"},"todayOrTommorrowUnpublishedLessons":[],"students":0,"rating":0}
			{
				time: moment(e.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.lesson_time, "HH:mm:ss").format("hh:mm"),
				AmPm: moment(e.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.lesson_time, "HH:mm:ss").format("A"),
				cours: e.name,
				lessonTitle: e.nextLesson.todayOrTommorrowUnpublishedLessons?.[0]?.subject, //"the best way to connect people" ,
				leftTime: getRelativeDate(
					e.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.lessonDate?.split(" ")[0] +
						"T" +
						e.nextLesson?.todayOrTommorrowUnpublishedLessons?.[0]?.lesson_time
				),
				meetingId: e?.nextLesson?.live_session?.meeting_id,
			}
		);
	});

	const sessions = data?.recentLessons
		?.map((e) => {
			return {
				time: moment(e?.nextLesson?.lesson_time, "HH:mm:ss").format("hh:mm"),
				AmPm: moment(e?.nextLesson?.lesson_time, "HH:mm:ss").format("A"),
				cours: e.name,
				lessonTitle: e?.nextLesson?.subject, //"the best way to connect people" ,
				leftTime: getRelativeDate(e?.nextLesson?.lesson_date?.split(" ")[0] + "T" + e?.nextLesson?.lesson_time),
				meetingId: e?.nextLesson?.livesession?.live_session?.meeting_id,
			};
		})
		.flat();
	// const recentlyUploadedFiles =

	// //hf1
	// [
	//   fileItem("File name 1.xlsx",23,"2 hours ago","history","xlx"),
	//   fileItem("File name 2.xlsx",23,"4 hours ago","English","jpg"),
	//   fileItem("English grammer.xlsx",23,"2 days ago","history","mp4"),
	//   fileItem("understnaindg.pdf",23,"2 hours ago","Geography","pdf"),
	//   // { icon: null, title: 'Third Event', description: 'This is the third event', image: 'image.png' },
	// ];

	//?.map(e=>fileItem(e.name,e.id,e.createdAt,"history",e.mime_type))
	// In your component...
	return (
		<Container>
			{/* ------------------1 greating search----------------------------- */}
			<GGreetingSearch />
			{/* {Helper.js()} */}
			{/* <Row>
  <Col md={12}>
  <GRecentlyAddedCourses />
  </Col>
  <Col md={12}>
   <GTodayLiveSessions />
   </Col>
</Row> */}
			{/* {Helper.js(data?.recentLessons,"data?.recentLessons")}
   <hr/>
     {Helper.js(sessions,"sessions")} */}
			{/* ------------------1 cover + my courss----------------------------- */}
			{myCoursesCount == 0 ? (
				<RDiscover
					lines={[
						<p style={{ color: "white", fontSize: "17px" }}>
							You aren't enrolled in any courses <span style={{ color: "#F58B1F" }}>yet</span>
						</p>,
						<p style={{ color: "white", fontSize: "26px" }}>Discover All Courses</p>,
					]}
				/>
			) : (
				<Row>
					<Col md={8}>
						<RHidable title="Recently added courses" count={myCoursesCount} nodata="No Recently added courses yet">
							<GMyCourses setCount={setMyCoursesCount} advanced={false} perView={2} />
						</RHidable>
					</Col>

					<Col md={4}>
						<RHidable
							count={sessions?.length}
							nodataComponent={
								<>
									<RHeader title="Today's Live Sessions" count={sessions?.length} />
									<RNoDataLine messageNoData="No Live sessions for today" />
								</>
							}
						>
							<GLiveSessions userType={"learner"} liveSessions={sessions} />
						</RHidable>
					</Col>
				</Row>
			)}
			{/* ------------------recent courses----------------------------- */}
			<Row>
				<Col md={12}>
					<RHidable count={recentCourses?.length} title="Recently Added Courses" nodata="No recently added Courses Yet">
						<GCoursesLine courses={[recentCourses]} notEnrolled={true} />
					</RHidable>
				</Col>
			</Row>
			{/* ------------------recently visited----------------------------- */}
			<Row className="mt-4">
				<Col xs="6" md="6" style={{ background: "#F9F9F9" }}>
					{/* <GCoursesLine coures={"RRecentlyVisitedCourses"}/> */}

					<RHidable count={3} title="Recently Visited" nodata="No recently visited Courses">
						<GRecentlyVisited />
					</RHidable>
				</Col>
				<Col xs="6" md="6" style={{ background: "#F9F9F9" }}>
					<RTitle text="Learning Progess" />
					<RLearningProgress />
				</Col>
			</Row>
			{/* ------------------recent  courses----------------------------- */}
			<Row>
				<Col xs="6" md="5">
					{/* <RTitle text="Tasks"/> */}
					{items1?.length ? <RHeader title="Tasks" count={items1?.length} /> : <></>}
					<Col xs="6" md="5">
						{/* <RHeader title="Tasks" count={(recentTasks&&recentTasks.length)?recentTasks.allTasksCount:recentTasks.allTasksCount}/> */}
						<RTimeLine items={items1} color="#668AD7" />
						<RTimeLine items={overDueItems} color="#668AD7" />
					</Col>
					<RNoDataLine messageNoData="No recent tasks" />
				</Col>
			</Row>
			{/* ------------------statistics---------------------------- */}
			<GStatisticsRow data={statistics?.statistics} />
			<GStatisticsRow
				data={{
					community: { type: "community", title: "Community", value: "no new Posts" },
					calender: { type: "calender", title: "Calender", value: "no new events" },
					...statistics?.statistics,
				}}
			/>{" "}
			{/* ------------------recently uploaded contents----------------------------- */}
			<Row className="mt-4">
				{/* First column */}
				<Col xs="12" md="7">
					<div style={{ height: "100%" }}>
						{/* <RTimeLine items={feedback} color="#668AD7" /> */}

						<RTitle text="Recently uploaded contents" />
						<RTimeLine
							items={data?.recentlyAddedContent?.map(
								(
									e //{data?.recentlyAddedContents?.map(e=>fileItem(e.name,e.id,e.createdAt,"history",e.mime_type))}
								) =>
									e.attachment
										? fileItem(
												"attachemnt",
												getFileNameWithoutExtension(e.name),
												"",
												getRelativeDate(e.created_at),
												"Course Name",
												getFileExtension(e.name),
												`/course-management/course/null/cohort/null/modules/view/student?moduleId=null&moduleContentId=null`,
												"fas fa-external-link-alt"
												//`/course-management/course/${courseid}/cohort/null/modules/view/student?moduleId=${moduleid}&moduleContentId=${moduleContentid}`
										  )
										: //fileItem(e.name,e.id,e.createdAt,"history",e.mime_type)
										  fileItem(
												"link",
												e?.link?.link_title,
												"",
												getRelativeDate(e.created_at),
												"Course Name",
												"",
												`/course-management/course/null/cohort/null/modules/view/student?moduleId=null&moduleContentId=null`,
												"fas fa-external-link-alt"
										  )
							)}
							color="#668AD7"
						/>
						<RNoDataLine messageNoData="No recently uploaded contents" />
					</div>
				</Col>

				<Col xs="12" md="5" className="d-flex align-items-center justify-content-center">
					<div>
						<GCalender advanced={false} />
					</div>
				</Col>
			</Row>
			{/* ------------------recent  courses----------------------------- */}
			<Row className="mt-4">
				{/* <div style={{display: (myCoursesCount==0)?"hidden":"block"}}>
    <RHeader 
  title="My Courses"
  count={myCoursesCount}
  link="/g/my-courses"
  linkTitle="See All"></RHeader>
      
      <GCoursesLine coures={"couresesYouMayLike"}/>

      </div> */}
			</Row>
			{/* ------------------recent  courses----------------------------- */}
			<Row>
				<Col md={12}>
					<RHidable count={recentCourses?.length} title="Courses You May Like" nodata="No Recommendations">
						<GCoursesLine courses={[recentCourses]} notEnrolled={true} />
					</RHidable>
				</Col>
			</Row>
		</Container>
	);
};

export default GLearnerDashboard;
