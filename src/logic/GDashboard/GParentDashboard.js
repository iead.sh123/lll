import React, { useEffect, useState } from "react";

 import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom/cjs/react-router-dom.min";

export const DiscussionContext = React.createContext();
import { Container, Row, Col, Card, CardBody, CardTitle, CardText,InputGroup ,InputGroupAddon,Input  } from 'reactstrap';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import quote from "assets/img/quote.png";
import nodata from "assets/img/no-data.png";
import GCalender from "logic/calender/GCalender";



import Helper from "components/Global/RComs/Helper";

import RTimeLine from "./RTimeLine";
import GGreetingSearch from "./GGreetingSearch";
import GStatisticsRow from "./GStatisticsRow";
import GMyCourses from "logic/Courses/MyCourses/GMyCourses";
import { fileImage } from "components/Global/RComs/RResourceViewer/RFile";
import { relativeDate } from "utils/dateUtil";
import RHeader from "components/Global/RComs/RHeader";
import { Services } from "engine/services";
import GTodayLessons from "./GTodayLessons";
import { getRelativeDate } from "utils/dateUtil";
import GStudentSwitch from "logic/General/GStudentSwitch";
import { changeStudentId } from "store/actions/global/auth.actions";
import AppCheckbox from "components/UI/AppCheckbox/AppCheckbox";
import GStudentDashboard from "./GStudentDashboard";
import tr from "components/Global/RComs/RTranslator";
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";
import GAbuseReportsList from "./GAbuseReportsList";


const GParentDashboard = () => {


  const selectedChildId=521;
  const [data,setData]=useState([]);
  const [recentLessons,setRecentLessons]=useState([]);
  const [recentTasks,setRecentTasks]=useState([]);
  const [recentlyAddedContents,setRecentlyAddedContents]=useState([]);
  const [statistics,setStatistics]=useState([]);
  const [todayActivities,setTodayActivities]=useState([]);
  
  
  
  
  useEffect (()=>{
      //setData(dd.data);
    const get_it=async()=>{
     await Helper.fastGet(
      Services.courses_manager.backend+"api/dashboard",
      "fail to get api/dashboard",(response)=>{Helper.cl(response,"response");  setData(response.data?.data)  },()=>{})
     await Helper.fastGet(Services.courses_manager.backend+"api/dashboard/recent-lessons","fail to get api/dashboard/recent-lessons",(response)=>{Helper.cl(response,"response");  setRecentLessons(response.data?.data)  },()=>{});
     await Helper.fastGet(Services.courses_manager.backend+"api/dashboard/recent-tasks","fail to get api/dashboard/recent-tasks",(response)=>{Helper.cl(response,"response");  setRecentTasks(response.data?.data)  },()=>{});
     await Helper.fastGet(Services.courses_manager.backend+"api/dashboard/recently-added-contents","fail to get api/dashboard/recently-added-contents",(response)=>{Helper.cl(response,"response");  setRecentlyAddedContents(response.data?.data)},()=>{});
     await Helper.fastGet(Services.courses_manager.backend+"api/dashboard/statistics","fail to get api/dashboard/statistics",(response)=>{Helper.cl(response,"response");  setStatistics(response.data?.data)  },()=>{});
     await Helper.fastGet(Services.courses_manager.backend+"api/dashboard/today","fail to get api/dashboard/today",(response)=>{Helper.cl(response,"response");  setTodayActivities(response.data?.data)  },()=>{});
      }
      get_it(); 
    },[])
  
    const fakeRecentTasks1={
      "outDatedTasks": [
          {
              "id": 3,
              "randomizeQuestionOrder": false,
              "randomizeQuestionPartsOrder": false,
              "startDate":"2023-10-23T07:47:58.722Z",
              "endDate": "2023-10-23T07:47:58.722Z",
              "created_at": "2023-10-04T07:47:58.722Z",
              "isDefault": true,
              "updated_at": "2023-10-04T07:47:58.722Z",
              "isClosed": false,
              "exam": {
                  "id": 7,
                  "name": "QTEST",
                  "description": "<p>QTESTQTESTQTESTQTESTQTEST<\/p>",
                  "questionsCount": 1,
                  "points": 2,
                  "files": null,
                  "is_published": false,
                  "created_at": "2023-10-04T07:47:55.698Z",
                  "updated_at": "2023-10-04T07:47:55.698Z"
              },
              "type": "exams"
          },
          {
              "id": 2,
              "randomizeQuestionOrder": false,
              "randomizeQuestionPartsOrder": false,
              "startDate": "2023-10-23T07:47:58.722Z",
              "endDate": "2023-10-23T07:47:58.722Z",
              "created_at": "2023-10-04T07:45:22.296Z",
              "isDefault": true,
              "updated_at": "2023-10-04T07:45:22.296Z",
              "isClosed": false,
              "exam": {
                  "id": 5,
                  "name": "QTEST",
                  "description": "<p>QTESTQTESTQTESTQTESTQTEST<\/p>",
                  "questionsCount": 1,
                  "points": 2,
                  "files": null,
                  "is_published": false,
                  "created_at": "2023-10-04T07:45:19.937Z",
                  "updated_at": "2023-10-04T07:45:19.937Z"
              },
              "type": "exams"
          },
          {
              "id": 1,
              "randomizeQuestionOrder": false,
              "randomizeQuestionPartsOrder": false,
              "startDate": "2023-10-23T07:47:58.722Z",
              "endDate": "2023-10-23T07:47:58.722Z",
              "created_at": "2023-10-04T07:22:03.001Z",
              "isDefault": true,
              "updated_at": "2023-10-04T07:22:03.001Z",
              "isClosed": false,
              "exam": {
                  "id": 3,
                  "name": "QTEST",
                  "description": "<p>QTESTQTESTQTESTQTESTQTEST<\/p>",
                  "questionsCount": 1,
                  "points": 2,
                  "files": null,
                  "is_published": false,
                  "created_at": "2023-10-04T07:22:00.719Z",
                  "updated_at": "2023-10-04T07:22:00.719Z"
              },
              "type": "exams"
          }
      ],
      "activeTasks": 
      [
          {
              "id": 2,
              "maximumLearnersAllowed": 200,
              "passRequired": true,
              "autoGrade": true,
              "percentagePassingGrade": 0,
              "randomizeQuestionOrders": 1,
              "dueDate": "2023-10-23T07:47:58.722Z",
              "cutoffDate": "2023-10-26T07:47:58.722Z",
              "isDefault": true,
              "created_at": "2023-10-03T08:11:05.333Z",
              "updated_at": "2023-10-03T08:11:05.333Z",
              "isClosed": false,
              "assignment": {
                  "id": 5,
                  "name": "string",
                  "description": "string",
                  "points": 10,
                  "files": null,
                  "questionsCount": 1,
                  "is_published": true,
                  "created_at": "2023-10-03T08:11:01.032Z",
                  "updated_at": "2023-10-03T08:11:01.032Z"
              },
              "type": "assignments"
          },
          {
              "id": 1,
              "maximumLearnersAllowed": 200,
              "passRequired": true,
              "autoGrade": true,
              "percentagePassingGrade": 0,
              "randomizeQuestionOrders": 1,
              "dueDate": "2023-10-23T07:47:58.722Z",
              "cutoffDate": "2023-10-26T07:47:58.722Z",
              "isDefault": true,
              "created_at": "2023-10-03T08:10:54.742Z",
              "updated_at": "2023-10-03T08:10:54.742Z",
              "isClosed": false,
              "assignment": {
                  "id": 3,
                  "name": "string",
                  "description": "string",
                  "points": 10,
                  "files": null,
                  "questionsCount": 1,
                  "is_published": true,
                  "created_at": "2023-10-03T08:10:53.803Z",
                  "updated_at": "2023-10-03T08:10:53.803Z"
              },
              "type": "assignments"
          },
          {
              "id": 3,
              "canContinue": false,
              "maxAttemptsAllowed": 1,
              "passRequired": true,
              "autoGrade": true,
              "isDefault": true,
              "percentagePassingGrade": 0,
              "randomizeQuestionOrders": 0,
              "startDate": "2023-10-23T07:47:58.722Z",
              "endDate":     "2023-10-23T07:47:58.722Z",
              "created_at": "2023-10-03T08:16:18.010Z",
              "updated_at": "2023-10-03T08:16:18.010Z",
              "isClosed": false,
              "quiz": {
                  "id": 14,
                  "name": "string",
                  "description": "string",
                  "files": null,
                  "is_published": true,
                  "questionsCount": 5,
                  "points": 50,
                  "created_at": "2023-10-03T08:16:16.243Z",
                  "updated_at": "2023-10-03T08:16:16.243Z"
              },
              "type": "quizzes"
          }
      ],
      "allTasksCount": 11
  }

  const fakeRecentTasks={
    "allTasksCount":10,
    "activeTasks":[],
    "outDatedTasks":[]
  }  ;
  const recentT=(data.recentTasks&&data.recentTasks.length)?data.recentTasks: fakeRecentTasks;

     
  // const items = [
  //   { icon: 'fa fa-pen', title: 'First Event', description: 'This is the first event', image: null },
  //   { icon: 'fa fa-camera', title: 'Second Event', description: 'This is the second event', image: null },
  //   { icon: null, title: 'Third Event', description: 'This is the third event', image: 'image.png' },
  // ];
//----------------------------------------------------------------

const link=(cid,mid)=>`/g/course-management/course/${cid}/cohort/320/modules/view/student?moduleId=`+mid;
const getTimeLineItemFromTask=(t,outdated=false)=> (
  t.type=="quizzes" ?     {roundIcon:true, link :  link(t.module?.course?.id,t?.module?.id), icon:outdated?'fas fa-ban':'fa fa-pen', title:"Quiz : " +t.quiz.name, description: getRelativeDate(t.endDate)      , image: null }:
  t.type=="assignments"  ?{ roundIcon:true,link : link(t.module?.course?.id,t?.module?.id) ,icon:outdated?'fas fa-ban':'fa fa-pen', title:"Assignment :"+ t.assignment. name, description:  getRelativeDate(t.dueDate), image: null }:
  t.type=="exams"  ?{roundIcon:true,link : link(t.module?.course?.id,t?.module?.id), icon:outdated?'fas fa-ban':'fa fa-question-mark', title: "Prepare for " + t.exam.name, description:   getRelativeDate(t.closeDate), image: null }:
  t.type=="polls"  ?{roundIcon:true, link : link(t.module?.course?.id,t?.module?.id),icon:outdated?'fas fa-ban':'fa fa-question-mark', title: "Prepare for " + t.poll.name, description: getRelativeDate(  t.closeDate), image: null }:
  t.type=="projects"  ?{roundIcon:true,link : link(t.module?.course?.id,t?.module?.id), icon:outdated?'fas fa-ban':'fa fa-question-mark', title: "Prepare for " + t.project.name, description:   getRelativeDate(t.closeDate), image: null }:
  t.type=="surveys"  ?{ roundIcon:tru6e,link : link(t.module?.course?.id,t?.module?.id),icon:outdated?'fas fa-ban':'fa fa-question-mark', title: "Prepare for " + t.survey.name, description:   getRelativeDate(t.closeDate), image: null }:
{roundIcon:true, icon: outdated?'fa fa-pen':'fa fa-pen', title:t?.type+'', description: 'This is the first event', image: null }
)
 

//-----------------------------------------------------------------

  const getTimeLineItemFromTask1=(t,outdated=false)=> (
        t.type=="quizzes" ?     { icon:  outdated?'fas fa-ban':'fa fa-pen-square',roundIcon:true, title:"Quiz : " +t.quiz.name, description: relativeDate(t.startDate), image: null }:
        t.type=="assignments"  ?{ icon:  outdated?'fas fa-ban':'fa fa-pen-square',roundIcon:true, title:"Assignment :"+ t.assignment. name, description:  relativeDate(t.dueDate), image: null }:
        t.type=="exams"  ?      { icon:  outdated?'fas fa-ban':'fa fa-question-mark',roundIcon:true, title: "Prepare for " + t.exam.name, description:  relativeDate(t.startDate), image: null }:
                                { icon:  outdated?'fas fa-ban':'fa fa-pen',roundIcon:true, title:t?.type+'Empty Event', description: 'This is the first event', image: null }
  )

  const items1= data?.recentTasks?.activeTasks?.map(t=>getTimeLineItemFromTask(t))

  //   const overDueItems = [
  //   {color:"red", icon: 'fas fa-ban', title: 'First Event', description: 'This is the first event', image: null },
  //   {color:"red", icon: 'fas fa-ban', title: 'Second Event', description: 'This is the second event', image: null },
  //   // { icon: null, title: 'Third Event', description: 'This is the third event', image: 'image.png' },
  // ];

  const overDueItems =data?.recentTasks?.outDatedTasks?.map(t=>getTimeLineItemFromTask(t,true));
  
  
  const reddivStyle = {
    display: 'flex',
    flexDirection:"column",
    alignItems: 'flex-start',
    color: 'red'
  };

  const greendivStyle = {
    display: 'flex', flexDirection:"column",
    alignItems: 'flex-start',
    color: 'green'
  };

  
  const orangedivStyle = {
    display: 'flex', flexDirection:"column",
    alignItems: 'flex-start',
    color: 'orange'
  };


  const smileyStyle = {
    fontSize: '12px',
    marginRight: '3px',
    
  };


  const textStyle = {
    fontSize: '12px',
    marginRight: '3px',
    
  };


  const feedback1 = [
    { icon: 'fas fa-pen', title: 'First Event',
    description:<div style={greendivStyle}>
                  <div style={smileyStyle}>😊 Good answer</div>
          
                  <div style={textStyle}>FDGSHDFGKLSHDFGLHSDFLGHS SLKDGH SDFGJSDLFKGHSDF KGSDLKF GHr</div>
                </div>
                
  , image: null },
    { icon: 'fas fa-ban', title: 'Second Event', description: <div style={reddivStyle}>
    <div style={smileyStyle}>😊Good answer</div>
 
    <div style={textStyle}>FDGSHDFGKLSHDFGLHSDFLGHS SLKDGH SDFGJSDLFKGHSDF KGSDLKF GHr</div>
  </div>, image: null },
    { icon: 'fas fa-ban', title: 'Second Event', description: <div style={orangedivStyle}>
    <div style={smileyStyle}>😊Good answer</div>
    
    <div style={textStyle}>FDGSHDFGKLSHDFGLHSDFLGHS SLKDGH SDFGJSDLFKGHSDF KGSDLKF GHr</div>
  </div>, image: null },
    { icon: 'fas fa-ban', title: 'Second Event', description: <div style={greendivStyle}>
    <div style={smileyStyle}>😊Good answer</div>
   
    <div style={textStyle}>FDGSHDFGKLSHDFGLHSDFLGHS SLKDGH SDFGJSDLFKGHSDF KGSDLKF GHr</div>
  </div>, image: null },
    // { icon: null, title: 'Third Event', description: 'This is the third event', image: 'image.png' },
  ];

  
  // In your component...

  const [mine,setMine]=useState(true);
  useEffect(() => {
    // Get the current URL
    const currentUrl = new URL(window.location.href);

    // Get the value of the 'mine' query parameter
    const mineValue = currentUrl.searchParams.get('mine');

    // Log the value to the console or use it as needed
    setMine(mineValue);
  }, []);
  const dispatch=useDispatch();

  const children=useSelector(s=>s.auth.user.extra.children)
  const selectedStudentId=useSelector(s=>s.auth.selectedStudentId)

const float="left";
const history=useHistory();
// return <>
// {mine?"mine true":"mine false"}
// <div > 
// <AppCheckbox
//         onChange={(event) => {
          
//           history.push(
//             `${baseURL}/${genericPath}/dashboard?mine=${event?.target?.checked}`
//           );
          
//         }}
//         label={<span>{tr`show My Dashboard`}</span>}
//       />
//       {mine?<>You're showing your own dashboard</>:<>you're showing your {children.filter(c=>c.id==selectedStudentId)?.name}</>}
//       </div>

// {mine?<GStudentDashboard />

// : 

return <Container>
  <div style={{backgroundColor:"yellow",height:"px"}}>you're showing your child: {children.filter(c=>c.id==selectedStudentId)?.[0]?.name}</div>
  <GGreetingSearch/>

  {/* <GStudentSwitch/> */}
{/* {Helper.jstree(data?.recentlyAddedContent)} */}

  <Row className="mt-4">
  <Col xs="6" md="7" style={{background:"#F9F9F9"}}>
   {/* < GLessonLister today={true}/> */}

   <RHeader title="Abuse Reports" count={1}/>
    <GAbuseReportsList/> 
      
       </Col>
  <Col xs="6" md="5">
    <RHeader title="Tasks" count={(recentTasks&&recentTasks.length)?recentTasks?.allTasksCount:recentTasks?.allTasksCount}/>
    {/* {Helper.js(data?.recentTasks?.outDatedTasks)} */}
    {
    items1&&items1.length?
    <div style={{border:"lightgray solid 1px",borderRadius:"15px"}}>
    <RTimeLine items={items1} color="#668AD7" />
    </div>:<></>
    }
    {
    overDueItems&&overDueItems.length?
    <div style={{border:"lightgray solid 1px",borderRadius:"15px"}}>
    <RTimeLine items={overDueItems} color="red" />
    </div>:<></>
    }
  </Col>
</Row>
         
         {/* {Helper.js(statistics,"statistics")} */}
         <GStatisticsRow data={statistics?.statistics}/>

<Row className="mt-4">
        {/* First column */}
        <Col xs="12" md="7">
          
          <div style={{ height: '100%' }}>
            <RTimeLine items={feedback1} color="#668AD7" />
          </div>
        </Col>

     
        <Col xs="12" md="5" className="d-flex align-items-center justify-content-center">
          <div>
           <GCalender advanced={false}/>
          </div>
        </Col>
      </Row>


    
  </Container>

};

export default GParentDashboard;
