import React from 'react';

const DashboardStatisticsCard = ({ title, description, color, icon }) => {
  
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
       margin:"8px",
        borderRadius: '15px',
        height: '80px',
        width: '270px',
        padding: '10px',
        border: `1px solid ${color}`,
      }}
    >
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <div style={{ fontSize: '10px', fontWeight: 700, color: '#808080' }}>{title}</div>
        <div style={{ fontSize: '16px', fontWeight: 600, color: 'black' }}>{description}</div>
      </div>
      <div style={{display:"flex",justifyContent:"center",alignItems:"center",  backgroundColor: color, padding: '10px', borderRadius: '10px', width: '40px', height: '40px' }}>
          <i className={icon} style={{ color: "white" }} />
          </div>
    </div>
  );
};


export default DashboardStatisticsCard;
