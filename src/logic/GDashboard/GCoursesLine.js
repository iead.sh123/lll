import Helper from "components/Global/RComs/Helper";
import RCard1 from "components/Global/RComs/RCards/RCard1";
import RCard from "components/Global/RComs/RCards/RCard";
import RSwiper from "components/Global/RComs/RSwiper/RSwiper";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { SwiperSlide } from "swiper/react";
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";

const GCoursesLine = ({ courses, withProgress, perView = 3, notEnrolled = false }) => {
	const history = useHistory();
	//   {
	//     "_id": "64ca17ba69bccdcf79031892",
	//     "name": "string",
	//     "creator_id": "1",
	//     "organization_id": "1",
	//     "is_master": true,
	//     "is_published": false,
	//     "color": null,
	//     "date_Synchronized": [],
	//     "updated_at": "2023-08-02T08:45:45.000000Z",
	//     "created_at": "2023-08-02T08:45:43.000000Z",
	//     "id": 296,
	//     "cohort_id": 204,
	//     "category_id": null,
	//     "term_id": 1,
	//     "curriculum_id": 1,
	//     "extra": {
	//         "synchronous": false,
	//         "isFree": false,
	//         "closed": false,
	//         "isOneDayCourse": false,
	//         "comingSoon": false,
	//         "language": [],
	//         "isOnline": false,
	//         "numberOfHours": null,
	//         "isPrivate": false,
	//         "start_date": null,
	//         "end_date": null
	//     },
	//     "tags": [],
	//     "image_id": null,
	//     "icon_id": null,
	//     "nextLesson": {
	//         "id": 501,
	//         "subject": "lesson2",
	//         "lesson_date": "2023-10-17 00:00:00",
	//         "end_date": null,
	//         "lesson_order": 2,
	//         "lesson_time": "11:26:00",
	//         "published": 1,
	//         "module_id": 138,
	//         "lesson_plan_id": null,
	//         "creator_id": 1,
	//         "created_at": "2023-10-17T05:26:44.000000Z",
	//         "updated_at": "2023-10-17T05:26:44.000000Z"
	//     },
	//     "todayOrTommorrowUnpublishedLessons": [],
	//     "students": 1,
	//     "lessons": 2,
	//     "posts": 0,
	//     "curriculum_name": null,
	//     "grade_level_name": null,
	//     "main_course_name": null
	// }

	// Helper.cl(lessons,"lessons");
	//   const lessonCards=lessons && lessons.filter(l=>l.nextLesson).map(l=>{
	//         // Extract the date and time values from the JSON object
	//         const lessonDate = l?.nextLesson?.lesson_date.split(' ')[0];
	//         const lessonTime = l?.nextLesson?.lesson_time;

	//         // Combine the date and time
	//         const actual_time = moment(`${lessonDate} ${lessonTime}`).format("YYYY-MM-DD HH:mm:ss");
	//     Helper.cl(actual_time,"actual_time")
	//         // Create the output object
	//         // Output the result
	//           return(
	//             {
	//             link: '/course/'+l.id,
	//             color: l.color??"black",
	//             users: l.students,
	//             currentInfo: '',
	//             id: l.id,
	//             image: l.image_id,
	//             categoryName: l.category_id,
	//             TimeToStart :actual_time,
	//             progress:withProgress?30:null,
	//             //rate: 4.5,
	//             // Add other properties as needed for your component
	//           })
	// });
	// {
	//   link: '/course/3',
	//   color: 'red',
	//   users: 150,
	//   currentInfo: 'Enroll now',
	//   id: 3,
	//   image: 'course3.jpg',
	//   categoryName: 'Mobile App Development',
	//   rate: 4.7,
	//   // Add other properties as needed for your component
	// },
	// {
	//   link: '/course/4',
	//   color: 'purple',
	//   users: 80,
	//   currentInfo: 'Enroll now',
	//   id: 4,
	//   image: 'course4.jpg',
	//   categoryName: 'Machine Learning',
	//   rate: 4.6,
	//   // Add other properties as needed for your component
	// },
	//];

	//courses,withProgress

	const lessonCards1 =
		courses &&
		courses.map((l) => {
			// Extract the date and time values from the JSON object
			const lessonDate = l?.nextLesson?.lesson_date.split(" ")[0];
			const lessonTime = l?.nextLesson?.lesson_time;

			// Combine the date and time
			const actual_time = moment(`${lessonDate} ${lessonTime}`).format("YYYY-MM-DD HH:mm:ss");
			Helper.cl(actual_time, "actual_time");
			// Create the output object
			// Output the result
			return {
				link: "/course/" + l.id,
				color: l.color ?? "black",
				users: l.students,
				currentInfo: "",
				id: l.id,
				image: l.image_id,
				categoryName: l.category_id,
				TimeToStart: actual_time,
				progress: withProgress ? 30 : null,
				//rate: 4.5,
				// Add other properties as needed for your component
			};
		});
	const lessonCards =
		courses &&
		courses
			// .filter((l) => l.nextLesson)
			.map((l) => {
				// Extract the date and time values from the JSON object
				const lessonDate = l?.nextLesson?.lesson_date.split(" ")[0];
				const lessonTime = l?.nextLesson?.lesson_time;

				// Combine the date and time
				const actual_time = moment(`${lessonDate} ${lessonTime}`).format("YYYY-MM-DD HH:mm:ss");
				Helper.cl(actual_time, "actual_time");
				// Create the output object
				// Output the result
				return {
					link: l.link,
					color: l.color ?? "black",
					users: l.students,
					currentInfo: "",
					id: l.id,
					image: l.image_id,
					categoryName: l?.category_id ?? "category1",
					TimeToStart: actual_time,
					progress: withProgress ? 30 : null,
					title: l.name,
					//rate: 4.5,
					// Add other properties as needed for your component
				};
			});

	return (
		<div style={{ width: "100%", display: "flex", gap: "20px" }}>
			{/* {Helper.jstree(courses,"course cards")}  */}
			{/* {lessonCards?.length}  */}
			{/* {perView} */}
			<RSwiper style={{ width: "100%" }} perView={perView}>
				{courses?.map((course, i) => (
					<SwiperSlide>
						{() => {
							const link = notEnrolled
								? `${baseURL}/landing/course/${course.id}/course-overview`
								: `${baseURL}/${genericPath}/course-management/course/${course.id}/cohort/null/modules`;

							return (
								<div
									onClick={() => {
										history.push(`${link}`);
									}}
								>
									{Helper.js(course[0]?.id)}
									<RCard key={i} course={course} />
								</div>
							);
						}}

						{/* </div> */}
					</SwiperSlide>
				))}
			</RSwiper>
		</div>
	);
};

export default GCoursesLine;
