import React, { useEffect, useState } from "react";
import "./GReportedContent.css";

import Helper from "components/Global/RComs/Helper";
import { Services } from "engine/services";
import RDiscussionPostDesign from "view/Discussion/RDiscussionPostDesign";
import RFlex from "components/Global/RComs/RFlex/RFlex";

import styles from "view/Discussion/RDiscussionPostDesign.js";
import RSeeMoreText from "view/Discussion/RSeeMoreText";
import RHeader from "components/Global/RComs/RHeader";
import REmptyData from "components/RComponents/REmptyData";
export const DiscussionContext = React.createContext();

const GReportedContent = ({ setNoData }) => {
	const [active, setActive] = useState(0);

	const [reportedContents, setReportedContents] = useState([]);
	useEffect(() => {
		const get_it = async () => {
			await Helper.fastGet(
				Services.discussion.backend + "api/post/category/Reported",
				"fail to get",
				(response) => {
					setReportedContents(response.data?.data?.posts?.data);
				},
				() => {}
			);
		};
		get_it();
	}, []);

	useEffect(() => {
		if (!reportedContents || reportedContents.length == 0) setNoData();
	}, [reportedContents]);
	return (
		<div style={{ margin: "1rem" }}>
			<RHeader title="Reported Contents" />

			{/* {Helper.js(activitiesItems ,"raitem")} */}
			{/* <div className="Activity_title"> Recent Activities </div>  <br/> */}
			{reportedContents && reportedContents.length ? (
				<div style={{ borderRadius: "20px", border: "lightgray 1px solid" }}>
					{reportedContents.map((post, index) => (
						// <div className="" onClick={()=>setActive(index)} style={{borderLeft:active==[index]?'5px solid rgb(28,113,170)':'5px solid rgb(65,181,255)'}}>
						<div style={{ borderBottom: index < reportedContents.length - 1 ? "lightgray 1px solid" : "" }}>
							{/* {index}
                          {reportedContents.length} */}
							<RFlex>
								<img src={`${Services.discussion.file}${post?.user?.profile_image}`} className={styles.post_img + " cursor-pointer"} />
								<div className={styles.post_name}>
									<p className={`font-weight-bold cursor-pointer  ${styles.paragraph}`}>
										{`${post?.user?.full_name}`}

										{post?.cohort_name && `${` | ${post?.cohort_name}`}`}
									</p>
									{post?.user?.type && <p className={`font-weight-normal ${styles.paragraph}`}>{post?.user?.type}</p>}
								</div>
							</RFlex>
							{post?.title && (
								<RFlex>
									<p className="m-0" style={{ fontWeight: 600 }}>
										{post?.title}
									</p>
								</RFlex>
							)}
							{/* see more on post */}
							<RSeeMoreText text={post?.text} postId={post?.id} contentId={post?.content_id} replyId={post?.reply_id} />
							<RFlex style={{ paddingBottom: "20px" }}></RFlex>

							{/* 
                        {username}
                        {"_id":"647869abc5ac61d5c9040fcb",
                        "text":"wwwwwwwwwwwwwww",
                        "title":"wwwwwwwwwwwwwww",
                        "cohort_id":10,
                        "enable_comments":true,
                        "private_comments":false,
                        "mandatory_comments":false,
                        "mandatory_replies":null,
                        "cohort_name":"section_kg2",
                        "published_at":"2023-06-01T09:49:30+00:00",
                        "user_id":5,"user":{"_id":{"$oid":"6461d0e3be6081707840227e"},"id":5,"full_name":"Eyad Sh","profile_image":"mvjwvlkxprzbzyweamngeqydo","updated_at":{"$date":{"$numberLong":"1684651776181"}},"created_at":{"$date":{"$numberLong":"1684651776181"}}},"content_id":366,"comments":[],"likes":[],"nb_comments":0,"nb_likes":0,"interactions_count":0,"last_comment_id":null,"attachments":[],"tags":[],"reporting_users":[{"user_id":1,"full_name":"user full","reason":"Illegal activity"},{"user_id":1,"full_name":"user full","reason":"dasdsadsad"}],"dest_user_id":5,"parent_content_id":366,"updated_at":"2023-06-01T09:51:20.522000Z","created_at":"2023-06-01T09:49:31.299000Z"} */}

							{/* {Helper.js(item ,"raitem")} */}
							{/* {item?.[0]} */}

							{/* </div> */}
							{/* {item?.[1]?.map(i=>(
                            <div className="Activity_item">{i}</div>
                          ))} */}
						</div>
					))}
				</div>
			) : (
				<REmptyData />
			)}
		</div>
	);
};
export default GReportedContent;
