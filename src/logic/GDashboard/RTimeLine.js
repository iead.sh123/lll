import React from 'react';
import PropTypes from 'prop-types';
import { Col } from 'reactstrap';
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';

const NotPublishedIcon = () => {
  const iconStyle = {
    color: '#F58B1F',
    marginRight: '5px', // Adjust spacing as needed
    fontSize: '20px', // Adjust the size of the icon
  };

  const labelStyle = {
    color: '#F58B1F',
    fontSize: '12px',
    Font:"Cairo",
    Weight:"600"
  };

  return (
    <div>
      <FontAwesomeIcon icon={faExclamationTriangle} style={iconStyle} />
      <span style={labelStyle}>Not published yet</span>
    </div>
  );
};


const RTimeLine = ({ items, color ,size=22 }) => {
  const history=useHistory();
  return (
    <div>
      {items?.map((item, index) => (
        <div
         onClick={item.onClick?item.onClick:
         ()=>{if(item?.link) 
         //history.replace(item?.link);
         window.open(item?.link, '_blank');
         }} key={index} style={{ display: 'flex', alignItems: 'flex-start', marginBottom: '20px', position: 'relative' ,zIndex:"10"}}>
         
          <Col xs="1" style={item.image?{
             border:"white 10px solid",
             padding: '10px', 
             width:size+"px",
             height:size+"px",
             //maxWidth:size+"px",
            //  maxHeight:size+"px",  
            //  transformStyle: "preserve-3d",
             display:"flex",
             justifyContent:'center', 
             alignItems:'center', 
            

          }:item.roundIcon?
          {border:"white 10px solid",
          backgroundColor:"white",
          borderRadius: '50%', 
          marginRight: '20px', 
          padding: '10px', 
          width:33+"px",
          height:33+"px",
          maxWidth:33+"px",
          maxHeight:33+"px",
          textAlign: 'center', 
           zIndex:"10000000",
           display:"flex",
           justifyContent:'center', 
           alignItems:'center', 
           transformStyle: "preserve-3d",}:


          { border:"white 10px solid",
          backgroundColor: color, 
          borderRadius: '50%', 
          marginRight: '20px', 
          padding: '10px', 
          width:size+"px",
          height:size+"px",
          maxWidth:size+"px",
          maxHeight:size+"px",
          textAlign: 'center', 
          zIndex:"10000000",
          display:"flex",
          justifyContent:'center', 
          alignItems:'center', 
          transformStyle: "preserve-3d",
           }}>
          {index < items.length - 1 && (
            <div
              key={`line-${index}`}
              style={{
                position: 'absolute',
                transform: "translateZ(-10px)",
                top: '50%',
                left: '50%-2px',
                width: '2px',
                height: '50px',
                backgroundColor: "lightgray",
                zIndex: 0,
              }}
            ></div>

         
          )}
             {/* {item.image ??"noImage"} */}
            {item.image ? 
            <div style={{border:"10px solid white",zIndex:"10"}}> 
              <img src={item.image} alt="Icon" style={{ width: size+'px', height: size+'px', objectFit: 'cover', borderRadius: '50%' ,zIndex:"10"}} /> 
              </div>
            : 
            <i className={item.icon} style={{fontSize:"20px",margin: "0", padding: "0", color: color }} />}
 
          </Col>
          
          <Col xs="11">
            <div style={{ fontSize: '14px', fontWeight: 700, color: 'black', marginBottom: '5px' }}>
              {item.title}
            
            </div>
            <div style={{ fontSize: '12px', fontWeight: 700, color: 'gray' }}>
            {item.description}
            {!item.published&&item?.publishCallback?
            <span onClick={item.publishCallback} ><NotPublishedIcon/></span>:<></>}</div>
          </Col>

        </div>
      ))}
    </div>
  );
};

RTimeLine.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      icon: PropTypes.string,
      image: PropTypes.string,
      title: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
    })
  ).isRequired,
  color: PropTypes.string.isRequired,
};

export default RTimeLine;