import react, { useState } from "react"
import { Row,Col, UncontrolledTooltip } from "reactstrap";
import './RRLiveSessions.css'
function RRLiveSessions({time,AmPm,course,lessonTitle,leftTime,meetingId,userType,text="Join",onClickAction}){
  const max = 1000000;
  const id =
  "data" +
  Math.floor(Math.random() * max) ;
    return(
        <div style={{margin:'3px',padding:"2px",height:"100px"}}>
    {/* {meetingId} */}
        <Row className="main_Row" style={{width: "393px",height: "90px"}}>
           <Col  xs  md ="3" lg="3  " className="time_col">
              <Row className="time"> <Col>{time}  </Col></Row>
              <Row className="time"> <Col>{AmPm} </Col></Row>
           </Col>
           <Col   md ="7" lg="7" >
              <Row   > <Col style={{
                color: "#000",
              fontFamily: "Cairo",
              fontSize: "17px",
              }}>{course}  </Col></Row>
              <Row> 
                <Col >
                    <span className="Lesson_title"> </span> 
                    <span className="Lesson_title1">{lessonTitle}</span>
                   
                </Col>
            
              </Row>
              <Row> 
                <Col >
                  {leftTime?<span className="left_time">{leftTime??"one hour"}</span> :''}  
                </Col>
            
              </Row>
           </Col>
           <Col md ="2" lg="2" className="join" 
           style={{    padding: "0",color:leftTime?'rgb(70,195,126)':'rgb(143,143,143)'}}>
           <div id={id} onClick={meetingId?onClickAction:()=>{}} style={{color:meetingId?"#46C37E":"#8F8F8F",
                  fontFamily: "Cairo",
                  fontSize: "14px",
                  fontStyle: "normal",
                  fontWeight: "500",
                  lineHeight: "normal",
                  textTransform: "capitalize"}}><i className="fa fa-cog"> </i> {text}</div> 
           </Col>

           {meetingId?<></>:<UncontrolledTooltip delay={0} target={id}>
           This Meeting is not Started Yet
          </UncontrolledTooltip>}

          </Row>
         
        </div>
    )
}
export default RRLiveSessions;