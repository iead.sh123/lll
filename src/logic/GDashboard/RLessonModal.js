import RButton from "components/Global/RComs/RButton";
import RCard1 from "components/Global/RComs/RCards/RCard1";
import RModal from "components/Global/RComs/RModal";
import tr from "components/Global/RComs/RTranslator";

import React, { useEffect, useState } from "react";


const RLessonModal = ({lesson,publish,open,setOpen}) => {

return  <div>
{/* <button onClick={()=>{setOpen(true)}}></button>
<button onClick={()=>{setOpen(false)}}></button> */}
 <RModal
      isOpen={open}
      toggle={() => setOpen(!open)}
      header={<h6 className="text-capitalize">Lesson</h6>}
      footer={
      <div> 
       <RButton text={tr`publish`} 
        color="primary"
        onClick={publish}
      /> 
        <RButton text={tr`cancel`} onClick={() => {
       setOpen(false)
        }}
        color="white"
        style={{color:"blue",background:"white",border:"0px solid"}}
        
      /> 
      </div>
    }
      body={
        <div>
          Lesson : {lesson.id}
    <div onClick={()=>{history.push("/lesson/"+lesson.id)}}><>
    
  </></div></div>}
      //  footer={<CommentsAndNotesInput handleAdd={handleAdd} />}
      size="lg"
  />
  </div>

 
  };

export default RLessonModal;
