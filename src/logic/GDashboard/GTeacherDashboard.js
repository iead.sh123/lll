import React, { useEffect, useState } from "react";

import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom/cjs/react-router-dom.min";

export const DiscussionContext = React.createContext();
import { Container, Row, Col, Card, CardBody, CardTitle, CardText, InputGroup, InputGroupAddon, Input } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClock, faSearch } from "@fortawesome/free-solid-svg-icons";
import quote from "assets/img/quote.png";
import nodata from "assets/img/no-data.png";
import GCalender from "logic/calender/GCalender";

import { usersAndPermissionsReducer, initialState } from "logic/UsersAndPermissions/State/UsersAndPermissions.reducer";
import { useReducer } from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import RecentAddedUser from "logic/UsersAndPermissions/RecentActivities/RecentAddedUser";
import RRole from "view/UsersAndPermissions/RRole";
import Helper from "components/Global/RComs/Helper";
import { getRecentAddedRolesAsync } from "logic/UsersAndPermissions/State/UsersAndPermissions.action";
import { getRecentUserTypesAsync } from "logic/UsersAndPermissions/State/UsersAndPermissions.action";
import { getRecentActivitiesAsync } from "logic/UsersAndPermissions/State/UsersAndPermissions.action";
import Loader from "utils/Loader";
import DashboardStatisticsCard from "./DashboardStatisticsCard";
import EventCard from "./EventCard";
import { event } from "jquery";

import { GCollaborationAreaProviders } from "logic/Collaboration/GCollaborationAreaProviders";
import { GCollaborationAreaConsumedContents } from "logic/Collaboration/GCollaborationAreaConsumedContents";
import { GCollaborationAreaClients } from "logic/Collaboration/GCollaborationAreaClients";
import { GConsumedContents } from "logic/Collaboration/GConsumedContents";
import RColumn from "view/RComs/Containers/RColumn";
import GGreetingSearch from "./GGreetingSearch";
import GLessonLister from "logic/Courses/CourseManagement/Lessons/SectionLessonLister/GLessonLister";
import GStatisticsRow from "./GStatisticsRow";
import GMyCourses from "logic/Courses/MyCourses/GMyCourses";
import GTodayLessons from "./GTodayLessons";
import RLessonModal from "./RLessonModal";
import GStudentsSubmitedTasks from "./GStudentsSubmitedTasks";
import GTimeline from "./GTimeline";
import { Services } from "engine/services";
import GRecentLessons from "./GRecentLessons";
import RBill from "components/Global/RComs/RBill";
import RTitle from "components/Global/RComs/Collaboration/RTitle";
import RFlexBetween from "components/Global/RComs/RFlex/RFlexBetween";
import RHeader from "components/Global/RComs/RHeader";
import RTimeLine from "./RTimeLine";
import moment from "moment";
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";
import REmptyData from "components/RComponents/REmptyData";

const GTeacherDashboard = () => {
	// .ungradedQuestionSets[0].questionset_originId
	// const dd={
	//   "status": 1,
	//   "code": 202,
	//   "msg": "success",
	//   "data": {
	//       "recent_lessons": [],
	//       "all": [
	//           {
	//               "_id": "64ca17ba69bccdcf79031892",
	//               "name": "string",
	//               "creator_id": "1",
	//               "organization_id": "1",
	//               "is_master": true,
	//               "is_published": false,
	//               "color": null,
	//               "date_Synchronized": [],
	//               "updated_at": "2023-08-02T08:45:45.000000Z",
	//               "created_at": "2023-08-02T08:45:43.000000Z",
	//               "id": 296,
	//               "cohort_id": 204,
	//               "category_id": null,
	//               "term_id": 1,
	//               "curriculum_id": 1,
	//               "extra": {
	//                   "synchronous": false,
	//                   "isFree": false,
	//                   "closed": false,
	//                   "isOneDayCourse": false,
	//                   "comingSoon": false,
	//                   "language": [],
	//                   "isOnline": false,
	//                   "numberOfHours": null,
	//                   "isPrivate": false,
	//                   "start_date": null,
	//                   "end_date": null
	//               },
	//               "tags": [],
	//               "image_id": null,
	//               "icon_id": null,
	//               "nextLesson": null,
	//               "todayOrTommorrowUnpublishedLessons": [],
	//               "students": 1,
	//               "lessons": 0,
	//               "posts": 0,
	//               "curriculum_name": null,
	//               "grade_level_name": null,
	//               "main_course_name": null
	//           },
	//           {
	//               "_id": "64f444fbf2df6d5cb908e654",
	//               "name": "Week2",
	//               "creator_id": "1",
	//               "organization_id": "1",
	//               "is_master": true,
	//               "is_published": false,
	//               "color": null,
	//               "date_Synchronized": [],
	//               "updated_at": "2023-09-03T08:34:03.000000Z",
	//               "created_at": "2023-09-03T08:34:03.000000Z",
	//               "id": 312,
	//               "cohort_id": 226,
	//               "category_id": null,
	//               "term_id": 1,
	//               "curriculum_id": 43,
	//               "extra": {
	//                   "synchronous": false,
	//                   "isFree": false,
	//                   "closed": false,
	//                   "isOneDayCourse": false,
	//                   "comingSoon": false,
	//                   "language": [],
	//                   "isOnline": false,
	//                   "numberOfHours": null,
	//                   "isPrivate": false,
	//                   "start_date": null,
	//                   "end_date": null
	//               },
	//               "tags": [],
	//               "image_id": null,
	//               "icon_id": null,
	//               "nextLesson": null,
	//               "todayOrTommorrowUnpublishedLessons": [],
	//               "students": 2,
	//               "lessons": 0,
	//               "posts": 0,
	//               "curriculum_name": "art_1",
	//               "grade_level_name": "1ST Grade",
	//               "main_course_name": "Art"
	//           },
	//           {
	//               "_id": "64f446d9f2df6d5cb908e65c",
	//               "name": "Week2",
	//               "creator_id": "1",
	//               "organization_id": "1",
	//               "is_master": true,
	//               "is_published": false,
	//               "color": null,
	//               "date_Synchronized": [],
	//               "updated_at": "2023-09-03T08:42:01.000000Z",
	//               "created_at": "2023-09-03T08:42:01.000000Z",
	//               "id": 314,
	//               "cohort_id": 228,
	//               "category_id": null,
	//               "term_id": 1,
	//               "curriculum_id": 43,
	//               "extra": {
	//                   "synchronous": false,
	//                   "isFree": false,
	//                   "closed": false,
	//                   "isOneDayCourse": false,
	//                   "comingSoon": false,
	//                   "language": [],
	//                   "isOnline": false,
	//                   "numberOfHours": null,
	//                   "isPrivate": false,
	//                   "start_date": null,
	//                   "end_date": null
	//               },
	//               "tags": [],
	//               "image_id": null,
	//               "icon_id": null,
	//               "nextLesson": null,
	//               "todayOrTommorrowUnpublishedLessons": [],
	//               "students": 2,
	//               "lessons": 0,
	//               "posts": 0,
	//               "curriculum_name": "art_1",
	//               "grade_level_name": "1ST Grade",
	//               "main_course_name": "Art"
	//           },
	//           {
	//               "_id": "64f4478ef2df6d5cb908e65f",
	//               "name": "Week2sss",
	//               "creator_id": "1",
	//               "organization_id": "1",
	//               "is_master": true,
	//               "is_published": false,
	//               "color": null,
	//               "date_Synchronized": [],
	//               "updated_at": "2023-09-03T08:45:02.000000Z",
	//               "created_at": "2023-09-03T08:45:02.000000Z",
	//               "id": 315,
	//               "cohort_id": 229,
	//               "category_id": null,
	//               "term_id": 1,
	//               "curriculum_id": 43,
	//               "extra": {
	//                   "synchronous": false,
	//                   "isFree": false,
	//                   "closed": false,
	//                   "isOneDayCourse": false,
	//                   "comingSoon": false,
	//                   "language": [],
	//                   "isOnline": false,
	//                   "numberOfHours": null,
	//                   "isPrivate": false,
	//                   "start_date": null,
	//                   "end_date": null
	//               },
	//               "tags": [],
	//               "image_id": null,
	//               "icon_id": null,
	//               "nextLesson": null,
	//               "todayOrTommorrowUnpublishedLessons": [],
	//               "students": 3,
	//               "lessons": 0,
	//               "posts": 0,
	//               "curriculum_name": "art_1",
	//               "grade_level_name": "1ST Grade",
	//               "main_course_name": "Art"
	//           },
	//           {
	//               "_id": "64f5b9dcf6b4497d5b015aa5",
	//               "name": "test course",
	//               "creator_id": "1",
	//               "organization_id": "1",
	//               "is_master": true,
	//               "is_published": false,
	//               "color": null,
	//               "date_Synchronized": [],
	//               "updated_at": "2023-09-04T11:05:00.000000Z",
	//               "created_at": "2023-09-04T11:05:00.000000Z",
	//               "id": 321,
	//               "cohort_id": 236,
	//               "category_id": null,
	//               "term_id": 1,
	//               "curriculum_id": 43,
	//               "extra": {
	//                   "synchronous": false,
	//                   "isFree": false,
	//                   "closed": false,
	//                   "isOneDayCourse": false,
	//                   "comingSoon": false,
	//                   "language": [],
	//                   "isOnline": false,
	//                   "numberOfHours": null,
	//                   "isPrivate": false,
	//                   "start_date": null,
	//                   "end_date": null
	//               },
	//               "tags": [],
	//               "image_id": null,
	//               "icon_id": null,
	//               "nextLesson": null,
	//               "todayOrTommorrowUnpublishedLessons": [],
	//               "students": 4,
	//               "lessons": 0,
	//               "posts": 0,
	//               "curriculum_name": "art_1",
	//               "grade_level_name": "1ST Grade",
	//               "main_course_name": "Art"
	//           },
	//           {
	//               "_id": "64f6f9bb27671ce24501b1c2",
	//               "name": "coursen for users enrollment",
	//               "creator_id": "1",
	//               "organization_id": "1",
	//               "is_master": true,
	//               "is_published": false,
	//               "color": null,
	//               "date_Synchronized": [],
	//               "updated_at": "2023-09-05T09:49:47.000000Z",
	//               "created_at": "2023-09-05T09:49:45.000000Z",
	//               "id": 327,
	//               "cohort_id": 246,
	//               "category_id": null,
	//               "term_id": 1,
	//               "curriculum_id": 43,
	//               "extra": {
	//                   "synchronous": false,
	//                   "isFree": false,
	//                   "closed": false,
	//                   "isOneDayCourse": false,
	//                   "comingSoon": false,
	//                   "language": [],
	//                   "isOnline": false,
	//                   "numberOfHours": null,
	//                   "isPrivate": false,
	//                   "start_date": null,
	//                   "end_date": null
	//               },
	//               "tags": [],
	//               "image_id": null,
	//               "icon_id": null,
	//               "nextLesson": null,
	//               "todayOrTommorrowUnpublishedLessons": [],
	//               "students": 1,
	//               "lessons": 0,
	//               "posts": 0,
	//               "curriculum_name": "art_1",
	//               "grade_level_name": "1ST Grade",
	//               "main_course_name": "Art"
	//           },
	//           {
	//               "_id": "64c20d13d8d56689cf026139",
	//               "name": "ldlaldladlladlladlldalldal",
	//               "color": "string",
	//               "creator_id": "1",
	//               "organization_id": "1",
	//               "is_master": true,
	//               "is_published": false,
	//               "date_Synchronized": [],
	//               "updated_at": "2023-07-27T06:43:53.850000Z",
	//               "created_at": "2023-07-27T06:22:10.000000Z",
	//               "id": 275,
	//               "cohort_id": 176,
	//               "category_id": null,
	//               "extra": {
	//                   "synchronous": true,
	//                   "isFree": false,
	//                   "closed": false,
	//                   "isOneDayCourse": true,
	//                   "comingSoon": false,
	//                   "language": [],
	//                   "numberOfHours": null,
	//                   "isPrivate": false,
	//                   "start_date": "2023-07-27T06:33:05.716Z",
	//                   "end_date": "2023-07-27T06:33:05.716Z"
	//               },
	//               "tags": [],
	//               "image_id": null,
	//               "icon_id": null,
	//               "nextLesson": null,
	//               "todayOrTommorrowUnpublishedLessons": [],
	//               "students": 0,
	//               "lessons": 0,
	//               "posts": 0
	//           },
	//           {
	//               "_id": "6502c26c429bdfff7f0b58e2",
	//               "name": "course1",
	//               "creator_id": "385",
	//               "organization_id": "1",
	//               "is_master": 0,
	//               "is_published": 1,
	//               "color": null,
	//               "date_Synchronized": "2023-09-14 08:20:53",
	//               "updated_at": "2023-09-14T08:22:54.000000Z",
	//               "created_at": "2023-09-14T08:20:53.000000Z",
	//               "id": 338,
	//               "icon_id": "mvjwvlkxprzbznjvbmngeqydo",
	//               "cohort_id": 257,
	//               "category_id": 62,
	//               "level_ids": [],
	//               "extra": {
	//                   "synchronous": 0,
	//                   "isFree": 1,
	//                   "closed": 0,
	//                   "isOneDayCourse": 1,
	//                   "comingSoon": 0,
	//                   "language": [],
	//                   "isOnline": 0,
	//                   "numberOfHours": null,
	//                   "isPrivate": 0,
	//                   "start_date": null,
	//                   "end_date": null
	//               },
	//               "tags": [],
	//               "category": {
	//                   "id": 135,
	//                   "course_id": 338,
	//                   "category_id": 62,
	//                   "created_at": "2023-09-14T08:20:53.000000Z",
	//                   "updated_at": "2023-09-14T08:20:53.000000Z",
	//                   "category_name": "category 1"
	//               },
	//               "course_id": 338,
	//               "image_id": "nxopxjdmlknadgwvbewgvyqrz",
	//               "learning_object_id": null,
	//               "parent_id": null,
	//               "nextLesson": null,
	//               "todayOrTommorrowUnpublishedLessons": [],
	//               "students": 0,
	//               "lessons": 0,
	//               "posts": 0
	//           }
	//       ],
	//       "recentTaks": {
	//           "outDatedTasks": [
	//               {
	//                   "id": 3,
	//                   "randomizeQuestionOrder": false,
	//                   "randomizeQuestionPartsOrder": false,
	//                   "startDate": null,
	//                   "endDate": null,
	//                   "created_at": "2023-10-04T07:47:58.722Z",
	//                   "isDefault": true,
	//                   "updated_at": "2023-10-04T07:47:58.722Z",
	//                   "isClosed": false,
	//                   "exam": {
	//                       "id": 7,
	//                       "name": "QTEST",
	//                       "description": "<p>QTESTQTESTQTESTQTESTQTEST<\/p>",
	//                       "questionsCount": 1,
	//                       "points": 2,
	//                       "files": null,
	//                       "is_published": false,
	//                       "created_at": "2023-10-04T07:47:55.698Z",
	//                       "updated_at": "2023-10-04T07:47:55.698Z"
	//                   },
	//                   "type": "exams"
	//               },
	//               {
	//                   "id": 2,
	//                   "randomizeQuestionOrder": false,
	//                   "randomizeQuestionPartsOrder": false,
	//                   "startDate": null,
	//                   "endDate": null,
	//                   "created_at": "2023-10-04T07:45:22.296Z",
	//                   "isDefault": true,
	//                   "updated_at": "2023-10-04T07:45:22.296Z",
	//                   "isClosed": false,
	//                   "exam": {
	//                       "id": 5,
	//                       "name": "QTEST",
	//                       "description": "<p>QTESTQTESTQTESTQTESTQTEST<\/p>",
	//                       "questionsCount": 1,
	//                       "points": 2,
	//                       "files": null,
	//                       "is_published": false,
	//                       "created_at": "2023-10-04T07:45:19.937Z",
	//                       "updated_at": "2023-10-04T07:45:19.937Z"
	//                   },
	//                   "type": "exams"
	//               },
	//               {
	//                   "id": 1,
	//                   "randomizeQuestionOrder": false,
	//                   "randomizeQuestionPartsOrder": false,
	//                   "startDate": null,
	//                   "endDate": null,
	//                   "created_at": "2023-10-04T07:22:03.001Z",
	//                   "isDefault": true,
	//                   "updated_at": "2023-10-04T07:22:03.001Z",
	//                   "isClosed": false,
	//                   "exam": {
	//                       "id": 3,
	//                       "name": "QTEST",
	//                       "description": "<p>QTESTQTESTQTESTQTESTQTEST<\/p>",
	//                       "questionsCount": 1,
	//                       "points": 2,
	//                       "files": null,
	//                       "is_published": false,
	//                       "created_at": "2023-10-04T07:22:00.719Z",
	//                       "updated_at": "2023-10-04T07:22:00.719Z"
	//                   },
	//                   "type": "exams"
	//               }
	//           ],
	//           "activeTasks": [
	//               {
	//                   "id": 2,
	//                   "maximumLearnersAllowed": 200,
	//                   "passRequired": true,
	//                   "autoGrade": true,
	//                   "percentagePassingGrade": 0,
	//                   "randomizeQuestionOrders": 1,
	//                   "dueDate": null,
	//                   "cutoffDate": null,
	//                   "isDefault": true,
	//                   "created_at": "2023-10-03T08:11:05.333Z",
	//                   "updated_at": "2023-10-03T08:11:05.333Z",
	//                   "isClosed": false,
	//                   "assignment": {
	//                       "id": 5,
	//                       "name": "string",
	//                       "description": "string",
	//                       "points": 10,
	//                       "files": null,
	//                       "questionsCount": 1,
	//                       "is_published": true,
	//                       "created_at": "2023-10-03T08:11:01.032Z",
	//                       "updated_at": "2023-10-03T08:11:01.032Z"
	//                   },
	//                   "type": "assignments"
	//               },
	//               {
	//                   "id": 1,
	//                   "maximumLearnersAllowed": 200,
	//                   "passRequired": true,
	//                   "autoGrade": true,
	//                   "percentagePassingGrade": 0,
	//                   "randomizeQuestionOrders": 1,
	//                   "dueDate": null,
	//                   "cutoffDate": null,
	//                   "isDefault": true,
	//                   "created_at": "2023-10-03T08:10:54.742Z",
	//                   "updated_at": "2023-10-03T08:10:54.742Z",
	//                   "isClosed": false,
	//                   "assignment": {
	//                       "id": 3,
	//                       "name": "string",
	//                       "description": "string",
	//                       "points": 10,
	//                       "files": null,
	//                       "questionsCount": 1,
	//                       "is_published": true,
	//                       "created_at": "2023-10-03T08:10:53.803Z",
	//                       "updated_at": "2023-10-03T08:10:53.803Z"
	//                   },
	//                   "type": "assignments"
	//               },
	//               {
	//                   "id": 3,
	//                   "canContinue": false,
	//                   "maxAttemptsAllowed": 1,
	//                   "passRequired": true,
	//                   "autoGrade": true,
	//                   "isDefault": true,
	//                   "percentagePassingGrade": 0,
	//                   "randomizeQuestionOrders": 0,
	//                   "startDate": null,
	//                   "endDate": null,
	//                   "created_at": "2023-10-03T08:16:18.010Z",
	//                   "updated_at": "2023-10-03T08:16:18.010Z",
	//                   "isClosed": false,
	//                   "quiz": {
	//                       "id": 14,
	//                       "name": "string",
	//                       "description": "string",
	//                       "files": null,
	//                       "is_published": true,
	//                       "questionsCount": 5,
	//                       "points": 50,
	//                       "created_at": "2023-10-03T08:16:16.243Z",
	//                       "updated_at": "2023-10-03T08:16:16.243Z"
	//                   },
	//                   "type": "quizzes"
	//               }
	//           ],
	//           "allTasksCount": 11
	//       },
	//       "recentlyAddedContent": [],
	//       "whatWeHaveForToday": {
	//           "courses": [],
	//           "unpublishedLessons": [],
	//           "unpublishedModuleContents": [],
	//           "ungradedQuestionSets": [
	//               {
	//                   "learner_id": 385,
	//                   "learner_full_name": "NewTeacher NewTeacher",
	//                   "questionset_id": 3,
	//                   "questionset_name": "QTEST",
	//                   "questionset_description": "<p>QTESTQTESTQTESTQTESTQTEST<\/p>",
	//                   "questionset_questionsCount": 1,
	//                   "questionset_points": 2,
	//                   "questionset_files": null,
	//                   "questionset_is_published": 0,
	//                   "questionset_created_at": "2023-10-04T07:22:00.719Z",
	//                   "questionset_updated_at": "2023-10-04T07:22:00.719Z",
	//                   "questionset_curriculumId": null,
	//                   "questionset_courseId": 275,
	//                   "questionset_creatorId": null,
	//                   "questionset_originId": 2,
	//                   "submission_id": 1,
	//                   "submitted_at": "2023-10-04T07:24:06.023Z",
	//                   "courseId": null,
	//                   "questionSetId": 3,
	//                   "moduleId": 39,
	//                   "questionSetType": "exams"
	//               },
	//               {
	//                   "learner_id": 385,
	//                   "learner_full_name": "NewTeacher NewTeacher",
	//                   "questionset_id": 5,
	//                   "questionset_name": "QTEST",
	//                   "questionset_description": "<p>QTESTQTESTQTESTQTESTQTEST<\/p>",
	//                   "questionset_questionsCount": 1,
	//                   "questionset_points": 2,
	//                   "questionset_files": null,
	//                   "questionset_is_published": 0,
	//                   "questionset_created_at": "2023-10-04T07:45:19.937Z",
	//                   "questionset_updated_at": "2023-10-04T07:45:19.937Z",
	//                   "questionset_curriculumId": null,
	//                   "questionset_courseId": 275,
	//                   "questionset_creatorId": null,
	//                   "questionset_originId": 4,
	//                   "submission_id": 2,
	//                   "submitted_at": "2023-10-04T07:45:45.731Z",
	//                   "courseId": null,
	//                   "questionSetId": 5,
	//                   "moduleId": 39,
	//                   "questionSetType": "exams"
	//               },
	//               {
	//                   "learner_id": 385,
	//                   "learner_full_name": "NewTeacher NewTeacher",
	//                   "questionset_id": 7,
	//                   "questionset_name": "QTEST",
	//                   "questionset_description": "<p>QTESTQTESTQTESTQTESTQTEST<\/p>",
	//                   "questionset_questionsCount": 1,
	//                   "questionset_points": 2,
	//                   "questionset_files": null,
	//                   "questionset_is_published": 0,
	//                   "questionset_created_at": "2023-10-04T07:47:55.698Z",
	//                   "questionset_updated_at": "2023-10-04T07:47:55.698Z",
	//                   "questionset_curriculumId": null,
	//                   "questionset_courseId": 275,
	//                   "questionset_creatorId": null,
	//                   "questionset_originId": 6,
	//                   "submission_id": 3,
	//                   "submitted_at": "2023-10-04T07:48:16.877Z",
	//                   "courseId": null,
	//                   "questionSetId": 7,
	//                   "moduleId": 39,
	//                   "questionSetType": "exams"
	//               },
	//               {
	//                   "learner_id": 385,
	//                   "learner_full_name": "NewTeacher NewTeacher",
	//                   "questionset_id": 9,
	//                   "questionset_name": "QTEST",
	//                   "questionset_description": "<p>QTESTQTESTQTESTQTESTQTEST<\/p>",
	//                   "questionset_questionsCount": 1,
	//                   "questionset_points": 2,
	//                   "questionset_files": null,
	//                   "questionset_is_published": 0,
	//                   "questionset_created_at": "2023-10-04T07:50:16.306Z",
	//                   "questionset_updated_at": "2023-10-04T07:50:16.306Z",
	//                   "questionset_curriculumId": null,
	//                   "questionset_courseId": 275,
	//                   "questionset_creatorId": null,
	//                   "questionset_originId": 8,
	//                   "submission_id": 4,
	//                   "submitted_at": "2023-10-04T07:50:33.831Z",
	//                   "courseId": null,
	//                   "questionSetId": 9,
	//                   "moduleId": 39,
	//                   "questionSetType": "exams"
	//               },
	//               {
	//                   "learner_id": 385,
	//                   "learner_full_name": "NewTeacher NewTeacher",
	//                   "questionset_id": 17,
	//                   "questionset_name": "QTEST",
	//                   "questionset_description": "<p>QTESTQTESTQTESTQTESTQTEST<\/p>",
	//                   "questionset_questionsCount": 1,
	//                   "questionset_points": 100,
	//                   "questionset_files": null,
	//                   "questionset_is_published": 0,
	//                   "questionset_created_at": "2023-10-07T02:53:48.229Z",
	//                   "questionset_updated_at": "2023-10-07T02:53:48.229Z",
	//                   "questionset_curriculumId": null,
	//                   "questionset_courseId": 275,
	//                   "questionset_creatorId": null,
	//                   "questionset_originId": 16,
	//                   "submission_id": 5,
	//                   "submitted_at": "2023-10-07T02:56:10.683Z",
	//                   "courseId": null,
	//                   "questionSetId": 17,
	//                   "moduleId": 39,
	//                   "questionSetType": "exams"
	//               },
	//               {
	//                   "learner_id": 1,
	//                   "learner_full_name": "eyad",
	//                   "questionset_id": 17,
	//                   "questionset_name": "QTEST",
	//                   "questionset_description": "<p>QTESTQTESTQTESTQTESTQTEST<\/p>",
	//                   "questionset_questionsCount": 1,
	//                   "questionset_points": 100,
	//                   "questionset_files": null,
	//                   "questionset_is_published": 0,
	//                   "questionset_created_at": "2023-10-07T02:53:48.229Z",
	//                   "questionset_updated_at": "2023-10-07T02:53:48.229Z",
	//                   "questionset_curriculumId": null,
	//                   "questionset_courseId": 275,
	//                   "questionset_creatorId": null,
	//                   "questionset_originId": 16,
	//                   "submission_id": 6,
	//                   "submitted_at": "2023-10-07T05:08:12.995Z",
	//                   "courseId": 353,
	//                   "questionSetId": 17,
	//                   "moduleId": 39,
	//                   "questionSetType": "exams"
	//               }
	//           ]
	//       },
	//       "statistics": {
	//           "community": {
	//               "type": "community",
	//               "title": "Community",
	//               "value": "0 Posts"
	//           },
	//           "courses": {
	//               "type": "courses",
	//               "title": "Courses",
	//               "value": "0 New Courses"
	//           },
	//           "collaboration": {
	//               "type": "collaboration",
	//               "title": "Collaboration",
	//               "value": "0 Contents"
	//           },
	//           "terms": {
	//               "type": "terms",
	//               "title": "Terms",
	//               "value": "0 Actions"
	//           }
	//       }
	//   }
	// };

	const [data, setData] = useState([]);
	// const [recentLessons,setRecentLessons]=useState([]);
	// const [recentTasks,setRecentTasks]=useState([]);
	// const [recentlyAddedContents,setRecentlyAddedContents]=useState([]);
	// const [statistics,setStatistics]=useState([]);
	// const [todayActivities,setTodayActivities]=useState([]);

	const history = useHistory();

	useEffect(() => {
		//setData(dd.data);
		const get_it = async () => {
			await Helper.fastGet(
				Services.courses_manager.backend + "api/dashboard",
				// "http://localhost:3000/td.json"
				"fail to get api/dashboard",
				(response) => {
					Helper.cl(response, "response");
					setData(response.data?.data);
				},
				() => {}
			);
			//  await Helper.fastGet(Services.courses_manager.backend+"api/dashboard/recent-lessons","fail to get api/dashboard/recent-lessons",(response)=>{Helper.cl(response,"response");  setRecentLessons(response.data?.data)  },()=>{});
			//  await Helper.fastGet(Services.courses_manager.backend+"api/dashboard/recent-tasks","fail to get api/dashboard/recent-tasks",(response)=>{Helper.cl(response,"response");  setRecentTasks(response.data?.data)  },()=>{});
			//  await Helper.fastGet(Services.courses_manager.backend+"api/dashboard/recently-added-contents","fail to get api/dashboard/recently-added-contents",(response)=>{Helper.cl(response,"response");  setRecentlyAddedContents(response.data?.data)},()=>{});
			//  await Helper.fastGet(Services.courses_manager.backend+"api/dashboard/statistics","fail to get api/dashboard/statistics",(response)=>{Helper.cl(response,"response");  setStatistics(response.data?.data)  },()=>{});
			//  await Helper.fastGet(Services.courses_manager.backend+"api/dashboard/today","fail to get api/dashboard/today",(response)=>{Helper.cl(response,"response");  setTodayActivities(response.data?.data)  },()=>{});
		};
		get_it();
	}, []);

	const [lessonModalOpen, setLessonModalOpen] = useState(false);
	const [lesson, setLesson] = useState({ id: 20 });
	const publishLesson = () => {
		alert("publish lesson");
	};

	const [myCoursesCount, setMyCoursesCount] = useState(0);

	const [whatWeHaveForToday, setWhatWeHaveForToday] = useState([]);
	const iconStyle = {
		marginRight: "5px",
		marginLeft: "5px",
		fontSize: "10px",
		color: "gray",
	};
	useEffect(() => {
		let temp_array = [];
		data?.whatWeHaveForToday?.unpublishedLessons.map((e) => {
			const formattedTime = moment(e?.lesson_date).format("HH:mm") + "-" + moment(e?.end_date).format("HH:mm");

			temp_array.push({
				icon: "fa fa-play-circle",
				roundIcon: true,
				title: (
					<div>
						{e?.course?.name ?? "unamed course"}
						<FontAwesomeIcon icon={faClock} style={iconStyle} />
						<span>{formattedTime}</span>
					</div>
				),
				description: "Lesson:".e?.subject,
				image: null,
				date: e?.lesson_date,
				published: e?.published,
				publishCallback: () => {
					history.push(
						`${baseURL}/${genericPath}/course-management/course/${e.module?.course?.id}/cohort/${e.module?.course?.cohort_id}/modules?moduleId=${e.module?.id}`
					);
				},
				onClick: () => {
					history.push(
						`${baseURL}/${genericPath}/course-management/course/${e.module?.course?.id}/cohort/${e.module?.course?.cohort_id}/modules?moduleId=${e.module?.id}`
					);
				},
			});
		});

		// []subject,lesson_date,published

		data?.whatWeHaveForToday?.unpublishedModuleContents.map((e) => {
			temp_array.push({
				icon:
					e.type == "assignments"
						? "fa fa-question-circle"
						: e.type == "quizzes"
						? "fa fa-tasks"
						: e.type == "exams"
						? "fa fa-question-circle"
						: e.type == "polls"
						? "fa fa-poll-circle"
						: e.type == "surverys"
						? "fa fa-poll-circle"
						: // e.type=="assignment"?
						  "fa fa-pen",
				roundIcon: true,
				title: e?.module?.course?.name ?? "unamed course",
				description: e.type + ":" + e.title,
				image: null,
				date: e.lesson_date,
				published: e.is_published,
				onClick: () => {
					history.push(
						`${baseURL}/${genericPath}/course-management/course/${e.module?.course?.id}/cohort/${
							e.module?.course?.cohort_id
						}/modules?moduleId=${e.module?.id}&moduleContentId=${e.content - id}`
					);
				},
			});
		});

		data?.whatWeHaveForToday?.unpublishedLessonContents.map((e) => {
			temp_array.push({
				icon: "fa fa-pen",
				title: e?.course?.name ?? "unamed course",
				description: e.subject,
				roundIcon: true,
				color: "white",
				image: null,
				date: e.lesson_date,
				published: e.is_published,
				publishCallback: () => {
					history.push(
						`${baseURL}/${genericPath}/course-management/course/${e.module?.course?.id}/cohort/${e.module?.course?.cohort_id}/modules?moduleId=${e.module?.id}&moduleContentId=${e.module_content_id}`
					);
				},
				onClick: () => {
					history.push(
						`${baseURL}/${genericPath}/course-management/course/${e.module?.course?.id}/cohort/${e.module?.course?.cohort_id}/modules?moduleId=${e.module?.id}&moduleContentId=${e.module_content_id}`
					);
				},
			});
		});

		setWhatWeHaveForToday(temp_array);
	}, [data?.whatWeHaveForToday]);

	// In your component...
	return (
		<Container style={{ width: "80vw" }}>
			<GGreetingSearch />

			<Row className="mt-4">
				<Col xs="6" md="7" style={{ background: "#F9F9F9" }}>
					<RHeader title="Today's Lessons" count={data?.recentLessons?.length} link="" color="white" linkTitle=""></RHeader>
					<GTodayLessons lessons={data?.recentLessons} mode="teacher" />
				</Col>
				<Col xs="6" md="5">
					<RHeader title="What we have for Today?" count={data?.recentTasks?.length} link="" color="white" linkTitle=""></RHeader>
					{data?.recentTasks ? (
						// <GTimeline />

						<RTimeLine items={whatWeHaveForToday} color="#668AD7" />
					) : (
						<REmptyData />
					)}
				</Col>
			</Row>
			<GStatisticsRow data={data?.statistics} />

			<Row className="mt-4">
				{/* First column */}
				<Col xs="12" md="7">
					<div style={{ height: "100%" }}>
						<RHeader title="Student submited Tasks" />
						{data?.whatWeHaveForToday?.ungradedQuestionSets ? (
							<GStudentsSubmitedTasks tasks={data?.whatWeHaveForToday?.ungradedQuestionSets} />
						) : (
							<REmptyData />
						)}
					</div>
				</Col>

				<Col xs="12" md="5" className="d-flex align-items-center justify-content-center">
					<div>
						<GCalender advanced={false} />
					</div>
				</Col>
			</Row>

			<Row className="mt-4">
				<div style={{ display: myCoursesCount == 0 ? "hidden" : "block" }}>
					<RHeader title="My Courses" count={myCoursesCount} link="/g/my-courses" linkTitle="See All"></RHeader>

					{/* <GMyCourses advanced={false} perView={/> */}
					<GMyCourses advanced={false} perView={3} />
				</div>
			</Row>
		</Container>
	);
	// const dispatch = useDispatch();
	const [usersAndPermissionsData, dispatch] = useReducer(usersAndPermissionsReducer, initialState);

	useEffect(() => {
		getRecentAddedRolesAsync(dispatch);
		getRecentUserTypesAsync(dispatch);
		getRecentActivitiesAsync(dispatch);
	}, []);

	const { user } = useSelector((state) => state.auth.user);
	const userData = {
		username: "John Doe",
	};

	const quotes = [
		{ text: "Don't count the days, make the days count.", image: "quote1.png" },
		// Add more quotes as needed
	];

	const cards = [
		{ title: "Card 1", description: "5 new posts", icon: "icon1.png" },
		{ title: "Card 2", description: "10 new messages", icon: "icon2.png" },
		{ title: "Card 3", description: "3 new notifications", icon: "icon3.png" },
		{ title: "Card 4", description: "7 new tasks", icon: "icon4.png" },
	];

	const events = [
		{
			icon: "fas fa-pen",
			color: "green",
			date: "2 days ago",
			description: "2 New Home Rooms Created",
			images: ["quote", "nodata", "quote", "nodata"],
			link: "http://google.com",
		},
		{
			icon: "fas fa-camera",
			color: "blue",
			date: "5 days ago",
			description: "New Photos Uploaded",
			images: ["quote", "nodata"],
			link: "http://example.com",
		},
		{
			icon: "fas fa-bell",
			color: "orange",
			date: "10 days ago",
			description: "10 New Notifications",
			images: ["5.png", "6.png"],
			link: "http://example.com",
		},
		{
			icon: "fas fa-tasks",
			color: "purple",
			date: "1 day ago",
			description: "Task Completed",
			images: ["7.png", "8.png"],
			link: "http://example.com",
		},
	];

	useEffect(() => {
		if (!reportedContentNoData) {
			SetreportedContentsMd(8);
			if (!mostConsumedCollaborationNoData) {
				setMostConsumedCollaborationMd(4);
				setQuoteMd(0);
			} else {
				setMostConsumedCollaborationMd(0);
				setQuoteMd(4);
			}
		} else {
			SetreportedContentsMd(0);
			if (!mostConsumedCollaborationNoData) {
				setMostConsumedCollaborationMd(4);
				setQuoteMd(8);
			} else {
				setMostConsumedCollaborationMd(0);
				setQuoteMd(12);
			}
		}
		// ,mostConsumedCollaborationNoData

		//     setQuoteMd

		//     setMostConsumedCollaborationMd
	}, [reportedContentNoData, mostConsumedCollaborationNoData]);

	const [reportedContentsMd, SetreportedContentsMd] = useState(4);
	const [quoteMd, setQuoteMd] = useState(4);
	const [mostConsumedCollaborationMd, setMostConsumedCollaborationMd] = useState(4);

	const [reportedContentNoData, setreportedContentNoData] = useState(true);
	const [mostConsumedCollaborationNoData, setmostConsumedCollaborationNoData] = useState(false);

	const [colAreaClientsNoData, setcolAreaClientsNoData] = useState(true);
	const [colAreaProvidersNoData, setcolAreaProvidersNoData] = useState(true);
	const [colAreaConsumedContentsNoData, setcolAreaConsumedContentsNoData] = useState(true);

	const reportedContents = () => <>ReportedContents</>;

	const quote = () => (
		<Col xs="12" md="12" style={{ background: "#F9F9F9" }}>
			<div
				style={{
					display: "flex",
					flexDirection: "column",
					justifyContent: "center",
					fontSize: "12px",
					fontWeight: 400,
					alignItems: "flex-start",
					height: "100px",
				}}
			>
				<div style={{ paddingRight: "10px", marginRight: "10px" }}>"Your quote here"</div>
				<div style={{ fontWeight: 700 }}>- Thomas A. Edison</div>
			</div>
			<img src={quote} alt="Quote" style={{ position: "absolute", right: "0px", top: "-50%", height: "75px", width: "auto" }} />
		</Col>
	);

	const mostConsumedCollaboration = () => (
		<GConsumedContents
			setNoData={(x) => {
				setmostConsumedCollaborationNoData(x);
			}}
		/>
	);
	return (
		<Container>
			<Row className="mt-4">
				<Col xs="6" md="7" style={{ background: "#F9F9F9" }}>
					<div>
						👋 Hello, <span>{userData.username}</span>. <br /> It's so nice to have you back.
					</div>
				</Col>
				<Col xs="6" md="5">
					<InputGroup>
						<Input placeholder="Search..." />
						<InputGroupAddon addonType="prepend">
							<span className="input-group-text">
								<FontAwesomeIcon icon={faSearch} />
							</span>
						</InputGroupAddon>
					</InputGroup>
				</Col>
			</Row>
			<Row className="" style={{ marginTop: "50px" }}>
				{reportedContentsMd ? (
					<Col xs="12" md={reportedContentsMd}>
						{reportedContents()}
					</Col>
				) : (
					<></>
				)}
				{quoteMd ? (
					<Col xs="12" md={quoteMd}>
						{quote()}
					</Col>
				) : (
					<></>
				)}
				{mostConsumedCollaborationMd ? (
					<Col xs="12" md={mostConsumedCollaborationMd}>
						{mostConsumedCollaboration()}
					</Col>
				) : (
					<></>
				)}
			</Row>

			<Row className="mt-4">
				<Col xs="12" md="12" style={{ display: "flex", flexDirection: "row" }}>
					<DashboardStatisticsCard title="New Posts" description="5 posts added" color="blue" icon="fas fa-pen" />
					<DashboardStatisticsCard title="Messages" description="12 new messages" color="green" icon="fas fa-pen" />
					<DashboardStatisticsCard title="Notifications" description="3 new notifications" color="orange" icon="fas fa-pen" />
					<DashboardStatisticsCard title="Tasks" description="7 pending tasks" color="purple" icon="fas fa-pen" />
				</Col>
			</Row>
			<Row className="mt-4">
				{cards.map((card, index) => (
					<Col key={index} xs="12" md="3">
						<Card>
							<CardBody>
								<CardTitle className="font-weight-bold text-gray font-size-12">{card.title}</CardTitle>
								<CardText className="font-size-14 font-weight-bold">{card.description}</CardText>
								<FontAwesomeIcon icon={card.icon} />
							</CardBody>
						</Card>
					</Col>
				))}
			</Row>

			<Row className="mt-4">
				{/* First column */}
				<Col xs="12" md="7">
					{/* Table placeholder (75% height) */}
					<div style={{ height: "50%" }}>
						<RFlex>
							<h6>{tr`Recently added users`}</h6>
						</RFlex>
						<RecentAddedUser />
					</div>
					{/* Swiper of cards placeholder (25% height) */}
					<div style={{ height: "50%" }}>
						<Col xs={12} md={6} style={{ marginBottom: "55px" }}>
							<h6 className="mb-3">{tr`Recent Added Roles`}</h6>
							<RFlex styleProps={{ flexWrap: "wrap" }}>
								{usersAndPermissionsData.recentAddedRolesLoading ? (
									<Loader />
								) : (
									usersAndPermissionsData.recentAddedRoles.map((role) => <RRole role={role} lister={true} />)
								)}
							</RFlex>
						</Col>
					</div>
				</Col>

				{/* Second column */}
				<Col xs="12" md="5" className="d-flex align-items-center justify-content-center">
					<div>
						<GCalender advanced={false} />
					</div>
				</Col>
			</Row>

			<Row className="mt-4">
				<Col xs={12} md={6}>
					{!(colAreaProvidersNoData && colAreaClientsNoData && colAreaConsumedContentsNoData) ? (
						<RColumn>
							{!colAreaProvidersNoData ? (
								<>
									<RTitle text="Providers" />
									<GCollaborationAreaProviders
										setNoData={(x) => {
											setcolAreaClientsNoData(x);
										}}
									/>
								</>
							) : (
								<></>
							)}

							{!colAreaClientsNoData ? (
								<>
									{" "}
									<RTitle text="Clients" />
									<GCollaborationAreaClients
										setNoData={(x) => {
											setcolAreaProvidersNoData(x);
										}}
									/>
								</>
							) : (
								<></>
							)}

							{!colAreaConsumedContentsNoData ? (
								<>
									{" "}
									<RTitle text="consumed contents" />
									<GCollaborationAreaConsumedContents
										setNoData={(x) => {
											setcolAreaConsumedContentsNoData(x);
										}}
									/>
								</>
							) : (
								<></>
							)}
						</RColumn>
					) : (
						<></>
					)}
				</Col>

				<Col xs={12} md={6}>
					<div
						style={{
							display: "flex",
							borderStartStartRadius: "150px",
							left: "20vw",
							background: "#F9F9F9",
							padding: "33px",
							paddingLeft: "100px",
						}}
					>
						{events.map((e) => (
							<EventCard {...e} />
						))}
					</div>
				</Col>
			</Row>
		</Container>
	);
};

export default GTeacherDashboard;
