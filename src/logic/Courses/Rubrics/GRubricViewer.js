import React from "react";
import useIsMasterCourse from "hocs/useIsMasterCourse";
import RRubricHeader from "view/Courses/CourseManagement/Rubric/RRubricHeader";
import RRubricViewer from "view/Courses/CourseManagement/Rubric/Viewer/RRubricViewer";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { useParams, useHistory } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useMutateData } from "hocs/useMutateData";
import { rubricApi } from "api/global/rubric";

const GRubricViewer = () => {
	const { courseId, rubricId } = useParams();
	const history = useHistory();
	const isMasterCourse = useIsMasterCourse();

	const { data, isLoading, isFetching, isError } = useFetchDataRQ({
		queryKey: ["rubricByIdViewer", rubricId],
		queryFn: () => rubricApi.getRubricById(rubricId),
	});

	const exportRubricAsPdfMutate = useMutateData({
		queryFn: () => rubricApi.exportRubricAsPdf(rubricId),
		downloadFile: true,
	});

	const handlePushToRubricEditor = () => {
		history.push(
			`${baseURL}/${genericPath}/${
				isMasterCourse
					? `course-catalog/master-course/${courseId}/rubric/${data?.data?.data?.id}`
					: `course-management/course/${courseId}/rubric/${data?.data?.data?.id}`
			}`
		);
	};

	const handleExportRubricAsPdf = (rubricId) => {
		exportRubricAsPdfMutate.mutate(rubricId);
	};

	if (isLoading || isFetching) return <Loader />;
	return (
		<RFlex styleProps={{ flexDirection: "column" }}>
			<RRubricHeader
				handlers={{ handlePushToRubricEditor, handleExportRubricAsPdf }}
				loading={{ exportRubricAsPdfLoading: exportRubricAsPdfMutate.isLoading }}
			/>
			<RRubricViewer rubricData={data?.data?.data} />
		</RFlex>
	);
};

export default GRubricViewer;
