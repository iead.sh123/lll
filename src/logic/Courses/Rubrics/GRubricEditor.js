import React, { useState } from "react";
import { useParams, useHistory } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useMutateData } from "hocs/useMutateData";
import { Form, Formik } from "formik";
import { rubricApi } from "api/global/rubric";
import RControlledAlertDialog from "components/RComponents/RContolledAlertDialog";
import RControlledDialog from "components/RComponents/RControlledDialog";
import useIsMasterCourse from "hocs/useIsMasterCourse";
import RRubricEditor from "view/Courses/CourseManagement/Rubric/RRubricEditor";
import GStandard from "./GStandard";
import RButton from "components/Global/RComs/RButton";
import Loader from "utils/Loader";
import tr from "components/Global/RComs/RTranslator";
import * as yup from "yup";

export const RubricContext = React.createContext();

const GRubricEditor = () => {
	const { courseId, rubricId } = useParams();
	const isMasterCourse = useIsMasterCourse();
	const history = useHistory();

	const [openedCollapses, setOpenedCollapses] = useState([]);
	const [standardModal, setStandardModal] = useState(false);
	const [categoryData, setCategoryData] = useState({ categoryIndex: null, standardIndex: null });
	const [isHovered, setIsHovered] = useState(null);
	const [isStandardHovered, setIsStandardHovered] = useState(null);
	const [selectedCategories, setSelectedCategories] = useState([]);
	const [isAlertDialogOpen, setIsAlertDialogOpen] = useState(false);

	const isAdd = rubricId == "add";

	const { data, isLoading, isFetching, isError } = useFetchDataRQ({
		queryKey: ["rubricById", rubricId],
		queryFn: () => rubricApi.getRubricById(rubricId),
		enableCondition: rubricId != null,
	});

	const rubricMutation = useMutateData({
		queryFn: (date) => rubricApi.add(date),
		invalidateKeys: ["rubric"],
		displaySuccess: true,
		onSuccessFn: ({ data, variables }) => {
			history.push(
				`${baseURL}/${genericPath}/${
					isMasterCourse ? `course-catalog/master-course/${courseId}/rubrics` : `course-management/course/${courseId}/rubrics`
				}`
			);
		},
	});

	const handleOpenStandardModal = ({ categoryIndex, standardIndex, arrayHelpers }) => {
		setStandardModal(true);
		setCategoryData({ categoryIndex: categoryIndex, standardIndex: standardIndex });
	};

	const handleCloseStandardModal = () => {
		setStandardModal(false);
		setCategoryData({ categoryIndex: null, standardIndex: null });
	};

	const collapsesCategoryToggle = (categoryId) => {
		const isOpen = openedCollapses.includes(categoryId);
		if (isOpen) {
			setOpenedCollapses(openedCollapses.filter((id) => id !== categoryId));
		} else {
			setOpenedCollapses([...openedCollapses, categoryId]);
		}
	};

	const actionsOnHover = (id) => {
		const isOpen = "hover" + id;
		if (isOpen == isHovered) {
			setIsHovered("hover");
		} else {
			setIsHovered("hover" + id);
		}
	};

	const actionsStandardOnHover = (id) => {
		const isOpen = "standardHover" + id;
		if (isOpen == isStandardHovered) {
			setIsStandardHovered("standardHover");
		} else {
			setIsStandardHovered("standardHover" + id);
		}
	};

	const handleSelectedCategories = (categoryId) => {
		setSelectedCategories((items) => [...items, categoryId]);
	};

	const handleRemoveCategoryFromSelectedCategory = (categoryId) => {
		setSelectedCategories(selectedCategories.filter((id) => id !== categoryId));
	};

	const handleChangePointStatus = () => {
		setIsAlertDialogOpen(true);
	};

	const handleChangePointedValue = (value, setFieldValue, values) => {
		changePointedValue(value, setFieldValue, values);
	};

	const changePointedValue = (value, setFieldValue, values) => {
		setFieldValue("pointed", value);

		if (value == false) {
			if (Array.isArray(values.categories)) {
				values.categories.forEach((category) => {
					if (Array.isArray(category.standards)) {
						category.standards.forEach((standard) => {
							delete standard.max_points;
							if (Array.isArray(standard.ratings)) {
								standard.ratings.forEach((rating) => {
									delete rating.from_score;
									delete rating.to_score;
								});
							}
						});
					}
				});
			}
		}
	};

	const handleRemoveStandard = (setFieldValue, values) => {
		const standards = values.categories[categoryData.categoryIndex].standards;
		if (standards.length > 0) {
			const newStandards = standards.slice(0, -1);
			setFieldValue(`categories[${categoryData.categoryIndex}].standards`, newStandards);
		}
	};
	const initialValues = {
		id: !isAdd ? rubricId : undefined,
		context_id: courseId,
		context_type: isMasterCourse ? "masterCourse" : "course",
		id: (data && data.data && data.data.data && data.data.data?.id) || null,
		title: (data && data.data && data.data.data && data.data.data?.title) || "",
		description: (data && data.data && data.data.data && data.data.data?.description) || "",
		is_published: (data && data.data && data.data.data && data.data.data?.is_published) || false,
		pointed: (data && data.data && data.data.data && data.data.data?.pointed) || true,
		total_points: (data && data.data && data.data.data && data.data.data?.total_points) || null,
		tags: (data && data.data && data.data.data && data.data.data?.tags) || [],
		categories: (data && data.data && data.data.data && data.data.data?.categories) || [],
		openDescriptionCollapse: (data && data.data && data.data.data && data.data.data?.description?.length > 0 ? true : false) || false,
		standardContainsTheWrongFields: true,
		standardChildForm: {},
	};

	const validationSchema = yup.object().shape({
		title: yup.string().required("Title is required"),
		categories: yup
			.array()
			.of(
				yup.object().shape({
					subject: yup
						.string()
						.required("subject is required")
						.test("unique", "subject must be unique", function (value) {
							const { options } = this;
							const categories = options.context.categories;
							const subjects = categories.map((cat) => cat.subject);
							const count = subjects.filter((sub) => sub === value).length;
							return count === 1;
						}),
					standards: yup
						.array()
						.of(
							yup.object().shape({
								subject: yup
									.string()
									.required("subject is required")
									.test("unique", "subject must be unique", function (value, context) {
										const { options } = this;
										const categories = options.context.categories;
										const subjects = categories.flatMap((cat) => cat.standards.map((std) => std.subject));
										const count = subjects.filter((sub) => sub === value).length;
										return count === 1;
									}),
							})
						)
						.min(1, "At least one standard is required"),
				})
			)
			.min(1, "At least one category is required"),
	});

	const submitHandler = (values) => {
		rubricMutation.mutate({ data: values });
	};

	if ((isLoading && rubricId != null) || isFetching) return <Loader />;
	return (
		<Formik
			validateOnChange={true}
			enableReinitialize={true}
			initialValues={initialValues}
			validationSchema={validationSchema}
			onSubmit={submitHandler}
		>
			{({ values, touched, errors, handleBlur, isValid, handleChange, handleSubmit, setFieldValue }) => {
				return (
					<RubricContext.Provider
						value={{
							formProperties: { values, touched, errors, handleBlur, handleChange, handleSubmit, setFieldValue },
							values,
							touched,
							errors,

							isHovered,
							isStandardHovered,
							selectedCategories,
							standardModal,

							handleChange,
							handleBlur,
							handleOpenStandardModal,
							handleSelectedCategories,
							handleRemoveCategoryFromSelectedCategory,
							handleChangePointedValue,
							handleChangePointStatus,

							setFieldValue,
							actionsOnHover,
							actionsStandardOnHover,

							collapsesCategoryToggle,
							openedCollapses,
						}}
					>
						<Form onSubmit={handleSubmit}>
							<RControlledDialog
								isOpen={standardModal}
								closeDialog={handleCloseStandardModal}
								dialogBody={
									<GStandard
										pointedValue={values.pointed}
										categoryData={categoryData}
										formProperties={{ values, setFieldValue }}
										setStandardModal={setStandardModal}
										setCategoryData={setCategoryData}
									/>
								}
								contentClassName="max-w-[450px]"
							/>

							<RControlledAlertDialog
								title={
									values.pointed
										? tr`are you sure to remove points from this rubric?`
										: tr`you need to insert all the points for this rubric to proceed`
								}
								description={values.pointed ? tr`this wil effect grading and assignments` : tr`are you sure to make this rubric pointed?`}
								isOpen={isAlertDialogOpen}
								headerItemsPosition="items-start"
								handleCloseAlert={() => setIsAlertDialogOpen(false)}
								confirmAction={() => handleChangePointedValue(!values.pointed, setFieldValue, values)}
							/>

							<RRubricEditor
								formProperties={{ values, touched, errors, handleBlur, handleChange, handleSubmit, setFieldValue }}
								handlers={{
									handleOpenStandardModal,
									handleSelectedCategories,
									handleRemoveCategoryFromSelectedCategory,
									handleChangePointedValue,
									handleChangePointStatus,
									collapsesCategoryToggle,
									actionsStandardOnHover,
									actionsOnHover,
								}}
								data={{ openedCollapses, isHovered, isStandardHovered, selectedCategories, standardModal }}
							/>

							<RButton
								type="submit"
								text={tr`save`}
								color="primary"
								loading={rubricMutation.isLoading}
								disabled={rubricMutation.isLoading}
							/>
						</Form>
					</RubricContext.Provider>
				);
			}}
		</Formik>
	);
};

export default GRubricEditor;
