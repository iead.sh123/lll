import React from "react";
import RButton from "components/Global/RComs/RButton";
import RStandard_1 from "view/Courses/CourseManagement/Rubric/RStandard_1";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { Form, Formik } from "formik";
import * as yup from "yup";

const GStandard = ({ pointedValue, categoryData, formProperties, setStandardModal, setCategoryData }) => {
	//	- - - - - - - - - - Formik - - - - - - - - - -
	const initialValues = {
		pointed: pointedValue,
		id: formProperties?.values?.standardChildForm?.id || undefined,
		fakeId: formProperties?.values?.standardChildForm?.id ? undefined : formProperties?.values?.standardChildForm?.fakeId || Math.random(),
		subject: formProperties?.values?.standardChildForm?.subject || "",
		max_points: formProperties?.values?.standardChildForm?.max_points || null,
		description: formProperties?.values?.standardChildForm?.description || "",
		is_range: formProperties?.values?.standardChildForm?.is_range || false,
		ratings: formProperties?.values?.standardChildForm?.ratings || [
			{ fakeId: Math.random(), description: "", title: "", from_score: null, to_score: null },
		],
	};

	const validationPointedSchema = yup.object().shape({
		subject: yup.string().required(`subject is required`),

		max_points: yup.number().when("$pointed", {
			is: (pointed) => pointed,
			then: () => yup.number().positive("Must Be Greater Than 0").required(`points is required`),
			otherwise: () => yup.number().nullable(),
		}),
		ratings: yup.array().of(
			yup.object().shape({
				title: yup
					.string()
					.required("title is required")
					.test("unique", "title must be unique", function (value, context) {
						const { options } = this;
						const ratings = options.context.ratings;
						const titles = ratings.map((cat) => cat.title);
						const count = titles.filter((t) => t === value).length;
						return count === 1;
					}),

				from_score: yup
					.number()
					.required("from score is required")
					.test("from_score", "from score must be less than or equal to max points", function (value) {
						const { options } = this;
						return value === null || value <= options.context.max_points;
					})
					.test("from_score", "from score must be less than to score", function (value) {
						const { options } = this;
						return this.parent.to_score === null ? true : value === null || value < this.parent.to_score;
					}),

				to_score: yup
					.number()
					.nullable()
					.test("is_range", "to score is required", function (value) {
						const { is_range } = this.options.context;
						return is_range ? value !== null : true;
					})
					.test("to_score", "to score must be less than or equal to max points", function (value) {
						const { options } = this;
						return value === null || value <= options.context.max_points;
					})
					.test("to_score", "to score must be greater than from score", function (value) {
						const { options } = this;
						return !options.context.is_range ? true : value === null || value > this.parent.from_score;
					}),
			})
		),
	});

	const validationSchema = yup.object().shape({
		subject: yup.string().required(`subject is required`),
		ratings: yup.array().of(
			yup.object().shape({
				title: yup
					.string()
					.required("title is required")
					.test("unique", "title must be unique", function (value, context) {
						const { options } = this;
						const ratings = options.context.ratings;
						const titles = ratings.map((cat) => cat.title);
						const count = titles.filter((t) => t === value).length;
						return count === 1;
					}),
			})
		),
	});

	const handleFormSubmit = (values, resetForm) => {
		const { categories } = formProperties.values;
		const { categoryIndex } = categoryData;

		const indexToUpdate = categories[categoryIndex]?.standards.findIndex((standard) =>
			standard.id ? standard.id === values.id : standard.fakeId === values.fakeId
		);

		if (indexToUpdate === -1) {
			formProperties.setFieldValue(`categories[${categoryIndex}].standards`, [...categories[categoryIndex].standards, values]);
		} else {
			const updatedStandards = [...categories[categoryIndex].standards];
			updatedStandards[indexToUpdate] = values;
			formProperties.setFieldValue(`categories[${categoryIndex}].standards`, updatedStandards);
		}
		setStandardModal(false);
		setCategoryData({ categoryIndex: null, standardIndex: null });
		formProperties.setFieldValue("standardChildForm", {});
	};

	return (
		<Formik
			initialValues={initialValues}
			validationSchema={pointedValue ? validationPointedSchema : validationSchema}
			onSubmit={handleFormSubmit}
		>
			{({ values, touched, errors, handleBlur, isValid, handleChange, handleSubmit, setFieldValue, resetForm }) => {
				return (
					<Form onSubmit={handleSubmit}>
						<RStandard_1
							formProperties={{
								values,
								touched,
								errors,
								handleBlur,
								handleChange,
								setFieldValue,
								isEdit: formProperties?.values?.standardChildForm?.id || formProperties?.values?.standardChildForm?.fakeId ? true : false,
							}}
						/>

						<RFlex className="pt-1">
							<RButton
								type="submit"
								text={
									formProperties?.values?.standardChildForm?.id || formProperties?.values?.standardChildForm?.fakeId ? tr`save` : tr`create`
								}
								color="primary"
							/>
							<RButton
								text={tr`cancel`}
								color="link"
								onClick={() => {
									setStandardModal(false);
									setCategoryData({ categoryIndex: null, standardIndex: null });
									formProperties.setFieldValue("standardChildForm", {});
								}}
							/>
						</RFlex>
					</Form>
				);
			}}
		</Formik>
	);
};

export default GStandard;
