import React from "react";
import { useHistory, useParams } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useMutateData } from "hocs/useMutateData";
import { useFormik } from "formik";
import { rubricApi } from "api/global/rubric";
import { Button } from "ShadCnComponents/ui/button";
import useIsMasterCourse from "hocs/useIsMasterCourse";
import DateUtcWithTz from "utils/dateUtcWithTz";
import RSearchInput from "components/RComponents/RSearchInput";
import RDropdown from "components/RComponents/RDropdown/RDropdown";
import iconsFa6 from "variables/iconsFa6";
import RAddTags from "components/RComponents/RAddTags";
import RLister from "components/Global/RComs/RLister";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const GRubricLister = () => {
	const { courseId } = useParams();
	const history = useHistory();
	const isMasterCourse = useIsMasterCourse();

	const handlePushToRubricEditor = (rId) => {
		if (rId) {
			history.push(
				`${baseURL}/${genericPath}/${
					isMasterCourse ? `course-catalog/master-course/${courseId}/rubric/${rId}` : `course-management/course/${courseId}/rubric/${rId}`
				}`
			);
		} else {
			history.push(
				`${baseURL}/${genericPath}/${
					isMasterCourse ? `course-catalog/master-course/${courseId}/rubric/add` : `course-management/course/${courseId}/rubric/add`
				}`
			);
		}
	};

	const handlePushToRubricViewer = (rId) => {
		history.push(
			`${baseURL}/${genericPath}/${
				isMasterCourse
					? `course-catalog/master-course/${courseId}/rubric/${rId}/view`
					: `course-management/course/${courseId}/rubric/${rId}/view`
			}`
		);
	};

	// - - - - - - - - - - - - - - Formik - - - - - - - - - - - - - -
	const initialValues = {
		rubrics: [],
		groupingOfSearchFields: {
			context_id: courseId,
			context_type: isMasterCourse ? "masterCourse" : "course",
			page: 1,
			published: undefined,
			search: undefined,
		},
		searchText: "",
	};

	const { values, setFieldValue } = useFormik({
		initialValues: initialValues,
	});

	// - - - - - - - - - - Handlers - - - - - - - - - -

	const handleFilterOnStatus = (status) => {
		setFieldValue("groupingOfSearchFields.published", status);
	};

	const handleChangePage = (page) => {
		setFieldValue("page", page);
	};

	const handleChangeSearch = (text) => {
		setFieldValue("searchText", text);
	};

	const handleSearch = (emptySearch) => {
		setFieldValue("groupingOfSearchFields.search", emptySearch !== undefined ? undefined : values.searchText);
	};

	const handelDeleteRubric = (rubricId) => {
		deleteRubricMutate.mutate(rubricId);
	};

	const handleChangeStatus = (data) => {
		changeRubricStatusMutate.mutate(data);
	};

	// - - - - - - - - - - - - - Queries - - - - - - - - - - - - - -
	const rubricsData = useFetchDataRQ({
		queryKey: ["rubrics", values.groupingOfSearchFields],
		queryFn: () => rubricApi.getRubrics(values.groupingOfSearchFields),
		onSuccessFn: ({ data }) => {
			setFieldValue("rubrics", data?.data?.data);
		},
	});

	const deleteRubricMutate = useMutateData({
		queryFn: (rubricId) => rubricApi.removeRubric(rubricId),
		onSuccessFn: ({ data, variables }) => {
			const updateData = values?.rubrics.filter((item) => item.id !== variables);
			setFieldValue("rubrics", updateData);
		},
	});

	const changeRubricStatusMutate = useMutateData({
		queryFn: (data) => rubricApi.changeRubricStatus(data.rubricId),
		onSuccessFn: ({ data, variables }) => {
			const ind = values.rubrics.findIndex((rubric) => rubric.id == variables.rubricId);
			if (ind !== -1) {
				setFieldValue(`rubrics[${ind}].is_published`, !variables.status);
			}
		},
	});

	console.log("rubricsData1111", rubricsData.isLoading);

	const _recordRubrics = {
		data: values.rubrics,

		columns: [
			{
				accessorKey: "name",
				renderHeader: () => <span>{tr`name`}</span>,
				renderCell: ({ row }) => (
					<RFlex>
						<span className="cursor-pointer hover:text-themePrimary" onClick={() => handlePushToRubricEditor(row.original?.id)}>
							{row.original?.title}
						</span>
						{row.original?.tags?.length > 0 && <RAddTags dataFromBackend={row.original?.tags} disabled={true} />}
					</RFlex>
				),
			},
			{
				accessorKey: "categories",
				renderHeader: () => <span>{tr`categories`}</span>,
				renderCell: ({ row }) => <span>{`${row.original?.categories.length} categories`}</span>,
			},
			{
				accessorKey: "creation_date",
				renderHeader: () => <span>{tr`creation_date`}</span>,
				renderCell: ({ row }) =>
					DateUtcWithTz({
						dateTime: row.original?.created_at,
						dateFormate: "L",
					}),
			},
			{
				accessorKey: "status",
				renderHeader: (info) => (
					<RFlex className="items-center">
						<span>{tr`status`}</span>
						<RDropdown
							actions={[
								{ name: "all", onClick: () => handleFilterOnStatus(undefined) },
								{ name: "publish", onClick: () => handleFilterOnStatus("1") },
								{ name: "draft", onClick: () => handleFilterOnStatus("0") },
							]}
							TriggerComponent={
								<button>
									<i className={iconsFa6.filter + " text-xs"} aria-hidden="true" />
								</button>
							}
						/>
					</RFlex>
				),
				renderCell: ({ row }) => (
					<React.Fragment>
						{changeRubricStatusMutate.isLoading && changeRubricStatusMutate?.variables.rubricId == row.original.id ? (
							<i className={iconsFa6.spinner} alt="spinner" />
						) : (
							<span
								className={`${row.original?.is_published ? "text-themeSuccess" : "text-themeDanger"} cursor-pointer`}
								onClick={() => handleChangeStatus({ rubricId: row.original?.id, status: row.original?.is_published })}
							>
								{row.original?.is_published ? tr`published` : tr`draft`}
							</span>
						)}
					</React.Fragment>
				),
			},
		],

		actions: [
			{
				justIcon: true,
				icon: iconsFa6.pdf,
				actionIconClass: "text-themePrimary",
				onClick: ({ row }) => handlePushToRubricViewer(row.original.id),
			},
			{
				icon: iconsFa6.delete,
				actionIconClass: "text-themeDanger",
				confirmAction: ({ row }) => handelDeleteRubric(row.original.id),
				inDialog: true,
				dialogTitle: ({ row }) => `${tr("Are you Sure ? This can't be undone!")}`,
				headerItemsPosition: "items-start",
			},
		],
	};

	return (
		<RFlex className="flex-col">
			<RFlex className="items-center">
				<Button
					onClick={() => {
						handlePushToRubricEditor();
					}}
				>{tr`new_rubric`}</Button>
				<RSearchInput
					searchLoading={rubricsData.isLoading}
					searchData={values.searchText}
					handleSearch={handleSearch}
					setSearchData={handleChangeSearch}
					handleChangeSearch={handleChangeSearch}
				/>
			</RFlex>
			{rubricsData.isLoading ? (
				<Loader />
			) : (
				<RLister
					Records={_recordRubrics}
					convertRecordsShape={false}
					info={rubricsData}
					withPagination={true}
					handleChangePage={(event) => handleChangePage(event)}
					page={values.groupingOfSearchFields.page}
					line1={tr`no_rubric_yet`}
				/>
			)}
		</RFlex>
	);
};

export default GRubricLister;
