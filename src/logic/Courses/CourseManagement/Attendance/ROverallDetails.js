import React, { useContext } from 'react'
import { AttendanceContext } from './GAttendanceDetailsLister'
import RLister from 'components/Global/RComs/RLister'
import RProfileName from 'components/Global/RComs/RProfileName/RProfileName'
import styles from './attendance.module.scss'
import { baseURL, genericPath } from 'engine/config'
import RFlex from 'components/Global/RComs/RFlex/RFlex'
import RLoader from 'components/Global/RComs/RLoader'
const ROverallDetails = () => {
    const attendanceContext = useContext(AttendanceContext)
    console.log("ROverallDetails is rerendered")
    if (attendanceContext.isLoadingOverallDetails)
        return <RLoader />
    const _overallAttendance = attendanceContext?.overallDetails?.students?.map((student, key) => {
        const allModulesCount = attendanceContext?.overallDetails?.all_module_contents
        const studentAttendanceRatio = parseInt((student.attended_module_contents * 100) / allModulesCount);
        return {
            id: student.id,
            table_name: "GAttendanceDetailsLister",
            details: [
                {
                    key: "Students",
                    value: <RProfileName name={student.name} img={student.image}
                        onClick={() => attendanceContext.history.push(`${baseURL}/${genericPath}/course-management/course/${attendanceContext.courseId}/cohort/${attendanceContext.cohortId}/attendance/student/${student.id}`)}
                        flexStyleProps={{ cursor: "pointer" }}
                    />,

                    type: "component"
                },
                { key: "Overall", keyStyle: { color: "#668AD7" }, value: <RFlex className="justify-content-end" styleProps={{ width: "50px" }}>{studentAttendanceRatio + "%"}</RFlex>, color: "#46C37E", type: 'component' },
                { key: "Presented Conent out of " + allModulesCount, value: <RFlex className="justify-content-end" styleProps={{ width: "130px" }}>{student.attended_module_contents}</RFlex>, color: "#585858",type:'component' },
            ],
            actions: [{ name: "Attendance Details", justIcon: true, icon: "fa-solid fa-message", onClick: () => { } }],
        };
    });
    return (
        <RLister Records={_overallAttendance} activeScroll={true} center={true} />
    )
}

export default React.memo(ROverallDetails)