import React from 'react'
import styles from './attendance.module.scss'
import { useState } from 'react'

//fa-regular fa-pen-to-square
//fa-solid fa-check
const CheckedDiv = (
    {
        initialIcon,
        index,
        iconStyle,
        divClassName,
        divStyle={ width: '22px', height: '22px', margin: 'auto', backgroundColor: '#D7E3FD', cursor: 'pointer' },
        onClick,
        detailsState,
        justIcon
    }
) => {

    return (
        <div className={divClassName} style={divStyle} onClick={()=>onClick(index)}>
            {justIcon?(
                <i className={justIcon} style={iconStyle}></i>
            ):(
                <i className={detailsState[index]?.icon? detailsState[index].icon:initialIcon } style={iconStyle}></i>

            ) }
        </div>
    )
}

export default CheckedDiv