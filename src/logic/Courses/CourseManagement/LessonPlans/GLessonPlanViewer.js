import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams, useHistory } from "react-router-dom";
import { genericPath, baseURL } from "engine/config";
import {
  getLessonPlanByLessonPlanId,
  exportLessonPlanAsPdf,
} from "store/actions/teacher/lessonPlan.actions";
import Loader from "utils/Loader";
import RLessonPlanViewer from "view/Courses/CourseManagement/LessonPlan/RLessonPlanViewer";

const GLessonPlanViewer = ({ LessonPlanId, importCollaboration }) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { lessonPlanId, courseId, curriculumId, cohortId } = useParams();

  useEffect(() => {
    dispatch(
      getLessonPlanByLessonPlanId(LessonPlanId ? LessonPlanId : lessonPlanId)
    );
  }, []);

  const { lessonPlan, lessonPlanLoading, exportLessonPlanAsPdfLoading } =
    useSelector((state) => state.lessonPlan);

  const handleBackToLessonPlanEditor = () => {
    if (courseId) {
      history.push(
        `${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/editor/lesson-plans/${lessonPlanId}`
      );
    } else {
      history.push(
        `${baseURL}/${genericPath}/course-management/curricula/${curriculumId}/editor/lesson-plans/${lessonPlanId}`
      );
    }
  };
  const handleExportLessonPlanAsPdf = async (lessonPlanId) => {
    dispatch(exportLessonPlanAsPdf(lessonPlanId));
  };
  return (
    <div>
      {lessonPlanLoading ? (
        <Loader />
      ) : (
        <RLessonPlanViewer
          lessonPlan={lessonPlan}
          handleBackToLessonPlanEditor={handleBackToLessonPlanEditor}
          previewMode={false}
          setPreviewMode={null}
          exportLessonPlanAsPdfLoading={exportLessonPlanAsPdfLoading}
          handleExportLessonPlanAsPdf={handleExportLessonPlanAsPdf}
          importCollaboration={importCollaboration}
        />
      )}
    </div>
  );
};

export default GLessonPlanViewer;
