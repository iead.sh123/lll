import React, { createContext, useState } from "react";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { setIn, useFormik } from "formik";
import { gradingApi } from "api/grading";
import { useParams } from "react-router-dom";
import Loader from "utils/Loader";
import RGradingGroups from "./RGradingGroups";
import RGradableItems from "./RGradableItems";
import Swal from "utils/Alert";
import { DANGER } from "utils/Alert";
import { useMutateData } from "hocs/useMutateData";
import {
	tablesTypes,
	gradingGroupStructure,
	gradableItemsStructure,
	finalMarksTable,
	itemSliderTable,
	alternativesTableData,
} from "./Constants";
import { Route } from "react-router-dom";
import RFinalMarks from "./RFinalMarks";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import * as colors from "config/constants";
import styles from "./grading.module.scss";
import { useHistory, useLocation } from "react-router-dom";
import RItemsSlider from "./RItemsSlider";
import RStudentsSlider from "./RStudentsSlider";
import { useSelector } from "react-redux";
import GStudentTerm from "./GStudentTerm";
import AppModal from "components/Global/ModalCustomize/AppModal";
import RViewAlternatives from "./RViewAlternatives";
import RAddAlternatives from "./RAddAlternatives";
import RAlternativesSlider from "./RAlternativesSlider";
import { toast } from "react-toastify";
export const GradingContext = createContext();
const GGradingManagement = () => {
	const { courseId, cohortId, itemId, itemType, studentId, parentType, parentId, alternativeId, alternativeType } = useParams();
	console.log("studentId", studentId);
	console.log("itemId", itemId);
	const userType = useSelector((state) => state?.auth?.user?.user_type);
	const userId = useSelector((state) => state?.auth?.user?.id);
	const [enterPressed, setEnterPressed] = useState(false);
	const [finalMarks, setFinalMarks] = useState(false);
	const [calculated, setCalculated] = useState(false);
	const [showAlternativesModal, setShowAlternativesModal] = useState(false);
	const [addAlternativesModal, setAddAlternativesModal] = useState(false);
	const history = useHistory();
	const location = useLocation();
	const from = location?.state?.from;
	const fromId = location?.state?.fromId;
	const fromType = location?.state?.fromType;
	const [finalMarksOrder, setFinalMarksOrder] = useState("alphabiticallyAsc");
	const [itemsSliderOrder, setItemSliderOrder] = useState("alphabiticallyAsc");
	const [clickedAlternativesItem, setClickedAlternativesItem] = useState({});
	const [gradingGroupsPage, setGradingGroupsPage] = useState(1);
	const [gradableItemsPage, setGradableItemsPage] = useState(1);
	const [alternativesPage, setAlternativesPage] = useState(1);
	const [alternativeSearchTerm, setAlternativeSearchTerm] = useState("");
	const handleGradingGroupsPageChange = (pageNumber) => {
		setGradingGroupsPage(pageNumber);
	};
	const handleGradableItemsPageChange = (pageNumber) => {
		setGradableItemsPage(pageNumber);
	};
	const handleAlternativesPageChange = (pageNumber) => {
		setAlternativesPage(pageNumber);
	};
	if (userType == "student") {
		return <GStudentTerm studentId={userId} />;
	}

	const closeShowAlternativesModal = () => {
		setShowAlternativesModal(false);
	};
	const closeAddAlternativesModal = () => {
		setAddAlternativesModal(false);
	};
	console.log("finalMarksOrder", finalMarksOrder);
	const initialValues = {
		gradingGroups: { ids: [] },
		gradableItems: { ids: [] },
		finalMarks: {},
		itemSlider: {},
		studentSlider: {},
		alternativesSlider: {},
		itemAlternatives: {},
		filteredAlternatives: {},
		alternativesIdsLoading: [],
	};
	const { values: gradingState, setFieldValue, resetForm } = useFormik({ initialValues });

	//--------------------------------------------grading groups------------------------------------
	const {
		data: gradingGroups,
		isLoading: isLoadingGradingGroups,
		isError: gradingGroupsIsError,
		error: gradingGroupsError,
		refetch: refetchGroups,
	} = useFetchDataRQ({
		queryKey: [tablesTypes.gradingGroups, gradingGroupsPage],
		enableCondition: !itemId && !studentId && !alternativeId ? true : false,
		queryFn: () => gradingApi.gradingGroupsListByCourse(courseId, false, gradingGroupsPage),
		onSuccessFn: (data) => {
			const groupsNames = [];
			const recordsIds = [];
			const records = data.data.data.records.items.reduce((result, record) => {
				const states = data.data.data.headers.reduce((result, header) => {
					result[header.label] = { touched: false, saved: true, value: record[header.value], type: header.type };
					return result;
				}, {});
				console.log("states", states);

				result[record.id] = { ...states };
				recordsIds.push(record.id);
				groupsNames.push({ label: record.name, value: record.id });
				console.log("recordrecord", result[record.id]);
				return result;
			}, {});
			records["ids"] = recordsIds;
			records["groupsNames"] = groupsNames;
			setFieldValue(tablesTypes.gradingGroups, records);
		},
	});
	const addGradingGroupMutation = useMutateData({
		queryFn: ({ courseId, payload }) => gradingApi.addGradingGroup(courseId, payload),
		multipleKeys: true,
		invalidateKeys: [
			[tablesTypes.gradingGroups, gradingGroupsPage],
			[tablesTypes.gradableItems, gradableItemsPage],
			[tablesTypes.finalMarks],
		],
		closeDialog: () => setEnterPressed(false),
		onErrorFn: (error) => {
			setEnterPressed(false);
		},
	});
	const editGradingGroupMutation = useMutateData({
		queryFn: ({ groupId, payload }) => gradingApi.updateGradingGroup(groupId, payload),
		multipleKeys: true,
		invalidateKeys: [
			[tablesTypes.gradingGroups, gradingGroupsPage],
			[tablesTypes.gradableItems, gradableItemsPage],
			[tablesTypes.finalMarks],
		],
		closeDialog: () => setEnterPressed(false),
		onErrorFn: (error) => {
			setEnterPressed(false);
		},
	});
	const deleteGradingGroupMutation = useMutateData({
		queryFn: ({ groupId }) => gradingApi.deleteGradingGroup(groupId),
		multipleKeys: true,
		invalidateKeys: [
			[tablesTypes.gradingGroups, gradingGroupsPage],
			[tablesTypes.gradableItems, gradableItemsPage],
			[tablesTypes.finalMarks],
		],
		closeDialog: () => setEnterPressed(false),
		onErrorFn: (error) => {
			setEnterPressed(false);
		},
	});
	const isGradingGroupValidate = (item) => {
		if (!item["Group Name"].value || !item["Weight"].value || item["Extra Points"].value === "") {
			return false;
		}
		return true;
	};
	const handleAddingGroup = () => {
		console.log("Adding new", gradingState.gradingGroups);
		if (gradingState?.gradingGroups?.ids?.length > 0)
			if (gradingState.gradingGroups.ids.some((value) => value <= 0)) {
				setAllInputTouched(tablesTypes.gradingGroups);
				return;
			}
		setEnterPressed(false);
		const lastId = -gradingState?.gradingGroups?.ids?.length;
		gradingState?.gradingGroups?.ids?.push(lastId);
		setFieldValue(tablesTypes.gradingGroups, {
			...gradingState.gradingGroups,
			[lastId]: {
				...gradingGroupStructure,
			},
		});
	};
	//-----------------------------------------------gradableItems--------------------------------------
	const {
		data: gradableItems,
		isLoading: isLoadingGradableItems,
		fetchStatus: GradableItemFetchStatus,
		isFetching: isFetchingGradableItems,
		isError: gradableItemsIsError,
		error: gradableItemsError,
		refetch: refetchItems,
	} = useFetchDataRQ({
		queryKey: [tablesTypes.gradableItems, gradableItemsPage],
		queryFn: () => gradingApi.gradableItemsListByCourse(courseId, null, null, gradableItemsPage),
		// enableCondition: (!activeItemSliderId && !activeStudentSliderId) ? true : false,
		onSuccessFn: (data) => {
			const recordsIds = [];
			const records = data.data.data.records.items.reduce((result, record) => {
				const states = data.data.data.headers.reduce((result, header) => {
					result[header.label] = { touched: false, saved: true, value: record[header.value], type: header.type };
					return result;
				}, {});
				console.log("states", states);
				result[record.id] = { ...states };
				result[record.id]["Group"].value = { label: record.groupName ?? "NoGroup", value: record.groupId ?? null };
				result[record.id]["Alternatives"] = {
					...result[record.id]["Alternatives"],
					parentAlternativeId: record.parentAlternativeId,
					parentAlternativeType: record.parentAlternativeType,
				};
				result[record.id]["Graded Students"].value = {
					numberOFStudents: record.numberOFStudents,
					numberOFGradedStudents: record.numberOFGradedStudents,
				};
				recordsIds.push(record.id);
				return result;
			}, {});
			records["ids"] = recordsIds;
			setCalculated(false);
			setFieldValue(tablesTypes.gradableItems, records);
		},
	});
	const addCustomGradableItemMutation = useMutateData({
		queryFn: ({ courseId, payload }) => gradingApi.createCustomGradableItem(courseId, payload),
		multipleKeys: true,
		invalidateKeys: [
			[tablesTypes.gradableItems, gradableItemsPage],
			[tablesTypes.finalMarks, finalMarksOrder],
		],
		closeDialog: () => setEnterPressed(false),
		onErrorFn: (error) => {
			setEnterPressed(false);
		},
	});
	const editGradableItemMutation = useMutateData({
		queryFn: ({ type, id, payload }) => gradingApi.updateGraddableItem(type, id, payload),
		multipleKeys: true,
		invalidateKeys: [
			[tablesTypes.gradableItems, gradableItemsPage],
			[tablesTypes.finalMarks, finalMarksOrder],
		],
		closeDialog: () => setEnterPressed(false),
		onErrorFn: (error) => {
			setEnterPressed(false);
		},
	});
	const deleteGradableItem = useMutateData({
		queryFn: ({ id }) => gradingApi.deleteGraddableItem(id),
		multipleKeys: true,
		invalidateKeys: [
			[tablesTypes.gradableItems, gradableItemsPage],
			[tablesTypes.finalMarks, finalMarksOrder],
		],
	});
	const assignGradableItemToGroupMutation = useMutateData({
		queryFn: ({ type, itemId, groupId }) => gradingApi.assignGradableItemToGroup(type, itemId, groupId),
		multipleKeys: true,
		invalidateKeys: [
			[tablesTypes.gradableItems, gradableItemsPage],
			[tablesTypes.finalMarks, finalMarksOrder],
		],
	});
	const isGradableItemsValidate = (item) => {
		if (!item["Points"].value || !item["Name"].value || !item["Weight"].value) {
			return false;
		}
		return true;
	};

	const handleSelectGroup = (event, item, { id, type, inputLabel }) => {
		console.log("h1234");
		if (!isGradableItemsValidate(item)) {
			setFieldValue(type, {
				...gradingState[type],
				[id]: {
					...gradingState[type][id],
					[inputLabel]: {
						...gradingState[type][id][inputLabel],
						value: { ...event },
						saved: true,
					},
				},
			});
			return;
		}
		id <= 0 ? "" : assignGradableItemToGroupMutation.mutate({ type: item["Type"].value, itemId: id, groupId: event.value });
	};

	const handleAddingItem = () => {
		console.log("Adding new", gradingState.gradableItems);
		if (gradingState?.gradableItems?.ids?.length > 0)
			if (gradingState.gradableItems.ids.some((value) => value < 0)) {
				setAllInputTouched(tablesTypes.gradableItems);
				return;
			}
		setEnterPressed(false);
		const lastId = -gradingState?.gradableItems?.ids?.length;
		gradingState?.gradableItems?.ids?.push(lastId);
		setFieldValue(tablesTypes.gradableItems, {
			...gradingState.gradableItems,
			[lastId]: {
				...gradableItemsStructure,
			},
		});
	};
	//-----------------------------------------Alternatives---------------------------------

	const { data: alternativesData, isLoading: isLoadingAlternatives } = useFetchDataRQ({
		queryKey: [tablesTypes.Alternatives, clickedAlternativesItem?.id],
		queryFn: () => gradingApi.loadItemAlternatives(clickedAlternativesItem.type ?? parentType, clickedAlternativesItem.id ?? parentId),
		enableCondition: showAlternativesModal || alternativeId ? true : false,
		keepPreviousData: true,
		onSuccessFn: (data) => {
			const alternativesIds = [];
			const alternatives = data.data.data.reduce((result, alternative) => {
				const alternativesData = alternativesTableData.reduce((result, header) => {
					result[header.label] = { value: alternative[header.value], hidden: header.hidden ? true : false };
					return result;
				}, {});
				result[alternative.id] = { ...alternativesData };
				alternativesIds.push(alternative.id);
				return result;
			}, {});
			alternatives["ids"] = alternativesIds;
			setFieldValue("itemAlternatives", alternatives);
		},
	});
	const {
		data: candidateAlternatives,
		isLoading: isLoadingCandidataeAlternatives,
		fetchStatus: candidateAlternativesFetchStatus,
	} = useFetchDataRQ({
		queryKey: ["filteredAlternatives", alternativesPage, alternativeSearchTerm],
		queryFn: () =>
			gradingApi.gradableItemsListByCourse(
				courseId,
				clickedAlternativesItem.id,
				clickedAlternativesItem.type,
				alternativesPage,
				alternativeSearchTerm
			),
		enableCondition: addAlternativesModal || alternativeId ? true : false,
		onSuccessFn: (data) => {
			const recordsIds = [];
			const records = data.data.data.records.items.reduce((result, record) => {
				const states = data.data.data.headers.reduce((result, header) => {
					result[header.label] = { touched: false, saved: true, value: record[header.value], type: header.type };
					return result;
				}, {});
				console.log("states", states);
				result[record.id] = { ...states };
				result[record.id]["Group"].value = { label: record.groupName ?? "NoGroup", value: record.groupId ?? null };
				result[record.id]["Alternatives"] = {
					...result[record.id]["Alternatives"],
					parentAlternativeId: record.parentAlternativeId,
					parentAlternativeType: record.parentAlternativeType,
				};
				result[record.id]["Graded Students"].value = {
					numberOFStudents: record.numberOFStudents,
					numberOFGradedStudents: record.numberOFGradedStudents,
				};
				recordsIds.push(record.id);
				return result;
			}, {});
			records["ids"] = recordsIds;
			setCalculated(false);
			setFieldValue("filteredAlternatives", records);
		},
	});
	const {
		isLoading: isLoadingAlternativesSlider,
		isFetching: isFetchingAlternativesSlider,
		isError: alternativeSliderisError,
		error: alternativeSliderError,
	} = useFetchDataRQ({
		queryKey: [tablesTypes.AlternativeSlider, alternativeId, itemsSliderOrder],
		queryFn: () => gradingApi.getAllStudentsDetailsInGradableItem(alternativeType, alternativeId, itemsSliderOrder),
		keepPreviousData: true,
		enableCondition: alternativeId && !isLoadingAlternatives ? true : false,
		onSuccessFn: (data) => {
			const gradableItem = data.data.data.gradableItem;
			const itemInfo = {
				id: gradableItem.id,
				itemType: gradableItem.type,
				itemName: gradableItem.name,
				groupName: gradableItem?.group?.name,
				groupId: gradableItem?.group?.id,
			};
			const recordsIds = [];
			const records = data.data.data.studentDetails.reduce((result, record) => {
				const states = itemSliderTable.reduce((result, header) => {
					result[header.label] = { touched: false, saved: true, value: record[header.value], type: header.type };
					return result;
				}, {});
				result[record.student.id] = { ...states };
				result[record.student.id]["Students"].value = { name: record.student.full_name, image: record.student.image };
				recordsIds.push(record.student.id);
				return result;
			}, {});
			records["ids"] = recordsIds;
			records["itemInfo"] = { ...itemInfo };
			setFieldValue(tablesTypes.AlternativeSlider, records);
		},
	});
	const addAlternativesMutation = useMutateData({
		queryFn: ({ itemId, itemType, payload }) => gradingApi.addAndRemoveAlternatives(itemType, itemId, payload),
		multipleKeys: true,
		invalidateKeys: [
			[tablesTypes.gradableItems, gradableItemsPage],
			[tablesTypes.Alternatives, clickedAlternativesItem?.id],
			["filteredAlternatives", alternativesPage, alternativeSearchTerm],
		],
		onSuccessFn: () => {
			console.log("Adding Alternatives Success");
		},
	});
	const deleteAlternitaveMutation = useMutateData({
		queryFn: ({ itemId, itemType }) => gradingApi.RemoveAlternative(itemId, itemType),
		multipleKeys: true,
		invalidateKeys: [
			[tablesTypes.gradableItems, gradableItemsPage],
			[tablesTypes.Alternatives, clickedAlternativesItem?.id],
			["filteredAlternatives", alternativesPage, alternativeSearchTerm],
		],
		onSuccessFn: () => {
			console.log("Success");
		},
	});

	const handleAddingAlternativeToItem = (itemId, itemType, alternativeId, alternativeType) => {
		const idsIsLoading = [...gradingState.alternativesIdsLoading];
		idsIsLoading.push(alternativeId);
		setFieldValue(tablesTypes.AlternativesIdsLoading, idsIsLoading);
		const payload = {
			added_alternatives: [{ id: alternativeId, type: alternativeType }],
			removed_alternatives: [],
		};
		addAlternativesMutation.mutate({ itemId, itemType, payload });
	};
	//------------------------------------Final Marks---------------------------------------------
	const {
		isLoading: isLoadingFinalMarks,
		fetchStatus: finalMarksFetchStatus,
		isError: finalMarksIsError,
		error: finalMarksError,
		refetch: refetchFinalMarks,
	} = useFetchDataRQ({
		queryKey: [tablesTypes.finalMarks, finalMarksOrder],
		queryFn: () => gradingApi.getStudentsFinalMarksForCourse(courseId, finalMarksOrder),
		keepPreviousData: true,
		enableCondition: !itemId && !alternativeId ? true : false,
		onSuccessFn: (data) => {
			const recordsIds = [];
			const studentsIds = {};
			const records = data.data.data.reduce((result, record) => {
				const states = finalMarksTable.reduce((result, header) => {
					result[header.label] = { touched: false, saved: true, value: record[header.value], type: header.type };
					return result;
				}, {});
				console.log("states", states);
				result[record.relation_id] = { ...states };
				result[record.relation_id]["Students"].value = { name: record.full_name, image: record.image };
				recordsIds.push(record.relation_id);
				studentsIds[record.relation_id] = record.id;
				return result;
			}, {});
			records["ids"] = recordsIds;
			records["studentsIds"] = studentsIds;
			setFieldValue(tablesTypes.finalMarks, records);
		},
	});

	const editStudentTeacherMarkMutation = useMutateData({
		queryFn: ({ payload }) => gradingApi.updateTeacherMarkForStudent(payload),
		invalidateKeys: [tablesTypes.finalMarks, finalMarksOrder],
		closeDialog: () => setEnterPressed(false),
		onErrorFn: (error) => {
			setEnterPressed(false);
		},
	});
	const calculateStudentsFinalMarksMutation = useMutateData({
		queryFn: ({ courseId }) => gradingApi.calculateStudentsFinalMarks(courseId),
		multipleKeys: true,
		invalidateKeys: [
			[tablesTypes.finalMarks, finalMarksOrder],
			[tablesTypes.studentSlider, studentId],
		],
		onSuccessFn: (data) => {
			setCalculated(true);
		},
	});
	console.log("ItemId", itemId);
	//------------------------------------Items Slider---------------------------------------------
	const {
		isLoading: isLoadingItemSlider,
		isFetching: isFetchingItemsSlider,
		isError: itemSliderisError,
		error: itemSliderError,
		refetch: refetchItemSlider,
	} = useFetchDataRQ({
		queryKey: [tablesTypes.itemSlider, itemId, itemsSliderOrder],
		queryFn: () => gradingApi.getAllStudentsDetailsInGradableItem(itemType ?? fromType, itemId ?? fromId, itemsSliderOrder),
		keepPreviousData: true,
		enableCondition: (itemId && !isLoadingGradableItems) || studentId ? true : false,
		onSuccessFn: (data) => {
			const gradableItem = data.data.data.gradableItem;
			const itemInfo = {
				id: gradableItem.id,
				itemType: gradableItem.type,
				itemName: gradableItem.name,
				groupName: gradableItem?.group?.name,
				groupId: gradableItem?.group?.id,
			};
			const recordsIds = [];
			const records = data.data.data.studentDetails.reduce((result, record) => {
				const states = itemSliderTable.reduce((result, header) => {
					result[header.label] = { touched: false, saved: true, value: record[header.value], type: header.type };
					return result;
				}, {});
				result[record.student.id] = { ...states };
				result[record.student.id]["Students"].value = { name: record.student.full_name, image: record.student.image };
				recordsIds.push(record.student.id);
				return result;
			}, {});
			records["ids"] = recordsIds;
			records["itemInfo"] = { ...itemInfo };
			setFieldValue(tablesTypes.itemSlider, records);
		},
	});
	//-------------------------------------Student Details Slider -----------------------------------
	const {
		isLoading: isLoadingStudentDetailsSlider,
		isError,
		refetch: refetchStudentDetailsSlider,
	} = useFetchDataRQ({
		queryFn: () => gradingApi.getStudentDetailsForGroup(courseId, studentId),
		queryKey: [tablesTypes.studentSlider, studentId],
		keepPreviousData: true,
		enableCondition: studentId && !isLoadingFinalMarks && !isLoadingGradableItems ? true : false,
		onSuccessFn: (data) => {
			const groupsIds = [];
			const studentDetailsSlider = {};
			studentDetailsSlider["courseInfo"] = data?.data?.data.courseInfo;
			studentDetailsSlider["studentInfo"] = data?.data?.data?.student;
			const groupscollapses = data?.data?.data?.groups?.reduce((result, groupCollapse) => {
				const itemsIds = [];
				groupsIds.push(groupCollapse?.group?.id);
				result[groupCollapse?.group?.id] = {};
				result[groupCollapse?.group?.id]["name"] = groupCollapse?.group?.name;
				result[groupCollapse?.group?.id]["groupMark"] = groupCollapse.groupMark;
				const items = groupCollapse.items.reduce((result, item) => {
					const itemId = item.studentDetails.isAlternative ? item.alternative.id : item.gradableItem.id;
					const itemType = item.studentDetails.isAlternative ? item.alternative.type : item.gradableItem.type;
					const itemName = item.studentDetails.isAlternative ? item.alternative.name : item.gradableItem.name;
					itemsIds.push(itemId);
					result[itemId] = {
						name: itemName,
						points: item.gradableItem.points,
						mark: item.studentDetails.calculatedFinalTeacherGrade,
						teacherGrade: { touched: false, saved: true, value: item.studentDetails.teacherGrade },
						type: itemType,
					};
					return result;
				}, {});
				items["ids"] = itemsIds;
				result[groupCollapse?.group?.id]["items"] = { ...items };
				return result;
			}, {});
			groupscollapses["ids"] = groupsIds;
			studentDetailsSlider["groups"] = { ...groupscollapses };
			console.log("studentDetailsSlider", studentDetailsSlider);
			setFieldValue(tablesTypes.studentSlider, studentDetailsSlider);
		},
	});
	const updateTeacherMarkForStudentGradableItem = useMutateData({
		queryFn: (payload) => gradingApi.updateTeacherGrdableStudentMark(payload),
		invalidateKeys: [tablesTypes.studentSlider, studentId],
		closeDialog: () => setEnterPressed(false),
		onErrorFn: (error) => {
			setEnterPressed(false);
		},
	});
	//-----------------------------------Global Functions-----------------------------------------
	const updateTeacherGradableMarkForStudent = (field, value, groupId, itemId) => {
		setFieldValue(tablesTypes.studentSlider, {
			...gradingState.studentSlider,
			groups: {
				...gradingState.studentSlider.groups,
				[groupId]: {
					...gradingState.studentSlider.groups[groupId],
					items: {
						...gradingState.studentSlider.groups[groupId].items,
						[itemId]: {
							...gradingState.studentSlider.groups[groupId].items[itemId],
							teacherGrade: {
								...gradingState.studentSlider.groups[groupId].items[itemId].teacherGrade,
								[field]: value,
							},
						},
					},
				},
			},
		});
	};
	const handleEditFunctionality = (event, item, { id, type, inputLabel, groupId, itemId }, savedFlag = false) => {
		if (type == tablesTypes.studentSlider) {
			updateTeacherGradableMarkForStudent("saved", false, groupId, itemId);
			return;
		}
		setFieldValue(type, {
			...gradingState[type],
			[id]: {
				...gradingState[type][id],
				[inputLabel]: {
					...gradingState[type][id][inputLabel],
					saved: savedFlag,
				},
			},
		});
	};
	const setInputTouched = (type, id, inputLabel, touchedFlag = false, groupId, itemId) => {
		if (type == tablesTypes.studentSlider) {
			updateTeacherGradableMarkForStudent("touched", true, groupId, itemId);
			return;
		}
		setFieldValue(type, {
			...gradingState[type],
			[id]: {
				...gradingState[type][id],
				[inputLabel]: {
					...gradingState[type][id][inputLabel],
					touched: touchedFlag,
				},
			},
		});
	};
	const setAllInputTouched = (type) => {
		const newState = Object.keys(gradingState[type]).reduce((acc, id) => {
			const headers = gradingState[type][id];
			acc[id] = Object.keys(headers).reduce((subAcc, header) => {
				subAcc[header] = { ...gradingState[type][id][header], touched: true };
				return subAcc;
			}, {});
			return acc;
		}, {});
		newState["ids"] = [...gradingState[type].ids];
		setFieldValue(type, newState);
	};
	const handleInputChange = (event, item, { id, type, groupId, itemId, inputLabel }) => {
		if (type == tablesTypes.studentSlider) {
			updateTeacherGradableMarkForStudent("value", event.target.value, groupId, itemId);
			return;
		}
		setFieldValue(type, {
			...gradingState[type],
			[id]: {
				...gradingState[type][id],
				[inputLabel]: {
					...gradingState[type][id][inputLabel],
					value: event.target.value,
				},
			},
		});
	};

	const handleInputSaved = (event, item, { id, type, groupId, itemId, studentId, inputLabel }) => {
		if (event.key == "Enter" || !event.key) {
			if (enterPressed) return;
			console.log("itemitem", item);
			console.log("itemitem", inputLabel);
			setInputTouched(type, id, inputLabel, true, groupId, itemId);
			if (item[inputLabel].value === "") return;

			let payload = {};
			switch (type) {
				case tablesTypes.gradingGroups:
					if (!isGradingGroupValidate(item)) {
						handleEditFunctionality(null, item, { id, type: tablesTypes.gradingGroups, inputLabel }, true);
						return;
					}
					setEnterPressed(true);
					payload = {
						name: item["Group Name"].value,
						weight: parseInt(item["Weight"].value),
						extraPoints: parseInt(item["Extra Points"].value),
					};
					id <= 0 ? addGradingGroupMutation.mutate({ courseId, payload }) : editGradingGroupMutation.mutate({ groupId: id, payload });
					break;
				case tablesTypes.gradableItems:
					if (!isGradableItemsValidate(item)) {
						handleEditFunctionality(null, item, { id, type: tablesTypes.gradableItems, inputLabel }, true);
						return;
					}
					setEnterPressed(true);
					id <= 0
						? (payload = {
								name: item["Name"].value,
								points: parseInt(item["Points"].value),
								groupId: parseInt(item["Group"].value.value),
								weight: parseInt(item["Weight"].value),
						  })
						: (payload = {
								name: item["Name"].value,
								points: parseInt(item["Points"].value),
								groupId: parseInt(item["Group"].value.value),
								weight: parseInt(item["Weight"].value),
								parentAlternativeId: item["Alternatives"].parentAlternativeId,
								parentAlternativeType: item["Alternatives"].parentAlternativeType,
						  });
					id <= 0
						? addCustomGradableItemMutation.mutate({ courseId, payload })
						: editGradableItemMutation.mutate({ type: item["Type"].value, id, payload });
					break;
				case tablesTypes.finalMarks:
					setEnterPressed(true);
					payload = { teacherGrades: [{ mark: parseFloat(item["Teacher Mark"].value), id: id }] };
					editStudentTeacherMarkMutation.mutate({ payload });
					break;
				case tablesTypes.studentSlider:
					setEnterPressed(true);
					payload = {
						teacherGrades: [{ mark: parseFloat(item.teacherGrade.value), type: item.type, studentId: studentId, gradableItemId: itemId }],
					};
					updateTeacherMarkForStudentGradableItem.mutate(payload);
					break;
			}
		}
	};

	const handleDelete = (id, type, extraInfo) => {
		switch (type) {
			case tablesTypes.gradingGroups:
				id <= 0 ? refetchGroups() : deleteGradingGroupMutation.mutate({ groupId: id });
				break;
			case tablesTypes.gradableItems:
				id <= 0 ? refetchItems() : deleteGradableItem.mutate({ id });
				break;
			case tablesTypes.Alternatives:
				deleteAlternitaveMutation.mutate({ itemId: id, itemType: extraInfo.itemType });
				break;
		}
	};

	// if (gradingGroupsIsError) {
	//     toast.error(gradingGroupsError.message)
	//     return
	// }
	// if (gradableItemsIsError) {
	//     toast.error(gradableItemsError.message)
	//     return
	// }
	// if (finalMarksIsError) {
	//     toast.error(finalMarksError.message)

	// }
	return (
		<GradingContext.Provider
			value={{
				setFieldValue,
				handleEditFunctionality,
				handleInputChange,
				handleInputSaved,
				handleDelete,
				handleAddingGroup,
				handleSelectGroup,
				handleAddingItem,
				refetchFinalMarks,
				setFinalMarksOrder,
				calculateFinalMarks: () => calculateStudentsFinalMarksMutation.mutate({ courseId }),
				setItemSliderOrder,
				setShowAlternativesModal,
				setAddAlternativesModal,
				setClickedAlternativesItem,
				handleAddingAlternativeToItem,
				handleGradingGroupsPageChange,
				handleGradableItemsPageChange,
				handleAlternativesPageChange,
				gradingState,
				gradingGroups,
				gradableItems,
				alternativesData,
				calculated,
				courseId,
				cohortId,
				history,
				location,
				isLoadingGradingGroups,
				isLoadingGradableItems: isLoadingGradableItems && GradableItemFetchStatus !== "idle",
				isLoadingFinalMarks: isLoadingFinalMarks && finalMarksFetchStatus !== "idle",
				isLoadingItemSlider,
				isFetchingItemsSlider,
				isFetchingGradableItems,
				isLoadingStudentDetailsSlider,
				isLoadingAlternatives,
				isCalculating: calculateStudentsFinalMarksMutation.isLoading,
				isLoadingAlternativesSlider,
				isDeletingAlternative: deleteAlternitaveMutation.isLoading,
				gradingGroupsPage,
				gradableItemsPage,
				alternativesPage,
				alternativeSearchTerm,
				setAlternativeSearchTerm,
			}}
		>
			{!itemId && !studentId && !alternativeId && (
				<>
					<RFlex className={styles.SwitchDiv} styleProps={{ width: "fit-content", gap: "25px" }}>
						<span
							onClick={() => setFinalMarks(false)}
							className={`p-0 m-0 ${!finalMarks ? "text-primary" : ""} ${finalMarks ? "" : styles.ActiveLink}`}
							style={{ cursor: "pointer", color: finalMarks ? colors.boldGreyColor : "" }}
						>
							{tr("Marks_Details")}
						</span>
						<span
							onClick={() => setFinalMarks(true)}
							className={`p-0 m-0 ${finalMarks ? "text-primary" : ""} ${finalMarks ? styles.ActiveLink : ""}`}
							style={{ cursor: "pointer", color: !finalMarks ? colors.boldGreyColor : "" }}
						>
							{tr("Final_Marks")}
						</span>
					</RFlex>
					{!finalMarks ? (
						<>
							<RGradingGroups />
							<RGradableItems />
						</>
					) : (
						<>
							<RFinalMarks />
						</>
					)}
					<AppModal
						show={showAlternativesModal}
						size={"lg"}
						header={true}
						headerSort={<RViewAlternatives item={clickedAlternativesItem} />}
						parentHandleClose={closeShowAlternativesModal}
					/>
					<AppModal
						show={addAlternativesModal}
						size={"lg"}
						header={true}
						headerSort={<RAddAlternatives item={clickedAlternativesItem} />}
						parentHandleClose={closeAddAlternativesModal}
					/>
				</>
			)}
			{studentId && (
				<RStudentsSlider
					studentId={parseInt(studentId)}
					studentsIds={
						from == "ItemsSlider"
							? gradingState?.itemSlider?.ids
							: gradingState?.finalMarks?.studentsIds && Object.values(gradingState?.finalMarks?.studentsIds)
					}
					courseId={courseId}
					cohortId={cohortId}
				/>
			)}
			{itemId && <RItemsSlider itemId={parseInt(itemId)} itemType={itemType} courseId={courseId} cohortId={cohortId} />}
			{alternativeId && (
				<RAlternativesSlider
					itemId={parseInt(alternativeId)}
					parentId={parseInt(parentId)}
					parentType={parentType}
					courseId={courseId}
					cohortId={cohortId}
				/>
			)}
		</GradingContext.Provider>
	);
};

export default GGradingManagement;
