import React, { useContext } from 'react'
import { GradingContext } from './GGradingManagement'
import RLister from 'components/Global/RComs/RLister'
import Loader from 'utils/Loader'
import iconsFa6 from 'variables/iconsFa6'
import RHoverInput from 'components/Global/RComs/RHoverInput/RHoverInput'
import Helper from 'components/Global/RComs/Helper'
import { tablesTypes } from './Constants'
import RFlex from 'components/Global/RComs/RFlex/RFlex'
import RButton from 'components/Global/RComs/RButton'
import tr from 'components/Global/RComs/RTranslator'
import * as colors from 'config/constants'
import styles from './grading.module.scss'
import { editableFields } from './Constants'
const RGradingTable = () => {
    const gradingData = useContext(GradingContext)
    console.log('gradingData.gradingState', gradingData.gradingState)
    const gradingGroups = gradingData?.gradingState?.gradingGroups
    console.log("gradingData.isLoadingGradingGroups", gradingData.isLoadingGradingGroups)
    const _gradingGroupsRecords = !gradingData.isLoadingGradingGroups ? gradingGroups?.ids?.map((recordId) => {
        const currentRecord = gradingGroups?.[recordId]
        console.log("currentRecord", currentRecord)
        return {
            id: recordId,
            table_name: tablesTypes.gradingGroups,
            details: Object.keys(currentRecord)?.map((header) => {
                const currentHeader = currentRecord?.[header]
                console.log("currentHeader", currentHeader)
                const editable = editableFields.includes(header)
                return {
                    key: header, value: editable ?
                        <div className='position-relative'>
                            <RHoverInput
                                inputValue={currentHeader.value}
                                handleInputDown={gradingData.handleInputSaved}
                                inputWidth='100px'
                                type={currentHeader.type}
                                focusOnInput={currentHeader.notFocused ? false : true}
                                inputPlaceHolder=""
                                handleInputChange={gradingData.handleInputChange}
                                inputIsNotValidate={!currentHeader.value && currentHeader.touched}
                                item={currentRecord}
                                extraInfo={{ id: recordId, type: tablesTypes.gradingGroups, inputLabel: header }}
                                handleOnBlur={gradingData.handleInputSaved}
                                handleEditFunctionality={gradingData.handleEditFunctionality}
                                saved={currentHeader.saved}
                            />
                            {!currentHeader.value && currentHeader.touched ?
                                <i className={`fas fa-exclamation-circle fa-md ${styles.ValidationIcon}`} style={{ color: colors.dangerColor }} /> : ''}
                        </div>
                        : currentHeader.value,
                    type: editable ? 'component' : ''
                }
            }),
            actions: [
                { name: "delete", icon: iconsFa6.delete, justIcon: true, actionIconClass: 'text-danger', onClick: () => { gradingData.handleDelete(recordId, tablesTypes.gradingGroups) } },
                // { name: "delete", icon: iconsFa6.delete, inDropdown: false, justIcon: true, actionIconClass: 'text-danger', onClick: () => { gradingData.handleDelete(recordId, tablesTypes.gradingGroups) } },
                // { name: "Add", icon: iconsFa6.plus, inDropdown: true, justIcon: true, actionIconClass: 'text-danger', color: colors.primaryColor, onClick: () => { gradingData.handleDelete(recordId, tablesTypes.gradingGroups) } },
            ]
        }
    }) : null
    return gradingData.isLoadingGradingGroups ? <Loader /> :
        <RFlex className="flex-column" styleProps={{ gap: '0px' }}>
            <RLister Records={_gradingGroupsRecords} withPagination={true} info={gradingData.gradingGroups} handleChangePage={gradingData.handleGradingGroupsPageChange} page={gradingData.gradingGroupsPage} />
            <RButton
                onClick={gradingData.handleAddingGroup}
                faicon={iconsFa6.plus}
                text={tr("Create_New_Group")}
                style={{ width: 'fit-content' }}
                className="p-0 m-0 text-primary"
                color="link" />
        </RFlex>

}

export default RGradingTable