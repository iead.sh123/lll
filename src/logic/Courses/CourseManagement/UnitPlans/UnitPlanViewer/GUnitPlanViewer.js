import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams, useHistory } from "react-router-dom";
import { genericPath, baseURL } from "engine/config";
import Loader from "utils/Loader";
import { getUnitPlanByIdAsync } from "store/actions/global/unitPlan.actions";
import RUnitPlanViewer from "view/UnitPlan/RUnitPlanViewer";

const GUnitPlanViewer = ({ UnitPlanId, importCollaboration }) => {
	const history = useHistory();
	const dispatch = useDispatch();

	const { courseId, cohortId, curriculumId, unitPlanId } = useParams();

	useEffect(() => {
		dispatch(getUnitPlanByIdAsync(UnitPlanId ? UnitPlanId : unitPlanId));
	}, []);

	const { unitPlan, unitPlanLoading, exportUnitPlanAsPdfLoading } = useSelector((state) => state.unitPlan);

	const handleBackToUnitPlanEditor = () => {
		if (courseId) {
			history.push(`${baseURL}/${genericPath}/course-management/course/${courseId}/cohort/${cohortId}/editor/unit-plans/${unitPlanId}`);
		} else {
			history.push(`${baseURL}/${genericPath}/course-management/curricula/${curriculumId}/editor/unit-plans/${unitPlanId}`);
		}
	};

	const handleExportUnitPlanAsPdf = async (unitPlanId) => {
		// dispatch(exportUnitPlanAsPdf(unitPlanId));
	};

	return (
		<div>
			{unitPlanLoading ? (
				<Loader />
			) : (
				<RUnitPlanViewer
					unitPlan={unitPlan}
					handleBackToUnitPlanEditor={handleBackToUnitPlanEditor}
					previewMode={false}
					setPreviewMode={null}
					exportUnitPlanAsPdfLoading={exportUnitPlanAsPdfLoading}
					handleExportUnitPlanAsPdf={handleExportUnitPlanAsPdf}
					importCollaboration={importCollaboration}
				/>
			)}
		</div>
	);
};

export default GUnitPlanViewer;
