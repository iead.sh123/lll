import React, { useState, useEffect, useRef } from "react";
import {
	initUnitPlan,
	getValidationUnitPlan,
	setUnitPlanValue,
	addValidationToUnitPlan,
	addSectionToUnitPlan,
	addDataToSection,
	addItemsToSection,
	removeSection,
	removeLessonPlanFromSection,
	removeRubricFromSection,
	removeAttachmentFromSection,
	removeItemFromSection,
	removeAttachmentFromItems,
	saveUnitPlanAsync,
	getUnitPlanByIdAsync,
	getUnitPlanByIdToCreateTemplateAsync,
} from "store/actions/global/unitPlan.actions";
import { useDispatch, useSelector } from "react-redux";
import { validationUnitPlanSchema } from "./GUnitPlanValidation";
import { UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from "reactstrap";
import { useHistory, useParams, useLocation } from "react-router-dom";
import GPickLessonPlanLister from "../../LessonPlans/GPickLessonPlanLister";
import useWindowDimensions from "components/Global/useWindowDimensions";
import GPickRubricLister from "../../LessonPlans/GPickRubricLister";
import RUnitPlanAddItem from "view/UnitPlan/RUnitPlanSections/RUnitPlanItems/RUnitPlanAddItem";
import RUnitPlanEditor from "view/UnitPlan/RUnitPlanEditor";
import GUnitPlanTree from "./GUnitPlanTree";
import withDirection from "hocs/withDirection";
import RButton from "components/Global/RComs/RButton";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import * as yup from "yup";

export const UnitPlanContext = React.createContext();

const GUnitPlanEditor = ({ dir }) => {
	const { sectionId, unitPlanId } = useParams();
	const history = useHistory();
	const dispatch = useDispatch();
	const location = useLocation();
	const targetRefs = useRef({});
	const queryParams = new URLSearchParams(location.search);
	const unitPlanItemParams = queryParams.get("unitPlanItem");
	const isTemplateParams = queryParams.get("isTemplate");

	const { width } = useWindowDimensions();
	const [previewMode, setPreviewMode] = useState(false);
	const [unitPlanSectionId, setUnitPlanSectionId] = useState(null);
	const [unitPlanItemId, setUnitPlanItemId] = useState(null);
	const [typeToPick, setTypeToPick] = useState(null);
	const [pickARubric, setPickARubric] = useState(false);
	const [pickALessonPlan, setPickALessonPlan] = useState(false);
	const [addItem, setAddItem] = useState(false);
	const mobile = width < 800;

	const {
		unitPlan,
		unitPlanErrors,
		test,
		unitPlanLoading,
		saveUnitPlanLoading,
		saveAsADraftUnitPlanLoading,
		saveAsATemplateUnitPlanLoading,
	} = useSelector((state) => state.unitPlan);

	useEffect(() => {
		if (unitPlanId && !isTemplateParams) {
			dispatch(getUnitPlanByIdAsync(unitPlanId));
		} else if (unitPlanId && isTemplateParams) {
			dispatch(getUnitPlanByIdToCreateTemplateAsync(unitPlanId));
		} else {
			dispatch(initUnitPlan());
		}

		dispatch(getValidationUnitPlan());
	}, [unitPlanId]);

	const handelUnitPlanChange = (name, value) => {
		dispatch(setUnitPlanValue(name, value));

		if (name !== "tags") {
			yup
				.reach(validationUnitPlanSchema, name)
				.validate(value)
				.then(() => {
					dispatch(
						addValidationToUnitPlan({
							name: name,
							message: undefined,
							sectionId: undefined,
							itemId: undefined,
						})
					);
				})
				.catch((err) => {
					dispatch(
						addValidationToUnitPlan({
							name: name,
							message: err.errors[0],
							sectionId: undefined,
							itemId: undefined,
						})
					);
				});
		}
	};

	const handleSaveUnitPlan = (type) => {
		const publishUnitPlan = {
			...unitPlan,
			section_id: sectionId,
			published:
				type == "save_and_publish"
					? true
					: type == "save_as_a_draft"
					? false
					: type == "save_as_a_template_and_publish"
					? true
					: type == "save_as_a_draft_template"
					? false
					: false,
			is_template:
				type == "save_and_publish"
					? false
					: type == "save_as_a_draft"
					? false
					: type == "save_as_a_template_and_publish"
					? true
					: type == "save_as_a_draft_template"
					? true
					: false,
			type:
				type == "save_and_publish"
					? "save_and_publish"
					: type == "save_as_a_draft"
					? "save_as_a_draft"
					: type == "save_as_a_template_and_publish"
					? "save_as_a_template_and_publish"
					: type == "save_as_a_draft_template"
					? "save_as_a_draft_template"
					: "",
		};

		dispatch(saveUnitPlanAsync({ data: publishUnitPlan }, history));

		// validationUnitPlanSchema
		//   .validate(publishUnitPlan, { abortEarly: false })
		//   .then(() => {
		//     dispatch(saveUnitPlanAsync({ data: publishUnitPlan }, history));
		//   })
		//   .catch((err) => {
		//     const validationErrors = {};
		//     err.inner.forEach((error) => {
		//       validationErrors[error.path] = error.message;
		//     });
		//     dispatch(
		//       addValidationToUnitPlan({
		//         name: undefined,
		//         message: validationErrors,
		//         sectionId: undefined,
		//         itemId: undefined,
		//       })
		//     );
		//   });
	};

	const handleAddNewSections = (indexAdd) => {
		dispatch(addSectionToUnitPlan(indexAdd));
	};

	const handelSectionChange = (name, value, sectionId) => {
		dispatch(addDataToSection(value, name, sectionId));

		// yup
		//   .reach(validationUnitPlanSchema, `uplsection[].${name}`)
		//   .validate(value)
		//   .then(() => {
		//     dispatch(
		//       addValidationToUnitPlan({
		//         name: name,
		//         message: undefined,
		//         sectionId: sectionId,
		//         itemId: undefined,
		//       })
		//     );
		//   })
		//   .catch((err) => {
		//     dispatch(
		//       addValidationToUnitPlan({
		//         name: name,
		//         message: err.errors[0],
		//         sectionId: sectionId,
		//         itemId: undefined,
		//       })
		//     );
		//   });
	};

	const handleAddItemsToSection = (sectionId, itemId, name, value, type, visible) => {
		dispatch(addItemsToSection(sectionId, itemId, name, value, type, visible));
	};

	const handleRemoveItemFromSection = (sectionId, itemId) => {
		dispatch(removeItemFromSection(sectionId, itemId));
	};

	const handleRemoveSection = (sectionId) => {
		dispatch(removeSection(sectionId));
	};

	const handleRemoveLessonPlanFromSection = (sectionId) => {
		dispatch(removeLessonPlanFromSection(sectionId));
	};

	const handleRemoveRubricFromSection = (sectionId) => {
		dispatch(removeRubricFromSection(sectionId));
	};

	const handleRemoveAttachmentFromSection = (sectionId) => {
		dispatch(removeAttachmentFromSection(sectionId));
	};

	const handleRemoveAttachmentFromItem = (sectionId, itemId, type) => {
		dispatch(removeAttachmentFromItems(sectionId, itemId, type));
	};

	const handleOpenPickARubric = () => setPickARubric(true);
	const handleClosePickARubric = () => setPickARubric(false);
	const handleOpenPickALessonPlan = () => setPickALessonPlan(true);
	const handleClosePickALessonPlan = () => setPickALessonPlan(false);
	const handleOpenAddItem = () => setAddItem(true);
	const handleCloseAddItem = () => setAddItem(false);

	useEffect(() => {
		const targetRef = targetRefs.current[+unitPlanItemParams];
		if (targetRef) {
			// targetRef.scrollIntoViewIfNeeded(true, {
			//   behavior: "smooth",
			//   block: "center",
			//   inline: "nearest",
			// });
			targetRef.scrollIntoView(true, {
				behavior: "smooth",
				block: "center",
				inline: "nearest",
			});
		}
	}, [unitPlanItemParams, targetRefs.current[+unitPlanItemParams]]);

	return (
		<>
			{unitPlanLoading ? (
				<Loader />
			) : (
				<>
					{pickARubric && unitPlanSectionId && (
						<GPickRubricLister
							handleAddItemsToSection={handleAddItemsToSection}
							handleClosePickARubric={handleClosePickARubric}
							openPickARubric={pickARubric}
							sectionId={unitPlanSectionId}
							itemId={unitPlanItemId}
							typeToPick={typeToPick}
						/>
					)}

					{pickALessonPlan && unitPlanSectionId && (
						<GPickLessonPlanLister
							handleAddItems={handleAddItemsToSection}
							handleClosePickALessonPlan={handleClosePickALessonPlan}
							openPickALessonPlan={pickALessonPlan}
							sectionId={unitPlanSectionId}
							itemId={unitPlanItemId}
							typeToPick={typeToPick}
						/>
					)}

					{addItem && unitPlanSectionId && unitPlanItemId && (
						<RUnitPlanAddItem
							handleAddItemsToSection={handleAddItemsToSection}
							handleCloseAddItem={handleCloseAddItem}
							openAddItem={addItem}
							sectionId={unitPlanSectionId}
							itemId={unitPlanItemId}
						/>
					)}
					<UnitPlanContext.Provider
						value={{
							unitPlan,
							previewMode,
							setPreviewMode,
							handelUnitPlanChange,
							handleAddNewSections,
							handelSectionChange,
							handleAddItemsToSection,
							handleRemoveSection,
							handleOpenPickARubric,
							setUnitPlanSectionId,
							setUnitPlanItemId,
							setTypeToPick,
							handleOpenPickALessonPlan,
							handleRemoveLessonPlanFromSection,
							handleRemoveRubricFromSection,
							handleRemoveAttachmentFromSection,
							handleRemoveItemFromSection,
							handleOpenAddItem,
							handleRemoveAttachmentFromItem,
							unitPlanErrors,
							dir,
							unitPlanItemParams,
							targetRefs,
						}}
					>
						<GUnitPlanTree />
						<RUnitPlanEditor />
					</UnitPlanContext.Provider>

					{!previewMode && (
						<RFlex
							styleProps={{
								justifyContent: "space-between",
								display: mobile ? "block" : "flex",
							}}
						>
							<RFlex>
								<RButton
									text={tr`save_and_publish`}
									onClick={() => handleSaveUnitPlan("save_and_publish")}
									color="primary"
									loading={saveUnitPlanLoading}
									disabled={saveUnitPlanLoading || saveAsADraftUnitPlanLoading || saveAsATemplateUnitPlanLoading ? true : false}
								/>
								<RButton
									text={tr`save_and_draft`}
									onClick={() => handleSaveUnitPlan("save_as_a_draft")}
									color="primary"
									outline
									className={`draft_button`}
									loading={saveAsADraftUnitPlanLoading}
									disabled={saveAsADraftUnitPlanLoading || saveUnitPlanLoading || saveAsATemplateUnitPlanLoading ? true : false}
								/>
							</RFlex>
							<RFlex>
								<UncontrolledDropdown direction="down">
									<DropdownToggle aria-haspopup={true} color="primary" data-toggle="dropdown" outline>
										<RFlex styleProps={{ alignItems: "center" }}>save as a template</RFlex>
									</DropdownToggle>
									<DropdownMenu persist aria-labelledby="navbarDropdownMenuLink" right>
										<DropdownItem
											key={1}
											onClick={() => handleSaveUnitPlan("save_as_a_template_and_publish")}
											disabled={saveAsATemplateUnitPlanLoading || saveUnitPlanLoading || saveAsADraftUnitPlanLoading ? true : false}
										>
											save as a template and publish
										</DropdownItem>
										<DropdownItem divider />
										<DropdownItem
											key={2}
											onClick={() => handleSaveUnitPlan("save_as_a_draft_template")}
											disabled={saveAsATemplateUnitPlanLoading || saveUnitPlanLoading || saveAsADraftUnitPlanLoading ? true : false}
										>
											save as a draft template
										</DropdownItem>
									</DropdownMenu>
								</UncontrolledDropdown>
							</RFlex>
						</RFlex>
					)}
				</>
			)}
		</>
	);
};

export default withDirection(GUnitPlanEditor);
