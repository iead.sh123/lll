import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import React, { useEffect, useState } from "react";
import { useParams, useLocation } from "react-router-dom";
import { Terms } from "./constants";
import { courseApi } from "../.../../../../api/course/index";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RLoader from "components/Global/RComs/RLoader";
import styles from "./GSyllabus.module.scss";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import * as colors from "config/constants";
import AppModal from "components/Global/ModalCustomize/AppModal";
import RFileUploaderInFileManagement from "components/RComponents/RFileUploaderInFileManagement";
import getSourceForType from "utils/getSourceForType";
import { useFormik } from "formik";
import * as Yup from "yup";
import { Input } from "reactstrap";
import { useMutateData } from "hocs/useMutateData";
import RCkEditor from "components/Global/RComs/RCkEditor";
const GSyllabus = () => {
	const { courseId } = useParams();
	const location = useLocation();
	const courseIsMaster = location.pathname.includes("master");
	const createBookEndPoint = courseIsMaster
		? courseApi.createMasterCourseBookEndpoint(courseId)
		: courseApi.createCourseBookEndPoint(courseId);
	const createFileEndPoint = courseIsMaster
		? courseApi.createMasterCouresFileEndpoint(courseId)
		: courseApi.createCourseFileEndpoint(courseId);
	const [openUploader, setOpenUploader] = useState({ isOpen: false, type: Terms.Books });
	const handleCloseUploader = () => {
		setOpenUploader({ ...openUploader, isOpen: false });
	};
	const [addingLink, setAddingLink] = useState(false);
	const [editorData, setEditorData] = useState("");
	const initialValues = {
		title: "",
		link: "",
	};
	const validationSchema = Yup.object({
		title: Yup.string().required(tr("title_is_required")),
		link: Yup.string().required("URL is required"),
	});
	const { values, handleChange, handleBlur, setFieldValue, setTouched, touched, errors } = useFormik({
		initialValues,
		validationSchema,
	});
	//--------------------------------------Fetching course Syllabus-------------------------
	const {
		data: syllabusData,
		isLoading: isLoadingSyllabusData,
		isFetching: isFetchingSyllabusData,
	} = useFetchDataRQ({
		queryKey: courseIsMaster ? [Terms.MasterCourse, Terms.Syllabus] : [Terms.Course, Terms.Syllabus],
		queryFn: courseIsMaster ? () => courseApi.getMasterCourseSyllabus(courseId) : () => courseApi.getCourseSyllabus(courseId),
		onSuccessFn: (data) => {
			setEditorData(data?.data?.data.length <= 0 ? "" : data?.data?.data[0]?.html_content);
		},
	});

	const createCourseSyllabus = useMutateData({
		queryFn: courseIsMaster
			? ({ payload }) => courseApi.createMasterCourseSyllabus(courseId, payload)
			: ({ payload }) => courseApi.createCourseSyllabus(courseId, payload),
		invalidateKeys: courseIsMaster ? [Terms.MasterCourse, Terms.Syllabus] : [Terms.Course, Terms.Syllabus],
	});
	//--------------------------------------Fetching course data------------------------------
	const {
		data: courseData,
		isLoading: isLoadingCourse,
		isFetching: isFetchingCourse,
	} = useFetchDataRQ({
		queryKey: courseIsMaster ? [Terms.MasterCourse, courseId] : [Terms.Course, courseId],
		queryFn: courseIsMaster ? () => courseApi.getMasterCourseById(courseId) : () => courseApi.getCourseById(courseId),
	});

	//----------------------------------------Fetching Links----------------------------------
	const {
		data: links,
		isLoading: isLoadingLinks,
		isFetching: isFetchingLinks,
	} = useFetchDataRQ({
		queryKey: courseIsMaster ? [Terms.MasterCourse, courseId, Terms.Links] : [Terms.Course, courseId, Terms.Links],
		queryFn: courseIsMaster ? () => courseApi.getMasterCourseLinks(courseId) : () => courseApi.getCourseLinks(courseId),
	});

	const createLinkMutation = useMutateData({
		queryFn: courseIsMaster
			? ({ payload }) => courseApi.createMasterCourseLink(courseId, payload)
			: ({ payload }) => courseApi.createCourseLink(courseId, payload),
		onSuccessFn: () => {
			setAddingLink(false);
			setFieldValue("link", "");
			setFieldValue("title", "");
		},
		invalidateKeys: courseIsMaster ? [Terms.MasterCourse, courseId, Terms.Links] : [Terms.Course, courseId, Terms.Links],
	});

	const deleteLinkMutation = useMutateData({
		queryFn: ({ linkId }) => courseApi.deleteMasterCourseLink(linkId),
		invalidateKeys: courseIsMaster ? [Terms.MasterCourse, courseId, Terms.Links] : [Terms.Course, courseId, Terms.Links],
	});
	//----------------------------------------Fetching Books--------------------------------------
	const {
		data: books,
		isLoading: isLoadingBooks,
		isFetching: isFetchingBooks,
		refetch: refetchBooks,
	} = useFetchDataRQ({
		queryKey: courseIsMaster ? [Terms.MasterCourse, courseId, Terms.Books] : [Terms.Course, courseId, Terms.Books],
		queryFn: courseIsMaster ? () => courseApi.getMasterCourseBooks(courseId) : () => courseApi.getCourseBooks(courseId),
	});

	const deleteBookMutation = useMutateData({
		queryFn: ({ bookId }) => (courseIsMaster ? courseApi.deleteMasterCourseBook(bookId) : courseApi.deleteCourseBook(bookId)),
		invalidateKeys: courseIsMaster ? [Terms.MasterCourse, courseId, Terms.Books] : [Terms.Course, courseId, Terms.Books],
	});
	//----------------------------------------Fetching Files--------------------------------------
	const {
		data: files,
		isLoading: isLoadingFiles,
		isFetching: isFetchingFiles,
		refetch: refetchFiles,
	} = useFetchDataRQ({
		queryKey: courseIsMaster ? [Terms.MasterCourse, courseId, Terms.Files] : [Terms.Course, courseId, Terms.Files],
		queryFn: courseIsMaster ? () => courseApi.getMasterCourseFiles(courseId) : () => courseApi.getCourseFiles(courseId),
	});
	const deleteFileMutaiton = useMutateData({
		queryFn: ({ fileId }) => (courseIsMaster ? courseApi.deleteMasterCourseBook(fileId) : courseApi.deleteCourseFile(fileId)),
		invalidateKeys: courseIsMaster ? [Terms.MasterCourse, courseId, Terms.Files] : [Terms.Course, courseId, Terms.Files],
	});
	//-------------------------------------------global--------------------------------------
	const formatUrl = (url) => {
		// Check if the URL already includes http:// or https://
		const validUrl = url.startsWith("http://") || url.startsWith("https://") ? url : `https://${url}`;
		return validUrl;
	};

	const courseInfo = courseData?.data?.data;
	console.log(courseInfo);
	const rightSectionData = [
		{ key: tr("semester_length"), value: courseInfo?.semester_length },
		{ key: tr("max_class_size"), value: courseInfo?.max_class_size },
		{ key: tr("methods_of_instructions"), value: courseInfo?.methods_of_instruction },
		{ key: tr("course_designation"), value: courseInfo?.course_designation },
		{ key: tr("industry_designation"), value: courseInfo?.industry_designation },
		{ key: tr("typically_offered"), value: courseInfo?.typically_offered },
	];
	const handleSavingLink = () => {
		if (Object.keys(errors).length > 0) {
			return;
		}
		const payload = {
			title: values.title,
			link: formatUrl(values.link),
		};
		createLinkMutation.mutate({ payload });
	};
	const handleAddingLink = () => {
		if (addingLink) {
			setTouched({
				link: true,
				title: true,
			});
		} else {
			setAddingLink(true);
		}
	};

	const handleDeleteBook = (bookId) => {
		deleteBookMutation.mutate({ bookId });
	};

	const handleDeleteFile = (fileId) => {
		deleteFileMutaiton.mutate({ fileId });
	};
	const handleDeleteLink = (linkId) => {
		if (!linkId) {
			setAddingLink(false);
			setFieldValue("title", "");
			setFieldValue("link", "");
			setTouched({
				link: false,
				title: false,
			});
		} else {
			deleteLinkMutation.mutate({ linkId });
			setAddingLink(false);
		}
	};
	// if (isLoadingCourse || isLoadingSyllabusData) {
	// 	return <RLoader />;
	// }
	console.log("formik errors", errors);
	console.log("formik touched", touched);
	return (
		<RFlex className="justify-between" id="containerrrr">
			<RFlex id="left part" className="flex-col w-[72%]" styleProps={{ gap: "15px" }}>
				<RFlex>
					<span className="font-bold text-[16px]">{courseInfo?.name}</span>
					<div className={styles.codeStyle}>{courseInfo?.code}</div>
				</RFlex>
				<RFlex styleProps={{ gap: "40px" }}>
					<span className="w-[87px]">{tr("Difficulty_Level")}</span>
					<span>Beginner</span>
				</RFlex>
				<RFlex styleProps={{ gap: "40px" }}>
					<span className="w-[87px]">{tr("Units")}</span>
					<RFlex className="flex-col" styleProps={{ gap: "5px" }}>
						<RFlex className="items-center">
							<i className={`${iconsFa6.clock} text-themePrimary`} />
							<span>{courseInfo?.hour}</span>
						</RFlex>
						<span>{courseInfo?.credit}</span>
					</RFlex>
				</RFlex>
				<RFlex styleProps={{ gap: "40px" }}>
					<span className="w-[87px]">{tr("description")}</span>
					<span className="w-[625px]">{courseInfo?.description}</span>
				</RFlex>
				<RFlex className="flex-col" styleProps={{ gap: "5px" }}>
					<span className="font-bold">{tr("Syllabus")}</span>
					<RCkEditor data={editorData} handleChange={(data) => setEditorData(data)} />
				</RFlex>
			</RFlex>
			<RFlex id="right part" className="flex-col w-[28%]" styleProps={{ gap: "15px" }}>
				{rightSectionData.map((row) => (
					<RFlex className="justify-between ">
						<span className="w-[150px] text-[12px] text-themeBoldGrey">{row.key}</span>
						<span className="text-[13px]">{row.value}</span>
					</RFlex>
				))}
				{isLoadingLinks ? (
					<RLoader mini />
				) : (
					<RFlex id="links" styleProps={{ gap: "2px" }} className="flex-col">
						<RFlex className="justify-between items-center">
							<span>{tr("links")}</span>
							<RButton text={tr("add")} color="link" className="p-0 m-0 text-primary" onClick={handleAddingLink} />
						</RFlex>
						<RFlex className="flex-col" styleProps={{ gap: "8px" }}>
							{addingLink && (
								<RFlex className="items-center justify-between" styleProps={{ gap: "5px" }}>
									<RFlex>
										<RFlex className="flex-col" styleProps={{ gap: "0px" }}>
											<span className="text-themeBoldGrey text-[13px]">{tr("Link_title")}</span>
											<Input
												name="title"
												value={values.title}
												onChange={handleChange}
												onBlur={(event) => {
													handleBlur(event);
													handleSavingLink();
												}}
												onKeyDown={(event) => {
													event.key == "Enter" ? handleSavingLink() : "";
												}}
												className={`${touched.title && errors.title ? "input__error" : ""} w-[145px]`}
											/>
										</RFlex>
										<RFlex className="flex-col" styleProps={{ gap: "0px" }}>
											<span className="text-themeBoldGrey text-[13px]">{tr("URL")}</span>

											<Input
												name="link"
												value={values.link}
												onChange={handleChange}
												onBlur={(event) => {
													handleBlur(event);
													handleSavingLink();
												}}
												onKeyDown={(event) => {
													event.key == "Enter" ? handleSavingLink() : "";
												}}
												className={`${touched.link && errors.link ? "input__error" : ""} w-[145px]`}
											/>
										</RFlex>
									</RFlex>

									{createLinkMutation.isLoading ? (
										<i className={`${iconsFa6.spinner}`} />
									) : (
										<i onClick={() => handleDeleteLink()} className={`${iconsFa6.minusSolid} text-themeDanger cursor-pointer mt-3`} />
									)}
								</RFlex>
							)}
							{links?.data?.data?.map((link, index) => (
								<RFlex className="items-center justify-between">
									<a className="underline text-themePrimary" href={link.link}>
										{link?.title ? link.title : "-"}
									</a>
									<i
										onClick={() => handleDeleteLink(link.id)}
										className={`${iconsFa6.minusSolid} text-themeDanger h-fit cursor-pointer mt-0`}
									/>
								</RFlex>
							))}
						</RFlex>
					</RFlex>
				)}
				{isLoadingBooks ? (
					<RLoader mini />
				) : (
					<RFlex id="books" className="flex-col ">
						<RFlex className="justify-between items-center text-sm">
							<span>{tr("books")}</span>
							<RFlex className="items-center" styleProps={{ gap: "5px" }}>
								<span className="text-themeBoldGrey">{tr("upload_book")}</span>
								<RButton
									faicon={iconsFa6.paperclip}
									color="primary"
									onClick={() => {
										setOpenUploader({ isOpen: true, type: Terms.Books });
									}}
									className="m-0 p-0 w-fit"
								/>
							</RFlex>{" "}
						</RFlex>
						<RFlex className="flex-col ">
							{books?.data?.data?.map((book, index) => (
								<RFlex className="justify-between items-center">
									<RFlex className="items-start">
										<img
											width={22}
											height={22}
											src={getSourceForType({ fileLink: book.file_full_url ?? true, mimeType: book.mime_type })}
										/>
										<RFlex className="flex-col" styleProps={{ gap: "0px" }}>
											<a href={book.file_full_url} target="_blank">
												{book.name}
											</a>
											<span className="text-themeBoldGrey text-[12px]">{tr("File_size")}:5mg</span>
										</RFlex>
									</RFlex>
									{deleteBookMutation.isLoading ? (
										<i className={iconsFa6.spinner} />
									) : (
										<i
											onClick={() => handleDeleteBook(book.id)}
											className={`${iconsFa6.minusSolid} text-themeDanger h-fit cursor-pointer mt-0`}
										/>
									)}
								</RFlex>
							))}
						</RFlex>
					</RFlex>
				)}
				{isLoadingFiles ? (
					<RLoader mini />
				) : (
					<RFlex id="files" className="flex-col ">
						<RFlex className="justify-between items-center">
							<span>{tr("files")}</span>
							<RFlex className="items-center" styleProps={{ gap: "5px" }}>
								<span className="text-themeBoldGrey">{tr("upload_files")}</span>
								<RButton
									faicon={iconsFa6.paperclip}
									color="primary"
									onClick={() => {
										setOpenUploader({ isOpen: true, type: Terms.Files });
									}}
									className="m-0 p-0 w-fit"
								/>
							</RFlex>
						</RFlex>
						<RFlex className="flex-col h-[335px] overflow-auto">
							{files?.data?.data?.map((file, index) => (
								<RFlex className="items-center justify-between">
									<RFlex className="items-start">
										<img
											width={22}
											height={22}
											src={getSourceForType({ fileLink: file.file_full_url ?? true, mimeType: file.mime_type })}
										/>
										<RFlex className="flex-col" styleProps={{ gap: "0px" }}>
											<a href={file.file_full_url} target="_blank">
												{file.name}
											</a>
											<span className="text-themeBoldGrey text-[12px]">{tr("File_size")}:5mg</span>
										</RFlex>
									</RFlex>
									<i onClick={() => handleDeleteFile()} className={`${iconsFa6.minusSolid} text-themeDanger h-fit cursor-pointer mt-0`} />
								</RFlex>
							))}
						</RFlex>
					</RFlex>
				)}
				<AppModal
					size={"lg"}
					show={openUploader.isOpen}
					header={"Upload Files"}
					parentHandleClose={handleCloseUploader}
					headerSort={
						<RFileUploaderInFileManagement
							typeFile={[
								"image/*",
								".docx",
								".doc",
								".pptx",
								// "video/mp4",
								// "video/x-matroska",
								// "video/webm",
								// "video/x-m4v",
								// "video/quicktime",
								"text/*",
								"application/*",
								// "*",
								// "*/*",
							]}
							parentCallback={() => {}}
							customAPI={openUploader.type == Terms.Books ? createBookEndPoint : openUploader.type == Terms.Files ? createFileEndPoint : ""}
							customFormData={{}}
							successCallback={() => {
								refetchBooks();
								refetchFiles();
							}}
							parentCallbackToFillData={() => {}}
						/>
					}
				/>
			</RFlex>
		</RFlex>
	);
};

export default GSyllabus;
