export const Terms = {
	MasterCourse: "master_course",
	Course: "course",
	Files: "files",
	Links: "links",
	Books: "books",
	Syllabus: "syllabus",
};
