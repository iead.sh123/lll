import React from "react";
import RCreateDiscussion from "view/Courses/Discussion/RCreateDiscussion";
import Loader from "utils/Loader";
import tr from "components/Global/RComs/RTranslator";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useMutateData } from "hocs/useMutateData";
import { discussionApi } from "api/discussion";
import { useFormik } from "formik";
import * as yup from "yup";

const GCreateDiscussion = ({ data, handlers }) => {
	// - - - - - - - - - - - - - - Queries - - - - - - - - - - - - - -
	const {
		data: discussionByIdData,
		isLoading,
		fetchStatus,
	} = useFetchDataRQ({
		queryKey: ["discussionId", data.discussionId],
		queryFn: () => discussionApi.getDiscussionById(data.discussionId),
		enableCondition: data.discussionId ? true : false,
		onSuccessFn: (data) => {
			console.log("data?.data?.data?.image_url_520025520025", data?.data?.data?.image_url);
			//Because i need to set values to use in edit mode
			setValues({
				title: data?.data?.data?.title,
				content: data?.data?.data?.content,
			});
		},
	});

	const createDiscussionMutate = useMutateData({
		queryFn: ({ templateId, data }) => discussionApi.createDiscussion({ templateId, data }),
		invalidateKeys: ["discussions"],
		onSuccessFn: () => {
			handlers.handleCloseCreateDiscussion(null);
		},
	});

	const updateDiscussionMutate = useMutateData({
		queryFn: ({ discussionId, data }) => discussionApi.updateDiscussion({ discussionId, data }),
		invalidateKeys: ["discussions"],
		onSuccessFn: () => {
			handlers.handleCloseCreateDiscussion(null);
		},
	});

	// - - - - - - - - - - - - - - Formik - - - - - - - - - - - - - -
	const initialValues = {
		title: "",
		content: "",
		post_first: false,
		allow_comments: false,
		private_comments: false,
		required_comments: false,
		max_comments: "",
		max_replies: "",
		published: false,
		comments_number: null,
		attachment: '',
	};

	const validationSchema = yup.object({
		title: yup.string().required(tr("title_is_required")),
		content: yup.string().required(tr("content_is_required")),

		comments_number: yup
			.number()
			.positive("Must Be Greater Than 0")
			.test("comments_number", "This feild is required and should be less than max comments", (value, context) => {
				const { required_comments, max_comments } = context.parent;
				return !required_comments || value <= max_comments;
			}),

		max_comments: yup.number().positive("Must Be Greater Than 0"),
	});

	const handleFormSubmit = (values, resetForm) => {
		if (data.discussionId) {
			updateDiscussionMutate.mutate({ discussionId: discussionByIdData?.data?.data.id, data: values });
		} else {
			createDiscussionMutate.mutate({ templateId: 1, data: values });
		}
		resetForm();
	};

	const { values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue, setValues, resetForm } = useFormik({
		initialValues,
		onSubmit: handleFormSubmit,
		validationSchema,
	});

	// console.log("valuesvaluesvalues", values);

	if (isLoading && fetchStatus !== "idle") return <Loader />;

	return (
		<RCreateDiscussion
			formProperties={{
				values,
				setFieldValue,
				resetForm,
				handleBlur,
				handleChange,
				touched,
				errors,
			}}
			handlers={{
				handleSubmit,
				handleCloseCreateDiscussion: handlers.handleCloseCreateDiscussion,
			}}
			loading={{
				createDiscussionLoading: createDiscussionMutate.isLoading || updateDiscussionMutate.isLoading,
			}}
		/>
	);
};

export default GCreateDiscussion;
