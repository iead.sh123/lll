import React, { useRef, useEffect, useState } from "react";
import AppModal from "components/Global/ModalCustomize/AppModal";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import RDiscussionHeader from "view/Courses/Discussion/RDiscussionHeader";
import GCreateDiscussion from "./GCreateDiscussion";
import RDiscussionTable from "view/Courses/Discussion/RDiscussionTable";
import { discussionApi } from "api/discussion";
import { useFormik } from "formik";
import { deleteSweetAlert } from "components/Global/RComs/RAlert2";
import { useInfinitFetchDataRQ } from "hocs/useInfinitFetchDataRQ";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useMutateData } from "hocs/useMutateData";
import { useIsInViewport } from "hocs/useIsInViewport";
import Loader from "utils/Loader";

const GDiscussion = () => {
	const ref = useRef();
	const isInViewport = useIsInViewport(ref);

	// - - - - - - - - - - - - - - Formik - - - - - - - - - - - - - -
	const initialValues = {
		discussionId: null,
		groupingOfSearchFields: "",
		showCreateDiscussion: false,
		alertForDeleteDiscussion: false,
	};

	const { values, setFieldValue } = useFormik({
		initialValues: initialValues,
	});

	// - - - - - - - - - - - - - Queries - - - - - - - - - - - - - -
	// {status, error, data, isFetchingNextPage, hasNextPage, fetchNextPage}
	const discussionsData = useInfinitFetchDataRQ({
		queryKey: ["discussions"],
		getNextPageParam: (prevData) => {
			if (prevData.data?.data?.meta.current_page < prevData.data?.data?.meta.last_page) {
				return prevData.data?.data?.meta.current_page + 1;
			} else {
				return undefined;
			}
		},
		queryFn: ({ pageParam = 1 }) => discussionApi.getDiscussions(pageParam, values.groupingOfSearchFields),
		selectFn: (data) => {
			const newData = data.pages
				.flatMap((page) => page.data.data)
				.map((item) => {
					// console.log(item.data);
					return item.data;
				});
			return [].concat(...newData);
		},
	});

	// const discussionsData = useFetchDataRQ({
	// 	queryKey: ["discussions"],
	// 	queryFn: () => discussionApi.getDiscussions(1, values.groupingOfSearchFields),
	// })

	useEffect(() => {
		if (isInViewport && discussionsData.hasNextPage && !discussionsData.isFetchingNextPage) {
			discussionsData.fetchNextPage();
		}
	}, [isInViewport, discussionsData.hasNextPage, discussionsData.fetchNextPage]);

	const deleteDiscussionMutate = useMutateData({
		queryFn: (discussionId) => discussionApi.deleteDiscussion(discussionId),
		invalidateKeys: ["discussions", values.groupingOfSearchFields],
		displaySuccess: true,
	});

	// - - - - - - - - - - - - - - Open Modals - - - - - - - - - - - - - -
	const hideAlert = () => setFieldValue("alertForDeleteDiscussion", null);
	const showAlerts = (child) => setFieldValue("alertForDeleteDiscussion", child);

	const handleOpenCreateDiscussion = (id) => {
		// console.log(" handleOpenCreateDiscussion", id);
		setFieldValue("showCreateDiscussion", true);
		setFieldValue("discussionId", id);
	};
	const handleCloseCreateDiscussion = (id) => {
		// console.log(" handleCloseCreateDiscussion", id);
		setFieldValue("showCreateDiscussion", false);
		setFieldValue("discussionId", id);
	};

	// - - - - - - - - - - - - - Handlers - - - - - - - - - - - - - -

	const handleSearch = (emptyData, pageNumber) => {
		const pageField = values?.page;
		const titleField = values?.title;
		const creditField = values?.credit;
		const statusField = values?.is_published == 0 ? "0" : values?.is_published == 2 ? "all" : values?.is_published;

		let queryParam = "";

		if (!titleField && !creditField && !statusField && !pageField) {
			queryParam = "";
		} else {
			queryParam = "?";

			if (titleField) {
				queryParam += `title=${emptyData !== undefined ? "" : titleField}&`;
			}
			if (creditField) {
				queryParam += `credit=${creditField}&`;
			}

			if (statusField || statusField == "all") {
				queryParam += `is_published=${statusField == "all" ? "" : statusField}&`;
			}

			if (pageField) {
				queryParam += `page=${pageNumber}&`;
			}
			queryParam = queryParam.slice(0, -1);
		}
		setFieldValue("groupingOfSearchFields", queryParam);
		setFieldValue("page", pageNumber || 1);
	};

	const successDelete = (parameters) => {
		deleteDiscussionMutate.mutate(parameters.discussionId);
	};

	const handleRemoveDiscussion = (discussionId) => {
		const message = tr`Are you Sure ? This can't be undone!`;
		const confirm = tr`Yes, delete it`;
		deleteSweetAlert(showAlerts, hideAlert, successDelete, { discussionId }, message, confirm);
	};

	const handleChangeStatus = ({ discussionId, status }) => {
		changeDiscussionStatusMutate.mutate({ discussionId: discussionId, data: { is_published: status } });
	};
	return (
		<>
			<RFlex>
				{/* - - - - - - Create Discussion- - - - - - - - */}
				<AppModal
					size="md"
					show={values.showCreateDiscussion}
					parentHandleClose={handleCloseCreateDiscussion}
					headerSort={
						<GCreateDiscussion data={{ discussionId: values.discussionId }} handlers={{ handleCloseCreateDiscussion, setFieldValue }} />
					}
				/>

				{/* - - - - - - - Content - - - - - - - */}
				<RFlex className="flex-col w-[100%] mb-10">
					<RDiscussionHeader data={{ values }} handlers={{ setFieldValue, handleOpenCreateDiscussion, handleSearch }} />
					<RDiscussionTable
						data={{ values, info: discussionsData }}
						handlers={{
							setFieldValue,
							handleOpenCreateDiscussion,
							handleRemoveDiscussion,
							handleChangeStatus,
							handleSearch,
						}}
						loading={{
							discussionsLoading: discussionsData.isLoading,
							deleteDiscussionLoading: deleteDiscussionMutate.isLoading,
							// changeDiscussionStatusLoading: changeDiscussionStatusMutate.isLoading,
						}}
					/>
					{discussionsData.hasNextPage && (
						<div className="flex-col justify-center" ref={ref}>
							{discussionsData.isFetchingNextPage && <Loader />}
						</div>
					)}
				</RFlex>
			</RFlex>
		</>
	);
};

export default GDiscussion;
