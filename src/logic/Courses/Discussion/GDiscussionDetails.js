import React, { useRef, useEffect, useState } from "react";
import AppModal from "components/Global/ModalCustomize/AppModal";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { discussionApi } from "api/discussion";
import { useFormik } from "formik";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useMutateData } from "hocs/useMutateData";
import Loader from "utils/Loader";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import RDiscussionDetails from "view/Courses/Discussion/RDiscussionDetails";

const GDiscussionDetails = () => {
	const params = useParams();
	const discussionId = params.discussionId;

	// - - - - - - - - - - - - - - Formik - - - - - - - - - - - - - -

	// - - - - - - - - - - - - - Queries - - - - - - - - - - - - - -

	const {
		data: discussionByIdData,
		isLoading: isLoadingDiscussion,
		fetchStatus,
	} = useFetchDataRQ({
		queryKey: ["discussionById", discussionId],
		queryFn: () => discussionApi.getDiscussionById(discussionId),
		enableCondition: discussionId ? true : false,
		onSuccessFn: (data) => {
			console.log(data);
		},
	});

	// - - - - - - - - - - - - - Handlers - - - - - - - - - - - - - -

	return (
		<>
			<RFlex>
				{/* - - - - - - - Content - - - - - - - */}
				<RFlex className="flex-col w-[100%] mb-10">
					<RDiscussionDetails data={discussionByIdData} />
				</RFlex>
			</RFlex>
		</>
	);
};

export default GDiscussionDetails;
