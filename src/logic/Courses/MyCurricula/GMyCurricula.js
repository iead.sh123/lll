import React, { useEffect, useState } from "react";
import { getSemestersByOrganizationAsync } from "store/actions/global/schoolInitiation.actions";
import { useDispatch, useSelector } from "react-redux";
import { baseURL, genericPath } from "engine/config";
import { getMyCurriculaAsync } from "store/actions/global/coursesManager.action";
import { SwiperSlide } from "swiper/react";
import { Row, Col } from "reactstrap";
import { Services } from "engine/services";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import HandsPoint from "assets/img/new/Hands_Point.png";
import DefaultImage from "assets/img/new/course-default-cover.png";
import RButton from "components/Global/RComs/RButton";
import styles from "./muCurricula.module.scss";
import Loader from "utils/Loader";
import RCard from "components/Global/RComs/RCards/RCard";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import RSwiper from "components/Global/RComs/RSwiper/RSwiper";
import REmptyData from "components/RComponents/REmptyData";

const GCurricula = ({ advanced = true }) => {
	const dispatch = useDispatch();
	const [searchData, setSearchData] = useState("");
	const [courses, setCourses] = useState();
	const [semesterIdSelected, setSemesterIdSelected] = useState(null);

	const { myCurriculaLoading, myCurricula } = useSelector((state) => state.coursesManagerRed);
	const { semestersLoading, semesters } = useSelector((state) => state.schoolInitiationRed);
	useEffect(() => {
		if (!semesterIdSelected && semesters && semesters.length > 0) {
			setSemesterIdSelected(semesters[0].id);
			handleSelectSemester(semesters[0].id);
		}
	}, [semesters, semesterIdSelected]);
	const { user } = useSelector((state) => state.auth);

	const handleChangeSearch = (text) => {
		setSearchData(text);
	};

	const handleSearch = (emptySearch) => {
		dispatch(getMyCurriculaAsync(emptySearch ?? searchData, semesterIdSelected));
	};

	const handleSelectSemester = (semesterId) => {
		dispatch(getMyCurriculaAsync(searchData, semesterId));
	};

	useEffect(() => {
		dispatch(getSemestersByOrganizationAsync(true));
	}, []);

	useEffect(() => {
		if (myCurricula && myCurricula.length >= 0) {
			buildMyCurricula();
		}
	}, [myCurricula]);

	const buildMyCurricula = () => {
		let courses = [];
		myCurricula?.map((curriculum, index) => {
			let data = {};
			data.image = curriculum.image.hash_id == undefined ? DefaultImage : `${Services.courses_manager.file + curriculum.image.hash_id}`;
			data.icon = curriculum.icon.hash_id == undefined ? undefined : `${Services.courses_manager.file + curriculum.icon.hash_id}`;
			data.id = curriculum.id;
			data.name = curriculum.name;
			data.color = curriculum.color;
			data.currentInfo = curriculum.stats;
			data.link = `${baseURL}/${genericPath}/course-management/curricula/${curriculum.id}/lesson-plans`;
			courses.push(data);
		});
		setCourses(courses);
	};

	const corseCards = courses?.map((course) => (
		<SwiperSlide>
			<RCard course={course} hiddenFlag={true} />
		</SwiperSlide>
	));
	if (!advanced)
		return (
			<>
				{" "}
				{myCurriculaLoading ? (
					<Loader />
				) : (
					<>
						{courses && courses?.length > 0 ? (
							<RFlex styleProps={{ flexWrap: "wrap", justifyContent: "space-between" }}>
								<div style={{ width: "75vw" }}>
									{" "}
									<RSwiper slidesPerView={3} navigation={true}>
										{corseCards}
									</RSwiper>
								</div>
							</RFlex>
						) : (
							<Row className={styles.empty__data}>
								<Col xs={12}>
									<REmptyData
										Image={HandsPoint}
										width={"281px"}
										height={"284px"}
										line1={tr`no_selected_semester`}
										line2={tr`select_a_semester_to_see_its_curricula`}
									/>
								</Col>
							</Row>
						)}
					</>
				)}
			</>
		);
	else
		return (
			<React.Fragment>
				{advanced ? (
					<RFlex
						styleProps={{
							flexWrap: "wrap",
							justifyContent: "space-between",
							marginBottom: "16px",
						}}
					>
						<RFlex styleProps={{ alignItems: "center" }}>
							{semestersLoading ? (
								<Loader />
							) : (
								<section>
									{semesters?.map((semester) => (
										<RButton
											className="btn-round"
											key={semester.id}
											text={semester.name}
											color="primary"
											onClick={() => {
												setSemesterIdSelected(semester.id);
												handleSelectSemester(semester.id);
											}}
											outline={semesterIdSelected == semester.id ? false : true}
										/>
									))}
								</section>
							)}
						</RFlex>
						<RSearchHeader
							searchLoading={myCurriculaLoading}
							searchData={searchData}
							handleSearch={handleSearch}
							setSearchData={setSearchData}
							handleChangeSearch={handleChangeSearch}
							inputPlaceholder={tr`search`}
							addNew={false}
							inputWidth={"30%"}
							disabledOnInput={semesterIdSelected ? false : true}
						/>
					</RFlex>
				) : (
					<></>
				)}
				{myCurriculaLoading ? (
					<Loader />
				) : (
					<React.Fragment>
						{courses && courses?.length > 0 ? (
							<RFlex styleProps={{ flexWrap: "wrap", justifyContent: "space-between" }}>
								{courses.map((course) => (
									<RCard course={course} hiddenFlag={true} />
								))}
							</RFlex>
						) : (
							<Row className={styles.empty__data}>
								<Col xs={12}>
									<REmptyData
										Image={HandsPoint}
										width={"281px"}
										height={"284px"}
										line1={tr`no_selected_semester`}
										line2={tr`select_a_semester_to_see_its_curricula`}
									/>
								</Col>
							</Row>
						)}
					</React.Fragment>
				)}
			</React.Fragment>
		);
};

export default GCurricula;
