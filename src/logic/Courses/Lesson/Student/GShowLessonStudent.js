import React from "react";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RShowLessonStudent from "view/Courses/Lesson/Student/RShowLessonStudent";
const GShowLessonStudent = () => {
	const lesson = {
		id: 1,
		title: "Lesson title",
		checked: false,
		status: "Draft",
		available_at: "01/1/2024",
		heading: "This assignment is very important. go ahead!",
		assignment: {
			title: "Assignment title",
			status: "Not submitted yet",
			availablity: "opens from 1/1/2024 to 6/6/2024",
		},
		discussion: {
			title: "Discussion title",
			status: "new",
			availablity: "opens at 1/1/2024",
		},
	};
	const lessonRecords = [
		{
			id: 1,
			title: "Lesson title",
			checked: false,
			status: "Draft",
			available_at: "01/1/2024",
		},
		{
			id: 2,
			title: "Lesson title",
			checked: true,
			status: "Puplished",
			available_at: "01/1/2024",
		},
		{
			id: 3,
			title: "Lesson title",
			checked: false,
			status: "Draft",
			available_at: "01/1/2024",
		},
		{
			id: 4,
			title: "Lesson title",
			checked: false,
			status: "Draft",
			available_at: "01/1/2024",
		},
		{
			id: 5,
			title: "Lesson title",
			checked: true,
			status: "Puplished",
			available_at: "01/1/2024",
		},
		{
			id: 6,
			title: "Lesson title",
			checked: true,
			status: "Puplished",
			available_at: "01/1/2024",
		},
		{
			id: 7,
			title: "Lesson title",
			checked: false,
			status: "Draft",
			available_at: "01/1/2024",
		},
		{
			id: 8,
			title: "Lesson title",
			checked: false,
			status: "Draft",
			available_at: "01/1/2024",
		},
	];

	return (
		<RFlex className="flex-col">
			<RShowLessonStudent lessonRecords={lessonRecords} lesson={lesson} />
		</RFlex>
	);
};

export default GShowLessonStudent;
