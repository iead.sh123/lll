import React from "react";
import useIsStudent from "hocs/useIsStudent";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RLessonListerStudent from "view/Courses/Lesson/Student/RLessonListerStudent";

const GLessonStudent = () => {
	const userType = useIsStudent();
	// console.log(userType)


	const lessonRecords = [
		{
			id: 1,
			title: "Lesson title",
			checked: false,
			status: "Draft",
			available_at: "01/1/2024",
		},
		{
			id: 2,
			title: "Lesson title",
			checked: true,
			status: "Puplished",
			available_at: "01/1/2024",
		},
		{
			id: 3,
			title: "Lesson title",
			checked: false,
			status: "Draft",
			available_at: "01/1/2024",
		},
		{
			id: 4,
			title: "Lesson title",
			checked: false,
			status: "Draft",
			available_at: "01/1/2024",
		},
		{
			id: 5,
			title: "Lesson title",
			checked: true,
			status: "Puplished",
			available_at: "01/1/2024",
		},
		{
			id: 6,
			title: "Lesson title",
			checked: true,
			status: "Puplished",
			available_at: "01/1/2024",
		},
		{
			id: 7,
			title: "Lesson title",
			checked: false,
			status: "Draft",
			available_at: "01/1/2024",
		},
		{
			id: 8,
			title: "Lesson title",
			checked: false,
			status: "Draft",
			available_at: "01/1/2024",
		},
	];

	return (
		<RFlex className="flex-col w-[70%]">
			<RLessonListerStudent
				data={lessonRecords}
				// data={{ values, info: lessonsData }}
				// loading={{
				// 	lessonsLoading: lessonsData.isLoading,
				// }}
			/>
		</RFlex>
	);
};

export default GLessonStudent;
