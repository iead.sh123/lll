import React, { useRef, useEffect, useState } from "react";
import { useParams, useHistory } from "react-router-dom";
import { useFormik } from "formik";
import useIsStudent from "hocs/useIsStudent";
import useIsMasterCourse from "hocs/useIsMasterCourse";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import RCreateLesson from "view/Courses/Lesson/RCreateLesson";
import RLessonLister from "view/Courses/Lesson/RLessonLister";
import Loader from "utils/Loader";
import * as yup from "yup";

const GLesson = () => {
	const userType = useIsStudent()
	// console.log(userType)
	const { courseId, lessonId } = useParams();
	const isMasterCourse = useIsMasterCourse();
	// const history = useHistory();
	const isAdd = lessonId == "add";

	// - - - - - - - - - - Formik - - - - - - - - - -
	const initialValues = {
		id: !isAdd ? lessonId : undefined,
		courseId: courseId,
		type: isMasterCourse ? "masterCourse" : "course",
		showAddTitle: false,
		title: "",
		checked: false,
		available_at: "",
		lessonRecords: [
			{
				id: 1,
				title: "Lesson title",
				checked: false,
				status: "Draft",
				available_at: "5/5/2024"
			},
			{
				id: 2,
				title: "Lesson title",
				checked: true,
				status: "Puplished",
				available_at: "5/5/2024"
			},
			{
				id: 3,
				title: "Lesson title",
				checked: false,
				status: "Draft",
				available_at: "5/5/2024"
			},
			{
				id: 4,
				title: "Lesson title",
				checked: false,
				status: "Draft",
				available_at: "5/5/2024"
			},
			{
				id: 5,
				title: "Lesson title",
				checked: true,
				status: "Puplished",
				available_at: "5/5/2024"
			},
			{
				id: 6,
				title: "Lesson title",
				checked: true,
				status: "Puplished",
				available_at: "5/5/2024"
			},
			{
				id: 7,
				title: "Lesson title",
				checked: false,
				status: "Draft",
				available_at: "5/5/2024"
			},
			{
				id: 8,
				title: "Lesson title",
				checked: false,
				status: "Draft",
				available_at: "5/5/2024"
			},
		],
	};

	const validationSchema = yup.object().shape({
		title: yup.string().required("lesson title is required"),
	});

	const handleFormSubmit = (values) => {
		const title = values.title;
		const checked = values.checked;
		const available_at = values.available_at
		const id = values.lessonRecords.length + 1;
		values.lessonRecords = [
			{
				id: id,
				title: title,
				checked: checked,
				available_at: available_at,
				status: "Draft",
			},
			...values.lessonRecords,
		];
		// console.log(values);
		// resetForm();
		setFieldValue("showAddTitle", false);
	};

	const { values, touched, errors, handleBlur, handleChange, handleSubmit, setFieldValue, setValues, resetForm } = useFormik({
		initialValues: initialValues,
		validationSchema: validationSchema,
		onSubmit: handleFormSubmit,
	});

	const updateLessonRecords = (updatedRecords) => {
		values.lessonRecords = updatedRecords;
	};

    const handleToggleStatus = () => {
        
    }

	return (
		<>
			<RFlex>
				<RFlex className="flex-col w-[100%] mb-10">
					<RCreateLesson formProperties={{ values, touched, errors, handleBlur, handleChange, handleSubmit, setFieldValue }} />
					<RLessonLister
						data={values}
						handlers={{ updateLessonRecords, handleToggleStatus }}
						// data={{ values, info: discussionsData }}
						// handlers={{
						// 	setFieldValue,
						// 	handleOpenCreateLesson,
						// 	handleRemoveLesson,
						// 	handleChangeStatus,
						// 	handleSearch,
						// }}
						// loading={{
						// 	discussionsLoading: discussionsData.isLoading,
						// }}
					/>
				</RFlex>
			</RFlex>
		</>
	);
};

export default GLesson;
