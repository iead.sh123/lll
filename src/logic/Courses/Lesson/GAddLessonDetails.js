import React from "react";
import { useParams, useHistory } from "react-router-dom";
import useIsMasterCourse from "hocs/useIsMasterCourse";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import Loader from "utils/Loader";
import RAddLessonDetails from "view/Courses/Lesson/AddDetails/RAddLessonDetails";

const GAddLessonDetails = () => {
	const { courseId, lessonId } = useParams();
	const isMasterCourse = useIsMasterCourse();
	const type = isMasterCourse ? "masterCourse" : "course"
	const lesson = {
		id: lessonId,
		title: "Lesson title",
		checked: false,
		status: "Draft",
		available_at: "5/5/2024",
	};
	const lessonRecords = [
		{
			id: 1,
			title: "Lesson title",
			checked: false,
			status: "Draft",
			available_at: "5/5/2024",
		},
		{
			id: 2,
			title: "Lesson title",
			checked: true,
			status: "Puplished",
			available_at: "5/5/2024",
		},
		{
			id: 3,
			title: "Lesson title",
			checked: false,
			status: "Draft",
			available_at: "5/5/2024",
		},
		{
			id: 4,
			title: "Lesson title",
			checked: false,
			status: "Draft",
			available_at: "5/5/2024",
		},
		{
			id: 5,
			title: "Lesson title",
			checked: true,
			status: "Puplished",
			available_at: "5/5/2024",
		},
		{
			id: 6,
			title: "Lesson title",
			checked: true,
			status: "Puplished",
			available_at: "5/5/2024",
		},
		{
			id: 7,
			title: "Lesson title",
			checked: false,
			status: "Draft",
			available_at: "5/5/2024",
		},
		{
			id: 8,
			title: "Lesson title",
			checked: false,
			status: "Draft",
			available_at: "5/5/2024",
		},
	];

	return (
		<>
			<RFlex>
				<RFlex className="flex-col w-[100%] mb-10">
					<RAddLessonDetails type={type} lessonRecords={lessonRecords} lesson={lesson} />
				</RFlex>
			</RFlex>
		</>
	);
};

export default GAddLessonDetails;
