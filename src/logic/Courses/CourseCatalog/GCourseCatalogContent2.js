import React, { useEffect } from "react";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useMutateData } from "hocs/useMutateData";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import { useFormik } from "formik";
import { courseApi } from "api/course";
import RCourseCatalogContent from "view/Courses/CourseCatalog/Content/RCourseCatalogContent";
import RCourseCatalogFilter from "view/Courses/CourseCatalog/Content/RCourseCatalogFilter";
import RCourseCatalogSearch from "view/Courses/CourseCatalog/Content/RCourseCatalogSearch";
import GCreateCourseCatalog from "./GCreateCourseCatalog";
import RControlledDialog from "components/RComponents/RControlledDialog";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import * as yup from "yup";
import RControlledAlertDialog from "components/RComponents/RContolledAlertDialog";
import tr from "components/Global/RComs/RTranslator";

const GCourseCatalogContent2 = ({ courseDepartments }) => {
	const { departmentId } = useParams();

	// - - - - - - - - - - - - - - Formik - - - - - - - - - - - - - -
	const initialValues = {
		masterCourses: {},
		groupingOfSearchFields: { name: "", credit: "", is_published: "", page: 1 },
		switchMode: false,
		showFilter: false,
		showCreateCourse: false,
		alertForDeleteCourse: false,
		masterCourseId: null,
		page: 1,
		name: "",
		credit: null,
		isAlertDialogOpen: false,
		courseDetails: {},
	};
	const validationSchema = yup.object().shape({ credit: yup.number().positive("To score be a positive number") });

	const { values, touched, errors, setFieldValue } = useFormik({
		initialValues: initialValues,
		validationSchema,
	});

	useEffect(() => {
		if (departmentId) {
			setFieldValue("groupingOfSearchFields.department", departmentId == "uncategorized" ? 0 : departmentId);
		} else {
			setFieldValue("groupingOfSearchFields.department", undefined);
		}
	}, [departmentId]);

	// - - [ Master Courses ] - - - - - - - - - - - Queries - - - - - - - - - - - - - -

	const masterCoursesData = useFetchDataRQ({
		queryKey: ["masterCourses", values.groupingOfSearchFields],
		queryFn: () => courseApi.masterCourses(values.groupingOfSearchFields),

		onSuccessFn: ({ data }) => {
			setFieldValue("masterCourses", data?.data);
		},
	});

	const deleteMasterCourseMutate = useMutateData({
		queryFn: (departmentId) => courseApi.deleteMasterCourse(departmentId),
		invalidateKeys: [["masterCourses", values.groupingOfSearchFields], ["courseDepartments"]],
		multipleKeys: true,
		displaySuccess: true,
	});

	const changeCourseStatusMutate = useMutateData({
		queryFn: ({ masterCourseId, data }) => courseApi.updateMasterCourse({ masterCourseId, data }),
		onSuccessFn: ({ data, variables }) => {
			const ind = values.masterCourses?.data.findIndex((course) => course.id == variables.masterCourseId);
			if (ind !== -1) {
				setFieldValue(`masterCourses.data[${ind}].is_published`, variables.data.is_published);
			}
		},
	});

	// - - - - - - - - - - - - - - Open Modals - - - - - - - - - - - - - -

	const handleOpenCreateCourse = (id) => {
		setFieldValue("showCreateCourse", true);
		setFieldValue("masterCourseId", id);
	};
	const handleCloseCreateCourse = (id) => {
		setFieldValue("showCreateCourse", false);
		setFieldValue("masterCourseId", id);
	};

	// - - - - - - - - - - - - - Handlers - - - - - - - - - - - - - -
	const handleSwitchBetweenTowMode = () => {
		setFieldValue("switchMode", !values.switchMode);
	};

	const handleShowFilter = () => {
		setFieldValue("showFilter", !values.showFilter);
	};

	const handleSearch = (emptyData) => {
		setFieldValue("groupingOfSearchFields.name", emptyData !== undefined ? "" : values?.name);
	};

	const handleResetFilter = () => {
		setFieldValue("groupingOfSearchFields.credit", "");
		handleShowFilter();
	};

	const handleRemoveCourse = (MasterCourseId) => {
		deleteMasterCourseMutate.mutate(MasterCourseId);
	};

	const handleChangeStatus = ({ masterCourseId, status }) => {
		changeCourseStatusMutate.mutate({ masterCourseId: masterCourseId, data: { is_published: status } });
	};

	const handleOpenDialogToDeleteCourse = (courseDetails) => {
		setFieldValue("isAlertDialogOpen", true);
		setFieldValue("courseDetails", courseDetails);
	};

	return (
		<React.Fragment>
			<RControlledAlertDialog
				title={`${tr("are_you_sure_you_want_to_delete")} ${values.courseDetails.name}`}
				isOpen={values.isAlertDialogOpen}
				headerItemsPosition="items-start"
				handleCloseAlert={() => setFieldValue("isAlertDialogOpen", false)}
				confirmAction={() => handleRemoveCourse(values.courseDetails.id)}
			/>

			{/* - - - Create Course- - - - Modals - - - - - - - */}

			<RControlledDialog
				isOpen={values.showCreateCourse}
				closeDialog={handleCloseCreateCourse}
				dialogBody={
					<GCreateCourseCatalog
						data={{ courseDepartments, masterCourseId: values.masterCourseId }}
						handlers={{ handleCloseCreateCourse }}
					/>
				}
				contentClassName="max-w-[450px]"
			/>

			{/* - - - - - - - Content - - - - - - - */}
			<RFlex className="flex-col w-[100%] pl-2">
				<RCourseCatalogSearch
					data={{ values }}
					handlers={{ setFieldValue, handleOpenCreateCourse, handleSearch }}
					loading={{ masterCoursesLoading: masterCoursesData.isLoading }}
				/>
				<RCourseCatalogFilter
					data={{ values, touched, errors }}
					handlers={{
						setFieldValue,
						handleSwitchBetweenTowMode,
						handleShowFilter,
						handleResetFilter,
						handleSearch,
					}}
				/>

				<RCourseCatalogContent
					data={{ values, info: masterCoursesData, changeCourseStatusMutate }}
					handlers={{
						setFieldValue,
						handleOpenCreateCourse,
						handleRemoveCourse,
						handleChangeStatus,
						handleSearch,
						handleOpenDialogToDeleteCourse,
					}}
					loading={{
						masterCoursesLoading: masterCoursesData.isLoading,
						deleteMasterCourseLoading: deleteMasterCourseMutate.isLoading,
						changeCourseStatusLoading: changeCourseStatusMutate.isLoading,
					}}
				/>
			</RFlex>
		</React.Fragment>
	);
};

export default GCourseCatalogContent2;
