import React from "react";
import GCourseCatalogContent2 from "./GCourseCatalogContent2";
import RCourseDepartments from "view/Courses/CourseCatalog/Department/RCourseDepartments";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { deleteSweetAlert } from "components/Global/RComs/RAlert2";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useMutateData } from "hocs/useMutateData";
import { courseApi } from "api/course";
import { useFormik } from "formik";

const GCoursesCatalog2 = () => {
	// - - - - - - - - - - - - - - Formik - - - - - - - - - - - - - -
	const initialValues = {
		courseDepartments: [],
		groupingOfSearchFields: {},
		nonDepartmentCount: 0,
		allCourseCount: 0,
		level: 0,
		department: { id: null, name: "" },
		addDepartment: false,
		alertForDeleteDepartment: false,
	};

	const { values, setFieldValue } = useFormik({
		initialValues: initialValues,
	});

	// - - [Department] - - - - - - - - - - - Queries - - - - - - - - - - - - - -
	const courseDepartmentsData = useFetchDataRQ({
		queryKey: ["courseDepartments"],
		queryFn: () => courseApi.courseDepartments(),
		onSuccessFn: ({ data }) => {
			setFieldValue("courseDepartments", data?.data?.department);
			setFieldValue("nonDepartmentCount", data?.data["non-department-count"]);
			setFieldValue("allCourseCount", data?.data["all-courses-count"]);
		},
	});

	const createDepartmentMutate = useMutateData({
		queryFn: (data) => courseApi.createDepartment(data),
		onSuccessFn: ({ data }) => {
			setFieldValue("courseDepartments", [...values?.courseDepartments, data?.data]);
			setFieldValue("department", { id: null, name: "" });
			setFieldValue("addDepartment", false);
		},
	});

	const updateDepartmentMutate = useMutateData({
		queryFn: ({ departmentId, data }) => courseApi.updateDepartment({ departmentId, data }),
		onSuccessFn: ({ data, variables }) => {
			let updateData = [];
			updateData = values?.courseDepartments.map((item) => {
				if (item.id === variables.departmentId) {
					return { ...item, name: variables.data?.name };
				}
				return item;
			});
			setFieldValue("courseDepartments", updateData);
		},
	});

	const deleteDepartmentMutate = useMutateData({
		queryFn: (departmentId) => courseApi.deleteDepartment(departmentId),
		onSuccessFn: ({ data, variables }) => {
			const updateData = values?.courseDepartments.filter((item) => item.id !== variables);
			setFieldValue("courseDepartments", updateData);
		},
	});

	// - - [Master Courses] - - - - - - - - - - - Queries - - - - - - - - - - - - - -

	// - - - - - - - - - - - - - - Open Modals - - - - - - - - - - - - - -
	const handleOpenAddDepartment = () => setFieldValue("addDepartment", true);
	const handleCloseAddDepartment = () => setFieldValue("addDepartment", false);

	const hideAlert = () => setFieldValue("alertForDeleteDepartment", null);
	const showAlerts = (child) => setFieldValue("alertForDeleteDepartment", child);

	// - - - - - - - - - - - - - - handlers - - - - - - - - - - - - - -
	const handleChangeDepartment = ({ id, value }) => {
		setFieldValue("department.id", id);
		setFieldValue("department.name", value);
	};

	const handleAddDepartment = (departmentId) => {
		if (values.department.name.length > 1) {
			if (departmentId) {
				updateDepartmentMutate.mutate({ departmentId: departmentId, data: values.department });
			} else {
				createDepartmentMutate.mutate(values.department);
			}
		}
	};

	const successDelete = (parameters) => {
		deleteDepartmentMutate.mutate(parameters.departmentId);
	};

	const handleRemoveDepartment = (departmentId) => {
		const message = tr`Are you Sure ? This can't be undone!`;
		const confirm = tr`Yes, delete it`;
		deleteSweetAlert(showAlerts, hideAlert, successDelete, { departmentId }, message, confirm);
	};
	return (
		<React.Fragment>
			{values.alertForDeleteDepartment}

			<RFlex className="w-[100%] gap-0 flex-wrap">
				<RCourseDepartments
					data={{ values }}
					handlers={{
						handleOpenAddDepartment,
						handleCloseAddDepartment,
						handleChangeDepartment,
						handleAddDepartment,
						handleRemoveDepartment,
					}}
					loading={{
						courseDepartmentsLoading: courseDepartmentsData.isLoading,
						addEditDepartmentLoading: createDepartmentMutate.isLoading || updateDepartmentMutate.isLoading,
						deleteDepartmentLoading: deleteDepartmentMutate.isLoading,
					}}
				/>
				<RFlex className="h-[88vh] gap-[5px] overflow-auto no-scrollbar w-[85%]">
					<GCourseCatalogContent2 courseDepartments={values.courseDepartments} />
				</RFlex>
			</RFlex>
			{/* <RTable /> */}
		</React.Fragment>
	);
};

export default GCoursesCatalog2;
