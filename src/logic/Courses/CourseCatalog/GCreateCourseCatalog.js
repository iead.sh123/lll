import React from "react";
import RCreateCourseCatalog from "view/Courses/CourseCatalog/Content/RCreateCourseCatalog";
import Loader from "utils/Loader";
import tr from "components/Global/RComs/RTranslator";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useMutateData } from "hocs/useMutateData";
import { courseApi } from "api/course";
import { useFormik } from "formik";
import * as yup from "yup";

const GCreateCourseCatalog = ({ data, handlers }) => {
	// - - - - - - - - - - - - - - Queries - - - - - - - - - - - - - -
	const {
		data: courseByIdData,
		isLoading,
		fetchStatus,
	} = useFetchDataRQ({
		queryKey: ["courseById", data.masterCourseId],
		queryFn: () => courseApi.getMasterCourseById(data.masterCourseId),
		enableCondition: data.masterCourseId ? true : false,
		onSuccessFn: (data) => {
			//Because i need to set values to use in edit mode
			setValues({
				name: data?.data?.data?.name,
				code: data?.data?.data?.code,
				credit: data?.data?.data?.credit,
				hour: data?.data?.data?.hour,
				description: data?.data?.data?.description,
				tags: data?.data?.data?.tags,
				semester_length: data?.data?.data?.semester_length,
				max_class_size: data?.data?.data?.max_class_size,
				methods_of_instruction: data?.data?.data?.methods_of_instruction,
				course_designation: data?.data?.data?.course_designation,
				industry_designation: data?.data?.data?.industry_designation,
				typically_offered: data?.data?.data?.typically_offered,
				department: [{ label: data?.data?.data?.department_name, value: data?.data?.data?.department }],
				image: data?.data?.data?.image_url ? [{ image_url: data?.data?.data?.image_url }] : [],
			});
		},
	});

	const createCourseMutate = useMutateData({
		queryFn: ({ templateId, data }) => courseApi.createMasterCourse({ templateId, data }),
		multipleKeys: true,
		invalidateKeys: [["masterCourses"], ["courseDepartments"]],
		onSuccessFn: () => {
			handlers.handleCloseCreateCourse(null);
		},
	});

	const updateCourseMutate = useMutateData({
		queryFn: ({ masterCourseId, data }) => courseApi.updateMasterCourse({ masterCourseId, data }),
		multipleKeys: true,
		invalidateKeys: [["masterCourses"], ["courseDepartments"]],
		onSuccessFn: () => {
			handlers.handleCloseCreateCourse(null);
		},
	});

	// - - - - - - - - - - - - - - Formik - - - - - - - - - - - - - -
	const initialValues = {
		name: "",
		code: "",
		credit: null,
		hour: null,
		description: "",
		tags: [],
		department: [],
		image: [],
		semester_length: "",
		max_class_size: null,
		methods_of_instruction: null,
		course_designation: null,
		industry_designation: null,
		typically_offered: null,
	};

	const validationSchema = yup.object({
		name: yup.string().required(tr("name_is_required")),
		code: yup.string().required(tr("code_is_required")),
		credit: yup.number().required().positive("Credit should be a positive number"),
		hour: yup.number().required().positive("Hour should be a positive number"),
		description: yup.string().required(tr("description_is_required")),
	});

	const handleFormSubmit = (values, resetForm) => {
		if (data.masterCourseId) {
			updateCourseMutate.mutate({
				masterCourseId: courseByIdData?.data?.data.id,
				data: { ...values, department: values.department?.length > 0 ? values.department[0]?.value : values.department?.value },
			});
		} else {
			createCourseMutate.mutate({ templateId: 1, data: { ...values, department: values.department?.value } });
		}
		resetForm();
	};

	const { values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue, setValues, resetForm } = useFormik({
		initialValues,
		onSubmit: handleFormSubmit,
		validationSchema,
	});

	if (isLoading && fetchStatus !== "idle") return <Loader />;

	return (
		<RCreateCourseCatalog
			data={{ courseDepartments: data.courseDepartments, masterCourseId: data.masterCourseId }}
			formProperties={{
				values,
				setFieldValue,
				resetForm,
				handleBlur,
				handleChange,
				touched,
				errors,
			}}
			handlers={{
				handleSubmit,
				handleCloseCreateCourse: handlers.handleCloseCreateCourse,
			}}
			loading={{
				createCourseLoading: createCourseMutate.isLoading || updateCourseMutate.isLoading,
			}}
		/>
	);
};

export default GCreateCourseCatalog;
