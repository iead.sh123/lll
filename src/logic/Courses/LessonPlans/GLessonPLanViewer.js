import React from "react";
import useIsMasterCourse from "hocs/useIsMasterCourse";
import RLessonPLanViewer from "view/Courses/LessonPlan/RLessonPLanViewer";
import RRubricHeader from "view/Courses/CourseManagement/Rubric/RRubricHeader";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { useParams, useHistory } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useMutateData } from "hocs/useMutateData";
import { lessonPlanApi } from "api/lessonPLan";
import RLessonPLanHeader from "view/Courses/LessonPlan/RLessonPLanHeader";

const GLessonPLanViewer = () => {
	const { courseId, lessonPlanId } = useParams();
	const history = useHistory();
	const isMasterCourse = useIsMasterCourse();

	const { data, isLoading, isFetching, isError } = useFetchDataRQ({
		queryKey: ["lessonPlanViewer", lessonPlanId],
		queryFn: () => lessonPlanApi.getLessonPlan(lessonPlanId),
	});

	const exportLessonPLanAsPdfMutate = useMutateData({
		queryFn: () => lessonPlanApi.exportAsPdf(lessonPlanId),
		downloadFile: true,
	});

	const handlePushToLessonPlanEditor = () => {
		history.push(
			`${baseURL}/${genericPath}/${
				isMasterCourse
					? `course-catalog/master-course/${courseId}/lesson-plan/${lessonPlanId}`
					: `course-management/course/${courseId}/lesson-plan/${lessonPlanId}`
			}`
		);
	};

	const handleExportLessonPlanAsPdf = (lessonPlanId) => {
		exportLessonPLanAsPdfMutate.mutate(lessonPlanId);
	};

	if (isLoading) return <Loader />;
	return (
		<RFlex className="flex-col">
			<RLessonPLanHeader
				handlers={{ handlePushToLessonPlanEditor, handleExportLessonPlanAsPdf }}
				loading={{ exportLessonPlanAsPdfLoading: exportLessonPLanAsPdfMutate.isLoading }}
			/>
			<RLessonPLanViewer lessonPlanData={data?.data?.data} />
		</RFlex>
	);
};

export default GLessonPLanViewer;
