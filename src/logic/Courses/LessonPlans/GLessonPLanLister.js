import React from "react";
import { useHistory, useParams } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useMutateData } from "hocs/useMutateData";
import { lessonPlanApi } from "api/lessonPLan";
import { useFormik } from "formik";
import { Button } from "ShadCnComponents/ui/button";
import useIsMasterCourse from "hocs/useIsMasterCourse";
import DateUtcWithTz from "utils/dateUtcWithTz";
import RSearchInput from "components/RComponents/RSearchInput";
import RDropdown from "components/RComponents/RDropdown/RDropdown";
import iconsFa6 from "variables/iconsFa6";
import RAddTags from "components/RComponents/RAddTags";
import RNewTabs from "components/RComponents/RNewTabs";
import RLister from "components/Global/RComs/RLister";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const GLessonPLanLister = () => {
	const { courseId } = useParams();
	const history = useHistory();
	const isMasterCourse = useIsMasterCourse();

	const handlePushToLessonPlanEditor = (rId) => {
		if (rId) {
			history.push(
				`${baseURL}/${genericPath}/${
					isMasterCourse
						? `course-catalog/master-course/${courseId}/lesson-plan/${rId}`
						: `course-management/course/${courseId}/lesson-plan/${rId}`
				}`
			);
		} else {
			history.push(
				`${baseURL}/${genericPath}/${
					isMasterCourse
						? `course-catalog/master-course/${courseId}/lesson-plan/add`
						: `course-management/course/${courseId}/lesson-plan/add`
				}`
			);
		}
	};

	const handlePushToLessonPlanViewer = (rId) => {
		history.push(
			`${baseURL}/${genericPath}/${
				isMasterCourse
					? `course-catalog/master-course/${courseId}/lesson-plan/${rId}/view`
					: `course-management/course/${courseId}/lesson-plan/${rId}/view`
			}`
		);
	};

	// - - - - - - - - - - - - - - Formik - - - - - - - - - - - - - -
	const initialValues = {
		lessonPLans: [],
		groupingOfSearchFields: {
			context_id: courseId,
			context_type: isMasterCourse ? "masterCourse" : "course",
			page: 1,
			published: undefined,
			filter: undefined,
			recently: 1,
			alphabetically: undefined,
		},
		activeTab: "recent",
		searchText: "",
	};

	const { values, setFieldValue } = useFormik({
		initialValues: initialValues,
	});

	// - - - - - - - - - - Handlers - - - - - - - - - -

	const handleFilterOnStatus = (status) => {
		setFieldValue("groupingOfSearchFields.published", status);
	};

	const handleChangePage = (page) => {
		setFieldValue("groupingOfSearchFields.page", page);
	};

	const handleChangeSearch = (text) => {
		setFieldValue("searchText", text);
	};

	const handleSearch = (emptySearch) => {
		setFieldValue("groupingOfSearchFields.filter", emptySearch !== undefined ? undefined : values.searchText);
	};

	const handelDeleteLessonPlan = (lessonPlanId) => {
		deleteLessonPlanMutate.mutate(lessonPlanId);
	};

	const handleChangeStatus = (data) => {
		const changeStatusData = {
			id: data.lessonPlanId,
			context_id: courseId,
			context_type: isMasterCourse ? "masterCourse" : "course",
			published: !data.status,
		};
		changeLessonPLanStatusMutate.mutate(changeStatusData);
	};

	const handleFilterOnTabs = (data) => {
		if (data == "recent") {
			setFieldValue("activeTab", "recent");
			setFieldValue("groupingOfSearchFields.recently", 1);
			setFieldValue("groupingOfSearchFields.alphabetically", undefined);
		} else {
			setFieldValue("activeTab", "alphabetically");
			setFieldValue("groupingOfSearchFields.alphabetically", 1);
			setFieldValue("groupingOfSearchFields.recently", undefined);
		}
	};

	// - - - - - - - - - - - - - Queries - - - - - - - - - - - - - -
	const lessonPLansData = useFetchDataRQ({
		queryKey: ["lessonPLans", values.groupingOfSearchFields],
		queryFn: () => lessonPlanApi.getLessonPlans(values.groupingOfSearchFields),
		onSuccessFn: ({ data }) => {
			setFieldValue("lessonPLans", data?.data?.data);
		},
	});

	const deleteLessonPlanMutate = useMutateData({
		queryFn: (lessonPlanId) => lessonPlanApi.deleteLessonPlan(lessonPlanId),
		onSuccessFn: ({ data, variables }) => {
			const updateData = values?.lessonPLans.filter((item) => item.id !== variables);
			setFieldValue("lessonPLans", updateData);
		},
	});

	const changeLessonPLanStatusMutate = useMutateData({
		queryFn: (data) => lessonPlanApi.create(data),
		onSuccessFn: ({ data, variables }) => {
			const ind = values.lessonPLans.findIndex((lessonPlan) => +lessonPlan.id == +variables.id);
			if (ind !== -1) {
				setFieldValue(`lessonPLans[${ind}].published`, variables.published);
			}
		},
	});

	const _recordLessonPlans = {
		data: values.lessonPLans,

		columns: [
			{
				accessorKey: "name",
				renderHeader: () => <span>{tr`title`}</span>,
				renderCell: ({ row }) => (
					<RFlex>
						<span className="cursor-pointer hover:text-themePrimary" onClick={() => handlePushToLessonPlanEditor(row.original?.id)}>
							{row.original?.name}
						</span>
						{row.original?.tags?.length > 0 && <RAddTags dataFromBackend={row.original?.tags} disabled={true} />}
					</RFlex>
				),
			},

			{
				accessorKey: "created_at",
				renderHeader: () => <span>{tr`creation_date`}</span>,
				renderCell: ({ row }) =>
					DateUtcWithTz({
						dateTime: row.original?.created_at,
						dateFormate: "L",
					}),
			},

			{
				accessorKey: "status",
				renderHeader: (info) => (
					<RFlex className="items-center">
						<span>{tr`status`}</span>
						<RDropdown
							actions={[
								{ name: "all", onClick: () => handleFilterOnStatus(undefined) },
								{ name: "publish", onClick: () => handleFilterOnStatus("1") },
								{ name: "draft", onClick: () => handleFilterOnStatus("0") },
							]}
							TriggerComponent={
								<button>
									<i className={iconsFa6.filter + " text-xs"} aria-hidden="true" />
								</button>
							}
						/>
					</RFlex>
				),
				renderCell: ({ row }) => (
					<React.Fragment>
						{changeLessonPLanStatusMutate.isLoading && changeLessonPLanStatusMutate?.variables.id == row.original.id ? (
							<i className={iconsFa6.spinner} alt="spinner" />
						) : (
							<span
								className={`${row.original?.published ? "text-themeSuccess" : "text-themeDanger"} cursor-pointer`}
								onClick={() => handleChangeStatus({ lessonPlanId: row.original?.id, status: row.original?.published })}
							>
								{row.original?.published ? tr`published` : tr`draft`}
							</span>
						)}
					</React.Fragment>
				),
			},
		],

		actions: [
			{
				icon: iconsFa6.pdf,
				actionIconClass: "text-themePrimary",
				onClick: ({ row }) => handlePushToLessonPlanViewer(row.original.id),
			},
			{
				icon: iconsFa6.delete,
				actionIconClass: "text-themeDanger",
				confirmAction: ({ row }) => handelDeleteLessonPlan(row.original.id),
				inDialog: true,
				dialogTitle: ({ row }) => `${tr`are_you_sure_you_want_to_delete`} ${row?.original?.name}`,
				dialogDescription: ({ row }) => `${tr`this_action_cannot_be_undone`}`,
				headerItemsPosition: "items-start",
			},
		],
	};

	return (
		<RFlex className="flex-col">
			<RFlex className="justify-between">
				<RFlex className="items-center">
					<Button
						onClick={() => {
							handlePushToLessonPlanEditor();
						}}
					>{tr`new_lesson_plan`}</Button>
					<RSearchInput
						searchLoading={lessonPLansData.isLoading}
						searchData={values.searchText}
						handleSearch={handleSearch}
						setSearchData={handleChangeSearch}
						handleChangeSearch={handleChangeSearch}
					/>
				</RFlex>
				<RNewTabs
					tabs={[
						{ title: "recent", value: "recent" },
						{ title: "alphabetically", value: "alphabetically" },
					]}
					setActiveTab={(data) => handleFilterOnTabs(data)}
					value={values.activeTab}
				/>
			</RFlex>
			{lessonPLansData.isLoading || lessonPLansData.isFetching ? (
				<Loader />
			) : (
				<RLister
					Records={_recordLessonPlans}
					convertRecordsShape={false}
					info={lessonPLansData}
					withPagination={true}
					handleChangePage={(event) => handleChangePage(event)}
					page={values.groupingOfSearchFields.page}
					line1={tr`no_lesson_plan_yet`}
				/>
			)}
		</RFlex>
	);
};

export default GLessonPLanLister;
