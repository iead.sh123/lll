import React from "react";
import { useParams, useHistory } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useMutateData } from "hocs/useMutateData";
import { lessonPlanApi } from "api/lessonPLan";
import { useFormik } from "formik";
import RLessonPLanEditor from "view/Courses/LessonPlan/RLessonPLanEditor";
import useIsMasterCourse from "hocs/useIsMasterCourse";
import Loader from "utils/Loader";
import * as yup from "yup";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import iconsFa6 from "variables/iconsFa6";
import tr from "components/Global/RComs/RTranslator";

export const RubricContext = React.createContext();

const GLessonPLanEditor = () => {
	const { courseId, lessonPlanId } = useParams();
	const isMasterCourse = useIsMasterCourse();
	const history = useHistory();
	const isAdd = lessonPlanId == "add";

	// - - - - - - - - - - - - - Queries - - - - - - - - - - - - - -
	const { data, isLoading, isFetching, fetchStatus } = useFetchDataRQ({
		queryKey: ["lessonPLan", lessonPlanId],
		queryFn: () => lessonPlanApi.getLessonPlan(lessonPlanId),
		enableCondition: !isAdd ? true : false,
		onSuccessFn: ({ data }) => {
			setValues({
				id: !isAdd ? lessonPlanId : undefined,
				context_id: courseId,
				context_type: isMasterCourse ? "masterCourse" : "course",
				name: data.data?.name,
				description: data.data?.description,
				lessons: data.data?.lessons,
				tags: data.data?.tags,
			});
		},
	});

	const addOrEditLessonPLanMutation = useMutateData({
		queryFn: (date) => lessonPlanApi.create(date),
		invalidateKeys: ["addLessonPlan"],
		displaySuccess: true,
		onSuccessFn: ({ data, variables }) => {
			history.push(
				`${baseURL}/${genericPath}/${
					isMasterCourse ? `course-catalog/master-course/${courseId}/lesson-plans` : `course-management/course/${courseId}/lesson-plans`
				}`
			);
		},
	});

	// - - - - - - - - - - Handlers - - - - - - - - - -
	const handleFormSubmit = (values) => {
		addOrEditLessonPLanMutation.mutate(values);
	};

	// - - - - - - - - - - Formik - - - - - - - - - -
	const initialValues = {
		id: !isAdd ? lessonPlanId : undefined,
		context_id: courseId,
		context_type: isMasterCourse ? "masterCourse" : "course",
		name: (!isAdd && data && data.data && data.data.data?.name) || "",
		description: (!isAdd && data && data.data && data.data.data?.description) || "",
		lessons: (!isAdd && data && data.data && data.data.data?.lessons) || [],
		tags: (!isAdd && data && data.data && data.data.data?.tags) || [],
	};

	const validationSchema = yup.object().shape({
		name: yup.string().required("name is required"),
	});

	const { values, touched, errors, handleBlur, handleChange, handleSubmit, setFieldValue, setValues } = useFormik({
		initialValues: initialValues,
		validationSchema: validationSchema,
		onSubmit: handleFormSubmit,
	});

	if ((isLoading && fetchStatus !== "idle") || isFetching) return <Loader />;

	return (
		<div className="flex flex-col gap-5">
			<RFlex>
				<div
					className="w-[22px] h-[22px] bg-themeLightBlue rounded-full flex items-center justify-center cursor-pointer"
					onClick={() => history.goBack()}
				>
					<i className={iconsFa6.chevronLeft + " fa-sm text-themePrimary "} alt="cursor" />
				</div>
				<span>{isAdd ? tr`create_lesson_plan` : tr`edit_lesson_plan`}</span>
			</RFlex>
			<RLessonPLanEditor
				formProperties={{ values, touched, errors, handleBlur, handleChange, handleSubmit, setFieldValue }}
				loading={{ addOrEditLessonPLanLoading: addOrEditLessonPLanMutation.isLoading }}
			/>
		</div>
	);
};

export default GLessonPLanEditor;
