import RButton from 'components/Global/RComs/RButton';
import tr from 'components/Global/RComs/RTranslator';
import React from 'react';
import { Table, Container } from 'reactstrap';
import RModal from 'components/Global/RComs/RModal';
import styles from './EventDetails.module.scss';

const EventDetails = ({ event, showComments, showNotes, isOpen, toggleModal }) => {
    return <RModal
        isOpen={isOpen}
        toggle={() => toggleModal()}
        header={<h6 className="text-capitalize">Details</h6>}
        body={
            <Container>
                <div className='d-flex justify-content-center mb-4 ' style={{borderBottom: '1px solid #000'}}>
                    <h6  onClick={() => showComments(event.id)} className={["text-capitalize pl-5" , styles.HeaderItem ].join(' ')} style={{borderRight: '1px solid #000'}}>comments</h6>

                    <h6 onClick={() => showNotes(event.id)} className={["text-capitalize pr-5" , styles.HeaderItem ].join(' ')} style={{borderLeft: '1px solid #000'}} >notes</h6>
                    {/* <RButton onClick={() => showComments(event.id)} text={tr`comments`}  outline/>
                    <RButton onClick={() => showNotes(event.id)} text={tr`notes`} outline /> */}
                </div>

                <Table striped responsive >
                    <tbody>
                        <tr>
                            <td>{tr`type`}</td>
                            <td>{event.eventTypeName}</td>
                        </tr>
                        <tr>
                            <td>{tr`name`}</td>
                            <td>{event.title}</td>
                        </tr>
                        <tr>
                            <td>{tr`start`}</td>
                            <td>{event.start}</td>
                        </tr>
                        <tr>
                            <td>{tr`end`}</td>
                            <td>{event.end}</td>
                        </tr>
                    </tbody>
                </Table>
            </Container>
        }
        //  footer={<CommentsAndNotesInput handleAdd={handleAdd} />}
        size="lg"
    />
}

export default EventDetails;