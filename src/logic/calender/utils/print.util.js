const daysOfWeek = {
    0: 'sun',
    1: 'mon',
    2: 'tue',
    3: 'wed',
    4: 'thur',
    5: 'fri',
    6: 'sat'
}

export const createPrintPayload = events => {
    return {
        configuration: {
            "mode": "UTF-8",
            "format": [
                190,
                400
            ],
            "margin_left": 10,
            "margin_right": 10,
            "margin_top": 10,
            "margin_bottom": 0,
            "margin_header": 10,
            "margin_footer": 10,
            "font_size": 15,
            "font_family": "Arial"
        },
        "data": {
            "title": "Calender",
            "headers": null,
            "footer": null,
            "topHeader": null,
            pages: [
                {
                    sections: [
                        {
                            "title": "Calender",
                            "type": "GeneralTable",
                            "data": {
                                "headers": [
                                    {
                                        "value": "Type",
                                        "properties": {
                                            "color": "#2C2C2C",
                                            "width": "30%",
                                            "font-weight": "800",
                                            "text-align": "left",
                                            "font-size": "20px"
                                        }
                                    },
                                    {
                                        "value": "Title",
                                        "properties": {
                                            "color": "#2C2C2C",
                                            "width": "30%",
                                            "font-weight": "800",
                                            "text-align": "left",
                                            "font-size": "20px"
                                        }
                                    },
                                    {
                                        "value": "from",
                                        "properties": {
                                            "color": "#2C2C2C",
                                            "width": "30%",
                                            "font-weight": "800",
                                            "text-align": "left",
                                            "font-size": "20px"
                                        }
                                    },
                                    {
                                        "value": "to",
                                        "properties": {
                                            "color": "#2C2C2C",
                                            "width": "30%",
                                            "font-weight": "800",
                                            "text-align": "left",
                                            "font-size": "20px"
                                        }
                                    },
                                    {
                                        "value": "start",
                                        "properties": {
                                            "color": "#2C2C2C",
                                            "width": "30%",
                                            "font-weight": "800",
                                            "text-align": "left",
                                            "font-size": "20px"
                                        }
                                    },
                                    {
                                        "value": "end",
                                        "properties": {
                                            "color": "#2C2C2C",
                                            "width": "30%",
                                            "font-weight": "800",
                                            "text-align": "left",
                                            "font-size": "20px"
                                        }
                                    },
                                    {
                                        "value": "repeat",
                                        "properties": {
                                            "color": "#2C2C2C",
                                            "width": "30%",
                                            "font-weight": "800",
                                            "text-align": "left",
                                            "font-size": "20px"
                                        }
                                    },
                                    {
                                        "value": "days of week",
                                        "properties": {
                                            "color": "#2C2C2C",
                                            "width": "30%",
                                            "font-weight": "800",
                                            "text-align": "left",
                                            "font-size": "20px"
                                        }
                                    }
                                ],
                                "body": events.map(event => {
                                    return [
                                        {
                                            value: event.eventTypeName,
                                            "properties": {
                                                "color": event.color,
                                                "width": "30%",
                                                "font-weight": "800",
                                                "text-align": "left",
                                                "font-size": "20px"
                                            }

                                        },
                                        {
                                            value: event.title,
                                            "properties": {
                                                "color": event.color,
                                                "width": "30%",
                                                "font-weight": "800",
                                                "text-align": "left",
                                                "font-size": "20px"
                                            }

                                        },
                                        {
                                            value: event.from,
                                            "properties": {
                                                "color": event.color,
                                                "width": "30%",
                                                "font-weight": "800",
                                                "text-align": "left",
                                                "font-size": "20px"
                                            }

                                        },
                                        {
                                            value: event.to,
                                            "properties": {
                                                "color": event.color,
                                                "width": "30%",
                                                "font-weight": "800",
                                                "text-align": "left",
                                                "font-size": "20px"
                                            }

                                        },
                                        {
                                            value: event.start,
                                            "properties": {
                                                "color": event.color,
                                                "width": "30%",
                                                "font-weight": "800",
                                                "text-align": "left",
                                                "font-size": "20px"
                                            }

                                        },
                                        {
                                            value: event.end,
                                            "properties": {
                                                "color": event.color,
                                                "width": "30%",
                                                "font-weight": "800",
                                                "text-align": "left",
                                                "font-size": "20px"
                                            }

                                        },
                                        {
                                            value: event.repeat,
                                            "properties": {
                                                "color": event.color,
                                                "width": "30%",
                                                "font-weight": "800",
                                                "text-align": "left",
                                                "font-size": "20px"
                                            }

                                        },
                                        {
                                            value: event.repeat === 'WEEKLY' && event?.daysOfWeek?.map(dn => daysOfWeek[dn]).join(','),
                                            "properties": {
                                                "color": event.color,
                                                "width": "30%",
                                                "font-weight": "800",
                                                "text-align": "left",
                                                "font-size": "20px"
                                            }

                                        },
                                    ]
                                })
                                ,
                                "properties": {
                                    "width": "80%",
                                    "border": "1px solid #dee2e6"
                                }
                            }
                        },
                    ],
                    "headers": [],
                    "footer": null,
                    "topHeader": null
                }
            ]
        },
        name: "calender"

    }
}



