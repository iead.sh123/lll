import React, { useState } from "react";
import { Container } from "reactstrap";
import Loader from "utils/Loader";
import RCalender from "view/Calender/RCalender";
import { calenderApi } from "api/calender";
import GPrintCalender from "./Print/GPrintCalender";
import { toast } from "react-toastify";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { Terms } from "./constants";
import RLoader from "components/Global/RComs/RLoader";
import { useFormik } from "formik";
import * as Yup from "yup";
import tr from "components/Global/RComs/RTranslator";
import { useMutateData } from "hocs/useMutateData";
import { useParams } from "react-router-dom";
import AppModal from "components/Global/ModalCustomize/AppModal";
import RCreateEvent from "view/Calender/RCreateEvent";
const GCalender = ({ advanced = true }) => {
	const { courseId } = useParams();
	const [printedCal, setPrintedCal] = React.useState(null);
	const [addingEventType, setAddingEventType] = useState(false);
	const [openCreateEventModal, setOpenCreateEventModal] = useState(false);
	const handleCloseCreateEventModal = () => {
		setOpenCreateEventModal(false);
	};
	const validationSchema = Yup.object({
		[Terms.EventTypes]: Yup.array().of(
			Yup.object({
				name: Yup.string().required(tr("Title_Is_Required")),
			})
		),
	});
	const initialValues = {
		[Terms.EventTypes]: [],
	};

	const { values, touched, errors, setFieldValue, setFieldTouched, setFieldError, handleChange, handleBlur } = useFormik({
		initialValues,
		validationSchema,
		validateOnMount: true,
	});
	const handleAddingEventType = () => {
		setAddingEventType(true);
		setFieldValue(Terms.EventTypes, [{ name: "", color: "black", saved: false }, ...values[Terms.EventTypes]]);
	};
	const handleInputBlur = (event, item, { index }) => {
		const name = values?.[Terms.EventTypes]?.[index]?.name;
		const color = values?.[Terms.EventTypes]?.[index]?.color;
		item.id
			? updateEventTypeMutation.mutate({ payload: { name, color }, eventTypeId: item.id, index })
			: createEventTypeMutation.mutate({ payload: { name, color }, index });
	};

	const handleEditFunctionality = (event, item, { index }, savedFlage = false) => {
		setFieldValue(`${Terms.EventTypes}[${index}].saved`, savedFlage);
	};
	const handlePrint = async () => {
		// try {
		// 	const payload = createPrintPayload(Object.values(calenderData.entities.events.byId));
		// 	const response = await calenderApi.print(payload);
		// 	if (!response.data.status) throw new Error(response.data.msg);
		// 	setPrintedCal(response.data.data);
		// } catch (error) {
		// 	toast.error(error?.message);
		// }
	};
	const handlePickColor = (color, index) => {
		setFieldValue(`${Terms.EventTypes}[${index}].color`, color);
	};
	//--------------------------------------------Events ------------------------------------
	const {
		data: events,
		isLoading: isLoadingEvents,
		isFetching: isFetchingEvents,
	} = useFetchDataRQ({
		queryKey: [Terms.Events],
		queryFn: () => calenderApi.getAllEvents(courseId ? courseId : null, courseId ? "course" : null),
		onSuccessFn: () => {},
	});

	//---------------------------------------------Events Types------------------------------
	const {
		data: eventTypes,
		isLoading: isLoadingEventTypes,
		isFetching: isFetchingEventTypes,
	} = useFetchDataRQ({
		queryKey: [Terms.EventTypes],
		queryFn: () => calenderApi.getEventsTypes(courseId ? courseId : null, courseId ? "course" : null),
		onSuccessFn: (data) => {
			setFieldValue(
				Terms.EventTypes,
				data?.data?.data.map((eventType, index) => ({
					...eventType,
					saved: true,
				}))
			);
		},
	});

	const updateEventTypeMutation = useMutateData({
		queryFn: ({ eventTypeId, payload }) => calenderApi.updateEventType(eventTypeId, payload),
		invalidateKeys: [Terms.EventTypes],
		onSuccessFn: ({ data, variables }) => {
			setFieldValue(`${Terms.EventTypes}[${variables.index}].saved`, true);
		},
	});
	const createEventTypeMutation = useMutateData({
		queryFn: ({ payload }) => calenderApi.createEventType(payload),
		invalidateKeys: [Terms.EventTypes],
		onSuccessFn: ({ data, variables }) => {
			setFieldValue(`${Terms.EventTypes}[${variables.index}].saved`, true);
		},
	});
	if (isLoadingEventTypes) {
		return <RLoader />;
	}
	if (!advanced) return <RCalender advanced={advanced} handlePrint={handlePrint} />;
	return (
		// <Container className="content fc-toolbar-fixed" fluid style={{ position: "relative" }}>
		<>
			<RCalender
				handlePrint={handlePrint}
				eventTypes={eventTypes?.data?.data}
				handleAddingEventType={handleAddingEventType}
				addingEventType={addingEventType}
				values={values}
				touched={touched}
				errors={errors}
				setFieldValue={setFieldValue}
				setFieldTouched={setFieldValue}
				setFieldError={setFieldError}
				handleChange={handleChange}
				handleBlur={handleBlur}
				handleInputBlur={handleInputBlur}
				handleEditFunctionality={handleEditFunctionality}
				handlePickColor={handlePickColor}
				setOpenCreateEventModal={setOpenCreateEventModal}
			/>
			<GPrintCalender isOpen={printedCal} handleToggle={() => setPrintedCal(null)} base64File={printedCal} />

			<AppModal
				headerSort={<RCreateEvent />}
				header={tr("Create_Event")}
				parentHandleClose={handleCloseCreateEventModal}
				show={openCreateEventModal}
			/>
		</>
		// </Container>
	);
};

export default GCalender;
