import React, { useState } from "react";
import Swal, { DANGER, SUCCESS } from "utils/Alert";
import { deleteSweetAlert } from "components/Global/RComs/RAlert";
import { get, destroy } from "config/api";
import { Services } from "engine/services";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import tr from "components/Global/RComs/RTranslator";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { successColor } from "config/constants";
import { dangerColor } from "config/constants";
import { useHistory } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import UserAvatar from "assets/img/new/svg/user_avatar.svg";
import iconsFa6 from "variables/iconsFa6";
import { toast } from "react-toastify";
const RecentAddedUser = () => {
	const history = useHistory();
	const [processedRecords, setProcessedRecords] = useState([]);
	const [alert, setAlert] = useState(false);

	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);
	const renderInfo = ({ name, image, organizationUserId, userType }) => {
		return (
			<RFlex
				onClick={() =>
					history.push(`${baseURL}/${genericPath}/users-and-permissions/user-type/${organizationUserId}/${userType}/permissions `)
				}
			>
				<img width={30} height={30} style={{ borderRadius: "100%" }} src={image} />
				<span>{name}</span>
			</RFlex>
		);
	};

	const getDataFromBackend = async (specific_url) => {
		const url = `${Services.auth_organization_management.backend}api/organization_user/recentlyAdded`;
		let response = await get(specific_url ? specific_url : url);
		if (response?.data?.status == 1) {
			return response;
		} else {
			toast.error(response.data.msg);
		}
	};

	const setTableData = (response) => {
		if (!response) {
			return [];
		}
		const data = response?.data?.data.map((r) => ({
			id: r?.organization_user_id,

			table_name: "RecentAddedUser",

			details: [
				{
					key: tr`name`,
					value: {
						name: r.name,
						image: r?.hash_id ? Services.auth_organization_management.file + r?.hash_id : UserAvatar,
						organizationUserId: r.organization_user_id,
						userType: r.user_type_id,
					},
					render: renderInfo,
				},
				{ key: tr`email`, value: r.email },
				{
					key: tr`state`,
					value: r.status == true ? tr`active` : tr`inactive`,
					color: r.status == true ? successColor : dangerColor,
				},
				{
					key: tr`type`,
					value: r.user_type,
				},
			],
			actions: [
				{
					name: tr("delete"),
					icon: iconsFa6.delete,
					color: "danger",
					outline: true,
					onClick: () => {
						handleRemove(r.organization_user_id);
					},
				},
			],
		}));
		setProcessedRecords(response);
		return data;
	};

	const propsLiterals = React.useMemo(
		() => ({
			listerProps: {
				records: setTableData(processedRecords) || [],
				getDataFromBackend: getDataFromBackend,
				setData: setTableData,
				getDataObject: (response) => response.data.data,
			},
		}),
		[processedRecords]
	);
	const successDelete = async (prameters) => {
		const response = await destroy(
			`${Services.auth_organization_management.backend}api/organization_user/removeOrganizationUserType/${prameters.organization_user_id}`
		);

		if (response) {
			if (response.data.status == 1) {
				const response1 = await getDataFromBackend();
				if (response1) {
					setTableData(response1);
					setAlert(null);
				} else {
					toast.error(response1.data.msg);
				}
			}
		} else {
			toast.error(response.data.msg);
		}
	};

	const handleRemove = (organization_user_id) => {
		const prameters = {
			organization_user_id,
		};
		const message = tr`Are you sure`;
		const confirm = tr`Yes, delete it`;
		deleteSweetAlert(showAlerts, hideAlert, successDelete, prameters, message, confirm);
	};
	return (
		<React.Fragment>
			{alert}
			<RAdvancedLister {...propsLiterals.listerProps} />
		</React.Fragment>
	);
};

export default RecentAddedUser;
