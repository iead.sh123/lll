import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import {
	GET_USERS_TYPES_BY_ORGANIZATION_REQUEST,
	GET_USERS_TYPES_BY_ORGANIZATION_SUCCESS,
	GET_USERS_TYPES_BY_ORGANIZATION_ERROR,
	GET_ROLES_BY_SPECIFIC_USER_REQUEST,
	GET_ROLES_BY_SPECIFIC_USER_SUCCESS,
	GET_ROLES_BY_SPECIFIC_USER_ERROR,
	GET_PERMISSIONS_BY_SPECIFIC_USER_REQUEST,
	GET_PERMISSIONS_BY_SPECIFIC_USER_SUCCESS,
	GET_PERMISSIONS_BY_SPECIFIC_USER_ERROR,
	FETCH_ORGANIZATION_ROLES_REQUEST,
	FETCH_ORGANIZATION_ROLES_SUCCESS,
	FETCH_ORGANIZATION_ROLES_ERROR,
	CREATE_ROLE_TO_ORGANIZATION_REQUEST,
	CREATE_ROLE_TO_ORGANIZATION_SUCCESS,
	CREATE_ROLE_TO_ORGANIZATION_ERROR,
	GET_ALL_ROLE_CATEGORIES_REQUEST,
	GET_ALL_ROLE_CATEGORIES_SUCCESS,
	GET_ALL_ROLE_CATEGORIES_ERROR,
	GET_ROLE_BY_ID_REQUEST,
	GET_ROLE_BY_ID_SUCCESS,
	GET_ROLE_BY_ID_ERROR,
	GET_PERMISSIONS_BY_ROLE_REQUEST,
	GET_PERMISSIONS_BY_ROLE_SUCCESS,
	GET_PERMISSIONS_BY_ROLE_ERROR,
	GET_ALL_CATEGORIES_ORGANIZATION_REQUEST,
	GET_ALL_CATEGORIES_ORGANIZATION_SUCCESS,
	GET_ALL_CATEGORIES_ORGANIZATION_ERROR,
	GET_PERMISSIONS_BY_CATEGORY_REQUEST,
	GET_PERMISSIONS_BY_CATEGORY_SUCCESS,
	GET_PERMISSIONS_BY_CATEGORY_ERROR,
	GET_PERMISSION_BY_ID_REQUEST,
	GET_PERMISSION_BY_ID_SUCCESS,
	GET_PERMISSION_BY_ID_ERROR,
	GET_RECENT_USER_TYPES_REQUEST,
	GET_RECENT_USER_TYPES_SUCCESS,
	GET_RECENT_USER_TYPES_ERROR,
	GET_RECENT_ADDED_ROLES_REQUEST,
	GET_RECENT_ADDED_ROLES_SUCCESS,
	GET_RECENT_ADDED_ROLES_ERROR,
	GET_RECENT_ACTIVITIES_REQUEST,
	GET_RECENT_ACTIVITIES_SUCCESS,
	GET_RECENT_ACTIVITIES_ERROR,
	GET_USERS_BY_ORGANIZATION_REQUEST,
	GET_USERS_BY_ORGANIZATION_SUCCESS,
	GET_USERS_BY_ORGANIZATION_ERROR,
	GET_PERMISSIONS_BY_ORGANIZATION_REQUEST,
	GET_PERMISSIONS_BY_ORGANIZATION_SUCCESS,
	GET_PERMISSIONS_BY_ORGANIZATION_ERROR,
	GET_USERS_BY_USER_TYPE_REQUEST,
	GET_USERS_BY_USER_TYPE_SUCCESS,
	GET_USERS_BY_USER_TYPE_ERROR,
} from "./UsersAndPermissionsTypes";

import { usersAndPermissionsApi } from "api/usersAndPermissions";
import { baseURL, genericPath } from "engine/config";
import { toast } from "react-toastify";

export const getUsersTypesByOrganizationAsync = async ({ dispatch, forDropDownList, history, pushToFirstTab }) => {
	try {
		dispatch({
			type: GET_USERS_TYPES_BY_ORGANIZATION_REQUEST,
		});
		const response = await usersAndPermissionsApi.getUsersTypes(forDropDownList);
		if (response.data.status === 1) {
			let data = response.data.data;

			dispatch({
				type: GET_USERS_TYPES_BY_ORGANIZATION_SUCCESS,
				payload: { data, forDropDownList },
			});

			if (pushToFirstTab) {
				history.replace(`${baseURL}/${genericPath}/users-and-permissions/user-type/${data[0]?.id}`);
			}
		} else {
			dispatch({
				type: GET_USERS_TYPES_BY_ORGANIZATION_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_USERS_TYPES_BY_ORGANIZATION_ERROR,
		});
		toast.error(error.message);
	}
};

export const getRolesBySpecificUserAsync = async (dispatch, organizationUserId, searchData) => {
	try {
		dispatch({
			type: GET_ROLES_BY_SPECIFIC_USER_REQUEST,
		});
		const response = await usersAndPermissionsApi.getRolesBySpecificUser(organizationUserId, searchData);
		if (response.data.status === 1) {
			let data = response.data.data;

			dispatch({
				type: GET_ROLES_BY_SPECIFIC_USER_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: GET_ROLES_BY_SPECIFIC_USER_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_ROLES_BY_SPECIFIC_USER_ERROR,
		});
		toast.error(error.message);
	}
};

export const getPermissionsBySpecificUserAsync = async (dispatch, organizationUserId, searchData) => {
	try {
		dispatch({
			type: GET_PERMISSIONS_BY_SPECIFIC_USER_REQUEST,
		});
		const response = await usersAndPermissionsApi.getPermissionsBySpecificUser(organizationUserId, searchData);
		if (response.data.status === 1) {
			let data = response.data.data;

			dispatch({
				type: GET_PERMISSIONS_BY_SPECIFIC_USER_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: GET_PERMISSIONS_BY_SPECIFIC_USER_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_PERMISSIONS_BY_SPECIFIC_USER_ERROR,
		});
		toast.error(error.message);
	}
};

export const getOrganizationRolesAsync = async (dispatch, searchData) => {
	try {
		dispatch({
			type: FETCH_ORGANIZATION_ROLES_REQUEST,
		});
		const response = await usersAndPermissionsApi.getOrganizationRoles(searchData);
		if (response.data.status === 1) {
			const data = response.data.data;

			dispatch({
				type: FETCH_ORGANIZATION_ROLES_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: FETCH_ORGANIZATION_ROLES_ERROR,
			});

			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: FETCH_ORGANIZATION_ROLES_ERROR,
		});
		toast.error(error.message);
	}
};

export const createRoleToOrganizationAsync = async (dispatch, data, roleId, history) => {
	try {
		dispatch({
			type: CREATE_ROLE_TO_ORGANIZATION_REQUEST,
		});
		const response = await usersAndPermissionsApi.createRoleToOrganization(data);
		if (response.data.status === 1) {
			dispatch({
				type: CREATE_ROLE_TO_ORGANIZATION_SUCCESS,
			});

			if (roleId) {
			} else {
				getOrganizationRolesAsync(dispatch);
				history.push(`${baseURL}/${genericPath}/users-and-permissions/roles`);
			}
		} else {
			dispatch({
				type: CREATE_ROLE_TO_ORGANIZATION_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: CREATE_ROLE_TO_ORGANIZATION_ERROR,
		});
		toast.error(error.message);
	}
};

export const getAllRoleCategoriesAsync = async (dispatch) => {
	try {
		dispatch({
			type: GET_ALL_ROLE_CATEGORIES_REQUEST,
		});
		const response = await usersAndPermissionsApi.getAllRoleCategories();
		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: GET_ALL_ROLE_CATEGORIES_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: GET_ALL_ROLE_CATEGORIES_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_ALL_ROLE_CATEGORIES_ERROR,
		});
		toast.error(error.message);
	}
};

export const getRoleByIdAsync = async (dispatch, roleId) => {
	try {
		dispatch({
			type: GET_ROLE_BY_ID_REQUEST,
		});
		const response = await usersAndPermissionsApi.getRoleById(roleId);
		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: GET_ROLE_BY_ID_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: GET_ROLE_BY_ID_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_ROLE_BY_ID_ERROR,
		});
		toast.error(error.message);
	}
};

export const getPermissionsByRoleAsync = async (dispatch, roleId, searchData) => {
	try {
		dispatch({
			type: GET_PERMISSIONS_BY_ROLE_REQUEST,
		});
		const response = await usersAndPermissionsApi.getPermissionsByRole(roleId, searchData);
		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: GET_PERMISSIONS_BY_ROLE_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: GET_PERMISSIONS_BY_ROLE_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_PERMISSIONS_BY_ROLE_ERROR,
		});
		toast.error(error.message);
	}
};

export const getAllCategoriesOrganizationAsync = async ({ dispatch, forDropDownList, history, pushToFirstTab }) => {
	try {
		dispatch({
			type: GET_ALL_CATEGORIES_ORGANIZATION_REQUEST,
		});
		const response = await usersAndPermissionsApi.getCategoriesToOrganization(forDropDownList);
		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: GET_ALL_CATEGORIES_ORGANIZATION_SUCCESS,
				payload: { data, forDropDownList },
			});
			if (pushToFirstTab) {
				history.replace(`${baseURL}/${genericPath}/users-and-permissions/permissions/${data[0]?.id}`);
			}
		} else {
			dispatch({
				type: GET_ALL_CATEGORIES_ORGANIZATION_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_ALL_CATEGORIES_ORGANIZATION_ERROR,
		});
		toast.error(error.message);
	}
};

export const getPermissionsByCategoryAsync = async (dispatch, categoryId, searchData, forDropDownList) => {
	try {
		dispatch({
			type: GET_PERMISSIONS_BY_CATEGORY_REQUEST,
		});
		const response = await usersAndPermissionsApi.getPermissionsByCategory(categoryId, searchData, forDropDownList);
		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: GET_PERMISSIONS_BY_CATEGORY_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: GET_PERMISSIONS_BY_CATEGORY_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_PERMISSIONS_BY_CATEGORY_ERROR,
		});
		toast.error(error.message);
	}
};

export const getPermissionByIdAsync = async (dispatch, permissionId) => {
	try {
		dispatch({
			type: GET_PERMISSION_BY_ID_REQUEST,
		});
		const response = await usersAndPermissionsApi.getPermissionById(permissionId);
		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: GET_PERMISSION_BY_ID_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: GET_PERMISSION_BY_ID_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_PERMISSION_BY_ID_ERROR,
		});
		toast.error(error.message);
	}
};

export const getRecentUserTypesAsync = async (dispatch) => {
	try {
		dispatch({
			type: GET_RECENT_USER_TYPES_REQUEST,
		});
		const response = await usersAndPermissionsApi.getRecentUserTypes();
		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: GET_RECENT_USER_TYPES_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: GET_RECENT_USER_TYPES_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_RECENT_USER_TYPES_ERROR,
		});
		toast.error(error.message);
	}
};

export const getRecentAddedRolesAsync = async (dispatch) => {
	try {
		dispatch({
			type: GET_RECENT_ADDED_ROLES_REQUEST,
		});
		const response = await usersAndPermissionsApi.getRecentAddedRoles();
		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: GET_RECENT_ADDED_ROLES_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: GET_RECENT_ADDED_ROLES_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_RECENT_ADDED_ROLES_ERROR,
		});
		toast.error(error.message);
	}
};

export const getRecentActivitiesAsync = async (dispatch) => {
	try {
		dispatch({
			type: GET_RECENT_ACTIVITIES_REQUEST,
		});
		const response = await usersAndPermissionsApi.getRecentActivities();
		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: GET_RECENT_ACTIVITIES_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: GET_RECENT_ACTIVITIES_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_RECENT_ACTIVITIES_ERROR,
		});
		toast.error(error.message);
	}
};

export const getUsersByOrganizationAsync = async (dispatch, forDropDownList) => {
	try {
		dispatch({
			type: GET_USERS_BY_ORGANIZATION_REQUEST,
		});
		const response = await usersAndPermissionsApi.getUsersByOrganization(forDropDownList);
		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: GET_USERS_BY_ORGANIZATION_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: GET_USERS_BY_ORGANIZATION_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_USERS_BY_ORGANIZATION_ERROR,
		});
		toast.error(error.message);
	}
};

export const getPermissionsByOrganizationAsync = async (dispatch, forDropDownList) => {
	try {
		dispatch({
			type: GET_PERMISSIONS_BY_ORGANIZATION_REQUEST,
		});
		const response = await usersAndPermissionsApi.getPermissionsByOrganization(forDropDownList);
		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: GET_PERMISSIONS_BY_ORGANIZATION_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: GET_PERMISSIONS_BY_ORGANIZATION_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_PERMISSIONS_BY_ORGANIZATION_ERROR,
		});
		toast.error(error.message);
	}
};

export const getUsersByUserTypeAsync = async (dispatch, userTypeId, forDropDownList) => {
	try {
		dispatch({
			type: GET_USERS_BY_USER_TYPE_REQUEST,
		});
		const response = await usersAndPermissionsApi.getUsersByUserType(userTypeId, forDropDownList);
		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: GET_USERS_BY_USER_TYPE_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: GET_USERS_BY_USER_TYPE_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_USERS_BY_USER_TYPE_ERROR,
		});
		toast.error(error.message);
	}
};
