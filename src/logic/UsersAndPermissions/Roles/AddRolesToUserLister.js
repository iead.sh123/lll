import React, { useState } from "react";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import { get } from "config/api";
import { useParams } from "react-router-dom";
import tr from "components/Global/RComs/RTranslator";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";

import { Services } from "engine/services";
import iconsFa6 from "variables/iconsFa6";
import { toast } from "react-toastify";

const AddRolesToUserLister = ({ payloadData, setPayloadData }) => {
	const { organizationUserId, tabTitle } = useParams();
	const [processedRecords, setProcessedRecords] = useState([]);

	const getDataFromBackend = async (specific_url) => {
		const url = `${Services.auth_organization_management.backend}api/role/rolesToAddForOrganizationUser/${organizationUserId}`;
		let response = await get(specific_url ? specific_url : url);

		if (response && response.data && response.data.status == 1) {
			return response;
		} else {
			toast.error(response.data.msg)
		}
	};

	const handleAddRolesToUser = (record) => {
		setPayloadData((prevState) => {
			// Check if the id already exists in the roles_ids array
			const exists = prevState.roles_ids.includes(record.id);

			if (exists) {
				// Remove the id from the roles_ids array
				const updatedRolesIds = prevState.roles_ids.filter((roleId) => roleId !== record.id);

				// Return the updated state
				return {
					...prevState,
					roles_ids: updatedRolesIds,
				};
			} else {
				// Add the id to the roles_ids array
				const updatedRolesIds = [...prevState.roles_ids, record.id];

				// Return the updated state
				return {
					...prevState,
					roles_ids: updatedRolesIds,
				};
			}
		});
	};

	const setTableData = (response) => {
		if (!response) {
			return [];
		}

		const data = response?.data?.data?.data?.map((rc) => ({
			id: rc.id,
			tableName: "AddRolesToUserLister",
			details: [
				{ key: tr`name`, value: rc.name },
				{ key: tr`Description`, value: rc.Description },
			],
			actions: [
				{
					name: "add",
					icon: payloadData.roles_ids.includes(rc.id) ? iconsFa6.delete : "fa fa-plus",
					outline: true,
					color: payloadData.roles_ids.includes(rc.id) ? "danger" : "primary",
					onClick: () => {
						handleAddRolesToUser(rc);
					},
				},
			],
		}));
		setProcessedRecords(response);

		return data;
	};

	const propsLiterals = React.useMemo(
		() => ({
			listerProps: {
				records: setTableData(processedRecords) || [],
				getDataFromBackend: getDataFromBackend,
				setData: setTableData,
				getDataObject: (response) => response.data.data,
			},
		}),
		[processedRecords, payloadData]
	);
	return <RAdvancedLister {...propsLiterals.listerProps} />;
};

export default AddRolesToUserLister;
