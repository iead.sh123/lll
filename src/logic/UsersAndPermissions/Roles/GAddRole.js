import React, { useEffect, useReducer } from "react";
import {
  getAllRoleCategoriesAsync,
  createRoleToOrganizationAsync,
  getRoleByIdAsync,
  getUsersTypesByOrganizationAsync,
  getPermissionsByOrganizationAsync,
  getUsersByOrganizationAsync,
  getPermissionsByCategoryAsync,
  getUsersByUserTypeAsync,
  getAllCategoriesOrganizationAsync,
} from "../State/UsersAndPermissions.action";
import { ADD_VALUE_TO_ROLE_CATEGORIES_FORM } from "../State/UsersAndPermissionsTypes";
import {
  usersAndPermissionsReducer,
  initialState,
} from "../State/UsersAndPermissions.reducer";
import { useHistory, useParams } from "react-router-dom";
import RAddRole from "view/UsersAndPermissions/Role/RAddRole";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";
import Loader from "utils/Loader";

const GAddRole = () => {
  const history = useHistory();
  const { roleId } = useParams();
  const [usersAndPermissionsData, dispatch] = useReducer(
    usersAndPermissionsReducer,
    initialState
  );

  useEffect(() => {
    getAllRoleCategoriesAsync(dispatch);
    if (!roleId) {
      // getUsersTypesByOrganizationAsync(dispatch, true);
      getUsersTypesByOrganizationAsync({
        dispatch: dispatch,
        forDropDownList: true,
      });
      getPermissionsByOrganizationAsync(dispatch, true);
      getUsersByOrganizationAsync(dispatch, true);
      getAllCategoriesOrganizationAsync({
        dispatch: dispatch,
        forDropDownList: true,
      });
    }

    if (roleId) {
      getRoleByIdAsync(dispatch, roleId);
    }
  }, [roleId]);

  const handleChange = (name, value) => {
    dispatch({
      type: ADD_VALUE_TO_ROLE_CATEGORIES_FORM,
      payload: {
        name,
        value,
      },
    });
  };

  const handlePermissionsByCategory = (categoryId) => {
    getPermissionsByCategoryAsync(dispatch, categoryId, null, true);
  };

  const handleUsersByUserType = (userTypeId) => {
    getUsersByUserTypeAsync(dispatch, userTypeId, true);
  };

  const handleAddRoleToOrganization = (e) => {
    e.preventDefault();
    createRoleToOrganizationAsync(
      dispatch,
      usersAndPermissionsData.roleCategoryData,
      roleId,
      history
    );
  };

  return (
    <React.Fragment>
      {usersAndPermissionsData.roleCategoryDataLoading ? (
        <Loader />
      ) : (
        <RAddRole
          usersAndPermissionsData={usersAndPermissionsData}
          handleChange={handleChange}
          handlePermissionsByCategory={handlePermissionsByCategory}
          handleUsersByUserType={handleUsersByUserType}
          roleId={roleId}
        />
      )}
      {!usersAndPermissionsData.roleCategoryData.app_level && (
        <RFlex className="mt-4">
          <RButton
            text={tr`save`}
            color="primary"
            onClick={handleAddRoleToOrganization}
            loading={usersAndPermissionsData.createRoleToOrganizationLoading}
            disabled={
              usersAndPermissionsData.createRoleToOrganizationLoading
                ? true
                : false
            }
          />
        </RFlex>
      )}
    </React.Fragment>
  );
};

export default GAddRole;
