import React, { useEffect, useReducer, useState } from "react";
import { usersAndPermissionsReducer, initialState } from "../State/UsersAndPermissions.reducer";
import { getOrganizationRolesAsync } from "../State/UsersAndPermissions.action";
import Loader from "utils/Loader";
import styles from "../GUsersAndPermissions.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RRole from "view/UsersAndPermissions/RRole";
import tr from "components/Global/RComs/RTranslator";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import { useHistory } from "react-router-dom";
import { baseURL, genericPath } from "engine/config";
import REmptyData from "components/RComponents/REmptyData";

const GRoleLister = () => {
	const history = useHistory();
	const [usersAndPermissionsData, dispatch] = useReducer(usersAndPermissionsReducer, initialState);
	const [searchData, setSearchData] = useState("");
	const [openModal, setOpenModal] = useState(false);

	const handleOpenModal = () => setOpenModal(true);
	const handleCloseModal = () => setOpenModal(false);

	const handleChangeSearch = (text) => {
		setSearchData(text);
	};
	const handleSearch = (emptySearch) => {
		getOrganizationRolesAsync(dispatch, emptySearch ?? searchData);
	};

	useEffect(() => {
		getOrganizationRolesAsync(dispatch);
	}, []);

	return (
		<>
			{usersAndPermissionsData.organizationRolesLoading ? (
				<Loader />
			) : (
				<>
					<RSearchHeader
						searchLoading={usersAndPermissionsData.organizationRolesLoading}
						searchData={searchData}
						handleSearch={handleSearch}
						setSearchData={setSearchData}
						handleChangeSearch={handleChangeSearch}
						handleOpenModal={() => {
							history.push(`${baseURL}/${genericPath}/users-and-permissions/role/add`);
						}}
						buttonName={tr`create_new_role`}
						addNew={true}
					/>
					{Object.keys(usersAndPermissionsData.organizationRoles).length == 0 ? (
						<REmptyData />
					) : (
						Object.keys(usersAndPermissionsData.organizationRoles).map((key) => {
							return (
								<div key={key} className={styles.role_div}>
									<h6>{key}</h6>
									<RFlex styleProps={{ flexWrap: "wrap" }}>
										{usersAndPermissionsData.organizationRoles[key].map((item) => (
											<div key={item.id}>
												<RRole role={item} lister={true} />
											</div>
										))}
									</RFlex>
								</div>
							);
						})
					)}
				</>
			)}
		</>
	);
};

export default GRoleLister;
