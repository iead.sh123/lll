import RFlex from "components/Global/RComs/RFlex/RFlex";
import React, { useState } from "react";
import RHeader from "../Shared/RHeader";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { Terms, rolesHeader } from "../constants";
import { usersManagementApi } from "api/UsersManagement";
import RLister from "components/Global/RComs/RLister";
import RLoader from "components/Global/RComs/RLoader";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";
import { useMutateData } from "hocs/useMutateData";
import { useCUDToQueryKey } from "hocs/useCUDToQueryKey";
import { deleteSweetAlert } from "components/Global/RComs/RAlert2";
import RCSVDrawer from "../Shared/RCSVDrawer";
import { useHistory } from "react-router-dom";
import GCreateRole from "./GCreateRole";
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";
import styles from "./rolesStyles.module.scss";
import { fileExcel } from "config/mimeTypes";
import RDropdown from "components/Global/RComs/RDropdown";
const GRoels = () => {
	const [alert1, setAlert] = useState(false);
	const [backendSearchTerm, setBackSearchTerm] = useState("");
	const [isDrawerOpen, setIsDrawerOpen] = useState({ isOpen: false, operationType: "" });
	const [page, SetPage] = useState(1);
	const [rolesIdsDeleteing, setRolesIdsDeleting] = useState([]);
	const history = useHistory();
	const handleChangePage = (page) => {
		SetPage(page);
	};

	const toggleDrawer = () => {
		setIsDrawerOpen((prevState) => !prevState);
	};
	const dropdownActions = [
		{
			name: tr("Import_CSV"),
			onClick: () => {
				setIsDrawerOpen({ isOpen: true, operationType: "Import" });
			},
		},
		{
			name: tr("Export_CSV"),
			onClick: () => {
				exportRolesMutaiton.mutate();
			},
		},
	];
	//-------------------------------import/export Roles------------------------------------

	const importRolesTemplateMutation = useMutateData({
		queryFn: () => usersManagementApi.downloadRolesTemplate(),
		downloadFile: true,
		mimeType: fileExcel[0],
		folderName: "Roles Template.xlsx",
	});
	const exportRolesMutaiton = useMutateData({
		queryFn: () => usersManagementApi.exportRoles(),
		downloadFile: true,
		mimeType: fileExcel[0],
		folderName: "Roles.xlsx",
	});
	//----------------------------------Fetch Roles---------------------------------------------
	const {
		data: roles,
		isLoading: isLoadingRoles,
		isFetching: isFetchingRoles,
		refetch: refetchRoles,
	} = useFetchDataRQ({
		queryKey: [Terms.Roles, backendSearchTerm],
		keepPreviousData: true,
		queryFn: () => usersManagementApi.getOrganizationRoles(backendSearchTerm),
		onSuccessFn: (data) => {
			console.log(data);
		},
	});
	const { CUDToQueryKey, operations } = useCUDToQueryKey();
	const deleteRoleMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.deleteOrganizationRoles(payload),
		invalidateKeys: [Terms.Roles, backendSearchTerm],
		onSuccessFn: ({ variables }) => {
			CUDToQueryKey({
				queryKey: [Terms.Roles, backendSearchTerm],
				insertionDepth: `data.data`,
				id: variables.payload?.ids[0],
				operation: operations.DELETE,
			});
			setRolesIdsDeleting(rolesIdsDeleteing.filter((id) => !variables?.payload?.ids?.includes(id)));
		},
		onErrorFn: (error, variables) => {
			setRolesIdsDeleting(rolesIdsDeleteing.filter((id) => !variables?.payload?.ids?.includes(id)));
		},
	});
	const handleDeleteRole = (role) => {
		const payload = {
			ids: [role.id],
		};
		deleteRoleMutation.mutate({ payload });
		setRolesIdsDeleting([...rolesIdsDeleteing, role.id]);
	};
	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);
	const deleteRole = (role) => {
		const confirm = tr`Yes_delete_it`;
		console.log("rolerole2", role);
		const message = (
			<div>
				<h6>
					{tr`Are_you_sure_to_delete`} <span className="p-0 m-0">{role.name}</span>
				</h6>
				<p style={{ fontSize: "14px", fontWeight: "400" }}>{tr`This_can't_be_undone`}</p>
			</div>
		);
		deleteSweetAlert(showAlerts, hideAlert, handleDeleteRole, { role }, message, confirm);
	};
	if (isLoadingRoles) return <RLoader />;

	// const records = roles?.data?.data?.map((role, index) => {
	// 	const rolesCount = roles?.data?.data?.length;
	// 	const DescriptionComponent = (
	// 		<RFlex className="align-items-center" styleProps={{ minHeight: "52px", maxWidth: "400px" }}>
	// 			{role.description}
	// 		</RFlex>
	// 	);
	// 	const roleNameComponent = (
	// 		<RFlex
	// 			styleProps={{ cursor: "pointer" }}
	// 			className={`${styles.hoverLink}`}
	// 			onClick={() => {
	// 				history.push(`${baseURL}/${genericPath}/users-management/roles/${role.id}`);
	// 			}}
	// 		>
	// 			<span className="p-0 m-0">{role?.name}</span>
	// 		</RFlex>
	// 	);
	// 	return {
	// 		details: rolesHeader.map((header, index) => ({
	// 			key:
	// 				header.name == "roleName" ? (
	// 					<RFlex className="align-items-center">
	// 						{tr(`${header.key}`)}{" "}
	// 						<span className="text-primary" style={{ fontSize: "8px" }}>
	// 							{rolesCount}
	// 						</span>
	// 					</RFlex>
	// 				) : (
	// 					header.key
	// 				),
	// 			keyType: header.name == "roleName" ? "component" : "",
	// 			value:
	// 				header.name == "roleName"
	// 					? roleNameComponent
	// 					: header.name == "permissions"
	// 					? role?.[header.value].length
	// 					: header.name == "description"
	// 					? DescriptionComponent
	// 					: role?.[header.value],
	// 			type: header.name == "roleName" || header.name == "description" ? "component" : "",
	// 		})),
	// 		actions: [
	// 			{
	// 				name: "",
	// 				icon: iconsFa6.delete,
	// 				loading: rolesIdsDeleteing.some((id) => id == role.id),
	// 				disabled: rolesIdsDeleteing.some((id) => id == role.id),
	// 				justIcon: true,
	// 				onClick: () => {
	// 					deleteRole(role);
	// 				},
	// 				actionIconClass: "text-danger",
	// 			},
	// 		],
	// 	};
	// });
	const columns = [
		{
			accessorKey: "name",
			renderHeader: () => (
				<RFlex className="align-items-center">
					<RFlex styleProps={{ gap: "5px" }}>
						{tr("Role_Name")}
						<span className={`${styles.lengthDiv} text-themePrimary`} style={{ fontSize: "8px" }}>
							{roles?.data?.data?.length}
						</span>
					</RFlex>
				</RFlex>
			),
			renderCell: ({ row }) => (
				<RFlex className={`${styles.hoverLink}`}>
					<span
						className="p-0 m-0 cursor-pointer"
						onClick={() => {
							history.push(`${baseURL}/${genericPath}/users-management/roles/${row.original.id}`);
						}}
					>
						{row.getValue("name")}
					</span>
				</RFlex>
			),
		},
		{
			accessorKey: "users",
			renderHeader: () => <span>{tr("Users")}</span>,
			renderCell: ({ row }) => <span>{row.getValue("users")}</span>,
		},
		{
			accessorKey: "permissions",
			renderHeader: () => <span>{tr("permissions")}</span>,
			renderCell: ({ row }) => {
				console.log("row?.original");
				return <span>{row?.getValue("permissions").length}</span>;
			},
		},
		{
			accessorKey: "description",
			renderHeader: () => <span>{tr("description")}</span>,
			renderCell: ({ row }) => <span className="text-themeBoldGrey">{row.getValue("description") ?? "-"}</span>,
		},
	];
	const actions = [
		{
			name: "delete",
			icon: iconsFa6.delete,
			actionIconClass: "text-themeDanger",
			confirmAction: ({ row }) => {
				handleDeleteRole(row.original);
			},
			inDialog: true,
			dialogTitle: ({ row }) => `${tr("Are_You_Sure_To_Delete")} ${row.original.name}`,
		},
	];
	const records = { data: roles?.data?.data, columns, actions };

	return (
		<>
			<RFlex className="flex-column">
				<RHeader
					createText={"New_Role"}
					onClick={() => {}}
					history={history}
					setBackSearchTerm={setBackSearchTerm}
					isFetching={isFetchingRoles}
					refetchFunction={refetchRoles}
					actions={dropdownActions}
					dataType={Terms.Roles}
					onCreateClick={() => history.push(`${baseURL}/${genericPath}/users-management/roles/add`)}
					searchPlaceholder={"Search_For_a_Role"}
				/>
				<RLister
					Records={records}
					convertRecordsShape={false}
					containerClassName="max-h-[60vh]"
					// info={roles}
					// withPagination={true}
					// handleChangePage={handleChangePage}
					// page={page}
					line1={"No_Roles_Yet"}
				/>
				<RCSVDrawer
					isOpen={isDrawerOpen.isOpen}
					operationType={isDrawerOpen.operationType}
					toggleDrawer={toggleDrawer}
					downloadTemplateFn={() => importRolesTemplateMutation.mutate()}
					currentType={Terms.Roles}
				/>
			</RFlex>
			{alert1}
		</>
	);
};

export default GRoels;
