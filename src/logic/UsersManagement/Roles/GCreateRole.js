import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import React, { useEffect, useState } from "react";
import iconsFa6 from "variables/iconsFa6";
import { createRoleTabs, Terms } from "../constants";
import RFilterTabs from "components/Global/RComs/RFilterTabs/RFilterTabs";
import { useFormik } from "formik";
import * as Yup from "yup";
import styles from "./rolesStyles.module.scss";
import RRoleSettings from "./RRoleSettings";
import { usersManagementApi } from "api/UsersManagement";
import { useMutateData } from "hocs/useMutateData";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import RPermissons from "../Shared/RPermissons";
import RLoader from "components/Global/RComs/RLoader";
import GFirstMembersHeader from "../Shared/GFirstMembersHeader";
import RUsersData from "../Shared/RUsersData";
import RTabs from "components/Global/RComs/RTabs/RTabs";
import RCSVDrawer from "../Shared/RCSVDrawer";
import RHeader from "../Shared/RHeader";
import RReviewPermissions from "../Shared/RReviewPermissions";
import RReviewUserData from "../Shared/RReviewUserData";
import { useHistory } from "react-router-dom";
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";
import * as colors from "config/constants";
import { convertFiltersShape } from "../Shared/ConvertFiltersShape";
import { fileExcel } from "config/mimeTypes";
import { toast } from "react-toastify";
import RNewTabs from "components/RComponents/RNewTabs";
import { Button } from "ShadCnComponents/ui/button";
const GCreateRole = ({ setIsCreating, backendSearchTerm }) => {
	//----------------------------Global--------------------------------------------------
	const [activeTab, setActiveTab] = useState(Terms.Settings);
	const [currentRoleName, setCurrentRoleName] = useState("");
	const [currentRoleId, setCurrentRoleId] = useState(null);
	const [isDrawerOpen, setIsDrawerOpen] = useState({ isOpen: false, operationType: "" });
	const history = useHistory();
	const toggleDrawer = () => {
		setIsDrawerOpen((prevState) => !prevState);
	};

	const dropdownActions = [
		{
			name: tr("Import_CSV"),
			onClick: () => {
				setIsDrawerOpen({ isOpen: true, operationType: "Import" });
			},
		},
		{
			name: tr("Export_CSV"),
			onClick: () => {
				exportUserMutaiton.mutate();
			},
		},
	];
	//---------------------------------import export users-------------------------------
	const importUsersTempalteMutation = useMutateData({
		queryFn: () => usersManagementApi.downloadUsersTemplate(),
		downloadFile: true,
		mimeType: fileExcel[0],
		folderName: "Users Template.xlsx",
	});
	const exportUserMutaiton = useMutateData({
		queryFn: () => usersManagementApi.exportUsers(),
		downloadFile: true,
		mimeType: fileExcel[0],
		folderName: "Users.xlsx",
	});
	//----------------------------Settings----------------------------------------
	const initialValues = {
		roleName: null,
		description: "",
	};
	const [savedSettingsValues, setSavedSettingsValues] = useState({
		roleName: null,
		description: null,
	});
	const validationSchema = Yup.object({
		roleName: Yup.string().required("Role Name is Required"),
		description: Yup.string(),
	});
	const {
		values: settingsValues,
		handleBlur,
		handleChange,
		setFieldTouched,
		setFieldError,
		errors,
		touched,
	} = useFormik({ initialValues, validationSchema });

	const addRoleMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.addOrganizationRole(payload),
		// invalidateKeys: [Terms.Roles, backendSearchTerm],
		onSuccessFn: ({ data }) => {
			setCurrentRoleName(settingsValues.roleName);
			setCurrentRoleId(data?.data?.[0]);
			setSavedSettingsValues({ ...settingsValues });
			setActiveTab(Terms.Permissions);
			toast.success(`${settingsValues.roleName} ${tr("Is_created_successfully")}`);
		},
	});
	const updateRoleMuation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.updateOrganizationRole(payload),
		// invalidateKeys: [Terms.Roles, backendSearchTerm],
		onSuccessFn: ({ data }) => {
			setCurrentRoleName(settingsValues.roleName);
			setSavedSettingsValues({ ...settingsValues });
			setActiveTab(Terms.Permissions);
		},
	});
	//----------------------------Permissions--------------------------------------------
	const [backendPermissoinSearchTerm, setBackendPermissoinSearchTerm] = useState("");
	const [frontPermissionSearchTerm, setFrontPermissionSearchTerm] = useState("");
	const [selectedPermissions, setSelectedPermissions] = useState([]);
	const [savedPermissions, setSavedPermissions] = useState([]);
	const {
		data: permissions,
		isLoading: isLoadingPermissions,
		isFetching: isFetchingPermissions,
	} = useFetchDataRQ({
		queryKey: [Terms.Permissions, backendPermissoinSearchTerm],
		queryFn: () => usersManagementApi.getOrganizationPermissions(backendPermissoinSearchTerm),
		keepPreviousData: true,
		onSuccessFn: (data) => {
			console.log("Success", data);
		},
	});

	const handleSelectPermission = (permission) => {
		const alreadyChecked = selectedPermissions.some((p1) => p1.id == permission.id);
		if (alreadyChecked) {
			const newPermessionsArray = selectedPermissions.filter((p1) => p1.id != permission.id);
			setSelectedPermissions(newPermessionsArray);
		} else {
			const newPermessionsArray = [...selectedPermissions, permission];
			setSelectedPermissions(newPermessionsArray);
		}
	};
	const handleSelectAllPermissions = (allPermissions) => {
		const allIsSelected = allPermissions.length == selectedPermissions.length;
		if (allIsSelected) {
			setSelectedPermissions([]);
		} else {
			const newPermessionsArray = [...allPermissions];
			setSelectedPermissions(newPermessionsArray);
		}
	};

	const assignPermissionsToRoleMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.assignPermissionsToRole(payload),
		// invalidateKeys: [Terms.Roles, backendSearchTerm],
	});
	const removePermissionsFromRoleMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.removePermissionsFromRole(payload),
	});
	const getRemovedPermissions = () => {
		const idsInSelectedPermissions = new Set(selectedPermissions.map((p) => p.id));
		const removedPermissions = savedPermissions.filter((p) => !idsInSelectedPermissions.has(p.id));
		const removedPermissionsNames = removedPermissions.map((p) => p.name);
		return removedPermissionsNames;
	};
	//--------------------------------Members------------------------------------------------

	const [frontMembersSearchTerm, setFrontMembersSearchTerm] = useState("");
	const [selectedMembers, setSelectedMembers] = useState([]);
	const [filters, setFilters] = useState([{ id: 1, active: false, pickedOption: null, isApplied: false }]);
	const [allMembersFilters, setAllMembersFilters] = useState({});
	const [savedMembers, setSavedMembers] = useState([]);

	const initialValuesMembers = {
		email: "",
		address: "",
		phoneNumber: "",
	};
	const phoneNumberRegex = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/;

	const validationSchemaMembers = Yup.object({
		email: Yup.string(),
		address: Yup.string(),
		phoneNumber: Yup.string(),
	});
	const {
		values: membersValues,
		touched: membersTouched,
		errors: membersErrors,
		handleChange: handleChangeMembers,
		handleBlur: handleBlurMembers,
	} = useFormik({
		initialValues: initialValuesMembers,
		validationSchema: validationSchemaMembers,
	});

	//-----------------------------------get Users with filters applyied-----------------------
	const {
		data: members,
		isLoading: isLoadingMembers,
		isFetching: isFetchingMembers,
	} = useFetchDataRQ({
		queryKey: [Terms.Members, allMembersFilters],
		queryFn: () => {
			const filters = convertFiltersShape(allMembersFilters);
			return usersManagementApi.getAllOrganizationUsers(filters);
		},
		keepPreviousData: true,
	});
	//-------------------------------------get users types for filter------------------------
	const {
		data: userTypes,
		isLoading: isLoadingUserTypes,
		isFetching: isFetchingUserTypes,
	} = useFetchDataRQ({
		queryKey: [Terms.UserTypes],
		queryFn: () => usersManagementApi.getUsersTypes(false),
	});

	const assignRoleToUsers = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.assignRoleToUsers(payload),
	});

	const handleSelectMember = (member) => {
		const alreadyChecked = selectedMembers.some((m1) => m1.id == member.id);
		if (alreadyChecked) {
			const newMembersArray = selectedMembers.filter((m1) => m1.id != member.id);
			setSelectedMembers(newMembersArray);
		} else {
			const newMembersArray = [...selectedMembers, member];
			setSelectedMembers(newMembersArray);
		}
	};
	const handleSelectAllMembers = (allMembers) => {
		const allIsSelected = allMembers.length == selectedMembers.length;
		if (allIsSelected) {
			setSelectedMembers([]);
		} else {
			const newMembersArray = [...allMembers];
			setSelectedMembers(newMembersArray);
		}
	};
	//----------------------------------------remove users from role-------------------------
	const getRemovedMembers = () => {
		const idsInSelectedUsers = new Set(selectedMembers.map((u) => u.id));
		const removedMembers = savedMembers.filter((u) => !idsInSelectedUsers.has(u.id));
		const removedMembersIds = removedMembers.map((u) => u.id);
		return removedMembersIds;
	};
	const removeMembersMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.removeMembersFromRole(payload),
	});
	console.log("selectedUsers", selectedMembers);
	//----------------------------------Global--------------------------------------------
	const getNextTab = () => {
		switch (activeTab) {
			case Terms.Settings:
				return tr(`assign_${Terms.Permissions}`);
			case Terms.Permissions:
				return tr(`add_${Terms.Members}`);
			case Terms.Members:
				return tr(Terms.Review);
			case Terms.Review:
				return tr("previous");
		}
	};
	const getSaveAction = () => {
		let payload = {};
		switch (activeTab) {
			case Terms.Settings:
				if (errors.roleName) {
					setFieldTouched("roleName", true);
					return;
				}
				payload = {
					roles: [
						{ name: settingsValues.roleName, description: settingsValues.description, id: currentRoleId ? currentRoleId : undefined },
					],
				};
				currentRoleId ? updateRoleMuation.mutate({ payload }) : addRoleMutation.mutate({ payload });
				// setActiveTab(Terms.Permissions);
				break;
			case Terms.Permissions:
				const removedPermissionsNames = getRemovedPermissions();
				if (removedPermissionsNames.length > 0) {
					payload = {
						permission_names: removedPermissionsNames,
						role_names: [currentRoleName],
					};
					removePermissionsFromRoleMutation.mutate({ payload });
				}
				if (selectedPermissions.length > 0) {
					const permissionsNames = selectedPermissions.map((permission) => permission.name);
					payload = {
						permission_names: permissionsNames,
						role_names: [currentRoleName],
					};
					assignPermissionsToRoleMutation.mutate({ payload });
				}
				setSavedPermissions(selectedPermissions);
				setActiveTab(Terms.Members);
				break;
			case Terms.Members:
				const removedMembersIds = getRemovedMembers();
				if (removedMembersIds.length > 0) {
					const payload = {
						user_ids: removedMembersIds,
						role_names: [currentRoleName],
					};
					removeMembersMutation.mutate({ payload });
				}
				if (selectedMembers.length > 0) {
					const usersIds = selectedMembers.map((user) => user.id);
					payload = {
						role_names: [currentRoleName],
						user_ids: usersIds,
					};
					assignRoleToUsers.mutate({ payload });
				}
				setSavedMembers(selectedMembers);
				setActiveTab(Terms.Review);
				break;
			case Terms.Review:
				history.replace(`${baseURL}/${genericPath}/users-management/roles`);
				break;
		}
	};
	const getNextAction = () => {
		switch (activeTab) {
			case Terms.Settings:
				setActiveTab(Terms.Permissions);
				break;
			case Terms.Permissions:
				setActiveTab(Terms.Members);
				break;
			case Terms.Members:
				setActiveTab(Terms.Review);
				break;
			case Terms.Review:
				setActiveTab(Terms.Members);
				break;
		}
	};
	useEffect(() => {
		if (settingsValues.roleName) {
			setFieldError("roleName", "Role Name is Required");
		}
	}, []);
	return (
		<RFlex className="flex-column" styleProps={{ position: "relative" }}>
			<RFlex className="align-items-center">
				<div
					style={{ cursor: "pointer" }}
					className={styles.backArrow}
					onClick={() => history.replace(`${baseURL}/${genericPath}/users-management/roles`)}
				>
					<i className={iconsFa6.chevronLeft} style={{ color: colors.primaryColor }} />
				</div>
				{tr("Create_new_role")}
			</RFlex>
			<RNewTabs
				value={activeTab}
				setActiveTab={setActiveTab}
				tabs={createRoleTabs.map((tab) => ({
					...tab,
					// content: // to pass the content to the tabs put it like that to remembert this way
					// 	tab.value == Terms.Settings
					// 		? settingsComponent
					// 		: tab.value == Terms.Permissions
					// 		? permissionsComponent
					// 		: tab.value == Terms.Members
					// 		? membersComponent
					// 		: tab.value == Terms.Review
					// 		? reviewComponent
					// 		: "",
				}))}
			/>
			{activeTab == Terms.Settings && (
				<RRoleSettings values={settingsValues} errors={errors} touched={touched} handleBlur={handleBlur} handleChange={handleChange} />
			)}
			{activeTab == Terms.Permissions && (
				<RPermissons
					permissions={permissions?.data?.data}
					setBackSearchTerm={setBackendPermissoinSearchTerm}
					selectOneFn={handleSelectPermission}
					selectAllFn={handleSelectAllPermissions}
					selectedPermissions={selectedPermissions}
					frontSearchTerm={frontPermissionSearchTerm}
					setFrontSearchTerm={setFrontPermissionSearchTerm}
					isFetching={isFetchingPermissions}
				/>
			)}
			{activeTab == Terms.Members &&
				(isLoadingMembers || isLoadingUserTypes ? (
					<RLoader />
				) : (
					<RFlex className="flex-column">
						<RHeader
							actions={dropdownActions}
							showCreate={false}
							showSearchHeader={false}
							showBulk={true}
							showColumnSettings={true}
							showRefresh={false}
						/>
						<GFirstMembersHeader
							allUsersLength={members?.data?.data?.records?.length}
							allMembersFilters={allMembersFilters}
							setAllMembersFilters={setAllMembersFilters}
							filters={filters}
							setFilters={setFilters}
							frontSearchTerm={frontMembersSearchTerm}
							setFrontSearchTerm={setFrontMembersSearchTerm}
							isFetching={isFetchingMembers}
							selectedUsers={selectedMembers}
							values={membersValues}
							touched={membersTouched}
							errors={membersErrors}
							handleChange={handleChangeMembers}
							handleBlur={handleBlurMembers}
						/>
						<RUsersData
							users={members?.data?.data}
							userTypes={userTypes?.data?.data}
							allMembersFilters={allMembersFilters}
							setAllMembersFilters={setAllMembersFilters}
							selectedUsers={selectedMembers}
							selectOneFn={handleSelectMember}
							selectAllFn={handleSelectAllMembers}
							inCreate={true}
						/>
						<RCSVDrawer
							isOpen={isDrawerOpen.isOpen}
							operationType={isDrawerOpen.operationType}
							toggleDrawer={toggleDrawer}
							downloadTemplateFn={() => importUsersTempalteMutation.mutate()}
							currentType={Terms.Users}
						/>
					</RFlex>
				))}
			{activeTab == Terms.Review && (
				<RFlex className="flex-column">
					<RRoleSettings
						values={settingsValues}
						savedSettingsValues={savedSettingsValues}
						errors={errors}
						touched={touched}
						handleBlur={handleBlur}
						handleChange={handleChange}
						review={true}
						showSettingsWord={true}
					/>

					<RReviewPermissions savedPermissions={savedPermissions} />
					<RReviewUserData selectedUsers={savedMembers} users={members?.data?.data} />
				</RFlex>
			)}
			<div className={styles.roolFooter}>
				<RFlex className="align-items-center" styleProps={{ gap: "0px" }}>
					<Button
						disabled={(activeTab != Terms.Settings && !currentRoleName) || addRoleMutation.isLoading || updateRoleMuation.isLoading}
						className="flex gap-1"
						onClick={getSaveAction}
					>
						{activeTab == Terms.Review ? tr("Save_and_Finish") : tr("Save_and_Next")}
						{(addRoleMutation.isLoading || updateRoleMuation.isLoading) && <i className={iconsFa6.spinner} />}
					</Button>
					<Button className="flex gap-1" variant="ghost" onClick={getNextAction}>
						{activeTab == Terms.Review && <i className={iconsFa6.chevronLeft} />}
						{getNextTab()}
						{activeTab != Terms.Review && <i className={iconsFa6.chevronRight} />}
					</Button>
				</RFlex>
			</div>
		</RFlex>
	);
};

export default GCreateRole;
