import React from "react";
import { Input } from "ShadCnComponents/ui/input";
import tr from "components/Global/RComs/RTranslator";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { Textarea } from "ShadCnComponents/ui/textarea";
const RRoleSettings = ({ values, errors, touched, handleBlur, handleChange, review, showSettings = true, savedSettingsValues }) => {
	return (
		<RFlex className="flex-column">
			<RFlex className="flex-column">
				{review && !savedSettingsValues.roleName && showSettings && (
					<span className="p-0 m-0" style={{ fontWeight: "bold", width: "fit-content" }}>
						{tr("settings")}
					</span>
				)}
				<RFlex className="flex-column">
					<RFlex className="align-items-center" styleProps={{ width: "100%" }}>
						<span style={{ width: "130px", height: "fit-content" }} className="p-0 m-0">
							{tr("Role_name")}
						</span>
						{review ? (
							<span className="p-0 m-0" style={{ maxWidth: "590px" }}>
								{savedSettingsValues.roleName ?? "-"}
							</span>
						) : (
							<RFlex className="flex-col gap-1">
								<Input
									name="roleName"
									placeholder={tr("Role_name")}
									className={`${errors.roleName && touched.roleName ? "input__error" : ""}`}
									onChange={handleChange}
									onBlur={handleBlur}
									value={values.roleName}
									type="text"
									style={{ width: "591px" }}
								/>
								{errors.roleName && touched.roleName && <span className="text-themeDanger text-[12px]">{errors.roleName}</span>}
							</RFlex>
						)}
					</RFlex>
					<RFlex className="align-items-start">
						<span style={{ width: "130px", height: "fit-content" }} className="p-0 m-0">
							{tr("Description")}
						</span>
						{review ? (
							<span className="p-0 m-0" style={{ maxWidth: "590px" }}>
								{savedSettingsValues.description ?? "-"}
							</span>
						) : (
							<Textarea
								name="description"
								className={`${errors.description && touched.description ? "input__error" : ""}`}
								onChange={handleChange}
								onBlur={handleBlur}
								value={values.description}
								style={{ width: "591px", lineHeight: "1.5" }}
							/>
						)}
					</RFlex>
				</RFlex>
			</RFlex>
		</RFlex>
	);
};

export default RRoleSettings;
