export const Terms = {
	Roles: "Roles",
	Users: "users",
	UserTypes: "userTypes",
	UserGroups: "userGroups",
	Settings: "Settings",
	Permissions: "Permissions",
	Members: "Members",
	Review: "Review",
	BasicInfo: "Basic_Info",
};

export const rolesHeader = [
	{ key: "Role_name", name: "roleName", value: "name" },
	{ key: "Users", name: "users", value: "users" },
	{ key: "Permissions", name: "permissions", value: "permissions" },
	{ key: "Description", name: "description", value: "description" },
];
export const userTypesHeader = [
	{ key: "User_Type", name: "userType", value: "name" },
	{ key: "Users", name: "users", value: "users_count" },
	{ key: "Description", name: "description", value: "description" },
];
export const permissionsHeader = [
	{ key: "Permissions", name: "permissions", value: "name" },
	{ key: "Description", name: "description", value: "description" },
	{ key: "Privilege", name: "privilege", value: "privileged" },
];
export const groupsHeader = [
	{ key: "User_group", name: "groupName", value: "name" },
	{ key: "Users", name: "users", value: "users_count" },
	{ key: "Description", name: "description", value: "description" },
];
export const createRoleTabs = [
	{ title: Terms.Settings, value: Terms.Settings },
	{ title: Terms.Permissions, value: Terms.Permissions },
	{ title: Terms.Members, value: Terms.Members },
	{ title: Terms.Review, value: Terms.Review },
];
export const viewRoleTabs = [
	{ title: Terms.Permissions, value: Terms.Permissions },
	{ title: Terms.Members, value: Terms.Members },
	{ title: Terms.Settings, value: Terms.Settings },
];
export const createUserTypeTabs = [
	{ title: Terms.Settings, value: Terms.Settings },
	{ title: Terms.Roles, value: Terms.Roles },
	{ title: Terms.Permissions, value: Terms.Permissions },
	{ title: Terms.Members, value: Terms.Members },
	{ title: Terms.Review, value: Terms.Review },
];
export const viewUserTypeTabs = [
	{ title: Terms.Roles, value: Terms.Roles },
	{ title: Terms.Permissions, value: Terms.Permissions },
	{ title: Terms.Members, value: Terms.Members },
	{ title: Terms.Settings, value: Terms.Settings },
];
export const usersTabs = [
	{ title: Terms.BasicInfo, value: Terms.BasicInfo },
	{ title: Terms.Roles, value: Terms.Roles },
	{ title: Terms.Permissions, value: Terms.Permissions },
];
export const filterOptions = [
	{ id: 1, label: "Phone Number", value: "phoneNumber", type: "text", placeholder: "search_for_user_phone_number" },
	{ id: 2, label: "Email", value: "email", type: "email", placeholder: "search_for_user_email" },
	{ id: 3, label: "Address", value: "address", type: "text", placeholder: "search_for_user_address" },
];
