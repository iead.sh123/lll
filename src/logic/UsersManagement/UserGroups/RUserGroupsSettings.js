import React from "react";
import { Input } from "ShadCnComponents/ui/input";
import tr from "components/Global/RComs/RTranslator";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import { Textarea } from "ShadCnComponents/ui/textarea";
const RUserGroupSettings = ({
	values,
	errors,
	touched,
	handleBlur,
	handleChange,
	review,
	showSettings = true,
	savedSettingsValues,
	hashId,
	setUploadId,
}) => {
	return (
		<RFlex className="flex-column">
			<RFlex className="flex-column" styleProps={{ gap: "0px" }}>
				{review && !savedSettingsValues.roleName && showSettings && (
					<span className="p-0 m-0" style={{ fontWeight: "bold", width: "fit-content" }}>
						{tr("settings")}
					</span>
				)}
				<RFlex className="flex-column">
					<RFlex className="items-center">
						<span className="w-[130px]">{tr("user_group_picture")}</span>
						<RFileSuite
							parentCallback={(e) => {
								setUploadId(e[0]);
							}}
							roundWidth={{ width: "65px", height: "65px" }}
							singleFile={true}
							fileType={["image/*"]}
							removeButton={false}
							value={[{ hash_id: hashId ? hashId : null }]}
							uploadName="upload"
							showReplace={false}
							showDelete={true}
							showFileList={false}
							showFileAdd={!review ? true : false}
							setSpecificAttachment={() => {}}
							theme="round"
							binary={true}
						/>
					</RFlex>
					<RFlex className="align-items-center" styleProps={{ width: "100%" }}>
						<span style={{ width: "130px", height: "fit-content" }} className="p-0 m-0">
							{tr("User_Group_Name")}
						</span>
						{review ? (
							<span className="p-0 m-0" style={{ maxWidth: "590px" }}>
								{savedSettingsValues.userGroupName ?? "-"}
							</span>
						) : (
							<RFlex className="flex flex-col gap-1">
								<Input
									name="userGroupName"
									placeholder={tr("User_Group_name")}
									className={`${errors.userGroupName && touched.userGroupName ? "input__error" : ""}`}
									onChange={handleChange}
									onBlur={handleBlur}
									value={values.userGroupName}
									type="text"
									style={{ width: "591px" }}
								/>
								{errors.userGroupName && touched.userGroupName && (
									<span className="text-themeDanger text-[12px]">{errors.userGroupName}</span>
								)}
							</RFlex>
						)}
					</RFlex>
					<RFlex className="align-items-start">
						<span style={{ width: "130px", height: "fit-content" }} className="p-0 m-0">
							{tr("Description")}
						</span>
						{review ? (
							<span className="p-0 m-0" style={{ maxWidth: "590px" }}>
								{savedSettingsValues.description ?? "-"}
							</span>
						) : (
							<Textarea
								name="description"
								className={`${errors.description && touched.description ? "input__error" : ""}`}
								onChange={handleChange}
								onBlur={handleBlur}
								value={values.description}
								type="text"
								style={{ width: "591px" }}
							/>
						)}
					</RFlex>
				</RFlex>
			</RFlex>
		</RFlex>
	);
};

export default RUserGroupSettings;
