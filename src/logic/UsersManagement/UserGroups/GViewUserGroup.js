import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import React, { useState } from "react";
import { Terms, viewUserTypeTabs } from "../constants";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import { usersManagementApi } from "api/UsersManagement";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RLoader from "components/Global/RComs/RLoader";
import RTabs from "components/Global/RComs/RTabs/RTabs";
import { useMutateData } from "hocs/useMutateData";
import RPermissons from "../Shared/RPermissons";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";
import RUsersData from "../Shared/RUsersData";
import { useFormik } from "formik";
import * as Yup from "yup";
import RCSVDrawer from "../Shared/RCSVDrawer";
import GFirstMembersHeader from "../Shared/GFirstMembersHeader";
import RHeader from "../Shared/RHeader";
import RUserGroupSettings from "./RUserGroupsSettings";
import AppModal from "components/Global/ModalCustomize/AppModal";
import GAddMembersModal from "../Shared/GAddMembersModal";
import { deleteSweetAlert } from "components/Global/RComs/RAlert2";
import { useCUDToQueryKey } from "hocs/useCUDToQueryKey";
import styles from "./userGroupsStyle.module.scss";
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";
import * as colors from "config/constants";
import { useHistory } from "react-router-dom";
import RRoles from "../Shared/RRoles";
import { convertFiltersShape } from "../Shared/ConvertFiltersShape";
import { fileExcel } from "config/mimeTypes";
import RControlledAlertDialog from "components/RComponents/RContolledAlertDialog";
import RControlledDialog from "components/RComponents/RControlledDialog";
import { Button } from "ShadCnComponents/ui/button";
import RNewTabs from "components/RComponents/RNewTabs";

const GViewUserGroup = () => {
	//--------------------------------Global--------------------------------------------
	const { groupId: currentUserGroupId } = useParams();
	const [activeTab, setActiveTab] = useState(Terms.Roles);
	const [permissionsEditMode, setPermissionsEditMode] = useState(false);
	const [rolesEditMode, setRolesEditMode] = useState(false);
	const [settingsEditMode, setSettingsEditMode] = useState(false);
	const [isDrawerOpen, setIsDrawerOpen] = useState({ isOpen: false, operationType: "" });
	const [currentUserGroupName, setCurrentUserGroupName] = useState("");
	const [addMembersModal, setAddMembersModal] = useState(false);
	const [finishPermissons, setFinishPermissions] = useState(false);
	const [finishRoles, setFinishRoles] = useState(false);
	const { CUDToQueryKey, operations } = useCUDToQueryKey();
	const [isAlertDialogOpen, setIsAlertDialogOpen] = useState(false);
	const [openDialog, setOpenDialog] = useState(false);
	const history = useHistory();
	const handleCloseModal = () => {
		setAddMembersModal(false);
	};
	const toggleDrawer = () => {
		setIsDrawerOpen((prevState) => !prevState);
	};
	const dropdownActions = [
		{
			name: tr("Bulk_remove"),
			onClick: () => setIsAlertDialogOpen(true),
		},
		{
			name: tr("Import_CSV"),
			onClick: () => {
				setIsDrawerOpen({ isOpen: true, operationType: "Import" });
			},
		},
		{
			name: tr("Export_CSV"),
			onClick: () => {
				exportUserMutaiton.mutate();
			},
		},
	];

	//---------------------------------import export users-------------------------------
	const importUsersTempalteMutation = useMutateData({
		queryFn: () => usersManagementApi.downloadUsersTemplate(),
		downloadFile: true,
		mimeType: fileExcel[0],
		folderName: "Users Template.xlsx",
	});
	const exportUserMutaiton = useMutateData({
		queryFn: () => usersManagementApi.exportUsers(),
		downloadFile: true,
		mimeType: fileExcel[0],
		folderName: "Users.xlsx",
	});
	//----------------------------------userGroup Info-----------------------------------------
	const {
		data: userGroup,
		isLoading: isLoadingUserGroup,
		isFetching: isFetchingUserGroup,
	} = useFetchDataRQ({
		queryKey: [Terms.UserGroups, currentUserGroupId],
		queryFn: () => usersManagementApi.getSpecificGroup(currentUserGroupId),
		onSuccessFn: (data) => {
			setFinishPermissions(true);
			setFinishRoles(true);
			setUserGroupValue("userGroupName", data?.data?.data?.name);
			setUserGroupValue("description", data?.data?.data?.description);
			setAllMembersFilters({ ...allMembersFilters, groups: [currentUserGroupId] });
			setCurrentUserGroupName(data?.data?.data?.name);
			setSelectedPermissions(data?.data?.data?.permissions);
			setSavedPermissions(data?.data?.data?.permissions);
			setSelectedRoles(data?.data?.data?.roles);
			setSavedRoles(data?.data?.data?.roles);
			setHashId(data?.data?.data?.image);
		},
	});
	//------------------------------Settings-------------------------------------------------
	const initialValues = {
		userGroupName: null,
		description: "",
	};
	const [uploadId, setUploadId] = useState(null);
	const [hashId, setHashId] = useState(null);
	const validationSchema = Yup.object({
		userGroupName: Yup.string().required("userGroupName is Required"),
		description: Yup.string(),
	});
	const {
		values: settingsValues,
		setFieldValue: setUserGroupValue,
		handleBlur,
		handleChange,
		setFieldTouched,
		setFieldError,
		errors: settingsErrors,
		touched,
	} = useFormik({ initialValues, validationSchema });

	const updateUserGroupMutation = useMutateData({
		queryFn: ({ id, payload }) => usersManagementApi.updateUserGroup(id, payload),
		invalidateKeys: [Terms.UserGroups, currentUserGroupId],
		onSuccessFn: ({ data }) => {
			setAllMembersFilters({ ...allMembersFilters, groups: [currentUserGroupId] });
			setCurrentUserGroupName(settingsValues.userGroupName);
			setHashId(data?.data?.image);
			setSettingsEditMode(false);
		},
	});
	//----------------------------Roles--------------------------------------------------
	const [backRoleSearchTerm, setBackRoleSearchTerm] = useState("");
	const [frontRoleSearchTerm, setFrontRoleSearchTerm] = useState("");
	const [selectedRoles, setSelectedRoles] = useState([]);
	const [savedRoles, setSavedRoles] = useState([]);

	const {
		data: roles,
		isLoading: isLoadingRoles,
		isFetching: isFetchingRoles,
		refetch: refetchRoles,
	} = useFetchDataRQ({
		queryKey: [Terms.Roles, backRoleSearchTerm],
		keepPreviousData: true,
		queryFn: () => usersManagementApi.getOrganizationRoles(backRoleSearchTerm),
		onSuccessFn: (data) => {
			const sortedRoles = getSortedRoles(data);
			CUDToQueryKey({
				operation: operations.REPLACE,
				queryKey: [Terms.Roles, backRoleSearchTerm],
				newData: sortedRoles,
				insertionDepth: "data.data",
			});
		},
		enableCondition: !!userGroup && finishRoles,
		keepPreviousData: true,
	});
	//-----------------------------------Sort Roles---------------------------------------
	const getSortedRoles = (roles) => {
		const rolesCopy = JSON.parse(JSON.stringify(roles));
		const selectedIds = new Set(selectedRoles?.map((r) => r.id));
		const rolesArray = rolesCopy?.data?.data;
		rolesArray?.sort((r1, r2) => {
			const r1IsSelected = selectedIds.has(r1.id);
			const r2IsSelected = selectedIds.has(r2.id);
			if (r1IsSelected && !r2IsSelected) {
				return -1;
			}
			if (!r1IsSelected && r2IsSelected) {
				return 1;
			}
			return 0;
		});
		return rolesArray;
	};
	//-------------------------------------Select Roles----------------------------------
	const handleSelectRole = (role) => {
		const alreadyChecked = selectedRoles.some((p1) => p1.id == role.id);
		if (alreadyChecked) {
			const newRolesArray = selectedRoles.filter((p1) => p1.id != role.id);
			setSelectedRoles(newRolesArray);
		} else {
			const newRolesArray = [...selectedRoles, role];
			setSelectedRoles(newRolesArray);
		}
	};
	// const handleSelectAllRoles = (allRoles) => {
	// 	const allIsSelected = allRoles.length == selectedRoles.length;
	// 	if (allIsSelected) {
	// 		setSelectedRoles([]);
	// 	} else {
	// 		const newRolesArray = [...allRoles];
	// 		setSelectedRoles(newPermessionsArray);
	// 	}
	// };
	const assignRolesToUserGroupMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.assignRolesToUserGroup(payload),
		invalidateKeys: [Terms.UserGroups, currentUserGroupId],
	});
	const removeRolesFromUserGroupMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.removeRolesFromUserGroup(payload),
		invalidateKeys: [Terms.UserGroups, currentUserGroupId],
	});
	const getRemovedRoles = () => {
		const idsInSelectedRoles = new Set(selectedRoles.map((p) => p.id));
		const removedRoles = savedRoles.filter((p) => !idsInSelectedRoles.has(p.id));
		const removedRolesNames = removedRoles.map((p) => p.name);
		return removedRolesNames;
	};
	//----------------------------Permissions--------------------------------------------
	const [backendPermissoinSearchTerm, setBackendPermissoinSearchTerm] = useState("");
	const [frontPermissionSearchTerm, setFrontPermissionSearchTerm] = useState("");
	const [selectedPermissions, setSelectedPermissions] = useState([]);
	const [savedPermissions, setSavedPermissions] = useState([]);
	const {
		data: permissions,
		isLoading: isLoadingPermissions,
		isFetching: isFetchingPermissions,
	} = useFetchDataRQ({
		queryKey: [Terms.Permissions, backendPermissoinSearchTerm],
		queryFn: () => usersManagementApi.getOrganizationPermissions(backendPermissoinSearchTerm),
		onSuccessFn: (data) => {
			const sortedPermissions = getSortedPermissions(data);
			CUDToQueryKey({
				operation: operations.REPLACE,
				queryKey: [Terms.Permissions, backendPermissoinSearchTerm],
				newData: sortedPermissions,
				insertionDepth: "data.data",
			});
		},
		enableCondition: !!userGroup && finishPermissons,
		keepPreviousData: true,
	});
	//-----------------------------------Sort Permissions---------------------------------------
	const getSortedPermissions = (permissions) => {
		const permissionsCopy = JSON.parse(JSON.stringify(permissions));
		const selectedIds = new Set(selectedPermissions?.map((p) => p.id));
		const permissionsArray = permissionsCopy?.data?.data;
		permissionsArray?.sort((p1, p2) => {
			const p1IsSelected = selectedIds.has(p1.id);
			const p2IsSelected = selectedIds.has(p2.id);
			if (p1IsSelected && !p2IsSelected) {
				return -1;
			}
			if (!p1IsSelected && p1IsSelected) {
				return 1;
			}
			return 0;
		});
		return permissionsArray;
	};
	//-------------------------------------Select Permissons----------------------------------
	const handleSelectPermission = (permission) => {
		const alreadyChecked = selectedPermissions.some((p1) => p1.id == permission.id);
		if (alreadyChecked) {
			const newPermessionsArray = selectedPermissions.filter((p1) => p1.id != permission.id);
			setSelectedPermissions(newPermessionsArray);
		} else {
			const newPermessionsArray = [...selectedPermissions, permission];
			setSelectedPermissions(newPermessionsArray);
		}
	};
	const handleSelectAllPermissions = (allPermissions) => {
		const allIsSelected = allPermissions.length == selectedPermissions.length;
		if (allIsSelected) {
			setSelectedPermissions([]);
		} else {
			const newPermessionsArray = [...allPermissions];
			setSelectedPermissions(newPermessionsArray);
		}
	};

	const assignPermissionsToUserGroupMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.assignPermissionsToUserGroup(payload),
		// invalidateKeys: [Terms.userGroups, backendSearchTerm],
	});
	const removePermissionsFromUserGroupMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.removePermissionsFromUserGroup(payload),
	});
	const getRemovedPermissions = () => {
		const idsInSelectedPermissions = new Set(selectedPermissions.map((p) => p.id));
		const removedPermissions = savedPermissions.filter((p) => !idsInSelectedPermissions.has(p.id));
		const removedPermissionsNames = removedPermissions.map((p) => p.name);
		return removedPermissionsNames;
	};

	//------------------------------------Members----------------------------------------------
	const [alert1, setAlert] = useState(false);
	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);
	// const phoneNumberRegex = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/;
	const [activeIdsLoading, setActiveIdsLoading] = useState([]);
	const [removeIdsLoading, setRemoveIdsLoading] = useState([]);
	const [frontMembersSearchTerm, setFrontMembersSearchTerm] = useState("");
	const [selectedMembers, setSelectedMembers] = useState([]);
	const [filters, setFilters] = useState([{ id: 1, active: false, pickedOption: null, isApplied: false }]);
	const [allMembersFilters, setAllMembersFilters] = useState({});
	const initialValuesMembers = {
		email: "",
		address: "",
		phoneNumber: "",
	};

	const validationSchemaMembers = Yup.object({
		email: Yup.string(),
		address: Yup.string(),
		phoneNumber: Yup.string(),
	});
	const {
		values: membersValues,
		touched: membersTouched,
		errors: membersErrors,
		handleChange: handleChangeMembers,
		handleBlur: handleBlurMembers,
	} = useFormik({
		initialValues: initialValuesMembers,
		validationSchema: validationSchemaMembers,
	});
	//------------------------------------get users with filters applyed-----------------------

	const {
		data: members,
		isLoading: isLoadingMembers,
		isFetching: isFetchingMembers,
	} = useFetchDataRQ({
		queryKey: [Terms.Members, allMembersFilters],
		enableCondition: currentUserGroupName != "" ? true : false,
		queryFn: () => {
			const filters = convertFiltersShape(allMembersFilters);
			return usersManagementApi.getAllOrganizationUsers(filters);
		},
		keepPreviousData: true,
	});
	//------------------------------------get User types to put it in dropdown----------------
	const {
		data: userTypes,
		isLoading: isLoadingUserTypes,
		isFetching: isFetchinguserGroups,
	} = useFetchDataRQ({
		queryKey: [Terms.UserTypes],
		queryFn: () => usersManagementApi.getUsersTypes(false),
	});

	//------------------------------------Remove Users From Role---------------------------------
	const handleRemoveMembers = (members) => {
		const userIds = members.map((member) => member.id);
		const payload = {
			user_ids: userIds,
		};
		removeUsersMutation.mutate({ id: currentUserGroupId, payload });
		setRemoveIdsLoading([...removeIdsLoading, ...userIds]);
	};
	const deleteSingleMember = (member) => {
		const confirm = tr`Yes, delete it`;
		const message = (
			<div>
				<h6>
					{tr`Are you sure to delete`}{" "}
					<span className="p-0 m-0">
						{member.full_name} {tr("from")} {currentUserGroupName}
					</span>
				</h6>
				<p style={{ fontSize: "14px", fontWeight: "400" }}>{tr`This can't be undone`}</p>
			</div>
		);
		deleteSweetAlert(showAlerts, hideAlert, handleRemoveMembers, { members: [member] }, message, confirm);
	};
	const deleteMultipleMembers = (members) => {
		const confirm = tr`Yes, delete it`;
		const message = (
			<div>
				<h6>
					{tr`Are you sure to delete`}{" "}
					<span className="p-0 m-0">
						{tr("the_selected_users")} {tr("from")} {currentUserGroupName}
					</span>
				</h6>
				<p style={{ fontSize: "14px", fontWeight: "400" }}>{tr`This can't be undone`}</p>
			</div>
		);
		deleteSweetAlert(showAlerts, hideAlert, handleRemoveMembers, { members }, message, confirm);
	};
	const removeUsersMutation = useMutateData({
		queryFn: ({ id, payload }) => usersManagementApi.removeMembersFromUserGroups(id, payload),
		onSuccessFn: ({ variables }) => {
			setSelectedMembers(selectedMembers.filter((member) => !variables?.payload?.user_ids?.includes(member.id)));
			variables?.payload?.user_ids?.forEach((id, index) => {
				CUDToQueryKey({
					operation: operations.DELETE,
					queryKey: [Terms.Members, allMembersFilters],
					id,
					insertionDepth: "data.data.records",
				});
			});
			setRemoveIdsLoading(removeIdsLoading.filter((id) => !variables?.payload?.user_ids?.includes(id)));
		},
		onErrorFn: (error, variables) => {
			setRemoveIdsLoading(removeIdsLoading.filter((id) => !variables?.payload?.user_ids?.includes(id)));
		},
		invalidateKeys: [Terms.Members, allMembersFilters],
	});
	console.log("LoadingLoading", removeIdsLoading);
	//----------------------------Deactivate and activate Users--------------------------------
	const deactivateUsersMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.deactivateUsersAccounts(payload),
		invalidateKeys: [Terms.Members, allMembersFilters],
		onSuccessFn: ({ variables }) => {
			setActiveIdsLoading(activeIdsLoading.filter((id) => !variables?.payload?.user_ids?.includes(id)));
			variables?.payload?.user_ids?.forEach((id, index) => {
				CUDToQueryKey({
					operation: operations.UPDATE,
					queryKey: [Terms.Members, allMembersFilters],
					id,
					newData: { is_active: false },
					insertionDepth: "data.data.records",
				});
			});
		},
		onErrorFn: (error, variables) => {
			setActiveIdsLoading(activeIdsLoading.filter((id) => !variables?.payload?.user_ids?.includes(id)));
		},
	});
	const activateUsersMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.activateUsersAccounts(payload),
		invalidateKeys: [Terms.Members, allMembersFilters],
		onSuccessFn: ({ variables }) => {
			setActiveIdsLoading(activeIdsLoading.filter((id) => !variables?.payload?.user_ids?.includes(id)));
			variables?.payload?.user_ids?.forEach((id, index) => {
				CUDToQueryKey({
					operation: operations.UPDATE,
					queryKey: [Terms.Members, allMembersFilters],
					id,
					newData: { is_active: true },
					insertionDepth: "data.data.records",
				});
			});
		},
		onErrorFn: (error, variables) => {
			setActiveIdsLoading(activeIdsLoading.filter((id) => !variables?.payload?.user_ids?.includes(id)));
		},
	});
	const changeMultipleUserStatus = () => {
		if (selectedMembers.length <= 0) return;
		const userIds = selectedMembers.map((member) => member.id);
		const payload = {
			user_ids: userIds,
		};
		deactivateUsersMutation.mutate({ payload });
		setActiveIdsLoading([...activeIdsLoading, ...userIds]);
	};
	const changeSingleUserStatus = (user) => {
		const payload = {
			user_ids: [user.id],
		};
		if (user.is_active) {
			deactivateUsersMutation.mutate({ payload });
			setActiveIdsLoading([...activeIdsLoading, user.id]);
		} else {
			activateUsersMutation.mutate({ payload });
			setActiveIdsLoading([...activeIdsLoading, user.id]);
		}
	};
	//---------------------------------------Handle Selecting members--------------------------
	const handleSelectMember = (member) => {
		const alreadyChecked = selectedMembers.some((m1) => m1.id == member.id);
		if (alreadyChecked) {
			const newMembersArray = selectedMembers.filter((m1) => m1.id != member.id);
			setSelectedMembers(newMembersArray);
		} else {
			const newMembersArray = [...selectedMembers, member];
			setSelectedMembers(newMembersArray);
		}
	};
	const handleSelectAllMembers = (allMembers) => {
		const allIsSelected = allMembers.length == selectedMembers.length;
		if (allIsSelected) {
			setSelectedMembers([]);
		} else {
			const newMembersArray = [...allMembers];
			setSelectedMembers(newMembersArray);
		}
	};

	//------------------------------------Global-----------------------------------------------
	const handleEditSaveClicked = () => {
		let payload = {};
		switch (activeTab) {
			case Terms.Roles:
				if (!rolesEditMode) {
					setRolesEditMode(true);
					return;
				}
				const removedRolesNames = getRemovedRoles();
				if (removedRolesNames.length > 0) {
					payload = {
						role_names: removedRolesNames,
						group_ids: [currentUserGroupId],
					};
					removeRolesFromUserGroupMutation.mutate({ payload });
				}
				if (selectedRoles.length > 0) {
					const roleNames = selectedRoles.map((role) => role.name);
					payload = {
						role_names: roleNames,
						group_ids: [currentUserGroupId],
					};
					assignRolesToUserGroupMutation.mutate({ payload });
				}
				const sortedRoles = getSortedRoles(roles);
				CUDToQueryKey({
					operation: operations.REPLACE,
					queryKey: [Terms.Roles, backRoleSearchTerm],
					newData: sortedRoles,
					insertionDepth: "data.data",
				});
				setSavedRoles(selectedRoles);
				setRolesEditMode(false);
				break;
			case Terms.Permissions:
				if (!permissionsEditMode) {
					setPermissionsEditMode(true);
					return;
				}
				//----------------------------------Removed Permissions----------------------------
				const removedPermissionsNames = getRemovedPermissions();
				if (removedPermissionsNames.length > 0) {
					payload = {
						permission_names: removedPermissionsNames,
						group_ids: [currentUserGroupId],
					};
					removePermissionsFromUserGroupMutation.mutate({ payload });
				}
				//----------------------------------add Permissions--------------------------------
				if (selectedPermissions.length > 0) {
					const permissionsNames = selectedPermissions.map((permission) => permission.name);
					payload = {
						permission_names: permissionsNames,
						group_ids: [currentUserGroupId],
					};
					assignPermissionsToUserGroupMutation.mutate({ payload });
				}
				const sortedPermissions = getSortedPermissions(permissions);
				CUDToQueryKey({
					operation: operations.REPLACE,
					queryKey: [Terms.Permissions, backendPermissoinSearchTerm],
					newData: sortedPermissions,
					insertionDepth: "data.data",
				});
				setPermissionsEditMode(false);
				setSavedPermissions(selectedPermissions);
				break;
			case Terms.Settings:
				if (!settingsEditMode) {
					setSettingsEditMode(true);
					return;
				}
				if (!settingsErrors.userGroupName) {
					payload = {
						id: currentUserGroupId,
						image: uploadId.url ? [uploadId] : undefined,
						name: settingsValues.userGroupName,
						description: settingsValues.description,
					};
					updateUserGroupMutation.mutate({ id: currentUserGroupId, payload });
				}
				break;
		}
		return;
	};
	if (isLoadingUserGroup) return <RLoader />;
	return (
		<RFlex className="flex-column">
			<RFlex className="align-items-center">
				<div
					style={{ cursor: "pointer" }}
					className={styles.backArrow}
					onClick={() => history.replace(`${baseURL}/${genericPath}/users-management/user-groups`)}
				>
					<i className={iconsFa6.chevronLeft} style={{ color: colors.primaryColor }} />
				</div>
				<span style={{ fontSize: "15px" }} className="p-0 m-0">
					{userGroup?.data?.data?.name}
				</span>
			</RFlex>
			<RFlex className="justify-content-between">
				<RNewTabs tabs={viewUserTypeTabs} setActiveTab={setActiveTab} value={activeTab} />
				{activeTab == Terms.Roles && (
					<Button
						className={`flex gap-1 ${!rolesEditMode ? "border-themePrimary" : ""}`}
						variant={!rolesEditMode ? "outline" : "default"}
						onClick={handleEditSaveClicked}
						disabled={
							updateUserGroupMutation.isLoading || assignRolesToUserGroupMutation.isLoading || removeRolesFromUserGroupMutation.isLoading
						}
					>
						{updateUserGroupMutation.isLoading || assignRolesToUserGroupMutation.isLoading || removeRolesFromUserGroupMutation.isLoading ? (
							<i className={iconsFa6.spinner} />
						) : !rolesEditMode ? (
							<i className={iconsFa6.edit} />
						) : (
							""
						)}
						{rolesEditMode ? tr("save") : tr("edit")}
					</Button>
				)}
				{activeTab == Terms.Settings && (
					<Button
						className={`flex gap-1 ${!settingsEditMode ? "border-themePrimary" : ""}`}
						variant={!settingsEditMode ? "outline" : "default"}
						onClick={handleEditSaveClicked}
						disabled={updateUserGroupMutation.isLoading}
					>
						{updateUserGroupMutation.isLoading ? (
							<i className={iconsFa6.spinner} />
						) : !settingsEditMode ? (
							<i className={iconsFa6.edit} />
						) : (
							""
						)}
						{settingsEditMode ? tr("save") : tr("edit")}
					</Button>
				)}
				{activeTab == Terms.Permissions && (
					<Button
						className={`flex gap-1 ${!permissionsEditMode ? "border-themePrimary" : ""}`}
						variant={!permissionsEditMode ? "outline" : "default"}
						onClick={handleEditSaveClicked}
						disabled={
							assignPermissionsToUserGroupMutation.isLoading ||
							removePermissionsFromUserGroupMutation.isLoading ||
							updateUserGroupMutation.isLoading
						}
					>
						{assignPermissionsToUserGroupMutation.isLoading ||
						removePermissionsFromUserGroupMutation.isLoading ||
						updateUserGroupMutation.isLoading ? (
							<i className={iconsFa6.spinner} />
						) : !permissionsEditMode ? (
							<i className={iconsFa6.edit} />
						) : (
							""
						)}
						{permissionsEditMode ? tr("save") : tr("edit")}
					</Button>
				)}
			</RFlex>
			{activeTab == Terms.Roles &&
				(isLoadingRoles || isLoadingUserGroup ? (
					<RLoader />
				) : (
					<RRoles
						roles={roles?.data?.data}
						handleSelectRole={handleSelectRole}
						// handleSelectAllRoles={handleSelectAsllRoles}
						frontSearchTerm={frontRoleSearchTerm}
						setFrontSearchTerm={setFrontRoleSearchTerm}
						setBackSearchTerm={setBackRoleSearchTerm}
						selectedRoles={selectedRoles}
						canEdit={rolesEditMode}
						view
					/>
				))}
			{activeTab == Terms.Permissions &&
				(isLoadingPermissions || isLoadingUserGroup ? (
					<RLoader />
				) : (
					<RPermissons
						permissions={permissions?.data?.data}
						setBackSearchTerm={setBackendPermissoinSearchTerm}
						selectOneFn={handleSelectPermission}
						selectAllFn={handleSelectAllPermissions}
						selectedPermissions={selectedPermissions}
						frontSearchTerm={frontPermissionSearchTerm}
						setFrontSearchTerm={setFrontPermissionSearchTerm}
						isFetching={isFetchingPermissions}
						canEdit={permissionsEditMode}
						view
					/>
				))}
			{activeTab == Terms.Members &&
				(isLoadingMembers || isLoadingUserTypes ? (
					<RLoader />
				) : (
					<RFlex className="flex-column">
						<RHeader
							actions={dropdownActions}
							disableCreate={updateUserGroupMutation.isLoading}
							loadingCreate={updateUserGroupMutation.isLoading}
							showCreate={true}
							createText={tr("Add_Members")}
							showSearchHeader={false}
							showBulk={true}
							showColumnSettings={true}
							showRefresh={false}
							onCreateClick={() => setOpenDialog(true)}
							showDeactivate={false}
							showDelete={false}
							deleteText="remove"
							deleteFn={() => deleteMultipleMembers(selectedMembers)}
							deactivateFn={changeMultipleUserStatus}
							deleteLoading={removeUsersMutation.isLoading}
							deactivateLoading={deactivateUsersMutation.isLoading || activateUsersMutation.isLoading}
						/>
						<GFirstMembersHeader
							allUsersLength={members?.data?.data?.records?.length}
							allMembersFilters={allMembersFilters}
							setAllMembersFilters={setAllMembersFilters}
							filters={filters}
							setFilters={setFilters}
							frontSearchTerm={frontMembersSearchTerm}
							setFrontSearchTerm={setFrontMembersSearchTerm}
							isFetching={isFetchingMembers}
							selectedUsers={selectedMembers}
							values={membersValues}
							touched={membersTouched}
							errors={membersErrors}
							handleChange={handleChangeMembers}
							handleBlur={handleBlurMembers}
							showAddUsers={false}
						/>
						<RUsersData
							users={members?.data?.data}
							userTypes={userTypes?.data?.data}
							allMembersFilters={allMembersFilters}
							setAllMembersFilters={setAllMembersFilters}
							selectedUsers={selectedMembers}
							selectOneFn={handleSelectMember}
							selectAllFn={handleSelectAllMembers}
							deleteSingleMember={handleRemoveMembers}
							isRemoving={removeUsersMutation.isLoading}
							changeSingleUserStatus={changeSingleUserStatus}
							activeIdsLoading={activeIdsLoading}
							removeIdsLoading={removeIdsLoading}
							disableRemoving={updateUserGroupMutation.isLoading}
							view
						/>
						<RCSVDrawer
							isOpen={isDrawerOpen.isOpen}
							operationType={isDrawerOpen.operationType}
							toggleDrawer={toggleDrawer}
							downloadTemplateFn={() => importUsersTempalteMutation.mutate()}
							currentType={Terms.Users}
						/>
					</RFlex>
				))}
			{activeTab == Terms.Settings && (
				<RUserGroupSettings
					values={settingsValues}
					errors={settingsErrors}
					touched={touched}
					review={!settingsEditMode}
					handleBlur={handleBlur}
					handleChange={handleChange}
					showSettings={false}
					savedSettingsValues={settingsValues}
					setUploadId={setUploadId}
					hashId={hashId}
					view
				/>
			)}
			<RControlledDialog
				isOpen={openDialog}
				closeDialog={() => {
					setOpenDialog(false);
				}}
				dialogBody={
					<GAddMembersModal
						currentUserGroupId={currentUserGroupId}
						currentUserGroupName={currentUserGroupName}
						keyToInvalidate={[Terms.Members, allMembersFilters]}
					/>
				}
			/>

			<RControlledAlertDialog
				title={`${tr("Are_you_sure_you_want_to_delete")} ${selectedMembers.length}`}
				isOpen={isAlertDialogOpen}
				handleCloseAlert={() => setIsAlertDialogOpen(false)}
				confirmAction={() => handleRemoveMembers(selectedMembers)}
			/>
		</RFlex>
	);
};

export default GViewUserGroup;
