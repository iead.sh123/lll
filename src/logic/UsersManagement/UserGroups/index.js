import RFlex from "components/Global/RComs/RFlex/RFlex";
import React, { useState } from "react";
import RHeader from "../Shared/RHeader";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { Terms, groupsHeader, rolesHeader } from "../constants";
import { usersManagementApi } from "api/UsersManagement";
import RLister from "components/Global/RComs/RLister";
import RLoader from "components/Global/RComs/RLoader";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";
import { useMutateData } from "hocs/useMutateData";
import { useCUDToQueryKey } from "hocs/useCUDToQueryKey";
import { deleteSweetAlert } from "components/Global/RComs/RAlert2";
import RCSVDrawer from "../Shared/RCSVDrawer";
import { useHistory } from "react-router-dom";
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";
import styles from "./userGroupsStyle.module.scss";
import { fileExcel } from "config/mimeTypes";
import RProfileName from "components/Global/RComs/RProfileName/RProfileName";
const GUserGroups = () => {
	const [alert1, setAlert] = useState(false);
	const [backendSearchTerm, setBackSearchTerm] = useState("");
	const [isDrawerOpen, setIsDrawerOpen] = useState({ isOpen: false, operationType: "" });
	const [page, SetPage] = useState(1);
	const [isCreating, setIsCreating] = useState(false);
	const [groupsIdsDeleting, setGroupsIdsDeleting] = useState([]);
	const history = useHistory();
	const handleChangePage = (page) => {
		SetPage(page);
	};

	const toggleDrawer = () => {
		setIsDrawerOpen((prevState) => !prevState);
	};

	//----------------------------------Fetch userGroups---------------------------------------------
	const {
		data: groups,
		isLoading: isLoadingGroups,
		isFetching: isFetchingGroups,
		refetch: refetchGroups,
	} = useFetchDataRQ({
		queryKey: [Terms.UserGroups, backendSearchTerm],
		keepPreviousData: true,
		queryFn: () => usersManagementApi.getUsersGroups(backendSearchTerm),
		onSuccessFn: (data) => {
			console.log(data);
		},
	});
	const { CUDToQueryKey, operations } = useCUDToQueryKey();
	const deleteGroupMutation = useMutateData({
		queryFn: ({ userGroupId }) => usersManagementApi.deleteUserGroup(userGroupId),
		invalidateKeys: [Terms.UserGroups, backendSearchTerm],
		onSuccessFn: ({ variables }) => {
			CUDToQueryKey({
				queryKey: [Terms.UserGroups, backendSearchTerm],
				insertionDepth: `data.data`,
				id: variables.payload?.userGroupId,
				operation: operations.DELETE,
			});
			setGroupsIdsDeleting(groupsIdsDeleting.filter((id) => variables.payload?.userGroupId != id));
		},
		onErrorFn: (error, variables) => {
			setGroupsIdsDeleting(groupsIdsDeleting.filter((id) => variables.payload?.userGroupId != id));
		},
	});
	const handleDeleteGroup = ({ group }) => {
		deleteGroupMutation.mutate({ userGroupId: group.id });
		setGroupsIdsDeleting([...groupsIdsDeleting, group.id]);
	};
	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);
	const deleteGroup = (group) => {
		const confirm = tr`Yes_delete_it`;
		const message = (
			<div>
				<h6>
					{tr`Are_you_sure_to_delete`} <span className="p-0 m-0">{group.name}</span>
				</h6>
				<p style={{ fontSize: "14px", fontWeight: "400" }}>{tr`This_can't_be_undone`}</p>
			</div>
		);
		deleteSweetAlert(showAlerts, hideAlert, handleDeleteGroup, { group }, message, confirm);
	};
	if (isLoadingGroups) return <RLoader />;

	// const records = groups?.data?.data?.map((group, index) => {
	// 	const groupsCount = groups?.data?.data?.length;
	// 	const DescriptionComponent = (
	// 		<RFlex className="align-items-center" styleProps={{ minHeight: "52px", maxWidth: "400px" }}>
	// 			{group.description}
	// 		</RFlex>
	// 	);
	// 	const groupNameComponent = (
	// 		<RFlex
	// 			styleProps={{ cursor: "pointer" }}
	// 			className={`${styles.hoverLink}`}
	// 			onClick={() => {
	// 				history.push(`${baseURL}/${genericPath}/users-management/user-groups/${group.id}`);
	// 			}}
	// 		>
	// 			<span className="p-0 m-0">{group?.name}</span>
	// 		</RFlex>
	// 	);
	// 	return {
	// 		details: groupsHeader.map((header, index) => ({
	// 			key:
	// 				header.name == "groupName" ? (
	// 					<RFlex className="align-items-center">
	// 						{tr(`${header.key}`)}{" "}
	// 						<span className="text-primary" style={{ fontSize: "8px" }}>
	// 							{groupsCount}
	// 						</span>
	// 					</RFlex>
	// 				) : (
	// 					header.key
	// 				),
	// 			keyType: header.name == "groupName" ? "component" : "",
	// 			value:
	// 				header.name == "groupName" ? groupNameComponent : header.name == "description" ? DescriptionComponent : group?.[header.value],
	// 			type: header.name == "groupName" || header.name == "description" ? "component" : "",
	// 		})),
	// 		actions: [
	// 			{
	// 				name: "",
	// 				icon: iconsFa6.delete,
	// 				loading: groupsIdsDeleting.some((id) => id == group.id),
	// 				disabled: groupsIdsDeleting.some((id) => id == group.id),
	// 				justIcon: true,
	// 				onClick: () => {
	// 					deleteGroup(group);
	// 				},
	// 				actionIconClass: "text-danger",
	// 			},
	// 		],
	// 	};
	// });

	const columns = [
		{
			accessorKey: "name",
			renderHeader: () => (
				<RFlex className="align-items-center">
					<RFlex styleProps={{ gap: "5px" }}>
						{tr("user_group")}
						<span className={`${styles.lengthDiv} text-themePrimary`} style={{ fontSize: "8px" }}>
							{groups?.data?.data?.length}
						</span>
					</RFlex>
				</RFlex>
			),

			renderCell: ({ row }) => (
				<RProfileName
					name={row.original.name}
					img={row.original.image}
					onClick={() => {
						history.push(`${baseURL}/${genericPath}/users-management/user-groups/${row.original?.id}`);
					}}
					flexStyleProps={{ cursor: "pointer", width: "fit-content" }}
					className={styles.hoverLink}
				/>
			),
		},
		{
			accessorKey: "users_count",
			renderHeader: () => <span>{tr("Users")}</span>,

			renderCell: ({ row }) => <span>{row.getValue("users_count")}</span>,
		},
		{
			accessorKey: "description",
			renderHeader: () => <span>{tr("description")}</span>,
			renderCell: ({ row }) => <span className="max-w-[400px] text-themeBoldGrey">{row.getValue("description") ?? "-"}</span>,
		},
	];
	const actions = [
		{
			name: "delete",
			icon: iconsFa6.delete,
			actionIconClass: "text-themeDanger",
			confirmAction: ({ row }) => {
				handleDeleteGroup(row.original);
			},
			inDialog: true,
			dialogTitle: ({ row }) => `${tr("Are_You_Sure_To_Delete")} ${row.original?.name}`,
		},
	];
	const records = { columns, data: groups?.data?.data, actions };
	return (
		<>
			<RFlex className="flex-column">
				<RHeader
					createText={"New_User_Group"}
					history={history}
					setBackSearchTerm={setBackSearchTerm}
					isFetching={isFetchingGroups}
					refetchFunction={refetchGroups}
					showBulk={false}
					dataType={Terms.UserGroups}
					onCreateClick={() => history.push(`${baseURL}/${genericPath}/users-management/user-groups/add`)}
					searchPlaceholder={"Search_For_a_user_group"}
				/>
				<RLister
					Records={records}
					convertRecordsShape={false}
					// info={roles}
					// withPagination={true}
					// handleChangePage={handleChangePage}
					// page={page}
					line1={"No_User_Groups_Yet"}
				/>
			</RFlex>
			{alert1}
		</>
	);
};

export default GUserGroups;
