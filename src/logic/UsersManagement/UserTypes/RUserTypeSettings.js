import React from "react";
import { Input } from "ShadCnComponents/ui/input";
import tr from "components/Global/RComs/RTranslator";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import RSelect from "components/Global/RComs/RSelect";
import iconsFa6 from "variables/iconsFa6";
import RButton from "components/Global/RComs/RButton";
import AppNewCheckbox from "components/Global/RComs/AppNewCheckbox/AppNewCheckbox";
import { Textarea } from "ShadCnComponents/ui/textarea";
import { Button } from "ShadCnComponents/ui/button";
import { Checkbox } from "ShadCnComponents/ui/checkbox";
import { Label } from "ShadCnComponents/ui/label";
const RUserTypeSettings = ({
	values,
	errors,
	touched,
	handleBlur,
	handleChange,
	review,
	showSettings = true,
	handleSelectLevel,
	savedSettingsValues,
	handleAddingAnotherLevel,
	levels,
	handleSetDefault,
	view,
}) => {
	console.log("errorserrors", errors);
	console.log("touchedtouched", touched);
	return (
		<RFlex className="flex-column">
			<RFlex className="flex-column">
				{review && !savedSettingsValues.userTypeName && showSettings && (
					<span className="p-0 m-0" style={{ fontWeight: "bold", width: "fit-content" }}>
						{tr("settings")}
					</span>
				)}
				<RFlex className="flex-column">
					<RFlex className="align-items-center" styleProps={{ width: "100%" }}>
						<span style={{ width: "130px", height: "fit-content" }} className="p-0 m-0 text-themeBoldGrey">
							{tr("User_Type_Title")}
						</span>
						{review ? (
							<span className="p-0 m-0" style={{ maxWidth: "590px" }}>
								{savedSettingsValues.userTypeName ?? "-"}
							</span>
						) : (
							<RFlex className="flex-col gap-1">
								<Input
									name="userTypeName"
									placeholder={tr("user_type_title")}
									className={`${errors.userTypeName && touched.userTypeName ? "input__error" : ""}`}
									onChange={handleChange}
									onBlur={handleBlur}
									value={values.userTypeName}
									type="text"
									style={{ width: "591px" }}
								/>
								{errors.userTypeName && touched.userTypeName && <span className="text-themeDanger text-[12px]">{errors.userTypeName}</span>}
							</RFlex>
						)}
					</RFlex>
					<RFlex className="align-items-start">
						<span style={{ width: "130px", height: "fit-content" }} className="p-0 m-0 text-themeBoldGrey ">
							{tr("Description")}
						</span>
						{review ? (
							<span className="p-0 m-0" style={{ maxWidth: "590px" }}>
								{savedSettingsValues.description ?? "-"}
							</span>
						) : (
							<Textarea
								name="description"
								className={`${errors.description && touched.description ? "input__error" : ""}`}
								onChange={handleChange}
								onBlur={handleBlur}
								value={values.description}
								type="text"
								style={{ width: "591px" }}
							/>
						)}
					</RFlex>
				</RFlex>
			</RFlex>
			{!review && !view && (
				<RFlex className="flex-column" styleProps={{ gap: "0px" }}>
					<RFlex className="flex flex-col gap-2">
						{levels.map((level) => (
							<RFlex className="align-items-center">
								<span style={{ width: "130px", height: "fit-content" }} className="p-0 m-0">
									{tr("Level")}
								</span>
								<div style={{ width: "591px" }}>
									<RSelect
										option={[]}
										closeMenuOnSelect={true}
										value={null}
										placeholder={tr("choose_level")}
										onChange={(e) => handleSelectLevel(e)}
									/>
								</div>
							</RFlex>
						))}
					</RFlex>
					<Button
						variant="ghost"
						className="flex gap-1 text-themePrimary p-0 hover:text-themePrimary"
						style={{ width: "fit-content", height: "fit-content" }}
						onClick={() => handleAddingAnotherLevel()}
					>
						<i className={`${iconsFa6.plus} text-themePrimary`} />
						{tr("Add_another_level")}
					</Button>
				</RFlex>
			)}
			{!review && !view && (
				<RFlex className="items-center" styleProps={{ gap: "5px" }}>
					<Checkbox
						id="default_type"
						onCheckedChange={() => {
							handleSetDefault();
						}}
					/>
					<Label htmlFor={`default_type`}>{tr("Set_as_default_type_for_manual_registering")}</Label>
				</RFlex>
			)}
		</RFlex>
	);
};

export default RUserTypeSettings;
