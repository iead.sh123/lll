import RFlex from "components/Global/RComs/RFlex/RFlex";
import React, { useState } from "react";
import RHeader from "../Shared/RHeader";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { Terms, userTypesHeader } from "../constants";
import { usersManagementApi } from "api/UsersManagement";
import RLister from "components/Global/RComs/RLister";
import RLoader from "components/Global/RComs/RLoader";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";
import { useMutateData } from "hocs/useMutateData";
import { useCUDToQueryKey } from "hocs/useCUDToQueryKey";
import { deleteSweetAlert } from "components/Global/RComs/RAlert2";
import { useHistory } from "react-router-dom";
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";
import styles from "./userTypes.module.scss";
import GCreateUserType from "./GCreateUserType";
const GUserTypes = () => {
	const [alert1, setAlert] = useState(false);
	const [backendSearchTerm, setBackSearchTerm] = useState("");
	const [page, SetPage] = useState(1);
	const [typesIdsDeleteing, setTypesIdsDeleteing] = useState([]);
	const history = useHistory();
	const handleChangePage = (page) => {
		SetPage(page);
	};

	const {
		data: userTypes,
		isLoading: isLoadingUserTypes,
		isFetching: isFetchingUserTypes,
		refetch: refetchUserTypes,
	} = useFetchDataRQ({
		queryKey: [Terms.UserTypes, backendSearchTerm],
		keepPreviousData: true,
		queryFn: () => usersManagementApi.getOrganizationUserTypes(backendSearchTerm),
		onSuccessFn: (data) => {
			console.log(data);
		},
	});
	const { CUDToQueryKey, operations } = useCUDToQueryKey();
	const deleteUserTypeMutation = useMutateData({
		queryFn: ({ id }) => usersManagementApi.deleteOrganizationUserType(id),
		invalidateKeys: [Terms.UserTypes, backendSearchTerm],
		onSuccessFn: ({ variables }) => {
			CUDToQueryKey({
				queryKey: [Terms.UserTypes, backendSearchTerm],
				insertionDepth: `data.data`,
				id: variables.id,
				operation: operations.DELETE,
			});
			setTypesIdsDeleteing(typesIdsDeleteing.filter((id) => id != variables?.id));
		},
		onErrorFn: (error, variables) => {
			setTypesIdsDeleteing(typesIdsDeleteing.filter((id) => id != variables?.id));
		},
	});
	const handleDeleteUserType = (userType) => {
		deleteUserTypeMutation.mutate({ id: userType.id });
		setTypesIdsDeleteing([...typesIdsDeleteing, userType.id]);
	};
	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);

	if (isLoadingUserTypes) return <RLoader />;

	const columns = [
		{
			accessorKey: "type",
			renderHeader: () => (
				<RFlex className="align-items-center">
					<RFlex styleProps={{ gap: "5px" }}>
						{tr("user_type")}
						<span className={`${styles.lengthDiv} text-themePrimary`} style={{ fontSize: "8px" }}>
							{userTypes?.data?.data?.length}
						</span>
					</RFlex>
				</RFlex>
			),

			renderCell: ({ row }) => (
				<RFlex className={`${styles.hoverLink}`}>
					<span
						onClick={() => {
							history.push(`${baseURL}/${genericPath}/users-management/user-types/${row.original?.id}`);
						}}
						className="cursor-pointer"
					>
						{row.getValue("type")?.name}
					</span>
				</RFlex>
			),
		},
		{
			accessorKey: "users_count",
			renderHeader: () => <span>{tr("Users")}</span>,

			renderCell: ({ row }) => <span>{row.getValue("users_count")}</span>,
		},
		{
			accessorKey: "description",
			renderHeader: () => <span>{tr("description")}</span>,
			renderCell: ({ row }) => <span className="max-w-[400px] text-themeBoldGrey">{row.getValue("type").description ?? "-"}</span>,
		},
	];
	const actions = [
		{
			name: "delete",
			icon: iconsFa6.delete,
			actionIconClass: "text-themeDanger",
			confirmAction: ({ row }) => {
				handleDeleteUserType(row.original);
			},
			inDialog: true,
			dialogTitle: ({ row }) => `${tr("Are_You_Sure_To_Delete")} ${row.original?.type?.name}`,
		},
	];
	const records = { columns, data: userTypes?.data?.data, actions };
	return (
		<>
			<RFlex className="flex-column">
				<RHeader
					createText={"New_User_Type"}
					history={history}
					setBackSearchTerm={setBackSearchTerm}
					isFetching={isFetchingUserTypes}
					refetchFunction={refetchUserTypes}
					showBulk={false}
					dataType={Terms.UserTypes}
					onCreateClick={() => history.push(`${baseURL}/${genericPath}/users-management/user-types/add`)}
					searchPlaceholder={"Search_For_a_User_Type"}
				/>
				<RLister
					Records={records}
					convertRecordsShape={false}
					// info={userTypes}
					// withPagination={true}
					// handleChangePage={handleChangePage}
					// page={page}
					line1={"No_User_Types_Yet"}
				/>
			</RFlex>
			{alert1}
		</>
	);
};

export default GUserTypes;
