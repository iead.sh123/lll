import RButton from "components/Global/RComs/RButton";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import React, { useEffect, useState } from "react";
import iconsFa6 from "variables/iconsFa6";
import { createUserTypeTabs, Terms } from "../constants";
import RFilterTabs from "components/Global/RComs/RFilterTabs/RFilterTabs";
import { useFormik } from "formik";
import * as Yup from "yup";
import styles from "./userTypes.module.scss";
import RUserTypeSettings from "./RUserTypeSettings";
import { usersManagementApi } from "api/UsersManagement";
import { useMutateData } from "hocs/useMutateData";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import RPermissons from "../Shared/RPermissons";
import RLoader from "components/Global/RComs/RLoader";
import GFirstMembersHeader from "../Shared/GFirstMembersHeader";
import RUsersData from "../Shared/RUsersData";
import RTabs from "components/Global/RComs/RTabs/RTabs";
import RCSVDrawer from "../Shared/RCSVDrawer";
import RHeader from "../Shared/RHeader";
import RReviewPermissions from "../Shared/RReviewPermissions";
import RReviewUserData from "../Shared/RReviewUserData";
import RRoles from "../Shared/RRoles";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import { useHistory } from "react-router-dom";
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";
import * as colors from "config/constants";
import { convertFiltersShape } from "../Shared/ConvertFiltersShape";
import { fileExcel } from "config/mimeTypes";
import { toast } from "react-toastify";
import { Button } from "ShadCnComponents/ui/button";
import RNewTabs from "components/RComponents/RNewTabs";
const GCreateUserType = ({ setIsCreating, backendSearchTerm }) => {
	//----------------------------Global--------------------------------------------------
	const [activeTab, setActiveTab] = useState(Terms.Settings);
	const [currentUserTypeName, setCurrentUserTypeName] = useState("");
	const [currentUserTypeId, setCurrentUserTypeId] = useState(null);
	const [currentOrganizationUserTypeId, setCurrentOrganizationUserTypeId] = useState(null);
	const [isDrawerOpen, setIsDrawerOpen] = useState({ isOpen: false, operationType: "" });
	const history = useHistory();
	const toggleDrawer = () => {
		setIsDrawerOpen((prevState) => !prevState);
	};
	const dropdownActions = [
		{
			name: tr("Import_CSV"),
			onClick: () => {
				setIsDrawerOpen({ isOpen: true, operationType: "Import" });
			},
		},
		{
			name: tr("Export_CSV"),
			onClick: () => {
				exportUserMutaiton.mutate();
			},
		},
	];

	//---------------------------------import export users-------------------------------
	const importUsersTempalteMutation = useMutateData({
		queryFn: () => usersManagementApi.downloadUsersTemplate(),
		downloadFile: true,
		mimeType: fileExcel[0],
		folderName: "Users Template.xlsx",
	});
	const exportUserMutaiton = useMutateData({
		queryFn: () => usersManagementApi.exportUsers(),
		downloadFile: true,
		mimeType: fileExcel[0],
		folderName: "Users.xlsx",
	});
	//----------------------------Settings--------------------------------------------------
	const [levels, setLevels] = useState([{ id: 1, pickedOption: null }]);

	const initialValues = {
		userTypeName: null,
		description: "",
	};
	const [savedSettingsValues, setSavedSettingsValues] = useState({
		userTypeName: null,
		description: null,
	});
	const validationSchema = Yup.object({
		userTypeName: Yup.string().required(tr("userType_Title_is_Required")),
		description: Yup.string(),
	});
	const {
		values: settingsValues,
		handleBlur,
		handleChange,
		setFieldTouched,
		setFieldError,
		errors,
		touched,
	} = useFormik({ initialValues, validationSchema });

	const addUserTypeMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.addOrganizationUserType(payload),
		// invalidateKeys: [Terms.UserTypes, backendSearchTerm],
		onSuccessFn: ({ data }) => {
			setCurrentUserTypeName(settingsValues.userTypeName);
			setCurrentUserTypeId(data?.data?.user_type_id);
			setCurrentOrganizationUserTypeId(data?.data?.id);
			setSavedSettingsValues({ ...settingsValues });
			toast.success(`${settingsValues.userTypeName} ${tr("Is_created_successfully")}`);
			setActiveTab(Terms.Roles);
		},
	});
	const updateUserTypeMuation = useMutateData({
		queryFn: ({ id, payload }) => usersManagementApi.updateOrganizationUserType(id, payload),
		// invalidateKeys: [Terms.UserTypes, backendSearchTerm],
		onSuccessFn: ({ data }) => {
			setCurrentUserTypeName(settingsValues.userTypeName);
			setSavedSettingsValues({ ...settingsValues });
			setActiveTab(Terms.Roles);
		},
	});
	const handleSelectLevel = (e) => {};
	const handleAddingAnotherLevel = () => {
		const highstId = levels.reduce((max, level) => {
			return max > level ? max : level.id;
		}, 0);
		setLevels([...levels, { id: highstId + 1, pickedOption: null }]);
	};
	const handleSetDefault = () => {};
	//--------------------------------Roles------------------------------------------
	const [backRoleSearchTerm, setBackRoleSearchTerm] = useState("");
	const [frontRoleSearchTerm, setFrontRoleSearchTerm] = useState("");
	const [selectedRoles, setSelectedRoles] = useState([]);
	const [savedRoles, setSavedRoles] = useState([]);

	const {
		data: roles,
		isLoading: isLoadingRoles,
		isFetching: isFetchingRoles,
		refetch: refetchRoles,
	} = useFetchDataRQ({
		queryKey: [Terms.Roles, backRoleSearchTerm],
		keepPreviousData: true,
		queryFn: () => usersManagementApi.getOrganizationRoles(backRoleSearchTerm),
		onSuccessFn: (data) => {
			console.log(data);
		},
	});

	const handleSelectRole = (role) => {
		const alreadyChecked = selectedRoles.some((p1) => p1.id == role.id);
		if (alreadyChecked) {
			const newRolesArray = selectedRoles.filter((p1) => p1.id != role.id);
			setSelectedRoles(newRolesArray);
		} else {
			const newRolesArray = [...selectedRoles, role];
			setSelectedRoles(newRolesArray);
		}
	};
	// const handleSelectAllRoles = (allRoles) => {
	// 	const allIsSelected = allRoles.length == selectedRoles.length;
	// 	if (allIsSelected) {
	// 		setSelectedRoles([]);
	// 	} else {
	// 		const newRolesArray = [...allRoles];
	// 		setSelectedRoles(newPermessionsArray);
	// 	}
	// };
	const assignRolesToUserTypeMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.assignRolesToUserType(payload),
		// invalidateKeys: [Terms.UserTypes, backendSearchTerm],
	});
	const removeRolesFromUserTypeMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.removeRolesFromUserType(payload),
	});
	const getRemovedRoles = () => {
		const idsInSelectedRoles = new Set(selectedRoles.map((r) => r.id));
		const removedRoles = savedRoles.filter((r) => !idsInSelectedRoles.has(r.id));
		const removedRolesNames = removedRoles.map((r) => r.name);
		return removedRolesNames;
	};
	//----------------------------Permissions--------------------------------------------
	const [backendPermissoinSearchTerm, setBackendPermissoinSearchTerm] = useState("");
	const [frontPermissionSearchTerm, setFrontPermissionSearchTerm] = useState("");
	const [selectedPermissions, setSelectedPermissions] = useState([]);
	const [savedPermissions, setSavedPermissions] = useState([]);
	const {
		data: permissions,
		isLoading: isLoadingPermissions,
		isFetching: isFetchingPermissions,
	} = useFetchDataRQ({
		queryKey: [Terms.Permissions, backendPermissoinSearchTerm],
		queryFn: () => usersManagementApi.getOrganizationPermissions(backendPermissoinSearchTerm),
		keepPreviousData: true,
		onSuccessFn: (data) => {
			console.log("Success", data);
		},
	});

	const handleSelectPermission = (permission) => {
		const alreadyChecked = selectedPermissions.some((p1) => p1.id == permission.id);
		if (alreadyChecked) {
			const newPermessionsArray = selectedPermissions.filter((p1) => p1.id != permission.id);
			setSelectedPermissions(newPermessionsArray);
		} else {
			const newPermessionsArray = [...selectedPermissions, permission];
			setSelectedPermissions(newPermessionsArray);
		}
	};
	const handleSelectAllPermissions = (allPermissions) => {
		const allIsSelected = allPermissions.length == selectedPermissions.length;
		if (allIsSelected) {
			setSelectedPermissions([]);
		} else {
			const newPermessionsArray = [...allPermissions];
			setSelectedPermissions(newPermessionsArray);
		}
	};

	const assignPermissionsToUserTypeMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.assignPermissionsToUserType(payload),
		// invalidateKeys: [Terms.UserTypes, backendSearchTerm],
	});
	const removePermissionsFromUserTypeMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.removePermissionsFromUserType(payload),
	});
	const getRemovedPermissions = () => {
		const idsInSelectedPermissions = new Set(selectedPermissions.map((p) => p.id));
		const removedPermissions = savedPermissions.filter((p) => !idsInSelectedPermissions.has(p.id));
		const removedPermissionsNames = removedPermissions.map((p) => p.name);
		return removedPermissionsNames;
	};
	//--------------------------------Members------------------------------------------------

	const [frontMembersSearchTerm, setFrontMembersSearchTerm] = useState("");
	const [selectedMembers, setSelectedMembers] = useState([]);
	const [filters, setFilters] = useState([{ id: 1, active: false, pickedOption: null, isApplied: false }]);
	const [allMembersFilters, setAllMembersFilters] = useState({});
	const [savedMembers, setSavedMembers] = useState([]);

	const initialValuesMembers = {
		email: "",
		address: "",
		phoneNumber: "",
	};
	const phoneNumberRegex = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/;

	const validationSchemaMembers = Yup.object({
		email: Yup.string(),
		address: Yup.string(),
		phoneNumber: Yup.string(),
	});
	const {
		values: membersValues,
		touched: membersTouched,
		errors: membersErrors,
		handleChange: handleChangeMembers,
		handleBlur: handleBlurMembers,
	} = useFormik({
		initialValues: initialValuesMembers,
		validationSchema: validationSchemaMembers,
	});

	//-----------------------------------get Users with filters applyied-----------------------
	const {
		data: members,
		isLoading: isLoadingMembers,
		isFetching: isFetchingMembers,
	} = useFetchDataRQ({
		queryKey: [Terms.Members, allMembersFilters],
		queryFn: () => {
			const filters = convertFiltersShape(allMembersFilters);
			return usersManagementApi.getAllOrganizationUsers(filters);
		},
		keepPreviousData: true,
	});
	//-------------------------------------get users types for filter-------------------------
	const {
		data: userTypes,
		isLoading: isLoadingUserTypes,
		isFetching: isFetchingUserTypes,
	} = useFetchDataRQ({
		queryKey: [Terms.UserTypes],
		queryFn: () => usersManagementApi.getUsersTypes(false),
	});

	const assignUserTypeToUsersMutation = useMutateData({
		queryFn: ({ id, payload }) => usersManagementApi.assignUserTypeToUser(id, payload),
	});

	const handleSelectMember = (member) => {
		const alreadyChecked = selectedMembers.some((m1) => m1.id == member.id);
		if (alreadyChecked) {
			const newMembersArray = selectedMembers.filter((m1) => m1.id != member.id);
			setSelectedMembers(newMembersArray);
		} else {
			const newMembersArray = [...selectedMembers, member];
			setSelectedMembers(newMembersArray);
		}
	};
	const handleSelectAllMembers = (allMembers) => {
		const allIsSelected = allMembers.length == selectedMembers.length;
		if (allIsSelected) {
			setSelectedMembers([]);
		} else {
			const newMembersArray = [...allMembers];
			setSelectedMembers(newMembersArray);
		}
	};
	const getRemovedMembers = () => {
		const idsInSelectedUsers = new Set(selectedMembers.map((u) => u.id));
		const removedMembers = savedMembers.filter((u) => !idsInSelectedUsers.has(u.id));
		const removedMembersIds = removedMembers.map((u) => u.id);
		return removedMembersIds;
	};
	const removeMembersMutation = useMutateData({
		queryFn: ({ id, payload }) => usersManagementApi.removeUsersFromUserType(id, payload),
	});
	console.log("selectedUsers", selectedMembers);
	//----------------------------------Global--------------------------------------------
	const getNextTab = () => {
		switch (activeTab) {
			case Terms.Settings:
				return tr(`assign_${Terms.Roles}`);
			case Terms.Roles:
				return tr(`assign_${Terms.Permissions}`);
			case Terms.Permissions:
				return tr(`add_${Terms.Members}`);
			case Terms.Members:
				return tr(Terms.Review);
			case Terms.Review:
				return tr("previous");
		}
	};
	const getSaveAction = () => {
		let payload = {};
		switch (activeTab) {
			case Terms.Settings:
				if (errors.userTypeName) {
					setFieldTouched("userTypeName", true);
					return;
				}
				payload = {
					title: settingsValues.userTypeName,
					name: settingsValues.userTypeName,
					description: settingsValues.description,
					id: currentUserTypeId ? currentUserTypeId : undefined,
				};
				currentUserTypeId ? updateUserTypeMuation.mutate({ id: currentUserTypeId, payload }) : addUserTypeMutation.mutate({ payload });
				break;
			case Terms.Roles:
				const removedRolesNames = getRemovedRoles();
				if (removedRolesNames.length > 0) {
					payload = {
						role_names: removedRolesNames,
						organization_user_type_ids: [currentOrganizationUserTypeId],
					};
					removeRolesFromUserTypeMutation.mutate({ payload });
				}
				if (selectedRoles.length > 0) {
					const roleNames = selectedRoles.map((role) => role.name);
					payload = {
						role_names: roleNames,
						organization_user_type_ids: [currentOrganizationUserTypeId],
					};
					assignRolesToUserTypeMutation.mutate({ payload });
				}
				setSavedRoles(selectedRoles);
				setActiveTab(Terms.Permissions);
				break;
			case Terms.Permissions:
				const removedPermissionsNames = getRemovedPermissions();
				if (removedPermissionsNames.length > 0) {
					payload = {
						permission_names: removedPermissionsNames,
						organization_user_type_ids: [currentOrganizationUserTypeId],
					};
					removePermissionsFromUserTypeMutation.mutate({ payload });
				}
				if (selectedPermissions.length > 0) {
					const permissionsNames = selectedPermissions.map((permission) => permission.name);
					payload = {
						permission_names: permissionsNames,
						organization_user_type_ids: [currentOrganizationUserTypeId],
					};
					assignPermissionsToUserTypeMutation.mutate({ payload });
				}
				setSavedPermissions(selectedPermissions);
				setActiveTab(Terms.Members);
				break;
			case Terms.Members:
				const removedMembersIds = getRemovedMembers();
				if (removedMembersIds.length > 0) {
					const payload = {
						user_ids: removedMembersIds,
					};
					removeMembersMutation.mutate({ id: currentOrganizationUserTypeId, payload });
				}
				if (selectedMembers.length > 0) {
					const usersIds = selectedMembers.map((user) => user.id);
					payload = {
						user_ids: usersIds,
					};
					assignUserTypeToUsersMutation.mutate({ id: currentOrganizationUserTypeId, payload });
					setSavedMembers(selectedMembers);
				}
				setActiveTab(Terms.Review);
				break;
			case Terms.Review:
				history.push(`${baseURL}/${genericPath}/users-management/user-types`);
				break;
		}
	};
	const getNextAction = () => {
		switch (activeTab) {
			case Terms.Settings:
				setActiveTab(Terms.Roles);
				break;
			case Terms.Roles:
				setActiveTab(Terms.Permissions);
				break;
			case Terms.Permissions:
				setActiveTab(Terms.Members);
				break;

			case Terms.Members:
				setActiveTab(Terms.Review);
				break;
			case Terms.Review:
				setActiveTab(Terms.Members);
				break;
		}
	};
	useEffect(() => {
		if (!settingsValues.userTypeName) {
			setFieldError("userTypeName", "user type Name is Required");
		}
	}, []);
	const dummyRole = {
		id: 1,
		title: "RoleTitle",
		description: "This is the desctiption for the first role",
		permissions: [
			{ id: 1, title: "FirstPermissions" },
			{ id: 2, title: "SecondPermissoinss" },
			{ id: 3, title: "ThirdPermissoins" },
			{ id: 4, title: "FourthPermissions" },
		],
	};
	return (
		<RFlex className="flex-column" styleProps={{ position: "relative" }}>
			<RFlex className="align-items-center">
				<div
					style={{ cursor: "pointer" }}
					className={styles.backArrow}
					onClick={() => history.replace(`${baseURL}/${genericPath}/users-management/user-types`)}
				>
					<i className={iconsFa6.chevronLeft} style={{ color: colors.primaryColor }} />
				</div>
				{tr("Create_new_user_type")}
			</RFlex>
			<RNewTabs tabs={createUserTypeTabs} setActiveTab={setActiveTab} value={activeTab} />
			{activeTab == Terms.Settings && (
				<RUserTypeSettings
					values={settingsValues}
					errors={errors}
					touched={touched}
					handleBlur={handleBlur}
					handleChange={handleChange}
					handleSelectLevel={handleSelectLevel}
					savedSettingsValues={settingsValues}
					handleAddingAnotherLevel={handleAddingAnotherLevel}
					levels={levels}
					handleSetDefault={handleSetDefault}
				/>
			)}
			{activeTab == Terms.Roles &&
				(isLoadingRoles ? (
					<RLoader />
				) : (
					<RRoles
						roles={roles?.data?.data}
						handleSelectRole={handleSelectRole}
						// handleSelectAllRoles={handleSelectAllRoles}
						frontSearchTerm={frontRoleSearchTerm}
						setFrontSearchTerm={setFrontRoleSearchTerm}
						setBackSearchTerm={setBackRoleSearchTerm}
						selectedRoles={selectedRoles}
					/>
				))}
			{activeTab == Terms.Permissions &&
				(isLoadingPermissions ? (
					<RLoader />
				) : (
					<RPermissons
						permissions={permissions?.data?.data}
						setBackSearchTerm={setBackendPermissoinSearchTerm}
						selectOneFn={handleSelectPermission}
						selectAllFn={handleSelectAllPermissions}
						selectedPermissions={selectedPermissions}
						frontSearchTerm={frontPermissionSearchTerm}
						setFrontSearchTerm={setFrontPermissionSearchTerm}
						isFetching={isFetchingPermissions}
					/>
				))}
			{activeTab == Terms.Members &&
				(isLoadingMembers || isLoadingUserTypes ? (
					<RLoader />
				) : (
					<RFlex className="flex-column">
						<RHeader
							actions={dropdownActions}
							showCreate={false}
							showSearchHeader={false}
							showBulk={true}
							showColumnSettings={true}
							showRefresh={false}
						/>
						<GFirstMembersHeader
							allUsersLength={members?.data?.data?.records?.length}
							allMembersFilters={allMembersFilters}
							setAllMembersFilters={setAllMembersFilters}
							filters={filters}
							setFilters={setFilters}
							frontSearchTerm={frontMembersSearchTerm}
							setFrontSearchTerm={setFrontMembersSearchTerm}
							isFetching={isFetchingMembers}
							selectedUsers={selectedMembers}
							values={membersValues}
							touched={membersTouched}
							errors={membersErrors}
							handleChange={handleChangeMembers}
							handleBlur={handleBlurMembers}
						/>
						<RUsersData
							users={members?.data?.data}
							userTypes={userTypes?.data?.data}
							allMembersFilters={allMembersFilters}
							setAllMembersFilters={setAllMembersFilters}
							selectedUsers={selectedMembers}
							selectOneFn={handleSelectMember}
							selectAllFn={handleSelectAllMembers}
							inCreate={true}
						/>
						<RCSVDrawer
							isOpen={isDrawerOpen.isOpen}
							operationType={isDrawerOpen.operationType}
							toggleDrawer={toggleDrawer}
							downloadTemplateFn={() => importUsersTempalteMutation.mutate()}
							currentType={Terms.Users}
						/>
					</RFlex>
				))}
			{activeTab == Terms.Review && (
				<RFlex className="flex-column">
					<RUserTypeSettings
						values={settingsValues}
						errors={errors}
						touched={touched}
						handleBlur={handleBlur}
						handleChange={handleChange}
						review={true}
						showSettingsWord={true}
						savedSettingsValues={savedSettingsValues}
					/>
					<RRoles selectedRoles={[]} roles={savedRoles} review />
					<RReviewPermissions savedPermissions={savedPermissions} />
					<RReviewUserData selectedUsers={savedMembers} users={members?.data?.data} />
				</RFlex>
			)}
			<div className={styles.roolFooter}>
				<RFlex className="align-items-center" styleProps={{ gap: "0px" }}>
					<Button
						disabled={
							(activeTab != Terms.Settings && !currentUserTypeName) || addUserTypeMutation.isLoading || updateUserTypeMuation.isLoading
						}
						loading={addUserTypeMutation.isLoading || updateUserTypeMuation.isLoading}
						className="flex gap-1"
						onClick={getSaveAction}
					>
						{activeTab == Terms.Review ? tr("Save_and_Finish") : tr("Save_and_Next")}
						{(addUserTypeMutation.isLoading || updateUserTypeMuation.isLoading) && <i className={iconsFa6.spinner} />}
					</Button>
					<Button variant="ghost" className="flex gap-1" onClick={getNextAction}>
						{activeTab == Terms.Review && <i className={iconsFa6.chevronLeft} />}
						{getNextTab()}
						{activeTab != Terms.Review && <i className={iconsFa6.chevronRight} />}
					</Button>
				</RFlex>
			</div>
		</RFlex>
	);
};

export default GCreateUserType;
