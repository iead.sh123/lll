import AppNewCheckbox from "components/Global/RComs/AppNewCheckbox/AppNewCheckbox";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import React, { useState } from "react";
import { permissionsHeader } from "../constants";
import styles from "./sharedStyle.module.scss";
import RLister from "components/Global/RComs/RLister";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import RButton from "components/Global/RComs/RButton";
import { Checkbox } from "ShadCnComponents/ui/checkbox";
import RSearchInput from "components/RComponents/RSearchInput";
const RPermissons = ({
	permissions,
	selectAllFn,
	selectOneFn,
	frontSearchTerm,
	setFrontSearchTerm,
	setBackSearchTerm,
	selectedPermissions,
	isFetching,
	canEdit = true,
	view = false,
	inUser = false,
	currentUserTypes,
	pickedUserType,
	handleChangePickedUserType,
}) => {
	const handleSearch = (clearData) => {
		let data = frontSearchTerm;
		if (clearData == "") {
			data = clearData;
		}
		setBackSearchTerm(data);
	};

	const columns = [
		{
			accessorKey: "name",
			renderHeader: () => (
				<RFlex className="items-center" styleProps={{ gap: "8px" }}>
					<Checkbox
						label={tr("permissions")}
						checked={selectedPermissions?.length == permissions?.length}
						onCheckedChange={() => {
							canEdit ? selectAllFn(permissions) : "";
						}}
					/>
					<span>{tr("Permissions")}</span>
				</RFlex>
			),
			renderCell: ({ row }) => (
				<RFlex className="items-center" styleProps={{ gap: "8px" }}>
					<Checkbox
						checked={selectedPermissions?.some((p1) => p1.id == row?.original?.id)}
						onCheckedChange={() => {
							canEdit ? selectOneFn(row?.original) : "";
						}}
					/>
					<span>{row?.original?.name}</span>
				</RFlex>
			),
		},
		{
			accessorKey: "description",
			renderHeader: () => <span>{tr("Description")}</span>,
			renderCell: ({ row }) => <span>{row?.original?.description == "" ? "-" : row?.original?.description}</span>,
		},
		{
			accessorKey: "privilege",
			renderHeader: () => <span>{tr("privilege")}</span>,
			renderCell: ({ row }) => (row?.original?.privileged ? <div className={styles.priviligeStyle}>{tr("Privilege")}</div> : "-"),
			maxSize: 500,
		},
	];
	const records = { data: permissions, columns };
	return (
		<RFlex className="flex-column " styleProps={{ marginBottom: view ? "0px" : "0px" }}>
			<RSearchInput
				searchLoading={isFetching}
				inputPlaceholder={tr`search_for_a_permission`}
				searchData={frontSearchTerm}
				handleChangeSearch={setFrontSearchTerm}
				setSearchData={setFrontSearchTerm}
				handleSearch={handleSearch}
			/>
			{inUser && (
				<RFlex className="gap-4 ">
					{currentUserTypes?.map((userType, index) => (
						<RButton
							text={userType.title}
							color="primary"
							outline={userType.id == pickedUserType.id ? false : true}
							className="m-0 rounded-[50px]"
							onClick={() => handleChangePickedUserType(userType)}
						/>
					))}
				</RFlex>
			)}
			<RLister Records={records} convertRecordsShape={false} containerClassName="max-h-[55vh]" />
		</RFlex>
	);
};

export default RPermissons;
