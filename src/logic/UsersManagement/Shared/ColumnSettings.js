import { Button } from "ShadCnComponents/ui/button";
import { usersManagementApi } from "api/UsersManagement";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { useMutateData } from "hocs/useMutateData";
import React from "react";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import iconsFa6 from "variables/iconsFa6";

const ColumnSettings = ({ columns, setColumnsState, keyToInvalidate, setSavedColumnsState, tableData }) => {
	const hideColumn = (columnId, hideFlag) => {
		setColumnsState((oldColumns) => {
			const arrayCopy = JSON.parse(JSON.stringify(oldColumns));
			const selectedColumnIndex = arrayCopy.findIndex((column) => column.id == columnId);
			arrayCopy[selectedColumnIndex].hidden = hideFlag;
			return arrayCopy;
		});
	};
	const pinColumn = (columnId, pinFlag) => {
		setColumnsState((oldColumns) => {
			const arrayCopy = JSON.parse(JSON.stringify(oldColumns));
			const selectedColumnIndex = arrayCopy.findIndex((column) => column.id == columnId);
			arrayCopy[selectedColumnIndex].pinned = pinFlag;
			return arrayCopy;
		});
	};
	const saveUserSettingsMutaion = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.addUserColumnSettings(payload),
		invalidateKeys: keyToInvalidate,
	});
	const handleSaveColumnSettings = () => {
		// const payload = Object.keys(columns).map((key) => {
		// 	return { ...columns[key] };
		// });
		columns.forEach((column) => {
			column.pinned && tableData.getColumn(column.label).pin("left");
		});
		setSavedColumnsState(columns);
		const transformedColumns = columns.map((column) => ({
			[column.label]: {
				hidden: column.hidden,
				pinned: column.pinned,
			},
		}));
		const payload = {
			collectionName: "users",
			settings: transformedColumns,
		};
		saveUserSettingsMutaion.mutate({ payload });
	};
	const handleDragEnd = (result) => {
		const sourceIndex = result?.source?.index;
		const destinationIndex = result?.destination?.index;
		if (sourceIndex == destinationIndex) return;
		const newColumns = JSON.parse(JSON.stringify(columns));
		const sourceColumn = newColumns.splice(sourceIndex, 1)[0];
		newColumns.splice(destinationIndex, 0, sourceColumn);
		setColumnsState(newColumns);
	};
	return (
		<RFlex className="flex-col h-[435px] justify-between">
			<DragDropContext onDragEnd={handleDragEnd}>
				<Droppable droppableId={"column_settings"}>
					{(provided, snapshot) => (
						<div className="flex flex-col gap-1.5 w-full" ref={provided.innerRef}>
							{columns.map((column, index) => {
								return (
									<Draggable draggableId={`${column.label} ${index}`} index={index}>
										{(provided, snapshot) => {
											return (
												<div className="justify-between items-center flex w-full" ref={provided.innerRef} {...provided.draggableProps}>
													<RFlex className="items-center" styleProps={{ gap: "5px" }}>
														<i className={iconsFa6.twoDash} {...provided.dragHandleProps} />
														<span>{tr(column.label)}</span>
													</RFlex>
													<RFlex>
														{column.hidden ? (
															<i
																onClick={() => hideColumn(column.id, false)}
																className={`${iconsFa6.eyeSlash} text-themeLight cursor-pointer`}
															/>
														) : (
															<i onClick={() => hideColumn(column.id, true)} className={`${iconsFa6.eye} text-themeLight cursor-pointer`} />
														)}
														{column.pinned ? (
															<i
																onClick={() => pinColumn(column.id, false)}
																className={`${iconsFa6.pin} text-themePrimary cursor-pointer`}
															/>
														) : (
															<i
																onClick={() => pinColumn(column.id, true)}
																className={`${iconsFa6.pin} text-themePrimary fa-rotate-by cursor-pointer rotate-45`}
															/>
														)}
													</RFlex>
												</div>
											);
										}}
									</Draggable>
								);
							})}
						</div>
					)}
				</Droppable>
			</DragDropContext>
			<RFlex>
				<Button className="flex gap-1" onClick={handleSaveColumnSettings}>
					{tr("save")}
					{saveUserSettingsMutaion.isLoading && <i className={iconsFa6.spinner} />}
				</Button>
				<Button variant="ghost" className="text-themePrimary hover:text-themePrimary" onClick={() => {}}>
					{tr("cancel")}
				</Button>
			</RFlex>
		</RFlex>
	);
};

export default ColumnSettings;
