export const convertFiltersShape = (allMembersFilters) => {
	const filtersAsParams = Object.keys(allMembersFilters).reduce((finalFilters, filterName) => {
		if (allMembersFilters[filterName] === null || allMembersFilters[filterName] === "") {
			return finalFilters;
		}
		if (filterName == "typeNames") {
			const typeNames = allMembersFilters[filterName]
				.map((type, index) => {
					return `typeNames[${index}]=${type}`;
				})
				.join("&");
			finalFilters = finalFilters.concat(`${typeNames}&`);
			return finalFilters;
		}
		if (filterName == "roles") {
			const rolesNames = allMembersFilters[filterName]
				.map((type, index) => {
					return `roles[${index}]=${type}`;
				})
				.join("&");
			finalFilters = finalFilters.concat(`${rolesNames}&`);
			return finalFilters;
		}
		if (filterName == "groups") {
			const groupNames = allMembersFilters[filterName]
				.map((type, index) => {
					return `groups[${index}]=${type}`;
				})
				.join("&");
			finalFilters = finalFilters.concat(`${groupNames}&`);
			return finalFilters;
		}
		if (filterName == "exceptRoles") {
			const exceptRoles = allMembersFilters[filterName].map((type, index) => {
				return `exceptRoles[${index}]=${type}`;
			});
			finalFilters = finalFilters.concat(`${exceptRoles}&`);
			return finalFilters;
		}
		if (filterName == "exceptTypes") {
			const exceptTypes = allMembersFilters[filterName].map((type, index) => {
				return `exceptTypes[${index}]=${type}`;
			});
			finalFilters = finalFilters.concat(`${exceptTypes}&`);
			return finalFilters;
		}
		if (filterName == "exceptGroups") {
			const exceptGroups = allMembersFilters[filterName].map((type, index) => {
				return `exceptGroups[${index}]=${type}`;
			});
			finalFilters = finalFilters.concat(`${exceptGroups}&`);
			return finalFilters;
		} else {
			const queryParamFilter = `${filterName}=${allMembersFilters[filterName]}`;
			if (allMembersFilters[filterName] !== "") {
				finalFilters = finalFilters.concat(`${queryParamFilter}&`);
			}
			return finalFilters;
		}
	}, "");
	return filtersAsParams.slice(0, -1);
};
