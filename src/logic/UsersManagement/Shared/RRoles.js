import RFlex from "components/Global/RComs/RFlex/RFlex";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import tr from "components/Global/RComs/RTranslator";
import React, { useState } from "react";
import RSingleRole from "./RSingleRole";

import RButton from "components/Global/RComs/RButton";
import { Accordion } from "ShadCnComponents/ui/accordion";
import RSearchInput from "components/RComponents/RSearchInput";

const RRoles = ({
	roles,
	selectedRoles,
	isFetching,
	frontSearchTerm,
	setFrontSearchTerm,
	setBackSearchTerm,
	handleSelectRole,
	review = false,
	canEdit = true,
	view,
	inUser = false,
	currentUserTypes,
	pickedUserType,
	handleChangePickedUserType,
}) => {
	const handleSearch = (clearData) => {
		let data = frontSearchTerm;
		if (clearData == "") {
			data = clearData;
		}
		setBackSearchTerm(data);
	};
	const [openCollapseId, setOpenCollapseId] = useState(null);
	const handleCollapseClicked = (collapseId) => {
		openCollapseId == collapseId ? setOpenCollapseId(null) : setOpenCollapseId(collapseId);
	};
	return (
		<RFlex className="flex-column" styleProps={{ gap: review ? "0px" : "10px", marginBottom: review ? "0px" : view ? "0px" : "75px" }}>
			{!review && (
				<RSearchInput
					searchLoading={isFetching}
					inputPlaceholder={tr`search_for_a_role_or_a_permission`}
					searchData={frontSearchTerm}
					handleChangeSearch={setFrontSearchTerm}
					setSearchData={setFrontSearchTerm}
					handleSearch={handleSearch}
				/>
			)}
			{inUser && (
				<RFlex className="gap-4 ">
					{currentUserTypes?.map((userType, index) => (
						<RButton
							text={userType.title}
							color="primary"
							outline={userType.id == pickedUserType.id ? false : true}
							className="m-0 rounded-[50px]"
							onClick={() => handleChangePickedUserType(userType)}
						/>
					))}
				</RFlex>
			)}
			{review && <span className="p-0 m-0 font-weight-bold">{tr("Roles")}</span>}
			{roles?.length > 0 ? (
				<Accordion type="single" collapsible className="w-[55%]">
					{roles?.map((role, index) => (
						<RSingleRole
							role={role}
							isSelected={selectedRoles?.some((r) => role.id == r.id)}
							isOpen={role.id == openCollapseId}
							handleCollapseClicked={handleCollapseClicked}
							handleSelectRole={handleSelectRole}
							canEdit={canEdit}
							review={review}
							index={index + 1}
						/>
					))}
				</Accordion>
			) : (
				<span className="p-0 m-0 text-themeBoldGrey">{tr("No_Roles_Yet")}</span>
			)}
		</RFlex>
	);
};

export default RRoles;
