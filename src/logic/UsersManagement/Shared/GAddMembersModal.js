import RLoader from "components/Global/RComs/RLoader";
import { useFormik } from "formik";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import React, { useState } from "react";
import * as Yup from "yup";
import RHeader from "./RHeader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import GFirstMembersHeader from "./GFirstMembersHeader";
import RUsersData from "./RUsersData";
import RCSVDrawer from "./RCSVDrawer";
import { Terms } from "../constants";
import { usersManagementApi } from "api/UsersManagement";
import { useMutateData } from "hocs/useMutateData";
import tr from "components/Global/RComs/RTranslator";
import { useCUDToQueryKey } from "hocs/useCUDToQueryKey";
import { convertFiltersShape } from "./ConvertFiltersShape";
const GAddMembersModal = ({
	currentRoleName,
	keyToInvalidate,
	currentUserTypeName,
	currentUserTypeId,
	currentUserGroupId,
	currentUserGroupName,
}) => {
	const { CUDToQueryKey, operations } = useCUDToQueryKey();
	const initialValuesMembers = {
		email: "",
		address: "",
		phoneNumber: "",
	};
	// const phoneNumberRegex = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/;

	const validationSchemaMembers = Yup.object({
		email: Yup.string(),
		address: Yup.string(),
		phoneNumber: Yup.string(),
	});
	const {
		values: membersValues,
		touched: membersTouched,
		errors: membersErrors,
		handleChange: handleChangeMembers,
		handleBlur: handleBlurMembers,
	} = useFormik({
		initialValues: initialValuesMembers,
		validationSchema: validationSchemaMembers,
	});
	const [loadingIds, setLoadingIds] = useState([]);
	const [frontMembersSearchTerm, setFrontMembersSearchTerm] = useState("");
	const [selectedMembers, setSelectedMembers] = useState([]);
	const [filters, setFilters] = useState([{ id: 1, active: false, pickedOption: null, isApplied: false }]);
	const [allMembersFilters, setAllMembersFilters] = useState({
		exceptRoles: currentRoleName ? [currentRoleName] : null,
		exceptTypes: currentUserTypeName ? [currentUserTypeName] : null,
		exceptGroups: currentUserGroupName ? [currentUserGroupId] : null,
	});

	const {
		data: members,
		isLoading: isLoadingMembers,
		isFetching: isFetchingMembers,
	} = useFetchDataRQ({
		queryKey: [Terms.Members, "Modal", allMembersFilters],
		queryFn: () => {
			const filters = convertFiltersShape(allMembersFilters);
			return usersManagementApi.getAllOrganizationUsers(filters);
		},
		keepPreviousData: true,
	});
	const {
		data: userTypes,
		isLoading: isLoadingUserTypes,
		isFetching: isFetchingUserTypes,
	} = useFetchDataRQ({
		queryKey: [Terms.UserTypes],
		queryFn: () => usersManagementApi.getUsersTypes(false),
	});
	const assignRoleToUsers = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.assignRoleToUsers(payload),
		invalidateKeys: [[Terms.Members, "Modal", allMembersFilters], keyToInvalidate],
		multipleKeys: true,
		onSuccessFn: ({ variables }) => {
			setLoadingIds(loadingIds.filter((id) => id != variables?.payload?.user_ids?.[0]));
			//------------------ remove user from the modal table---------------------
			CUDToQueryKey({
				operation: operations.DELETE,
				queryKey: [Terms.Members, "Modal", allMembersFilters],
				insertionDepth: "data.data.records",
				id: variables?.payload?.user_ids?.[0],
			});
			CUDToQueryKey({
				operation: operations.ADD,
				queryKey: keyToInvalidate,
				insertionDepth: "data.data.records",
				newData: variables?.user,
			});
		},
		onErrorFn: (error, variables) => {
			setLoadingIds(loadingIds.filter((id) => id != variables?.payload?.user_ids?.[0]));
		},
	});
	const handleAddingUserToRole = (user) => {
		const payload = {
			role_names: [currentRoleName],
			user_ids: [user.id],
		};
		assignRoleToUsers.mutate({ payload, user });
		setLoadingIds([...loadingIds, user.id]);
	};
	const assignUserTypeToUsersMutation = useMutateData({
		queryFn: ({ id, payload }) => usersManagementApi.assignUserTypeToUser(id, payload),
		invalidateKeys: [[Terms.Members, "Modal", allMembersFilters], keyToInvalidate],
		multipleKeys: true,
		onSuccessFn: ({ variables }) => {
			setLoadingIds(loadingIds.filter((id) => id != variables?.payload?.user_ids?.[0]));
			//------------------ remove user from the modal table---------------------
			CUDToQueryKey({
				operation: operations.DELETE,
				queryKey: [Terms.Members, "Modal", allMembersFilters],
				insertionDepth: "data.data.records",
				id: variables?.payload?.user_ids?.[0],
			});
			CUDToQueryKey({
				operation: operations.ADD,
				queryKey: keyToInvalidate,
				insertionDepth: "data.data.records",
				newData: variables?.user,
			});
		},
		onErrorFn: (error, variables) => {
			setLoadingIds(loadingIds.filter((id) => id != variables?.payload?.user_ids?.[0]));
		},
	});
	const handleAddingUserToUserType = (user) => {
		const payload = {
			user_ids: [user.id],
		};
		assignUserTypeToUsersMutation.mutate({ id: currentUserTypeId, payload, user });
		setLoadingIds([...loadingIds, user.id]);
	};
	const handleSelectMember = (member) => {
		const alreadyChecked = selectedMembers.some((m1) => m1.id == member.id);
		if (alreadyChecked) {
			const newMembersArray = selectedMembers.filter((m1) => m1.id != member.id);
			setSelectedMembers(newMembersArray);
		} else {
			const newMembersArray = [...selectedMembers, member];
			setSelectedMembers(newMembersArray);
		}
	};
	const handleSelectAllMembers = (allMembers) => {
		const allIsSelected = allMembers.length == selectedMembers.length;
		if (allIsSelected) {
			setSelectedMembers([]);
		} else {
			const newMembersArray = [...allMembers];
			setSelectedMembers(newMembersArray);
		}
	};
	return isLoadingMembers || isLoadingUserTypes ? (
		<RLoader />
	) : (
		<RFlex className="flex-column">
			<RHeader
				showCreate={false}
				showSearchHeader={false}
				showBulk={false}
				showColumnSettings={true}
				showRefresh={false}
				showImportCSV={true}
				showCreateAccount={true}
				showDetails={false}
				importCSVFn={() => {}}
				createAccountFn={() => {}}
				columnSettingsFn={() => {}}
			/>
			<GFirstMembersHeader
				allUsersLength={members?.data?.data?.records?.length}
				allMembersFilters={allMembersFilters}
				setAllMembersFilters={setAllMembersFilters}
				filters={filters}
				setFilters={setFilters}
				frontSearchTerm={frontMembersSearchTerm}
				setFrontSearchTerm={setFrontMembersSearchTerm}
				isFetching={isFetchingMembers}
				selectedUsers={selectedMembers}
				values={membersValues}
				touched={membersTouched}
				errors={membersErrors}
				handleChange={handleChangeMembers}
				handleBlur={handleBlurMembers}
				showAddUsers={false}
			/>
			<RUsersData
				filterHeader={true}
				users={members?.data?.data}
				userTypes={userTypes?.data?.data}
				allMembersFilters={allMembersFilters}
				setAllMembersFilters={setAllMembersFilters}
				selectedUsers={selectedMembers}
				selectOneFn={handleSelectMember}
				selectAllFn={handleSelectAllMembers}
				addUser={currentUserTypeName ? handleAddingUserToUserType : handleAddingUserToRole}
				modal={true}
				checkboxExists={false}
				loadingIds={loadingIds}
				inModal={true}
			/>
		</RFlex>
	);
};

export default GAddMembersModal;
