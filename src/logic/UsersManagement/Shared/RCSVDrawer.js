import React, { useState } from "react";
import Drawer from "react-modern-drawer";
import styles from "./sharedStyle.module.scss";
import RDrawerContent from "./RDrawerContent";
import RDrawer from "components/RComponents/RDrawer/RDrawer";
import RControlledDrawer from "components/RComponents/RControlledDrawer/RControlledDrawer";
const RCSVDrawer = ({ toggleDrawer, isOpen, operationType, downloadTemplateFn, currentType }) => {
	return (
		<div>
			<RControlledDrawer
				isOpen={isOpen}
				closeDrawer={toggleDrawer}
				drawerBody={<RDrawerContent operationType={operationType} downloadTemplateFn={downloadTemplateFn} currentType={currentType} />}
			/>
		</div>
	);
};

export default RCSVDrawer;
