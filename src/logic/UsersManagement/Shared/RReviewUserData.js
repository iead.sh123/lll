import RProfileName from "components/Global/RComs/RProfileName/RProfileName";
import React from "react";
import * as colors from "config/constants";
import tr from "components/Global/RComs/RTranslator";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RLister from "components/Global/RComs/RLister";
import styles from "./sharedStyle.module.scss";
import RTooltip from "components/RComponents/RTooltip/RTooltip";
const RReviewUserData = ({ users, selectedUsers = [] }) => {
	const columns = [
		{
			inModal: true,
			accessorKey: "name",
			renderHeader: () => (
				<RFlex styleProps={{ gap: "8px" }} className="items-center min-w-max">
					<RFlex styleProps={{ gap: "5px" }}>
						{tr("User_name")}
						<span className={`${styles.lengthDiv} text-themePrimary min-w-max`} style={{ fontSize: "8px" }}>
							{selectedUsers?.length}
						</span>
					</RFlex>
				</RFlex>
			),
			renderCell: ({ row }) => (
				<RFlex styleProps={{ gap: "8px" }} className="items-center">
					<RProfileName name={row.original.full_name} img={row.original.image} flexStyleProps={{ cursor: "default" }} />
				</RFlex>
			),
		},
		{
			inModal: true,
			accessorKey: "user_types",
			renderHeader: () => (
				<RFlex className="items-baseline">
					<span className="min-w-max">{tr("user_types")}</span>
				</RFlex>
			),
			renderCell: ({ row }) => (
				<p className="p-0 m-0">
					{row.original?.user_types?.length <= 0
						? "-"
						: row.original?.user_types?.map((userType, index) => {
								return (
									<>
										<RTooltip
											triggerClassName={`${userType.is_active ? "text-black" : "text-themeLight"}`}
											tooltipText={userType.is_active ? "active" : "inactive"}
											trigger={<span className="cursor-text">{userType.type}</span>}
										/>
										{index != row.original?.user_types?.length - 1 ? "," : ""}
									</>
								);
						  })}
				</p>
			),
		},
		{
			inModal: true,
			accessorKey: "identifier",
			renderHeader: () => <span className="min-w-max">{tr("ID")}</span>,
			renderCell: ({ row }) => <span>{row.getValue("identifier") ?? "-"}</span>,
		},
		{
			accessorKey: "gender",
			renderHeader: () => (
				<RFlex className="items-baseline">
					<span>{tr("Gender")}</span>
				</RFlex>
			),
			renderCell: ({ row }) => <span className="text-themeBoldGrey">{row.getValue("gender") == "m" ? tr("Male") : tr("Female")}</span>,
		},
		{
			inModal: true,
			accessorKey: "email",
			renderHeader: () => <span className="min-w-max">{tr("Email")}</span>,
			renderCell: ({ row }) => <span className="text-themeBoldGrey">{row.getValue("email")}</span>,
		},
		{
			accessorKey: "address",
			renderHeader: () => <span className="min-w-max">{tr("Address")}</span>,
			renderCell: ({ row }) => <span className="text-themeBoldGrey text-left">{row.getValue("address") ?? "-"}</span>,
		},
		{
			accessorKey: "is_active",
			renderHeader: () => (
				<RFlex className="items-baseline">
					<span>{tr("Status")}</span>
				</RFlex>
			),
			renderCell: ({ row }) => (
				<span
					className="p-0 m-0 flex gap-2 items-center"
					style={{
						color: row.original.is_active ? colors.successColor : colors.dangerColor,
						cursor: "default",
					}}
				>
					{row.original.is_active ? tr("active") : tr("inactive")}
				</span>
			),
		},
		{
			accessorKey: "phone_number",
			renderHeader: () => <span className="min-w-max">{tr("phone_number")}</span>,
			renderCell: ({ row }) => <span>{row.getValue("phone_number") ?? "-"}</span>,
		},
		{
			accessorKey: "emergency_number",
			renderHeader: () => <span className="min-w-max">{tr("emergency_number")}</span>,
			renderCell: ({ row }) => <span className="text-themeBoldGrey ">{row.getValue("emergency_number") ?? "-"}</span>,
		},
	];
	const records = { columns, data: selectedUsers };
	return (
		<RFlex className="flex-col" styleProps={{ marginBottom: "100px" }}>
			<span style={{ fontWeight: "bold" }}>{tr("Members")}</span>
			{selectedUsers.length > 0 ? (
				<RLister Records={records} convertRecordsShape={false} />
			) : (
				<RFlex className="flex-column text-themeBoldGrey">
					<span>{tr("No_members_yet")}</span>
				</RFlex>
			)}
		</RFlex>
	);
};

export default RReviewUserData;
