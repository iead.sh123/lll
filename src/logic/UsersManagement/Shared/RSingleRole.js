import { AccordionTrigger } from "ShadCnComponents/ui/accordion";
import { AccordionItem } from "ShadCnComponents/ui/accordion";
import { AccordionContent } from "ShadCnComponents/ui/accordion";
import { Checkbox } from "ShadCnComponents/ui/checkbox";
import AppNewCheckbox from "components/Global/RComs/AppNewCheckbox/AppNewCheckbox";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import React from "react";
import iconsFa6 from "variables/iconsFa6";

const RSingleRole = ({ role, isOpen, isSelected, handleSelectRole, handleCollapseClicked, review, canEdit, index }) => {
	return (
		<AccordionItem value={index} className="flex-column align-items-start" styleProps={{ gap: "5px" }}>
			<AccordionTrigger className="justify-content-between align-items-center hover:no-underline" styleProps={{ gap: "1px", width: "80%" }}>
				<RFlex className="align-items-start" styleProps={{ gap: "8px" }}>
					<RFlex className="items-center">
						{!review && (
							<Checkbox
								checked={isSelected}
								onCheckedChange={(event) => {
									// event.stopPropagation();
									console.log("event", event);
									canEdit ? handleSelectRole(role) : "";
								}}
							/>
						)}
						<RFlex
							className="align-items-center"
							styleProps={{ gap: "3px", cursor: "pointer" }}
							onClick={() => handleCollapseClicked(role.id)}
						>
							<span className="p-0 m-0">{role.name}</span>
						</RFlex>
					</RFlex>
				</RFlex>
				<RFlex className="align-items-center justify-content-start">
					<span className="text-primary text-[12px]">
						{role.permissions.length} {tr("permission")}
					</span>
					<p className="p-0 m-0 text-themeBoldGrey " style={{ width: "400px", maxWidth: "400px" }}>
						{role.description}
					</p>
				</RFlex>
			</AccordionTrigger>
			<AccordionContent>
				<RFlex styleProps={{ paddingLeft: review ? "0px" : "23px" }} className="flex-column">
					{role?.permissions?.length <= 0 ? (
						<span className="text-themeBoldGrey text-[12px]">{tr("no_permissoins")}</span>
					) : (
						role.permissions.map((permission, index) => {
							return (
								<span className="p-0 m-0" style={{ fontSize: "12px" }}>
									{permission.name}
								</span>
							);
						})
					)}
				</RFlex>
			</AccordionContent>
		</AccordionItem>
	);
};

export default RSingleRole;
