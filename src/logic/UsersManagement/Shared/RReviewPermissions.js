import RFlex from "components/Global/RComs/RFlex/RFlex";
import RLister from "components/Global/RComs/RLister";
import React from "react";
import { permissionsHeader } from "../constants";
import styles from "./sharedStyle.module.scss";
import tr from "components/Global/RComs/RTranslator";
const RReviewPermissions = ({ savedPermissions }) => {
	// const records = savedPermissions.map((permission, index) => {
	// 	return {
	// 		details: permissionsHeader.map((header) => {
	// 			const privilegeValueComponent = permission.privileged == 1 ? <div className={styles.priviligeStyle}>{tr("Privilege")}</div> : "";
	// 			return {
	// 				key: header.key,
	// 				value: header.name == "privilege" ? privilegeValueComponent : permission[header.value],
	// 				type: header.name == "privilege" ? "component" : "",
	// 			};
	// 		}),
	// 	};
	// });
	const columns = [
		{
			accessorKey: "name",
			renderHeader: () => <span>{tr("Permissions")}</span>,
			renderCell: ({ row }) => <span>{row?.original?.name}</span>,
		},
		{
			accessorKey: "description",
			renderHeader: () => <span>{tr("Description")}</span>,
			renderCell: ({ row }) => <span>{row?.original?.description == "" ? "-" : row?.original?.description}</span>,
		},
		{
			accessorKey: "privilege",
			renderHeader: () => <span>{tr("privilege")}</span>,
			renderCell: ({ row }) => (row?.original?.privileged ? <div className={styles.priviligeStyle}>{tr("Privilege")}</div> : "-"),
		},
	];
	const records = { columns, data: savedPermissions };
	return (
		<RFlex className="flex-column">
			<span style={{ fontWeight: "bold" }}>{tr("Permisssions")}</span>
			{records?.data?.length > 0 ? (
				<RLister Records={records} activeScroll={true} convertRecordsShape={false} />
			) : (
				<RFlex className="flex-column text-themeBoldGrey">
					<span>{tr("No_permessions_yet")}</span>
				</RFlex>
			)}
		</RFlex>
	);
};

export default RReviewPermissions;
