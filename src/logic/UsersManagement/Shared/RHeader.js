import RButton from "components/Global/RComs/RButton";
import RDropdownIcon from "components/Global/RComs/RDropdownIcon/RDropdownIcon";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import styles from "../usersManagementStyle.module.scss";
import React, { useState } from "react";
import iconsFa6 from "variables/iconsFa6";
import * as colors from "config/constants";
import RSearchHeader from "components/Global/RComs/RSearchHeader/RSearchHeader";
import RDropdown from "components/RComponents/RDropdown/RDropdown";
import { Button } from "ShadCnComponents/ui/button";
import RAlertDialog from "components/RComponents/RAlertDialog";
import RSearchInput from "components/RComponents/RSearchInput";
import RDrawer from "components/RComponents/RDrawer/RDrawer";
import ColumnSettings from "./ColumnSettings";
const RHeader = ({
	createText,
	searchPlaceholder,
	actions = [],
	dataType,
	setBackSearchTerm,
	refetchFunction,
	isFetching,
	onCreateClick,
	showColumnSettings = false,
	showSearchHeader = true,
	showCreate = true,
	showRefresh = true,
	showBulk = true,
	showDeactivate = false,
	showCreateGroup = false,
	showAddToGroup = false,
	showSendEmail = false,
	showDelete = false,
	showImportCSV = false,
	showCreateAccount = false,
	deleteText = "Delete",
	showDetails = true,
	deactivateFn,
	createGroupFn,
	addToGroupFn,
	sendEmailFn,
	deleteFn,
	importCSVFn,
	createAccountFn,
	columnSettingsFn,
	deactivateLoading,
	deleteLoading,
	createGroupLoading,
	addToGroupLoading,
	sendEmailLoading,
	disableCreate = false,
	loadingCreate = false,
	addSearchToFilters = false,
	setAllMembersFilters,
	allMembersFilters,
	downloadTemplateFn,
	groups,
	handleAddUsersToGroups,
	selectedUsers,
	columns,
	setColumnsState,
	keyToInvalidate,
	setSavedColumnsState,
	tableData,
}) => {
	const [frontSearchTerm, setFrontSearchTerm] = useState("");
	const handleDragEnd = (event) => {
		console.log("dragEnd", event);
	};
	const handleSearch = (clearData) => {
		let data = frontSearchTerm;
		if (clearData == "") {
			data = clearData;
		}
		setBackSearchTerm(data);
		if (addSearchToFilters) {
			setAllMembersFilters({ ...allMembersFilters, searchText: data });
		}
	};
	const dropDownComponent = (
		<Button variant="outline" className="border-themePrimary flex items-center gap-1">
			{tr(`Bulk_Operations`)}
			<i className={iconsFa6.chevronDown} />
		</Button>
	);

	return (
		<RFlex className="flex-column" id="header" styleProps={{ width: "100%" }}>
			<RFlex className="justify-content-between" styleProps={{ width: "100%" }} id="Top">
				<RFlex id="Top Left" styleProps={{ gap: "15px" }}>
					{showCreate && (
						<Button
							onClick={() => {
								onCreateClick();
							}}
							className="m-0"
						>
							{tr(`${createText}`)}
						</Button>
					)}
					{showBulk && <RDropdown TriggerComponent={dropDownComponent} actions={actions} />}
					{showImportCSV && (
						<RButton
							text={tr(`import_csv`)}
							color="primary"
							outline
							onClick={() => {
								importCSVFn();
							}}
							className="m-0"
						/>
					)}
					{showRefresh && (
						<Button variant="outline" className="border-themePrimary flex items-center gap-1" onClick={refetchFunction}>
							{isFetching ? <i className={iconsFa6.spinner} /> : <i className={iconsFa6.rotateRight} />}
							{tr("Refresh")}
						</Button>
					)}
					{showCreateAccount && <RButton color="primary" outline className="m-0" onClick={createAccountFn} text={tr("create_account")} />}
					{showColumnSettings && (
						<RDrawer
							triggerComponent={
								<Button variant="outline" className="border-themePrimary">
									{tr(`Column_settings`)}{" "}
								</Button>
							}
							title={{ text: tr("Column_Settings") }}
							drawerBody={
								<ColumnSettings
									columns={columns}
									handleDragEnd={handleDragEnd}
									setColumnsState={setColumnsState}
									keyToInvalidate={keyToInvalidate}
									setSavedColumnsState={setSavedColumnsState}
									tableData={tableData}
								/>
							}
						/>
					)}
					{showDeactivate && (
						<RAlertDialog
							component={
								<Button variant="ghost" className="p-0 text-themePrimary hover:text-themePrimary">
									{tr("deactivate")}
								</Button>
							}
							title={`${tr("Are_you_sure_you_want_to_deactivate")} ${selectedUsers.length}`}
							description=""
							confirmAction={() => deactivateFn()}
							confirm={{ text: tr("yes,deactivate"), className: "bg-themeDanger" }}
						/>
					)}
					{showCreateGroup && (
						<RButton
							text={tr("Create_user_group")}
							color="link"
							className="text-primary m-0"
							onClick={() => createGroupFn()}
							loading={createGroupLoading}
						/>
					)}
					{showAddToGroup && (
						<RDropdown
							TriggerComponent={
								<button className="text-themePrimary m-0 text-[12px] flex gap-1 items-center">
									{tr("add_to_group")}
									<i className={iconsFa6.chevronRight} />
								</button>
							}
							actions={groups.map((group) => ({ name: group.name, onClick: () => handleAddUsersToGroups(group) }))}
						/>
					)}
					{showSendEmail && (
						<RButton
							text={tr("send_notification_email")}
							color="link"
							className="text-primary m-0"
							onClick={() => sendEmailFn()}
							loading={sendEmailLoading}
						/>
					)}
					{showDelete && (
						<RButton
							text={tr(`${deleteText}`)}
							color="link"
							className="m-0"
							onClick={() => deleteFn()}
							loading={deleteLoading}
							disabled={deleteLoading}
							faicon={iconsFa6.delete}
							style={{ color: colors.dangerColor }}
						/>
					)}
				</RFlex>
				{showDetails && (
					<RFlex id="Top right" className="align-items-center">
						<i className={iconsFa6.info} style={{ color: colors.boldGreyColor }} />
						<span className="m-0 p-0" style={{ color: colors.boldGreyColor }}>
							{tr("Details")}
						</span>
					</RFlex>
				)}
			</RFlex>
			{showSearchHeader && (
				<RSearchInput
					searchLoading={isFetching}
					inputPlaceholder={tr(searchPlaceholder)}
					searchData={frontSearchTerm}
					handleChangeSearch={setFrontSearchTerm}
					setSearchData={setFrontSearchTerm}
					handleSearch={handleSearch}
				/>
			)}
		</RFlex>
	);
};

export default RHeader;
