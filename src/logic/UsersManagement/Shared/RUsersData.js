import RProfileName from "components/Global/RComs/RProfileName/RProfileName";
import React, { useState } from "react";
import * as colors from "config/constants";
import AppNewCheckbox from "components/Global/RComs/AppNewCheckbox/AppNewCheckbox";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RLister from "components/Global/RComs/RLister";
import { RLabel } from "components/Global/RComs/RLabel";
import styles from "./sharedStyle.module.scss";
import { useHistory } from "react-router-dom";
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";
import { Checkbox } from "ShadCnComponents/ui/checkbox";
import RDropdown from "components/RComponents/RDropdown/RDropdown";
import { Button } from "ShadCnComponents/ui/button";
import RTooltip from "components/RComponents/RTooltip/RTooltip";
import RCheckDropdown from "components/RComponents/RCheckDropdown/RCheckDropdown";
import RAlertDialog from "components/RComponents/RAlertDialog";
const RUsersData = ({
	users,
	selectedUsers = [],
	selectOneFn,
	selectAllFn,
	userTypes,
	allMembersFilters,
	setAllMembersFilters,
	filterHeader = false,
	view = false,
	modal = false,
	deleteSingleMember,
	addUser,
	loadingIds,
	checkboxExists = true,
	changeSingleUserStatus,
	activeIdsLoading,
	inCreate = false,
	inModal = false,
	inUsers = false,
	headers,
	getTableData,
}) => {
	const history = useHistory();
	const getVisibleColumns = () => {
		// const notHiddenColumns = columns.filter((column) => !users?.headers?.[column.accessorKey]?.hidden);
		// return notHiddenColumns;
		// order the columns based on the header that came in the users.headers
		const columnsToHide = headers.filter((column) => column.hidden).map((column) => column.label);
		const notHiddenSortedColumns = columns
			.filter((column) => !columnsToHide.includes(column.accessorKey))
			.sort((a, b) => a.index - b.index);
		return notHiddenSortedColumns;
	};
	const getColumnIndex = (columnLabel) => {
		const columnIndex = headers.find((header) => header.label == columnLabel).index;
		return columnIndex;
	};
	const [genderActions, setGenderActions] = useState({
		1: {
			checked: false,
			name: "Male",
			onCheckedChange: (checkedFlag) => {
				const newFilter = checkedFlag ? { ...allMembersFilters } : { ...allMembersFilters, gender: "m" };
				setAllMembersFilters(newFilter);
			},
		},
		2: {
			checked: false,
			name: "Female",
			onCheckedChange: (checkedFlag) => {
				const newFilter = checkedFlag ? { ...allMembersFilters } : { ...allMembersFilters, gender: "f" };
				setAllMembersFilters(newFilter);
			},
		},
	});

	const [activeActions, setActiveActions] = useState({
		1: {
			checked: false,
			name: "Active",
			onCheckedChange: (checkedFlag) => {
				const newFilter = checkedFlag ? { ...allMembersFilters } : { ...allMembersFilters, is_active: true };
				setAllMembersFilters(newFilter);
			},
		},
		2: {
			checked: false,
			name: "Inactive",
			onCheckedChange: (checkedFlag) => {
				const newFilter = checkedFlag ? { ...allMembersFilters } : { ...allMembersFilters, is_active: false };
				setAllMembersFilters(newFilter);
			},
		},
	});

	const [userTypesActions, setUserTypeActions] = useState(
		userTypes?.reduce((result, userType, index) => {
			result[index] = {
				checked: false,
				name: userType?.type?.name,
				onCheckedChange: (checkedFlag) => {
					setAllMembersFilters((currentAllMembersFilters) => {
						const newFilter = checkedFlag
							? {
									...currentAllMembersFilters,
									typeNames: currentAllMembersFilters?.typeNames?.filter((typeName, index) => typeName != userType?.type?.name),
							  }
							: {
									...currentAllMembersFilters,
									typeNames: currentAllMembersFilters?.typeNames
										? [...currentAllMembersFilters?.typeNames, userType?.type?.name]
										: [userType?.type?.name],
							  };
						return newFilter;
					});
				},
			};
			return result;
		}, {})
	);
	const columns = [
		{
			inModal: true,
			index: getColumnIndex("full_name"),
			accessorKey: "full_name",
			renderHeader: () => (
				<RFlex styleProps={{ gap: "8px" }} className="items-center min-w-max">
					{checkboxExists && (
						<Checkbox
							checked={users?.records?.length == selectedUsers?.length}
							onCheckedChange={() => {
								selectAllFn(users?.records);
							}}
						/>
					)}
					<RFlex styleProps={{ gap: "5px" }}>
						{tr("User_name")}
						<span className={`${styles.lengthDiv} text-themePrimary min-w-max`} style={{ fontSize: "8px" }}>
							{users?.records?.length}
						</span>
					</RFlex>
				</RFlex>
			),
			renderCell: ({ row }) => (
				<RFlex styleProps={{ gap: "8px" }} className="items-center">
					{checkboxExists && (
						<Checkbox
							checked={selectedUsers.some((u1) => u1.id == row.original.id)}
							onCheckedChange={() => {
								selectOneFn(row.original);
							}}
						/>
					)}
					<RProfileName
						name={row.original.full_name}
						img={row.original.image}
						onClick={() => {
							history.push(`${baseURL}/${genericPath}/users-management/users/${row.original.id}`);
						}}
						flexStyleProps={{ cursor: "pointer" }}
						className={styles.hoverLink}
					/>
				</RFlex>
			),
		},
		{
			inModal: true,
			accessorKey: "user_types",
			index: getColumnIndex("user_types"),

			renderHeader: ({ column }) => {
				return (
					<RFlex className="items-baseline">
						<span className="min-w-max">{tr("user_types")}</span>
						<RCheckDropdown
							TriggerComponent={
								<Button variant="ghost" className="flex flex-col gap-2 p-0 h-fit">
									<i className={`${iconsFa6.filter}`} />
								</Button>
							}
							actions={userTypesActions}
							setActions={setUserTypeActions}
							multiFilter={true}
						/>
					</RFlex>
				);
			},
			renderCell: ({ row }) => (
				<p className="p-0 m-0 lg:w-[250px] 2xl:w-auto ">
					{row.original?.user_types?.length <= 0
						? "-"
						: row.original?.user_types?.map((userType, index) => {
								return (
									<>
										<RTooltip
											tooltipText={userType.is_active ? "active" : "inactive"}
											trigger={
												<span className={`${userType.is_active ? "text-black" : "text-themeLight"} cursor-default`}>{userType.type}</span>
											}
										/>
										{index != row.original?.user_types?.length - 1 ? "," : ""}
									</>
								);
						  })}
				</p>
			),
		},
		{
			inModal: true,
			index: getColumnIndex("identifier"),
			accessorKey: "identifier",
			renderHeader: () => <span className="min-w-max">{tr("ID")}</span>,
			renderCell: ({ row }) => <span>{row.getValue("identifier") ?? "-"}</span>,
		},
		{
			accessorKey: "gender",
			index: getColumnIndex("gender"),
			renderHeader: () => (
				<RFlex className="items-baseline">
					<span>{tr("Gender")}</span>
					<RCheckDropdown
						TriggerComponent={
							<Button variant="ghost" className="flex flex-col gap-2 p-0 h-fit">
								<i className={`${iconsFa6.filter} outline-none`} />
							</Button>
						}
						actions={genderActions}
						setActions={setGenderActions}
					/>
				</RFlex>
			),
			renderCell: ({ row }) => <span className="text-themeBoldGrey">{row.getValue("gender") == "m" ? tr("Male") : tr("Female")}</span>,
		},
		{
			inModal: true,
			accessorKey: "email",
			index: getColumnIndex("email"),
			renderHeader: () => <span className="min-w-max">{tr("Email")}</span>,
			renderCell: ({ row }) => <span className="text-themeBoldGrey">{row.getValue("email")}</span>,
		},
		{
			accessorKey: "address",
			index: getColumnIndex("address"),
			renderHeader: () => <span className="min-w-max">{tr("Address")}</span>,
			renderCell: ({ row }) => (
				<RTooltip
					tooltipText={row.getValue("address")}
					trigger={<span className="text-themeBoldGrey text-left">{row.getValue("address")?.slice(0, 25) ?? "-"}</span>}
				/>
			),
		},
		{
			accessorKey: "is_active",
			index: getColumnIndex("is_active"),
			renderHeader: () => (
				<RFlex className="items-baseline">
					<span>{tr("Status")}</span>
					<RCheckDropdown
						TriggerComponent={
							<Button variant="ghost" className="flex flex-col gap-2 p-0 h-fit">
								<i className={`${iconsFa6.filter}`} />
							</Button>
						}
						actions={activeActions}
						setActions={setActiveActions}
					/>
				</RFlex>
			),
			renderCell: ({ row }) =>
				row.original.is_active && inUsers ? (
					<RAlertDialog
						title={`${tr("Are_you_sure_you_want_to_deactivate")} ${row.original.full_name}`}
						description=""
						component={
							<span
								className="p-0 m-0 flex gap-2 items-center"
								style={{
									color: row.original.is_active ? colors.successColor : colors.dangerColor,
									cursor: checkboxExists && !inCreate ? "pointer" : "default",
								}}
							>
								{row.original.is_active ? tr("active") : tr("inactive")}
								{activeIdsLoading?.includes(row.original.id) && (
									<i
										className={`${iconsFa6.spinner}`}
										style={{ color: row.original.is_active ? colors.successColor : colors.dangerColor }}
									/>
								)}
							</span>
						}
						confirmAction={() =>
							checkboxExists && !inCreate ? (activeIdsLoading?.includes(row?.original?.id) ? "" : changeSingleUserStatus(row.original)) : ""
						}
					/>
				) : (
					<span
						className="p-0 m-0 flex gap-2 items-center"
						style={{
							color: row.original.is_active ? colors.successColor : colors.dangerColor,
						}}
					>
						{row.original.is_active ? tr("active") : tr("inactive")}
					</span>
				),
		},
		{
			accessorKey: "phone_number",
			index: getColumnIndex("phone_number"),
			renderHeader: () => <span className="min-w-max">{tr("phone_number")}</span>,
			renderCell: ({ row }) => <span>{row.getValue("phone_number") ?? "-"}</span>,
		},
		{
			index: getColumnIndex("emergency_number"),
			accessorKey: "emergency_number",
			renderHeader: () => <span className="min-w-max">{tr("emergency_number")}</span>,
			renderCell: ({ row }) => <span className="text-themeBoldGrey ">{row.getValue("emergency_number") ?? "-"}</span>,
		},
	];
	const actions = [
		{
			name: view ? "delete" : "add",
			actionIconClass: view ? "text-themeDanger" : "text-themePrimary",
			icon: view ? iconsFa6.delete : iconsFa6.plus,
			inDialog: view ? true : false,
			onClick: view ? undefined : ({ row }) => addUser(row.original),
			dialogTitle: ({ row }) => `${tr("Are_you_sure_you_want_to_delete")} ${row.original.full_name}`,
			confirmAction: view
				? ({ row }) => {
						deleteSingleMember([row.original]);
				  }
				: undefined,
		},
	];

	const records = {
		columns: inModal ? columns.filter((column) => column.inModal) : getVisibleColumns(columns),
		data: users?.records ?? [],
		actions: inCreate ? undefined : actions,
	};
	return (
		<RFlex styleProps={{ marginBottom: view || inModal ? "0px" : "75px" }}>
			<RLister
				Records={records}
				convertRecordsShape={false}
				line1={tr("no_users_yet")}
				containerClassName="max-h-[60vh]"
				callBack={getTableData}
			/>
		</RFlex>
	);
};

export default RUsersData;
