import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import React, { useEffect, useState } from "react";
import { Terms } from "../constants";
import { usersManagementApi } from "api/UsersManagement";
import tr from "components/Global/RComs/RTranslator";
import RHeader from "../Shared/RHeader";
import RUsersData from "../Shared/RUsersData";
import RCSVDrawer from "../Shared/RCSVDrawer";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RLoader from "components/Global/RComs/RLoader";
import { useHistory } from "react-router-dom";
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";
import { useMutateData } from "hocs/useMutateData";
import { deleteSweetAlert } from "components/Global/RComs/RAlert2";
import { useCUDToQueryKey } from "hocs/useCUDToQueryKey";
import { convertFiltersShape } from "../Shared/ConvertFiltersShape";
import { fileExcel } from "config/mimeTypes";
import { toast } from "react-toastify";
import RControlledAlertDialog from "components/RComponents/RContolledAlertDialog";
import RDrawer from "components/RComponents/RDrawer/RDrawer";
import RControlledDrawer from "components/RComponents/RControlledDrawer/RControlledDrawer";
import { Button } from "ShadCnComponents/ui/button";

const GUsers = () => {
	const [backUsersSearchTerm, setBackUserSearchTerm] = useState("");
	const [selectedMembers, setSelectedMembers] = useState([]);
	const [removeIdsLoading, setRemoveIdsLoading] = useState([]);
	const [activeIdsLoading, setActiveIdsLoading] = useState([]);
	const { CUDToQueryKey, operations } = useCUDToQueryKey();
	const [allMembersFilters, setAllMembersFilters] = useState({});
	const organizationId = 1;
	const [isDialogOpen, setIsDialogOpen] = useState(false);
	const [columnsState, setColumnsState] = useState([]);
	const [savedColumnsState, setSavedColumnsState] = useState([]);
	const history = useHistory();
	const [isDrawerOpen, setIsDrawerOpen] = useState({ isOpen: false, operationType: "" });
	const [tableData, setTableData] = useState();
	const getTableData = (data) => {
		setTableData(data);
	};
	useEffect(() => {
		columnsState.forEach((column) => column.pinned && tableData.getColumn(column.label).pin("left"));
	}, [tableData]);

	const toggleDrawer = () => {
		setIsDrawerOpen((prevState) => ({ isOpen: false, operationType: "" }));
	};

	console.log("isDrawerOpen", isDrawerOpen);
	const dropdownActions = [
		{
			name: tr("Bulk_delete"),
			onClick: () => (selectedMembers.length > 0 ? setIsDialogOpen(true) : ""),
		},
		{
			name: tr("Import_CSV"),
			onClick: () => {
				setIsDrawerOpen({ isOpen: true, operationType: "Import" });
			},
		},
		{
			name: tr("Export_CSV"),
			onClick: () => {
				exportUserMutaiton.mutate();
			},
		},
	];

	//--------------------------------Users Groups------------------------------------------
	const {
		data: groups,
		isLoading: isLoadingGroups,
		isFetching: isFetchingGroups,
		refetch: refetchGroups,
	} = useFetchDataRQ({
		queryKey: [Terms.UserGroups],
		keepPreviousData: true,
		queryFn: () => usersManagementApi.getUsersGroups(),
		onSuccessFn: (data) => {
			console.log(data);
		},
	});

	const handleAddUsersToGroups = (group) => {
		const payload = {
			user_ids: selectedMembers.map((user) => user.id),
		};
		addUsersToUserGroupMutation.mutate({ id: group.id, payload });
	};

	const addUsersToUserGroupMutation = useMutateData({
		queryFn: ({ id, payload }) => usersManagementApi.addMembersToUserGroup(id, payload),
		onSuccessFn: () => {
			toast.success(tr("Users_Added_Successfully"));
		},
	});
	//--------------------------------Members------------------------------------------------
	//-------------------------------import/export users------------------------------------
	const importUsersTempalteMutation = useMutateData({
		queryFn: () => usersManagementApi.downloadUsersTemplate(),
		downloadFile: true,
		mimeType: fileExcel[0],
		folderName: "Users Template.xlsx",
	});
	const exportUserMutaiton = useMutateData({
		queryFn: () => usersManagementApi.exportUsers(),
		downloadFile: true,
		mimeType: fileExcel[0],
		folderName: "Users.xlsx",
	});
	//-----------------------------------get Users with filters applyied-----------------------
	const {
		data: members,
		isLoading: isLoadingUsers,
		isFetching: isFetchingUsers,
		refetch: refetchUsers,
	} = useFetchDataRQ({
		queryKey: [Terms.Members, allMembersFilters],
		queryFn: () => {
			const filters = convertFiltersShape(allMembersFilters);

			return usersManagementApi.getAllOrganizationUsers(filters);
		},
		onSuccessFn: (data) => {
			const headers = data?.data?.data?.headers;
			const headersArray = headers.map((header, index) => ({
				id: index,
				index,
				label: Object.keys(header)[0],
				hidden: header[Object.keys(header)[0]].hidden,
				pinned: header[Object.keys(header)[0]].pinned,
			}));
			if (tableData) {
				// headersArray.forEach((header) => tableData.getColumn(header.label).pin("left"));
				console.log("tableData", tableData);
			}
			setColumnsState(headersArray);
			setSavedColumnsState(headersArray);
		},
		keepPreviousData: true,
	});
	//-------------------------------------get users types for filter------------------------
	const {
		data: userTypes,
		isLoading: isLoadingUserTypes,
		isFetching: isFetchingUserTypes,
	} = useFetchDataRQ({
		queryKey: [Terms.UserTypes],
		queryFn: () => usersManagementApi.getUsersTypes(false),
	});

	const handleSelectMember = (member) => {
		const alreadyChecked = selectedMembers.some((m1) => m1.id == member.id);
		if (alreadyChecked) {
			const newMembersArray = selectedMembers.filter((m1) => m1.id != member.id);
			setSelectedMembers(newMembersArray);
		} else {
			const newMembersArray = [...selectedMembers, member];
			setSelectedMembers(newMembersArray);
		}
	};
	const handleSelectAllMembers = (allMembers) => {
		const allIsSelected = allMembers?.length == selectedMembers?.length;
		if (allIsSelected) {
			setSelectedMembers([]);
		} else {
			const newMembersArray = [...allMembers];
			setSelectedMembers(newMembersArray);
		}
	};
	//----------------------------removing users--------------------------------
	const handleRemoveMembers = (members) => {
		if (members.length <= 0) {
			return;
		}
		const userIds = members.map((member) => member.id);
		const payload = {
			user_ids: userIds,
		};
		removeUsersMutation.mutate({ payload });
		setRemoveIdsLoading([...removeIdsLoading, ...userIds]);
	};

	const removeUsersMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.removeUserFromOrganization(organizationId, payload),
		onSuccessFn: ({ variables }) => {
			setSelectedMembers(selectedMembers.filter((member) => !variables?.payload?.user_ids?.includes(member.id)));
			variables?.payload?.user_ids?.forEach((id, index) => {
				CUDToQueryKey({
					operation: operations.DELETE,
					queryKey: [Terms.Members, allMembersFilters],
					id,
					insertionDepth: "data.data.records",
				});
			});
			setRemoveIdsLoading(removeIdsLoading.filter((id) => !variables?.payload?.user_ids?.includes(id)));
		},
		onErrorFn: (error, variables) => {
			setRemoveIdsLoading(removeIdsLoading.filter((id) => !variables?.payload?.user_ids?.includes(id)));
		},
		invalidateKeys: [Terms.Members, allMembersFilters],
	});
	//------------------------------------activation status----------------------------------
	const deactivateUsersMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.deactivateUsersAccounts(payload),
		invalidateKeys: [Terms.Members, allMembersFilters],
		onSuccessFn: ({ variables }) => {
			setActiveIdsLoading(activeIdsLoading.filter((id) => !variables?.payload?.user_ids?.includes(id)));
			variables?.payload?.user_ids?.forEach((id, index) => {
				CUDToQueryKey({
					operation: operations.UPDATE,
					queryKey: [Terms.Members, allMembersFilters],
					id,
					newData: { is_active: false },
					insertionDepth: "data.data.records",
				});
			});
		},
		onErrorFn: (error, variables) => {
			setActiveIdsLoading(activeIdsLoading.filter((id) => !variables?.payload?.user_ids?.includes(id)));
		},
	});
	const activateUsersMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.activateUsersAccounts(payload),
		invalidateKeys: [Terms.Members, allMembersFilters],
		onSuccessFn: ({ variables }) => {
			setActiveIdsLoading(activeIdsLoading.filter((id) => !variables?.payload?.user_ids?.includes(id)));
			variables?.payload?.user_ids?.forEach((id, index) => {
				CUDToQueryKey({
					operation: operations.UPDATE,
					queryKey: [Terms.Members, allMembersFilters],
					id,
					newData: { is_active: true },
					insertionDepth: "data.data.records",
				});
			});
		},
		onErrorFn: (error, variables) => {
			setActiveIdsLoading(activeIdsLoading.filter((id) => !variables?.payload?.user_ids?.includes(id)));
		},
	});
	const changeMultipleUserStatus = () => {
		if (selectedMembers?.length <= 0) return;
		const userIds = selectedMembers.map((member) => member.id);
		const payload = {
			user_ids: userIds,
		};
		deactivateUsersMutation.mutate({ payload });
		setActiveIdsLoading([...activeIdsLoading, ...userIds]);
	};
	const changeSingleUserStatus = (user) => {
		const payload = {
			user_ids: [user.id],
		};
		if (user.is_active) {
			deactivateUsersMutation.mutate({ payload });
			setActiveIdsLoading([...activeIdsLoading, user.id]);
		} else {
			activateUsersMutation.mutate({ payload });
			setActiveIdsLoading([...activeIdsLoading, user.id]);
		}
	};
	console.log("allMembersFilters", allMembersFilters);
	//----------------------------------------global------------------------------------------
	if (isLoadingUsers || isLoadingUserTypes || isLoadingGroups) {
		return <RLoader />;
	}
	return (
		<RFlex className="flex-column">
			<RHeader
				actions={dropdownActions}
				selectedUsers={selectedMembers}
				createText={"New User"}
				onCreateClick={() => history.push(`${baseURL}/${genericPath}/users-management/users/add`)}
				showCreate={true}
				showSearchHeader={true}
				showBulk={true}
				showColumnSettings={selectedMembers?.length > 0 ? false : true}
				showRefresh={selectedMembers?.length > 0 ? false : true}
				refetchFunction={refetchUsers}
				isFetching={isFetchingUsers}
				deactivateFn={changeMultipleUserStatus}
				setBackSearchTerm={setBackUserSearchTerm}
				allMembersFilters={allMembersFilters}
				setAllMembersFilters={setAllMembersFilters}
				showDeactivate={selectedMembers?.length > 0}
				showAddToGroup={selectedMembers?.length > 0}
				showSendEmail={false}
				groups={groups?.data?.data}
				handleAddUsersToGroups={handleAddUsersToGroups}
				columns={columnsState}
				setColumnsState={setColumnsState}
				addSearchToFilters
				keyToInvalidate={[Terms.Members, allMembersFilters]}
				setSavedColumnsState={setSavedColumnsState}
				tableData={tableData}
			/>

			<RUsersData
				users={members?.data?.data}
				userTypes={userTypes?.data?.data}
				allMembersFilters={allMembersFilters}
				setAllMembersFilters={setAllMembersFilters}
				selectedUsers={selectedMembers}
				selectOneFn={handleSelectMember}
				selectAllFn={handleSelectAllMembers}
				deleteSingleMember={handleRemoveMembers}
				isRemoving={removeUsersMutation.isLoading}
				changeSingleUserStatus={changeSingleUserStatus}
				activeIdsLoading={activeIdsLoading}
				removeIdsLoading={removeIdsLoading}
				inUsers={true}
				view={true}
				getTableData={getTableData}
				headers={savedColumnsState}
			/>

			<RCSVDrawer
				downloadTemplateFn={() => importUsersTempalteMutation.mutate()}
				isOpen={isDrawerOpen.isOpen}
				operationType={isDrawerOpen.operationType}
				toggleDrawer={toggleDrawer}
				currentType={Terms.Users}
			/>
			<RControlledAlertDialog
				title={`${tr("Are_you_sure_you_want_to_delete")} ${selectedMembers.length}`}
				isOpen={isDialogOpen}
				handleCloseAlert={() => setIsDialogOpen(false)}
				confirmAction={() => handleRemoveMembers(selectedMembers)}
			/>
		</RFlex>
	);
};

export default GUsers;
