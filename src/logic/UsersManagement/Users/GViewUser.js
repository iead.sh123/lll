import RFlex from "components/Global/RComs/RFlex/RFlex";
import React, { useEffect, useRef, useState } from "react";
import iconsFa6 from "variables/iconsFa6";
import * as colors from "config/constants";
import { useHistory, useParams } from "react-router-dom";
import RTabs from "components/Global/RComs/RTabs/RTabs";
import { Terms, usersTabs } from "../constants";
import styles from "./userStyle.module.scss";
import tr from "components/Global/RComs/RTranslator";
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import { Form, useFormik } from "formik";
import * as Yup from "yup";
import RButton from "components/Global/RComs/RButton";
import RUserBasicInfo from "./RUserBasicInfo";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { usersManagementApi } from "api/UsersManagement";
import RLoader from "components/Global/RComs/RLoader";
import { useMutateData } from "hocs/useMutateData";
import RRoles from "../Shared/RRoles";
import RPermissons from "../Shared/RPermissons";
import { useCUDToQueryKey } from "hocs/useCUDToQueryKey";
import { Button } from "ShadCnComponents/ui/button";
import RNewTabs from "components/RComponents/RNewTabs";
const GViewUser = () => {
	//---------------------------------Global-----------------------------------------------------
	const [activeTab, setActiveTab] = useState(Terms.BasicInfo);
	const { CUDToQueryKey, operations } = useCUDToQueryKey();
	const currentOrganization = { title: "TestOrg", id: 1 };
	const { userId: paramUserId } = useParams();
	const [currentUserTypes, setCurrentUserTypes] = useState([]);
	const [pickedUserType, setPickedUserType] = useState({
		id: -1,
		title: tr("on_organization_Level"),
		active: true,
		roles: [],
		permissions: [],
	});
	const [pickedGroup, setPickedGroup] = useState(null);
	//-----------------------------------Groups-------------------------------------------
	const {
		data: groups,
		isLoading: isLoadingGroups,
		isFetching: isFetchingGroups,
		refetch: refetchGroups,
	} = useFetchDataRQ({
		queryKey: [Terms.UserGroups],
		keepPreviousData: true,
		queryFn: () => usersManagementApi.getUsersGroups(),
		onSuccessFn: (data) => {},
	});
	const handleSelectGroup = (pickedOption) => {
		if (!pickedOption.value) {
			setPickedGroup(null);
			return;
		}
		setPickedGroup(pickedOption);
	};

	const handleChangePickedUserType = (pickedUserType) => {
		setPickedUserType(pickedUserType);
		const sortedRoles = getSortedRoles(roles, pickedUserType);
		CUDToQueryKey({
			operation: operations.REPLACE,
			queryKey: [Terms.Roles, backRoleSearchTerm],
			newData: sortedRoles,
			insertionDepth: "data.data",
		});
		const sortedPermissions = getSortedPermissions(permissions, pickedUserType);
		CUDToQueryKey({
			operation: operations.REPLACE,
			queryKey: [Terms.Permissions, backendPermissoinSearchTerm],
			newData: sortedPermissions,
			insertionDepth: "data.data",
		});
	};

	const [finsihGettingUser, setFinishGettingUser] = useState(false);
	const {
		data: userDetails,
		isLoading: isLoadingUserDetails,
		isFetching: isFetchingUserDeatils,
	} = useFetchDataRQ({
		queryKey: [Terms.Users, paramUserId],
		queryFn: () => usersManagementApi.getUserDetails(paramUserId),
		onSuccessFn: (data) => {
			const info = data?.data?.data;
			setFinishGettingUser(true);
			setValues({
				username: info.name,
				firstname: info.first_name,
				lastname: info.last_name,
				email: info.email,
				userid: info.identifier,
				address: info.details?.address ?? "-",
				phone: info.details?.phone_number ?? "",
				emergency: info.details?.emergency_number ?? "",
			});
			setGender(info.gender == "m" ? "male" : "female");
			// setUserTypesCount();
			setHashId(info.image);
			const types = data?.data?.data?.organization_users.map((userType, index) => ({
				id: userType.type ? userType.type.id : -1,
				title: userType.type ? userType.type.type.name : tr("on_organization_level"),
				active: userType.is_active,
				roles: userType.roles,
				permissions: userType.permissions,
			}));
			const userTypesCountArray = types
				.filter((type) => type.id > 0)
				.map((type, index) => ({
					id: type.id,
					activeStatus: type.active,
					pickedOption: { label: type.title, value: type.id },
					disabledStatus: false,
				}));
			setUserTypesCount(userTypesCountArray);
			const isAccountActive = types.some((type) => {
				return type.active;
			});
			setAccountActiveStatus(isAccountActive);

			let typesSelectedPermissions = {};
			let typesSelectedRoles = {};
			let typeSavedPermissions = {};
			let typeSavedRoles = {};
			types.forEach((type, index) => {
				typesSelectedPermissions[type.id] = type.permissions;
				typesSelectedRoles[type.id] = type.roles;
				typeSavedPermissions[type.id] = type.permissions;
				typeSavedRoles[type.id] = type.roles;
			});
			setSelectedPermissions({ ...selectedPermissions, ...typesSelectedPermissions });
			setSelectedRoles({ ...selectedRoles, ...typesSelectedRoles });
			setSavedPermissions({ ...savedPermissions, ...typeSavedPermissions });
			setSavedRoles({ ...savedRoles, ...typeSavedRoles });
			setCurrentUserTypes([...types]);
		},
	});
	//---------------------------------User Basic Info--------------------------------------------------
	const [userTypesCount, setUserTypesCount] = useState([{ id: 1, pickedOption: null, activeStatus: false, disabledStatus: true }]);
	const [accountActiveStatus, setAccountActiveStatus] = useState(true);
	const [basicInfoEditMode, setBasicInfoEditMode] = useState(false);
	const [uploadId, setUploadId] = useState(null);
	const [hashId, setHashId] = useState(null);
	const [pickedOrganization, setPickedOrganization] = useState(null);
	const [gender, setGender] = useState("male");
	const initialValues = {
		username: "",
		firstname: "",
		lastname: "",
		userid: "",
		email: "",
		address: "",
		phone: "",
		emergency: "",
	};
	const phoneRegex = /^[+]?[(]?[0-9]{1,4}[)]?[-\s.]?[0-9]{1,3}[-\s.]?[0-9]{3,4}[-\s.]?[0-9]{3,4}$/im;
	const validationSchema = Yup.object({
		username: Yup.string().required(tr("User_Name_is_required")),
		firstname: Yup.string().required(tr("First_Name_is_required")),
		lastname: Yup.string().required(tr("Last_Name_is_required")),
		email: Yup.string().email(tr("email_is_invalid")).required(tr("email_is_required")),
		phone: Yup.string().matches(phoneRegex, tr("Invalide_Phone_Number")),
		emergency: Yup.string().matches(phoneRegex, tr("Invalide_Phone_Number")),
		userid: Yup.string().required(tr("User_id_Is_Required")),
	});
	const { values, touched, errors, setFieldValue, setValues, setFieldTouched, handleSubmit, setFieldError, handleChange, handleBlur } =
		useFormik({
			initialValues,
			validationSchema,
			validateOnMount: true,

			onSubmit: (values) => getSaveAction(),
		});
	const handleAddingUserType = () => {
		const maxId = userTypesCount.reduce((max, type) => (max > type.id ? max : type.id), 0);
		setUserTypesCount([...userTypesCount, { id: maxId + 1, pickedOption: null, activeStatus: false, disabledStatus: true }]);
	};
	const {
		data: userTypes,
		isLoading: isLoadingUserTypes,
		isFetching: isFetchingUserTypes,
	} = useFetchDataRQ({
		queryKey: [Terms.UserTypes],
		queryFn: () => usersManagementApi.getUsersTypes(),
	});
	const handleSelectOption = (option, stateType) => {
		const userTypesCountCopy = JSON.parse(JSON.stringify(userTypesCount));
		const typeIndex = userTypesCountCopy.findIndex((type) => type.id == stateType.id);
		if (!option.value) {
			userTypesCountCopy[typeIndex].pickedOption = [];
			userTypesCountCopy[typeIndex].disabledStatus = true;
			userTypesCountCopy[typeIndex].activeStatus = false;
		} else {
			userTypesCountCopy[typeIndex].pickedOption = option;
			userTypesCountCopy[typeIndex].disabledStatus = false;
			userTypesCountCopy[typeIndex].activeStatus = true;
		}
		setUserTypesCount(userTypesCountCopy);
	};
	const handleSwitchChange = (stateType) => {
		const userTypesCountCopy = JSON.parse(JSON.stringify(userTypesCount));
		const typeIndex = userTypesCountCopy.findIndex((type) => type.id == stateType.id);
		userTypesCountCopy[typeIndex].activeStatus = !userTypesCountCopy[typeIndex].activeStatus;
		setUserTypesCount(userTypesCountCopy);
	};
	const handleAccountActiveChanged = () => {
		setAccountActiveStatus(!accountActiveStatus);
	};
	const generateEmailFromUsername = () => {
		if (errors.username) {
			setFieldTouched("username", true);
			return;
		}
		const domain = "@OrganizationName.org";
		const email = values.username.trim().replace(/\s/g, ".") + domain;
		setFieldValue("email", email);
	};
	const suggestPassword = () => {
		const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		let password = "";
		const length = 8; // Length of the password

		for (let i = 0; i < length; i++) {
			const randomIndex = Math.floor(Math.random() * characters.length);
			password += characters.charAt(randomIndex);
		}
		setFieldValue("password", password);
	};

	const updateUserAccountMutation = useMutateData({
		queryFn: ({ payload }) => usersManagementApi.updateUserAccount(paramUserId, payload),
		onSuccessFn: ({ data }) => {
			const types = data?.data?.organization_users.map((userType, index) => ({
				id: userType.type ? userType.type.id : -1,
				title: userType.type ? userType.type.type.name : tr("on_organiztion_level"),
				active: false,
				roles: userType.roles,
				permissions: userType.permissions,
			}));
			let typesSelectedPermissions = {};
			let typesSelectedRoles = {};
			let typeSavedPermissions = {};
			let typeSavedRoles = {};
			types.forEach((type, index) => {
				typesSelectedPermissions[type.id] = type.permissions;
				typesSelectedRoles[type.id] = type.roles;
				typeSavedPermissions[type.id] = type.permissions;
				typeSavedRoles[type.id] = type.roles;
			});
			setSelectedPermissions({ ...selectedPermissions, ...typesSelectedPermissions });
			setSelectedRoles({ ...selectedRoles, ...typesSelectedRoles });
			setSavedPermissions({ ...savedPermissions, ...typeSavedPermissions });
			setSavedRoles({ ...savedRoles, ...typeSavedRoles });
			setCurrentUserTypes([...types]);
			setBasicInfoEditMode(false);
			setHashId(data?.data?.image);
			// scrollToTop();
		},
		invalidateKeys: [Terms.Users, paramUserId],
	});
	const handleCreatingNewUserAccount = () => {
		if (Object.keys(errors).length > 0) {
			return;
		}
		const types = userTypesCount.map((type, index) => ({ id: type?.pickedOption?.value, active: type?.activeStatus }));
		const finalTypes = types.filter((type) => type.id);
		const payload = {
			name: values.username,
			image: uploadId.url ? [uploadId] : undefined,
			first_name: values.firstname,
			last_name: values.lastname,
			gender: gender.charAt(0),
			email: values.email,
			password: values.password,
			c_password: values.password,
			organization_group_id: pickedGroup ? pickedGroup.value : undefined,
			organization_id: currentOrganization.id,
			details: {
				address: values.address,
				emergency_number: values.emergency,
				phone_number: values.phone,
			},
			is_active: accountActiveStatus,
			identifier: values.userid,
			types: finalTypes.length > 0 ? finalTypes : undefined,
			// tags: [
			// 	{
			// 		id: 0,
			// 		name: "string",
			// 		color_id: 0,
			// 	},
			// ,
		};
		const removedTypesIds = getRemovedUserTypes();
		removedTypesIds.length > 0 &&
			removedTypesIds.forEach((userTypeId) => {
				userTypeId > 0 && removeMembersMutation.mutate({ id: userTypeId, payload: { user_ids: [paramUserId] } });
			});
		updateUserAccountMutation.mutate({ payload });
	};
	const handleSelectOrganization = (e) => {
		setPickedOrganization(e);
	};
	const topRef = useRef(null);

	const getRemovedUserTypes = () => {
		const savedUserTypesIds = currentUserTypes.map((userType) => userType.id);
		const currentUserTypesIds = userTypesCount.map((userType) => userType.pickedOption?.value);
		const removedTypesIds = savedUserTypesIds.filter((userTypeId) => !currentUserTypesIds.includes(userTypeId));
		return removedTypesIds;
	};
	const removeMembersMutation = useMutateData({
		queryFn: ({ id, payload }) => usersManagementApi.removeUsersFromUserType(id, payload),
		// invalidateKeys: [],
	});
	//--------------------------------Roles------------------------------------------
	const [backRoleSearchTerm, setBackRoleSearchTerm] = useState("");
	const [frontRoleSearchTerm, setFrontRoleSearchTerm] = useState("");
	const [selectedRoles, setSelectedRoles] = useState({});
	const [savedRoles, setSavedRoles] = useState({});
	const [rolesEditMode, setRolesEditMode] = useState(false);

	const {
		data: roles,
		isLoading: isLoadingRoles,
		isFetching: isFetchingRoles,
		refetch: refetchRoles,
	} = useFetchDataRQ({
		queryKey: [Terms.Roles, backRoleSearchTerm],
		enableCondition: finsihGettingUser,
		keepPreviousData: true,
		queryFn: () => usersManagementApi.getOrganizationRoles(backRoleSearchTerm),
		onSuccessFn: (data) => {
			// const sortedRoles = getSortedRoles(data);
			// CUDToQueryKey({
			// 	operation: operations.REPLACE,
			// 	queryKey: [Terms.Roles, backRoleSearchTerm],
			// 	newData: sortedRoles,
			// 	insertionDepth: "data.data",
			// });
		},
	});

	const handleSelectRole = (role) => {
		const alreadyChecked = selectedRoles[pickedUserType.id].some((p1) => p1.id == role.id);
		if (alreadyChecked) {
			const newRolesArray = { ...selectedRoles, [pickedUserType.id]: selectedRoles[pickedUserType.id].filter((p1) => p1.id != role.id) };
			setSelectedRoles(newRolesArray);
		} else {
			const newRolesArray = { ...selectedRoles, [pickedUserType.id]: [...selectedRoles[pickedUserType.id], role] };
			setSelectedRoles(newRolesArray);
		}
	};
	// const handleSelectAllRoles = (allRoles) => {
	// 	const allIsSelected = allRoles.length == selectedRoles.length;
	// 	if (allIsSelected) {
	// 		setSelectedRoles([]);
	// 	} else {
	// 		const newRolesArray = [...allRoles];
	// 		setSelectedRoles(newPermessionsArray);
	// 	}
	// };
	const removeRolesFromUserInSpecificUserTypeMutation = useMutateData({
		queryFn: ({ typeId, payload }) => usersManagementApi.removeRolesFromUsers(typeId < 0 ? undefined : typeId, payload),
		onSuccessFn: () => {
			// scrollToTop();
		},
		invalidateKeys: [Terms.Users, paramUserId],
	});
	const assignRolesToUserInSpecificUserTypeMutation = useMutateData({
		queryFn: ({ typeId, payload }) => usersManagementApi.assignRolesToUsers(typeId < 0 ? undefined : typeId, payload),
		onSuccessFn: () => {
			// scrollToTop();
		},
		invalidateKeys: [Terms.Users, paramUserId],
	});
	const getRemovedRolesForEachUserType = () => {
		const idsInEachUserTypeSelectedRoles = Object.keys(selectedRoles).reduce((result, key) => {
			result[key] = new Set(selectedRoles[key].map((role) => role.id));
			return result;
		}, {});
		const removedRolesInEachUserType = Object.keys(savedRoles).reduce((result, key) => {
			result[key] = savedRoles[key].filter((role, index) => !idsInEachUserTypeSelectedRoles[key].has(role.id));
			return result;
		}, {});
		const removedRolesNamesInEachUserType = Object.keys(removedRolesInEachUserType).reduce((result, key) => {
			result[key] = removedRolesInEachUserType[key].map((role, index) => role.name);
			return result;
		}, {});
		return removedRolesNamesInEachUserType;
	};
	const getSortedRoles = (roles, pickedOption) => {
		const rolesCopy = JSON.parse(JSON.stringify(roles));
		const savedUserTypeRolesIds = new Set(savedRoles[pickedOption.id]?.map((r) => r.id));
		const rolesArray = rolesCopy?.data?.data;
		rolesArray?.sort((r1, r2) => {
			const r1IsSelected = savedUserTypeRolesIds.has(r1.id);
			const r2IsSelected = savedUserTypeRolesIds.has(r2.id);
			if (r1IsSelected && !r2IsSelected) {
				return -1;
			}
			if (!r1IsSelected && r2IsSelected) {
				return 1;
			}
			return 0;
		});
		return rolesArray;
	};
	//----------------------------Permissions--------------------------------------------
	const [backendPermissoinSearchTerm, setBackendPermissoinSearchTerm] = useState("");
	const [frontPermissionSearchTerm, setFrontPermissionSearchTerm] = useState("");
	const [selectedPermissions, setSelectedPermissions] = useState({});
	const [savedPermissions, setSavedPermissions] = useState({});
	const [permissionsEditMode, setPermissionsEditMode] = useState(false);
	const {
		data: permissions,
		isLoading: isLoadingPermissions,
		isFetching: isFetchingPermissions,
	} = useFetchDataRQ({
		queryKey: [Terms.Permissions, backendPermissoinSearchTerm],
		queryFn: () => usersManagementApi.getOrganizationPermissions(backendPermissoinSearchTerm),
		keepPreviousData: true,
		enableCondition: finsihGettingUser,
		onSuccessFn: (data) => {
			// const sortedPermissions = getSortedPermissions(data);
			// CUDToQueryKey({
			// 	operation: operations.REPLACE,
			// 	queryKey: [Terms.Permissions, backendPermissoinSearchTerm],
			// 	newData: sortedPermissions,
			// 	insertionDepth: "data.data",
			// });
		},
	});

	const handleSelectPermission = (permission) => {
		const alreadyChecked = selectedPermissions[pickedUserType.id].some((p1) => p1.id == permission.id);
		if (alreadyChecked) {
			const newPermessionsArray = {
				...selectedPermissions,
				[pickedUserType.id]: selectedPermissions[pickedUserType.id].filter((p1) => p1.id != permission.id),
			};
			setSelectedPermissions(newPermessionsArray);
		} else {
			const newPermessionsArray = { ...selectedPermissions, [pickedUserType.id]: [...selectedPermissions[pickedUserType.id], permission] };
			setSelectedPermissions(newPermessionsArray);
		}
	};
	const handleSelectAllPermissions = (allPermissions) => {
		const allIsSelected = allPermissions.length == selectedPermissions[pickedUserType.id].length;
		if (allIsSelected) {
			setSelectedPermissions({ ...selectedPermissions, [pickedUserType.id]: [] });
		} else {
			const newPermessionsArray = [...allPermissions];
			setSelectedPermissions({ ...selectedPermissions, [pickedUserType.id]: newPermessionsArray });
		}
	};

	const assignPermissionsToUserInSpecificUserTypeMutation = useMutateData({
		queryFn: ({ typeId, payload }) => usersManagementApi.assignPermissonsToUsers(typeId < 0 ? undefined : typeId, payload),
		invalidateKeys: [Terms.Users, paramUserId],
		onSuccessFn: () => {
			// history.replace(`${baseURL}/${genericPath}/users-management/users`);
		},
	});
	const removePermissionsFromUserInSpecificUserTypeMutation = useMutateData({
		queryFn: ({ typeId, payload }) => usersManagementApi.removePermissionsFromUsers(typeId < 0 ? undefined : typeId, payload),
		invalidateKeys: [Terms.Users, paramUserId],
		onSuccessFn: () => {
			// history.replace(`${baseURL}/${genericPath}/users-management/users`);
		},
	});
	const getRemovedPermissionsForEachUserType = () => {
		const idsInEachUserTypeSelectedPermissions = Object.keys(selectedPermissions).reduce((result, key) => {
			result[key] = new Set(selectedPermissions[key].map((permission) => permission.id));
			return result;
		}, {});
		const removedPermissionsInEachUserType = Object.keys(savedPermissions).reduce((result, key) => {
			result[key] = savedPermissions[key].filter((permission, index) => !idsInEachUserTypeSelectedPermissions[key].has(permission.id));
			return result;
		}, {});
		const removedPermissionsNamesInEachUserType = Object.keys(removedPermissionsInEachUserType).reduce((result, key) => {
			result[key] = removedPermissionsInEachUserType[key].map((permission, index) => permission.name);
			return result;
		}, {});
		return removedPermissionsNamesInEachUserType;
	};
	const getSortedPermissions = (permissions, pickedOption) => {
		const permissionsCopy = JSON.parse(JSON.stringify(permissions));
		const savedUserTypePermissionsIds = new Set(savedPermissions[pickedOption.id]?.map((p) => p.id));
		const permissionsArray = permissionsCopy?.data?.data;
		permissionsArray?.sort((p1, p2) => {
			const p1IsSelected = savedUserTypePermissionsIds.has(p1.id);
			const p2IsSelected = savedUserTypePermissionsIds.has(p2.id);
			if (p1IsSelected && !p2IsSelected) {
				return -1;
			}
			if (!p1IsSelected && p2IsSelected) {
				return 1;
			}
			return 0;
		});
		return permissionsArray;
	};
	//----------------------------------------Global-------------------------------------------
	const history = useHistory();

	const scrollToTop = () => {
		if (topRef.current) {
			topRef.current.scrollIntoView({ behavior: "smooth", block: "start" });
		}
	};
	const handleEditSaveClicked = () => {
		let payload = {};
		switch (activeTab) {
			case Terms.BasicInfo:
				if (!basicInfoEditMode) {
					setBasicInfoEditMode(true);
					return;
				}
				handleCreatingNewUserAccount();
				break;
			case Terms.Roles:
				if (!rolesEditMode) {
					setRolesEditMode(true);
					return;
				}
				let newSavedRoles = {};
				const allUserTypeRemovedRoles = getRemovedRolesForEachUserType();
				Object.keys(allUserTypeRemovedRoles).forEach((key, index) => {
					const removedRolesNames = allUserTypeRemovedRoles[key];
					if (removedRolesNames.length > 0) {
						payload = {
							role_names: removedRolesNames,
							user_ids: [paramUserId],
						};
						removeRolesFromUserInSpecificUserTypeMutation.mutate({ typeId: key, payload });
					}
					if (selectedRoles[key].length > 0) {
						const roleNames = selectedRoles[key].map((role) => role.name);
						payload = {
							role_names: roleNames,
							user_ids: [paramUserId],
						};
						assignRolesToUserInSpecificUserTypeMutation.mutate({ typeId: key, payload });
					}
					newSavedRoles[key] = selectedRoles[key];
				});
				const sortedRoles = getSortedRoles(roles, pickedUserType);
				CUDToQueryKey({
					operation: operations.REPLACE,
					queryKey: [Terms.Roles, backRoleSearchTerm],
					newData: sortedRoles,
					insertionDepth: "data.data",
				});
				setRolesEditMode(false);
				setSavedRoles({ ...newSavedRoles });
				// setActiveTab(Terms.Permissions);
				break;
			case Terms.Permissions:
				if (!permissionsEditMode) {
					setPermissionsEditMode(true);
					return;
				}
				let newSavedPermissions = {};
				const allUserTypeRemovedPermissions = getRemovedPermissionsForEachUserType();
				Object.keys(allUserTypeRemovedPermissions).forEach((key, index) => {
					const removedPermissionsNames = allUserTypeRemovedPermissions[key];
					if (removedPermissionsNames.length > 0) {
						payload = {
							permission_names: removedPermissionsNames,
							user_ids: [paramUserId],
						};
						removePermissionsFromUserInSpecificUserTypeMutation.mutate({ typeId: key, payload });
					}
					if (selectedPermissions[key].length > 0) {
						const permissionsNames = selectedPermissions[key].map((permission) => permission.name);
						payload = {
							permission_names: permissionsNames,
							user_ids: [paramUserId],
						};
						assignPermissionsToUserInSpecificUserTypeMutation.mutate({ typeId: key, payload });
					}
					newSavedPermissions[key] = selectedPermissions[key];
				});
				const sortedPermissions = getSortedPermissions(permissions, pickedUserType);
				CUDToQueryKey({
					operation: operations.REPLACE,
					queryKey: [Terms.Permissions, backendPermissoinSearchTerm],
					newData: sortedPermissions,
					insertionDepth: "data.data",
				});
				setPermissionsEditMode(false);
				setSavedPermissions({ ...newSavedPermissions });
				// setActiveTab(Terms.Permissions);
				break;
		}
		return;
	};
	console.log(currentUserTypes);
	return isLoadingUserDetails || isLoadingGroups ? (
		<RLoader />
	) : (
		<RFlex className="flex-column" styleProps={{ position: "relative" }}>
			<RFlex className="align-items-center">
				<div
					ref={topRef}
					style={{ cursor: "pointer" }}
					className={styles.backArrow}
					onClick={() => history.replace(`${baseURL}/${genericPath}/users-management/users`)}
				>
					<i className={iconsFa6.chevronLeft} style={{ color: colors.primaryColor }} />
				</div>
				{userDetails?.data?.data?.full_name}
			</RFlex>
			<RFlex className="justify-between">
				<RNewTabs tabs={usersTabs} setActiveTab={setActiveTab} value={activeTab} />
				{activeTab == Terms.BasicInfo && (
					<Button
						className={`flex gap-1 ${!basicInfoEditMode ? "border-themePrimary" : ""}`}
						variant={!basicInfoEditMode ? "outline" : "default"}
						onClick={handleEditSaveClicked}
						disabled={updateUserAccountMutation.isLoading}
					>
						{" "}
						{updateUserAccountMutation.isLoading ? (
							<i className={iconsFa6.spinner} />
						) : !basicInfoEditMode ? (
							<i className={iconsFa6.edit} />
						) : (
							""
						)}
						{basicInfoEditMode ? tr("save") : tr("edit")}
					</Button>
				)}
				{activeTab == Terms.Roles && (
					<Button
						className={`flex gap-1 ${!rolesEditMode ? "border-themePrimary" : ""}`}
						variant={!rolesEditMode ? "outline" : "default"}
						onClick={handleEditSaveClicked}
						disabled={
							updateUserAccountMutation.isLoading ||
							assignRolesToUserInSpecificUserTypeMutation.isLoading ||
							removeRolesFromUserInSpecificUserTypeMutation.isLoading
						}
					>
						{updateUserAccountMutation.isLoading ||
						assignRolesToUserInSpecificUserTypeMutation.isLoading ||
						removeRolesFromUserInSpecificUserTypeMutation.isLoading ? (
							<i className={iconsFa6.spinner} />
						) : !rolesEditMode ? (
							<i className={iconsFa6.edit} />
						) : (
							""
						)}
						{rolesEditMode ? tr("save") : tr("edit")}
					</Button>
				)}
				{activeTab == Terms.Permissions && (
					<RButton
						text={permissionsEditMode ? tr("save") : tr("edit")}
						faicon={permissionsEditMode ? "" : iconsFa6.edit}
						color="primary"
						className="m-0"
						outline={permissionsEditMode ? false : true}
						onClick={handleEditSaveClicked}
						loading={
							updateUserAccountMutation.isLoading ||
							assignPermissionsToUserInSpecificUserTypeMutation.isLoading ||
							removePermissionsFromUserInSpecificUserTypeMutation.isLoading
						}
						disabled={
							updateUserAccountMutation.isLoading ||
							assignPermissionsToUserInSpecificUserTypeMutation.isLoading ||
							removePermissionsFromUserInSpecificUserTypeMutation.isLoading
						}
					/>
				)}
			</RFlex>
			{activeTab == Terms.BasicInfo &&
				(isLoadingUserTypes ? (
					<RLoader />
				) : (
					<RUserBasicInfo
						values={values}
						touched={touched}
						errors={errors}
						handleChange={handleChange}
						handleBlur={handleBlur}
						userTypesCount={userTypesCount}
						handleAddingUserType={handleAddingUserType}
						userTypes={userTypes?.data?.data}
						handleSelectOption={handleSelectOption}
						handleSwitchChange={handleSwitchChange}
						handleAccountActiveChanged={handleAccountActiveChanged}
						accountActiveStatus={accountActiveStatus}
						gender={gender}
						setGender={setGender}
						generateEmailFromUsername={generateEmailFromUsername}
						suggestPassword={suggestPassword}
						setUploadId={setUploadId}
						handleSelectOrganization={handleSelectOrganization}
						currentOrganization={currentOrganization}
						pickedOrganization={pickedOrganization}
						hashId={hashId}
						scrollToTop={scrollToTop}
						editable={basicInfoEditMode}
						groups={groups?.data?.data}
						handleSelectGroup={handleSelectGroup}
						pickedGroup={pickedGroup}
						view
					/>
				))}
			{activeTab == Terms.Roles &&
				(isLoadingRoles ? (
					<RLoader />
				) : (
					<RRoles
						roles={getSortedRoles(roles, pickedUserType)}
						handleSelectRole={handleSelectRole}
						// handleSelectAllRoles={handleSelectAllRoles}
						frontSearchTerm={frontRoleSearchTerm}
						setFrontSearchTerm={setFrontRoleSearchTerm}
						setBackSearchTerm={setBackRoleSearchTerm}
						selectedRoles={selectedRoles[pickedUserType.id]}
						canEdit={rolesEditMode}
						inUser
						currentUserTypes={currentUserTypes}
						pickedUserType={pickedUserType}
						handleChangePickedUserType={handleChangePickedUserType}
					/>
				))}
			{activeTab == Terms.Permissions &&
				(isLoadingPermissions ? (
					<RLoader />
				) : (
					<RPermissons
						permissions={getSortedPermissions(permissions, pickedUserType)}
						setBackSearchTerm={setBackendPermissoinSearchTerm}
						selectOneFn={handleSelectPermission}
						selectAllFn={handleSelectAllPermissions}
						selectedPermissions={selectedPermissions[pickedUserType.id]}
						frontSearchTerm={frontPermissionSearchTerm}
						setFrontSearchTerm={setFrontPermissionSearchTerm}
						isFetching={isFetchingPermissions}
						canEdit={permissionsEditMode}
						inUser
						currentUserTypes={currentUserTypes}
						pickedUserType={pickedUserType}
						handleChangePickedUserType={handleChangePickedUserType}
					/>
				))}
		</RFlex>
	);
};

export default GViewUser;
