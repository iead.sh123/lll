import { Label } from "ShadCnComponents/ui/label";
import { RadioGroupItem } from "ShadCnComponents/ui/radio-group";
import { RadioGroup } from "ShadCnComponents/ui/radio-group";
import { Switch } from "ShadCnComponents/ui/switch";
import RButton from "components/Global/RComs/RButton";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RSelect from "components/Global/RComs/RSelect";
import tr from "components/Global/RComs/RTranslator";
import React from "react";
import { Input } from "ShadCnComponents/ui/input";
import iconsFa6 from "variables/iconsFa6";
import { Button } from "ShadCnComponents/ui/button";

const RUserBasicInfo = ({
	values,
	touched,
	errors,
	handleChange,
	handleBlur,
	userTypes,
	userTypesCount,
	handleAddingUserType,
	handleSelectOption,
	handleSwitchChange,
	accountActiveStatus,
	handleAccountActiveChanged,
	setGender,
	generateEmailFromUsername,
	suggestPassword,
	setUploadId,
	hashId,
	editable = true,
	view = false,
	gender,
	groups = [],
	handleSelectGroup,
	pickedGroup,
}) => {
	const getAvailableOptions = () => {
		const allOptions = userTypes?.map((type, index) => ({ label: type?.type.name, value: type?.id }));
		const pickedOptionsIds = userTypesCount?.map((type) => type?.pickedOption?.value);
		const avilableOptions = allOptions?.filter((type) => !pickedOptionsIds?.includes(type?.value));
		const finalOptions = [{ label: "no_choosen_User_Type", value: null }].concat(avilableOptions);
		return finalOptions;
	};
	return (
		<RFlex className="flex-column">
			<span className="p-0 m-0">{tr("Profile_Picture")}</span>
			<RFileSuite
				parentCallback={(e) => {
					setUploadId(e[0]);
				}}
				roundWidth={{ width: "70px", height: "70px", type: "default" }}
				singleFile={true}
				fileType={["image/*"]}
				removeButton={false}
				value={[{ hash_id: hashId ? hashId : null }]}
				uploadName="upload"
				showReplace={false}
				showDelete={true}
				showFileList={false}
				showFileAdd={editable ? true : false}
				setSpecificAttachment={() => {}}
				theme="round"
				binary={true}
			/>
			<RFlex id="username" className="">
				<label className="p-0 m-0 w-[130px]">{tr("User_full_name")}</label>
				{editable ? (
					<RFlex className="flex-col" styleProps={{ gap: "5px" }}>
						<Input
							name="username"
							onChange={handleChange}
							onBlur={handleBlur}
							value={values.username}
							className={`${touched.username && errors.username ? "input__error" : ""} w-[240px]`}
						/>
						{touched.username && errors.username && <p className="text-themeDanger text-[11px] ">{errors.username}</p>}
					</RFlex>
				) : (
					<span className="w-[130px]">{values.username}</span>
				)}
			</RFlex>
			<RFlex id="first name" className="">
				<label className="p-0 m-0 w-[130px]">{tr("First_name")}</label>
				{editable ? (
					<RFlex className="flex-col" styleProps={{ gap: "5px" }}>
						<Input
							name="firstname"
							onChange={handleChange}
							onBlur={handleBlur}
							value={values.firstname}
							className={`${touched.firstname && errors.firstname ? "input__error" : ""} w-[240px]`}
						/>
						{touched.firstname && errors.firstname && <p className="text-themeDanger text-[11px] ">{errors.firstname}</p>}
					</RFlex>
				) : (
					<span className="w-[130px]">{values.firstname}</span>
				)}
			</RFlex>
			<RFlex id="last name" className="">
				<label className="p-0 m-0 w-[130px]">{tr("last_name")}</label>

				{editable ? (
					<RFlex className="flex-col" styleProps={{ gap: "5px" }}>
						<Input
							name="lastname"
							onChange={handleChange}
							onBlur={handleBlur}
							value={values.lastname}
							className={`${touched.lastname && errors.lastname ? "input__error" : ""} w-[240px]`}
						/>
						{touched.lastname && errors.lastname && <p className="text-themeDanger text-[11px] ">{errors.lastname}</p>}
					</RFlex>
				) : (
					<span className="w-[130px]">{values.lastname}</span>
				)}
			</RFlex>
			<RFlex id="User Types" className="flex-column gap-0">
				<RFlex id="no button" className="flex-col">
					{userTypesCount?.map((stateType) => {
						return (
							<RFlex>
								<label className="w-[130px]">{tr("User_type")}</label>
								<RFlex className="items-center gap-1">
									{editable ? (
										<div className="w-[240px]">
											<RSelect
												// isDisabled={true}
												option={getAvailableOptions()}
												closeMenuOnSelect={true}
												value={stateType?.pickedOption}
												placeholder={tr`choose_user_type`}
												onChange={(e) => {
													handleSelectOption(e, stateType);
												}}
												required
											/>
										</div>
									) : (
										<span className="w-[120px]">{stateType?.pickedOption?.label}</span>
									)}
									{editable ? (
										<RFlex className="items-center">
											<Label className="cursor-pointer" htmlFor={`user-type-mode${stateType.id}`}>
												{tr("active")}
											</Label>
											<Switch
												checked={stateType.activeStatus}
												onCheckedChange={() => {
													handleSwitchChange(stateType);
												}}
												disabled={stateType.disabledStatus}
												className="data-[state=checked]:bg-themeSuccess"
												id={`user-type-mode${stateType.id}`}
											/>
										</RFlex>
									) : (
										<span className={`${stateType?.activeStatus ? "text-themeSuccess" : "text-themeDanger"}`}>
											{stateType?.activeStatus ? "Active" : "Inactive"}
										</span>
									)}
								</RFlex>
							</RFlex>
						);
					})}
				</RFlex>
				{editable && (
					<Button
						faicon={iconsFa6.plus}
						className="flex gap-1 p-0 w-fit text-themePrimary hover:text-themePrimary "
						variant="ghost"
						onClick={handleAddingUserType}
					>
						<i className={iconsFa6.plus} />
						{tr("Add_another_user_type")}
					</Button>
				)}
			</RFlex>
			<RFlex id="user account switch" className="items-center">
				<label className="w-[130px]">{tr("User_account")}</label>
				<RFlex className="items-center">
					<Label
						className={`${editable ? "cursor-pointe	r" : "cursor-default"} ${
							editable ? "" : accountActiveStatus ? "text-themeSuccess" : "text-themeDanger"
						}`}
						htmlFor="user-account-mode"
					>
						{tr("active")}
					</Label>
					{editable && (
						<Switch
							checked={accountActiveStatus}
							onCheckedChange={handleAccountActiveChanged}
							className="data-[state=checked]:bg-themeSuccess"
							id="user-account-mode"
						/>
					)}
				</RFlex>
			</RFlex>
			<RFlex id="userid" className="">
				<label className="p-0 m-0 w-[130px]">{tr("user_id")}</label>
				{!view ? (
					<RFlex className="flex-col" styleProps={{ gap: "5px" }}>
						<Input
							name="userid"
							onChange={handleChange}
							onBlur={handleBlur}
							value={values.userid}
							className={`${touched.userid && errors.userid ? "input__error" : ""} w-[240px]`}
						/>
						{touched.userid && errors.userid && <p className="text-themeDanger text-[11px] ">{errors.userid}</p>}
					</RFlex>
				) : (
					<span className="w-[130px]">{values.userid}</span>
				)}
			</RFlex>

			<RFlex id="groups">
				<label className="w-[130px]">{tr("group")}</label>
				{editable ? (
					<RFlex className="items-center gap-1">
						<div className="w-[240px]">
							<RSelect
								option={[{ label: "choose_a_group", value: null }].concat(
									groups?.map((group, index) => ({ label: group.name, value: group.id }))
								)}
								closeMenuOnSelect={true}
								value={pickedGroup}
								placeholder={tr`choose_a_group`}
								onChange={(e) => {
									handleSelectGroup(e);
								}}
								required
							/>
						</div>
					</RFlex>
				) : (
					<span className="w-[130px]">{pickedGroup?.label ?? "-"}</span>
				)}
			</RFlex>
			<RFlex id="gender">
				<label className="w-[130px]">{tr("gender")}</label>
				<RadioGroup
					value={gender}
					className="flex flex-row"
					onValueChange={(e) => {
						editable ? setGender(e) : "";
					}}
				>
					<RFlex className="gap-2 items-center">
						<RadioGroupItem
							value="male"
							id="male"
							iconClassName="h-2.5 w-2.5 bg-themePrimary rounded-full"
							className="border-[1px] border-themePrimary"
						/>
						<Label htmlFor="male">{tr("Male")}</Label>
					</RFlex>
					<RFlex className="gap-2 items-center">
						<RadioGroupItem
							value="female"
							id="female"
							iconClassName="h-2.5 w-2.5 bg-themePrimary rounded-full"
							className="border-[1px] border-themePrimary "
						/>
						<Label htmlFor="female">{tr("Female")}</Label>
					</RFlex>
				</RadioGroup>
			</RFlex>
			<RFlex id="email" className="gap-1">
				<label className="p-0 m-0 w-[130px]">{tr("Email")}</label>
				{!view ? (
					<RFlex className="flex-col" styleProps={{ gap: "5px" }}>
						<Input
							type="email"
							name="email"
							onChange={handleChange}
							onBlur={handleBlur}
							value={values.email}
							className={`${touched.email && errors.email ? "input__error" : ""} w-[240px]`}
						/>
						{touched.email && errors.email && <p className="text-themeDanger text-[11px] ">{errors.email}</p>}
					</RFlex>
				) : (
					<span className="w-[130px]">{values.email}</span>
				)}
				{!view && (
					<RButton
						text={tr("generate_email_from_username")}
						color="link"
						className="m-0 p-0 text-primary"
						faicon={iconsFa6.rotateArrow}
						onClick={generateEmailFromUsername}
					/>
				)}
			</RFlex>
			{!view && (
				<RFlex id="password" className="gap-1">
					<label className="p-0 m-0 w-[130px]">{tr("password")}</label>
					<RFlex className="flex-col" styleProps={{ gap: "5px" }}>
						<Input
							name="password"
							onChange={handleChange}
							onBlur={handleBlur}
							value={values.password}
							className={`${touched.password && errors.password ? "input__error" : ""} w-[240px]`}
						/>
						{touched.password && errors.password && <p className="text-themeDanger text-[11px] ">{errors.password}</p>}
					</RFlex>
					<RButton text={tr("suggest_password")} color="link" className="m-0 p-0 text-primary" onClick={suggestPassword} />
				</RFlex>
			)}
			<RFlex id="Address" className="gap-1">
				<label className="p-0 m-0 w-[130px]">{tr("Address")}</label>
				{editable ? (
					<RFlex className="flex-col" styleProps={{ gap: "5px" }}>
						<Input
							name="address"
							onChange={handleChange}
							onBlur={handleBlur}
							value={values.address}
							className={`${touched.address && errors.address ? "input__error" : ""} w-[240px]`}
						/>
						{touched.address && errors.address && <p className="text-themeDanger text-[11px] ">{errors.address}</p>}
					</RFlex>
				) : (
					<span className="w-[130px]">{values.address ?? "-"}</span>
				)}
			</RFlex>
			<RFlex id="Phone" className="gap-1">
				<label className="p-0 m-0 w-[130px]">{tr("phone")}</label>
				{editable ? (
					<RFlex className="flex-col" styleProps={{ gap: "5px" }}>
						<Input
							name="phone"
							onChange={handleChange}
							onBlur={handleBlur}
							value={values.phone}
							className={`${touched.phone && errors.phone ? "input__error" : ""} w-[240px]`}
						/>
						{touched.phone && errors.phone && <p className="text-themeDanger text-[11px] ">{errors.phone}</p>}
					</RFlex>
				) : (
					<span className="w-[130px]">{values.phone ?? "-"}</span>
				)}
			</RFlex>
			<RFlex id="Emergency" className="gap-1">
				<label className="p-0 m-0 w-[130px]">{tr("emergency_number")}</label>
				{editable ? (
					<RFlex className="flex-col" styleProps={{ gap: "5px" }}>
						<Input
							name="emergency"
							onChange={handleChange}
							onBlur={handleBlur}
							value={values.emergency}
							className={`${touched.emergency && errors.emergency ? "input__error" : ""} w-[240px]`}
						/>
						{touched.emergency && errors.emergency && <p className="text-themeDanger text-[11px] ">{errors.emergency}</p>}
					</RFlex>
				) : (
					<span className="w-[130px]">{values.emergency ?? "-"}</span>
				)}
			</RFlex>
			<span className="font-bold">{tr("Extra_Info")}</span>
			<RFlex id="level" className="gap-1">
				<span className="w-[130px]">{tr("Selected_Level_From_The_User_Type")}</span>
				<div className="w-[240px]">
					<RSelect
						option={[{ label: "choose_level", value: null }].concat(
							userTypes?.map((type, index) => ({ label: type.type.name, value: type.id }))
						)}
						closeMenuOnSelect={true}
						// value={stateType?.pickedOption}
						placeholder={tr`choose_level`}
						onChange={(e) => {
							// handleSelectOption(e, stateType);
						}}
						required
					/>
				</div>
			</RFlex>

			<div className="m-20"></div>
		</RFlex>
	);
};
export default RUserBasicInfo;
