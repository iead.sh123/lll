import React from 'react'
import RFlex from 'components/Global/RComs/RFlex/RFlex'
import SchoolLogo from '../../assets/img/SchoolLogo.svg'
import RTextIcon from 'components/Global/RComs/RTextIcon/RTextIcon'
import { Services } from 'engine/services'
import RButton from 'components/Global/RComs/RButton'
import GSchoolCreating from './GSchoolCreating'
import AppModal from 'components/Global/ModalCustomize/AppModal'
import tr from 'components/Global/RComs/RTranslator'
import DefaultSchoolLogo from 'assets/img/new/school_default logo.svg'
const SchoolInfo = ({ info }) => {
    const [modalOpen, setModalOpen] = React.useState(false)
    const closeModal = () => {
        setModalOpen(false)
    }
    return (
        <>
            <RFlex id="Info Container" styleProps={{ gap: "33px", flexDirection: 'row', justifyContent: 'space-between' }}>
                <RFlex styleProps={{ gap: '15px' }}>
                    <img src={info.image?Services.storage.file + info.image : DefaultSchoolLogo} alt='School Logo' width={100} height={100} style={{ objectFit: 'cover', borderRadius: '100%' }} />
                    <RFlex className="flex-column" styleProps={{ gap: '5px' }}>
                        {info.name}
                        <RTextIcon
                            icon="fa-solid fa-location-dot"
                            iconStyle={{ width: '14px', height: '14px', color: "#585858", marginLeft: '1px' }}
                            text={info.address ? info.address : "Address"}
                            flexStyle={{ gap: '5px' }} />
                        <RTextIcon
                            icon="fa-solid fa-envelope"
                            iconStyle={{ width: '14px', height: '14px', color: "#585858" }}
                            text={info.email ? info.email : "Contact Information"}
                            flexStyle={{ gap: '5px' }} />
                        <RTextIcon
                            icon="fa-brands fa-facebook"
                            iconStyle={{ width: '14px', height: '14px', color: "#585858" }}
                            text={info.url ? info.url : "FacebookPage"}
                            flexStyle={{ gap: '5px' }} />
                    </RFlex>
                </RFlex>
                <RButton text={tr("Edit")} style={{ height: 'fit-content', width: 'fit-content', margin: '0px' }} faicon="fa-solid fa-pen" outline disabled={info.gettingLoading || info.editingLoading} color="primary" onClick={() => { setModalOpen(!modalOpen) }} />

            </RFlex>
            <AppModal
                headerSort={<GSchoolCreating isModal={true} closeModal={closeModal} />}
                show={modalOpen}
                parentHandleClose={closeModal}
                style={{ 'min-width': '530px' }}
                modalBodyClass="p-0" />
        </>

    )
}

export default SchoolInfo