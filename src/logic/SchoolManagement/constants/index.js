export const types = {
    School: 'school',
    Stage: 'stage',
    GradeLevel: 'grad_level',
    Curricula: 'curricula'
}

export const dataTypes = {
    Principals: 'Principals',
    SeniorTeacher: 'SeniorTeachers',
    Students: 'Students',
}

export const scaleTypes={
    Letter:'letter',
    Min:'min',
    Max:'max',
    GPA:'GPA'
}
export const assessmentsTypes={
    Name:'name',
    Timestamp:'timestamp'
}