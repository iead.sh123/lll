import React, { useState, useEffect, useRef } from "react";
import { Collapse, Input } from "reactstrap";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RTextIcon from "components/Global/RComs/RTextIcon/RTextIcon";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import * as colors from "config/constants";
import styles from "./SchoolLister.module.scss";
import { types } from "./constants";
import GCurriculaCourse from "./GCurriculaCourse";
import Loader from "utils/Loader";
import RDropdownIcon from "components/Global/RComs/RDropdownIcon/RDropdownIcon";
import iconsFa6 from "variables/iconsFa6";
import { primaryColor, greyColor, boldGreyColor } from "config/constants";
import tr from "components/Global/RComs/RTranslator";
const RCollapsedDiv = ({
	handleInputChange,
	saveClicked,
	handleInputDown,
	collapseChilds,
	number,
	currentType,
	childType,
	inputValue,
	inputPlaceHolder,
	saved,
	handleDivClicked,
	handleEditFunctionality,
	handleDeleteFunctionality,
	handleSaveButton,
	generateChildComponent,
	buttonText,
	index,
	isOpen = false,
	setOpenCurriculaCategories,
	id,
	order,
	addClicked,
	openLevelId,
	isItActive,
	openCourseCataloug,
}) => {
	const inputRef = useRef(null);
	useEffect(() => {
		if (inputRef.current) {
			inputRef.current.focus();
		}
		return () => {};
	}, [saved]);
	// if (id != openLevelId && openLevelId != null) {
	//     inputRef.current ? inputRef.current.blur() : ""
	// }
	let inputIsNotValidate = false;
	let AddButtonDisable = false;
	inputIsNotValidate = saveClicked && inputValue == "" ? true : false;
	AddButtonDisable = id <= 0 && addClicked ? true : false;
	const leftMarginDiv = currentType === types.Stage ? "0px" : currentType === types.GradeLevel ? "20px" : "40px";
	const leftMarginAddingButton = currentType === types.Stage ? "20px" : currentType === types.GradeLevel ? "40px" : "60px";
	let openIndexType = "";

	if (currentType == types.Stage) {
		openIndexType = "gradeLevelOpenIndex";
	} else if (currentType === types.Curricula) {
		openIndexType = "curricluaOpenIndex";
	}
	const [isHovering, setIsHovering] = useState(false);
	return (
		<RFlex id="InEducation" className="flex-column" styleProps={{ width: "100%" }}>
			<RFlex
				id="padding div"
				// className={currentType === types.Curricula ? "flex-column" : "flex-row"}
				styleProps={{
					gap: "0px",
					height: "42px",
					padding: !isHovering ? "7px 20px" : "0px 20px",
					cursor: "pointer",
					boxShadow: "0px 0px 8px 0px #E4E4E4",
					marginLeft: leftMarginDiv,
				}}
				className={isItActive ? styles.ActiveCollapse : ""}
				onClick={() => {
					inputRef.current ? inputRef.current.blur() : "";
					handleDivClicked(id);
				}}
			>
				<RFlex
					id="Spacebetween div"
					styleProps={{ minHeight: isHovering ? "38px" : "100%", width: "100%" }}
					className="justify-content-between align-items-center"
				>
					{/* between input and save button */}
					<RFlex id="input and save div" className=" align-items-center" styleProps={{ gap: "18px", height: "100%", width: "320px" }}>
						{/* just input field or (flex between value and edit and delete icon) */}
						<RFlex
							id="input or value and icons"
							styleProps={{ height: "100%" }}
							className=" align-items-center"
							onMouseEnter={() => {
								setIsHovering(true);
							}}
							onMouseLeave={() => {
								setIsHovering(false);
							}}
						>
							{!saved ? (
								<RFlex className="flex-column align-items-start">
									<div style={{ position: "relative" }}>
										{inputIsNotValidate || AddButtonDisable ? (
											<i className={`fas fa-exclamation-circle fa-md ${styles.ValidationIcon}`} style={{ color: colors.dangerColor }} />
										) : (
											""
										)}
										<Input
											style={{ padding: "7px 12px", width: "200px" }}
											onChange={handleInputChange}
											onClick={(event) => {
												event.stopPropagation();
											}}
											value={inputValue}
											placeholder={inputPlaceHolder}
											onBlur={(event) => handleSaveButton()}
											onKeyDown={handleInputDown}
											className={inputIsNotValidate || AddButtonDisable ? "input__error" : ""}
											// ref={inputRef}
											innerRef={inputRef}
										/>
									</div>
								</RFlex>
							) : (
								<>
									<p
										style={{
											display: "flex",
											textAlign: "center",
											flexWrap: "wrap",
											alignContent: "center",
											padding: "7px 10px",
											margin: "0px",
											minWidth: "200px",
											minHeight: isHovering ? "38px" : "100%",
											border: isHovering ? "1px solid " + colors.strokeColor : "1px solid white",
											borderRadius: "2px",
										}}
										className={`${styles.AnimateBorder} ${isItActive ? styles.ActiveText : ""}`}
										onClick={(event) => {
											event.stopPropagation();
											handleEditFunctionality(id, order);
										}}
									>
										<span>{inputValue}</span>
									</p>

									{isHovering ? (
										<>
											<i
												className="fa-regular fa-trash-can text-danger"
												onClick={(event) => {
													event.stopPropagation();
													handleDeleteFunctionality(id, order);
												}}
											/>
											{/* <i className='fa-solid fa-pen' onClick={(event) => { event.stopPropagation(); handleEditFunctionality() }} /> */}
										</>
									) : (
										""
									)}
								</>
							)}
						</RFlex>
						{/* {!saved &&
                            <button className='text-primary'
                                disabled={inputIsNotValidate}
                                style={{ padding: '0px', margin: '0px', cursor: "pointer", border: 'none', backgroundColor: 'transparent' }}
                                onClick={(event) =>{ event.stopPropagation();handleSaveButton()}}
                            ><span className='p-0 m-0'>Save</span></button>} */}
					</RFlex>
					{number > 0 ? (
						<span style={{ padding: "0px", margin: "0px", color: colors.greyColor, fontSize: "12px" }}>
							{number} {childType}
						</span>
					) : (
						<span style={{ padding: "0px", margin: "0px", color: colors.greyColor, fontSize: "12px" }}>no {childType} Yet</span>
					)}
					{
						<i
							className={`${!isOpen ? "fa-solid fa-angle-right" : "fa-solid fa-angle-down"} ${isItActive ? "text-primary" : ""}`}
							style={{ width: "10px" }}
						/>
					}
				</RFlex>
			</RFlex>
			{currentType === types.Curricula && (
				<Collapse isOpen={isOpen}>
					{collapseChilds.loading ? (
						<Loader />
					) : (
						<RFlex className="flex-column">
							{collapseChilds.ids.length > 0 && (
								<RFlex className="flex-column">
									{collapseChilds.ids.map((key, id) => {
										return <GCurriculaCourse course={collapseChilds[key]} />;
									})}
								</RFlex>
							)}
							<span
								style={{ margin: "0px 0px 5px 60px", padding: "0px 15px", cursor: "pointer" }}
								className="text-primary"
								onClick={openCourseCataloug}
							>
								Pick From Course catalogue
							</span>
						</RFlex>
					)}
				</Collapse>
			)}
			{currentType != types.Curricula && (
				<Collapse isOpen={isOpen}>
					<RFlex id="collapse flex " className="flex-column" styleProps={{ gap: "5px" }}>
						<Droppable droppableId={`level:${currentType},id:${id}`} type={`level:${currentType},type:${id}`}>
							{(provided, snapshot) => (
								<div ref={provided.innerRef} id="under Collapse Div" style={{ display: "flex", flexDirection: "column", gap: "5px" }}>
									{collapseChilds.map((child, index) => (
										<Draggable
											key={`item:${currentType},key:${child.id}`}
											draggableId={`item:${currentType},id:${String(child.id)}`}
											index={index}
										>
											{(provided, snapshot) => (
												<div>
													<div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
														{child.component}
													</div>
													{provided.placeholder}
												</div>
											)}
										</Draggable>
									))}
									{provided.placeholder}
								</div>
							)}
						</Droppable>

						{collapseChilds.length === 0 ? (
							<RFlex styleProps={{ marginLeft: leftMarginAddingButton }}>
								<span style={{ padding: "0px", margin: "0px", color: "color: #5e5f63" }}>
									{currentType === types.Stage ? "No Grage Level Yet" : "No Curriculum Yet"}
								</span>
								{types.GradeLevel == currentType ? (
									<RDropdownIcon
										dropdownClassname="btn-magnify"
										menuClassname={styles.CurriculaDropDown}
										dropdownStyle={{ width: "fit-content" }}
										actions={[
											{ label: "Create new", action: generateChildComponent },
											{ label: "Pick a category ", action: () => setOpenCurriculaCategories(true) },
										]}
										component={
											<RTextIcon
												icon={iconsFa6.chevronDown}
												text={tr`add_curriculum`}
												iconOnRight={true}
												flexStyle={{ color: primaryColor, cursor: "pointer", width: "fit-content" }}
											/>
										}
										// menuClassname={styles.SelectDropDown}
										// iconContainerStyle={{ border: '1px solid' + colors.successColor, borderRadius: '100%' }}
									/>
								) : (
									<RTextIcon
										flexStyle={{ cursor: "pointer", gap: "5px" }}
										text={buttonText}
										onlyText
										textStyle={{ color: "#668ad7" }}
										onClick={generateChildComponent}
									/>
								)}
							</RFlex>
						) : types.GradeLevel == currentType ? (
							<RDropdownIcon
								dropdownClassname="btn-magnify"
								menuClassname={styles.CurriculaDropDown}
								dropdownStyle={{ width: "fit-content", marginLeft: leftMarginAddingButton }}
								actions={[
									{ label: "Create new", action: generateChildComponent },
									{ label: "Pick A category ", action: () => setOpenCurriculaCategories(true) },
								]}
								component={
									<RTextIcon
										icon={iconsFa6.chevronDown}
										text={tr`add_curriculum`}
										iconOnRight={true}
										flexStyle={{ color: primaryColor, cursor: "pointer", width: "fit-content" }}
									/>
								}
								// menuClassname={styles.SelectDropDown}
								// iconContainerStyle={{ border: '1px solid' + colors.successColor, borderRadius: '100%' }}
							/>
						) : (
							<RTextIcon
								flexStyle={{ cursor: "pointer", gap: "5px", marginLeft: leftMarginAddingButton, width: "fit-content" }}
								text={buttonText}
								onlyText
								textStyle={{ color: "#668ad7" }}
								onClick={generateChildComponent}
							/>
						)}
					</RFlex>
				</Collapse>
			)}
		</RFlex>
	);
};

export default RCollapsedDiv;
