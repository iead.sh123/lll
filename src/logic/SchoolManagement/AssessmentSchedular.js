import RFlex from 'components/Global/RComs/RFlex/RFlex'
import React, { useState } from 'react'
import * as actions from 'store/actions/global/schoolManagement.action'
import { useDispatch, useSelector } from 'react-redux'
import RHoverComponent from 'components/Global/RComs/RHoverComponent/RhoverComponent'
import { Input } from 'reactstrap'
import RHoverInput from 'components/Global/RComs/RHoverInput/RHoverInput'
import { assessmentsTypes } from './constants'
import { SET_ASSESSMENT_SAVED, SET_ASSESSMENT_TOUCHED, CHANGE_ASSESSMENT_INPUT, SET_ASSESSMENT_TIMESTAMP } from 'store/actions/global/globalTypes'
import tr from 'components/Global/RComs/RTranslator'
import iconsFa6 from 'variables/iconsFa6'
import RButton from 'components/Global/RComs/RButton'
import AppModal from 'components/Global/ModalCustomize/AppModal'
import GCalender from 'logic/calender/GCalender'
import Loader from 'utils/Loader'
import { SAVE_ASSESSMENT_INPUT } from 'store/actions/global/globalTypes'
import { UNSAVE_ASSESSMENT_INPUT } from 'store/actions/global/globalTypes'
const AssessmentSchedular = ({ assessments }) => {
    const isAssessmentValidate = (name, timestamp) => {
        if (name == "" || !timestamp) {
            return false
        }
        return true

    }
    const dispatch = useDispatch()
    const [enterPressed, setEnterPressed] = useState(false)
    const [openCalendarModal, setOpenCalendarModal] = useState(false)
    const { openGradLevelId } = useSelector((state) => state.schoolManagementRed.openGradLevelId)
    const handleEditFunctionality = (event, assessment) => {
        setEnterPressed(false)
        dispatch({ type: UNSAVE_ASSESSMENT_INPUT, payload: { id: assessment.id } })
    }
    const handleCloseModal = () => {
        setOpenCalendarModal(false)
    }
    let disableCreateEvents = false
    assessments.ids.map((key) => {
        if (key <= 0)
            disableCreateEvents = true
    })
    const canIAdd = () => {
        let canAdd = true
        assessments.ids.forEach(key => {
            if (!isAssessmentValidate(assessments[key].name, assessments[key].timestamp)) {
                canAdd = false
            }
        })
        return canAdd
    }
    const handleNewAssessment = () => {
        if (canIAdd()) {
            setEnterPressed(false)
            dispatch(actions.addNewAssessment())
        }
        else {
            assessments.ids.forEach(key => {
                dispatch({ type: SET_ASSESSMENT_TOUCHED, payload: { id: assessments[key].id, type: assessmentsTypes.Name } })
                dispatch({
                    type: SET_ASSESSMENT_TOUCHED, payload: { id: assessments[key].id, type: assessmentsTypes.Timestamp }
                })
            })
            console.log("You Can't Add")
        }
    }
    const handleInputDown = async (event, assessment, { type }) => {
        if (event.key == 'Enter') {
            if (enterPressed) {
                return
            }
            setEnterPressed(true)

            dispatch({ type: SET_ASSESSMENT_TOUCHED, payload: { id: assessment.id, type } })
            let success = false
            if (assessment.name == "")
                return
            if (assessment.id > 0) {
                success = await dispatch(actions.addOrEditAssessment({ id: assessment.id, name: assessment.name, timestamp: assessment.timestamp }))
            }
            else {
                if (isAssessmentValidate(assessment.name, assessment.timestamp)) {
                    success = await dispatch(actions.addOrEditAssessment({ id: assessment.id, name: assessment.name, timestamp: assessment.timestamp }))

                }
                else {
                    dispatch({ type: SAVE_ASSESSMENT_INPUT, payload: { id: assessment.id } })

                }
            }
            if (success) {
                dispatch({ type: SAVE_ASSESSMENT_INPUT, payload: { id: assessment.id } })

            }
        }
    }
    const handleOnBlur = async (event, assessment, { type }) => {
        if (enterPressed) {
            return
        }
        dispatch({ type: SET_ASSESSMENT_TOUCHED, payload: { id: assessment.id, type } })
        let success = false
        if (assessment.name == "")
            return

        if (assessment.id > 0) {
            success = await dispatch(actions.addOrEditAssessment({ id: assessment.id, name: assessment.name, timestamp: assessment.timestamp }))

        }
        else {
            if (isAssessmentValidate(assessment.name, assessment.timestamp)) {
                success = await dispatch(actions.addOrEditAssessment({ id: assessment.id, name: assessment.name, timestamp: assessment.timestamp }))
            }
            else {
                dispatch({ type: SAVE_ASSESSMENT_INPUT, payload: { id: assessment.id } })
            }

        }
        if (success) {
            dispatch({ type: SAVE_ASSESSMENT_INPUT, payload: { id: assessment.id } })

        }
    }
    const handleInputChange = (event, assessment, { type }) => {
        dispatch({ type: CHANGE_ASSESSMENT_INPUT, payload: { id: assessment.id, value: event.target.value } })
    }
    const handleDateChange = async (assessment, value) => {
        dispatch({ type: SET_ASSESSMENT_TIMESTAMP, payload: { id: assessment.id, newValue: value } })

    }
    const handleDateBlur = async (assessment, { type }) => {
        dispatch({ type: SET_ASSESSMENT_TOUCHED, payload: { id: assessment.id, type } })
        let success = false
        if (assessment.id > 0) {
            success = await dispatch(actions.addOrEditAssessment({ id: assessment.id, name: assessment.name, timestamp: assessment.timestamp }))
        }
        else {
            if (isAssessmentValidate(assessment.name, assessment.timestamp)) {
                success = await dispatch(actions.addOrEditAssessment({ id: assessment.id, name: assessment.name, timestamp: assessment.timestamp }))
            }
        }
    }
    const deleteAssessment = (assessment) => {
        console.log("Delete")
        dispatch(actions.deleteAssessment(assessment))
    }

    return (
        assessments.loading ? <Loader /> :
            assessments.ids.length <= 0 ?
                <RFlex className='flex-column' styleProps={{ gap: "5px" }}>
                    <RFlex>
                        <RFlex className="align-items-center">

                            <span className='p-0 m-0 font-weight-bold'>Assessment Schedular</span>
                            <i
                                className='fas fa-calendar-alt p-0 m-0 text-primary'
                                style={{ cursor: 'pointer' }}
                                onClick={() => setOpenCalendarModal(true)} />
                        </RFlex>
                        <AppModal
                            show={openCalendarModal}
                            size={"lg"}
                            header={true}
                            headerSort={<GCalender />}
                            parentHandleClose={handleCloseModal} />
                    </RFlex>
                    <RButton text="New Schedular"
                        faicon='fa-solid fa-plus'
                        color="link"
                        className="text-primary m-0 p-0"
                        style={{ width: 'fit-content' }}
                        onClick={handleNewAssessment} />
                </RFlex> :

                <RFlex className="flex-column" styleProps={{ width: '100%', gap: '5px' }}>
                    <RFlex className="justify-content-between align-items-center">
                        <RFlex className="align-items-center">

                            <span className='p-0 m-0 font-weight-bold'>Assessment Schedular</span>
                            <i
                                className='fas fa-calendar-alt p-0 m-0 text-primary'
                                style={{ cursor: 'pointer' }}
                                onClick={() => setOpenCalendarModal(true)} />
                            < RButton
                                text="AddEvents"
                                color="link"
                                disabled={disableCreateEvents}
                                className="text-primary m-0 p-0"
                                onClick={() => dispatch(actions.createAssessmentsEvents())} />
                        </RFlex>
                        <AppModal
                            show={openCalendarModal}
                            size={"lg"}
                            header={true}
                            headerSort={<GCalender />}
                            parentHandleClose={handleCloseModal} />
                        <RFlex className="justify-content-end" styleProps={{ width: 'fit-content' }}>
                            <RButton text="New Schedular"
                                faicon='fa-solid fa-plus'
                                color="link"
                                className="text-primary m-0 p-0"
                                style={{ width: 'fit-content' }}
                                onClick={handleNewAssessment} />

                        </RFlex>
                    </RFlex>
                    <RFlex className="flex-column justify-content-center" styleProps={{ width: '100%', gap: '5px' }}>
                        {assessments.ids.map((key, index) => {
                            const assessment = assessments[key]
                            const isError = assessment.touched['name'] && assessment.name == ""
                            return (
                                <RFlex className={`justify-content-between ${isError ? "align-items-baseline" : "align-items-center"}`}
                                    styleProps={{ width: '95%', gap: '7px' }}>
                                    <RFlex className="flex-column" styleProps={{ gap: "2px" }}>
                                        <RHoverInput
                                            inputValue={assessment.name}
                                            handleInputDown={handleInputDown}
                                            inputWidth='100px'
                                            inputPlaceHolder=""
                                            handleInputChange={handleInputChange}
                                            inputIsNotValidate={!assessment['name'] && assessment['touched']?.name}
                                            item={assessment}
                                            extraInfo={{ type: assessmentsTypes.Name }}
                                            handleOnBlur={handleOnBlur}
                                            handleEditFunctionality={handleEditFunctionality}
                                            saved={assessment?.saved}
                                        />
                                        {assessment.touched['name'] && assessment.name == "" && <p className='p-0 m-0 text-danger'>Name is Required</p>}

                                    </RFlex>
                                    <RFlex className="flex-column" styleProps={{ gap: "2px" }}>
                                        <Input
                                            name="dueDate"
                                            type="date"
                                            // onFocus={() => dispatch({ type: SET_FROM_TIME_TOUCHED, payload: { id: assessment.id } })}
                                            placeholder={tr`due_time`}
                                            onBlur={() => { handleDateBlur(assessment, { type: assessmentsTypes.Timestamp }) }}
                                            value={assessment.timestamp || null}
                                            onChange={(e) => handleDateChange(assessment, e.target.value)}
                                            style={{ height: "40px", width: "150px" }}
                                            className={assessment.touched['timestamp'] && !assessment.timestamp ? "input__error" : ""}
                                        />
                                        {assessment.touched['timestamp'] && !assessment.timestamp && <p className='p-0 m-0 text-danger'>Date is Required</p>}

                                    </RFlex>
                                    <i
                                        className={`${iconsFa6.delete} text-danger`}
                                        style={{ cursor: 'pointer' }}
                                        onClick={() => deleteAssessment(assessment)} />
                                </RFlex>
                            )
                        })}
                    </RFlex>
                </RFlex>
    )
}

export default AssessmentSchedular