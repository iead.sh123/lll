import RFlex from 'components/Global/RComs/RFlex/RFlex'
import React, { useState } from 'react'
import styles from './SchoolCreating.module.scss'
import { Input } from "reactstrap";
import RButton from 'components/Global/RComs/RButton';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import RSchoolCreating from './RSchoolCreating';
import { useSelector, useDispatch } from 'react-redux';
import { schoolManagementAPI } from 'api/SchoolManagement';
import Loader from 'utils/Loader';
import * as actions from 'store/actions/global/schoolManagement.action'
import { useHistory } from "react-router-dom";
import RTwoButtons from 'components/Global/RComs/RTwoButtons/RTwoButtons';
export const SchoolContext = React.createContext()
const GSchoolCreating = ({ isModal = false, closeModal }) => {
    const [uploadedImage, setUploadedImage] = useState(null);
    const { info, loading } = useSelector((state) => state?.schoolManagementRed)
    const dispatch = useDispatch()
    const history = useHistory()
    const initialValues = {
        email: info?.email ?? '',
        address: info?.address ?? '',
        url: info?.url ?? ''
    }
    const { organization_id } = useSelector((state) => state?.auth?.schoolsAndTypes?.[0])
    const validationSchema = Yup.object().shape({
        // logo: Yup.mixed().required('logo is required'),
        address: Yup.string(),
        email: Yup.string(),
        url: Yup.string()
    });



    const editSchoolInfo = async (organization_id, values, upload_id) => {
        dispatch(actions.editSchoolInfo(organization_id, values, upload_id, info?.image, history, !isModal))
    }
    const getSchoolInfo = () => {
        dispatch(actions.getSchoolInfo(organization_id, history))

    }

    React.useEffect(() => {
        getSchoolInfo()
        return () => { }
    }, [])

    const submitHandler = (values) => {
        editSchoolInfo(organization_id, values, uploadedImage?.id)
        closeModal()
    }
    return (info.gettingLoading ? <Loader /> :

        (<>
            {/* {isNavigate ? history.replace("/g/school-managment") : ""} */}
            <RFlex className={`${isModal ? styles.CreateContainerModal : styles.CreateContainer}`} styleProps={{ gap: '20px' }}>
                {!isModal ?
                    <div style={{ marginBottom: '0px',fontSize:'18px' }}>Let's Start By Getting <strong>{info?.name ?? "name"} </strong>Info</div>
                    : <p className='p-0 m-0'>Edit School Information</p>
                }

                <Formik
                    validateOnChange={true}
                    enableReinitialize={true}
                    initialValues={initialValues}
                    validationSchema={validationSchema}
                    onSubmit={submitHandler}>
                    {({ values, touched, errors, handleBlur, handleChange, handleSubmit, setFieldValue, initialValues }) => {
                        return (
                            <SchoolContext.Provider
                                value={{
                                    values,
                                    touched,
                                    errors,
                                    handleBlur,
                                    handleChange,
                                    setFieldValue,
                                    uploadedImage,
                                    setUploadedImage,
                                    info,
                                }}>

                                <Form className={`d-flex flex-column ${isModal ? "align-items-start" : 'align-items-center'}`} style={{ gap: '20px' }} onSubmit={handleSubmit}>
                                    <RSchoolCreating isModal={isModal} />
                                    {!isModal ? (
                                        <button className={styles.SaveButton} type='submit'>
                                            {!info.editingLoading ? (
                                                <>
                                                    <span className='p-0 m-0'>Save And Start My School</span>
                                                    <i className='fa-solid fa-arrow-right' />
                                                </>) : (
                                                <>
                                                    <>
                                                        <span className='p-0 m-0'>Sending...</span>
                                                        <i className="fa fa-refresh fa-spin"></i>
                                                    </>
                                                </>
                                            )}
                                        </button>
                                    ) : (
                                        !info.editingLoading ? (
                                            <RTwoButtons
                                                text1="Save"
                                                text2="Cancel"
                                                button1Style={{ backgroundColor: '#668AD7', color: 'white' }}
                                                button2Style={{ backgroundColor: 'white', color: '#668AD7' }}
                                                flexStyle={{ gap: '16px' }}
                                                type1='submit'
                                                onClick2={closeModal}

                                            />
                                        ) : (
                                            <>
                                                <span className='p-0 m-0'>Sending...</span>
                                                <i className="fa fa-refresh fa-spin"></i>
                                            </>
                                        )
                                    )}

                                </Form>
                            </SchoolContext.Provider>

                        )
                    }}

                </Formik>
            </RFlex>
        </>
        )
    )
}

export default GSchoolCreating