import React, { useEffect } from "react";
import RCollapsedDiv from "./RCollapsedDiv";
import { useState } from "react";
import GGradeLevel from "./GGradeLevel";
import styles from "./SchoolLister.module.scss";
import { useDispatch, useSelector } from "react-redux";
import * as actions from "store/actions/global/schoolManagement.action";
import { types } from "./constants";
import Swal, { DANGER } from "utils/Alert";
import { deleteSweetAlert } from "components/Global/RComs/RAlert2";
import tr from "components/Global/RComs/RTranslator";
const GEducationStage = ({ id, educationCurrentIndex, onDelete }) => {
	const dispatch = useDispatch();
	const user = useSelector((state) => state?.auth);
	const organization_id = user && user?.user && user?.user?.organization_id;
	const [saved, setSaved] = useState(id > 0 ? true : false);
	const [saveClicked, setSaveClicked] = useState(false);
	const [addClicked, setAddClicked] = useState(false);
	const [enterPressed, setEnterPressed] = useState(false);
	const [alert1, setAlert] = useState(false);

	const currentStage = useSelector((state) => state.schoolManagementRed?.stages[id]);
	const { openStageId, openGradLevelId } = useSelector((state) => state.schoolManagementRed);

	const [inputValue, setInputValue] = useState(currentStage?.name ?? "");
	const grades = useSelector((state) => state.schoolManagementRed?.gradeLevels);

	// if (educationOpenIndex == educationCurrentIndex) {
	//     dispatch(actions.setActiveId(types.Stage, id))
	// }
	const isItActive = openStageId == id && openGradLevelId == null ? true : false;
	const editEducation = () => {
		// dispatch(actions.setOpenId(types.Stage, id))
		setEnterPressed(false);
		setSaved(!saved);
	};

	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);
	const deleteEducation = (id, order) => {
		const confirm = tr`Yes, delete it`;
		const message = (
			<div>
				<h6>{tr`Are you sure? `}</h6>
				<p style={{ fontSize: "14px", fontWeight: "400" }}>{tr`Deleting is permanent`}</p>
			</div>
		);
		deleteSweetAlert(showAlerts, hideAlert, onDelete, { stageId: id, order }, message, confirm);
	};

	const handleInputChange = (event) => {
		setSaveClicked(false);
		setAddClicked(false);
		setInputValue(event.target.value);
	};

	const handleDivClicked = (id) => {
		// handleEducationCollapse(index)
		dispatch(actions.setOpenId(types.Stage, id));
		if (id <= 0) return;
		if (openStageId != id) {
			dispatch(actions.getPrincipalForEducation(id));
		}
	};

	const handleInputDown = async (event) => {
		if (event.key === "Enter") {
			if (enterPressed) return;
			setEnterPressed(true);
			setSaveClicked(true);
			if (inputValue != "") {
				const success = await dispatch(actions.saveEducationStage(id, inputValue, currentStage.order, organization_id));
				setEnterPressed(false);
				setSaved(success);
			}
			// event.stopPropagation()
		}
	};

	const handleSaveButton = async () => {
		if (enterPressed) {
			return;
		}
		setSaveClicked(true);
		if (inputValue != "") {
			const success = await dispatch(actions.saveEducationStage(id, inputValue, currentStage.order, organization_id));
			setEnterPressed(false);
			setSaved(success);
		}
		// event.stopPropagation()
	};
	const generateGradeLevelComponent = () => {
		if (id <= 0) {
			setAddClicked(true);
			return;
		}
		const lastItemId = currentStage?.gradeLevelIds[currentStage.gradeLevelIds?.length - 1];
		const lastOrder = lastItemId ? grades[lastItemId].grade_level_order : 0;
		dispatch(actions.addSpecificCollapse(types?.GradeLevel, Object.keys(grades)?.length, id, lastOrder + 1));
	};
	const handleDeleteGradeLevel = ({ gradeLevelId, order }) => {
		dispatch(actions?.deleteSpecificCollapse(types.GradeLevel, gradeLevelId, id, order));
	};

	const showAlert = (gradeLevelId, order) => {
		Swal.input({
			title: tr`are_you_sure_to_delete_it`,
			message: tr``,
			type: DANGER,
			placeHolder: "",
			onConfirm: () => handleDeleteGradeLevel(gradeLevelId, order),
			onCancel: () => null,
		});
	};
	const gradeLevelComponents = currentStage.gradeLevelIds.map((key, index) => {
		return {
			component: <GGradeLevel id={parseInt(key)} educationStageId={id} gradLevelCurrentIndex={index} onDelete={handleDeleteGradeLevel} />,
			id: key,
		};
	});

	return (
		<>
			<RCollapsedDiv
				handleInputChange={handleInputChange}
				handleInputDown={handleInputDown}
				handleDivClicked={handleDivClicked}
				handleSaveButton={handleSaveButton}
				generateChildComponent={generateGradeLevelComponent}
				handleEditFunctionality={editEducation}
				handleDeleteFunctionality={deleteEducation}
				setSaved={setSaved}
				id={id}
				collapseChilds={gradeLevelComponents}
				order={currentStage.order}
				isOpen={id == openStageId ? true : false}
				openLevelId={openStageId}
				inputValue={inputValue}
				number={currentStage?.gradeLevelIds?.length}
				saved={saved}
				addClicked={addClicked}
				saveClicked={saveClicked}
				inputPlaceHolder="education stage"
				buttonText="Add Grade Level"
				currentType={types.Stage}
				childType="Grade Levels"
				index={educationCurrentIndex}
				childPrefix={"childPrefix"}
				isItActive={isItActive}
			/>
			{alert1}
		</>
	);
};

export default GEducationStage;
