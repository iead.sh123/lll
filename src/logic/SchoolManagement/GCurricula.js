import React from 'react'
import RCollapsedDiv from './RCollapsedDiv'
import { useState } from 'react'
import styles from './SchoolLister.module.scss'
import { useDispatch, useSelector } from 'react-redux'
import * as actions from 'store/actions/global/schoolManagement.action'
import { types } from './constants'
import AppModal from 'components/Global/ModalCustomize/AppModal'
import GCoursesSchool from './GCoursesSchool'
import { deleteSweetAlert } from 'components/Global/RComs/RAlert2'
import tr from 'components/Global/RComs/RTranslator'


const GCurricula = (
    {
        id,
        gradeLevelId,
        curriculuaCurrentIndex,
        onDelete,
    }
) => {
    const dispatch = useDispatch()
    // if (curriculaOpenIndex == curriculuaCurrentIndex) {
    //     dispatch(actions.setActiveId(types.Curricula, id))
    // }
    const currentCurruicula = useSelector((state) => state.schoolManagementRed?.curriculas[id])
    const activeCourses = useSelector((state) => state.schoolManagementRed?.activeCourses)
    const [saved, setSaved] = useState(id > 0 ? true : false)
    const [inputValue, setInputValue] = useState(currentCurruicula?.name ?? "")
    const { openCurriculaId } = useSelector((state) => state.schoolManagementRed)
    const [alert1, setAlert] = useState(false);

    const [enterPressed, setEnterPressed] = useState(false)
    const [saveClicked, setSaveClicked] = useState(false)
    const [addClicked, setAddClicked] = useState(false)
    const [openCourses, setOpenCourses] = useState(false)
    let coursesNumber = 0
    if (currentCurruicula.master_courses) {
        const courses = JSON.parse(currentCurruicula.master_courses)
        coursesNumber = courses?.length
    }
    const handleCloseCoursesModal = () => {
        setOpenCourses(false)
    }
    const childPrefix = 'content'

    const editCurricula = () => {
        console.log("Edit Triggered")
        setEnterPressed(false)
        setSaved(!saved)
    }

    const hideAlert = () => setAlert(null);
    const showAlerts = (child) => setAlert(child);

    const deleteCurricula = (id) => {
        const confirm = tr`Yes, delete it`;
        const message = (
            <div>
                <h6>{tr`Are you sure? `}</h6>
                <p style={{ fontSize: "14px", fontWeight: "400" }}>{tr`Deleting is permanent`}</p>
            </div>
        );
        deleteSweetAlert(showAlerts, hideAlert, onDelete, { curriculaId: id }, message, confirm);
    }
    const handleInputChange = (event) => {
        setSaveClicked(false)
        setAddClicked(false)
        setInputValue(event.target.value)
    }

    const handleInputDown = async (event) => {
        if (event.key === 'Enter') {
            if (enterPressed)
                return
            setSaveClicked(true)
            setEnterPressed(true)
            if (inputValue != "") {
                const success = await dispatch(actions?.saveCurricula(id, inputValue, currentCurruicula.order, gradeLevelId))
                setEnterPressed(false)
                setSaved(success);
                if (id <= 0)
                    return
                dispatch(actions.getCurriculaContent(id))

            }
        }
    }
    const handleSaveButton = async () => {
        if (enterPressed) {
            return
        }
        setSaveClicked(true)
        if (inputValue != '') {
            const success = await dispatch(actions?.saveCurricula(id, inputValue, currentCurruicula.order, gradeLevelId))
            setEnterPressed(false)
            setSaved(success);
            if (id <= 0)
                return
            dispatch(actions.getCurriculaContent(id))

        }
    }
    const handleDivClicked = (index) => {
        console.log("Div Clicked");
        dispatch(actions.setOpenId(types.Curricula, id))
        if (id <= 0)
            return
        if (openCurriculaId != id) {
            dispatch(actions.getCurriculaContent(id))

        }

    }
    const openCourseCataloug = () => {
        if (id <= 0) {
            setAddClicked(true)
            // setErrorMessage('Please Create GradeLevel First')
            return
        }
        setOpenCourses(true)
    }

    // const [curriculaBodyElements, setCurriculaBodyElements] = useState(<GCurriculaContent />);
    // const generateCurriculaBodyComponent = () => {
    //     // Create a new instance of your dynamic component and add it to the state
    //     const newComponent = <span className='m-0 p-0 text-primary'>Body</span>;
    //     setCurriculaBodyElements([...curriculaBodyElements, newComponent]);
    // };

    return (
        <>
            <RCollapsedDiv
                handleInputChange={handleInputChange}
                handleInputDown={handleInputDown}
                handleDivClicked={handleDivClicked}
                handleEditFunctionality={editCurricula}
                handleDeleteFunctionality={deleteCurricula}
                handleSaveButton={handleSaveButton}
                setSaved={setSaved}
                id={id}
                collapseChilds={activeCourses}
                openLevelId={openCurriculaId}
                inputValue={inputValue}
                isOpen={openCurriculaId == id ? true : false}
                number={coursesNumber}
                saved={saved}
                addClicked={addClicked}
                saveClicked={saveClicked}
                index={curriculuaCurrentIndex}
                inputPlaceHolder="Curricula here"
                buttonText="Add Curricula"
                currentType={types.Curricula}
                childType="Courses"
                childPrefix={childPrefix}
                openCourseCataloug={openCourseCataloug}

            />
            <AppModal size={"lg"}
                show={openCourses}
                header={"Courses"}
                parentHandleClose={handleCloseCoursesModal}
                headerSort={<GCoursesSchool isModal={true} handleCloseCoursesModal={handleCloseCoursesModal} />} />
            {alert1}
        </>
    )
}

export default GCurricula