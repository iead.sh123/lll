import React from 'react'
import RCollapsedDiv from './RCollapsedDiv'
import { useState } from 'react'
import GCurricula from './GCurricula'
import styles from './SchoolLister.module.scss'
import { useDispatch, useSelector } from 'react-redux'
import * as actions from 'store/actions/global/schoolManagement.action'
import { types } from './constants'
import Swal, { DANGER } from 'utils/Alert'
import tr from "components/Global/RComs/RTranslator";
import { deleteSweetAlert } from 'components/Global/RComs/RAlert2'
import GCoursesCategories from './GCoursesCategories'
import AppModal from 'components/Global/ModalCustomize/AppModal'

const GGradeLevel = (
    {
        id,
        educationStageId,
        gradLevelCurrentIndex,
        onDelete,
    }
) => {
    const dispatch = useDispatch()
    console.log("GradeLevel", typeof (id))

    const [saved, setSaved] = useState(id > 0 ? true : false)
    const [openCurriculaCategories, setOpenCurriculaCategories] = useState(false)
    const [saveClicked, setSaveClicked] = useState(false)
    const openGradLevelId = useSelector((state) => state.schoolManagementRed?.openGradLevelId)
    const [enterPressed, setEnterPressed] = useState(false)
    const [alert1, setAlert] = useState(false);

    const currentGrade = useSelector((state) => state.schoolManagementRed?.gradeLevels[id])
    const [inputValue, setInputValue] = useState(currentGrade.title ?? "")
    const curriculas = useSelector((state) => state.schoolManagementRed?.curriculas)
    const [addClicked, setAddClicked] = useState(false)
    const [errorMessage, setErrorMessage] = useState('')

    const isItActive = openGradLevelId == id ? true : false
    const handleCloseCoursesModal = () => {
        setOpenCurriculaCategories(false)
    }
    const editGradeLevel = () => {
        console.log("Edit Triggered")
        setEnterPressed(false)
        setSaved(!saved)
    }

    const hideAlert = () => setAlert(null);
    const showAlerts = (child) => setAlert(child);

    const deleteGradeLevel = (id, order) => {
        const confirm = tr`Yes, delete it`;
        const message = (
            <div>
                <h6>{tr`Are you sure? `}</h6>
                <p style={{ fontSize: "14px", fontWeight: "400" }}>{tr`Deleting is permanent`}</p>
            </div>
        );
        deleteSweetAlert(showAlerts, hideAlert, onDelete, { gradeLevelId: id, order }, message, confirm);
    }

    const handleInputChange = (event) => {
        setSaveClicked(false)
        setAddClicked(false)
        setInputValue(event.target.value)
    }

    const handleDivClicked = (index) => {
        console.log("Div Clicked");
        dispatch(actions.setOpenId(types.GradeLevel, id))
        if (id <= 0)
            return
        if (openGradLevelId != id) {
            dispatch(actions.getStudentsForGrade(id))
            dispatch(actions.getAllAssessments(id))
        }

    }


    const handleInputDown = async (event) => {
        if (event.key === 'Enter') {
            if (enterPressed)
                return
            setSaveClicked(true)
            setEnterPressed(true)
            if (inputValue != "") {
                const success = await dispatch(actions?.saveGradeLevel(id, inputValue, currentGrade.grade_level_order, educationStageId))
                setEnterPressed(false)
                
                setSaved(success);

            }
        }
    }
    const handleSaveButton = async () => {
        if (enterPressed) {
            return
        }
        setSaveClicked(true)
        if (inputValue != '') {
            const success = await dispatch(actions?.saveGradeLevel(id, inputValue, currentGrade.grade_level_order, educationStageId))
            setEnterPressed(false)
            setSaved(success);
        }
    }
    const generateCurriculaComponent = () => {
        // Create a new instance of your dynamic component and add it to the state
        if (id <= 0) {
            setAddClicked(true)
            // setErrorMessage('Please Create GradeLevel First')
            return
        }
        const lastItemId = currentGrade.curriculaIds[currentGrade.curriculaIds.length - 1]
        const lastOrder = lastItemId ? curriculas[lastItemId].order : 0
        dispatch(actions.addSpecificCollapse(types?.Curricula, Object.keys(curriculas)?.length, id, lastOrder + 1))

    };

    const handleDeleteCurricula = ({ curriculaId }) => {
        dispatch(actions?.deleteSpecificCollapse(types.Curricula, curriculaId, id))

    };
    const showAlert = (curriculaId) => {
        Swal.input({
            title: tr`are_you_sure_to_delete_it`,
            message: tr``,
            type: DANGER,
            placeHolder: "",
            onConfirm: () => handleDeleteCurricula(curriculaId),
            onCancel: () => null,
        });
    }
    const curriculasComponents = currentGrade.curriculaIds.map((key, index) => {
        return {
            component: <GCurricula
                id={parseInt(key)}
                gradeLevelId={id}
                curriculuaCurrentIndex={index}
                onDelete={handleDeleteCurricula} />,
            id: key
        }
    })

    return (
        <>
            <RCollapsedDiv
                handleInputChange={handleInputChange}
                handleInputDown={handleInputDown}
                handleDivClicked={handleDivClicked}
                handleEditFunctionality={editGradeLevel}
                handleDeleteFunctionality={deleteGradeLevel}
                handleSaveButton={handleSaveButton}
                generateChildComponent={generateCurriculaComponent}
                setSaved={setSaved}
                id={id}
                order={currentGrade.order}
                collapseChilds={curriculasComponents}
                inputValue={inputValue}
                openLevelId={openGradLevelId}
                isOpen={openGradLevelId == id ? true : false}
                saved={saved}
                addClicked={addClicked}
                saveClicked={saveClicked}
                number={currentGrade?.curriculaIds?.length}
                index={gradLevelCurrentIndex}
                inputPlaceHolder="Grade Level"
                buttonText="Add Curricula"
                currentType={types.GradeLevel}
                childType="Curricula"
                childPrefix={"childPrefix"}
                isItActive={isItActive}
                setOpenCurriculaCategories={setOpenCurriculaCategories}

            />
            <AppModal size={"lg"}
                show={openCurriculaCategories}
                header={true}
                parentHandleClose={handleCloseCoursesModal}
                headerSort={<GCoursesCategories handleCloseCoursesModal={handleCloseCoursesModal} />} />
            {alert1}
        </>
    )
}

export default GGradeLevel