import React, { useContext, useEffect, useState } from "react";
import { LandingPageContext } from "../GHome";
import { useHistory } from "react-router-dom";
import { Services } from "engine/services";
import RButton from "components/Global/RComs/RButton";
import styles from "../Home.module.scss";
import RCard from "components/Global/RComs/RCards/RCard";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import REmptyData from "components/RComponents/REmptyData";

const GOfflineCourses = () => {
	const history = useHistory();

	const LandingPageData = useContext(LandingPageContext);
	const [courses, setCourses] = useState([]);

	useEffect(() => {
		if (
			LandingPageData &&
			LandingPageData.landingPage &&
			LandingPageData.landingPage.offlineCourses &&
			LandingPageData.landingPage.offlineCourses.courses &&
			LandingPageData.landingPage.offlineCourses.courses.length > 0
		) {
			buildCourses();
		}
	}, [
		LandingPageData &&
			LandingPageData.landingPage &&
			LandingPageData.landingPage.offlineCourses &&
			LandingPageData.landingPage.offlineCourses.courses &&
			LandingPageData.landingPage.offlineCourses.courses.length > 0,
		LandingPageData.cartItems,
		LandingPageData.addToCartLoading,
	]);

	const buildCourses = () => {
		let handleCourses = [];

		LandingPageData.landingPage?.offlineCourses?.courses?.map((course, index) => {
			let data = {};
			data.image = course.image ? `${Services.courses_manager.file + course.image}` : null;
			data.icon = course.icon ? `${Services.courses_manager.file + course.icon}` : null;
			data.id = course.id;
			data.name = course?.name;
			data.color = course?.color;
			data.rate = course?.rating;
			data.categoryName = course?.category?.category_name;
			data.users = course?.teachers;
			data.isOnline = course?.isOnline;
			data.extra = course?.extra;
			data.overview = course?.overview;
			data.isEnroll = course?.isEnrolled;
			data.enrollCourse = {
				onClick: () => LandingPageData.handleEnrollInCourse(course.id),
				loading: LandingPageData.enrollInCourseLoading,
			};
			data.addToCart = {
				onClick: () => LandingPageData.handleAddToCart({ productID: course?.id, productType: course?.paymentInfo?.type }),
				loading: LandingPageData.addToCartLoading,
				disabled: LandingPageData.cartItems && LandingPageData.cartItems?.find((item) => +item.ProductID == course.id) ? true : false,
			};

			data.link = `/landing/course/${course.id}/course-overview`;
			handleCourses.push(data);
		});

		setCourses(handleCourses);
	};

	const handlePushToAllCourses = () => {
		history.push(
			`/landing/all-courses?booleanFilters=${Object.keys(Object.values(LandingPageData.landingPage.offlineCourses.filters)[0])}`
		);
	};

	return (
		<section className={styles.padding}>
			{courses && courses?.length > 0 ? (
				<React.Fragment>
					<h6>{LandingPageData?.landingPage?.offlineCourses?.label}</h6>
					<section className={styles.all_courses}>
						{courses.map((course) => (
							<RCard course={course} hiddenFlag={true} />
						))}
					</section>{" "}
					<RFlex styleProps={{ justifyContent: "center" }}>
						<RButton color="primary" text={tr`see_all_courses`} onClick={handlePushToAllCourses} />
					</RFlex>
				</React.Fragment>
			) : (
				<div style={{ justifyContent: "center" }}>
					<REmptyData />
				</div>
			)}
		</section>
	);
};

export default GOfflineCourses;
