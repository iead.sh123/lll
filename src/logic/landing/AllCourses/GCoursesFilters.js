import React, { useState, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";
import { allCoursesAsync } from "store/actions/global/coursesManager.action";
import RCoursesFilters from "view/Landing/AllCourses/RCoursesFilters";
import Loader from "utils/Loader";
import { Form, FormGroup, Input, UncontrolledDropdown, DropdownToggle, DropdownMenu } from "reactstrap";
import styles from "./AllCourses.module.scss";
import tr from "components/Global/RComs/RTranslator";

const GCoursesFilters = () => {
	const dispatch = useDispatch();
	const location = useLocation();
	const history = useHistory();
	const dropdownRef = useRef(null);

	const queryParams = new URLSearchParams(location.search);
	const categoryIdParams = queryParams.get("categoryId");
	const booleanFiltersParams = queryParams.get("booleanFilters");
	const textParams = queryParams.get("text");

	const [selectedBooleanItems, setSelectedBooleanItems] = useState(booleanFiltersParams ? booleanFiltersParams.split(",") : []);
	const [searchData, setSearchData] = useState(textParams ? textParams : "");

	const { allFiltersToCourses, allFiltersToCoursesLoading } = useSelector((state) => state.coursesManagerRed);
	const { user } = useSelector((state) => state.auth);

	const handleFilter = (type, booleanFilter) => {
		if (type == "boolean") {
			const index = selectedBooleanItems.indexOf(booleanFilter);

			if (index === -1) {
				setSelectedBooleanItems((prevSelectedItems) => {
					const updatedState = [...prevSelectedItems, booleanFilter];
					handleStateUpdate(updatedState);
					return updatedState; // Return the new state value
				});
			} else {
				let newArray = selectedBooleanItems.filter((item) => item !== booleanFilter);
				setSelectedBooleanItems(() => {
					const updatedState = newArray;
					handleStateUpdate(updatedState);
					return updatedState;
				});
			}
		}
	};

	const handleStateUpdate = (updatedState) => {
		let queryParams = "";

		if (updatedState.length > 0) {
			if (categoryIdParams && searchData) {
				queryParams = `?text=${searchData}&categoryId=${categoryIdParams}&booleanFilters=${updatedState}`;
			} else if (!categoryIdParams && searchData) {
				queryParams = `?text=${searchData}&booleanFilters=${updatedState}`;
			} else if (categoryIdParams && !searchData) {
				queryParams = `?categoryId=${categoryIdParams}&booleanFilters=${updatedState}`;
			} else {
				queryParams = `?booleanFilters=${updatedState}`;
			}
		} else {
			if (categoryIdParams && searchData) {
				queryParams = `?text=${searchData}&categoryId=${categoryIdParams}`;
			} else if (!categoryIdParams && searchData) {
				queryParams = `?text=${searchData}`;
			} else if (categoryIdParams && !searchData) {
				queryParams = `?categoryId=${categoryIdParams}`;
			} else {
				queryParams = "";
			}
		}
		history.push(`/landing/all-courses${queryParams}`);
	};

	const handleChangeSearch = (text) => {
		setSearchData(text);

		handleStateUpdate(selectedBooleanItems);
	};

	const handleApplySearch = ({ reset }) => {
		let data = {};

		const newFormate = reset
			? []
			: selectedBooleanItems.reduce((acc, item) => {
					acc[item] = true;
					return acc;
			  }, {});

		if (categoryIdParams) {
			data.category_id = categoryIdParams;
		}

		if (selectedBooleanItems?.length > 0) {
			data = { ...data, ...newFormate };
		}

		if (searchData !== "") {
			data.name = searchData;
		}

		dispatch(allCoursesAsync(user, 1, data));
	};

	const handleResetSearch = () => {
		if (selectedBooleanItems?.length > 0) {
			setSelectedBooleanItems(() => {
				const updatedState = [];
				handleStateUpdate(updatedState);
				return updatedState;
			});
			handleApplySearch({ reset: true });
		}
	};

	const closeDropdown = () => {
		if (dropdownRef.current) {
			dropdownRef.current.toggle(); // Close the dropdown
		}
	};

	return (
		<Form className={styles.form__input}>
			<FormGroup>
				<Input
					type="text"
					placeholder={tr`search`}
					className={styles.search__input}
					value={searchData}
					onChange={(event) => {
						handleChangeSearch(event.target.value);
					}}
				/>
				<i
					aria-hidden="true"
					className={false ? `fa fa-refresh fa-spin ${styles.search__input__icon}` : `fa fa-search ${styles.search__input__icon}`}
					onClick={() => searchData !== "" && handleApplySearch({ reset: false })}
				/>

				<UncontrolledDropdown direction="right" className={`${styles.filter__input__icon}`}>
					<DropdownToggle
						aria-haspopup={true}
						color="default"
						data-toggle="dropdown"
						nav
						style={{
							padding: "0px",
							color: "#d9d9d9",
						}}
					>
						<i aria-hidden="true" className={`fas fa-sliders-h `} />
					</DropdownToggle>{" "}
					<DropdownMenu
						persist
						aria-labelledby="navbarDropdownMenuLink"
						right
						className={`${styles.dropdown__menu} + " mb-4"`}
						innerRef={dropdownRef}
					>
						{allFiltersToCoursesLoading ? (
							<Loader />
						) : (
							<RCoursesFilters
								allFiltersToCourses={allFiltersToCourses}
								handleFilter={handleFilter}
								handleApplySearch={handleApplySearch}
								handleResetSearch={handleResetSearch}
								selectedBooleanItems={selectedBooleanItems}
								closeDropdown={closeDropdown}
							/>
						)}
					</DropdownMenu>
				</UncontrolledDropdown>
			</FormGroup>
		</Form>
	);
};

export default GCoursesFilters;
