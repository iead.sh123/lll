import { gradingApi } from "api/grading";
import tr from "components/Global/RComs/RTranslator";
import Swal, { SUCCESS, DANGER } from "utils/Alert";
import { entities } from "../constants/entities.constants";
import { GradableItemTypes } from "../constants/gradable-items-type";
import { pages } from "../constants/pages.constant";
import Helper from "components/Global/RComs/Helper";
import { toast } from "react-toastify";

//steppers
export const NAVIGATE_STEPPER = "stepper/navigate";

//entities
export const LOAD_ENTITIES_LIST = "grading-entities/load";

export const ADD_ENTITY = "grading-entities/add";

export const DELETE_ENTITY = "grading-entities/delete";

export const UPDATE_ENTITY = "grading-entities/update";

//customized
export const UPDATE_ALTERNATIVES = "gradableitems/updateAlternatives";

//student details
export const LOAD_GRADABLE_ITEM_STUDENT_DETAILS = "gradableitemstudent/details-for-multiple-students";

const fakeIdsGenerator = (function* () {
	let i = 0;
	while (true) {
		yield "f_" + ++i;
	}
})();

export const loadGradingGroups = async (courseId, dispatch, mounted) => {
	const sourceEndPointCallback = async () => await gradingApi.gradingGroupsListByCourse(courseId);
	return await loadEntitiesList(sourceEndPointCallback, entities.gradingGroups, dispatch, mounted);
};

export const loadCourseGradableItems = async (courseId, dispatch, mounted) => {
	const sourceEndPointCallback = async () => await gradingApi.gradableItemsListByCourse(courseId);
	return await loadEntitiesList(
		sourceEndPointCallback,
		entities.gradableItems,
		dispatch,
		mounted,
		(rec) => `${rec.type}_${rec.id}`,
		(rec) => ({
			...rec,
			numberOfAlternatives: +rec?.alternatives?.length,
			alternatives: rec.alternatives.map((alt) => ({ ...alt, id: alt.type + "_" + alt.id })),
		})
	);
};

const loadEntitiesList = async (
	sourceEndPointCallback,
	entityName,
	dispatch,
	mounted,
	deriveId = (d) => d.id,
	mapRecord = (rec) => rec
) => {
	try {
		const response = await sourceEndPointCallback();

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: LOAD_ENTITIES_LIST,
				payload: {
					byId:
						response.data?.data?.records?.reduce?.(
							(prev, curr) => ({
								...prev,
								[deriveId(curr)]: { ...mapRecord(curr), id: deriveId(curr) },
							}),
							{}
						) ?? {},
					entityName,
					ids: response.data?.data?.records?.map?.((d) => deriveId(d)) ?? [],
					headers: response.data?.data?.headers,
				},
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const addGradingGroups = async (courseId, data, dispatch, mounted) => {
	const sourceEndPointCallback = async () => await gradingApi.addGradingGroup(courseId, data);
	return await addEntity(sourceEndPointCallback, entities.gradingGroups, dispatch, mounted, (gr) => ({ ...gr, items: 0 }));
};

// export const addCourseGradableItems = async (courseId, dispatch, mounted) => {
//     const sourceEndPointCallback = async () => await gradingApi.gradableItemsListByCourse(courseId);
//     return await loadEntitiesList(sourceEndPointCallback, entities.gradableItems, dispatch, mounted);
// }

const addEntity = async (sourceEndPointCallback, entityName, dispatch, mounted, mapRecordCallBack = (a) => a) => {
	try {
		const response = await sourceEndPointCallback();

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: ADD_ENTITY,
				payload: {
					entityName,
					record: mapRecordCallBack(response?.data?.data),
				},
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const deleteGradingGroup = async (id, dispatch, mounted) => {
	const sourceEndPointCallback = async () => await gradingApi.deleteGradingGroup(id);
	return await deleteEntity(sourceEndPointCallback, entities.gradingGroups, id, dispatch, mounted);
};

export const updateGradingGroup = async (id, data, dispatch, mounted) => {
	const sourceEndPointCallback = async () => await gradingApi.updateGradingGroup(id, data);
	return await updateEntity(sourceEndPointCallback, entities.gradingGroups, id, data, dispatch, mounted, (d) => data);
};

const deleteEntity = async (sourceEndPointCallback, entityName, id, dispatch, mounted) => {
	try {
		const response = await sourceEndPointCallback();

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: DELETE_ENTITY,
				payload: {
					entityName,
					id,
				},
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

const updateEntity = async (sourceEndPointCallbak, entityName, id, newData, dispatch, mounted, mapRecord = (r) => r) => {
	try {
		const response = await sourceEndPointCallbak(id, newData);

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: UPDATE_ENTITY,
				payload: {
					entityName,
					id,
					data: mapRecord(response.data?.data),
				},
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const assignGradableItemToGroup = async (gradableItemType, itemId, group, dispatch, mounted) => {
	const sourceEndPointCallback = async () => await gradingApi.assignGradableItemToGroup(gradableItemType, itemId, group.id);
	return await updateEntity(sourceEndPointCallback, entities.gradableItems, itemId, { ...group }, dispatch, mounted);
};

export const createCustomGradableItem = async (data, courseId, group, dispatch, mounted) => {
	const sourceEndPointCallback = async () => await gradingApi.createCustomGradableItem(courseId, { ...data, group: group.id });
	return await addEntity(sourceEndPointCallback, entities.gradableItems, dispatch, mounted, (rec) => ({
		...rec,
		alternatives: [],
		numberOfAlternatives: 0,
		type: GradableItemTypes.Custom,
		id: `${GradableItemTypes.Custom}_${rec.id}`,
		groupName: group.name,
		type: GradableItemTypes.Custom,
		percentageInCourse: 0.0,
		numberOfAlternatives: 0,
		numberOFGradedStudents: 0,
	}));
};

export const updateGraddableItems = async (id, data, dispatch, mounted) => {
	Helper.cl(id, "grading.actions updateGraddableItems id");
	Helper.cl(data, "grading.actions updateGraddableItems data");
	const sourceEndPointCallback = async () => await gradingApi.updateGraddableItem(id?.split("_")?.[0], +id?.split("_")?.[1], { ...data });
	return await updateEntity(sourceEndPointCallback, entities.gradableItems, id, data, dispatch, mounted, (r) => ({
		...r,
		id: id?.split("_")?.[0] + "_" + r.id,
		groupName: r?.group?.name,
		groupId: r?.group?.id,
	}));
};

export const deleteGraddableItem = async (id, dispatch, mounted) => {
	const sourceEndPointCallback = async () => await gradingApi.deleteGraddableItem(+id.split("_")[1]);
	return await deleteEntity(sourceEndPointCallback, entities.gradableItems, id, dispatch, mounted);
};

export const submitAlternatives = async (itemCompositeId, alternativesPayload, dispatch, mounted) => {
	try {
		const [type, id] = itemCompositeId?.split?.("_") ?? [null, null];
		const response = await gradingApi.addAndRemoveAlternatives(type, id, {
			added_alternatives: alternativesPayload?.added_alternatives ?? [],
			removed_alternatives: alternativesPayload?.removed_alternatives ?? [],
		});

		if (!response.data?.status) throw new Error(response.data?.msg);

		mounted.current &&
			dispatch({
				type: UPDATE_ALTERNATIVES,
				payload: {
					id: itemCompositeId,
					alternativesPayload: {
						added_alternatives: alternativesPayload?.added_alternatives ?? [],
						removed_alternatives: alternativesPayload?.removed_alternatives ?? [],
					},
				},
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};

export const moveToNextItem = async (itemCompositeId, alternativesPayload, dispatch, mounted) => {
	try {
		const [type, id] = itemCompositeId?.split?.("_") ?? [null, null];
		const response = await gradingApi.addAndRemoveAlternatives(type, id, {
			added_alternatives: alternativesPayload?.added_alternatives ?? [],
			removed_alternatives: alternativesPayload?.removed_alternatives ?? [],
		});

		if (!response.data?.status) throw new Error(response.data?.msg);

		mounted.current &&
			dispatch({
				type: UPDATE_ALTERNATIVES,
				payload: {
					id: itemCompositeId,
					alternativesPayload: {
						added_alternatives: alternativesPayload?.added_alternatives ?? [],
						removed_alternatives: alternativesPayload?.removed_alternatives ?? [],
					},
				},
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};
export const moveToPreviousItem = async (itemCompositeId, alternativesPayload, dispatch, mounted) => {
	try {
		const [type, id] = itemCompositeId?.split?.("_") ?? [null, null];
		const response = await gradingApi.addAndRemoveAlternatives(type, id, {
			added_alternatives: alternativesPayload?.added_alternatives ?? [],
			removed_alternatives: alternativesPayload?.removed_alternatives ?? [],
		});

		if (!response.data?.status) throw new Error(response.data?.msg);

		mounted.current &&
			dispatch({
				type: UPDATE_ALTERNATIVES,
				payload: {
					id: itemCompositeId,
					alternativesPayload: {
						added_alternatives: alternativesPayload?.added_alternatives ?? [],
						removed_alternatives: alternativesPayload?.removed_alternatives ?? [],
					},
				},
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};
export const loadGradableItemDetailsForAllStudents = async (gradableItemType, gradableItemId, dispatch, mounted) => {
	try {
		const response = await gradingApi.getAllStudentsDetailsInGradableItem(gradableItemType, gradableItemId);

		if (!response.data.status) throw new Error(response.data.msg);

		mounted?.current &&
			dispatch({
				type: LOAD_GRADABLE_ITEM_STUDENT_DETAILS,
				payload: {
					item: response.data.data?.gradableItem,
					students: {
						byId: response.data.data?.studentDetails?.reduce?.((prev, curr) => ({ ...prev, [curr?.student?.id]: curr }), {}) ?? {},
						ids: response.data.data?.studentDetails?.map?.((sd) => sd?.student?.id) ?? [],
					},
				},
			});
	} catch (err) {
		return toast.error(err?.message);
	}
};
