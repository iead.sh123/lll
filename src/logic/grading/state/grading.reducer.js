import { current, isDraft, produce } from "immer";
import { pages } from "../constants/pages.constant";
import {
	ADD_ENTITY,
	DELETE_ENTITY,
	LOAD_ENTITIES_LIST,
	LOAD_GRADABLE_ITEM_STUDENT_DETAILS,
	NAVIGATE_STEPPER,
	UPDATE_ALTERNATIVES,
	UPDATE_ENTITY,
} from "./grading.actions";
import { entities } from "../constants/entities.constants";
import Helper from "components/Global/RComs/Helper";

export const initialState = {
	selectedPage: pages.GROUP_AND_ITEMS,
	entities: {
		gradingGroups: {
			byId: {},
			headers: [],
			ids: [],
		},
		gradableItems: {
			byId: {},
			headers: [],
			ids: [],
		},
	},
	currentGroup: null,
	currentItem: null,
	// maybe customized furthur like update and delete cascade options
	GradableItemStudentsDetails: {},
	hasMany: {
		[entities.gradingGroups]: {
			related: entities.gradableItems,
			setNull: ["groupName", "groupId"],
		},
	},
};

export const reducer = (state = initialState, action) => {
	const { type, payload } = action;
	let temp1, temp2, temp3;

	return produce(state, (draft) => {
		let temp1 = null;
		switch (type) {
			case NAVIGATE_STEPPER:
				draft.selectedPage = payload;
				return draft;

			case LOAD_ENTITIES_LIST:
				draft.entities[payload.entityName] = { ...payload, entityName: undefined };
				return draft;

			case ADD_ENTITY:
				draft.entities[payload.entityName].ids.unshift(payload.record.id);
				draft.entities[payload.entityName].byId[payload.record.id] = payload.record;
				// draft.entities[payload.entityName] = recalucateEntityPercenateges(draft.entities[payload.entityName]);
				return draft;

			case DELETE_ENTITY:
				draft.entities[payload.entityName].ids = draft.entities[payload.entityName].ids.filter((i) => i !== payload.id);

				delete draft.entities[payload.entityName].byId[payload.id];

				//reflect relation effect

				temp1 = current(draft.hasMany[payload.entityName]);
				if (temp1) {
					draft.entities[temp1.related].ids.forEach((id) => {
						temp1.setNull.map((att) => {
							draft.entities[temp1.related].byId[id][att] = null;
						});
					});
				}

				if (payload.entityName === entities.gradableItems) {
					temp2 = current(draft.entities.gradableItems.byId[payload.id].alternatives);
					for (let i = 0; i < temp2.length; i++) {
						draft.entities.gradableItems.byId[temp2[i].id] = temp2[i];
						draft.entities.gradableItems.ids.push(temp2[i].id);
					}
				}

				// draft.entities[payload.entityName] = recalucateEntityPercenateges(draft.entities[payload.entityName]);
				return draft;

			case UPDATE_ENTITY:
				draft.entities[payload.entityName].byId[payload.id] = { ...draft.entities[payload.entityName].byId[payload.id], ...payload.data };

				// if (payload.data.weight) {
				//     draft.entities[payload.entityName] = recalucateEntityPercenateges(draft.entities[payload.entityName]);
				// }

				return draft;

			case UPDATE_ALTERNATIVES:
				//add added alternatives to the intended items
				draft.entities.gradableItems.byId[payload.id].alternatives = draft?.entities?.gradableItems?.byId[payload.id]?.alternatives?.concat(
					payload.alternativesPayload.added_alternatives.map((al) => current(draft.entities.gradableItems.byId[al.type + "_" + al.id]))
				);

				//filter outside items from added alternatives
				draft.entities.gradableItems.ids = draft.entities.gradableItems.ids.filter(
					(id) => !payload.alternativesPayload.added_alternatives.some((aAlt) => id === aAlt.type + "_" + aAlt.id)
				);

				//remove removed alternatives from the intended item
				temp1 =
					draft?.entities?.gradableItems?.byId[payload.id]?.alternatives?.filter((alt) =>
						payload.alternativesPayload.removed_alternatives.some((rAlt) => alt.id === rAlt.type + "_" + rAlt.id)
					) ?? [];
				draft.entities.gradableItems.byId[payload.id].alternatives = draft?.entities?.gradableItems?.byId[payload.id]?.alternatives?.filter(
					(alt) => !payload.alternativesPayload.removed_alternatives.some((rAlt) => alt.id === rAlt.type + "_" + rAlt.id)
				);

				//add removed alternatives to outside items
				for (let i = 0; i < temp1.length; i++) {
					draft.entities.gradableItems.byId[temp1[i].id] = temp1[i];
					draft.entities.gradableItems.ids.push(temp1[i].id);
				}

				//update intended alternatives number
				draft.entities.gradableItems.byId[payload.id].numberOfAlternatives +=
					payload.alternativesPayload.added_alternatives.length - payload.alternativesPayload.removed_alternatives.length;

				// draft.entities[payload.entityName] = recalucateEntityPercenateges(draft.entities.gradableItems);

				return draft;

			case LOAD_GRADABLE_ITEM_STUDENT_DETAILS:
				draft.GradableItemStudentsDetails = payload;
				return draft;
			//----------------------------------Student details by hazem slider

			// case "SET_CURRENT_GROUP":
			//     draft.currentGroup=payload.group_id;

			// return draft;

			case "SET_CURRENT_ITEM": {
				Helper.cl(payload.item_type + "_" + payload?.item_id, "payload.item_type_payload?.item_id");
				draft.currentItem = payload.item_id;
				//Helper.cl(draft.entities.gradableItems.byId);
				if (typeof payload.item_id === "string" && payload.item_id.includes("_")) {
					draft.currentItem = draft?.entities?.gradableItems?.byId[payload?.item_id]; //
					// ...
				} else {
					draft.currentItem = draft?.entities?.gradableItems?.byId[payload.item_type + "_" + payload?.item_id]; //
				}

				Helper.cl(draft.currentItem, "draft.currentItem");
				//alert(JSON.stringify(s));
				draft.currentGroup = draft?.entities?.gradingGroups?.byId[draft.currentItem.groupId];
			}
			case "SET_STUDENT_ITEM_MARK": {
				if (draft.GradableItemStudentsDetails?.students?.byId?.[payload.student_id])
					draft.GradableItemStudentsDetails.students.byId[payload.student_id].calculatedFinalTeacherGrade = payload.new_grade;
			}

			default:
				return draft;
		}
	});
};

//as a draft by reference
// const recalucateEntityPercenateges = (collection , parentPercentage) => {

//     collection = /*isDraft(collection) ? current(collection) : */collection;

//     let totalWeight = collection.ids.reduce(
//         (prev, curr) => prev + collection.byId[curr].weight,
//         0,
//     );

//     totalWeight = totalWeight > 0 ? totalWeight : 1;

//     let checkIfThereAreRemainings = false;

//     collection.byId = collection.ids.map(async (gi, ind) => {
//         checkIfThereAreRemainings =
//         checkIfThereAreRemainings ||
//         !!((collection.byId[gi].weight / totalWeight) * 100)?.toString()?.split('.')?.[1];

//         gi.precentage =
//         (checkIfThereAreRemainings &&
//             ind === items.length - 1
//             ? collection.byId[gi].weight / totalWeight + 0.01
//             : collection.byId[gi].weight / totalWeight) * parentPercentage;
//             //  return gi;
//         })

//         return collection;
//     }
