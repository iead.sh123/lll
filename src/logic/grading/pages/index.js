import { pages } from "../constants/pages.constant";
import React from "react";

export const pageMapping = {
	[pages.GROUP_AND_ITEMS]: React.lazy(() => import("./groupsAndItems/GroupsAndItems")),

	[pages.STUDENT_DETAILS]: React.lazy(() => import("./StudentDetailsItemByItem/StudentDetailsItemByItem")),
	[pages.COURSE_FINAL_MARKS]: React.lazy(() => import("./courseFinalMarks/CourseFinalMarks")),
};
