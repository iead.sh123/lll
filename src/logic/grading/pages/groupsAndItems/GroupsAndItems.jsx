import { entities } from "logic/grading/constants/entities.constants";
import React from "react";
import RGradesTable from "view/grading/RGradesTable/RGradesTable";

import { Row } from "reactstrap";
import tr from "components/Global/RComs/RTranslator";
import { GradableItemTypes } from "logic/grading/constants/gradable-items-type";
import PickAlternativesModal from "./components/PickAlternativesModal";
import Helper from "components/Global/RComs/Helper";

const GroupsAndItems = ({
	handleLoadCourseGraddableItems,
	handleLoadCourseGradingGroups,
	gradingGroups,
	courseGradableItems,
	loading,
	handleDeleteGradingGroup,
	handleAddGroup,
	handleUpdateGradingGroup,
	handleAssignGradableItemToGroup,
	handleDeleteGradableItems,
	handleUpdateGradableItems,
	handleSubmitAlternatives,
	handleCreateCustomGradableItem,
	handleLoadGradableItemStudentsDetails,
}) => {
	const loadAll = async () => {
		await handleLoadCourseGradingGroups();
		await handleLoadCourseGraddableItems();
	};

	const [editedEntityId, setEditedId] = React.useState(null);
	const [loadingEdit, setLoadingEdit] = React.useState(false);
	const [alternativeModalSelectedId, setAlternativeModalSelectedId] = React.useState(null);
	const [collapsedAlternatives, setCollapsedAlternatives] = React.useState([]);

	const updateGradingGroup = async (data) => {
		if (editedEntityId) {
			setLoadingEdit(true);
			await handleUpdateGradingGroup(editedEntityId, data);
			setEditedId(null);
			setLoadingEdit(false);
		}
	};

	const updateGraddableItem = async (data) => {
		Helper.cl(data, "updateGraddableItem data");

		if (editedEntityId) {
			setLoadingEdit(true);
			await handleUpdateGradableItems(editedEntityId, data);
			setEditedId(null);
			setLoadingEdit(false);
		}
	};

	const onRecordClick = (columnName, rec) => {
		if (columnName === "numberOfAlternatives") {
			if (!collapsedAlternatives.some((al) => al === rec.id)) {
				setCollapsedAlternatives([...collapsedAlternatives, rec.id]);
				return;
			}
			setCollapsedAlternatives(collapsedAlternatives.filter((ca) => ca !== rec.id));
		} else if (columnName === "numberOFGradedStudents") {
			const [type, id] = rec.id.split("_");
			handleLoadGradableItemStudentsDetails(type, +id);
		}
	};

	React.useEffect(() => {
		loadAll();
	}, []);

	return (
		<React.Fragment>
			{Helper.js(gradingGroups, "gradable gradingGroups")}
			<RGradesTable
				keyPrefix={entities.gradingGroups}
				headers={gradingGroups.headers}
				dataCollection={gradingGroups}
				title={tr("Grading Groups")}
				loading={loading.includes(entities.gradingGroups)}
				deriveActions={(_) => [
					{
						name: tr("delete"),
						icon: "fas fa-trash-alt",
						color: "danger",
						//   outline
						onClick: (r) => handleDeleteGradingGroup(r.id),
					},
					{
						name: tr("edit"),
						icon: loadingEdit ? "fa fa-spinner" : "fa fa-pen",
						color: "primary",
						//   outline
						onClick: (r) => (loadingEdit ? null : setEditedId(r.id)),
					},
				]}
				handleAddNew={handleAddGroup}
				handleUpdate={updateGradingGroup}
				editedId={editedEntityId}
				loadingEdit={loadingEdit}
			/>
			{Helper.js(courseGradableItems, "gradable Items")}

			<RGradesTable
				keyPrefix={entities.gradableItems}
				headers={[
					{
						type: "select",
						label: "Group",
						value: "groupName",
						options: Object.values(gradingGroups.byId).map((r) => ({ value: r.id, label: r.name })),
						inForm: true,
					},
					...courseGradableItems.headers.filter((h) => h.value !== "groupName"),
				]}
				title={tr("Gradable Items")}
				collapsedChildren={collapsedAlternatives.reduce(
					(prev, curr) => ({
						...prev,
						[curr]: courseGradableItems.byId[curr].alternatives.map((a) => ({
							...a,
							groupName: {
								icon: "fas fa-file-alt",
								value: "Alternative :",
								color: "#F58B1F",
							},
							actions: [
								{
									name: tr("delete"),
									icon: "fas fa-trash-alt",
									color: "danger",
									outline: true,
									onClick: (r, parent) => {
										handleSubmitAlternatives(parent.id, {
											removed_alternatives: [{ type: r.type, id: r.id?.split?.("_")?.length > 1 ? +r.id.split("_")[1] : +r?.id }],
										});
										// setCollapsedAlternatives(collapsedAlternatives.filter(ca=>ca !== r.id));
									},
								},
							],
						})),
					}),
					{}
				)}
				deriveActions={(rec) => [
					...(rec?.type === GradableItemTypes.Custom
						? [
								{
									name: tr("delete"),
									icon: "fas fa-trash-alt",
									color: "danger",
									//   outline
									onClick: (r) => handleDeleteGradableItems(r.id), //handleDeleteGradingGroup(r.id)
								},
						  ]
						: []),
					{
						name: tr("edit"),
						icon: loadingEdit ? "fa fa-spinner" : "fa fa-pen",
						color: "primary",
						//   outline
						onClick: (r) => (loadingEdit ? null : setEditedId(r.id)),
					},
					{
						name: tr("add alternative"),
						icon: "fas fa-file-alt",
						color: "primary",
						//   outline
						onClick: (r) => setAlternativeModalSelectedId(r.id),
					},
				]}
				loading={loading.includes(entities.gradableItems)}
				editedId={editedEntityId}
				loadingEdit={loadingEdit}
				disabledFieldsForEdit={
					editedEntityId?.split?.("_")?.[0] && editedEntityId?.split?.("_")?.[0] !== GradableItemTypes.Custom ? ["name"] : []
				}
				dataCollection={courseGradableItems}
				handleUpdate={updateGraddableItem}
				handleAddNew={handleCreateCustomGradableItem}
				onRecordClick={onRecordClick}
			/>
			<PickAlternativesModal
				isOpen={!!alternativeModalSelectedId}
				toggle={() => setAlternativeModalSelectedId(null)}
				gradableItemsCollection={courseGradableItems}
				itemCompositeId={alternativeModalSelectedId}
				handleSubmitAlternatives={handleSubmitAlternatives}
			/>
		</React.Fragment>
	);
};

export default GroupsAndItems;
