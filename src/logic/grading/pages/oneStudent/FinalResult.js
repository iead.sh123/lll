import react, { useState } from "react"
import { Row,Col } from "reactstrap";
import './RGrading.css'


function FinalResult({Mark,GPA,letterMark,result}){
   
  
    return(
      
     <Row className="final_result_row">
           <Col xs lg="3" className="final_result">Final Resalt</Col>
           <Col xs lg="1.5">
             <Row  className="final_result">Mark</Row>
             <Row className= "final_result_row2">{Mark}</Row>
           </Col>
           <Col xs lg="1.5">
             <Row className="final_result">GPA</Row>
             <Row className= "final_result_row2">{GPA}</Row>
             </Col>
           <Col xs lg="1.5">
            <Row  className="final_result">letter Mark</Row>
             <Row className= "final_result_row2">{letterMark}</Row>
             </Col>
           <Col xs lg="1.5">
             <Row  className="final_result">Pass\Fall</Row>
             <Row  className= "final_result_row2  " >
                <span className="result" style={{backgroundColor:result=="Fail"?"#E95959":"green",}}>
                   {result}
                   </span></Row>
           </Col>
           <Col xs lg="3" className="final_result">
           <button  className='Recalculate_but'><i  className= "fa fa-calculator"></i>Recalculate</button>
           </Col>
           </Row>

   
    )

}

export default FinalResult;


