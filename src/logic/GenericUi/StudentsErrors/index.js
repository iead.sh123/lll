import React from "react";
import RButton from "components/Global/RComs/RButton";
import Loader from "utils/Loader";
import tr from "components/Global/RComs/RTranslator";
import { useSelector, useDispatch } from "react-redux";
import { sendToParentApp } from "store/actions/admin/studentsErrors.Action";
import { useParams } from "react-router-dom";
import { Row, Col } from "reactstrap";

const StudentsErrors = ({ setShowStudents }) => {
  const dispatch = useDispatch();
  const { entity_specific_id } = useParams();
  const { studentsErrors, studentsErrorsLoading } = useSelector(
    (state) => state.StudentsErrorsRed
  );

  return (
    <>
      {studentsErrorsLoading ? (
        <Loader />
      ) : (
        <Row>
          <Col xs={12}>
            <h5 style={{ textAlign: "center" }}>{studentsErrors.msg}</h5>
          </Col>
          <Col xs={12}>
            <ul>
              {studentsErrors?.students?.map((el) => (
                <li style={{ textAlign: "center", listStyle: "none" }}>{el}</li>
              ))}
            </ul>
          </Col>
          <Col xs={12} style={{ display: "flex", justifyContent: "end" }}>
            <RButton
              text={tr`cancel`}
              onClick={() => setShowStudents(false)}
              outline
              color="primary"
            />
            <RButton
              text={tr`Publish`}
              onClick={() =>
                dispatch(sendToParentApp(entity_specific_id, setShowStudents))
              }
              color="primary"
            />
          </Col>
        </Row>
      )}
    </>
  );
};

export default StudentsErrors;
