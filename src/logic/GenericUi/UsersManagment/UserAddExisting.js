import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchUserFromUserAsync } from "store/actions/admin/manageSchool.Action";
import { Row, Col, Button } from "reactstrap";
import RLister from "components/Global/RComs/RLister";
import Loader from "utils/Loader";
import {
	checkUser,
	saveUsersFromUserAsync,
	fetchAcceptedUserTypesAsync,
	getAvailableUsersAsync,
	saveAvailableUsersAsync,
	emptyAvailableUsersAsync,
} from "store/actions/admin/manageSchool.Action";
import NewPaginator from "components/Global/NewPaginator/NewPaginator";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";
import RSelect from "components/Global/RComs/RSelect";
import iconsFa6 from "variables/iconsFa6";
import REmptyData from "components/RComponents/REmptyData";

const UserAddExisting = ({ addExistingHandleClose, specificName }) => {
	const [users, setUsers] = useState([]);
	const [userSelected, setUserSelected] = useState({ acceptedType: null });
	const dispatch = useDispatch();
	const {
		allUsersFromUser,
		userChecked,
		FirstPageUrl,
		NextPageUrl,
		PrevPageUrl,
		LastPageUrl,
		CurrentPage,
		LastPage,
		Total,
		acceptedUserTypes,
		availableUsers,
		acceptedUserTypesLoading,
		availableUsersLoading,
		allUsersFromUserLoading,
		saveUsersFromUserLoading,
		saveAvailableUsersLoading,
	} = useSelector((state) => state.manageSchoolRed);

	useEffect(() => {
		// dispatch(fetchUserFromUserAsync(specificName, null));
		dispatch(fetchAcceptedUserTypesAsync(specificName));
	}, []);

	useEffect(() => {
		if (acceptedUserTypes) {
			const AllUsers = [];
			acceptedUserTypes.map((el, index) => AllUsers.push({ label: el, value: index }));
			setUsers(AllUsers);
		}
	}, [acceptedUserTypes]);

	useEffect(() => {
		dispatch(emptyAvailableUsersAsync());
	}, []);

	const handleSelectUsers = (event) => {
		const ArraySelected = event;
		let ArraySelectedTwo = [];
		for (let i = 0; i < ArraySelected.length; i++) {
			ArraySelectedTwo.push(ArraySelected[i].label);
		}

		setUserSelected({
			...userSelected,
			acceptedType: ArraySelectedTwo,
		});
	};

	const filterUser = () => {
		dispatch(getAvailableUsersAsync(specificName, { payload: userSelected }, null));
	};

	const _records = React.useMemo(
		() =>
			availableUsers?.map((rc) => ({
				title: rc?.label,
				details: [{ key: "Name", value: rc?.label }],
				actions: [
					{
						name: "check",
						icon: rc.check == true ? iconsFa6.delete : "fa fa-plus",
						color: rc.check == true && rc.color ? rc.color : "primary",
						onClick: () => dispatch(checkUser(rc.value)),
					},
				],
			})),
		[availableUsers]
	);

	const handleSave = (e) => {
		e.preventDefault();
		dispatch(saveAvailableUsersAsync(specificName, { payload: { items: userChecked } }, addExistingHandleClose));
	};
	return (
		<div className="content">
			{acceptedUserTypesLoading ? (
				<Loader />
			) : (
				<>
					<Row>
						<Col xs={4}>
							<RSelect
								option={users}
								closeMenuOnSelect={true}
								placeholder={tr`select_user_type`}
								isMulti
								onChange={(e) => {
									handleSelectUsers(e);
								}}
							/>
						</Col>

						<Col xs={4}>
							<RButton
								style={{ position: "relative", top: "-4px" }}
								text={tr`Go`}
								onClick={filterUser}
								color="primary"
								disabled={userSelected?.acceptedType?.length == 0 ? true : false}
							/>
						</Col>
					</Row>

					{availableUsersLoading ? (
						<Loader />
					) : availableUsers?.length !== 0 ? (
						<Row>
							<Col xs={12}>
								<RLister Records={_records} />
							</Col>

							<Col xs={12}>
								<NewPaginator
									firstPageUrl={FirstPageUrl}
									lastPageUrl={LastPageUrl}
									currentPage={CurrentPage}
									lastPage={LastPage}
									prevPageUrl={PrevPageUrl}
									nextPageUrl={NextPageUrl}
									total={Total}
									userSelected={userSelected}
									getAction={getAvailableUsersAsync}
								/>
							</Col>

							<Col xs={12} style={{ display: "flex", justifyContent: "flex-end" }}>
								<Button
									block
									className="btn-round ml-4 mb-2 mt-1 "
									color="primary"
									type="submit"
									disabled={userChecked.length == 0 ? true : false}
									onClick={(e) => handleSave(e)}
									style={{ width: 100 }}
								>
									{saveAvailableUsersLoading ? (
										<>
											{tr`save`} <i className="fa fa-refresh fa-spin"></i>
										</>
									) : (
										tr("save")
									)}
								</Button>

								<RButton text={tr`cancel`} onClick={addExistingHandleClose} color="primary" outline />
							</Col>
						</Row>
					) : (
						<REmptyData line1={tr`please_select_user_type_first`} />
					)}
				</>
			)}
		</div>
	);
};

export default UserAddExisting;
