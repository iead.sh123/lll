import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
	getHomePagePostsAsync,
	initialDataToCreatePost,
	getValidationCreatePost,
	setCreatePostValues,
	setValidationToPost,
	addPostAsync,
	removePostAsync,
	editPostAsync,
	searchInPostAsync,
	getAllCategoriesAsync,
	emptyPostsArray,
	setReportingOnPost,
	saveReportingOnPostAsync,
	emptyReportingOnPost,
	getPostsCreatedBySpecificUserAsync,
	addLikeOnPostOrCommentAsync,
	emptyPeopleLikes,
	getAllMandatoryPostsAsync,
	setCreateCommentValues,
	addCommentAsync,
	updateCommentAsync,
	removeCommentAsync,
	loadPreviousCommentsAsync,
	changeFlagSpecificContentLoad,
} from "store/actions/global/discussions.actions";
import { validationCreatePostSchema } from "./CreatePost/GCreatePostValidation";
import { deleteSweetAlert } from "components/Global/RComs/RAlert";
import { useParams, useHistory } from "react-router-dom";
import RDiscussionHeader from "view/Discussion/RDiscussionHeader";
import InfiniteScroll from "react-infinite-scroll-component";
import RDiscussion from "view/Discussion/RDiscussion";
import GCreatePost from "./CreatePost/GCreatePost";
import Loader from "utils/Loader";
import tr from "components/Global/RComs/RTranslator";
import * as yup from "yup";
import GReportPost from "./ReportPost/GReportPost";

import GShowAllPeople from "./ShowAllPeople/GShowAllPeople";
import GContentOfThePost from "./ContentOfThePost/GContentOfThePost";
import GAllUsersInMandatory from "./AllUsersInMandatory/GAllUsersInMandatory";
import GSeeMoreContent from "./SeeMoreContent/GSeeMoreContent";
import REmptyData from "components/RComponents/REmptyData";

export const DiscussionContext = React.createContext();

const GDiscussion = () => {
	const dispatch = useDispatch();
	const history = useHistory();
	const { sectionId, cohortId, userId, searchQuery, mandatoryFlag } = useParams();
	const [seeTheContentOfThePost, setSeeTheContentOfThePost] = useState(false);
	const [seeAllUsersInMandatory, setSeeAllUsersInMandatory] = useState(false);
	const [openedReplayCollapses, setOpenedReplayCollapses] = useState(null);
	const [seeMoreContent, setSeeMoreContent] = useState(false);
	const [openedCollapses, setOpenedCollapses] = useState(null);
	const [showAllPeople, setShowAllPeople] = useState(false);
	const [lastCommentId, setLastCommentId] = useState(false);
	const [textToSearch, setTextToSearch] = useState(null);
	const [reportModal, setReportModal] = useState(false);
	const [typeContent, setTypeContent] = useState(false);
	const [createPost, setCreatePost] = useState(false);
	const [contentId, setContentId] = useState(null);
	const [postId, setPostId] = useState(null);
	const [alert, setAlert] = useState(false);
	const [editableCommentText, setEditableCommentText] = useState(false);
	const [editableReplayText, setEditableReplayText] = useState(false);
	const [specificPost, setSpecificPost] = useState(false);
	const [specificContent, setSpecificContent] = useState(false);
	const [commentId, setCommentId] = useState(false);

	const {
		posts,
		mostReactedPost,
		mandatoryPosts,
		loadMorePage,
		createPost: cPost,
		reportingOnPost,
		createPostValidation,
		categories,
		allReportCategories,
		addComment,
		previousComments,
		loadMoreComment,
		test,
		createComment,

		addPostLoading,
		getPostByIdLoading,
		postsLoading,
		categoriesLoading,
		searchLoading,
		allReportCategoriesLoading,
		saveReportOnContentLoading,
		reportingOnPostLoading,
		addLikeLoading,
		addCommentLoading,
		updateCommentLoading,
		removeCommentLoading,
		loadPreviousLoading,
		specificContentLoad,
		getCommentByIdLoading,
	} = useSelector((state) => state.discussionsRed);

	const handleOpenAddPost = () => {
		setCreatePost(true);
		if (cohortId) {
			dispatch(setCreatePostValues("cohort_id", cohortId));
			dispatch(setCreatePostValues("for_community", false));
		}
	};
	const handleCloseAddPost = () => {
		setCreatePost(false);
		dispatch(initialDataToCreatePost());
		setPostId(null);
	};

	const handleOpenAddReport = () => setReportModal(true);
	const handleCloseAddReport = () => {
		setReportModal(false);
		setPostId(null);
		dispatch(emptyReportingOnPost());
	};

	const handleOpenSeeAllPeople = () => setShowAllPeople(true);
	const handleCloseSeeAllPeople = () => {
		setShowAllPeople(false);
		setContentId(null);
		dispatch(emptyPeopleLikes());
	};

	const handleOpenSeeTheContentOfThePost = () => setSeeTheContentOfThePost(true);

	const handleCloseSeeTheContentOfThePost = () => {
		setSeeTheContentOfThePost(false);
		setSpecificPost(false);
		setPostId(null);
	};

	const handleOpenSeeAllUsersInMandatory = () => setSeeAllUsersInMandatory(true);
	const handleCloseSeeAllUsersInMandatory = () => {
		setSeeAllUsersInMandatory(false);
		setPostId(null);
	};

	const handleOpenSeeMoreContent = () => setSeeMoreContent(true);
	const handleCloseSeeMoreContent = () => {
		setSeeMoreContent(false);
		setSpecificContent(false);

		dispatch(changeFlagSpecificContentLoad(false));
	};

	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);

	useEffect(() => {
		if (searchQuery) {
			dispatch(searchInPostAsync(searchQuery, 1, cohortId));
		} else if (userId) {
			dispatch(getPostsCreatedBySpecificUserAsync(userId, 1, cohortId));
		} else if (mandatoryFlag && cohortId) {
			dispatch(getAllMandatoryPostsAsync(cohortId, 1));
		} else {
			dispatch(getHomePagePostsAsync(1, cohortId));
		}
		dispatch(emptyPostsArray());
		dispatch(initialDataToCreatePost(cohortId));
		dispatch(getValidationCreatePost());
		dispatch(getAllCategoriesAsync(cohortId));
	}, [searchQuery, userId, cohortId, mandatoryFlag]);

	const fetchData = () => {
		loadMorePage !== null && searchQuery
			? dispatch(searchInPostAsync(searchQuery, loadMorePage, cohortId))
			: userId
			? dispatch(getPostsCreatedBySpecificUserAsync(userId, loadMorePage, cohortId))
			: mandatoryFlag && cohortId
			? dispatch(getAllMandatoryPostsAsync(cohortId, loadMorePage))
			: dispatch(getHomePagePostsAsync(loadMorePage, cohortId));
	};

	const handleChangeCreatePost = (name, value) => {
		dispatch(setCreatePostValues(name, value));
		if (name == "text" || name == "cohort_id") {
			yup
				.reach(validationCreatePostSchema, name)
				.validate(value)
				.then(() => {
					dispatch(
						setValidationToPost({
							name: name,
							message: undefined,
						})
					);
				})
				.catch((err) => {
					dispatch(
						setValidationToPost({
							name: name,
							message: err.errors[0],
						})
					);
				});
		}
	};

	const handleCreatePost = () => {
		validationCreatePostSchema
			.validate(cPost, { abortEarly: false })
			.then(() => {
				dispatch(addPostAsync(cPost, handleCloseAddPost, cohortId, searchQuery, history, sectionId));
			})
			.catch((err) => {
				const validationErrors = {};
				err.inner.forEach((error) => {
					validationErrors[error.path] = error.message;
				});

				dispatch(
					setValidationToPost({
						name: undefined,
						message: validationErrors,
					})
				);
			});
	};

	const handleUpdatePost = () => {
		dispatch(
			editPostAsync(cPost, postId, handleCloseAddPost, cohortId, searchQuery, history, sectionId, handleCloseSeeTheContentOfThePost)
		);
	};

	const successDelete = (prameters) => {
		if (prameters.type == "post") {
			dispatch(removePostAsync(prameters.postId, hideAlert, cohortId, searchQuery, history, handleCloseSeeTheContentOfThePost));
		} else {
			dispatch(
				removeCommentAsync(
					prameters.commentId,
					hideAlert,
					cohortId,
					searchQuery,
					history,
					specificPost,
					postId,
					specificContent,
					typeContent,
					contentId
				)
			);
		}
	};

	const handleDeleteItem = (postId) => {
		const prameters = {
			postId,
			type: "post",
		};
		const message = tr`Are you sure`;
		const confirm = tr`Yes, delete it`;
		deleteSweetAlert(showAlerts, hideAlert, successDelete, prameters, message, confirm);
	};

	const handleSearch = (text) => {
		// dispatch(searchInPostAsync(text, loadMorePage));
	};

	const handleChangeAddReport = (name, value) => {
		dispatch(setReportingOnPost(name, value));
	};

	const handleSaveReportingOnPost = () => {
		dispatch(saveReportingOnPostAsync({ ...reportingOnPost, post_id: postId }, contentId, handleCloseAddReport));
	};

	const handleAddLike = ({ content_id, type, post_id, comment_id, typeContent, postType }) => {
		const data = {
			content_id,
			type,
			post_id,
			comment_id,
			typeContent,
			postType,
		};
		dispatch(addLikeOnPostOrCommentAsync(data));
	};

	const handleCommentCollapsesToggle = (id) => {
		const isOpen = "collapse" + id;
		if (isOpen == openedCollapses) {
			setOpenedCollapses("collapse");
		} else {
			setOpenedCollapses("collapse" + id);
		}
	};

	const handleReplayCollapsesToggle = (id) => {
		const isOpenReplay = "collapse" + id;
		if (isOpenReplay == openedReplayCollapses) {
			setOpenedReplayCollapses("collapse");
		} else {
			setOpenedReplayCollapses("collapse" + id);
		}
	};

	const handleEditableCommentText = (id) => {
		const isOpenEditable = "text" + id;
		if (isOpenEditable == editableCommentText) {
			setEditableCommentText("text");
		} else {
			setEditableCommentText("text" + id);
		}
	};

	const handleEditableReplayText = (id) => {
		const isOpenEditableReplay = "text" + id;
		if (isOpenEditableReplay == editableReplayText) {
			setEditableReplayText("text");
		} else {
			setEditableReplayText("text" + id);
		}
	};

	const handleChangeCreateComment = (name, value) => {
		dispatch(setCreateCommentValues(name, value));
	};

	const handleSaveComment = () => {
		dispatch(addCommentAsync(addComment, cohortId, specificPost, postId, specificContent, typeContent, contentId));
	};

	const handleEditComment = () => {
		dispatch(
			updateCommentAsync(addComment.comment_id, addComment, cohortId, specificPost, postId, specificContent, typeContent, contentId)
		);
	};

	const handleRemoveComment = (commentId) => {
		const prameters = {
			commentId,
			type: "comment",
		};
		const message = tr`Are you sure`;
		const confirm = tr`Yes, delete it`;
		deleteSweetAlert(showAlerts, hideAlert, successDelete, prameters, message, confirm);
	};

	const handleLoadPreviousComments = (contentId, lastCommentId, typeContent, typeSeeMore) => {
		dispatch(
			loadPreviousCommentsAsync({
				contentId: contentId,
				nextId: lastCommentId,
				type: typeContent,
				specificPost: specificPost,
				specificContent: false,
				typeSeeMore: typeSeeMore,
			})
		);
	};

	return (
		<React.Fragment>
			{alert}
			<DiscussionContext.Provider
				value={{
					posts,
					mostReactedPost,
					mandatoryPosts,
					reportingOnPost,
					cPost,
					categories,
					allReportCategories,
					createPostValidation,
					createComment,

					handleOpenAddPost,
					handleCloseAddPost,
					handleOpenAddReport,
					handleCloseAddReport,
					handleChangeCreatePost,
					handleSaveReportingOnPost,
					handleChangeAddReport,
					handleCreatePost,
					handleDeleteItem,
					handleUpdatePost,
					handleSearch,
					handleAddLike,
					handleOpenSeeAllPeople,
					handleCloseSeeAllPeople,
					handleOpenSeeTheContentOfThePost,
					handleCloseSeeTheContentOfThePost,
					handleOpenSeeAllUsersInMandatory,
					handleCloseSeeAllUsersInMandatory,
					handleCommentCollapsesToggle,
					handleChangeCreateComment,
					handleSaveComment,
					handleRemoveComment,
					handleOpenSeeMoreContent,
					handleCloseSeeMoreContent,
					handleReplayCollapsesToggle,
					handleEditableCommentText,
					handleEditableCommentText,
					handleEditableReplayText,
					handleEditComment,
					handleLoadPreviousComments,

					addPostLoading,
					getPostByIdLoading,
					postsLoading,
					categoriesLoading,
					searchLoading,
					allReportCategoriesLoading,
					saveReportOnContentLoading,
					reportingOnPostLoading,
					addLikeLoading,
					addCommentLoading,
					updateCommentLoading,
					removeCommentLoading,
					loadPreviousLoading,
					getCommentByIdLoading,

					setCommentId,
					setSpecificPost,
					setSpecificContent,

					setTextToSearch,
					setPostId,
					setContentId,
					setLastCommentId,
					setTypeContent,
					typeContent,
					lastCommentId,
					textToSearch,
					reportModal,
					createPost,
					postId,
					contentId,
					showAllPeople,
					seeTheContentOfThePost,
					seeAllUsersInMandatory,
					openedCollapses,
					openedReplayCollapses,
					seeMoreContent,
					previousComments,
					loadMoreComment,
					editableCommentText,
					editableReplayText,
					specificPost,
					specificContent,
					specificContentLoad,
					commentId,
				}}
			>
				{createPost && <GCreatePost />}
				{reportModal && <GReportPost />}
				{showAllPeople && <GShowAllPeople />}
				{seeTheContentOfThePost && <GContentOfThePost />}
				{seeAllUsersInMandatory && <GAllUsersInMandatory />}
				{seeMoreContent && <GSeeMoreContent />}

				<RDiscussionHeader />

				{posts?.length > 0 ? (
					<div id="scrollableDiv2" style={{ height: "65vh", overflow: "auto" }}>
						<InfiniteScroll
							dataLength={posts?.length}
							next={fetchData}
							hasMore={loadMorePage !== null ? true : false}
							loader={loadMorePage !== null && <Loader />}
							style={{ overflow: "hidden" }}
							scrollableTarget="scrollableDiv2"
						>
							<RDiscussion />
						</InfiniteScroll>
					</div>
				) : postsLoading ? (
					<Loader />
				) : (
					<REmptyData width="350px" height="350px" />
				)}
			</DiscussionContext.Provider>
		</React.Fragment>
	);
};

export default GDiscussion;
