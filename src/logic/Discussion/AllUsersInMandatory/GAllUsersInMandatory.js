import React, { useState, useContext } from "react";
import Swal, { DANGER } from "utils/Alert";
import { DiscussionContext } from "logic/Discussion/GDiscussion";
import { Services } from "engine/services";
import { get } from "config/api";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import AppModal from "components/Global/ModalCustomize/AppModal";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

const GAllUsersInMandatory = () => {
	const DiscussionData = useContext(DiscussionContext);
	const [processedRecords, setProcessedRecords] = useState([]);
	const [totalUsers, setTotalUsers] = useState(0);
	const [usersDoesNotComplete, setUsersDoesNotComplete] = useState(0);

	const getDataFromBackend = async () => {
		const url = `${Services.discussion.backend}api/post/mandatory/${DiscussionData.postId}/details`;

		let response = await get(url);

		if (response.data.status == 1) {
			return response;
		} else {
			toast.error(response.data.msg);
		}
	};

	const setTableData = (response) => {
		if (!response) {
			return [];
		}

		const data = response?.data?.data?.user_details?.data.map((r) => ({
			id: r?.id,
			table_name: "allUsersInMandatory",

			details: [
				{
					key: tr`full_name`,
					value: r?.full_name,
				},
				{
					key: tr`comment`,
					value: `${r?.comment_added ? tr`done` : tr`un_done`}`,
					color: `${r?.comment_added ? "green" : "black"}`,
				},
				{
					key: tr`replies`,
					value: `${r?.replies_count}/${response?.data?.data?.mandatory_replies} answered`,
					color: `${r?.replies_count == response?.data?.data?.mandatory_replies ? "green" : "black"}`,
				},
			],
		}));
		setTotalUsers(response?.data?.data?.total_user);

		setUsersDoesNotComplete(response?.data?.data?.users_who_does_not_complete_the_condition);
		setProcessedRecords(response);

		return data;
	};
	const propsLiterals = React.useMemo(
		() => ({
			listerProps: {
				records: setTableData(processedRecords) || [],
				getDataFromBackend: getDataFromBackend,
				setData: setTableData,
				getDataObject: (response) => response.data.data.user_details,
			},
		}),
		[processedRecords]
	);
	return (
		<AppModal
			show={DiscussionData.seeAllUsersInMandatory}
			parentHandleClose={DiscussionData.handleCloseSeeAllUsersInMandatory}
			header={
				usersDoesNotComplete &&
				totalUsers && (
					<RFlex styleProps={{ alignItems: "center" }}>
						<i className="fa fa-user fa-lg"></i>
						<p style={{ position: "relative", top: "10px" }}>
							{usersDoesNotComplete}/{totalUsers}
							{tr`did not complete their tasks`}
						</p>
					</RFlex>
				)
			}
			size={"xl"}
			headerSort={<RAdvancedLister {...propsLiterals.listerProps} />}
		/>
	);
};

export default GAllUsersInMandatory;
