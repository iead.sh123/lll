import React, { useState, useEffect } from "react";
import { coursesManagerApi } from "api/global/coursesManager";
import { deleteSweetAlert } from "components/Global/RComs/RAlert2";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { boldGreyColor } from "config/constants";
import { useMutateData } from "hocs/useMutateData";
import { paymentApi } from "api/global/payment";
import { useFormik } from "formik";
import RDiscountDetails from "view/Organization/PaymentPolicy/Discount/RDiscountDetails";
import GAddCourses from "./GAddCourses";
import AppModal from "components/Global/ModalCustomize/AppModal";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";

const GDiscountDetails = ({ discountId, discountDetails, handleEmptyRightSideAfterDeActivate, generalDiscountData }) => {
	const [alert, setAlert] = useState(false);
	const [openCourse, setOpenCourse] = useState(false);
	const [showActions, setShowActions] = useState(null);

	const initialValues = {
		allCourses: [],
		sort: "",
		discountDetails: true,
	};

	const { values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue, resetForm } = useFormik({
		initialValues,
	});

	// To Empty Selected Data When Click On Select To Remove Courses
	useEffect(() => {
		if (discountDetails?.id) {
			setFieldValue("allCourses", []);
			setShowActions(null);
		}
	}, [discountDetails?.id]);

	// --------------- Start Queries ---------------

	const coursesByDiscountIdData = useFetchDataRQ({
		queryKey: ["coursesByDiscountId", discountDetails?.id, values.sort],
		queryFn: () => coursesManagerApi.coursesByDiscountId(discountDetails?.id, values.sort),
	});

	const discountDeactivateMutate = useMutateData({
		queryFn: () =>
			paymentApi.discountDeactivate({
				discountId: discountDetails?.id,
				discountType: discountDetails?.discountType,
			}),
		invalidateKeys: [["discountsList"], ["coursesByDiscountId", discountId], ["checkGeneralDiscount"]],
		multipleKeys: true,
		closeDialog: () => setAlert(false),
		onSuccessFn: () => {
			handleEmptyRightSideAfterDeActivate();
		},
	});

	const deleteDiscountFromProductsLoading = useMutateData({
		queryFn: (data) => paymentApi.deleteDiscountFromProducts(data),
		invalidateKeys: ["coursesByDiscountId", discountDetails?.id, values.sort],
		onSuccessFn: () => {
			setFieldValue("allCourses", []);
		},
	});

	// --------------- End Queries ---------------

	// --------------- Start Actions Modals ---------------

	const handleOpenCourseModal = () => {
		setOpenCourse(true);
	};
	const handleCloseCourseModal = () => {
		setOpenCourse(false);
	};

	const hideAlert = () => setAlert(null);

	const showAlerts = (child) => setAlert(child);

	// --------------- End Actions Modals ---------------

	const successDelete = (prameters) => {
		discountDeactivateMutate.mutate({});
	};

	const handleDeleteCourses = (discountId) => {
		deleteDiscountFromProductsLoading.mutate({
			discountId,
			data: {
				products: values?.allCourses,
				productType: coursesByDiscountIdData?.data?.data?.data[0]?.productType,
			},
		});
	};

	const handleSortBy = (data) => {
		setFieldValue("sort", data);
	};
	const handleDeactivateDiscount = () => {
		const prameters = {
			discountId,
		};
		const message = (
			<RFlex styleProps={{ flexDirection: "column", gap: 15 }}>
				<h6>
					{discountDetails.value}% {tr`will_be_no_longer_useable`}
				</h6>
				<span style={{ color: boldGreyColor }}>{tr`this_can't_be_undone,_are_you_sure`}?</span>
			</RFlex>
		);
		const confirm = tr`deactivate`;
		deleteSweetAlert(showAlerts, hideAlert, successDelete, prameters, message, confirm);
	};

	const handleShowActions = (discountId) => {
		setShowActions(discountId);
	};

	const handleHideActions = () => {
		setShowActions(null);
	};

	const handleSelectedAllCourse = () => {
		const allCourses = coursesByDiscountIdData?.data?.data?.data?.map((course) => course?.id);
		setFieldValue("allCourses", allCourses);
	};

	const handleClearSelectedCourses = () => {
		setFieldValue("allCourses", []);
		handleHideActions();
	};

	const handleSelectCourses = (courseId) => {
		let updatedArray = [...values.allCourses];

		const courseIndex = updatedArray.indexOf(courseId);
		if (courseIndex == -1) {
			updatedArray.push(courseId);
		} else {
			updatedArray = updatedArray.filter((cId) => cId !== courseId);
		}
		setFieldValue("allCourses", updatedArray);
	};

	if (coursesByDiscountIdData.isLoading) return <Loader />;
	return (
		<>
			{alert}
			<AppModal
				size={"lg"}
				show={openCourse}
				parentHandleClose={handleCloseCourseModal}
				headerSort={<GAddCourses handleCloseCourseModal={handleCloseCourseModal} discountId={discountDetails?.id} />}
			/>
			<RDiscountDetails
				discountDetails={discountDetails}
				generalDiscountData={generalDiscountData}
				coursesByDiscountId={{ data: coursesByDiscountIdData?.data?.data?.data, loading: coursesByDiscountIdData.isLoading }}
				handlers={{
					handleDeactivateDiscount: handleDeactivateDiscount,
					handleOpenCourseModal: handleOpenCourseModal,
					handleShowActions: handleShowActions,
					handleHideActions: handleHideActions,
					handleSelectedAllCourse: handleSelectedAllCourse,
					handleClearSelectedCourses: handleClearSelectedCourses,
					handleSortBy: handleSortBy,
					handleSelectCourses: handleSelectCourses,
					handleDeleteCourses: handleDeleteCourses,
				}}
				states={{ values: values, showActions: showActions }}
				loading={{
					deleteDiscountFromProductsLoading: deleteDiscountFromProductsLoading.isLoading,
				}}
			/>
		</>
	);
};

export default GDiscountDetails;
