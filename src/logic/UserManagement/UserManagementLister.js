import React, { useState, useEffect } from "react";
import filterReducer, { filterState } from "components/Global/RComs/filterState/filterReducer";
import Swal, { DANGER } from "utils/Alert";
import { Form, FormGroup, Input } from "reactstrap";
import { baseURL, genericPath } from "engine/config";
import { post, get, destroy } from "config/api";
import { deleteSweetAlert } from "components/Global/RComs/RAlert";
import { resetFilters } from "store/actions/admin/genericFilter.Action";
import { primaryColor } from "config/constants";
import { useHistory } from "react-router-dom";
import { useParams } from "react-router-dom";
import { Services } from "engine/services";
import UserManagementAddFromExisting from "./UserManagementAddFromExisting";
import useWindowDimensions from "components/Global/useWindowDimensions";
import UserManagementAdd from "./UserManagementAdd";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import UserAvatar from "assets/img/new/svg/user_avatar.svg";
import AppModal from "components/Global/ModalCustomize/AppModal";
import RButton from "components/Global/RComs/RButton";
import Loader from "utils/Loader";
import styles from "./UserManagement.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import iconsFa6 from "variables/iconsFa6";
import { toast } from "react-toastify";

const UserManagementLister = ({ user_type, fetchDataAfterAdded = false }) => {
	const history = useHistory();
	const { tabTitle } = useParams();
	const [processedRecords, setProcessedRecords] = useState([]);
	const [deActiveLoading, setDeActiveLoading] = useState(false);
	const [hasImportFile, setHasImportFile] = useState(false);
	const [showExcelPage, setShowExcelPage] = useState(false);
	const [importExcel, setImportExcel] = useState(null);
	const [filterField, setFilterField] = useState(null);
	const [filterValue, setFilterValue] = useState(null);
	const [table_props, setTableProps] = useState([]);
	const [hasAddUser, setHasAddUser] = useState(false);
	const [loaded, setLoaded] = useState(false);
	const [userModal, setUserModal] = useState(false);
	const [alert, setAlert] = useState(false);
	const [searchData, setSearchData] = useState("");
	const [searchLoading, setSearchLoading] = useState(false);
	const [addFromExistingModal, setAddFromExistingModal] = useState(false);
	const [payloadData, setPayloadData] = useState([]);
	const [addUserFromExistingLoading, setAddUserFromExistingLoading] = useState(false);

	const { width } = useWindowDimensions();
	const mobile = width < 1000;

	const [localnewfilter, filterdispatch] = React.useReducer(filterReducer, filterState);

	const hideAlert = () => setAlert(null);
	const showAlerts = (child) => setAlert(child);
	const handleOpenAddUser = () => setUserModal(true);
	const handleCloseAddUser = () => {
		setUserModal(false);
	};

	const handleOpenAddFromExisting = () => setAddFromExistingModal(true);
	const handleCloseAddFromExisting = () => setAddFromExistingModal(false);

	const dispatch = filterdispatch;

	useEffect(() => {
		dispatch(resetFilters);
		setLoaded(true);

		return () => {
			setLoaded(false);
			dispatch(resetFilters);
		};
	}, []);

	//------------------------Advanced Lister Stuff
	const getDataFromBackend = async (specific_url) => {
		let Request = {
			payload: {
				Filters: loaded ? localnewfilter?.newFilter : [],
			},
		};

		const url = `${Services.auth_organization_management.backend}api/organization/usersForSpecificUserType/${tabTitle}`;
		let response1 = await get(specific_url ? specific_url : url);

		if (response1 && response1.data.status == 1 && response1.data && response1.data.data) {
			const data = response1.data.data;

			if (data.has_add_permission) setHasAddUser(true);
			if (data.has_import_file) setHasImportFile(true);
			if (data.table_props) setTableProps(data.table_props);

			return response1;
		} else {
			toast.error(response.data.msg);
		}
	};

	const renderInfo = ({ name, image }) => {
		return (
			<RFlex>
				<img width={30} height={30} style={{ borderRadius: "100%" }} src={image} />
				<span>{name}</span>
			</RFlex>
		);
	};
	const setData = (response) => {
		let processedRecords1 = [];

		response?.data?.data?.users?.data?.map((r, i) => {
			let record = {};
			record.id = r.id;
			record.table_name = "userManagementUiLister";

			record.details = [
				{
					key: tr`name`,
					value: {
						name: r.name,
						image: r?.hash_id !== null ? Services.auth_organization_management.file + r?.hash_id : UserAvatar,
					},
					render: renderInfo,
				},
				{ key: tr`email`, value: r.email },
				{
					key: tr`status`,
					value: r?.status ? tr`active` : tr`inactive`,
					color: r?.status ? "green" : "red",
				},
			];

			record.actions = [
				{
					name: "Manage",
					icon: "fa fa-edit",

					onClick: () => {
						history.push(`${baseURL}/${genericPath}/users-and-permissions/user-type/${r.organization_user_type_id}/${tabTitle}/properties`);
					},
				},

				// {
				//   permissions: response.data.data.has_edit_permission,
				//   name: "Edit",
				//   icon: "fa fa-pencil",
				//   color: "info",
				//   onClick: () => {
				//     history.push(
				//       `${baseURL}/${genericPath}/edit-user/${user_type}/${r.id}`
				//     );
				//   },
				// },
				// {
				//   permissions: response.data.data.has_deActive_permission,
				//   name: r.active == 1 ? "De Active" : "Active",
				//   icon: r.active ? "fa fa-check" : "fa fa-ban",
				//   color: r.active == 1 ? "success" : "info",
				//   onClick: () => handleDeActiveClicked(r.id),
				// },
				{
					// permissions: !response.data.data.has_delete_permission,
					name: "remove",
					icon: iconsFa6.delete,
					color: "info",
					onClick: () => {
						// handleDeleteClicked(r.id);
						handleRemove(r.organization_user_type_id);
					},
				},
			];

			processedRecords1.push(record);
		});
		// setImportExcel(response?.data?.data?.ok_to_upload_from_file);

		setProcessedRecords(processedRecords1);
	};

	const getFilteredData = async () => {
		const response = await getDataFromBackend();
		if (response) {
			setData(response);
		}
	};

	const handleDeActiveClicked = async (id) => {
		setDeActiveLoading(true);

		const responseDeActive = await get(
			`${Services.auth_organization_management.backend}api/organization/deActive/user/${id}/type/${user_type}/organization`
		);

		if (responseDeActive && responseDeActive.data.status == 1) {
			const response1 = await getDataFromBackend();
			setDeActiveLoading(true);
			if (response1) {
				setData(response1);
				setDeActiveLoading(false);
			}
		} else {
			toast.error(response.data.msg);
		}
	};

	//GenericSuccessHandleDelete
	const successDelete = async (prameters) => {
		const response = await destroy(
			`${Services.auth_organization_management.backend}api/organization_user/removeOrganizationUserType/${prameters.id}`
		);

		if (response) {
			if (response.data.status == 1) {
				const response1 = await getDataFromBackend();
				if (response1) {
					setData(response1);
					setAlert(null);
				} else {
					toast.error(response.data.msg);
				}
			}
		} else {
			toast.error(response.data.msg);
		}
	};

	const handleRemove = (id) => {
		const prameters = {
			id,
		};
		const message = tr`Are you sure`;
		const confirm = tr`Yes, delete it`;
		deleteSweetAlert(showAlerts, hideAlert, successDelete, prameters, message, confirm);
	};
	//GenericAddRecord
	const actionButtons = [];
	if (hasAddUser) {
		actionButtons.push({
			text: tr("add_new"),
			icon: "nc-icon nc-simple-add",
			className: "h_btn_inline_12 btn-round float-Right  ",
			onClick: () => {
				history.push(`${baseURL}/${genericPath}/add-user/${user_type}`);
			},
		});
	}

	//GenericImportExcel
	if (!hasImportFile && importExcel !== null && importExcel?.upload_from_file !== false) {
		actionButtons.push({
			text: importExcel?.button_text,
			className: "h_btn_inline_12 btn-round float-Right  ",
			onClick: () => {
				setShowExcelPage(!showExcelPage);
			},
		});
	}

	const handleSearch = async (emptySearch) => {
		let specific_url;
		specific_url = `${Services.auth_organization_management.backend}api/organization/usersForSpecificUserType/${tabTitle}?filter=${
			emptySearch ?? searchData
		}`;
		setSearchLoading(true);
		const response = await getDataFromBackend(specific_url);
		if (response) {
			if (response.data.status == 1) {
				setData(response);
				setSearchLoading(false);
			} else {
				setSearchLoading(false);
			}
		} else {
			setSearchLoading(false);

			toast.error(response.data.msg);
		}
	};

	const handleAddUserFromExisting = async () => {
		setAddUserFromExistingLoading(true);
		const response = await post(`${Services.auth_organization_management.backend}api/user/store/exists/${tabTitle}`, { data: payloadData });

		if (response) {
			if (response.data.status == 1) {
				setAddUserFromExistingLoading(false);
				handleCloseAddFromExisting();
				setPayloadData([]);
				const response1 = await getDataFromBackend();
				if (response1) {
					setData(response1);
				} else {
					toast.error(response.data.msg);
				}
			}
		} else {
			setAddUserFromExistingLoading(false);
			toast.error(response.data.msg);
		}
	};

	return (
		<div className="content">
			{alert}
			{deActiveLoading ? (
				<Loader />
			) : (
				<React.Fragment>
					<AppModal
						show={userModal}
						parentHandleClose={handleCloseAddUser}
						header={
							tabTitle == "principal"
								? tr`add_principal`
								: tabTitle == "student"
								? tr`student`
								: tabTitle == "senior_teacher"
								? "senior_teacher"
								: ""
						}
						size={"lg"}
						headerSort={
							<UserManagementAdd
								handleCloseModal={handleCloseAddUser}
								getDataFromBackend={getDataFromBackend}
								setTableData={setData}
								fetchDataAfterAdded={fetchDataAfterAdded}
							/>
						}
					/>

					<AppModal
						show={addFromExistingModal}
						parentHandleClose={handleCloseAddFromExisting}
						header={
							<RFlex>
								<span>{tr`add_from_existing`}</span>
								{addUserFromExistingLoading ? (
									<i className="fa fa-refresh fa-spin"></i>
								) : payloadData.length > 0 ? (
									<span
										className={styles.btn_hover}
										style={{
											color: primaryColor,
											textDecoration: "underline",
											cursor: "pointer",
										}}
										onClick={() => {
											handleAddUserFromExisting();
										}}
									>
										{tr`save`}
									</span>
								) : (
									""
								)}
							</RFlex>
						}
						size={"lg"}
						headerSort={
							<UserManagementAddFromExisting
								handleCloseAddFromExisting={handleCloseAddFromExisting}
								getData={getDataFromBackend}
								setData={setData}
								payloadData={payloadData}
								setPayloadData={setPayloadData}
							/>
						}
					/>

					<RFlex
						styleProps={{
							display: mobile ? "block" : "flex",
							width: "100%",
							alignItems: "center",

							gap: 30,
						}}
					>
						<RButton color="primary" text={tr`create`} faicon="fa fa-plus" outline onClick={() => handleOpenAddUser()} />
						<Form className={styles.form_input}>
							<FormGroup>
								<Input
									type="text"
									placeholder={tr`search`}
									className={styles.search_input}
									defaultValue={searchData}
									onChange={(event) => {
										setSearchData(event.target.value);
									}}
								/>
								<i
									aria-hidden="true"
									className={
										searchLoading ? `fa fa-refresh fa-spin ${styles.search_input_icon}` : `fa fa-search ${styles.search_input_icon}`
									}
									onClick={() => searchData !== "" && handleSearch()}
								/>
								<i
									aria-hidden="true"
									className={`fa fa-close ${styles.clear_input_icon}`}
									onClick={() => {
										if (searchData !== "") {
											setSearchData("");
											handleSearch("");
										}
									}}
								/>
							</FormGroup>
						</Form>
						<p
							style={{
								color: primaryColor,
								textDecoration: "underline",
								cursor: "pointer",
								position: "relative",
								top: "6px",
							}}
							onClick={handleOpenAddFromExisting}
						>{tr`add_from_existing`}</p>
					</RFlex>

					<RAdvancedLister
						getDataFromBackend={getDataFromBackend}
						setData={setData}
						records={processedRecords}
						actionButtons={actionButtons}
						getDataObject={(response) => response.data.data.users}
						getData={getFilteredData}
						setFilterField={setFilterField}
						setFilterValue={setFilterValue}
						filterdispatch={filterdispatch}
						localnewfilter={localnewfilter}
						filterField={filterField}
						filterValue={filterValue}
						table_props={table_props}
						table_name={tabTitle}
					/>
				</React.Fragment>
			)}
		</div>
	);
};

export default UserManagementLister;
