import React from "react";
import RUploadFileSection from "view/FileManagement/RMyFiles/RUploadFileSection";
import RFilterSection from "view/FileManagement/RMyFiles/RFilterSection";
import RMyFilesLister from "view/FileManagement/RMyFiles/RMyFilesLister";
import GManageAccess from "./GManageAccess";
import RMyFilesCard from "view/FileManagement/RMyFiles/RMyFilesCard";
import GDetailsFile from "./GDetailsFile";
import GUploadFile from "./GUploadFile";
import GCopyFile from "./GCopyFile";
import GMoveFile from "./GMoveFile";
import AppModal from "components/Global/ModalCustomize/AppModal";
import Loader from "utils/Loader";
import Drawer from "react-modern-drawer";
import GShare from "./GShare";
import styles from "../FileManagement.module.scss";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { useParams, useHistory, useLocation } from "react-router-dom";
import { fileManagementApi } from "api/global/fileManagement";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useMutateData } from "hocs/useMutateData";
import { genericPath } from "engine/config";
import { useFormik } from "formik";
import { baseURL } from "engine/config";
import { toast } from "react-toastify";
import "react-modern-drawer/dist/index.css";
import downloadFile from "utils/downloadFile";

const GMyFiles = ({ type }) => {
	const history = useHistory();
	const location = useLocation();
	const { folderId, courseId } = useParams();
	const courseIsMaster = location.pathname.includes("master-course");
	const removeContents = ["trashed", "favorite", "shared"].includes(type);

	const initialValues = {
		data: {},
		fileType: [],
		usersWhoMadeModified: [],
		groupingOfSearchFields: {
			name: "",
			sortByFiled: "",
			sortByOrderType: 1,
			mime_type: "",
			modified_by: "",
			folder_id: null,
		},
		showUploadFile: false,
		showCopyFile: false,
		showMoveFile: false,
		switchMode: true,
		showDetail: false,
		showManageAccess: false,
		showShare: false,
		trashedHint: false,
		ids: [],
		fileIds: [],
		folderIds: [],
		itemDetails: {},
		itemLogs: {},
		itemAccess: [],
		fileBase64: null,
		fileDetail: {},
		itemLink: [],
		item: {},
		breadCrumbing: [],
	};

	const { values, setFieldValue } = useFormik({
		initialValues: initialValues,
	});

	// - - - - - - - - - - - - - - Queries - - - - - - - - - - - - - -

	const sortByField = values?.groupingOfSearchFields?.sortByFiled;
	const sortByOrderType = values?.groupingOfSearchFields?.sortByOrderType == 0 ? "0" : values?.groupingOfSearchFields?.sortByOrderType;
	const mimeType = values?.groupingOfSearchFields?.mime_type;
	const modifiedBy = values?.groupingOfSearchFields?.modified_by;
	const nameField = values?.groupingOfSearchFields?.name;
	const asStudentField = courseId ? true : false;
	const folderIdField = folderId;

	let queryParam = "";

	if (!sortByField && !sortByOrderType && !mimeType && !modifiedBy && !nameField && !folderIdField && !courseId && !asStudentField) {
		queryParam = "";
	} else {
		queryParam = "?";
		if (asStudentField) {
			queryParam += `as_student=${asStudentField}&`;
		}
		if (courseId) {
			queryParam += `content_id=${courseId}&content_type=${courseIsMaster ? "masterCourse" : "course"}&`;
		}

		if (folderIdField) {
			queryParam += `folder_id=${folderIdField}&`;
		}

		if (nameField) {
			queryParam += `name=${nameField}&`;
		}

		if (sortByField) {
			queryParam += `field=${sortByField}&`;
		}

		if (sortByOrderType) {
			queryParam += `sort=${sortByOrderType}&`;
		}

		if (mimeType) {
			queryParam += `mime_type=${mimeType}&`;
		}

		if (modifiedBy) {
			queryParam += `modified_by=${modifiedBy}&`;
		}
		// Remove the trailing '&' if it exists
		queryParam = queryParam.slice(0, -1);
	}

	const rootFoldersData = useFetchDataRQ({
		queryKey:
			type == "shared"
				? ["sharedItems", folderId, values?.groupingOfSearchFields]
				: type == "favorite"
				? ["favoriteItems", folderId, values?.groupingOfSearchFields]
				: type == "trashed"
				? ["trashedItems", folderId, values?.groupingOfSearchFields]
				: ["rootFolders", folderId, values?.groupingOfSearchFields],
		queryFn: () =>
			type == "shared"
				? fileManagementApi.sharedItems(queryParam)
				: type == "favorite"
				? fileManagementApi.favoriteItems(queryParam)
				: type == "trashed"
				? fileManagementApi.trashedItems(queryParam)
				: fileManagementApi.rootFolders(queryParam),
		onSuccessFn: ({ data }) => {
			const modifiedSubFolders = data.data.sub_folders.map((subFolder) => ({
				...subFolder,
				saved: true,
			}));

			const modifiedFiles = data.data.files.map((file) => ({
				...file,
				saved: true,
			}));

			const updatedData = {
				...data.data,
				sub_folders: modifiedSubFolders,
				files: modifiedFiles,
			};

			if (!folderId) {
				setFieldValue("breadCrumbing", []);
			}
			if (folderId) {
				setFieldValue("folderIds", []);
				setFieldValue("fileIds", []);
			}
			setFieldValue("data", updatedData);
		},
	});

	const fileTypeData = useFetchDataRQ({
		queryKey: ["fileType"],
		queryFn: () => fileManagementApi.fileType("file_type"),
		onSuccessFn: ({ data }) => {
			setFieldValue("fileType", data?.data);
		},
	});

	const usersWhoMadeModifiedData = useFetchDataRQ({
		queryKey: ["usersWhoMadeModified"],
		queryFn: () => fileManagementApi.fileType("modified_by"),
		onSuccessFn: ({ data }) => {
			setFieldValue("usersWhoMadeModified", data?.data);
		},
	});

	const breadCrumbingData = useFetchDataRQ({
		queryKey: ["breadCrumbing", folderId],
		queryFn: () => fileManagementApi.breadCrumbing(folderId),
		enableCondition: folderId ? true : false,
		onSuccessFn: ({ data }) => {
			setFieldValue("breadCrumbing", data?.data);
		},
	});

	const createSubFolderMutate = useMutateData({
		queryFn: (data) => fileManagementApi.createSubFolders(data),
		onSuccessFn: ({ data }) => {
			values.data.sub_folders.splice(values.data.sub_folders.length - 1, 1);
			values.data.sub_folders.push({ ...data.data, saved: true });
		},
	});

	const addItemsToFavoriteListMutate = useMutateData({
		queryFn: (data) => fileManagementApi.addItemsToFavoriteList(data),
		onSuccessFn: ({ data, variables }) => {
			if (variables.single) {
				// Single Item ( In Table Format )
				const newArr = values.ids.filter((id) => id !== variables.contentId);
				setFieldValue("ids", newArr);

				if (variables.contentType == "folder") {
					const folderId = variables.folder_ids[0];
					let updatedFiles = [];
					// Because I want to remove the folder if I'm on the favorites page otherwise I want to switch the status
					if (type == "favorite") {
						updatedFiles = values.data.sub_folders.filter((item) => item.id !== fileId);
					} else {
						updatedSubFolders = values.data.sub_folders.map((item) => {
							if (item.id === folderId) {
								return { ...item, is_favorite: variables.is_favorite };
							}
							return item;
						});
					}

					setFieldValue("data.sub_folders", updatedSubFolders);
				} else {
					const fileId = variables.file_ids[0];
					let updatedFiles = [];
					// Because I want to remove the file if I'm on the favorites page otherwise I want to switch the status
					if (type == "favorite") {
						updatedFiles = values.data.files.filter((item) => item.id !== fileId);
					} else {
						updatedFiles = values.data.files.map((item) => {
							if (item.id === fileId) {
								return { ...item, is_favorite: variables.is_favorite };
							}
							return item;
						});
					}

					setFieldValue("data.files", updatedFiles);
				}
			} else {
				// Multi Items
				let updatedSubFolders = [];
				let updatedFiles = [];

				// Because I want to remove the file and folder if I'm on the favorites page otherwise I want to switch the status
				if (type == "favorite") {
					updatedSubFolders = values.data.sub_folders.filter((item) => !variables.folder_ids.includes(item.id));
					updatedFiles = values.data.files.filter((item) => !variables.file_ids.includes(item.id));
				} else {
					updatedSubFolders = values.data.sub_folders.map((item) => {
						if (variables.folder_ids.includes(item.id)) {
							return { ...item, is_favorite: variables.is_favorite };
						}
						return item;
					});

					updatedFiles = values.data.files.map((item) => {
						if (variables.file_ids.includes(item.id)) {
							return { ...item, is_favorite: variables.is_favorite };
						}
						return item;
					});
				}

				setFieldValue("data.sub_folders", updatedSubFolders);
				setFieldValue("data.files", updatedFiles);

				//Empty folderIds - fileIds
				setFieldValue("folderIds", []);
				setFieldValue("fileIds", []);
			}
		},
	});

	const deleteItemsFromListMutate = useMutateData({
		queryFn: (data) => fileManagementApi.deleteItemsFromList(data),
		onSuccessFn: ({ data, variables }) => {
			const updatedSubFolders = values.data?.sub_folders.filter((item) => !variables?.folder_ids.includes(item.id));
			const updatedFiles = values.data?.files.filter((item) => !variables?.file_ids.includes(item.id));
			setFieldValue("data.sub_folders", updatedSubFolders);
			setFieldValue("data.files", updatedFiles);
			//Empty folderIds - fileIds
			setFieldValue("folderIds", []);
			setFieldValue("fileIds", []);
		},
		displaySuccess: true,
	});

	const updateSubFolderMutate = useMutateData({
		queryFn: (data) => fileManagementApi.updateFolder(data),
		onSuccessFn: ({ data, variables }) => {
			const folderIndex = values.data.sub_folders.findIndex((file) => file.id == variables.folderId);
			setFieldValue(`data.sub_folders[${folderIndex}].saved`, true);
			setFieldValue(`folderIds`, []);
		},
	});

	const updateFileMutate = useMutateData({
		queryFn: (data) => fileManagementApi.updateFile(data),
		onSuccessFn: ({ data, variables }) => {
			const fileIndex = values.data.files.findIndex((file) => file.id == variables.fileId);
			setFieldValue(`data.files[${fileIndex}].saved`, true);
			setFieldValue(`fileIds`, []);
		},
	});

	const downloadFileMutate = useMutateData({
		queryFn: ({ mimeType, folderName }) => fileManagementApi.downloadFile(values.fileIds[0]),
		downloadFile: true,
		onSuccessFn: ({ data, variables }) => {
			setFieldValue(`fileIds`, []);
		},
	});

	const restoreItemsMutate = useMutateData({
		queryFn: (data) => fileManagementApi.restoreItems(data),
		invalidateKeys: ["trashedItems", folderId, values?.groupingOfSearchFields],
		onSuccessFn: () => {
			setFieldValue("folderIds", []);
			setFieldValue("fileIds", []);
		},
	});

	// - - - - - - - - - - - - - - Handlers - - - - - - - - - - - - - -
	const handleCloseTrashedHint = () => {
		setFieldValue("trashedHint", true);
	};

	const handleOpenUploadFile = () => {
		setFieldValue("showUploadFile", true);
	};
	const handleCloseUploadFile = () => {
		setFieldValue("showUploadFile", false);
	};

	const handleOpenCopyFile = () => {
		setFieldValue("showCopyFile", true);
	};
	const handleCloseCopyFile = () => {
		setFieldValue("showCopyFile", false);
	};

	const handleOpenMoveFile = () => {
		setFieldValue("showMoveFile", true);
	};
	const handleCloseMoveFile = () => {
		setFieldValue("showMoveFile", false);
	};

	const handleOpenManageAccess = () => {
		setFieldValue("showManageAccess", true);
		setFieldValue("showDetail", false);
	};
	const handleCloseManageAccess = () => {
		setFieldValue("showManageAccess", false);
	};

	const handleOpenShare = () => {
		setFieldValue("showShare", true);
		setFieldValue("showDetail", false);
	};
	const handleCloseShare = () => {
		setFieldValue("showShare", false);
	};

	const handleSwitchBetweenTowMode = () => {
		setFieldValue("switchMode", !values.switchMode);
	};

	const handleToggleDrawerToOpenDetail = () => {
		setFieldValue("showDetail", !values.showDetail);
		if (values.showDetail) {
			setFieldValue(`fileBase64`, "");
			setFieldValue(`folderIds`, []);
			setFieldValue(`fileIds`, []);
		}
	};

	// When upload image from modal i need to set data in formik state (because i don't need recall api after close modal)
	const handleAddDataToList = (data) => {
		// i put saved true because when add new file i don't need to update on name
		// (because if don't add saved after upload file updateFileMutate it's Run , because in RMyFiles => RHoverInput => handleOnBlur it's run after add new files )
		const mergeArray = [...data, ...values.data.files];
		const uniqueArray = Object.values(
			mergeArray.reduce((acc, obj) => {
				acc[obj.id] = obj;
				acc[obj.id].saved = true;
				return acc;
			}, {})
		);
		setFieldValue("data.files", uniqueArray);
	};

	//  - - - - - - - Handlers Sort - - - - - - -
	const handleSortBy = (item) => {
		setFieldValue("groupingOfSearchFields.sortByFiled", item == values?.groupingOfSearchFields?.sortByFiled ? "" : item);
	};

	const handleOrderBy = (order) => {
		setFieldValue("groupingOfSearchFields.sortByOrderType", order == values?.groupingOfSearchFields.sortByOrderType ? 1 : order);
	};

	//  - - - - - - - Handlers Filter - - - - - - -
	const handleFilterOnUsers = (user) => {
		setFieldValue("groupingOfSearchFields.modified_by", user == values?.groupingOfSearchFields?.modified_by ? "" : user);
	};

	const handleFilterOnFileType = (mimeType) => {
		setFieldValue("groupingOfSearchFields.mime_type", mimeType == values?.groupingOfSearchFields?.mime_type ? "" : mimeType);
	};

	//  - - - - - - - Handle Add New Folder - - - - - - -
	const handleAddNewFolder = () => {
		const folderNames = values.data.sub_folders.map((folder) => folder.folder_name);

		let defaultFolderName = "NewFolder";

		// Check if 'NewFolder' already exists
		if (folderNames.includes(defaultFolderName)) {
			let index = 1;
			// Keep incrementing the index until a unique folder name is found
			while (folderNames.includes(`${defaultFolderName}(${index})`)) {
				index++;
			}
			// Set the default folder name with the incremented index
			defaultFolderName = `${defaultFolderName}(${index})`;
		}
		const newFolder = { fakeId: -1, folder_name: defaultFolderName, size: 0, created_at: new Date(), saved: false };
		setFieldValue("data.sub_folders", [...values.data.sub_folders, newFolder]);
	};

	const handleInputSaved = (event, item) => {
		if (item.id) {
			if (item.content_type) {
				updateFileMutate.mutate({ fileId: item.id, data: { file_name_by_user: event.target.value } });
			} else {
				updateSubFolderMutate.mutate({ folderId: item.id, data: { folder_name: event.target.value } });
			}
		} else {
			if (event.key == "Enter" || !event.key) {
				createSubFolderMutate.mutate({ folder_name: event.target.value, parent_folder: values.data.current_folder.id });
			}
		}
	};

	const handleChangeText = (value, name) => {
		setFieldValue(name, value);
	};

	//  - - ** Favorite ** - - - - - Handle Add Items To Favorite (Single Item) - - - - - - -
	const handleAddSingleItemToFavorite = ({ contentId, contentType, isFavorite }) => {
		if (contentType == "folder") {
			addItemsToFavoriteListMutate.mutate({
				folder_ids: [contentId],
				file_ids: [],
				is_favorite: !isFavorite,
				single: true,
				contentType: contentType,
				contentId: contentId,
			});
			setFieldValue("ids", [...values.ids, contentId]);
		} else {
			addItemsToFavoriteListMutate.mutate({
				folder_ids: [],
				file_ids: [contentId],
				is_favorite: !isFavorite,
				single: true,
				contentType: contentType,
				contentId: contentId,
			});
			setFieldValue("ids", [...values.ids, contentId]);
		}
	};

	const handleAddItemsToFavorite = () => {
		addItemsToFavoriteListMutate.mutate({ folder_ids: values.folderIds, file_ids: values.fileIds, is_favorite: true });
	};

	const handleRemoveItemsFromFavorite = () => {
		addItemsToFavoriteListMutate.mutate({ folder_ids: values.folderIds, file_ids: values.fileIds, is_favorite: false });
	};

	//  - - - - - - - Handle Select Multi Ids ( ids )- - - - - - -
	const handleSelectItems = ({ contentId, contentType }) => {
		if (contentType === "folder") {
			if (!values.folderIds.includes(contentId)) {
				setFieldValue("folderIds", [...values.folderIds, contentId]);
			} else {
				setFieldValue(
					"folderIds",
					values.folderIds.filter((id) => id !== contentId)
				);
			}
		} else {
			if (!values.fileIds.includes(contentId)) {
				setFieldValue("fileIds", [...values.fileIds, contentId]);
			} else {
				setFieldValue(
					"fileIds",
					values.fileIds.filter((id) => id !== contentId)
				);
			}
		}
	};

	//  - - - - - - - Handle Select And UnSelect All Items ( folderIds , fileIds ) - - - - - - -
	const handleSelectedAllItems = () => {
		const FolderIds = values.data.sub_folders.map((folder) => folder.id);
		const FileIds = values.data.files.map((file) => file.id);

		setFieldValue("folderIds", FolderIds);
		setFieldValue("fileIds", FileIds);
	};

	const handleClearSelectedItems = () => {
		setFieldValue("folderIds", []);
		setFieldValue("fileIds", []);
	};

	//  - - - - - - - Handle Delete Items From List - - - - - - -
	const handleDeleteItemsFromList = () => {
		deleteItemsFromListMutate.mutate({ folder_ids: values.folderIds, file_ids: values.fileIds });
	};

	//  - - - - - - - Handle Show Specific Folder - - - - - - -
	const handleShowSpecificFolder = (folderId) => {
		history.push(`${baseURL}/${genericPath}/file-management/my-files${folderId ? `/${folderId}` : ""}`);
	};

	//  - - - - - - - Handle Close Copy Modal And Empty Selected Ids - - - - - - -
	const handleCloseCopyModal = () => {
		setFieldValue("groupingOfSearchFields.name", "");
		setFieldValue("fileIds", []);
		setFieldValue("folderIds", []);
		setFieldValue("ids", []);
		handleCloseCopyFile();
	};
	//  - - - - - - - Handle Close Move Modal And Empty Selected Ids - - - - - - -
	const handleCloseMoveModal = () => {
		setFieldValue("groupingOfSearchFields.name", "");
		setFieldValue("fileIds", []);
		setFieldValue("folderIds", []);
		setFieldValue("ids", []);
		handleCloseMoveFile();
	};

	//  - - - - - - - Handle Add New Folder - - - - - - -
	const handleRenameItems = () => {
		if (values.fileIds?.length > 0) {
			const fileIndex = values.data.files.findIndex((file) => file.id == values.fileIds[0]);
			setFieldValue(`data.files[${fileIndex}].saved`, false);
		} else {
			const folderIndex = values.data.sub_folders.findIndex((file) => file.id == values.folderIds[0]);
			setFieldValue(`data.sub_folders[${folderIndex}].saved`, false);
		}
	};

	//  - - - - - - - Handle Set File Details (Using This File Details When Download File) - - - - - - -
	const handleSetFileDetail = (data) => {
		setFieldValue(`fileDetail`, data);
	};

	//  - - - - - - - Handle SetBase64 (بشكل مبدأي مشان التفاصييل لقدام بدو يعدلها القودااااا) - - - - - - -
	const handleSetBase64 = (data) => {
		setFieldValue(`fileBase64`, data);
	};

	//  - - - - - - - Handle Set Root Folder Id (to use in detail [Next To Switch]) - - - - - - -
	const handleSetRootFolder = () => {
		setFieldValue(`folderIds`, [values?.data?.current_folder?.id]);
	};

	//  - - - - - - - Handle Download File - - - - - - -
	const handleDownloadFile = () => {
		if (values.item?.file_url) {
			downloadFile(values.item?.file_url);
		} else {
			downloadFileMutate.mutate({
				mimeType: values.fileDetail.mime_type,
				folderName: values.fileDetail.file_name_by_user,
			});
		}
	};

	//  - - - - - - - Handle Set Item Link  - - - - - - -
	const handleSetItem = ({ item }) => {
		if (!values.itemLink.includes(item?.link?.link)) {
			setFieldValue("itemLink", [...values.itemLink, item?.link?.link]);
		} else {
			setFieldValue(
				"itemLink",
				values.itemLink.filter((id) => id !== item?.link?.link)
			);
		}
		// Set Data In Item To use in (copy link and manage access)
		setFieldValue("item", item);
	};

	//  - - - - - - - Handle Copy Link - - - - - - -
	const handleCopyLink = () => {
		// navigator.clipboard.writeText("dfdsf").then(() => {
		// 	toast.success(tr`link_is_copied`);
		// });
		navigator.clipboard.writeText(`http://localhost:3000${baseURL}/${genericPath}/${values?.itemLink?.[0]}`);
		toast.success(tr`link_is_copied`);
	};

	//  - - - - - - - Handle Restore Items - - - - - - -
	const handleRestoreItems = (itemId, contentType) => {
		const folderIdToAdd = !contentType && itemId ? itemId : undefined;
		const fileIdToAdd = contentType && itemId ? itemId : undefined;

		restoreItemsMutate.mutate({
			folder_ids: [...values.folderIds, folderIdToAdd],
			file_ids: [...values.fileIds, fileIdToAdd],
		});
	};

	if (
		fileTypeData.isLoading ||
		usersWhoMadeModifiedData.isLoading ||
		(breadCrumbingData.isLoading && breadCrumbingData.fetchStatus !== "idle")
	)
		return <Loader />;
	return (
		<React.Fragment>
			{/* - - - - - - - Modals - - - - - - - */}

			{/* - - - Upload File- - - - Modals - - - - - - - */}
			<AppModal
				size="lg"
				show={values.showUploadFile}
				parentHandleClose={handleCloseUploadFile}
				headerSort={
					<GUploadFile
						folderId={folderId ? folderId : rootFoldersData?.data?.data?.data?.current_folder?.id}
						handleAddDataToList={handleAddDataToList}
					/>
				}
			/>

			{/* - - - Move File- - - - Modals - - - - - - - */}
			<AppModal
				size="lg"
				show={values.showMoveFile}
				parentHandleClose={handleCloseMoveFile}
				headerSort={
					<GMoveFile
						idsSelected={{ folderIds: values.folderIds, fileIds: values.fileIds }}
						parentInvalidateKey={["rootFolders", folderId, values?.groupingOfSearchFields]}
						handleCloseMoveModal={handleCloseMoveModal}
					/>
				}
			/>

			{/* - - - Copy File- - - - Modals - - - - - - - */}
			<AppModal
				size="lg"
				show={values.showCopyFile}
				parentHandleClose={handleCloseCopyFile}
				headerSort={
					<GCopyFile
						idsSelected={{ folderIds: values.folderIds, fileIds: values.fileIds }}
						parentInvalidateKey={["rootFolders", folderId, values?.groupingOfSearchFields]}
						handleCloseCopyModal={handleCloseCopyModal}
					/>
				}
			/>

			{/* - - - Manage Access - - - - Modals - - - - - - - */}
			<AppModal
				size="md"
				show={values.showManageAccess}
				parentHandleClose={handleCloseManageAccess}
				headerSort={
					<GManageAccess
						data={{ item: values.item, folderIds: values.folderIds, fileIds: values.fileIds }}
						parentInvalidateKey={["rootFolders", folderId, values?.groupingOfSearchFields]}
						handlers={{ setFieldValue, handleCloseManageAccess, handleCopyLink }}
					/>
				}
			/>

			{/* - - - Share - - - - Modals - - - - - - - */}
			<AppModal
				size="md"
				show={values.showShare}
				parentHandleClose={handleCloseShare}
				headerSort={
					<GShare
						data={{ item: values.item, folderIds: values.folderIds, fileIds: values.fileIds }}
						parentInvalidateKey={["rootFolders", folderId, values?.groupingOfSearchFields]}
						handlers={{ setFieldValue, handleCloseShare, handleCopyLink }}
					/>
				}
			/>

			{/* - - - - - - - Drawer - - - - - - - */}

			{values.showDetail && (
				<Drawer open={values.showDetail} onClose={handleToggleDrawerToOpenDetail} direction="right" className={"drawer_cart"}>
					<GDetailsFile data={{ values, removeContents, type }} handlers={{ handleOpenManageAccess, handleOpenShare, setFieldValue }} />
				</Drawer>
			)}

			{/* - - - - - - - Content - - - - - - - */}
			<RFlex className={styles.container}>
				{/* - - - - - - - Upload File/Folder And Create New Folder - - - - - - - */}
				<RUploadFileSection
					data={{ values, removeContents, type }}
					handlers={{ handleOpenUploadFile, handleAddNewFolder, setFieldValue }}
					loading={{
						createSubFolderLoading: createSubFolderMutate.isLoading,
						updateSubFolderLoading: updateSubFolderMutate.isLoading,
						updateFileLoading: updateFileMutate.isLoading,
						allItemsLoading: rootFoldersData.isLoading,
					}}
				/>
				{/* - - - - - - - Filters - - - - - - - */}
				<RFilterSection
					data={{ values, removeContents, type, folderId }}
					handlers={{
						handleSwitchBetweenTowMode,
						handleSortBy,
						handleOrderBy,
						handleSelectedAllItems,
						handleClearSelectedItems,
						handleDeleteItemsFromList,
						handleAddItemsToFavorite,
						handleRemoveItemsFromFavorite,
						handleOpenMoveFile,
						handleOpenCopyFile,
						handleRenameItems,
						handleToggleDrawerToOpenDetail,
						handleSetRootFolder,
						handleDownloadFile,
						handleCopyLink,
						handleOpenManageAccess,
						handleOpenShare,
						handleRestoreItems,
						handleCloseTrashedHint,
						handleShowSpecificFolder,
					}}
					loading={{
						deleteItemsLoading: deleteItemsFromListMutate.isLoading,
						addItemsToFavoriteListLoading: addItemsToFavoriteListMutate.isLoading,
						restoreItemsLoading: restoreItemsMutate.isLoading,
					}}
				/>
				{/* - - - - - - - All Files Contain Tow Mode - - - - - - - */}
				{values?.switchMode ? (
					<RMyFilesCard
						data={{ values }}
						handlers={{
							handleChangeText,
							handleInputSaved,
							handleSelectItems,
							handleShowSpecificFolder,
							handleSetBase64,
							handleSetFileDetail,
							handleSetItem,
						}}
						loading={{
							rootFoldersLoading: rootFoldersData.isLoading,
							rootFoldersFetching: rootFoldersData.isFetching,
							deleteItemsLoading: deleteItemsFromListMutate.isLoading,
							addItemsToFavoriteListLoading: addItemsToFavoriteListMutate.isLoading,
						}}
					/>
				) : (
					<RMyFilesLister
						data={{ values, removeContents, type }}
						handlers={{
							handleFilterOnUsers,
							handleFilterOnFileType,
							handleChangeText,
							handleInputSaved,
							handleAddSingleItemToFavorite,
							handleSelectItems,
							handleDeleteItemsFromList,
							handleShowSpecificFolder,
							handleToggleDrawerToOpenDetail,
							handleAddItemsToFavorite,
							handleRemoveItemsFromFavorite,
							handleOpenMoveFile,
							handleOpenCopyFile,
							handleRenameItems,
							handleSetBase64,
							handleSetFileDetail,
							handleDownloadFile,
							handleCopyLink,
							handleSetItem,
							handleOpenManageAccess,
							handleOpenShare,
							handleRestoreItems,
						}}
						loading={{
							rootFoldersLoading: rootFoldersData.isLoading,
							deleteItemsLoading: deleteItemsFromListMutate.isLoading,
							addItemsToFavoriteListLoading: addItemsToFavoriteListMutate.isLoading,
							restoreItemsLoading: restoreItemsMutate.isLoading,
						}}
					/>
				)}
			</RFlex>
		</React.Fragment>
	);
};

export default GMyFiles;
