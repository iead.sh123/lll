import React from "react";
import RDetailsFile from "view/FileManagement/RMyFiles/RDetailsFile";
import RLogsFile from "view/FileManagement/RMyFiles/RDetailsFile/RLogsFile";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { fileManagementApi } from "api/global/fileManagement";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";

const GDetailsFile = ({ data, handlers }) => {
	const { courseId } = useParams();
	const { user } = useSelector((state) => state.auth);
	const itIsOwnerToThisItem = data?.item?.owner == user?.id;
	const checkSharedAndOwner = data?.type === "shared" && itIsOwnerToThisItem;

	const folderDetailsData = useFetchDataRQ({
		queryKey: ["folderDetails", data.values.folderIds[0]],
		queryFn: () => fileManagementApi.folderDetails(data.values.folderIds[0]),
		enableCondition: data.values.folderIds.length > 0 ? true : false,
		onSuccessFn: ({ data }) => {
			handlers.setFieldValue("itemDetails", data?.data);
		},
	});

	const fileDetailsData = useFetchDataRQ({
		queryKey: ["fileDetails", data.values.fileIds[0]],
		queryFn: () => fileManagementApi.fileDetails(data.values.fileIds[0]),
		enableCondition: data.values.fileIds.length > 0 ? true : false,
		onSuccessFn: ({ data }) => {
			handlers.setFieldValue("itemDetails", data?.data);
		},
	});

	const folderLogsData = useFetchDataRQ({
		queryKey: ["folderLogs", data.values.folderIds[0]],
		queryFn: () => fileManagementApi.folderLogs(data.values.folderIds[0]),
		enableCondition: data.values.folderIds.length > 0 ? true : false,
		onSuccessFn: ({ data }) => {
			handlers.setFieldValue("itemLogs", data?.data);
		},
	});

	const fileLogsData = useFetchDataRQ({
		queryKey: ["fileLogs", data.values.fileIds[0]],
		queryFn: () => fileManagementApi.fileLogs(data.values.fileIds[0]),
		enableCondition: data.values.fileIds.length > 0 ? true : false,
		onSuccessFn: ({ data }) => {
			handlers.setFieldValue("itemLogs", data?.data);
		},
	});

	const folderAccessData = useFetchDataRQ({
		queryKey: ["folderAccess", data.values.folderIds[0]],
		queryFn: () => fileManagementApi.folderAccess(data.values.folderIds[0]),
		enableCondition: courseId
			? courseId
				? true
				: false
			: data?.type === "shared"
			? checkSharedAndOwner
				? true
				: false
			: data.values.folderIds.length > 0
			? true
			: false,
		onSuccessFn: ({ data }) => {
			handlers.setFieldValue("itemAccess", data?.data);
		},
	});

	const fileAccessData = useFetchDataRQ({
		queryKey: ["fileAccess", data.values.fileIds[0]],
		queryFn: () => fileManagementApi.fileAccess(data.values.fileIds[0]),
		enableCondition: courseId
			? courseId
				? true
				: false
			: data?.type === "shared"
			? checkSharedAndOwner
				? true
				: false
			: data.values.fileIds.length > 0
			? true
			: false,

		onSuccessFn: ({ data }) => {
			handlers.setFieldValue("itemAccess", data?.data);
		},
	});

	if (
		(folderDetailsData.isLoading && folderDetailsData.fetchStatus !== "idle") ||
		(fileDetailsData.isLoading && fileDetailsData.fetchStatus !== "idle") ||
		(folderLogsData.isLoading && folderLogsData.fetchStatus !== "idle") ||
		(fileLogsData.isLoading && fileLogsData.fetchStatus !== "idle") ||
		(folderAccessData.isLoading && folderAccessData.fetchStatus !== "idle") ||
		(fileAccessData.isLoading && fileAccessData.fetchStatus !== "idle")
	)
		return <Loader />;

	return (
		<RFlex styleProps={{ flexDirection: "column" }}>
			<RDetailsFile data={{ data, user }} handlers={{ handlers }} />
			<RLogsFile data={{ data }} />
		</RFlex>
	);
};

export default GDetailsFile;
