import React from "react";
import { initialState, reducer } from "./state/terms-management.reducer";

import { faBookOpen, faCalendar, faCalendarCheck, faUserGraduate, faUserPen, faUserPlus, faUsers } from "@fortawesome/free-solid-svg-icons";
import tr from "components/Global/RComs/RTranslator";
import RNavigationHeader from "components/Global/RComs/RNavigationHeader/RNavigationHeader";
import { pages } from "./constants/pages.constant";
import {
	navigateStepper,
	loadAcademicYears,
	addAcademicYear,
	selectAcademicYear,
	loadTerms,
	addTerm,
	activateTerm,
	deactivateTerm,
	finalizeTerm,
	manageTerm,
	updateAcademicYear,
	updateTerm,
	manageGradeLevel,
	loadCurricula,
	loadWeeklyScheduleEvents,
	deleteWeeklyScheduleEvent,
	getAvailableWeeklyScheduleElements,
	addOtherElementType,
	updateOtherElementType,
	deleteOtherElementType,
	addWeeklyScheduleElement,
	updateWeeklyScheduleElement,
	addToPayload,
	addToDeleteElements,
	saveWeeklySchedule,
	setEditedHomeRoom,
	loadstudentsByGradeLevels,
	loadOrganizationTeachers,
	enrollHomeRoomUser,
	deleteHomeRoomUsers,
	manageCurriculum,
	deleteCourseUsers,
	enrollUsersInCourse,
	createHomeRoom,
	createCourse,
	loadCourse,
	changeUserClass,
	changeUserHomeroom,
} from "./state/terms-manager.actions";
import { pageMapping } from "./pages";
import Loader from "components/Global/Loader";
import { elementsOpType } from "./constants/elements.optype.constant";
import { homeRoomOrCurriculaMode } from "./constants/homeroms-curricula-editor-modes.constants";
import { useHistory, useLocation } from "react-router-dom/cjs/react-router-dom.min";
import { baseURL } from "engine/config";
import { genericPath } from "engine/config";

const TermManagement = () => {
	//refs
	const mounted = React.useRef();

	//url params
	const location = useLocation();
	const queryParams = new URLSearchParams(location.search);

	const academicYearId = queryParams.get("academicYearId");
	const termId = queryParams.get("termId");
	const gradeLevelId = queryParams.get("gradeLevelId");

	const history = useHistory();

	const [data, dispatch] = React.useReducer(reducer, initialState);
	const [loading, setLoading] = React.useState(false);
	const [loadingPage, setLoadingPage] = React.useState(false);

	React.useEffect(() => {
		mounted.current = true;

		return () => {
			mounted.current = false;
		};
	}, []);

	React.useEffect(() => {
		initializeRoutePage();
	}, [academicYearId, gradeLevelId, termId]);

	const initializeRoutePage = async () => {
		setLoadingPage(true);

		if (!academicYearId) {
			setLoadingPage(false);
			return;
		}

		if (academicYearId) await handleManageAcademicYear(+academicYearId, false, true);

		if (!termId) {
			handleNavigateStep(pages.TERMS);
			setLoadingPage(false);
			return;
		}

		await handleManageTerm(+termId, false, true);

		if (!gradeLevelId) {
			handleNavigateStep(pages.MANAGE_TERMS);
			setLoadingPage(false);
			return;
		}

		await handleSelectGradeLevel(+gradeLevelId, +termId, false, true);

		handleNavigateStep(pages.HOME_ROOMS);
		setLoadingPage(false);
	};

	//stepper handlers
	const handleNavigateStep = (stepId) => navigateStepper(stepId, dispatch);

	const handleLoadAcademicYears = async () => {
		setLoading(true);
		await loadAcademicYears(dispatch, mounted);
		setLoading(false);
	};

	const handleSaveAcademicYear = async (data) => {
		if (loading) return;
		setLoading(true);
		await addAcademicYear(dispatch, mounted, data);
		setLoading(false);
	};

	const handleUpdateAcademicYear = async (id, data) => {
		if (loading) return;
		setLoading(true);
		await updateAcademicYear(dispatch, mounted, id, data);
		setLoading(false);
	};

	const handleUpdateTerm = async (id, data) => {
		if (loading) return;
		setLoading(true);
		await updateTerm(dispatch, mounted, id, data);
		setLoading(false);
	};

	const handleLoadTerms = async () => {
		setLoading(true);
		await loadTerms(dispatch, mounted, data.selectedAcademicYear);
		setLoading(false);
	};

	const handleSaveTerm = async (termData) => {
		if (loading) return;

		setLoading(true);
		await addTerm(dispatch, mounted, data.selectedAcademicYear, termData);
		setLoading(false);
	};

	const handleManageAcademicYear = async (_academicYearId, push = true, dontChangePage = false) => {
		await selectAcademicYear(_academicYearId, dispatch, dontChangePage);
		push && history.push(`${baseURL}/${genericPath}/terms-manager?academicYearId=${_academicYearId}`);
		return;
	};

	const handleManageTerm = async (_termId, push = true, dontChangePage = false) => {
		if (loading) return;

		setLoading(true);

		await manageTerm(mounted, dispatch, _termId, dontChangePage);

		push && history.push(`${baseURL}/${genericPath}/terms-manager?academicYearId=${data.selectedAcademicYear}&termId=${_termId}`);
		setLoading(false);
	};

	const handleDeactivateTerm = async (termId) => {
		if (loading) return;
		setLoading(true);
		await deactivateTerm(termId, mounted, dispatch);
		setLoading(false);
	};

	const handleActivateTerm = async (termId) => {
		if (loading) return;
		setLoading(true);
		await activateTerm(termId, mounted, dispatch);
		setLoading(false);
	};

	const handleFinalize = async (termId) => {
		if (loading) return;
		setLoading(true);
		await finalizeTerm(termId, mounted, dispatch);
		setLoading(false);
	};

	const handleSelectGradeLevel = async (_gradeLevelId, _termId = null, push = true, dontChangePage = false) => {
		await manageGradeLevel(mounted, dispatch, _termId ? +_termId : data.selectedTerm, _gradeLevelId, dontChangePage);
		push &&
			history.push(
				`${baseURL}/${genericPath}/terms-manager?academicYearId=${data.selectedAcademicYear}&termId=${data.selectedTerm}&gradeLevelId=${_gradeLevelId}`
			);
	};

	const handleLoadCurricula = async () => {
		setLoading(true);
		await loadCurricula(dispatch, mounted, data.selectedTerm, data.selectedGradeLevel);
		setLoading(false);
	};

	const handleLoadWeeklyScheduleElements = async () => {
		setLoading(true);
		await loadWeeklyScheduleEvents(dispatch, mounted, data.selectedTerm, data.selectedGradeLevel);
		setLoading(false);
	};

	const handleDeleteWeeklyScheduleElement = async (evId) => {
		return addToPayload(dispatch, elementsOpType.DELETE, evId);

		// setLoading(true);
		// await deleteWeeklyScheduleEvent(dispatch, mounted, evId); //push in events to delete
		// setLoading(false);
	};

	const loadAvailableWeeklyScheduleElements = async () => {
		setLoading(true);
		await getAvailableWeeklyScheduleElements(dispatch, mounted, data.selectedGradeLevel, data.selectedTerm);
		setLoading(false);
	};

	const addOtherWeeklyScheduleElement = async (data) => {
		setLoading(true);
		await addOtherElementType(dispatch, mounted, data);
		setLoading(false);
	};

	const handleAddWeeklyScheduleElement = async (payload) => {
		return addToPayload(dispatch, elementsOpType.ADD, payload);
		// setLoading(true);
		// //push in events to update without id
		// await addWeeklyScheduleElement(dispatch, mounted, data.selectedGradeLevel, data.selectedTerm, payload);
		// setLoading(false);
	};

	const updateOtherWeeklyScheduleElement = async (id, data) => {
		setLoading(true);
		//push in events to update with id
		await updateOtherElementType(dispatch, mounted, id, data);
		setLoading(false);
	};

	const deleteOtherWeeklyScheduleElement = async (id) => {
		setLoading(true);
		await deleteOtherElementType(dispatch, mounted, id);
		setLoading(false);
	};

	const handleUpdateWeeklyScheduleElement = async (id, payload) => {
		return addToPayload(dispatch, elementsOpType.UPDATE, { ...payload, id });
		// if (loading)
		//   return;

		// setLoading(true);
		// await updateWeeklyScheduleElement(dispatch, mounted, data.selectedGradeLevel,
		//   data.selectedTerm,
		//   { ...payload, id });
		// setLoading(false);
	};

	const handleSaveWeeklySchedule = async () => {
		if (loading || (!data.weeklySchedulePayload.elements.length && !data.weeklySchedulePayload.elementIdsToDelete.length)) return;

		setLoading(true);
		await saveWeeklySchedule(dispatch, mounted, data.selectedGradeLevel, data.selectedTerm, {
			...data.weeklySchedulePayload,
			elements: data.weeklySchedulePayload.elements.map((el) => ({ ...el, id: isNaN(el.id) ? undefined : +el.id })),
		});
		setLoading(false);
	};

	const handleManageHomeRoom = async (homeRoomId) => {
		setLoading(true);
		await setEditedHomeRoom(dispatch, mounted, homeRoomId);
		setLoading(false);
	};

	const handleLoadGradeLevelStudents = async (selectedGradeLevels) => {
		setLoading(true);
		await loadstudentsByGradeLevels(dispatch, mounted, data.selectedTerm, selectedGradeLevels);
		setLoading(false);
	};

	const handleLoadOrganizationTeachers = async (searchText) => {
		setLoading(true);
		await loadOrganizationTeachers(dispatch, mounted, data.selectedTerm, searchText);
		setLoading(false);
	};

	const handleEnrollHomeRoomUsers = async (hrId, users) => {
		setLoading(true);
		await enrollHomeRoomUser(dispatch, mounted, hrId, users);
		setLoading(false);
	};

	const handleDeleteHomeRoomUser = async (hrId, type, id) => {
		setLoading(true);
		await deleteHomeRoomUsers(dispatch, mounted, hrId, type, id);
		setLoading(false);
	};

	const handleManageCurriculum = async (curriculumId) => {
		setLoading(true);
		await manageCurriculum(dispatch, mounted, data.entities.curricula.byId[curriculumId]);
		setLoading(false);
	};

	const handleSelectCourse = async (courseId) => {
		setLoading(true);
		await loadCourse(dispatch, mounted, courseId);
		setLoading(false);
	};

	const handleEnrollCourseUsers = async (coId, users) => {
		setLoading(true);
		alert(coId);
		await enrollUsersInCourse(dispatch, mounted, coId, users);
		setLoading(false);
	};

	const handleDeleteCourseUser = async (coId, type, id) => {
		setLoading(true);
		await deleteCourseUsers(dispatch, mounted, coId, type, id);
		setLoading(false);
	};

	const handleAddHomeRoom = async (payload) => {
		setLoading(true);

		const id = await createHomeRoom(dispatch, mounted, data.selectedTerm, data.selectedGradeLevel, payload);
		setLoading(false);
		return id;
	};

	const handleAddCourse = async (payload) => {
		setLoading(true);
		const id = await createCourse(dispatch, mounted, { ...payload, term_id: data.selectedTerm, curriculum_id: data.selectedCurriculum });
		setLoading(false);
		return id;
	};

	const handleChangeUserClass = async (type, user, oldCourseId, newCourseId) => {
		setLoading(true);
		await changeUserClass(dispatch, mounted, type, user, oldCourseId, newCourseId);
		setLoading(false);
	};

	const handleChangeUserHomeRoom = async (type, user, oldHomeRoomId, newHomeRoomId) => {
		setLoading(true);
		await changeUserHomeroom(dispatch, mounted, type, user, oldHomeRoomId, newHomeRoomId);
		setLoading(false);
	};

	const pageProps = {
		[pages.ACADEMIC_YEARS]: {
			handleLoadAcademicYears,
			academicYears: data.entities.academic_years,
			handleSave: handleSaveAcademicYear,
			handleManageAcademicYear,
			handleUpdate: handleUpdateAcademicYear,
		},
		[pages.TERMS]: {
			handleLoadTerms,
			terms: data.entities.terms,
			handleSave: handleSaveTerm,
			handleManage: handleManageTerm,
			handleDeactivate: handleDeactivateTerm,
			handleActivate: handleActivateTerm,
			handleFinalize,
			handleUpdate: handleUpdateTerm,
		},
		[pages.MANAGE_TERMS]: {
			gradeLevels: data.entities.gradeLevels,
			selectedTerm: data.entities.terms.byId[data.selectedTerm],
			handleSelectgradeLevel: handleSelectGradeLevel,
		},
		[pages.HOME_ROOMS]: {
			homeRooms: data.entities.homerooms,
			handleManageHomeRoom,
			handleLoadOrganizationTeachers,
			teachers: data.entities.teachers,
			students: data.entities.students,
			handleLoadGradeLevelStudents,
			handleSave: handleAddHomeRoom,
		},
		[pages.CURRICULA]: {
			curricula: data.entities.curricula,
			handleLoadCurricula,
			handleManageCurriculum,
		},
		[pages.WEEKLY_SCHEDULE]: {
			periods: data.entities.periods,
			events: data.entities.weeklyScheduleElements,
			handleLoadWeeklyScheduleElements,
			loadAvailableWeeklyScheduleElements,
			handleDeleteWeeklyScheduleElement,
			availableWeeklyScheduleElements: data.entities.availableWeeklyScheduleElements,
			addOtherWeeklyScheduleElement,
			updateOtherWeeklyScheduleElement,
			deleteOtherWeeklyScheduleElement,
			handleAddWeeklyScheduleElement,
			handleUpdateWeeklyScheduleElement,
			handleSaveWeeklySchedule,
			saveDisabled: loading || (!data.weeklySchedulePayload.elements.length && !data.weeklySchedulePayload.elementIdsToDelete.length),
		},
		[pages.HOME_ROOM_EDITOR]: {
			mode: homeRoomOrCurriculaMode.HOMEROOM,
			edited: data.editedHomeroom,
			homeRooms: data.entities.homerooms,
			teachers: data.entities.teachers,
			students: data.entities.students,
			gradeLevels: data.entities.gradeLevels,
			handleLoadGradeLevelStudents,
			handleSelect: handleManageHomeRoom,
			handleSave: handleAddHomeRoom,
			handleLoadOrganizationTeachers,
			handleEnrollUsers: handleEnrollHomeRoomUsers,
			handleDeleteUser: handleDeleteHomeRoomUser,
			handleChangeUserClassOrHomeRoom: handleChangeUserHomeRoom,
		},
		[pages.CURRICULA_EDITOR]: {
			mode: homeRoomOrCurriculaMode.CURRICULA,
			handleLoadGradeLevelStudents,
			handleLoadOrganizationTeachers,
			edited: data.editedCourse,
			teachers: data.entities.teachers,
			students: data.entities.students,
			gradeLevels: data.entities.gradeLevels,
			handleEnrollUsers: handleEnrollCourseUsers,
			handleDeleteUser: handleDeleteCourseUser,
			curricula: data.entities.curricula,
			handleSelect: handleSelectCourse,
			handleSave: handleAddCourse,
			handleChangeUserClassOrHomeRoom: handleChangeUserClass,
			_selectedCurriculum: data.selectedCurriculum,
		},
	};

	const Page = pageMapping[data.selectedPage];
	return (
		<React.Fragment>
			{[pages.HOME_ROOMS, pages.WEEKLY_SCHEDULE, pages.CURRICULA].includes(data.selectedPage) ? (
				<RNavigationHeader
					steps={[
						{ id: pages.HOME_ROOMS, onClick: () => handleNavigateStep(pages.HOME_ROOMS), icon: faUsers, text: tr`Home Rooms` },
						{ id: pages.CURRICULA, onClick: () => handleNavigateStep(pages.CURRICULA), icon: faBookOpen, text: tr`Curricula` },
						{
							id: pages.WEEKLY_SCHEDULE,
							onClick: () => handleNavigateStep(pages.WEEKLY_SCHEDULE),
							icon: faCalendarCheck,
							text: tr`Weekly Schedule`,
						},
					]}
					selectedStep={data.selectedPage}
					mini
				/>
			) : (
				<RNavigationHeader
					steps={[
						{
							id: pages.ACADEMIC_YEARS,
							onClick: () => handleNavigateStep(pages.ACADEMIC_YEARS),
							icon: faUserGraduate,
							text: tr`Create Academic Year`,
						},
						{
							id: pages.TERMS,
							disabled: data.selectedPage !== pages.MANAGE_TERMS,
							onClick: () => handleNavigateStep(pages.TERMS),
							icon: faUserPlus,
							text: tr`Create Terms`,
						},
						{
							id: pages.MANAGE_TERMS,
							disabled: data.selectedPage === pages.ACADEMIC_YEARS || data.selectedPage === pages.TERMS,
							onClick: () => handleNavigateStep(pages.MANAGE_TERMS),
							icon: faUserPen,
							text: tr`Manage Terms`,
						},
					]}
					selectedStep={data.selectedPage}
				/>
			)}
			<React.Suspense fallback={<Loader />}>
				{loadingPage ? <Loader /> : <Page {...pageProps[data.selectedPage]} loading={loading} />}
			</React.Suspense>
		</React.Fragment>
	);
};

export default TermManagement;
