import { faCalendarCheck } from "@fortawesome/free-regular-svg-icons";
import { faChevronRight, faUserFriends } from "@fortawesome/free-solid-svg-icons";
import RButton from "components/Global/RComs/RButton";
import tr from "components/Global/RComs/RTranslator";
import moment from "moment";
import React from "react";
import RRegularCard from "view/TermsManager/cards/RegularCard/RRegularCard";
import CreateAcademicYearModal from "./components/CreateAcademicYearModal";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import Loader from "utils/Loader";
import REmptyData from "components/RComponents/REmptyData";

const AcademicYears = ({ handleLoadAcademicYears, academicYears, handleSave, handleUpdate, handleManageAcademicYear, loading }) => {
	React.useEffect(() => {
		handleLoadAcademicYears();
	}, []);

	const [createModalIsOpen, setCreateModalIsOpen] = React.useState(false);
	const [editedAcademicYearId, setEditedAcademicYearId] = React.useState(null);

	const handleToggle = () => setCreateModalIsOpen(!createModalIsOpen);

	const handleEditAcademicYear = (id) => {
		setEditedAcademicYearId(id);
		handleToggle();
	};

	return (
		<div>
			{loading ? (
				<Loader />
			) : (
				<React.Fragment>
					<div style={{ display: "flex", justifyContent: "end" }}>
						<RButton
							text={tr`create_new_academic_year`}
							faicon="fa fa-plus"
							color="primary"
							outline
							onClick={handleToggle}
							// style={{alignSelf: 'end'}}
						/>
					</div>

					<CreateAcademicYearModal
						isOpen={createModalIsOpen}
						toggle={handleToggle}
						handleSave={
							editedAcademicYearId ? (data) => handleUpdate(editedAcademicYearId, data) && setEditedAcademicYearId(null) : handleSave
						}
						edited={academicYears?.byId?.[editedAcademicYearId]}
					/>

					{academicYears?.ids?.length ? (
						<RFlex styleProps={{ flexWrap: "wrap" }}>
							{academicYears?.ids?.map((acId) => (
								<RRegularCard
									key={"academicyear-" + acId}
									id={acId}
									header={academicYears.byId[acId]?.name}
									items={[
										{
											text:
												moment(academicYears.byId[acId]?.startDate).format("yyyy") +
												" - " +
												moment(academicYears.byId[acId]?.endDate).format("yyyy"),
											icon: faCalendarCheck,
										},
										{
											text: tr(`${academicYears.byId[acId].termsCount > 0 ? academicYears?.byId[acId]?.termsCount : "No"} Terms Created`),
											icon: faUserFriends,
										},
									]}
									contextMenuActions={[
										{
											id: acId + "-academicyear-manage",
											disabled: false,
											onClick: () => handleManageAcademicYear(acId),
											icon: "fa fa-pencil-square-o",
											name: tr`Manage`,
										},
										{
											id: acId + "-academicyear-edit",
											disabled: false,
											onClick: () => handleEditAcademicYear(acId),
											icon: "fa fa-pencil",
											name: tr`Edit`,
										},
									]}
								/>
							))}
						</RFlex>
					) : (
						<REmptyData
							line2={
								<p>
									click on <span style={{ color: "#668AD7" }}>+ Create new academic year </span>to start creating academic years
								</p>
							}
						/>
					)}
				</React.Fragment>
			)}
		</div>
	);
};

export default AcademicYears;
