import { faBookOpen, faCableCar, faIcons, faListDots, faPlus, faUsers } from "@fortawesome/free-solid-svg-icons";
import RAccordion from "components/Global/RComs/RAccordion";
import React from "react";
import AccordionCard from "view/TermsManager/Accordion/AccordionCard";
import TowLevelsAccordion from "view/TermsManager/Accordion/TowLevelsAccordion";
import CreateOtherModal from "./CreateOtherModal";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from "reactstrap";
import iconsFa6 from "variables/iconsFa6";

const WeeklyScheduleEditor = ({
	availableWeeklyScheduleElements,
	addOtherWeeklyScheduleElement,
	updateOtherWeeklyScheduleElement,
	deleteOtherWeeklyScheduleElement,
}) => {
	const [createModalIsOpen, setCreateModalOpen] = React.useState(false);

	const toggleCreateModal = () => setCreateModalOpen(!createModalIsOpen);

	const [edited, setEdited] = React.useState(null);

	const items = [
		{
			icon: faBookOpen,
			id: "weeklyScheduleCurricula",
			component: <p>Curricula</p>,
			collapseId: "weeklyScheduleCurricula",
			items: availableWeeklyScheduleElements?.curricula?.ids?.map((cId) => {
				const c = availableWeeklyScheduleElements?.curricula?.byId[cId];

				return {
					component: <p>{c.name}</p>,
					id: cId,
					collapseId: "curricula" + c.id,
					items: c.courses.map((cour) => ({
						component: <p>{cour.name}</p>,
						draggableId: "course_" + cour.id + "_" + cour.name,
						id: "course" + cour.id,
					})),
				};
			}),
		},

		{
			icon: faUsers,
			component: <p>Home Rooms</p>,
			id: "weeklySchedulehomerooms",
			collapseId: "weeklySchedulehomerooms",
			items: availableWeeklyScheduleElements?.homerooms?.ids?.map((hrId) => ({
				component: <p>{availableWeeklyScheduleElements?.homerooms?.byId[hrId].name}</p>,
				draggableId: "homeroom_" + hrId + "_" + availableWeeklyScheduleElements?.homerooms?.byId[hrId].name,
				id: "homeroom" + hrId,
			})),
		},

		{
			icon: faIcons,
			component: <p>Others</p>,
			collapseId: "weeklyScheduleOthers",
			id: "weeklyScheduleOthers",
			items: [
				{
					icon: faPlus,
					onClick: () => toggleCreateModal(),
					component: <p>Create New</p>,
					id: "createWeeklyScheduleOtherElement",
					style: { borderStyle: "dashed", borderWidth: "2px" },
				},

				...(availableWeeklyScheduleElements?.others?.ids?.map((hrId) => ({
					component: (
						<RFlex styleProps={{ justifyContent: "space-between", width: "100%" }}>
							<p>{availableWeeklyScheduleElements?.others?.byId[hrId].name}</p>
							<UncontrolledDropdown direction="right">
								<DropdownToggle
									aria-haspopup={true}
									// caret
									color="default"
									data-toggle="dropdown"
									nav
									style={{
										color: "black",
										fontWeight: "bolder",
										fontSize: "large",
										// background: `black`,
										// borderRadius: "100%",
									}}
								>
									<i class="fa fa-ellipsis-v" aria-hidden="true" style={{ cursor: "pointer" }}></i>
								</DropdownToggle>
								<DropdownMenu persist aria-labelledby="navbarDropdownMenuLink" right>
									<DropdownItem
										onClick={() => {
											setEdited(availableWeeklyScheduleElements?.others?.byId[hrId]);
											toggleCreateModal();
										}}
									>
										<RFlex
											styleProps={{
												alignItems: "center",
												justifyContent: "space-between",
												// ...(action.color && { color: action.color })
											}}
										>
											<i class="fa fa-pen" aria-hidden="true" />
											<span>Edit</span>
										</RFlex>
									</DropdownItem>

									<DropdownItem
										onClick={() => {
											deleteOtherWeeklyScheduleElement(hrId);
										}}
									>
										<RFlex
											styleProps={{
												alignItems: "center",
												justifyContent: "space-between",
												// ...(action.color && { color: action.color })
											}}
										>
											<i className={iconsFa6.delete} aria-hidden="true" />
											<span>Delete</span>
										</RFlex>
									</DropdownItem>
								</DropdownMenu>
							</UncontrolledDropdown>
						</RFlex>
					),
					id: "other" + hrId,
					draggableId: "other_" + hrId + "_" + availableWeeklyScheduleElements?.others?.byId[hrId].name,
				})) ?? []),
			],
		},
	];

	return (
		<React.Fragment>
			<CreateOtherModal
				isOpen={createModalIsOpen}
				toggle={toggleCreateModal}
				edited={edited}
				handleSave={
					edited
						? (data) => updateOtherWeeklyScheduleElement(edited.id, data) && setEdited(null)
						: (data) => addOtherWeeklyScheduleElement(data)
				}
			/>
			<TowLevelsAccordion items={items} />
		</React.Fragment>
	);
};

export default WeeklyScheduleEditor;
