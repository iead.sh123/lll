import React from "react";
import {
	loadWeeklyScheduleEvents,
	getAvailableWeeklyScheduleElements,
	addOtherElementType,
	updateOtherElementType,
	deleteOtherElementType,
	addToPayload,
	saveWeeklySchedule,
} from "./state/terms-manager.actions";
import { initialState, reducer } from "./state/terms-management.reducer";
import { elementsOpType } from "./constants/elements.optype.constant";
import { pageMapping } from "./pages";
import { pages } from "./constants/pages.constant";
import Loader from "utils/Loader";
import { useDispatch } from "react-redux";
import { checkIfWeeklyScheduleHaveEvents } from "store/actions/global/schoolManagement.action";
import { useSelector } from "react-redux";

const TermManagement_1 = ({ academicYearId, termId, gradeLevelId, show, closeModal }) => {
	const dispatchTerm = useDispatch();
	const mounted = React.useRef();

	const [data, dispatch] = React.useReducer(reducer, initialState);

	console.log("datadatadata 122333333", data.entities.weeklyScheduleElements);
	const [loading, setLoading] = React.useState(false);
	const { weeklyScheduleHaveEvents } = useSelector((state) => state.schoolManagementRed);

	React.useEffect(() => {
		if (data?.entities?.weeklyScheduleElements?.ids?.length > 0) {
			dispatchTerm(checkIfWeeklyScheduleHaveEvents(true));
		} else if (data?.entities?.weeklyScheduleElements?.ids?.length == 0 && show) {
			dispatchTerm(checkIfWeeklyScheduleHaveEvents(false));
		} else if (!show) {
			dispatchTerm(checkIfWeeklyScheduleHaveEvents(false));
		}
	}, [data, termId, gradeLevelId, show]);

	React.useEffect(() => {
		mounted.current = true;

		return () => {
			mounted.current = false;
		};
	}, []);

	const handleLoadWeeklyScheduleElements = async () => {
		setLoading(true);
		await loadWeeklyScheduleEvents(dispatch, mounted, termId, gradeLevelId);
		setLoading(false);
	};

	const handleDeleteWeeklyScheduleElement = async (evId) => {
		return addToPayload(dispatch, elementsOpType.DELETE, evId);
	};

	const loadAvailableWeeklyScheduleElements = async () => {
		setLoading(true);
		await getAvailableWeeklyScheduleElements(dispatch, mounted, gradeLevelId, termId);
		setLoading(false);
	};

	const addOtherWeeklyScheduleElement = async (data) => {
		setLoading(true);
		await addOtherElementType(dispatch, mounted, data);
		setLoading(false);
	};

	const handleAddWeeklyScheduleElement = async (payload) => {
		return addToPayload(dispatch, elementsOpType.ADD, payload);
	};

	const updateOtherWeeklyScheduleElement = async (id, data) => {
		setLoading(true);
		//push in events to update with id
		await updateOtherElementType(dispatch, mounted, id, data);
		setLoading(false);
	};

	const deleteOtherWeeklyScheduleElement = async (id) => {
		setLoading(true);
		await deleteOtherElementType(dispatch, mounted, id);
		setLoading(false);
	};

	const handleUpdateWeeklyScheduleElement = async (id, payload) => {
		return addToPayload(dispatch, elementsOpType.UPDATE, { ...payload, id });
	};

	const handleSaveWeeklySchedule = async () => {
		if (loading || (!data.weeklySchedulePayload.elements.length && !data.weeklySchedulePayload.elementIdsToDelete.length)) return;

		setLoading(true);
		await saveWeeklySchedule(dispatch, mounted, gradeLevelId, termId, {
			...data.weeklySchedulePayload,
			elements: data.weeklySchedulePayload.elements.map((el) => ({ ...el, id: isNaN(el.id) ? undefined : +el.id })),
		});
		setLoading(false);
		// handleLoadWeeklyScheduleElements();
		// loadAvailableWeeklyScheduleElements();

		closeModal();
	};

	const pageProps = {
		[pages.WEEKLY_SCHEDULE]: {
			periods: data.entities.periods,
			events: data.entities.weeklyScheduleElements,
			handleLoadWeeklyScheduleElements,
			loadAvailableWeeklyScheduleElements,
			handleDeleteWeeklyScheduleElement,
			availableWeeklyScheduleElements: data.entities.availableWeeklyScheduleElements,
			addOtherWeeklyScheduleElement,
			updateOtherWeeklyScheduleElement,
			deleteOtherWeeklyScheduleElement,
			handleAddWeeklyScheduleElement,
			handleUpdateWeeklyScheduleElement,
			handleSaveWeeklySchedule,
			saveDisabled: loading || (!data.weeklySchedulePayload.elements.length && !data.weeklySchedulePayload.elementIdsToDelete.length),
			// disableActions: weeklyScheduleHaveEvents,
			termId,
			gradeLevelId,
			show,
			weeklyScheduleHaveEvents,
		},
	};

	const Page = pageMapping[pages.WEEKLY_SCHEDULE];
	return (
		<React.Fragment>
			<React.Suspense fallback={<Loader />}>
				<Page {...pageProps["WEEKLY_SCHEDULE"]} loading={loading} />
			</React.Suspense>
		</React.Fragment>
	);
};

export default TermManagement_1;
