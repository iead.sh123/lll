import React from "react";
import RProfileName from "components/Global/RComs/RProfileName/RProfileName";
import RHoverInput from "components/Global/RComs/RHoverInput/RHoverInput";
import iconsFa6 from "variables/iconsFa6";
import RLister from "components/Global/RComs/RLister";
import RTitle from "components/RComponents/RTitle";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { collaborationApi } from "api/global/collaboration";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useMutateData } from "hocs/useMutateData";
import { useSelector } from "react-redux";
import { dangerColor } from "config/constants";
import { useFormik } from "formik";
import * as yup from "yup";

const GLevels = () => {
	const { user } = useSelector((state) => state?.auth);

	// ------------------ Start Formik ------------------
	const validationSchema = yup.object({
		levels: yup.array().of(
			yup.object({
				name: yup.string().required(tr`name is required`),
				description: yup.string().required(tr`description is Required`),
			})
		),
	});

	const initialStates = {
		levels: [],
		idsForDeletion: [],
		enterPressed: false,
	};

	const { values, errors, touched, setFieldValue, setTouched, setErrors, setFieldError, setFieldTouched, handleChange, handleBlur } =
		useFormik({
			initialValues: initialStates,
			validationSchema: validationSchema,
		});

	// ------------------ End Formik ------------------

	// ------------------ Start Queries ------------------

	const { data, isLoading, isFetching, isError } = useFetchDataRQ({
		queryKey: ["levels"],
		queryFn: () => collaborationApi.readLevels(),
		onSuccessFn: ({ data }) => {
			if (data && data.data && data.data.data) {
				const modifiedData = data.data.data.map((item) => ({
					...item,
					saved: { name: true, description: true },
					focus: { name: true, description: true },
				}));
				setFieldValue("levels", modifiedData);
			}
		},
	});

	const createLevelMutate = useMutateData({
		queryFn: (data) => collaborationApi.createLevel(data),
		invalidateKeys: ["levels"],
		onSuccessFn: ({ data }) => {
			const index = values.levels.findIndex((obj) => obj.fakeId && obj.fakeId < 0);
			values.levels[index].id = data?.data?.id;
			values.levels[index].saved = { name: true, description: true };
			delete values.levels[index].fakeId;
			setFieldValue("enterPressed", false);
		},
		onErrorFn: () => {
			setFieldValue("enterPressed", false);
		},
	});

	// in update we don't need to invalidateKeys
	const updateLevelMutate = useMutateData({
		queryFn: ({ levelId, data }) => collaborationApi.updateLevel(levelId, data),
		onSuccessFn: ({ data, variables }) => {
			const index = values.levels.findIndex((obj) => obj.id == variables.levelId);
			values.levels[index].saved = { name: true, description: true };
			setFieldValue("enterPressed", false);
		},
		onErrorFn: () => {
			setFieldValue("enterPressed", false);
		},
	});

	const deleteLevelMutate = useMutateData({
		queryFn: (levelId) => collaborationApi.deleteLevel(levelId),
		invalidateKeys: ["levels"],
		onSuccessFn: ({ data: data, variables: variables }) => {
			const newData = values.idsForDeletion.filter((el) => el !== variables);
			const filteredArray = values.levels?.filter((item) => {
				const itemId = item.id ? item.id : item.fakeId;
				return itemId != variables;
			});

			// setFieldValue("idsForDeletion", newData);
			setFieldValue("levels", filteredArray);
		},
	});

	// ------------------ End Queries ------------------

	// ------------------ Start Handle Actions ------------------

	const handleAddLevel = () => {
		const newObject = {
			fakeId: -values?.levels.length - 1,
			name: "",
			description: "",
			order: values?.levels.length + 1,
			saved: { name: false, description: false },
		};

		setFieldValue("levels", [newObject, ...values.levels]);
	};

	// To Switch Between To Formate Input-Label
	const handleEditFunctionality = (event, item, { propertyName, index }, savedFlag = false) => {
		const newArray = JSON.parse(JSON.stringify(values.levels));
		newArray[index].saved[propertyName] = savedFlag;
		setFieldValue("levels", [...newArray]);
	};

	const handleInputSaved = (event, item, { propertyName, index }) => {
		if (event.key == "Enter" || !event.key) {
			if (values.enterPressed) return;
			if (errors?.levels?.[index]) {
				// if (errors?.levels?.[index]?.[propertyName]) {
				// 	return;
				// } else {
				// 	handleEditFunctionality({ propertyName, index }, true);
				// 	return;
				// }
			} else if (!errors.levels?.[index] && values.levels?.[index].id) {
				setFieldValue("enterPressed", true);
				updateLevelMutate.mutate({ levelId: item.id, data: values.levels?.[index] });
			} else {
				setFieldValue("enterPressed", true);
				const payload = {
					fakeId: values.levels?.[index].fakeId,
					name: values.levels?.[index].name,
					description: values.levels?.[index].description,
					order: values.levels?.[index].order,
				};
				createLevelMutate.mutate(payload);
			}
		}
	};

	const handleDeleteLevel = (levelId) => {
		if (levelId > 0) {
			setFieldValue("idsForDeletion", [...values.idsForDeletion, levelId]);
			deleteLevelMutate.mutate(levelId);
		} else {
			const filteredArray = values.levels?.filter((item) => {
				const itemId = item.id ? item.id : item.fakeId;
				return itemId != levelId;
			});

			setFieldValue("levels", filteredArray);
		}
	};

	// ------------------ End Handle Actions ------------------

	const CreatorCom = ({ item }) => {
		return (
			<RProfileName
				key={item.id ? item.id : item.fakeId}
				img={item.creator ? item.creator.image : user?.image}
				name={item.creator ? item.creator.name : `${user?.first_name + " " + user?.last_name}`}
			/>
		);
	};

	const _records = values?.levels?.map((rc, index) => {
		const touched = touched?.["levels"]?.[index];
		const error = errors?.["levels"]?.[index];

		return {
			details: [
				{
					key: tr`level`,
					value: (
						<div style={{ position: "relative" }}>
							<RHoverInput
								handleEditFunctionality={handleEditFunctionality}
								handleInputChange={handleChange}
								handleInputDown={handleInputSaved}
								handleOnBlur={handleInputSaved}
								name={`levels.${index}.name`}
								inputValue={values.levels[index]?.name}
								type={"text"}
								focusOnInput={true}
								inputPlaceHolder={tr("name")}
								inputIsNotValidate={error?.["name"]}
								item={rc}
								extraInfo={{
									propertyName: "name",
									index: index,
								}}
								saved={values.levels[index].saved?.name}
							/>
							{error?.["name"] && <i className={`${iconsFa6.exclamationCircle} fa-md validation__icon`} style={{ color: dangerColor }} />}
						</div>
					),
					type: "component",
				},
				{ key: tr`creator_by`, value: <CreatorCom item={rc} />, type: "component" },
				{
					key: tr`description`,
					value: (
						<div style={{ position: "relative" }}>
							<RHoverInput
								handleEditFunctionality={handleEditFunctionality}
								handleInputChange={handleChange}
								handleInputDown={handleInputSaved}
								handleOnBlur={handleInputSaved}
								name={`levels.${index}.description`}
								inputValue={values.levels[index]?.description}
								type={"text"}
								focusOnInput={false}
								inputPlaceHolder={tr("description")}
								inputIsNotValidate={error?.["description"]}
								item={rc}
								extraInfo={{
									propertyName: "description",
									index: index,
								}}
								saved={values.levels[index].saved?.description}
							/>
							{error?.["description"] && (
								<i className={`${iconsFa6.exclamationCircle} fa-md validation__icon`} style={{ color: dangerColor }} />
							)}
						</div>
					),
					type: "component",
				},
			],
			actions: [
				{
					justIcon: true,
					actionIconStyle: { color: dangerColor },
					icon: iconsFa6.delete,
					loading: values.idsForDeletion.includes(rc.id ? rc?.id : rc.fakeId) ? true : false,
					disabled: values.idsForDeletion.includes(rc.id ? rc?.id : rc.fakeId) ? true : false,
					onClick: () => handleDeleteLevel(rc.id ? rc.id : rc.fakeId),
				},
			],
		};
	});

	if (isLoading) return <Loader />;
	return (
		<RFlex styleProps={{ flexDirection: "column" }}>
			<RTitle
				text={tr`levels`}
				count={values.levels?.length}
				actionHandler={{ handleClick: handleAddLevel, name: tr`add_level` }}
				disabled={errors?.levels || createLevelMutate.isLoading || isLoading || updateLevelMutate.isLoading ? true : false}
			/>
			<RLister Records={_records} removeImage={true} line1={tr`no_levels_yet`} />
		</RFlex>
	);
};

export default GLevels;
