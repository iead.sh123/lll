import React from "react";
import RButton from "components/Global/RComs/RButton";
import Loader from "utils/Loader";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import tr from "components/Global/RComs/RTranslator";
import { collaborationApi } from "api/global/collaboration";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";
import { useMutateData } from "hocs/useMutateData";
import { useFormik } from "formik";
import * as yup from "yup";
import RAddAgreement from "view/Collaboration/CollaborationAgreement/RAddAgreement";
import { useSelector } from "react-redux";

const GAddAgreement = ({ agreementId, handleCloseEditorAgreement }) => {
	const { user } = useSelector((state) => state?.auth);
	// ------------------ Formik ------------------
	const initialValues = {};
	const formSchema = yup.object().shape({});
	const handleFormSubmit = (values) => {};

	const { values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue, resetForm } = useFormik({
		initialValues,
		onSubmit: handleFormSubmit,
		validationSchema: formSchema,
	});

	// ------------------ Queries ------------------
	const readAgreementData = useFetchDataRQ({
		queryKey: ["read-agreement"],
		queryFn: () => collaborationApi.readAgreement(agreementId),
		onSuccessFn: ({ data }) => {},
		enableCondition: agreementId ? true : false,
	});

	const agreementsListData = useFetchDataRQ({
		queryKey: ["agreements-list"],
		queryFn: () => collaborationApi.agreementsList(user?.organization_id),
		onSuccessFn: ({ data }) => {},
		enableCondition: user.organization_id ? true : false,
	});

	const organizationsData = useFetchDataRQ({
		queryKey: ["organizations"],
		queryFn: () => collaborationApi.allOrganizations(),
		onSuccessFn: ({ data }) => {},
	});

	const levelsData = useFetchDataRQ({
		queryKey: ["levels"],
		queryFn: () => collaborationApi.allLevels(user?.organization_id),
		onSuccessFn: ({ data }) => {},
		enableCondition: user.organization_id ? true : false,
	});

	const createAgreementMutate = useMutateData({
		queryFn: (data) => collaborationApi.createAgreement(data),
		invalidateKeys: ["clients"],
		onSuccessFn: ({ data }) => {},
	});

	const updateAgreementMutate = useMutateData({
		queryFn: (data, agreementId) => collaborationApi.updateAgreement(data, agreementId),
		invalidateKeys: ["clients"],
		onSuccessFn: ({ data }) => {},
	});

	if (
		(readAgreementData.isLoading && readAgreementData.fetchStatus !== "idle") ||
		agreementsListData.isLoading ||
		organizationsData.isLoading ||
		levelsData.isLoading
	)
		return <Loader />;

	return (
		<form onSubmit={handleSubmit}>
			<RAddAgreement
				formProperties={{ values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue, resetForm }}
				data={{
					organizations: organizationsData?.data?.data?.data?.data,
					levels: levelsData?.data?.data?.data?.data,
					agreementsList: agreementsListData?.data?.data?.data?.data,
				}}
				loading={{
					organizationsLoading: organizationsData.isLoading,
					levelsLoading: levelsData.isLoading,
					updateAgreementLoading: updateAgreementMutate.isLoading,
					createAgreementLoading: createAgreementMutate.isLoading,
				}}
				handlers={{ handleCloseEditorAgreement: handleCloseEditorAgreement }}
				agreementId={agreementId}
			/>
		</form>
	);
};

export default GAddAgreement;
