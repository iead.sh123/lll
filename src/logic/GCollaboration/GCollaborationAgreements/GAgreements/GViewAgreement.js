import React from "react";
import Loader from "utils/Loader";
import { collaborationApi } from "api/global/collaboration";
import { useFetchDataRQ } from "hocs/useFetchDataRQ";

const GViewAgreement = ({ agreementId, handleCloseViewAgreement }) => {
	// ------------------ Queries ------------------
	const readAgreementData = useFetchDataRQ({
		queryKey: ["read-agreement"],
		queryFn: () => collaborationApi.readAgreement(agreementId),
		onSuccessFn: ({ data }) => {},
	});

	if (readAgreementData.isLoading) return <Loader />;

	return <div>GViewAgreement</div>;
};

export default GViewAgreement;
