import React from "react";
import GAgreements from "./GAgreements";
import GProviders from "./GProviders";
import RFlex from "components/Global/RComs/RFlex/RFlex";

const GCollaborationAgreements = () => {
	return (
		<RFlex styleProps={{ gap: 35, flexDirection: "column", marginBottom: 10 }}>
			<GAgreements />
			<GProviders />
		</RFlex>
	);
};

export default GCollaborationAgreements;
