import axios from "axios";
import store from "store";
import { newAuth, newAuthToken } from "engine/config";
import { ObjToFormData } from "utils/objToFormData";

const apiClient = axios.create({
	baseURL: "", //process.env.REACT_APP_BACKEND_URL ,
	headers: {
		Accept: "application/json",
		"Content-Type": "application/json",
	},
});

function resolved(result) {}

function rejected(result) {
	console.error(result);
}

apiClient.interceptors.request.use(
	(config) => {
		if (newAuth) {
			config.headers["Authorization"] = `Bearer ${newAuthToken}`;
		} else if (store.getState().auth.token && config.url !== "api/auth/login") {
			config.headers["Authorization"] = config.newAuth ? `Bearer ${newAuthToken}` : `Bearer ${store.getState().auth.token}`;
			config.headers["lang"] = localStorage.getItem("lang");
		}
		if (config.formData) {
			config.data = ObjToFormData(config.data);
			// Set Content-Type to undefined to let Axios set it automatically for FormData
			delete config.headers["Content-Type"];
		}

		config.headers["app-id"] = "new_courses";

		return config;
	},
	(error) => {
		return Promise.reject(error);
	}
);

// apiClient.interceptors.response.use(
// 	(response) => response,
// 	(error) => {
// 		if (error.response) {
// 			if (error.response.status === 401) {
// 				// navigate('/auth/sign-in');
// 			} else {
// 				// Handle other response errors
// 				console.error("Response error:", error.response);
// 			}
// 		} else if (error.request) {
// 			// Handle request error
// 			console.error("Request error:", error.request);
// 		} else {
// 			// Handle other errors
// 			console.error("Error:", error.message);
// 		}
// 		return Promise.reject(new Error("Network fail"));
// 	}
// );

apiClient.interceptors.response.use(
	(response) => response,
	(error) => {
		if (error) {
			if (error?.response) {
				// if (error.response.status === 401 &&  config.url.includes("api/auth/me")) {
				//   persistor.purge();
				//   history.push(process.env.REACT_APP_BASE_URL + "/auth/login");
				//   location.reload();
				// }
			}

			return Promise.reject(new Error("Netowrk fail")).then(resolved, rejected);
		}
		return Promise.reject(new Error("Netowrk fail")).then(resolved, rejected);
	}
);

const { get, post, put, delete: destroy, patch } = apiClient;
export { get, post, put, destroy, patch };

export default apiClient;
