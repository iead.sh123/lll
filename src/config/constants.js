export const baseUrl = process.env.REACT_APP_BASE_URL;
export const backendUrl = process.env.REACT_APP_BACKEND_URL;
export const resourcesBaseUrl = backendUrl + "api/files/get/";

// Keys
export const CART_ITEMS = "cart-items";

// Colors
export const primaryColor = "#668AD7";
export const secondaryColor = "#668AD7";
export const thirdColor = "#fd832c";
export const successColor = "#46c37e";
export const dangerColor = "#DD0000";
export const warningColor = "#F58B1F";
export const lightBlueColor = "#EEF2FB";
export const linkColor = "#007bff";
export const greyColor = "#818181";
export const boldGreyColor = "#585858";
export const strokeColor = "#d9d9d9";
export const lightGray = "#9C9C9C";
export const dialogGray = "#71717A";
export const DATE_FORMATE = "MMMM Do YYYY, h:mm:ss a";
export const DATE_FORMATE_WITHOUT_TIME = "YYYY-MM-DD";
export const tagsColor = ["#FF0000", "#008000", "#FFFF00", "#0000FF", "#FFA500", "#800080", "#808080"];
//Random Colors
export const RANDOM_COLOR = [
	{ textColor: "#0051FE", backgroundColor: "#D9E5FE" },
	{ textColor: "#F34400", backgroundColor: "#FEF1EC" },
	{ textColor: "#EB2578", backgroundColor: "#FDECF2" },
	{ textColor: "#F59F00", backgroundColor: "#FFF9DB" },
	{ textColor: "#4260DE", backgroundColor: "#E3E7FA" },
	{ textColor: "#7C7C7C", backgroundColor: "#F2F2F2" },
	{ textColor: "#B0A305", backgroundColor: "#FBF5C3" },
	{ textColor: "#DB2838", backgroundColor: "#FFE4D7" },
	{ textColor: "#5F3DC4", backgroundColor: "#F3F0FF" },
	{ textColor: "#099268", backgroundColor: "#E6FCF5" },
	{ textColor: "#764D2B", backgroundColor: "#EAD7C6" },
];

export const Folder_Color = {
	blue: { topColor: "#739DB2", gradientColorOne: "#9AD0EC", gradientColorTow: "#60A1C2" },
	green: { topColor: "#14C38E", gradientColorOne: "#00FFAB", gradientColorTow: "#05B97D" },
	orange: { topColor: "#EC8327", gradientColorOne: "#FFCD38", gradientColorTow: "#F1882C" },
	blue2: { topColor: "#496EE9", gradientColorOne: "#56BBF1", gradientColorTow: "#4D77FF" },
	red: { topColor: "#E8000E", gradientColorOne: "#FF5403", gradientColorTow: "#F90716" },
	purple: { topColor: "#7000EC", gradientColorOne: "#B983FF", gradientColorTow: "#F90716" },
	yellow: { topColor: "#FBBC1A", gradientColorOne: "#FFE69C", gradientColorTow: "#FFC937" },
};
export const CharactersToRandomize = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
