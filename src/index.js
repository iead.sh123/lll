import App from "App";
import React from "react";
import ReactDOM from "react-dom";
import store, { persistor } from "store/index.js";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { ReactQueryDevtools } from "@tanstack/react-query-devtools";
import { ToastContainer } from "react-toastify";
import { BrowserRouter } from "react-router-dom";
import { PersistGate } from "redux-persist/integration/react";
import { Provider } from "react-redux";
import "bootstrap/dist/css/bootstrap.css";
import "assets/scss/paper-dashboard.scss?v=1.3.0";
import "./index.css";
import "perfect-scrollbar/css/perfect-scrollbar.css";
import "react-toastify/dist/ReactToastify.css";

const queryClient = new QueryClient();

ReactDOM.render(
	<Provider store={store}>
		<PersistGate loading={null} persistor={persistor}>
			<QueryClientProvider client={queryClient}>
				<BrowserRouter>
					<App />
				</BrowserRouter>
				<ReactQueryDevtools initialIsOpen={false} position="bottom-right" />
			</QueryClientProvider>
		</PersistGate>
		<ToastContainer
			position="top-center"
			autoClose={2500}
			limit={2}
			hideProgressBar={false}
			newestOnTop={false}
			closeOnClick
			rtl={false}
			pauseOnFocusLoss
			draggable
			pauseOnHover
			theme="light"
		/>
	</Provider>,

	document.getElementById("root")
);

// ReactDOM.createRoot(document.getElementById("root")).render(
// 	<React.StrictMode>
// 		<Provider store={store}>
// 			<PersistGate loading={null} persistor={persistor}>
// 				<QueryClientProvider client={queryClient}>
// 					<BrowserRouter>
// 						<App />
// 					</BrowserRouter>
// 					<ReactQueryDevtools initialIsOpen={false} position="bottom-right" />
// 				</QueryClientProvider>
// 			</PersistGate>
// 		</Provider>
// 	</React.StrictMode>
// );
