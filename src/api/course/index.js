import { post, get, put, destroy } from "config/api";
import { Services } from "engine/services";

export const courseApi = {
	// - - - - - - - -  Department - - - - - - - -
	courseDepartments: () => get(`${Services.course.backend}api/course/departments`),
	createDepartment: (data) => post(`${Services.course.backend}api/course/departments`, data),
	updateDepartment: ({ departmentId, data }) => put(`${Services.course.backend}api/course/departments/${departmentId}`, data),
	deleteDepartment: (departmentId) => destroy(`${Services.course.backend}api/course/departments/${departmentId}`),

	// - - - - - - - -  Master Course - - - - - - - -
	masterCourses: (params) => get(`${Services.course.backend}api/course/master-courses`, { params }),
	createMasterCourse: ({ templateId, data }) =>
		post(`${Services.course.backend}api/course/templates/${templateId}/master-courses`, data, { formData: true }),
	updateMasterCourse: ({ masterCourseId, data }) =>
		put(`${Services.course.backend}api/course/master-courses/${masterCourseId}`, data, { formData: true }),
	deleteMasterCourse: (masterCourseId) => destroy(`${Services.course.backend}api/course/master-courses/${masterCourseId}`),
	getMasterCourseById: (masterCourseId) => get(`${Services.course.backend}api/course/master-courses/${masterCourseId}`),
	getMasterCourseLinks: (masterCourseId) => get(`${Services.course.backend}api/course/master-courses/${masterCourseId}/links`),
	getMasterCourseBooks: (masterCourseId) => get(`${Services.course.backend}api/course/master-courses/${masterCourseId}/books`),
	getMasterCourseFiles: (masterCourseId) => get(`${Services.course.backend}api/course/master-courses/${masterCourseId}/files`),
	createMasterCourseLink: (masterCourseId, payload) =>
		post(`${Services.course.backend}api/course/master-courses/${masterCourseId}/links`, payload),
	createMasterCourseBookEndpoint: (masterCourseId) => `${Services.course.backend}api/course/master-courses/${masterCourseId}/books`,
	createMasterCouresFileEndpoint: (masterCourseId) => `${Services.course.backend}api/course/master-courses/${masterCourseId}/files`,
	getMasterCourseSyllabus: (masterCourseId) => get(`${Services.course.backend}api/course/master-courses/${masterCourseId}/syllabus`),
	createMasterCourseSyllabus: (masterCourseId, payload) =>
		post(`${Services.course.backend}api/course/master-courses/${masterCourseId}/syllabus`, payload),
	deleteMasterCourseFile: (fileId) => destroy(`${Services.course.backend}api/course/master-courses/files/${fileId}`),
	deleteMasterCourseBook: (bookId) => destroy(`${Services.course.backend}api/course/master-courses/books/${bookId}`),
	deleteMasterCourseLink: (linkId) => destroy(`${Services.course.backend}api/course/master-courses/links/${linkId}`),

	clonedCourse: ({ courseId, data }) => post(`${Services.course.backend}api/course/master-courses/${courseId}/courses`, data),

	//------------------------Course-----------------
	getCourseById: (courseId) => get(`${Services.course.backend}api/course/courses/${courseId}`),
	getCourseLinks: (courseId) => get(`${Services.course.backend}api/course/courses/${courseId}/links`),
	getCourseBooks: (courseId) => get(`${Services.course.backend}api/course/courses/${courseId}/books`),
	getCourseFiles: (courseId) => get(`${Services.course.backend}api/course/courses/${courseId}/files`),
	createCourseLink: (courseId, payload) => post(`${Services.course.backend}api/course/courses/${courseId}/links`, payload),
	createCourseBookEndPoint: (courseId) => `${Services.course.backend}api/course/courses/${courseId}/books`,
	createCourseFileEndpoint: (courseId) => `${Services.course.backend}api/course/courses/${courseId}/files`,
	getCourseSyllabus: (courseId) => get(`${Services.course.backend}api/course/courses/${courseId}/syllabus`),
	createCourseSyllabus: (courseId, payload) => post(`${Services.course.backend}api/course/courses/${courseId}/syllabus`, payload),
	deleteCourseFile: (fileId) => destroy(`${Services.course.backend}api/course/courses/files/${fileId}`),
	deleteCourseBook: (bookId) => destroy(`${Services.course.backend}api/course/courses/books/${bookId}`),
	deleteCourseLink: (linkId) => destroy(`${Services.course.backend}api/course/courses/links/${linkId}`),
};
