import { get, post, put, destroy } from "config/api";

export const socialMediaApi = {
  getAllPost: (pageNumber) =>
    get(`social/QueryAccessibilityPosts?page=${pageNumber}`),
  // getAllPost: () => get("social/QueryAccessibilityPosts"),
  NewPost: (data) => post("social/me/savePost", data),
  addComment: (data) => post("social/me/saveComment", data),
  writeLike: (data) => post("social/me/saveLike", data),
  editPost: (FormData, PostId) => put(`social/post/update/${PostId}`, FormData),
  removePost: (PostId) => destroy(`social/post/${PostId}`),
  removeComment: (CommentId) => destroy(`social/me/deleteComment/${CommentId}`),
  editComment: (FormData, CommentId) =>
    put(`social/me/updateComment/${CommentId}`, FormData),
  moreComments: (postId, Next) => get(`social/getComments/${postId}/${Next}`),
  moreLikes: (contentId, Next) =>
    get(`social/fetchnextLikes/${contentId}/${Next}`),

  pagenation: (url) => get(url),
  specificpost: (urlPostId) =>
    get(`social/posts/getPost?url_post_id=${urlPostId}`),
  filtringTag: (tag) => get(`social/tag/posts?tag_name=${tag}`),

  // searchPost: (query) => get(`social/posts/search?query=${query}`),
  searchPost: (query, pageNumber) =>
    get(`social/posts/search?page=${pageNumber}&query=${query}`),
  AccessLevels: (text) => get(`social/all/AccessLevelsByUser/${text}`),

  postByUser: (UserId) => get(`social/posts/getPostsUserId?user_id=${UserId}`),
};
