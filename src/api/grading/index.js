import { get, post, put, destroy, patch } from "config/api";
import { Services } from "engine/services";

export const gradingApi = {

  // managing groups

  gradingGroupsListByCourse: (courseId, forDropDownList = false, page) => get(`${Services.grading.backend}grading-groups/courses/${courseId}?page=${page}${forDropDownList ? '&forDropDownList=true' : ''}`),
  addGradingGroup: (courseId, payload) => post(`${Services.grading.backend}grading-groups/courses/${courseId}`, payload),
  deleteGradingGroup: (id) => destroy(`${Services.grading.backend}grading-groups/${id}`),
  updateGradingGroup: (id, payload) => patch(`${Services.grading.backend}grading-groups/${id}`, payload),


  // managing items
  gradableItemsListByCourse: (courseId, exceptId, exceptType, page, searchText="") => get(`${Services.grading.backend}grading-groups/course/${courseId}/groups/gradable-items?includeUnlinked=true&page=${page}searchText=${searchText}${exceptType ? `&exceptType=${exceptType}` : ''}${exceptId ? `&exceptId=${exceptId}` : ''}`),
  assignGradableItemToGroup: (type, itemId, groupId) => put(`${Services.grading.backend}graddable-items/type/${type}/${itemId}/groups/${groupId}`),
  updateGraddableItem: (type, id, payload) => patch(`${Services.grading.backend}graddable-items/type/${type}/${id}`, payload),
  deleteGraddableItem: (id) => destroy(`${Services.grading.backend}graddable-items/type/custom/${id}`),
  loadItemAlternatives: (type, id) => get(`${Services.grading.backend}graddable-items/type/${type}/${id}/alternatives`),
  addAndRemoveAlternatives: (type, itemId, payload) => post(`${Services.grading.backend}graddable-items/type/${type}/${itemId}/alternatives/bulk`, payload),
  RemoveAlternative: (itemId, type) => destroy(`${Services.grading.backend}graddable-items/alternative/type/${type}/${itemId}`),
  getCandidateALternatives: () => get(`${Services.grading.backend}graddable-items/type/{type}/{itemId}/alternatives`),
  createCustomGradableItem: (courseId, payload) => post(`${Services.grading.backend}graddable-items/courses/${courseId}/customs`, payload),
  //-----------------------------------------------Final Marks---------------------------------
  getStudentsFinalMarksForCourse: (courseId, orderBy = '') => get(`${Services.grading.backend}student-gradable-items/student-course-details/courses/${courseId}?orderBy=${orderBy}`),
  calculateStudentsFinalMarks: (courseId) => post(`${Services.grading.backend}student-gradable-items/student-course-details/courses/${courseId}/re-calculate`),
  updateTeacherMarkForStudent: (payload) => patch(`${Services.grading.backend}student-gradable-items/student-course-details/update-course-teacher-grades`, payload),
  //-----------------------------------------student items
  getAllStudentsDetailsInGradableItem: (type, itemId, orderBy) => get(`${Services.grading.backend}student-gradable-items/students-details-list/type/${type}/${itemId}?orderBy=${orderBy}`),

  //------------------------------------------Student Details Slider --------------------------------
  getStudentDetailsForGroup: (courseId, studentId) => get(`${Services.grading.backend}student-gradable-items/courses/${courseId}/students/${studentId}/details/groups`),
  updateTeacherGrdableStudentMark: (payload) => patch(`${Services.grading.backend}student-gradable-items/teacher-marks`, payload),
  //-------------------------------Manage students stuff
  //----------------------------------Student GradeBook----------------------------
  getAllOrganizationTerms: () => get(`${Services.terms.backend}terms/organization/all`),
  getTermData: (termId) => get(`${Services.grading.backend}student-gradable-items/my-grade-book/terms/${termId}`),
  batchStudentItemGrade: (type, itemId, student_id, grade) =>
    patch(`${Services.grading.backend}student-gradable-items/teacher-marks`
      ,
      {
        "teacherGrades": [
          {
            "mark": +grade,
            "type": type,
            "studentId": +student_id,
            "gradableItemId": +itemId
          }
        ]
      }
    )





};
