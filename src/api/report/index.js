import { get, post } from "config/api";

export const reportApi = {
  getAllReport: () => get("api/reports/getAll"),
  getReportParameter: (ReportId) =>
    get(`api/reports/report-parameters/${ReportId}`),
  viewReportGridView: (ReportId, ParametersValue) =>
    post(`api/reports/call-report/${ReportId}`, ParametersValue),
  viewReportById: (ReportId) => get(`api/reports/${ReportId}`),
};
