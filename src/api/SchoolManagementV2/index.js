import { get, post, patch, destroy, put } from "config/api";
import { Services } from "engine/services";
const sc_get = (route) => get(`${Services.organizations.backend}${route}`);
const sc_post = (route, payload) => post(`${Services.organizations.backend}${route}`, payload);
const sc_delete = (route) => destroy(`${Services.organizations.backend}${route}`);
const sc_put = (route, payload) => put(`${Services.organizations.backend}${route}`, payload);
const sc_patch = (route, payload) => patch(`${Services.organizations.backend}${route}`, payload);

export const schoolManagementV2API = {
    updateSchoolInforamtion: (payload) => sc_post('api/v1/organization/settings', payload),
    getOrganization: (organizationId) => sc_get(`api/v1/organization/${organizationId}`),
    getOrganizationTypes: () => sc_get(`api/v1/types`),
    createUpdateLevel: (payload, organization_id) => sc_post(`api/v1/levels/${organization_id}`, payload),
    deleteLevel: (levelId) => sc_delete(`api/v1/levels/${levelId}`),
    reOrderLevels: (payload) => sc_post(`api/v1/levels/reorder`, payload),
    getOutsideUsers: (levelId, userType) => sc_get(`api/v1/levels/${levelId}/outside-users?account_type_id=${userType}`),
    addUpdateContentToLevel: (payload) => sc_post(`api/v1/levels/content`, payload),
    getOutsideUsers: (levelId, userTypeId) => sc_get(`api/v1/levels/${levelId}/outside-users?account_type_id=${userTypeId}`),
    enrollUserToLevel: (levelId, payload) => sc_post(`api/v1/levels/${levelId}/user`, payload),
    getAllCategories: () => get(`${Services.courses_manager.backend}api/all-categories/true`),
    getCoursesByCategoryId: ({ curriculaId, name = '', category_id = null, noPagination = false, page }) => get(`${Services.courses_manager.backend}api/course-catalogue/except-curriculum/${curriculaId}?page=${page}&name=${name}&category_id=${category_id}&noPagination=${noPagination}`),
}