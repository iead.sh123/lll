import { get, post } from "config/api";
import { Services } from "engine/services";

export const tagApis = {
	search: (params) => get(`${Services.tag.backend}api/v1/tags`, { params }),
	colors: () => get(`${Services.tag.backend}api/v1/colors`),
};
