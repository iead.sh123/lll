import { get, post, patch, destroy, put } from "config/api";
import { Services } from "engine/services";
const dp_get = (route) => get(`${Services.departments_programs.backend}${route}`);
const dp_post = (route, payload) => post(`${Services.departments_programs.backend}${route}`, payload);
const dp_delete = (route, payload) => destroy(`${Services.departments_programs.backend}${route}`, payload);
const dp_put = (route, payload) => put(`${Services.departments_programs.backend}${route}`, payload);
const dp_patch = (route, payload) => patch(`${Services.departments_programs.backend}${route}`, payload);

export const departmentsProgramsApi = {
	getAllDepartemnts: () => dp_get(`api/v1/departments`),
};
