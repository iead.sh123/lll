import { get } from "config/api";

export const studentGradesAPis = {
  fetchUserCoursesGrades: (termId, userId) =>
    get(
      `api/studentrabs/studentGradingBook/${termId}${
        userId ? `/${userId}` : ""
      }`
    ),

  fetchReportTemplate: (userId) =>
    get(`api/report-template/cards${userId ? `/${userId}` : ""}`),

  fetchTerms: () => get(`api/studentrabs/school-terms`),
};
