import { post, get, destroy } from "config/api";
import { Services } from "engine/services";

export const authApi = {
	login: (data) => post(`${Services.authentication.backend}api/auth/login`, data),

	register: (data) => post(`${Services.auth_organization_management.backend}api/learner/register`, data),

	me: () => get(`${Services.auth_organization_management.backend}api/auth/me`),

	// refresh: () =>
	//   post(`${Services.auth_organization_management.backend}api/auth/refresh`),

	logout: () => post(`${Services.auth_organization_management.backend}api/auth/logout`),

	allOrganizations: () => get(`${Services.auth_organization_management.backend}api/allOrganizations`),

	updatePassword: (request) => post(`${Services.auth_organization_management.backend}api/auth/updatePassword`, request),

	getUserPhoto: () => get(`${Services.auth_organization_management.backend}api/auth/getProfileImage`),
	updateUserPhoto: (image) => post(`${Services.auth_organization_management.backend}api/auth/updateProfileImage`, image),
	deleteUserPhoto: () => destroy(`${Services.auth_organization_management.backend}api/auth/profileImage`),

	reset: (req) => post(`${Services.auth_organization_management.backend}api/auth/forgetpassword`, req),
	change_password: (req) => post(`${Services.auth_organization_management.backend}api/auth/resetpassword`, req),
	userIdentity: (user_id) => get(`${Services.auth_organization_management.backend}api/user/${user_id}/user-identity`),
};
