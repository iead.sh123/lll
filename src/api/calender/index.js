import { get, post, put, patch } from "config/api";
import { Services } from "engine/services";

export const calenderApi = {
	getEventsTypes: (contextId = null, contextName = null) =>
		get(
			`${Services.calender.backend}event-types?${contextName ? `contextName=${contextName}&` : ""}${
				contextId ? `contextId=${contextId}` : ""
			}`
		),
	createEventType: (payload) => post(`${Services.calender.backend}event-types`, payload),
	updateEventType: (eventTypeId, payload) => patch(`${Services.calender.backend}event-types/${eventTypeId}`, payload),
	getAllEvents: (contextId = null, contextName = null) =>
		get(
			`${Services.calender.backend}user-events?${contextName ? `contextName=${contextName}&` : ""}${
				contextId ? `contextId=${contextId}` : ""
			}`
		),
	// types: ()=>get(`${Services.calender.backend}event-types/blocks`),
	// blockEventTypes: (blockerId, organizationId, blockedIds) => put(`${Services.calender.backend}event-types/${blockerId}/organizations/${organizationId}/blocks`, {
	//   events: blockedIds
	// }),
	// saveRelations: (payload)=>put(`${Services.calender.backend}event-types/blocks`,payload),
	// comments: eventId => get(`${Services.calender.backend}comments/events/${eventId}`),
	// notes: eventId => get(`${Services.calender.backend}notes/${eventId}`),
	// addComment: (eventId,payload)  => post(`${Services.calender.backend}comments/events/${eventId}`,payload),
	// addNote: (eventId,payload) =>post(`${Services.calender.backend}notes/${eventId}`,payload),
	// print: payload=>post(`${Services.print.backend}api/JsonToPdf`,payload)
};
