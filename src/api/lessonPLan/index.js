import { get, post } from "config/api";
import { Services } from "engine/services";

export const lessonPlanApi = {
	getLessonPlans: (params) => get(`${Services.lesson_plan.backend}api/v1/lesson-plan/get`, { params }),
	getLessonPlan: (lessonPlanId) => get(`${Services.lesson_plan.backend}api/v1/lesson-plan/getLessonPlan/${lessonPlanId}`),
	create: (data) => post(`${Services.lesson_plan.backend}api/v1/lesson-plan/store`, data),
	deleteLessonPlan: (lessonPlanId) => post(`${Services.lesson_plan.backend}api/v1/lesson-plan/delete/${lessonPlanId}`),
	exportAsPdf: (lessonPlanId) =>
		get(`${Services.lesson_plan.backend}api/v1/print/${lessonPlanId}`, {
			responseType: "blob",
		}),
};
