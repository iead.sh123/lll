import { get, post, destroy } from "config/api";

export const curriculaManagmentApi = {
  getRecordsNomination: (url) => get(url),
  importQS: (request) => post(`api/question-set/import`, request),
  importUP: (request) => post(`api/UnitPlan/import`, request),
  importTopic: (request) => post(`api/rubrics/import`, request),
  deleteTopic: (itemId) => destroy(`api/topics/${itemId}`),
  storeCourseResources: (request) =>
    post(`api/course-resources/store`, request),
  deleteCourseResources: (itemId) =>
    destroy(`api/course-resources/delete/${itemId}`),
  importLessonPlan: (request) =>
    post(`tch-api/lesson-plan/import-to-course`, request),
  initialNoteHistory: (request) => post(`chats/initialNoteHistory`, request),
  linkCourseWithStandardRubric:(course_id)=>get(`api/rubrics/link-with-standard/${course_id}`)
};
