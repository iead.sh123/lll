import { get, post, destroy } from "config/api";

export const assessmentPlanApi = {
  getGrades: () => get(`emp-api/assessment-plan/all_grades`),

  getMainCourses: (ids) => post(`emp-api/assessment-plan/grade_courses`, ids),

  postAssessmentPlan: (allData) =>
    post(`emp-api/assessment-plan/store`, allData),

  getAssessmentPlanById: (id) => get(`emp-api/assessment-plan/${id}`),

  destroyAssessmentPlan: (assessmentId) =>
    destroy(`emp-api/assessment-plan/${assessmentId}`),
};
