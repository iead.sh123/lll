import { get, post, destroy } from "config/api";

export const adminReportCardApi = {
  availableItems: () => get(`/api/report-template/available-items`),
  availableValues: () => get(`/api/report-template/available-values`),
  createNewTemplate: (data) => post(`/api/report-template/store`, data),
  showTemplateById: (id) => get(`/api/report-template/${id}`),
  deleteTemplate: (id) => destroy(`/api/report-template/delete/${id}`),
  studentTemplate: (studentId) =>
    get(`/api/report-template/student/${studentId}`),
  studentPrintTemplate: (studentId, classId) =>
    get(`/api/report-template/student/card/${studentId}/class/${classId}`),
  addStudentComment: (studentId, data) =>
    post(`/api/report-template/student/card/${studentId}`, data),
  getSemesters: () => get(`/api/report-template/semesters`),
  getEvaluationTypes: () => get(`/api/report-template/evaluation-types`),

  getStudentPrintNoAuth: (studentId, classId) =>
    get(`/student/card/${studentId}/class/${classId}`),

  saveFormatAsPdf: (studentId, classId) =>
    post(`/api/report-template/student/pdf/${studentId}/class/${classId}`),
};
