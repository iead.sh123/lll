import { post, get } from "config/api";

export const adminApi = {
  addUsers: (data) => post("api/users/store", data),

  gradesLevels: () => get("api/users/FKlists"),

  getUserById: (tabel_name, id) => post(`/api/users/index/${tabel_name}`, id),

  postUploadFile: (data) => post("api/import/upload", data),

  postSaveFile: (data) => post("api/import/saveFromFile", data),

  getAllUsersFromUserUrl: (url, data) => post(url, data),

  getAllUsersFromUser: (table_name) =>
    get(`api/users/addExisting/get-available-users/${table_name}`),

  saveUsersFromUser: (data) => post(`api/users/addExisting/store`, data),

  attendanceReport: (date) =>
    get(`emp-api/quick-school/attendance/report?date=${date}`),

  synchronizeSection: () => get(`api/sync-rabs/get-class-sections`),

  saveSynchronizeRab: (data) => post(`api/sync-rabs/link`, data),

  studentsErrors: (school_class) =>
    post(`api/report-template/publishAllStudentReports/pdf/${school_class}`),

  confirmationSendPdfToParent: (school_class) =>
    post(
      `api/report-template/confirmPublishAllStudentReports/pdf/${school_class}`
    ),

  acceptedUserTypes: (table_name) =>
    get(`api/users/getAcceptedUserTypesForAddToNewUserType/${table_name}`),

  availableUsers: (table_name, data) =>
    post(`api/users/getExistingAvialableUsers/${table_name}`, data),

  storeAvailableUsers: (table_name, data) =>
    post(`api/users/storeExistingAvialableUsers/${table_name}`, data),
};
