import { get, post, destroy } from "config/api";

export const adminHomeRoomsApi = {
    homerooms: ()=>get(`/emp-api/home-room/create`),
    storeHomerooms: data=>post(`/emp-api/home-room/create`,data),
    assignHomerooms: data=>post(`/emp-api/home-room/assign-students-teachers`,data),
    lockUnlock: gradeId=>get(`/emp-api/flag/post-Grade-CreateHomeroomFlag/${gradeId}`)
};
