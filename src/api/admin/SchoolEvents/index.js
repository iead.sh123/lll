import { get, post, destroy } from "config/api";

export const schoolEventsApi = {
  addEditSchoolEvents: (data) => post(`api/school-event/store`, data),
  getSchoolEventsById: (schoolEventId) =>
    get(`api/school-event/${schoolEventId}`),

  //announcement
  getAnnouncement: (page) => get(`api/announcement?page=${page}`),
  volunteerInAnnouncement: (announciableId) =>
    post(`api/school-event/volunteer/${announciableId}`),

  postAnnouncements: (data, page) =>
    post(`api/announcement/getannouncements?page=${page}`, data),

  getVolunteers: (schoolEventId, page) =>
    get(`api/school-event/volunteers/${schoolEventId}?page=${page}`),

  getUserTypes: (accessability_level_id) =>
    get(`social/usertypes/${accessability_level_id}`),

  addEditAnnouncement: (data) => post(`api/announcement/store`, data),

  singleAnnouncement: (id) => get(`api/announcement/${id}`),

  deleteAnnouncement: (id) => destroy(`api/announcement/${id}`),

  publishAnnouncement: (announcement_ids) =>
    post(`api/announcement/publish`, announcement_ids),

  acceptRejectVolunteers: (data) =>
    post(`api/school-event/volunteers/accept`, data),
};
