import { get, post } from "config/api";
import { Services } from "engine/services";

export const discussionApi = {
	getDiscussions: (pageParam, params) => get(`${Services.discussion.backend}api/posts/254?page=${pageParam}`, { params }),
	getDiscussionById: (discussionId) => get(`${Services.discussion.backend}api/posts/show/${discussionId}`),
	createDiscussion: (data) => post(`${Services.discussion.backend}api/discussion/store`, data),
	deleteDiscussion: (discussionId) => post(`${Services.discussion.backend}api/discussion/delete/${discussionId}`),
	updateDiscussion: (discussionId) => put(),
	changeStatus: (discussionId) => get(`${Services.discussion.backend}api/discussion/change-status/${discussionId}`),
};
