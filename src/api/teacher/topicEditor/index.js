import { get, post } from "config/api";

export const topicEditorAPI = {
  addTopic: (payload) => post('api/topics',payload),
  getTopic: (id)=>get(`api/topics/${id}`),
  getCourses: ()=>get('api/courses/userCourses')
};
