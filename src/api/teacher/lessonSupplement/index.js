import { get } from "config/api";

export const lessonSupplementApi = {
  getLessonSupplement: (lessonId) =>
    get(`api/lessons/vcSupplementRecordings/${lessonId}`),
  getSupplementRecord: (lessonId) =>
    get(`api/lessons/vcSupplementRecordings/add/${lessonId}`),
};
