import { get, post, destroy } from "config/api";

export const dashboardeApi = {
	startNewMeeting: (LessonId) => post(`tch-api/current-lesson/start-new-meeting/${LessonId}`),
	meetingInfo: (LessonId) => get(`tch-api/current-lesson/meeting-info/${LessonId}`),
	getLessonInfo: (LessonId) => get(`tch-api/current-lesson/lesson-info/${LessonId}`),
	EndMeeting: (LessonId) => post(`tch-api/current-lesson/end-meeting/${LessonId}`),
	EndLesson: (LessonId) => post(`tch-api/current-lesson/end-lesson/${LessonId}`),
	getToDoList: () => get(`api/dashboard/tch-ToDoList`),
	getTeacherCourses: () => get(`api/dashboard/tch-courses`),
	getTeacherLiveSessions: () => get(`api/dashboard/tch-LiveSessions`),
	getTeacherEvents: () => get(`api/dashboard/tch-events`),
	getTeacherCollaborations: () => get(`api/dashboard/tch-collaboration`),
	markAsDone: (request) => post(`api/dashboard/tch-markAsDone`, request),
	getSTeacherCourses: () => get(`api/dashboard/courses`),
	getSTeacherLiveSessions: () => get(`api/dashboard/LiveSessions`),
	getSchools: () => get(`api/dashboard/schools`),
	getSTeacherCollaborations: () => get(`api/dashboard/collaboration`),

	getTeacherHomeRoom: () => get(`emp-api/home-room`),
	getStudentHomeRoom: (homeRoomId) => get(`emp-api/home-room/${homeRoomId}/students`),
	getAttendanceTypes: () => get(`emp-api/quick-school/attendance/type`),
	postSetAttendance: (date, attendance) => post(`emp-api/quick-school/attendance/setAttendance/student?date=${date}`, attendance),

	searchStudentsAttendance: (data) => post(`emp-api/quick-school/attendance/searchAttendance`, data),

	deleteStudentAttendance: (studentGID, studentQsID, studentName, studentDate, homeRoomId) =>
		destroy(
			`emp-api/quick-school/attendance/deleteAttendance/student?studentFullName=${studentName}&date=${studentDate}&homeRoomId=${homeRoomId}&studentGuidanceId=${studentGID}&studentQuickSchoolId=${studentQsID}`
		),
};
