import { get, post, put, patch, destroy } from "config/api";
import { Services } from "engine/services";
const at_get = (route) => get(`${Services.attendance.backend}${route}`);
const at_post = (route, payload) => post(`${Services.attendance.backend}${route}`, payload);
export const attendanceApi = {
	getAllStudentsAttendance: (courseId, cohortId, name) =>
		at_get(`api/v1/attendance/students/${courseId}/${cohortId}${name ? `?name=${name}` : ""}`),
	getAllStudentsOverall: (courseId, name) => at_get(`api/v1/attendance/overall/${courseId}${name ? `?name=${name}` : ""}`),
	getCourseModuleContent: (courseId, date) => at_get(`api/v1/course/${courseId}/module-contents?date=${date}`),
	getAttendanceSettings: (courseId) => at_get(`api/v1/attendance/${courseId ? courseId : ""}`),
	setAttendanceSetting: (payload) => at_post(`api/v1/attendance`, payload),
	getTeacherCourses: (courseId) => at_get(`api/v1/course/by-creator/${courseId}`),
	getStudentDetails: (courseId, studentId, date) => at_get(`api/v1/attendance/student-details/${courseId}/${studentId}?date=${date}`),
	assignAttendanceToStudents: (courseId, payload) => at_post(`api/v1/attendance/students/${courseId}`, payload),
};
