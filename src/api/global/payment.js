import { get, post, put, patch, destroy } from "config/api";
import { Services } from "engine/services";

export const paymentApi = {
	fetchCartItems: () => get(`${Services.payment.backend}api/payment/carts`),

	cartPrices: (items) =>
		get(
			`${Services.payment.backend}api/payment/carts/price${
				items?.length > 0 ? `?${items.map((item, index) => `cartItems=${item}`).join("&")}` : ""
			}`
		),

	addItemToCart: (data) => post(`${Services.payment.backend}api/payment/carts`, data),

	removeItemFromCart: (itemId) => destroy(`${Services.payment.backend}api/payment/carts/${itemId}`),

	applyCouponOnItems: (data) => post(`${Services.payment.backend}api/payment/apply-coupon`, data),

	deleteCouponFromItems: () => destroy(`${Services.payment.backend}api/payment/apply-coupon`),

	createPaymentIntent: (data) => post(`${Services.payment.backend}api/payment/create-payment-intent`, data),

	enrollCart: (data) => post(`${Services.payment.backend}api/payment/carts/enroll`, data),

	finalCart: (items) =>
		get(
			`${Services.payment.backend}api/payment/final-cart${
				items?.length > 0 ? `?${items.map((item, index) => `cartItems=${item}`).join("&")}` : ""
			}`
		),

	paymentOrder: (orderId) => get(`${Services.payment.backend}api/payment/orders/${orderId}`),

	// Organization Discount
	discountsList: (params) => get(`${Services.payment.backend}api/payment/discounts${params ? params : ""}`),

	addDiscount: ({ type, data }) => post(`${Services.payment.backend}api/payment/discounts${type ? `?type=${type}` : ""}`, data),

	checkGeneralDiscount: () => get(`${Services.payment.backend}api/payment/organization/discount`),

	discountDeactivate: ({ discountId, discountType }) =>
		destroy(`${Services.payment.backend}api/payment/discounts/${discountId}${`?type=${discountType}`}`),

	// Organization Coupon
	couponsList: (params) => get(`${Services.payment.backend}api/payment/coupons${params ? params : ""}`),

	addCoupon: ({ data }) => post(`${Services.payment.backend}api/payment/coupons`, data),

	couponDetails: ({ couponId, params }) => get(`${Services.payment.backend}api/payment/coupon-users/${couponId}${params ? params : ""}`),

	couponDeactivate: (couponId) => destroy(`${Services.payment.backend}api/payment/coupons/${couponId}`),

	// Organization Coupon Archive
	couponsArchiveList: (params) => get(`${Services.payment.backend}api/payment/coupons/archive${params ? params : ""}`),

	// Organization Discount Archive
	discountsArchiveList: (params) => get(`${Services.payment.backend}api/payment/discounts/archive${params ? params : ""}`),

	applyDiscountToProducts: ({ discountId, data }) => post(`${Services.payment.backend}api/payment/discount/${discountId}/products`, data),

	deleteDiscountFromProducts: ({ discountId, data }) =>
		patch(`${Services.payment.backend}api/payment/discount/${discountId}/products`, data),

	minAndMaxPricesToUsingInFilter: () => get(`${Services.payment.backend}api/payment/item/courses/max-min`),
};
