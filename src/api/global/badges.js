import { get, post, destroy } from "config/api";
import { Services } from "engine/services";

export const badgesApi = {
  getBadges: (courseId, curriculumId, filter) =>
    get(
      `${Services.badge.backend}api/v1/badge/get${
        courseId ? `?course_id=${courseId}` : `?curriculm_id=${curriculumId}`
      }${filter ? `&filter=${filter}` : ""}`
    ),

  removeBadge: (badgeId) =>
    destroy(`${Services.badge.backend}api/v1/badge/delete/${badgeId}`),

  storeBadge: (data) =>
    post(`${Services.badge.backend}api/v1/badge/store`, data),
};
