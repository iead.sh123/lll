import { get } from "config/api";
import { versionServiceComponent } from "engine/config";
import { Services } from "engine/services";

export const genericComponents = {
  getGenericComponents: () =>
    get(
      `${Services.auth_organization_management.backend}api/component/getComponentTree`
    ),

  getGenericComponentByNode: (name, id) =>
    get(
      `${Services.auth_organization_management.backend}api/component/getComponentTree/${name}/${id}`
    ),
};
