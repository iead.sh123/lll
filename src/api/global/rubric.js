import { get, post, put, patch, destroy } from "config/api";
import { Services } from "engine/services";

export const rubricApi = {
	getRubrics: (params) => get(`${Services.rubric.backend}api/v1/rubric`, { params }),

	changeRubricStatus: (rubricId) => get(`${Services.rubric.backend}api/v1/rubric/change-status/${rubricId}`),

	removeRubric: (rubricId) => post(`${Services.rubric.backend}api/v1/rubric/delete/${rubricId}`),

	exportRubricAsPdf: (rubricId) =>
		get(`${Services.rubric.backend}api/v1/rubric/print/${rubricId}`, {
			responseType: "blob",
		}),

	getRubricById: (rubricId) => get(`${Services.rubric.backend}api/v1/rubric/view/${rubricId}`),

	add: (data) => post(`${Services.rubric.backend}api/v1/rubric/store`, data),

	studentsDetails: (courseId, rubricId) => get(`${Services.rubric.backend}api/v1/rubric/users/${courseId}/${rubricId}`),

	exportStudentEvaluationsAsPdf: (rubricId, studentId) =>
		get(`${Services.rubric.backend}api/v1/rubric/evaluations/print/${rubricId}/${studentId}`, {
			responseType: "blob",
		}),

	addRatingsToUser: (data) => post(`${Services.rubric.backend}api/v1/users/rate`, data),
};
