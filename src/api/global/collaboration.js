import { get, post, put, destroy } from "config/api";
import { Services } from "engine/services";

export const collaborationApi = {
	getProviders: () => get(`${Services.collaboration.backend}api/collaboration-area/providers`),
	getClients: () => get(`${Services.collaboration.backend}api/collaboration-area/destination`),

	// Setting
	getContent: () => get(`${Services.collaboration.backend}api/organization/contents`),
	shareableContent: (data) => post(`${Services.collaboration.backend}api/organization/sharable-contents`, data),
	approvalToContent: (data) => post(`${Services.collaboration.backend}api/organization/set-need-approval`, data),

	readLevels: () => get(`${Services.collaboration.backend}api/organization/collaboration-levels`),
	createLevel: (data) => post(`${Services.collaboration.backend}api/collaboration-level`, data),
	updateLevel: (levelId, data) => put(`${Services.collaboration.backend}api/collaboration-level/${levelId}`, data),
	deleteLevel: (levelId) => destroy(`${Services.collaboration.backend}api/collaboration-level/${levelId}`),

	//Agreement
	// * Clients List
	clients: () => get(`${Services.collaboration.backend}api/collaboration-agreement/clients`),
	// * Add Agreement
	agreementsList: (organizationId) => get(`${Services.collaboration.backend}api/organization/${organizationId}/contents`),
	allLevels: (organizationId) => get(`${Services.collaboration.backend}api/organization/${organizationId}/levels`),
	allOrganizations: () => get(`${Services.collaboration.backend}api/organization/all`),
	createAgreement: (data) => post(`${Services.collaboration.backend}api/collaboration-agreement`, data),
	readAgreement: (agreementId) => post(`${Services.collaboration.backend}api/collaboration-agreement/${agreementId}`),
	updateAgreement: (data, agreementId) => put(`${Services.collaboration.backend}api/collaboration-agreement/${agreementId}`, data),
	deleteAgreement: (agreementId) => destroy(`${Services.collaboration.backend}api/collaboration-agreement/remove/${agreementId}`),

	// * Providers List
	providers: () => get(`${Services.collaboration.backend}api/collaboration-agreement/providers`),
	acceptAgreementProvider: (agreementId) => get(`${Services.collaboration.backend}api/collaboration-agreement/approve/${agreementId}`),
};
