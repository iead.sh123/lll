import { get, post } from "config/api";

export const tagAPIs = {
  getTagsByPrefix: (pref) => get(`api/tags/getPrefixTags?prefix=${pref}`),
};
