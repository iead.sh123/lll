import React, { useEffect, useState } from "react";
import {
	Button,
	Card,
	CardHeader,
	CardBody,
	CardFooter,
	Form,
	Input,
	InputGroupAddon,
	InputGroupText,
	InputGroup,
	Container,
	Col,
	Row,
} from "reactstrap";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";

import { signIn } from "store/actions/global/auth.actions";
import tr from "components/Global/RComs/RTranslator";
import { useSelector } from "react-redux";
import "react-toastify/dist/ReactToastify.css";
import guidance from "assets/img/logo.png";
import RLoader from "components/Global/RComs/RLoader";
import withDirection from "hocs/withDirection";
import store from "store/index.js";
import { refresh } from "store/actions/global/auth.actions";
import OrganizationTypeSwitcher from "./OrganizationTypeSwitcher";
import initiateToken from "./FireBaseServices";
import { removeApisNotWorkingFromLocale } from "engine/config";
function Login({ dir }) {
	const dispatch = useDispatch();
	let history = useHistory();

	const [hideLogin, setHideLogin] = useState(true);
	const [loading, setLoading] = useState(false);

	const { user } = useSelector((state) => state?.auth);
	const [loginForm, setLoginForm] = useState({ username: "", password: "" });

	const handleInputChange = (e) => {
		setLoginForm({ ...loginForm, [e.target.id]: e.target.value });
	};

	const handleLoginSubmit = (e) => {
		e.preventDefault();
		setLoading(true);
		dispatch(signIn(loginForm, login_callback, login_fail));
	};

	useEffect(() => {
		if (user) setHideLogin(true);
	}, [user]);

	// window.location.href;
	// http://amly.guidancealliance.com/";
	const HREF = window.location.href;
	const SUBHREF1 = HREF.indexOf("//") + 2;
	const SUBHREF2 = HREF.indexOf(".");

	const IMAGEHREF = HREF.substr(SUBHREF1, SUBHREF2 - SUBHREF1);

	//---------------------------step1
	React.useEffect(() => {
		async function fetch() {
			const access_token = store.getState().auth.token;
			const refresh_token = store.getState().auth.refresh_token;
			if (access_token) {
				await dispatch(refresh({ refresh_token: refresh_token }, login_callback, refresh_fail));
			} else {
				setHideLogin(false);
			}
		}

		fetch();
	}, []);

	const refresh_fail = () => {
		setHideLogin(false);
	};

	const login_fail = () => {
		setLoading(false);
		setHideLogin(false);
	};

	const login_callback = async (data) => {
		if (data?.user && !removeApisNotWorkingFromLocale) {
			await initiateToken(data.user.original?.id);
		}
		history.push(process.env.REACT_APP_BASE_URL + "/g/dashboard");
	};

	return (
		<div>
			<Container>
				<Row>
					<Col className="ml-auto mr-auto" lg="4" md="6">
						{/* <FirebaseManager></FirebaseManager> */}
						<Form action="" className="form" onSubmit={handleLoginSubmit}>
							<Card
								className="card-login global-glass-effect"
								style={{
									background: "linear-gradient(135deg,rgba(255,255,255,0.1),rgba(255,255,255,0))",
									backdropFilter: "blur(20px)",

									border: "1px solid rgba(255,255,255,0.5)",
								}}
							>
								<CardHeader>
									<CardHeader>
										{IMAGEHREF?.length > 0 ? (
											<div
												style={{
													textAlign: "center",
													display: "flex",
													justifyContent: "center",
													position: "relative",
													alignItems: "flex-end",
												}}
											>
												<div
													style={{
														position: "relative",
														left: dir && "12px",
														right: !dir && "12px",
													}}
												>
													<img
														src={guidance}
														alt="guidance image"
														style={{
															width: 100,
															height: 100,
														}}
													/>
												</div>

												<div
													style={{
														position: "relative",
														right: dir && "12px",
														left: !dir && "12px",
													}}
													onClick={(e) => {
														var win = window.open("https://landing.guidancealliance.com/", "_blank");
														win.focus();
													}}
												>
													<img
														src={`${process.env.REACT_APP_RESOURCE_URL}${IMAGEHREF}.png`}
														alt="school image"
														style={{
															width: 100,
															height: 100,
														}}
													/>
												</div>
											</div>
										) : (
											<div
												style={{
													textAlign: "center",
												}}
												onClick={(e) => {
													var win = window.open("https://landing.guidancealliance.com/", "_blank");
													win.focus();
												}}
											>
												<img src={guidance} alt="school image" style={{ width: 100 }} />
											</div>
										)}
									</CardHeader>
								</CardHeader>

								{hideLogin ? (
									<CardBody>
										<OrganizationTypeSwitcher />

										<RLoader />
									</CardBody>
								) : (
									<>
										<CardBody>
											<InputGroup>
												<InputGroupAddon addonType="prepend" style={{ backgroundColor: "rgb(232, 240, 254)" }}>
													<InputGroupText
														style={{
															position: "relative",
															right: !dir && "6px",
														}}
													>
														<i className="nc-icon nc-single-02" />
													</InputGroupText>
												</InputGroupAddon>
												<Input placeholder="Username" type="text" id="username" onChange={handleInputChange} required />
											</InputGroup>
											<InputGroup>
												<InputGroupAddon addonType="prepend" style={{ backgroundColor: "rgb(232, 240, 254)" }}>
													<InputGroupText
														style={{
															position: "relative",
															right: !dir && "6px",
														}}
													>
														<i className="nc-icon nc-key-25" />
													</InputGroupText>
												</InputGroupAddon>
												<Input
													placeholder="Password"
													type="password"
													id="password"
													autoComplete="off"
													onChange={handleInputChange}
													required
												/>
											</InputGroup>
											{/* <a
                   href={
                     process.env.REACT_APP_BASE_URL + "/auth/reset-password"
                   }
                   style={{
                     float: "right",
                     color: "#333",
                     textDecoration: "none",
                   }}
                 >{tr`forget_password`}</a> */}
										</CardBody>
										<CardFooter>
											<Button
												block
												className="btn-round mb-3"
												color="warning"
												type="submit"
												//disabled={loading ? true : false}
											>
												{loading ? (
													<>
														{tr`login`} <i className="fa fa-refresh fa-spin"></i>
													</>
												) : (
													tr("login")
												)}
											</Button>
										</CardFooter>
									</>
								)}
							</Card>
						</Form>
					</Col>
				</Row>
			</Container>
			<div className="full-page-background" />
			{/* <ToastContainer
				position="top-center"
				autoClose={5000}
				hideProgressBar
				newestOnTop={false}
				closeOnClick
				rtl={false}
				pauseOnFocusLoss
				draggable
				pauseOnHover
			/> */}
		</div>
	);
}

export default withDirection(Login);
