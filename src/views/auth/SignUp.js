import React, { useEffect } from "react";
import { Row, Col, Form, FormGroup, Input, FormText, Label } from "reactstrap";
import { signUpAsync, refresh, getAllOrganizationsAsync } from "store/actions/global/auth.actions";
import { useDispatch, useSelector } from "react-redux";
import { specificOrganization } from "engine/config";
import { baseURL, genericPath } from "engine/config";
import { primaryColor } from "config/constants";
import { CART_ITEMS } from "config/constants";
import { useHistory } from "react-router-dom";
import { useFormik } from "formik";
import RPasswordFields from "components/Global/RComs/RInputs/RPasswordFields";
import initiateToken from "./FireBaseServices";
import RSelect from "components/Global/RComs/RSelect";
import RButton from "components/Global/RComs/RButton";
import styles from "./auth.module.scss";
import tr from "components/Global/RComs/RTranslator";
import * as yup from "yup";
import { removeApisNotWorkingFromLocale } from "engine/config";

const SignUp = () => {
	const dispatch = useDispatch();
	const history = useHistory();
	var currentURL = window.location.href;
	var subdomain = currentURL.split(".")[0].split("://")[1];
	const oid = subdomain.toLowerCase().includes("test") ? 1013 : 1014;
	const { organizations, organizationLoading, registerLoading } = useSelector((state) => state.auth);

	const handleFormSubmit = async (values) => {
		dispatch(signUpAsync(values, signup_callback));
		typeof window !== "undefined" && localStorage.removeItem(CART_ITEMS);
	};

	useEffect(() => {
		dispatch(getAllOrganizationsAsync());
	}, []);

	const initialValues = {
		first_name: "",
		last_name: "",
		name: "",
		email: "",
		password: "",
		password_confirmation: "",
		organization_id: specificOrganization ?? null,
	};

	const formSchema = yup.object().shape({
		first_name: yup.string(tr`invalid first name`).required(tr`first name is required`),
		last_name: yup.string(tr`invalid last name`).required(tr`last name address is required`),
		name: yup.string(tr`invalid full name`).required(tr`full name is required`),
		organization_id: yup.string(tr`invalid organization`).required(tr`organization is required`),
		email: yup
			.string()
			.email(tr`invalid email`)
			.required(tr`email address is required`),
		password: yup
			.string()
			.matches(/^(?=.{1,})/, tr`must contain 1 character`)
			.required(tr`password is required`),
		password_confirmation: yup
			.string()
			.matches(/^(?=.{1,})/, tr`must contain 1 character`)
			.oneOf([yup.ref("password"), null], tr`passwords must match`)
			.required(tr`confirm password is required`),
	});

	const { values, errors, touched, handleBlur, handleChange, handleSubmit, setFieldValue } = useFormik({
		initialValues,
		onSubmit: handleFormSubmit,
		validationSchema: formSchema,
	});

	const handlePushToSignInPage = () => {
		history.push(baseURL + `/auth/login`);
	};

	React.useEffect(() => {
		async function fetch() {
			const access_token = store.getState().auth.token;
			const refresh_token = store.getState().auth.refresh_token;
			if (access_token) {
				await dispatch(refresh({ refresh_token: refresh_token }, signup_callback));
			}
		}

		fetch();
	}, []);

	const signup_callback = (data) => {
		if (data?.user && !removeApisNotWorkingFromLocale) {
			initiateToken(data.user.original?.id);
		}

		history.push(`${baseURL}/${genericPath}/my-courses`);
	};

	return (
		<section className={styles.section__sign__up__layout}>
			<h6 className={styles.auth__text}>
				{tr`register_in`} &nbsp;
				<span className={styles.auth__text_organization}>Guidance</span>
			</h6>
			<Form className="w-100" onSubmit={handleSubmit}>
				<Row className={styles.auth__S__U__row}>
					<Col md={6} className={`${styles.auth__input}${" "}${styles.padding__left}`}>
						<Label>{tr("first_name")}</Label>
						<FormGroup>
							<Input
								name="first_name"
								type="text"
								value={values.first_name}
								placeholder={tr("first_name")}
								onBlur={handleBlur}
								onChange={handleChange}
								className={(!!touched.first_name && !!errors.first_name) || (touched.first_name && errors.first_name) ? "input__error" : ""}
							/>
							{touched.first_name && errors.first_name && <FormText color="danger">{errors.first_name}</FormText>}
						</FormGroup>
					</Col>

					<Col md={6} className={`${styles.auth__input}${" "}${styles.padding__right}`}>
						<Label>{tr("last_name")}</Label>
						<FormGroup>
							<Input
								name="last_name"
								type="text"
								value={values.last_name}
								placeholder={tr("last_name")}
								onBlur={handleBlur}
								onChange={handleChange}
								className={(!!touched.last_name && !!errors.last_name) || (touched.last_name && errors.last_name) ? "input__error" : ""}
							/>
							{touched.last_name && errors.last_name && <FormText color="danger">{errors.last_name}</FormText>}
						</FormGroup>
					</Col>
					<Col md={6} className={`${styles.auth__input}${" "}${styles.padding__left}`}>
						<Label>{tr("full_name")}</Label>
						<FormGroup>
							<Input
								name="name"
								type="text"
								value={values.name}
								placeholder={tr("full_name")}
								onBlur={handleBlur}
								onChange={handleChange}
								className={(!!touched.name && !!errors.name) || (touched.name && errors.name) ? "input__error" : ""}
							/>
							{touched.name && errors.name && <FormText color="danger">{errors.name}</FormText>}
						</FormGroup>
					</Col>
					<Col md={6} className={`${styles.auth__input}${" "}${styles.padding__right}`}>
						<Label>{tr("email_address")}</Label>
						<FormGroup>
							<Input
								name="email"
								type="email"
								value={values.email}
								placeholder={tr("username_or_email_address")}
								onBlur={handleBlur}
								onChange={handleChange}
								className={(!!touched.email && !!errors.email) || (touched.email && errors.email) ? "input__error" : ""}
							/>
							{touched.email && errors.email && <FormText color="danger">{errors.email}</FormText>}
						</FormGroup>
					</Col>

					<Col md={6} className={`${styles.auth__input}${" "}${styles.padding__left}`}>
						<Label>{tr("password")}</Label>
						<FormGroup>
							<RPasswordFields
								onChange={handleChange}
								onBlur={handleBlur}
								touched={touched.password}
								errors={errors.password}
								value={values.password}
								placeholder={tr`password`}
								name={`password`}
							/>
						</FormGroup>
					</Col>
					<Col md={6} className={`${styles.auth__input}${" "}${styles.padding__right}`}>
						<Label>{tr("confirm_password")}</Label>
						<FormGroup>
							<RPasswordFields
								onChange={handleChange}
								onBlur={handleBlur}
								touched={touched.password_confirmation}
								errors={errors.password_confirmation}
								value={values.password_confirmation}
								placeholder={tr`confirm_password`}
								name={`password_confirmation`}
							/>
						</FormGroup>
					</Col>
					<Col style={{ display: specificOrganization ? "none" : "block" }} xs={12}>
						<RSelect
							name="organization_id"
							onChange={(e) => setFieldValue("organization_id", specificOrganization ?? e.value)}
							option={organizations}
							isLoading={organizationLoading}
							touched={touched.organization_id}
							errors={errors.organization_id}
							className={
								(!!touched.organization_id && !!errors.organization_id) || (touched.organization_id && errors.organization_id)
									? "input__error"
									: ""
							}
						/>

						{touched.organization_id && errors.organization_id && <FormText color="danger">{errors.organization_id}</FormText>}
					</Col>
				</Row>

				<Row>
					<Col xs={12} className="p-0 m-0">
						<RButton
							className="w-100"
							type="submit"
							text={tr`register`}
							color="primary"
							disabled={registerLoading}
							loading={registerLoading}
						/>
					</Col>
				</Row>
				<Row>
					<Col xs={12} className="text-center">
						<span className="text-muted">
							{tr`already_have_an_account`}? &nbsp;
							<span style={{ color: primaryColor }} onClick={handlePushToSignInPage} className="cursor-pointer">{tr`log_in`}</span>
						</span>
					</Col>
				</Row>
			</Form>
		</section>
	);
};

export default SignUp;
