import React from "react";

const OrganizationImage = () => {
  const { hostname } = new URL(window.location.href);
  const organizationImage = hostname.split(".")[0];
  return <div>OrganizationImage</div>;
};

export default OrganizationImage;
