import React, { useEffect, useState } from "react";

import { initializeApp } from "firebase/app";
import { getMessaging, getToken, onMessage } from "firebase/messaging";
import firebaseConfig from "./firebase-config";
const FirebaseManager = () => {
	const [token, setToken] = useState("");
	// Initialize Firebase
	const firebaseApp = initializeApp(firebaseConfig);
	const messaging = getMessaging(firebaseApp);
	getToken(messaging, {
		vapidKey: "BIuWWRuP_RcwT_dyR1ZOGo7eH0dl8sOicQa10EoBwm9hyhpNJ5n97DqFepk3hgZl7G7AsZtfd7-2GQryHZWdJMM",
	})
		.then((currentToken) => {
			if (currentToken) {
				// Send the token to your server and update the UI if necessary
				// ...
				setToken(currentToken);
			} else {
				// Show permission request UI
				// console.log(
				//   "bc4:No registration token available. Request permission to generate one."
				// );
				// ...
			}
		})
		.catch((err) => {
			// console.log("bc4:An error occurred while retrieving token. ", err);
			// ...
		});

	onMessage(messaging, (payload) => {
		// console.log("bc4:Message received. ", payload);
		// ...
	});

	async function requestNotificationsPermissions() {
		// console.log("bc4:Requesting notifications permission...");
		const permission = await Notification.requestPermission();

		if (permission === "granted") {
			// console.log("bc4:Notification permission granted.");
			// Notification permission granted.
			await saveMessagingDeviceToken();
		} else {
			// console.log("bc4:Unable to get permission to notify.");
		}
	}

	async function saveMessagingDeviceToken() {
		try {
			const currentToken = await getToken(getMessaging());
			if (currentToken) {
				// console.log("bc4:Got FCM device token:", currentToken);
				// Saving the Device Token to Cloud Firestore.
				// const tokenRef = doc(getFirestore(), 'fcmTokens', currentToken);
				//  await setDoc(tokenRef, { uid: getAuth().currentUser.uid });

				// This will fire when a message is received while the app is in the foreground.
				// When the app is in the background, firebase-messaging-sw.js will receive the message instead.
				onMessage(getMessaging(), (message) => {
					console.log("bc4:New foreground notification from Firebase Messaging!", message.notification);
				});
			} else {
				// Need to request permissions to show notifications.
				requestNotificationsPermissions();
			}
		} catch (error) {
			console.error("bc4:Unable to get messaging token.", error);
		}
	}
	saveMessagingDeviceToken();
	return (
		<>
			token<br></br>
			{token}
		</>
	);
};

export default FirebaseManager;
