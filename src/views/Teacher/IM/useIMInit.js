import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { initializeConversations } from "store/actions/IMCycle2.actions";
import Swal from "utils/Alert";
import { DANGER } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { subscribeToPusher } from "utils/Pusher";
import useSound from "use-sound";
import sound from "assets/audio/notification.mp3";
import { eventTypes, GENERIC_EVENT_NAME } from "./constants/eventNames";
import { MessageStatus } from "store/actions/IMCycle2.actions";
import { pushMessage } from "store/actions/IMCycle2.actions";
import { chatSeen } from "store/actions/IMCycle2.actions";
import { unsendMessage } from "store/actions/IMCycle2.actions";
import { messageUnsent } from "store/actions/IMCycle2.actions";
import { changePointer } from "store/actions/IMCycle2.actions";
import { pushChat } from "store/actions/IMCycle2.actions";
import { updateImageThumbnail } from "store/actions/IMCycle2.actions";
import Helper from "components/Global/RComs/Helper";
import { subscribeToReceiver } from "views/auth/FireBaseServices";
import { toast } from "react-toastify";

const useIMInit = () => {
	const { storeIsInitialized, activeChat, lastMessageChatId } = useSelector((state) => state?.iMcycle2);
	const dispatch = useDispatch();
	const loggedUserId = useSelector((state) => state?.auth?.user?.id);
	const [loading, setLoading] = React.useState(false);

	//const [play, { sound: ssound }] = useSound(sound);

	const handlePushChat = async (chat) => {
		Helper.cl(chat, "handlePushChat");
		try {
			await dispatch(pushChat(chat.id));
		} catch (err) {
			return;
		}
	};

	const changePointerIfLastMessageInActiveChat = async () => {
		Helper.cl("changePoi f1");
		if (activeChat && lastMessageChatId && activeChat === lastMessageChatId) {
			Helper.cl("changePointe f2");
			try {
				Helper.cl("changePoin f3");
				await dispatch(changePointer(lastMessageChatId));
				Helper.cl("changePointe f4");
			} catch (err) {
				Helper.cl("changePointef5");
				return;
			}
		}
	};

	// React.useEffect(() => {
	//   changePointerIfLastMessageInActiveChat();
	// }, [lastMessageChatId]);

	const handlePushMessage = (chatId, message, currentUserId) => {
		Helper.cl(message, "handlePushMessage metaEvent");
		dispatch(
			pushMessage(chatId, {
				id: message.id,
				message: message.message,
				tags: message.user_message?.find?.((um) => um.user_id === currentUserId)?.tags.map((t) => t.name) ?? [],
				user_id: message.user_id,
				status: MessageStatus.SUCCESS,
				created_at: message.created_at,
				updated_at: message.updated_at,
				replyFor: {
					message: message?.src_messages?.[0]?.pivot?.brief,
					id: message?.src_messages?.[0]?.id,
				},
				attachments: message.resources ?? [],
			})
		);

		changePointerIfLastMessageInActiveChat();
	};

	const handleUpdateImageThumbNail = (chatId, msgId, resourceId, newImageThumnail) => {
		dispatch(updateImageThumbnail(chatId, msgId, resourceId, newImageThumnail));
	};

	const handleUnsendMessage = (chatId, messageId) => {
		dispatch(messageUnsent(chatId, messageId));
	};

	const handleChangePointer = (chatId, userId, pointer) => {
		Helper.cl("cp1 f1");
		dispatch(chatSeen(chatId, userId, pointer));
	};

	const handleEvents = (metaEvent) => {
		Helper.cl(metaEvent, "handleEvents metaEvent");
		try {
			const eventnot = metaEvent?.notifications?.[0];
			const event = eventnot.payload;
			Helper.cl(eventnot.type, "event.type??");
			switch (eventnot.type) {
				case eventTypes.NEW_CHAT:
					//   play();
					return handlePushChat(event.chat);

				case eventTypes.NEW_MESSAGE: {
					//play();
					Helper.cl(event.message, "ime new message");
					return handlePushMessage(event.chat_id, event.message, loggedUserId);
				}
				case eventTypes.MESSAGE_UNSENT:
					return handleUnsendMessage(event.chat_id, event.message_id);

				case eventTypes.CHANGE_POINTER:
					return handleChangePointer(event.chat_id, event.user_id, event.message_id);
				case eventTypes.TUMBNAIL_UPDATED:
					return handleUpdateImageThumbNail(event.chat_id, event.message_id, event.image_id, event.thumbUrl);
			}
		} catch (err) {
			return;
		}
	};

	const init = async () => {
		try {
			Helper.cl("iminit");
			if (storeIsInitialized) {
				return;
			}

			if (!loading) {
				setLoading(true);
				await dispatch(initializeConversations(loggedUserId));
				setLoading(false);
			}

			const events = {
				[`${process.env.REACT_APP_SLOT_NAME}_${loggedUserId}`]: {
					[GENERIC_EVENT_NAME]: handleEvents,
				},
			};

			//subscribeToPusher(events);

			// Helper.cl(events,"imn: before subscribtion to events:")
			subscribeToReceiver(events);
			//  Helper.cl("imn: after subscribtion to events")
		} catch (error) {
			setLoading(true);
			toast.error(error?.message);
			return;
		} finally {
			//   setloading({ left: false, right: false })
		}
	};

	React.useEffect(() => {
		if (!loggedUserId) {
			return;
		} else init();
	}, [loggedUserId]);

	return storeIsInitialized;
};

export default useIMInit;
