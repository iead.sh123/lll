import React, { useEffect, useState } from "react";
import RAdvancedLister from "components/Global/RComs/RAdvancedLister";
import { IMApi } from "api/IMNewCycle";
import tr from "components/Global/RComs/RTranslator";
import { Button } from "reactstrap";
import { get } from "config/api";
import AddUsers from "./AddUsers";
import ChatGallery from "./ChatGallery";
import { RLabel } from "components/Global/RComs/RLabel";
import UserImg from "../userImg";
import GroupEdit from "./GroupEdit";
import styles from "./GroupManagement.module.scss";
import ChatFolder from "../chatList/chatFolder";
import { Row, Col } from "reactstrap";
import chatListStyles from "../chatList/chatList.module.scss";
import { useDispatch, useSelector } from "react-redux";
import { DELETE_CHAT } from "store/actions/IMCycle2.actions";
import Helper from "components/Global/RComs/Helper";
import { Services } from "engine/services";
import RAccordion from "components/Global/RComs/RAccordion";
import RPlus from "components/Global/RComs/Rplus ";
import RRow from "view/RComs/Containers/RRow";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import SharedGroups from "./SharedGroups";
import { authApi } from "api/auth";
import UserPosition from "./UserPosition";
import StarredMessages from "./StarredMessages";
import UserAvatar from "assets/img/new/svg/user_avatar.svg";

const COMS = {
  ADD_USER: "ADD_USER",
  GALLERY: "GALLERY",
};

const COMS_MAPPING = {
  [COMS.ADD_USER]: AddUsers,
  [COMS.GALLERY]: ChatGallery,
};

const GroupManagement = ({
  chatId,
  isAdmin,
  toggleModal,
  isOpen,
  loggedUserId,
  isGroup,
  closeAll,
}) => {
  const [records, setRecords] = React.useState([]);
  const [activeCom, setActiveCom] = React.useState(null);
  const dispatch = useDispatch();
  // const [showAddUsers, setShowAddUsers] = React.useState(false);
  // const [galleryIsActive, setGalleryIsActive] = React.useState(false);
  const {
    entities,
    activeChat,
  } = useSelector((state) => state?.iMcycle2);
  const getOtherUserId=()=>{
    const otherUserId=entities?.chats?.byId?.[activeChat].entities?.users?.ids?.filter((id,index)=>id!=loggedUserId)[0]
    return otherUserId
  }
  const toggleGallery = () =>
    activeCom ? setActiveCom(null) : setActiveCom(COMS.GALLERY);
  // const [deletedUsers, setDeletedUsers] = React.useState([]);
  React.useEffect(() => {
    isOpen && setActiveCom(null);
  }, [isOpen]);

  const [sharedGroups, setSharedGroups] = useState(null)
  const [position,setPosition]=useState(null)
  const [starredMessages,setStarredMessages]=useState(null)
  const toggleShowAddUsers = () =>
    activeCom ? setActiveCom(null) : setActiveCom(COMS.ADD_USER);

  const handelMakeAdmin = async (userId) => {
    const response = await IMApi.markAsGroupAdmin(chatId, userId);
    try {
      if (!response.data.status) throw new Error(response.data.msg);
      // records.some(rc => rc.id === userId) && (records.find(rc => rc.id === userId).is_admin = true);

      setData(await getDataFromBackend());
    } catch (err) {
      return;
    }
  };

  const toggleDeletedUser = async (userId) => {
    try {
      let response;
      response = await IMApi.removeGroupParticipants(chatId, userId);
      if (!response.data.status) throw new Error(response.data.msg);

      if (userId == loggedUserId) {
        dispatch({ type: DELETE_CHAT, payload: chatId });
        closeAll();
      } else {
        setData(await getDataFromBackend());
      }
    } catch (err) { }
  };
const leaveGroup = async () => {
    //try {
      let response;
      response = await IMApi.leaveGroup(chatId);
      if (!response.data.status) throw new Error(response.data.msg);

    
        dispatch({ type: DELETE_CHAT, payload: chatId });
        closeAll();
      
   // } catch (err) { }
  };
  const getDataFromBackend = async (specific_url) => {
    const url = Services.im.backend + `api/group/participants/${chatId}`;
    let response1 = await get(specific_url ? specific_url : url);

    if (response1) {
      return response1;
    }
  };

  const getSharedGroups = async () => {
    let response = await IMApi.chatSharedGroup(chatId)
    if (!response.data.status) throw new Error(response.data.msg);
    if (response) {
      setSharedGroups(response)
      return response
    }

  }
  const getUserPosition = async () => {
    const otherUserId=getOtherUserId()
    let response = await authApi.userIdentity(otherUserId)
    if (!response.data.status) throw new Error(response.data.msg);
    if (response){
      setPosition(response.data?.data?.position)
    }
  }
  const getStarredMessages=async ()=>{
    let response =await IMApi.starredMessages(chatId)

    if (!response.data.status) throw new Error(response.data.msg);
    if(response){
      setStarredMessages(response)
    }
  }

  useEffect(() => {
    getStarredMessages()
    getOtherUserId()
    !isGroup ? getSharedGroups() : ''
    !isGroup ? getUserPosition():''
    return () => { }
  }, [])

  const setData = (response) =>
    setRecords(
      response?.data?.data?.data?.data?.map((r, index) => {
        return {
          title: r.full_name,
          allTitle: r.full_name,
          id: r.user_id,
          Titles: [r.full_name],
          table_name: `GroupManagement`,
          details: [
            {
              key: tr`name`,
              value: (
                <div id="User" style={{ display: "flex" }}>
                  <div style={{ width: "50px", marginLeft: "15px" }}>
                    <UserImg
                      index={1}
                      image={Services?.storage?.file + r.image}
                      online={r.online}
                      name={r.full_name}
                      width={38}
                    />
                  </div>
                  <RLabel
                    value={r?.full_name}
                    lettersToShow={15}
                    dangerous={false}
                  ></RLabel>
                </div>
              ),
            },
          ],
          actions: isAdmin
            ? [
              !r.is_admin && {
                id: r.user_id + "_makeAdmin",
                name: tr`make admin`,
                icon: "fa fa-user",
                color: "info",
                onClick: () => handelMakeAdmin(r.user_id),
              },
              {
                id: r.user_id + "_removeFromGroup",
                name: tr`remove from group`,
                justIcon: true,
                icon: 'fa-solid fa-trash-can',
                color: 'red',
                icon: 'fa-solid fa-trash-can',
                actionIconStyle: { color: 'red' },
                onClick: () => toggleDeletedUser(r.user_id),
              }, 
              (r.user_id == loggedUserId)&&
              {
                id: r.user_id + "_leavegroup",
                name: tr`leave from group`,
                justIcon: true,
                icon: 'fa-solid fa-trash-can',
                color: 'red',
                faicon: 'fa-solid fa-trash-can',
                actionIconStyle: { color: 'red' },
                onClick: () => {leaveGroup()},
              },
            ].filter((a) => a)
            : [],
        };
      })
    );

  const comProps = {
    [COMS.ADD_USER]: {
      chatId: chatId,
      toggleAddUsers: toggleShowAddUsers,
      loggedUserId: loggedUserId,
    },
    [COMS.GALLERY]: {
      // resources:
      chatId,
      handleGoBack: () => {
        setActiveCom(null);
        setShowItem(1);
      },
    },
  };

  const Com = activeCom && COMS_MAPPING[activeCom];

  const buttonStyle = { fontSize: "20px", alignSelf: "flex-end" };

  const [groupItems, setGroupItems] = useState([
    { id: 1, name: "Participants", icon: "fa fa-user", color: "red" },
    { id: 2, name: "Group Info", icon: "fa fa-users", color: "red" },
    { id: 3, name: "Gallery", icon: "fa fa-file-image-o", color: "red" },
  ]);

  const [showItem, setShowItem] = useState(1);

  const accordionHeader = {
    caption: "My Accordion",
    headerButtons: [
      {
        caption: "Button 1",
        onClick: () => console.log("Button 1 clicked"),
        icon: "fa fa-envelope",
      },
      {
        caption: "Button 2",
        onClick: () => console.log("Button 2 clicked"),
        icon: "fa fa-bell",
      },
    ],
  };
  const manage = () => {
    return (
      <div>
        {" "}

        {Com ? (
          <div>
            <button
              onClick={() => {
                setActiveCom(false);
              }}
            >
              back
            </button>

            {/* {COMS_MAPPING[COMS.ADD_USER]??"null"} */}
            <Com {...comProps[COMS.ADD_USER]} />
          </div>
        ) : (
          <div
            id="Users"
            style={{
              overflow: "scroll",
              overflowX: "hidden",
              height: "fit-content",
            }}
          >
            {/* <Button
          
          >{tr`Add Users To Group`}</Button> */}
            {isAdmin?<RFlex className="align-items-center">
              <RPlus onClick={toggleShowAddUsers}></RPlus>
              <span className="p-0 m-0 text-primary"
                style={{ cursor: 'pointer' }}
                onClick={toggleShowAddUsers}>Add New Members</span>
            </RFlex>:<></>}
            <RAdvancedLister
              hideTableHeader={true}
              colorEveryOtherRow={false}
              firstCellImageProperty={"image"}
              getDataFromBackend={getDataFromBackend}
              setData={setData}
              records={records}
              getDataObject={(response) => response.data?.data?.data}
              characterCount={15}
              marginT={"mt-3"}
              marginB={"mb-2"}
              align={"left"}
              noBorders={true}
              usingPaginator={false}
            />

            {isAdmin && (
              <Row>
                <Col
                  xs={12}
                  style={{
                    display: "flex",
                    justifyContent: "center",
                  }}
                ></Col>
              </Row>
            )}
          </div>
        )}
      </div>
    );
  };
  const items = Array.prototype.concat.apply(isGroup ? [
    // {
    //   buttonLabel: "properties",
    //   content: (
    //     <GroupEdit
    //       chatId={chatId}
    //       closeAll={closeAll}
    //       isAdmin={isAdmin}
    //       loggedUserId={loggedUserId}
    //     />
    //   ),
    //   disabled: false,
    //   navbar: true,
    //   icon: "fa fa-pen",
    //   color: "black",
    // },
    {
      buttonLabel: "All members",
      content: manage(),
      disabled: false,
      navbar: true,
      icon: "fa-solid fa-user-group",
      color: "black",
    }] : [
    {
      buttonLabel: "Common Groups",
      content: <SharedGroups response={sharedGroups} />,
      disabled: false,
      navbar: true,
      icon: "fa fa-user-group",
      color: "black",
    },
  ], [



    {
      buttonLabel: "Shared Attachments",
      content: <ChatGallery chatId={chatId} />,
      disabled: false,
      navbar: true,
      icon: "fa fa-paperclip",
      color: "black",
    },
    {
      buttonLabel: "Starred Chats",
      content: <StarredMessages response={starredMessages} loggedUserId={loggedUserId} />,
      disabled: false,
      navbar: true,
      icon: "fa-solid fa-star",
      color: "yellow",
    }
  ]);

  return (
    <>
      <RFlex
        className={`content flex-column ${styles.group_management}`}
        id="group-managment"
        style={{ overflow: "scroll", overflowX: "hidden", height: "80vh" }}
      >
        {/* <div className="new_messege_show_head" style={{ paddingTop: "10px" }}>
          <button className="show_icon" onClick={closeAll}>
            <i className="fa fa-angle-left"></i>
          </button>
          {isGroup?tr`Group Managment`:"This Chat"}
        </div> */}
        {isGroup?
          <GroupEdit
            chatId={chatId}
            closeAll={closeAll}
            isAdmin={isAdmin}
            loggedUserId={loggedUserId}
          />:<>
          {/* <RFlex >
          <UserImg
                  index={0}
                  image={image}
                  // image={userLogo2}
                  online={i.online}
                  name={i.name}
                  width={props.floating === "true" ? 30 : 56}
                  id={i.id}
                  length={props.users.length}
                />
            <RFlex className='flex-column'>

            </RFlex>
          </RFlex> */}
          <UserPosition position={position} />
          </>
        }

        <RAccordion items={items} />

        {/* <RAccordion header={accordionHeader} listItems={accordionListItems} /> */}
        {/* <RAccordion
  header={{caption:"h1"}}
  listItems={[
{icon:"",title:"1",header:{caption:"h11"},collapsed:false,LComponent:<div>1111111</div>,childrenNodes:[<div>1111111</div>]},
{icon:"",title:"2",header:{caption:"h12"},collapsed:false,LComponent:<div>222222222222222</div>,childrenNodes:[<div>1111111</div>]},
{icon:"",title:"3",header:{caption:"h13"},collapsed:false,LComponent:<div>333333333333</div>,childrenNodes:[<div>1111111</div>]},
{icon:"",title:"4",header:{caption:"h14"},collapsed:false,LComponent:<div>44444444444</div>,childrenNodes:[<div>1111111</div>]},
// it
// collapsed
// childrenNodes
// //----------------------------------------------
// header.caption
// header.headerButtons
// hb.tooltip
// hb.caption

  ]}></RAccordion> */}
      </RFlex>
    </>
  );
  return (
    <>
      <div
        className={`content ${styles.group_management}`}
        id="group-managment"
      >
        {/* <Row className={styles.top_group_management}>
          <Col
            xs={10}
            style={{
              display: "flex",
              justifyContent: "start",
              alignItems: "center",
            }}
          >
            <h6>{tr`Group Managment`}</h6>
          </Col>
          <Col xs={2} style={{ display: "flex", justifyContent: "end" }}>
            <button onClick={closeAll} style={buttonStyle}>
              <i className="fa fa-close" />
            </button>
          </Col>
        </Row> */}
        <div className="new_messege_show_head" style={{ paddingTop: "10px" }}>
          <button className="show_icon" onClick={closeAll}>
            <i className="fa fa-angle-left"></i>
          </button>
          {tr`Group Managment`}
        </div>

        {/* <div className={styles.group_management_list}> */}
        <div className="list_scroll">
          {groupItems.map((item) => (
            <div
              key={item.id}
              className={`chat_List_Item`}
              onClick={(e) => {
                // if (item.id == 3) {
                //   setShowItem(item.id);
                //   toggleGallery();
                // } else {
                //   setShowItem(item.id);
                // }

                setShowItem(item.id);
              }}
              style={{
                fontWeight: "bold",
                backgroundColor: "white",
                border: "lightgray 1px solid",
                margin: "1px",

                borderBottom: item.id == showItem ? "red 4px solid" : "",
              }}
            >
              <i className={item.icon} style={{ marginRight: "4px" }} />
              <span>{" " + tr(item.name) + " "}</span>
            </div>
          ))}
        </div>

        {!activeCom ? (
          showItem == 1 ? (
            <div
              style={{
                overflow: "scroll",
                overflowX: "hidden",
                height: "440px",
              }}
            >
              <RAdvancedLister
                hideTableHeader={true}
                colorEveryOtherRow={false}
                firstCellImageProperty={"image"}
                getDataFromBackend={getDataFromBackend}
                setData={setData}
                records={records}
                getDataObject={(response) => response.data?.data?.data}
                characterCount={15}
                marginT={"mt-3"}
                marginB={"mb-2"}
                align={"left"}
              />

              {isAdmin && (
                <Row>
                  <Col
                    xs={12}
                    style={{
                      display: "flex",
                      justifyContent: "center",
                    }}
                  >
                    <Button
                      onClick={toggleShowAddUsers}
                    >{tr`Add Users To Group`}</Button>
                  </Col>
                </Row>
              )}
            </div>
          ) : showItem == 2 ? (
            <GroupEdit
              chatId={chatId}
              closeAll={closeAll}
            // isAdmin={isAdmin}
            // loggedUserId={loggedUserId}
            />
          ) : showItem == 3 ? (
            <ChatGallery chatId={chatId} />
          ) : (
            ""
          )
        ) : (
          <Com {...comProps[activeCom]} />
        )}
      </div>
    </>
  );
};

export default GroupManagement;
