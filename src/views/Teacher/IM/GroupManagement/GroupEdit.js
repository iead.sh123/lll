import React from "react";
import { useDispatch } from "react-redux";
import { Button } from "reactstrap";
import { editGroup } from "store/actions/IMCycle2.actions";
import store from "store/index.js";
import tr from "components/Global/RComs/RTranslator";
import RInlineTextbox from "components/Global/RComs/RInlineTextbox";
import RInlineTextArea from "components/Global/RComs/RInlineTextArea";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";

import { toast } from "react-toastify";
function GroupEdit({ chatId, closeAll, isGroup }) {
	const dispatch = useDispatch();

	//
	const group = store.getState().iMcycle2.entities.chats.byId?.[chatId]; //useSelector((s)=>s.iMcycle2.entities.chats.byId?.[chatId]);
	const [groupImage, setGroupImage] = React.useState(group?.image);
	const [groupName, setGroupName] = React.useState(group?.name);
	const [groupDescription, setGroupDescription] = React.useState(group?.description);
	// useEffect(() => {
	//   if (group.name) setGroupName(group.name);
	//   if (group.image) setGroupImage(group.image);
	// }, [group]);

	//    useEffect(() => {
	//     const submit =async ()=>{
	//         if(validate())
	//         await send();
	//     }
	//  submit();
	//    }, [groupImage,groupName,groupDescription]);

	const validate = () => {
		if (groupName === "") {
			toast.error(tr`group name must not be empty`);

			return false;
		}
		return true;
	};
	const send = async () => {
		await await dispatch(editGroup(chatId, groupName, groupImage, groupDescription));
		closeAll();
	};

	return (
		<RFlex id="Whole GroupEdit" className="flex-column" styleProps={{ gap: "10px" }}>
			<RFlex id="Name + Image" className="align-items-center">
				<RFileSuite
					parentCallback={(e) => setGroupImage(e[0])}
					roundWidth={{ width: "63px", height: "63px", type: "group" }}
					singleFile={true}
					fileType={["image/*"]}
					removeButton={false}
					value={[group.image]}
					uploadName="upload"
					showReplace={false}
					showDelete={true}
					showFileList={false}
					showFileAdd={true}
					setSpecificAttachment={() => {}}
					theme="round"
					binary={true}
				></RFileSuite>
				{/* {Helper.js(group,"group")} */}
				<RFlex id="name + created by" styleProps={{ gap: "5px" }} className="flex-column align-items-start justify-content-center">
					<RFlex id="name + edit button" styleProps={{ gap: "5px" }}>
						<RInlineTextbox text={groupName} setText={setGroupName} />
					</RFlex>
					<span style={{ color: "#585858" }} className="p-0 m-0">
						Created by you
					</span>
				</RFlex>
			</RFlex>
			<RInlineTextArea text={groupDescription} setText={setGroupDescription} label={"Group Description"} />
			<Button text="submit" onClick={() => validate() && send()}>
				{tr`Apply Changes`}
			</Button>
		</RFlex>
	);
}
export default GroupEdit;
