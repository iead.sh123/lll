import RFlex from 'components/Global/RComs/RFlex/RFlex'
import RResourceViewer from 'components/Global/RComs/RResourceViewer/RResourceViewer';
import { Services } from 'engine/services';
import React from 'react'
import { MessageStatus } from "store/actions/IMCycle2.actions";
import moment from "moment";
import { relativeDate } from "utils/HandleDate";
import handleMessage from "components/Global/handleMessage";
import UserAvatar from "assets/img/new/svg/user_avatar.svg";

import UserImg from '../userImg';
import userLogo2 from '../../../../assets/img/testUserImage.svg'
const StarredMessages = ({ response, loggedUserId }) => {
  return (
    <RFlex className="flex-column">

      {response?.data?.data?.map((message, index) => {
        const currentUsr = message.message.user_id === loggedUserId
        console.log(message.message.user_id,loggedUserId)
        return (
          <RFlex
      id="Whole message2"
      /*onContextMenu={toggleContextMenu}*/
      styleProps={{marginLeft:'0px'}}
      className={currentUsr ? 'SentMessage' : 'ReciveMessage'}
    >

      <RFlex id="username with message body and actions" className="flex-column" styleProps={{ width: '35%', gap: '5px' }}>
        {!currentUsr &&
          <RFlex id="username with date" className="align-items-center" styleProps={{ width: "fit-content", gap: '5px' }}>
            <span className="UserNameStyle" style={{ margin: '0px', padding: '0px' }}>{message?.message?.user?.full_name}</span>
            <span className="DateStyle" style={{ margin: '0px', padding: '0px' }} title={moment(message?.created_at).format("YYYY:MM:DD HH:mm")}>
              {relativeDate(
                moment(message?.created_at).format("YYYY/MM/DD HH:mm")
              )}
            </span>

          </RFlex>}
        <RFlex id="chat body with actions and pointers" styleProps={{ width: '100%',marginLeft:!currentUsr?!currentUsr?'0px':'45px':'0px' }} className={currentUsr ? "flex-row-reverse align-items-stretch" : "align-items-stretch"}>

          <div
            id="chat body"
            className={currentUsr ? "SentMessageContent" : "ReciveMessageContent"}
            style={{
              width: message?.message?.length < 30 ? 'fit-content' : '',
              position:'relative'
            }}
          >
            <div id="message content" className="chatContent_msg">
                <p id="22222" style={{ padding: "0px", margin: "0px" }}
                  dangerouslySetInnerHTML={{
                    __html: handleMessage(message?.message?.message ?? ""),
                  }}
                />
            </div>
            <div id="file viewer">
              {/* {message?.attachments?.map(att=><img src={`${process.env.REACT_APP_RESOURCE_URL}${att.url}`}></img>)} */}
              {message?.status === MessageStatus.PENDING &&
                message?.attachments?.length ? (
                <Loader />
              ) : (
                <>
                  <RResourceViewer
                    sourceUrl={Services.storage.file}
                    resources={message?.attachments}
                  />


                </>
              )}
            </div>
            

          </div>
        </RFlex>

      </RFlex>



      {!currentUsr ? <div id="User Image" style={{ alignSelf: "flex-start" }}>
        <UserImg
          name={message?.user?.full_name /* ?userName : 'You' */}
          index={0}
          // image={Services?.storage?.file + userImg}
          image={UserAvatar}
          id={message?.user_id}
          online={false}
          width={46}
        />
      </div> : ""}

    </RFlex >
        )
      })}
    </RFlex>

  )
}

export default StarredMessages