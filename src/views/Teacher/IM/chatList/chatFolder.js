import React from "react";
import tr from "components/Global/RComs/RTranslator";

export default function ChatFolder({
  groupName,
  groupIcon,
  groupColor,
  handleSelectGroup,
  selected,
}) {
  return (
    <div
      className="chat_List_Item"
      onClick={handleSelectGroup}
      style={{
        fontWeight: "bold",
        backgroundColor: "white",
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        justifyContent: "center",
        border: "lightgray 1px solid",
        margin: "1px",
        borderBottom: selected ? "red 4px solid" : "",
        flexGrow: "1",
      }}
    >
      <i className={groupIcon} style={{ marginRight: "4px" }} />
      <span>{" " + tr(groupName) + " "}</span>
    </div>
  );
}
