import React from "react";

const userFirstLetter = (props) => {

 try{

  const getFirstLetter = (name) => {
    return name.charAt(0);
  };

  const colorGenerator = (inputString) => {
    var sum = 0;

    for (var i in inputString) {
      sum += inputString.charCodeAt(i);
    }

    var r = ~~(
      ("0." +
        Math.sin(sum + 1)
          .toString()
          .substr(6)) *
      256
    );
    var g = ~~(
      ("0." +
        Math.sin(sum + 2)
          .toString()
          .substr(6)) *
      256
    );
    var b = ~~(
      ("0." +
        Math.sin(sum + 3)
          .toString()
          .substr(6)) *
      256
    );

    var rgb = "rgb(" + r + ", " + g + ", " + b + ")";

    return (
      <div
        className="User_Img_img"
        style={{
          backgroundColor: props.str ? rgb : "gray",
          width: `${props.width}px`,
          height: `${props.width}px`,
          padding: `${(props.width * 5) / 100}%`,
          fontSize:
          props.width > 45
          ? "30px":
            props.width > 35
              ? "22px"
              : props.width > 28
              ? "18px"
              : props.width > 15
              ? "14px"
              : "8px",
          borderWidth:
            props.width > 35
              ? "2.2px"
              : props.width > 28
              ? "2px"
              : props.width > 15
              ? "1.6px"
              : "0.9px",
          outlineWidth:
            props.width > 35
              ? "1.6px"
              : props.width > 28
              ? "1.1px"
              : props.width > 15
              ? "0.9px"
              : "0.6px",
          marginLeft: `${props.left}px`,
        }}
      >
        <p style={{ position: "relative",textAlign: "center",fontWeight:"600", top: props.top }}>
          {getFirstLetter(props.str ? props.str : "A").toString().toUpperCase() }
        </p>
      </div>
    );
  };

  return <div>{colorGenerator(props.str)}</div>;
}catch(err){
    return <div>this is </div>
  }
};
export default userFirstLetter;
