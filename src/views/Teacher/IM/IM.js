import React, { useDebugValue, useEffect, useState } from "react";
import ChatContent from "./chatContent/chatContent";
import ChatList from "./chatList/chatList";
import "./chat.css";
import { useDispatch, useSelector } from "react-redux";
import { Row, Col, Container } from "reactstrap";
import tr from "components/Global/RComs/RTranslator";
import { trackLastSeenAsync } from "store/actions/global/track.action";
import { initializeConversations } from "store/actions/IMCycle2.actions";
import withDirection from "hocs/withDirection";
import Swal from "utils/Alert";
import { DANGER, SUCCESS } from "utils/Alert";
import Loader from "utils/Loader";
import { selectChat } from "store/actions/IMCycle2.actions";
import { fetchMoreMessages } from "store/actions/IMCycle2.actions";
import { changePointer } from "store/actions/IMCycle2.actions";
import { sendMessage, createChat } from "store/actions/IMCycle2.actions";
import Settings from "./settings/Settings";
import { updateSettings } from "store/actions/IMCycle2.actions";
import { IMApi } from "api/IMNewCycle";
import { searchMessage } from "store/actions/IMCycle2.actions";
import { MessageTags, ICONS } from "./constants/MessageTags";
import { tagMessage } from "store/actions/IMCycle2.actions";
import { deleteMessage } from "store/actions/IMCycle2.actions";
import { setForwardedMessage } from "store/actions/IMCycle2.actions";
import { forwardMessage } from "store/actions/IMCycle2.actions";
import { unsendMessage } from "store/actions/IMCycle2.actions";
import { setRepliedOnMessageId } from "store/actions/IMCycle2.actions";
import { WARNING } from "utils/Alert";
import { useHistory } from "react-router-dom";
import { LAYOUTS, MAPPING } from "./rightSideLayouts";
import { CHAT_ACTIONS_ICONS, ChatTags } from "./constants/ChatTags";
import { tagChat } from "store/actions/IMCycle2.actions";
import { deleteChat } from "store/actions/IMCycle2.actions";
import { selectGroup } from "store/actions/IMCycle2.actions";
import { selectMultipleMessages } from "store/actions/IMCycle2.actions";
import { selectMultipleChats } from "store/actions/IMCycle2.actions";
import NewMessage from "./newMessage/NewMessage";
import { Collapse } from "reactstrap";
import RModal from "components/Global/RComs/RModal";
import RButton from "components/Global/RComs/RButton";
import Helper from "components/Global/RComs/Helper";
import useIMInit from "./useIMInit";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import AppModal from "components/Global/ModalCustomize/AppModal";
import NotificationPermissionComponent from "./NotificationPermissionComponent";
import { toast } from "react-toastify";

function IM({ dir, floating }) {
	const dispatch = useDispatch();
	const [loading, setloading] = React.useState({ right: false, left: false });
	const [rightSideLayout, setRightSideLayout] = React.useState(null);
	const [modalLayout, setModalLayout] = React.useState(null);
	const [showSettings, setShowSettings] = React.useState(false);
	const [newMessageVisible, setNewMessageVisible] = React.useState(false);
	const [IMcollapseOpen, setIMCollapseOpen] = React.useState(false);

	const [sending, setSending] = React.useState(false);

	const history = useHistory();

	const loggedUserId = useSelector((state) => state?.auth?.user?.id);
	const userType = useSelector((state) => state?.auth?.user?.type);
	const {
		activeChat,
		settings,
		searchedMessage,
		entities,
		forwardedMessage,
		repliedMessage,
		storeIsInitialized,
		selectedGroup,
		selectedMultipleChats,
		selectedMultipleMessages,
		unread,
	} = useSelector((state) => state?.iMcycle2);

	const user = useSelector((state) => state?.auth?.user);

	// const toggleSdfgdfghowSettings = () => setShowSettings(!showSettings);
	const toggleIMCollapse = () => {
		if (!IMcollapseOpen) {
			// setColor("bg-white");
		} else {
			//  setColor("navbar-transparent");
		}
		setIMCollapseOpen(!IMcollapseOpen);
	};
	const hideRightSideLayout = () => {
		setRightSideLayout(null);
		setModalLayout(null);
	};
	const hideModalLayout = () => {
		setModalLayout(null);
	};

	const toggleManagement = () => {
		if (rightSideLayout === LAYOUTS.GROUP_MANAGEMENT) return setRightSideLayout(null);
		setRightSideLayout(LAYOUTS.GROUP_MANAGEMENT);
	};
	const toggleReport = () => {
		if (rightSideLayout === LAYOUTS.REPORT_CONTENT) return setRightSideLayout(null);
		setRightSideLayout(LAYOUTS.REPORT_CONTENT);
	};
	const handleGoToMessage = async (msgId, chatId) => {
		//  Helper.cl("gtm 1");
		try {
			dispatch(selectGroup("All"));
			// Helper.cl("gtm 2");
			const chunks = entities.chats.byId[chatId ?? activeChat]?.entities.chunks;
			//Helper.cl("gtm 3");
			const msgChunkId = chunks?.ids.find((ch_id) => msgId >= chunks.byId[ch_id]?.start && msgId <= chunks.byId[ch_id]?.end);
			// Helper.cl("gtm 4");
			await dispatch(searchMessage(msgId, msgChunkId, chatId, loggedUserId));
			//  Helper.cl("gtm 5");
			rightSideLayout && setRightSideLayout(null);
			modalLayout && setModalLayout(null);
			//  Helper.cl("gtm 6");
		} catch (error) {
			return;
		}
	};

	const handleDeleteMessage = async (chatId, msgId) => {
		try {
			const selected = selectedMultipleMessages.length ? selectedMultipleMessages : [msgId];

			for (let i = 0; i < selected.length; i++) {
				await dispatch(deleteMessage(chatId, selected[i], entities?.chats?.byId[chatId]?.entities?.messages?.byId[msgId]?.status));
			}
			unSelectAll();
		} catch (err) {
			toast.error(err?.message);
		}
	};

	const handleUnsendMessage = async (chatId, msgId) => {
		try {
			const selected = selectedMultipleMessages.length ? selectedMultipleMessages : [msgId];

			for (let i = 0; i < selected.length; i++) {
				await dispatch(unsendMessage(chatId, selected[i]));
			}
			unSelectAll();
		} catch (err) {
			toast.error(err?.message);
		}
	};

	const handleTagMessage = async (chatId, messageId, tag) => {
		try {
			const selected = selectedMultipleMessages.length ? selectedMultipleMessages : [messageId];

			for (let i = 0; i < selected.length; i++) {
				const message = entities.chats.byId[chatId].entities.messages.byId[selected[i]];

				const payload = {
					tag: tag,
					value: !message?.tags?.some((t) => t === tag),
				};

				await dispatch(tagMessage(chatId, selected[i], payload));
				unSelectAll();
			}
		} catch (err) {
			toast.error(err?.message);
		}
	};

	const handleSetForwardMessage = (chatId, msgId) => {
		if (entities?.chats.byId?.[chatId]?.entities.messages.byId?.[msgId]?.tags?.some?.((tg) => tg === MessageTags.Unsend)) {
			toast.warning(tr`cannot forward unsent messages`);

			return;
		}

		msgId ? setRightSideLayout(LAYOUTS.FORWARD_MESSAGE) : setRightSideLayout(null);
		dispatch(setForwardedMessage(selectedMultipleMessages?.length ? selectedMultipleMessages : [msgId]));
	};

	const handleSetRepliedMessage = (msgId) => dispatch(setRepliedOnMessageId(msgId));

	const handleForwardMessage = async (msgIds, contactOrChatId, isExisting) => {
		// try {
		for (let i = 0; i < msgIds.length; i++) {
			const message = entities.chats.byId[activeChat].entities.messages.byId[msgIds[i]];
			await dispatch(forwardMessage(message, contactOrChatId, loggedUserId, isExisting));
		}
		// } catch (err) {
		//   toast.error(err?.message);

		// } finally {
		//   handleSetForwardMessage(null);
		// }
	};

	const handleReplyToMessage = (chatId, messageId) => {
		if (entities?.chats.byId?.[chatId].entities.messages.byId?.[messageId]?.tags?.some?.((tg) => tg === MessageTags.Unsend)) {
			toast.warning(tr`cannot reply to unsent messages`);

			return;
		}
		handleSetRepliedMessage(messageId);
	};

	const handleReportMessage = (chatId, messageId) => {
		setMessageTorReport(messageId);
		setChatTorReport(chatId);
		setModalLayout(LAYOUTS.REPORT_MESSAGE);

		// return user?.type === "teacher"
		//   ? history.push(
		//       `${process.env.REACT_APP_BASE_URL}/teacher/report-content/messages/${messageId}`
		//     )
		//   : user?.type === "employee"
		//   ? history.push(
		//       `${process.env.REACT_APP_BASE_URL}/admin/report-content/messages/${messageId}`
		//     )
		//   : user?.type === "student"
		//   ? history.push(
		//       `${process.env.REACT_APP_BASE_URL}/student/report-content/messages/${messageId}`
		//     )
		//   : user?.type === "parent"
		//   ? history.push(
		//       `${process.env.REACT_APP_BASE_URL}/parent/report-content/messages/${messageId}`
		//     )
		//   : history.push(
		//       `${process.env.REACT_APP_BASE_URL}/senior-teacher/report-content/messages/${messageId}`
		//     );
	};

	//chats
	const handleTagChat = (chatId, tag) => {
		const selected = selectedMultipleChats.length ? selectedMultipleChats : [chatId];

		for (let i = 0; i < selected.length; i++) {
			const tagged = entities?.chats?.byId?.[selected[i]]?.tags?.some?.((tg) => tg === tag);
			dispatch(tagChat(selected[i], tag, tagged));
		}
		unSelectAll();
	};

	const handleDeleteChat = (chatId) => {
		const selected = selectedMultipleChats.length ? selectedMultipleChats : [chatId];

		for (let i = 0; i < selected.length; i++) {
			dispatch(deleteChat(selected[i]));
		}
		unSelectAll();
	};

	const handleSelectMultipleChats = (chatId) => dispatch(selectMultipleChats(chatId));

	const [chatToReport, setChatTorReport] = useState(-1);
	const [messageToReport, setMessageTorReport] = useState(-1);
	const handleReportChat = (chatId) => {
		setChatTorReport(chatId);

		setModalLayout(LAYOUTS.REPORT_CONTENT);
		// return user?.type === "teacher"
		//   ? history.push(
		//       `${process.env.REACT_APP_BASE_URL}/teacher/report-content/chats/${chatId}`
		//     )
		//   : user?.type === "employee"
		//   ? history.push(
		//       `${process.env.REACT_APP_BASE_URL}/admin/report-content/chats/${chatId}`
		//     )
		//   : user?.type === "student"
		//   ? history.push(
		//       `${process.env.REACT_APP_BASE_URL}/student/report-content/chats/${chatId}`
		//     )
		//   : user?.type === "parent"
		//   ? history.push(
		//       `${process.env.REACT_APP_BASE_URL}/parent/report-content/chats/${chatId}`
		//     )
		//   : history.push(
		//       `${process.env.REACT_APP_BASE_URL}/senior-teacher/report-content/chats/${chatId}`
		//     );
	};

	const handleSelectGroup = (groupTag) => dispatch(selectGroup(groupTag));

	const handleSelectMultipleMessages = (chatId, messageId) => dispatch(selectMultipleMessages(messageId));

	const init = async () => {
		try {
			if (storeIsInitialized) return;
			setloading({ left: true, right: false });
			await dispatch(initializeConversations(loggedUserId));
		} catch (error) {
			toast.error(error?.message);
		} finally {
			setloading({ left: false, right: false });
		}
	};

	const unSelectAll = (e) => {
		if (e?.target?.id?.split("_")?.[0] === "ctm") return;
		dispatch(selectMultipleChats(null));
		dispatch(selectMultipleMessages(null));
	};
	const ready = useIMInit();
	useEffect(() => {
		// //init();
		// window.addEventListener("click", unSelectAll);
		// return () => {
		// 	window.removeEventListener("click", unSelectAll);
		// };
	}, []);

	const setSelectedChat = (id) => {
		newMessageVisible && setNewMessageVisible(false);
		dispatch(selectChat(id));
		rightSideLayout && setRightSideLayout(null);
		modalLayout && setModalLayout(null);
	};

	const handleUpdateSettings = (name, value) => dispatch(updateSettings(name, value));

	const submitSettings = async () => {
		try {
			// setloading({ left: false, right: true });
			const response = await IMApi.updateSettings({ payload: settings });
			if (!response.data.status) throw new Error(response.data.msg);
			toast.success(response.data.message);

			//hideRightSideLayout();
			hideModalLayout();
		} catch (error) {
			toast.error(error?.message);
		}
		// finally {
		//   // setloading({ left: false, right: false });
		// }
	};

	const handleSend = async (
		message,
		attachments = [],
		recievers = null /*if not null new conversation is initialized */,
		groupName = null,
		replyFor = null,
		groupImage = null
	) => {
		const replyMessage = replyFor ? entities?.chats.byId?.[activeChat]?.entities.messages.byId?.[replyFor] : null;

		setSending(true);
		try {
			await dispatch(
				recievers
					? createChat(
							recievers,
							{
								content: message,
								sendDate: new Date().getTime(),
								senderId: user.id,
								attachments: [],
							},
							user,
							groupName,
							groupImage
					  )
					: sendMessage(
							activeChat,
							{
								content: message,
								sendDate: new Date().getTime(),
								senderId: user.id,
								attachments: attachments,
							},
							replyMessage
					  )
			);
			hideRightSideLayout();
			hideModalLayout();

			repliedMessage && handleSetRepliedMessage(null);
		} catch (err) {
			if (recievers) toast.error(err?.message);
			else throw err;
		} finally {
			setSending(false);
		}
	};

	const handleChangePointer = async () => {
		try {
			if (!activeChat) return;

			await dispatch(changePointer(activeChat));
		} catch (err) {
			toast.error(err?.message);
		}
	};

	//direction true if up an dfalse if bottom

	const paginateMessages = async (direction) => {
		Helper.cl("paginateMessages ...");
		try {
			const activeChunk = entities.chats.byId[activeChat].entities.chunks.byId[entities.chats.byId[activeChat].activeChunk];
			Helper.cl(activeChunk, "activeChunk");
			if (
				(direction && activeChunk?.nextPage && activeChunk?.nextPage < 0) ||
				(!direction && activeChunk.prevPage && activeChunk.prevPage < 0)
			) {
				Helper.cl("paginateMessages1");
				return false;
			}
			// chat doesn't have more messages

			const p =
				entities.chats.byId[activeChat].entities.chunks.byId[entities.chats.byId[activeChat].activeChunk][
					direction ? "nextPage" : "prevPage"
				];
			Helper.cl(p, "fetching more messages ... p");
			await dispatch(fetchMoreMessages(activeChat, p, direction, loggedUserId));
			return true;
		} catch (err) {
			// toast.error(err?.message);

			return false;
		}
	};

	// const toggleNewMessageVisible = () => {
	//   setNewMessageVisible(!newMessageVisible);
	// };
	const MessageActionSelected = [
		{
			label: "report",
			action: (chatId, msgId) => handleReportMessage(chatId, msgId),
			icon: ICONS[MessageTags.Reported],
		},
		{
			label: "reply",
			action: (chatId, msgId) => handleReplyToMessage(chatId, msgId, ""),
			icon: ICONS[MessageTags.Reply],
		},
	];

	const MessageActions = [
		{
			label: "starred",
			action: (chatId, msgId) => handleTagMessage(chatId, msgId, MessageTags.Starred),
			icon: ICONS[MessageTags.Starred],
			color: "yellow",
		},
		// {
		//   label: "important",
		//   action: (chatId, msgId) =>
		//     handleTagMessage(chatId, msgId, MessageTags.Important),
		//   icon: ICONS[MessageTags.Important],
		//   color: "#668ad7"
		// },
		// {
		//   label: "urgent",
		//   action: (chatId, msgId) =>
		//     handleTagMessage(chatId, msgId, MessageTags.Urgent),
		//   icon: ICONS[MessageTags.Urgent],
		//   color: "orange"
		// },
		{
			label: "unsend",
			action: (chatId, msgId) => handleUnsendMessage(chatId, msgId),
			icon: ICONS[MessageTags.Unsend],
		},
		{
			label: "forward",
			action: (chatId, msgId) => handleSetForwardMessage(chatId, msgId),
			icon: ICONS[MessageTags.Forward],
			color: "black",
		},
		{
			label: "Select",
			action: (chatId, msgId) => handleSelectMultipleMessages(chatId, msgId),
			icon: CHAT_ACTIONS_ICONS[ChatTags.Select],
			color: "green",
		},
		{
			label: "delete",
			action: (chatId, msgId) => handleDeleteMessage(chatId, msgId),
			icon: ICONS[MessageTags.Delete],
			color: "red",
		},
	];

	const chatActionSelected = [
		{
			label: "Report",
			action: (chatId) => handleReportChat(chatId),
			icon: CHAT_ACTIONS_ICONS[ChatTags.Reported],
			color: "black",
		},
	];
	const chatActions = [
		{
			label: "Starred",
			action: (chatId) => handleTagChat(chatId, ChatTags.Starred),
			icon: CHAT_ACTIONS_ICONS[ChatTags.Starred],
			color: "yellow",
		},
		// {
		//   label: "Urgent",
		//   action: (chatId) => handleTagChat(chatId, ChatTags.Urgent),
		//   icon: CHAT_ACTIONS_ICONS[ChatTags.Urgent],
		// },
		{
			label: "Muted",
			action: (chatId) => handleTagChat(chatId, ChatTags.Muted),
			icon: CHAT_ACTIONS_ICONS[ChatTags.Muted],
			color: "black",
		},
		{
			label: "Archive",
			action: (chatId) => handleTagChat(chatId, ChatTags.Archived),
			icon: CHAT_ACTIONS_ICONS[ChatTags.Archived],
			color: "black",
		},
		// {
		//   label: "Important",
		//   action: (chatId) => handleTagChat(chatId, ChatTags.Important),
		//   icon: CHAT_ACTIONS_ICONS[ChatTags.Important],
		// },

		{
			label: "Select",
			action: (chatId) => handleSelectMultipleChats(chatId),
			icon: CHAT_ACTIONS_ICONS[ChatTags.Select],
			color: "green",
		},
		{
			label: "Delete",
			action: (chatId) => handleDeleteChat(chatId),
			icon: CHAT_ACTIONS_ICONS[ChatTags.Delete],
			color: "red",
		},
	];

	// useEffect(() => {
	// 	(user?.type == "parent" || user?.type == "student") && dispatch(trackLastSeenAsync({ payload: { trackable_type: "chats" } }));
	// }, []);
	const { lastMessageChatId } = useSelector((state) => state?.iMcycle2);
	const changePointerIfLastMessageInActiveChat = async () => {
		Helper.cl(activeChat, "changePointerIfLastMessageInActiveChat f1 activeChat");
		Helper.cl(lastMessageChatId, "changePointerIfLastMessageInActiveChat f1 lastMessageChatId");
		if (activeChat && lastMessageChatId && activeChat === lastMessageChatId) {
			Helper.cl("changePointerIfLastMessageInActiveChat f2");
			try {
				Helper.cl("changePointerIfLastMessageInActiveChat f3 pre dispach");
				await dispatch(changePointer(lastMessageChatId));
				Helper.cl("changePointerIfLastMessageInActiveChat f4 after await success");
			} catch (err) {
				Helper.cl(err, "changePointerIfLastMessageInActiveChat f5 err");
				return;
			}
		}
	};

	React.useEffect(() => {
		changePointerIfLastMessageInActiveChat();
	}, [lastMessageChatId]);

	const rightSideLayoutProps = {
		[LAYOUTS.FORWARD_MESSAGE]: {
			handleForward: (contactOrChatId, isExisting) => handleForwardMessage(forwardedMessage, contactOrChatId, isExisting),
			loggedUserId: loggedUserId,
			cancelForwarding: () => handleSetForwardMessage(null),
			messageId: forwardedMessage,
		},
		[LAYOUTS.NEW_MESSAGE]: {
			handleSend: handleSend,
			sending: sending,
			loggedUserId: loggedUserId,
			toggleNewMessage: hideRightSideLayout,
		},
		[LAYOUTS.SETTINGS]: {
			settings: settings,
			updateSettings: handleUpdateSettings,
			toggleSettings: hideRightSideLayout,
			handleSubmit: submitSettings,
		},
		// [LAYOUTS.SEARCH_MULTIPLE_CHATS]: {
		//   hideSearchMultiple: () => setRightSideLayout(null),
		//   handleSearchMessage: (chatId, messageId) =>
		//     handleGoToMessage(messageId, chatId),
		// },
		[LAYOUTS.GROUP_MANAGEMENT]: {
			isGroup: entities?.chats?.byId?.[activeChat]?.name,
			chatId: activeChat,
			isAdmin: entities?.chats?.byId?.[activeChat]?.isAdmin,
			loggedUserId: loggedUserId,
			closeAll: toggleManagement,
		},

		[LAYOUTS.REPORT_CONTENT]: {
			loggedUserId: loggedUserId,
			chatId: chatToReport,
			table_name: messageToReport && messageToReport > 0 ? "messages" : "chats",
			row_id: messageToReport && messageToReport > 0 ? messageToReport : chatToReport,
			closeAll: toggleReport,
		},

		[LAYOUTS.REPORT_MESSAGE]: {
			loggedUserId: loggedUserId,
			chatId: chatToReport,
			message_id: messageToReport,
			closeAll: () => {
				setModalLayout(null);
			},
		},
	};

	const RightSide = MAPPING[rightSideLayout];

	//useEffect(()=>{})
	const ModalContent = MAPPING[modalLayout];
	// Helper.cl(ModalContent,"ModalContent")

	return (
		<RFlex
			dir={dir ? "ltr" : "rtl"}
			id="IM"
			styleProps={{ width: "100%", flexDirection: "row", height: !floating ? "88vh" : "65vh" }}
			// className={
			//   user?.type === "student" || user?.type === "parent"
			//     ? "paddingTopIM"
			//     : "content"
			// }
		>
			<NotificationPermissionComponent />
			{floating ? (
				<>
					<ChatList
						chats={entities.chats}
						setSelectedChat={setSelectedChat}
						toggleNewMessageVisible={() => setModalLayout(LAYOUTS.NEW_MESSAGE)}
						loggedUserId={loggedUserId}
						selectedChat={activeChat}
						chatActions={selectedMultipleChats.length <= 1 ? chatActions.concat(chatActionSelected) : chatActions}
						groupAsFolders={settings.show_chat_folders}
						toggleSettings={() => setModalLayout(LAYOUTS.SETTINGS)}
						toggleSearch={() => setModalLayout(LAYOUTS.SEARCH_MULTIPLE_CHATS)}
						handleSelectGroup={handleSelectGroup}
						selectedGroup={selectedGroup}
						selectedMultipleChats={selectedMultipleChats}
						handleGoToMessage={handleGoToMessage}
						unread={unread}
						floating={true}
					/>
					<AppModal
						// open={openModal}
						// setOpen={()=>}

						//isOpen={(rightSideLayoutProps&&rightSideLayout&& rightSideLayoutProps[rightSideLayout])}
						show={rightSideLayoutProps && modalLayout && rightSideLayoutProps[modalLayout]}
						parentHandleClose={() => {
							setModalLayout(null);
						}}
						// header={<h6 className="text-capitalize">{modalLayout}</h6>}
						// footer={<div>
						//   {/* <RButton  text={tr`Create Group`} onClick={()=>{}} color="primary" />  */}
						//   <RButton text={tr`cancel`} onClick={() => { () => { } }}
						//     color="white"
						//     style={{ color: "#668ad7", background: "white", border: "0px solid" }}
						//   />
						// </div>
						// }

						headerSort={
							<div style={{ height: "500px" }}>
								{/* rightSideLayoutProps={Helper.js(rightSideLayoutProps)}<br/>
    modalLayout={Helper.js(modalLayout)}<br/>
    rightSideLayoutProps[modalLayout]={(rightSideLayoutProps&&modalLayout&& rightSideLayoutProps[modalLayout])?Helper.js( rightSideLayoutProps[modalLayout]):"nothing"}
    <br/>ModalContent={ModalContent?Helper.js(ModalContent):"modl content is null"} */}
								{rightSideLayoutProps && modalLayout && rightSideLayoutProps[modalLayout] ? (
									<ModalContent {...rightSideLayoutProps[modalLayout]} />
								) : (
									<div>something is null</div>
								)}
							</div>
						}
					></AppModal>
				</>
			) : (
				<>
					<AppModal
						// open={openModal}
						// setOpen={()=>}

						//isOpen={(rightSideLayoutProps&&rightSideLayout&& rightSideLayoutProps[rightSideLayout])}
						show={rightSideLayoutProps && modalLayout && rightSideLayoutProps[modalLayout]}
						parentHandleClose={() => {
							setModalLayout(null);
						}}
						// header={<h6 className="text-capitalize">{modalLayout}</h6>}
						// footer={<div>
						//   {/* <RButton  text={tr`Create Group`} onClick={()=>{}} color="primary" />  */}
						//   <RButton text={tr`cancel`} onClick={() => { () => { } }}
						//     color="white"
						//     style={{ color: "#668ad7", background: "white", border: "0px solid" }}
						//   />
						// </div>
						// }

						headerSort={
							<div style={{ height: "500px" }}>
								{/* rightSideLayoutProps={Helper.js(rightSideLayoutProps)}<br/>
    modalLayout={Helper.js(modalLayout)}<br/>
    rightSideLayoutProps[modalLayout]={(rightSideLayoutProps&&modalLayout&& rightSideLayoutProps[modalLayout])?Helper.js( rightSideLayoutProps[modalLayout]):"nothing"}
    <br/>ModalContent={ModalContent?Helper.js(ModalContent):"modl content is null"} */}
								{rightSideLayoutProps && modalLayout && rightSideLayoutProps[modalLayout] ? (
									<ModalContent {...rightSideLayoutProps[modalLayout]} />
								) : (
									<div>something is null</div>
								)}
							</div>
						}
					></AppModal>
					{
						/*loading.left */ !storeIsInitialized ? (
							<Loader />
						) : (
							<ChatList
								chats={entities.chats}
								setSelectedChat={setSelectedChat}
								toggleNewMessageVisible={() => setModalLayout(LAYOUTS.NEW_MESSAGE)}
								loggedUserId={loggedUserId}
								selectedChat={activeChat}
								chatActions={selectedMultipleChats.length <= 1 ? chatActions.concat(chatActionSelected) : chatActions}
								groupAsFolders={settings.show_chat_folders}
								toggleSettings={() => setModalLayout(LAYOUTS.SETTINGS)}
								toggleSearch={() => setModalLayout(LAYOUTS.SEARCH_MULTIPLE_CHATS)}
								handleSelectGroup={handleSelectGroup}
								selectedGroup={selectedGroup}
								selectedMultipleChats={selectedMultipleChats}
								handleGoToMessage={handleGoToMessage}
								unread={unread}
							/>
						)
					}
					{!storeIsInitialized ? (
						<>
							<Loader />
						</>
					) : (
						<ChatContent
							showIcons=""
							chat={entities?.chats?.byId[activeChat]}
							newMessageVisible={false} //{rightSideLayout === LAYOUTS.NEW_MESSAGE}
							loggedUserId={loggedUserId}
							loadMoreMessages={paginateMessages}
							changePointer={handleChangePointer}
							sendMessage={handleSend}
							handleSearch={handleGoToMessage}
							searchedMessage={searchedMessage}
							sending={sending}
							messageActions={selectedMultipleMessages?.length <= 1 ? MessageActions.concat(MessageActionSelected) : MessageActions}
							setRepliedOnMessageId={handleSetRepliedMessage}
							repliedMessageId={repliedMessage}
							selectedMultipleMessages={selectedMultipleMessages}
							toggleManagement={toggleManagement}
						/>
					)}
					<RFlex
						styleProps={{
							display: rightSideLayout ? "block" : "none",
							width: "32%",
							marginTop: "-20px",
							minHeight: "100%",
							padding: "20px 10px",
						}}
						className="RightLayout"
					>
						{/* rightSideLayoutProps={Helper.js(rightSideLayoutProps)}<br/>
    rightSideLayout={Helper.js(rightSideLayout)}<br/>
    rightSideLayoutProps[rightSideLayout]={(rightSideLayoutProps&&rightSideLayout&& rightSideLayoutProps[rightSideLayout])?Helper.js( rightSideLayoutProps[rightSideLayout]):"nothing"}
    <br/>rightSide={RightSide ?Helper.js(RightSide):"rightSide is null"} */}
						{rightSideLayoutProps && rightSideLayout && rightSideLayoutProps[rightSideLayout] ? (
							<RightSide {...rightSideLayoutProps[rightSideLayout]} />
						) : (
							<div>something is null</div>
						)}
					</RFlex>
				</>
			)}
		</RFlex>
	);
}

export default withDirection(IM);
