import { ChatTags } from "./ChatTags"

export const chat_Folders = [
    {
        label: 'All',
        icon: 'fa fa-comments',
        color: 'black',
        id: ChatTags.All
        
    },
    {
        label: 'Starred',
        icon: 'fa fa-star',
        color: '#fec89a',
        id: ChatTags.Starred
    },
    {
        label: 'Important',
        icon: 'fa fa-id-card',
        color: '#f9dcc4',
        id: ChatTags.Important
    },
    {
        label: 'Reported',
        icon: 'fa fa-hand-paper-o',
        color: '#f8edeb',
        id: ChatTags.Reported
    },
    {
        label: 'Urgent',
        icon: 'fa fa-free-code-camp',
        color: '#fcd5ce',
        id: ChatTags.Urgent
    },

    {
        label: 'Muted',
        icon: 'fa fa-bell-slash',
        color: '#ffb5a7',
        id: ChatTags.Muted
    },
    {
        label: 'Archived',
        icon: 'fa fa-archive',
        color: '#fcbf49',
        id: ChatTags.Archived
    },
    
]