import { IMApi } from "api/IMNewCycle";
import { CategoryScale } from "chart.js";
import Helper from "components/Global/RComs/Helper";
import tr from "components/Global/RComs/RTranslator";
import React, { useState } from "react";
import { useEffect } from "react";
import { toast } from "react-toastify";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";

import Swal, { SUCCESS, WARNING, DANGER } from "utils/Alert";

export const ReportContent = (props) => {
	const [categories, setCategories] = useState();
	const [category, setCategory] = useState();
	const [text, setText] = useState();

	useEffect(() => {
		const getAllReportCategories = async (chatId) => {
			const response = await IMApi.getAllReportCategories(chatId);
			setCategories(response.data.data.data);
			Helper.cl(response.data.data.data, "reponse.data.data");
		};
		getAllReportCategories();
	}, []);

	const handleSubmit = async (event) => {
		event.preventDefault(); // prevent default form submission behavior

		// call the API with the category state
		const response = await IMApi.saveReport({
			content_report_category_id: category,
			table_name: props.table_name,
			row_id: props.row_id,
			report_text: text,
		});

		// alert(response);
		if (response.data.status == 1) {
		} else {
			toast.error(response.data.msg);
		}
	};
	return (
		<Form onSubmit={handleSubmit}>
			<FormGroup>
				<Label>Category:</Label>
			</FormGroup>
			{/* {Helper.js(categories)} */}
			{categories &&
				categories.map((cat) => (
					<FormGroup check>
						{/* {Helper.js(cat)} */}
						<Label check>
							<Input type="radio" name="category" value={cat.id} checked={category === cat.id} onChange={() => setCategory(cat.id)} />{" "}
							{cat.type}
						</Label>
					</FormGroup>
				))}
			<FormGroup>
				<Label>
					More Info
					<Input type="text" name="text" onChange={(e) => setText(e.target.value)} />
				</Label>
				<div></div>
				<Button type="submit">Submit</Button>
			</FormGroup>
		</Form>
	);
};
