import React, { useState } from "react";
import EmojiPicker, { EmojiStyle } from "emoji-picker-react";
import RButtonIcon from "components/Global/RComs/RButtonIcon";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";
import produce from "immer";
import RFileSuite from "components/Global/RComs/RFile/RFileSuite";
import SendMessageIcon from "../../../../assets/img/sendMessage.svg";
import MessageInputSender from "./MessageInputSender";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import styles from "./reply.module.scss";

function MessageInput({ sendMessage, chatId, repliedMessageId, chat, setRepliedOnMessageId, validateEmptyMessage = true }) {
	const [hotArea, setHotArea] = useState(false);
	const [msgInput, setMsgInput] = useState("");
	const [attachments, setAttachments] = React.useState([]);
	const [showEmojiPicker, setShowEmojiPicker] = React.useState(false);
	const emojiArea = React.useRef(null);

	const username = chat?.entities?.users?.byId?.[chat?.entities?.messages?.byId?.[repliedMessageId]?.user_id]?.username;

	const messageContent = chat?.entities?.messages?.byId?.[repliedMessageId]?.message;
	const hideEmojiPicker = React.useCallback(
		(e) => !emojiArea.current?.contains(e.target) && e.target.id !== "emoji-toggler" && setShowEmojiPicker(false),
		[showEmojiPicker, emojiArea]
	);

	React.useEffect(() => {
		window.addEventListener("click", hideEmojiPicker);

		return () => window.removeEventListener("click", hideEmojiPicker);
	}, []);

	const handleSendMessageInterceptor = async (text, attachments) => {
		try {
			if (validateEmptyMessage && !text && !attachments.length /*|| sending*/) {
				toast.error("Empty message");
				return;
			}

			await sendMessage(text, attachments);

			setMsgInput("");
			setAttachments([]);
			setHotArea(false);

			await dispatch(changePointer(chatId));
		} catch (err) {}
	};

	const msgOnChange = (e) => {
		setMsgInput(e.target.value);
	};

	const _handleKeyDown = async (e) => {
		if (e.key === "Enter") {
			handleSendMessageInterceptor(msgInput, attachments);
		}
	};

	const addEmoji = (e) => {
		let sym = e.unified.split("-");
		let codesArray = [];
		sym.forEach((el) => codesArray.push("0x" + el));
		let emoji = String.fromCodePoint(...codesArray);
		setMsgInput(msgInput + emoji);
	};
	const [attachments1, setAttachments1] = useState([]);

	const setSpecificAttachment = (att) => {
		// Helper.cl(att,"setSpecificAttachment att");
		const new_attachments = produce(attachments1, (attachments_temp) => {
			const ind = attachments_temp.findIndex((a) => a.tempid == att.tempid);
			//Helper.cl(ind,"setSpecificAttachment1 att1");
			if (ind == -1) {
				attachments_temp.push(att);
			} else attachments_temp[ind] = att;
		});
		setAttachments1(new_attachments);
	};
	return (
		<div id="Sender" style={{ display: "flex", flexDirection: "column", alignItems: "start" }}>
			{hotArea && (
				<RFileSuite
					parentCallback={(files) => setAttachments(files)}
					placeholder="files"
					fileSize={5}
					binary={true}
					uploadName={"upload name 1"}
					setSpecificAttachment={setSpecificAttachment}
					theme="row1"
					fullWidth={true}
					removeButton={true}
					value={attachments}
					style={hotArea ? { position: "relative", width: "300px" } : { width: "1px", height: "1px", display: "none" }}
					showReplace={true}
					slideStyle={{ width: "100%", border: "0px red solid" }}
					swiperStyle={{ width: "100%", border: "0px green solid" }}
					dynamicSlide={true}
				/>
			)}
			{repliedMessageId && (
				<RFlex className="flex-column" styleProps={{ width: "98%", padding: "0px 8px", gap: "0px", marginBottom: "12px" }}>
					<span className="p-0 m-0">{username}</span>
					<RFlex className="justify-content-between align-items-center">
						<p className={`m-0 ${styles.ReplayDiv}`}>{messageContent}</p>
						<i className="fa-solid fa-close fa-lg" style={{ cursor: "pointer" }} onClick={() => setRepliedOnMessageId(null)} />
					</RFlex>
				</RFlex>
			)}
			<div
				className="R_text_box d-flex justify-content-center"
				style={{ width: "100%", gap: "10px" }}
				dir="ltr"
				onClick={(e) => e.preventDefault()}
				onDrop={() => {
					setHotArea(false);
				}}
				onDragOver={() => {
					setHotArea(true);
				}}
				onDragLeave={() => {
					setHotArea(false);
				}}
			>
				{showEmojiPicker ? (
					<div
						style={{
							backgroundColor: "white",
							zIndex: 1000000000,
							position: "absolute",
							bottom: "15%",
							left: "10%",
						}}
						ref={emojiArea}
					>
						<EmojiPicker emojiStyle={EmojiStyle.GOOGLE} onEmojiClick={addEmoji} />{" "}
					</div>
				) : (
					<i id="emoji-toggler" className="nc-icon nc-satisfied" onClick={() => setShowEmojiPicker(true)} />
				)}

				{/* <RButtonIcon
          onClick={() => {
            setHotArea(!hotArea);
          }}
          style={
            !hotArea
              ? {
                  border: "red 3px solid",
                  position: "relative",
                  width: "100%",
                  backgroundColor: "white",
                }
              : { display: "none" }
          }
          color={"red"}
          backgroundColor={"white"}
          text={!hotArea ? "upload" : "close"}
          padding={"10px"}
          faicon={!hotArea ? "paperclip" : "close"}
        ></RButtonIcon> */}

				<RButtonIcon
					onClick={() => {
						if (!hotArea) {
							setAttachments([]);
						}
						setHotArea(!hotArea);
					}}
					ButtonAdditionalStyle={
						!hotArea
							? {
									// border: "red 3px solid",
									// position: "relative",
									// width: "100%",
									backgroundColor: "white",
									border: "0px solid white",
							  }
							: {
									backgroundColor: "white",
									border: "0px solid white",
							  }
					}
					color={"red"}
					backgroundColor={"white"}
					text={!hotArea ? "upload" : "close"}
					padding={"10px"}
					faicon={!hotArea ? "paperclip" : "close"}
				></RButtonIcon>

				<MessageInputSender
					required={true}
					Input={msgInput}
					onClick={() => setShowEmojiPicker(false)}
					value
					msgOnChange={msgOnChange}
					onKeyDown={_handleKeyDown}
					placeholder={tr("type_here")}
					ClassNames="MessageInput"
					id={"Tooltip-chat" + chatId}
					// style={{ borderRadius: "2px", border: "1px solid #668ad7", padding: "3px", margin: "3px" }}
				></MessageInputSender>

				<button
					className="SendMessageButton"
					disabled={validateEmptyMessage && msgInput == "" ? true : false}
					onClick={() => {
						handleSendMessageInterceptor(msgInput, attachments);
					}}
				>
					<img src={SendMessageIcon} />
					{/* <RSubmit
            disabled={validateEmptyMessage && msgInput == "" ? true : false}
            icon={"nc-icon nc-send"}
            value=" "
            className="Send_btn btn btn-secondary"
            style={{color:"#668ad7",borderRadius:"3px"}}
            onClick={() => {
              handleSendMessageInterceptor(msgInput, attachments);
            }}
            // value={tr`send`}
          /> */}
				</button>
			</div>
			{/* {msgInput == "" && (
        <UncontrolledPopover
          placement="top"
          target={"Tooltip-chat" + chatId}
          trigger="click"
        >
          <PopoverHeader>{tr`warning`}</PopoverHeader>
          <PopoverBody>{tr`message_content_should_not_be_empty`}</PopoverBody>
        </UncontrolledPopover>
      )} */}
		</div>
	);
}

export default MessageInput;
