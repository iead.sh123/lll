import React, { useState } from "react";
import UserImg from "../userImg";
import moment from "moment";
import tr from "components/Global/RComs/RTranslator";
import handleMessage from "components/Global/handleMessage";
import { relativeDate } from "utils/HandleDate";
import { MessageStatus } from "store/actions/IMCycle2.actions";
import { ICONS, MessageTags } from "../constants/MessageTags";
import RResourceViewer from "components/Global/RComs/RResourceViewer/RResourceViewer";
import Loader from "utils/Loader";
import ContextMenu from "../ContextMenu/ContextMenu";
import { Services } from "engine/services";
import Helper from "components/Global/RComs/Helper";
import RFlex from "components/Global/RComs/RFlex/RFlex";
import optionIcon from "../../../../assets/img/options.svg";
import userLogo2 from "../../../../assets/img/testUserImage.svg";

function MessegeItem({
	message,
	loggeduserid,
	user_pointers,
	user_name,
	userImg,
	user_online,
	searchedMessage,
	isGroupSelected,
	actions,
	showUsr = true,
}) {
	const msgRef = React.useRef();
	const [showContext, setShowContext] = React.useState(false);
	const [isHovering, setIsHovering] = useState(false);
	const [isStarred, setIsStarred] = useState(false);
	React.useLayoutEffect(() => {
		if (message?.id === searchedMessage) {
			msgRef?.current?.scrollIntoViewIfNeeded(false, {
				block: "center",
				behavior: "smooth",
				inline: "nearest",
			});
		}
	}, [searchedMessage]);
	React.useEffect(() => {
		const starred = message?.tags?.filter((t) => t === "starred");
		setIsStarred((previousValue) => (starred?.length > 0 ? true : false));
	});
	const hideContextMenu = false; // React.useCallback(() => setShowContext(false), [showContext]);

	// React.useEffect(() => {
	//   window.addEventListener('click', hideContextMenu)
	//   return () => window.removeEventListener('click', hideContextMenu)
	// }, []);

	const toggleContextMenu = (e) => {
		e.preventDefault();

		setShowContext(!showContext);
	};

	let userName = user_name;
	let loggedUser = true;

	if (loggeduserid !== message?.user_id) {
		loggedUser = false;
	}
	if (!message) return <></>;

	return (
		<RFlex
			ref={msgRef}
			id="Whole message"
			/*onContextMenu={toggleContextMenu}*/
			className={loggedUser ? "SentMessage" : "ReciveMessage"}
			onMouseEnter={() => setIsHovering(true)}
			onMouseLeave={() => setIsHovering(false)}
			styleProps={isGroupSelected ? { background: "#aaa", gap: "2px" } : { gap: "2px" }}
		>
			<RFlex id="username with message body and actions" className="flex-column" styleProps={{ width: "35%", gap: "5px" }}>
				{showUsr && (
					<RFlex id="username with date" className="align-items-center" styleProps={{ width: "fit-content", gap: "5px" }}>
						<span className="UserNameStyle" style={{ margin: "0px", padding: "0px" }}>
							{userName}
						</span>
						<span
							className="DateStyle"
							style={{ margin: "0px", padding: "0px" }}
							title={moment(message?.created_at).format("YYYY:MM:DD HH:mm")}
						>
							{relativeDate(moment(message?.created_at).format("YYYY/MM/DD HH:mm"))}
						</span>
					</RFlex>
				)}
				<RFlex
					id="chat body with actions and pointers"
					styleProps={{ width: "100%", marginLeft: !loggedUser ? (showUsr ? "0px" : "45px") : "0px" }}
					className={loggedUser ? "flex-row-reverse align-items-stretch" : "align-items-stretch"}
				>
					<div
						id="chat body"
						className={loggedUser ? "SentMessageContent" : "ReciveMessageContent"}
						style={{
							background: showContext ? "blue" : searchedMessage === message?.id ? "red" : "",
							width: message?.message?.length < 30 ? "fit-content" : "",
							position: "relative",
						}}
					>
						<div id="message content" className="chatContent_msg">
							{message?.replyFor?.id ? (
								<div
									id="6666666666666666666666666666666"
									style={{
										fontStyle: "italic",
										borderBottom: "1px solid #FFF",
										background: "#D9E5FE",
										borderRadius: "4px",
									}}
								>
									{message?.replyFor?.message}
								</div>
							) : (
								<></>
							)}

							{message?.tags?.some?.((tg) => tg === MessageTags.Unsend) ? (
								<p id="1111" style={{ color: "orange" }}>
									{tr`This message has been deleted`} !
								</p>
							) : (
								<p
									id="22222"
									style={{ padding: "0px", margin: "0px" }}
									dangerouslySetInnerHTML={{
										__html: handleMessage(message?.message ?? ""),
									}}
								/>
							)}
						</div>
						<div id="file viewer">
							{/* {message?.attachments?.map(att=><img src={`${process.env.REACT_APP_RESOURCE_URL}${att.url}`}></img>)} */}
							{message?.status === MessageStatus.PENDING && message?.attachments?.length ? (
								<Loader />
							) : (
								<>
									<RResourceViewer sourceUrl={Services.storage.file} resources={message?.attachments} />
								</>
							)}
						</div>
						<RFlex id="pointers" className="MessagePointers" styleProps={{ gap: "0px" }}>
							{message?.user_id === loggeduserid && (
								<i
									style={{
										color:
											message?.status === MessageStatus.SUCCESS ? "green" : message?.status === MessageStatus.PENDING ? "#668ad7" : "red",
									}}
									className={
										message?.status === MessageStatus.SUCCESS
											? "fa fa-check"
											: message?.status === MessageStatus.PENDING
											? "fa fa-spinner"
											: "fa fa-exclamation"
									}
								/>
							)}

							{message?.tags?.map?.((t) => (
								<i key={"tag-" + t} className={ICONS[t]} style={{ color: t === "starred" ? "yellow" : "" }} />
							))}
						</RFlex>
					</div>
					<RFlex id="actions and pointers" className="flex-column justify-content-center align-items-end" styleProps={{ gap: "0px" }}>
						<RFlex id="actions" styleProps={{ gap: "2px", flexDirection: "row-reverse", visibility: isHovering ? "visible" : "hidden" }}>
							<ContextMenu
								icon={optionIcon}
								actions={actions.filter((ac) => {
									if (message?.user_id === loggeduserid) return ac.label !== "report" && ac.label !== "reply" && ac.label !== "starred";
									else {
										return ac.label !== "unsend" && ac.label !== "reply" && ac.label !== "starred";
									}
								})}
							/>
							{actions
								.filter((ac, index) => ac.label === MessageTags.Reply || ac.label === MessageTags?.Starred)
								.map((ac, index) => {
									return ac.label === MessageTags?.Starred ? (
										<i
											className={isStarred ? ac.icon : "fa-regular fa-star"}
											onClick={ac.action}
											style={{ cursor: "pointer", color: isStarred ? "yellow" : "#585858" }}
										/>
									) : (
										<i className={ac.icon} onClick={ac.action} style={{ cursor: "pointer", color: "#585858" }} />
									);
								})}
						</RFlex>
					</RFlex>
				</RFlex>
			</RFlex>

			{showUsr ? (
				<div id="User Image" style={{ alignSelf: "flex-start" }}>
					<UserImg
						name={userName /* ?userName : 'You' */}
						index={0}
						// image={Services?.storage?.file + userImg}
						image={userLogo2}
						id={message?.user_id}
						online={user_online}
						width={46}
					/>
				</div>
			) : (
				""
			)}
		</RFlex>
	);
}

export default MessegeItem;
