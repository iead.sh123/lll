import React from "react";

const MessageInputSender = (props) => {
  const calculateRows = () => {
    const lineCount = (props.Input.match(/\n/g) || []).length + 1;
    return Math.min(lineCount, 2); // Limit to a maximum of 2 rows
  };

  // Calculate the height based on the number of rows
  const calculateHeight = () => {
    const lineHeight = 20; // Adjust this value based on your font size and line height
    return `${lineHeight * calculateRows()}px`;
  };


const {  id,
  onClick, onKeyDown,placeholder,required,style,Input,msgOnChange, ClassNames, ...otherProps } = props;


  return (
    <textarea
    {...otherProps}
      required={required}
      rows={calculateRows()}
      className={ClassNames}
      value={Input}
      placeholder={placeholder}
      onChange={msgOnChange}
    //   onKeyDown={onKeyDown}
      {... onClick ? {onClick: onClick} : {}}
      id={id ? id : ""}
    />
  );
};

export default MessageInputSender;
