import Helper from "components/Global/RComs/Helper";
import React, { Component } from "react";
import { useRef } from "react";
import { useState } from "react";
/* import { Popover, PopoverHeader, PopoverBody} from 'reactstrap' */
import { Tooltip, UncontrolledTooltip } from "reactstrap";
import avatar from "../../../assets/img/avatar.png";
import UserFirstLetter from "./userFirstLetter";

// \src\views\Teacher\IM\userImg.js
// \src\assets\img\avatar.png
function UserImg(props) {
  //const [style, setStyle] = useState({display: 'none'});
  // return (
  //     <div className="App">
  //         <h2>Hidden Button in the box. Move mouse in the box</h2>
  //         <div style={{border: '1px solid gray', width: 300, height: 300, padding: 10, margin: 100}}
  //         >
  //             <button>Click</button>
  //         </div>
  //     </div>
  // );
  let width = 30;
  if (props.width) width = props.width;

  let classNames = "profile";
  if (props.classNames) classNames = props.classNames;

  let left = 0;
  // if (props.index > 1) {
  // left = -17;
  // }

  let style = {
    position: "absolute",
    fontWeight: 600,
    width: `${width}px`,
    height: `${width}px`,
    //   left :`${left}px`,
    borderWidth:
      props.width > 35
        ? "2.2px"
        : props.width > 28
        ? "2px"
        : props.width > 15
        ? "1.6px"
        : "0.9px",
    outlineWidth:
      props.width > 35
        ? "1.6px"
        : props.width > 28
        ? "1.1px"
        : props.width > 15
        ? "0.9px"
        : "0.6px",
  };

  /* borderRadius:'50%', borderColor:"#fff",
          borderStyle:'solid',  outlineStyle:"solid",
          outlineColor:'rgba(223, 223, 223, 1) ',
          position: "absolute"  */
  ///////////////////////////
  /*  const [popoverOpen,setPopoverOpen]=useState(false);
               
      const  toggle=()=> {
        setPopoverOpen(!popoverOpen)
              }
      
      const  onHover = () => {
          setPopoverOpen(true)
                 }
      
      const onHoverLeave = () => {
          setPopoverOpen(false)
        } */
  //////////////////////////////////
  const [tooltipOpen, setTooltipOpen] = useState(false);
  const toggle = () => {
    setTooltipOpen(!tooltipOpen);
  };
  const max = 1000000;
  const ref = useRef(null);
  const tooltipTarget = "Tooltip" + props.id + Math.floor(Math.random() * max);
  return (
    <>
      <div
        ref={ref}
        style={{
          width: `${width}px`,
          height: `${width}px`,
          // zIndex: props.index > -1 ? props.length - props.index : props.length,
          zIndex:0,
          marginLeft: props.index > 0 ? (props.index*(-20)+"px") : 0,
          left: props.index > 0 ? (props.index*(-20)+"px") : 0,
          direction: "ltr",
        }}
        className={(classNames, "User_Img")}
        /* onMouseOver={onHover}    onMouseLeave={onHoverLeave} */
      >
        <div style={{ position: "relative", width: "100%", height: "100%" }}>
          {props.image && !props.image.endsWith("undefined")       ? (
            <img
              src=
              // {
              //   process.env.REACT_APP_RESOURCE_URL +
              //   (props.image.url ? props.image.url : props.image)
              // }
              {props.image}
              alt="#"
              id="some user image"
              className="User_Img_img"
              style={style}
              width={`${width}px`}
              height={`${width}px`}
              //    title={props.name?props.name:"Profile"}
            />
          ) : (
          <UserFirstLetter str={props.name} width={width} />
          )}

          {/*props.online ? (
            width > 20 ? (
              <div
                id="onlinemark"
                className={width > 30 ? "is_online" : "is_online_small"}
              >
                {" "}
              </div>
            ) : (
              <div id="none"></div>
            )
            ) : width > 20 ? (
            <div
              id="onlinemark"
              className={width > 30 ? "is_offline" : "is_offline_small"}
            >
              {" "}
            </div>
          ) : (
            <div id="none"></div>
          )*/}



        </div>
      </div>
      <UncontrolledTooltip
        target={ref}
        placement="bottom"
        isOpen={tooltipOpen}
        toggle={toggle}
      >
        {props.name}
      </UncontrolledTooltip>

      {/*  <Popover
          placement=""
          isOpen={popoverOpen}
          target={"Popover1"+props.id}
        >
          <PopoverHeader>{props.name}</PopoverHeader>
          
        </Popover> */}
    </>
  );
}

export default UserImg;
