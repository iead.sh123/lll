import React from 'react';
import { IMApi } from 'api/IMNewCycle';
import tr from 'components/Global/RComs/RTranslator';
import AsyncSelect from "react-select/async";

const SearchMultipleChats = ({handleSearchMessage , hideSearchMultiple})=>{
    const debounceTimer = React.useRef(null);
    // const [message, setMessage] = React.useState(null);
    const [searchValue, setSearchValue] = React.useState("");
  
    // const [GroupImage, setGroupImage] = React.useState(null);
  
    // const onSelectMessageChange = (message) => setMessageId(values.map(v => ({ id: v.id, username: v.label, rel: v.rel })));
  
    const debounceLoadMessages = React.useCallback((delay = 1000) => {
      const loadMessages = async () => {
        const response = await IMApi.searchMultipleChats(searchValue);
        if (!response.data.status)
          return [];
  
        return response.data?.data?.messages.map(msg => ({
          label: msg.prefix_to_view + " : "+msg.message?.substring(0,25),
          value: msg.id,
          id: msg.id,
          chatId: msg.chat_id
        }))
  
      }
  
      return new Promise((resolve) => {
        clearTimeout(debounceTimer.current);
        debounceTimer.current = setTimeout(() => {
          resolve(loadMessages());
        }, delay);
      });
  
    }, [searchValue]);

return  <AsyncSelect
//   isMulti
styles={{
  container: (baseStyles, state) => ({
    ...baseStyles,
    width: "100%",
  }),
}}
  onInputChange={val => setSearchValue(val)}
  defaultValue={[]}
  placeholder="Search"
  loadOptions={() => debounceLoadMessages(1000)}
  onChange={(val)=>handleSearchMessage(val.chatId , val.id)}
/>
  
    return ( <div className={"search_container "}> 
                 {/* <div className="new_messege_show_head">
          ]<button
              className="show_icon"
              onClick={hideSearchMultiple}
            >
              <i className="fa fa-angle-left"></i>
            </button>=
            {tr("search")
          </div> */}
          <div className="RAutocomplete">
  
            <AsyncSelect
            //   isMulti
              onInputChange={val => setSearchValue(val)}
              defaultValue={[]}
              loadOptions={() => debounceLoadMessages(1000)}
              onChange={(val)=>handleSearchMessage(val.chatId , val.id)}
            />
  
          </div>
  
       
      </div>
    );
}


export default SearchMultipleChats;