import { IMApi } from "api/IMNewCycle";
import Helper from "components/Global/RComs/Helper";
import { MessageTags } from "views/Teacher/IM/constants/MessageTags";

export const INITIALIZE_IM = "im/initialize";
export const SET_SETTINGS = "im/setSettings";

export const GET_MESSSAGES = "messages/get";

//chats
export const SELECT_CHAT = "chats/select";

export const CHANGE_POINTER = "chats/changePointer";

export const PUSH_CONVERSATION = "chats/push";

//groups
export const SELECT_GROUP = "groups/select";

//messages
export const PUSH_MESSAGE = "messages/push";
export const CHANGE_MESSAGE_STATUS = "messages/changeStatus";
export const SET_SEARCHED_MESSAGE = "messages/search";
export const SET_CHUNK = "chunks/load";
export const TAG_MESSAGE = "messages/tag";
export const DELETE_MESSAGE = "message/delete";
export const UNSEND_MESSAGE = "messages/unsend";
export const SELECT_MULTIPLE_MESSAGES = "messages/selectMultiple";

export const UPDATE_GROUP = "UPDATE_GROUP";

export const SET_FORWARDED_MESSAGE = "messages/setForward";
export const SET_REPLIED_MESSAGE = "messages/setReplied";

export const TAG_CHAT = "chats/tag";
export const DELETE_CHAT = "chats/delete";
export const SELECT_MULTIPLE_CHATS = "chats/selectMultiple";

export const CHAT_SEEN = "chats/Read";
// export const FORWARD_MESSAGE = "messages/forwardMessage";

export const UPDATE_IMAGE_THUMBNAIL = "images/updateThumbnail";

export const MessageStatus = {
  ERROR: "ERROR",
  PENDING: "PENDING",
  SUCCESS: "SUCCESS",
  DELETED: "DELETED",
};

const messageIdsGenerator = (function* () {
  let i = 0;
  while (true) yield "msg" + ++i;
})();

const chunkIdsGenerator = (function* () {
  let i = 0;
  while (true) yield "chunk" + ++i;
})();

export const updateSettings = (name, value) => ({
  type: SET_SETTINGS,
  payload: {
    [name]: value,
  },
});

export const searchMessage =
  (msgId, chunkId, chatId, currentUserId) => async (dispatch) => {
    // the msg is already in the store
    if (chunkId) {
      return dispatch({
        type: SET_SEARCHED_MESSAGE,
        payload: {
          msgId,
          chunkId,
          chatId,
        },
      });
    }

    const response = await IMApi.loadMessagesAround(msgId);
    if (!response.data.status) throw new Error(response.data.msg);

    dispatch({
      type: SET_CHUNK,
      payload: {
        chatId: chatId,
        msgId: msgId,
        chunkId: chunkIdsGenerator.next().value,
        chunk: response.data.data.chanck.map((ch) => ({
          id: ch.id,
          message: ch.message,
          tags:
            ch.user_message
              ?.find?.((um) => um.user_id === currentUserId)
              ?.tags.map((t) => t.name) ?? [],
          user_id: ch.user_id,
          status: MessageStatus.SUCCESS,
          attachments: ch.resources,
          created_at: ch.created_at,
          updated_at: ch.updated_at,
          replyFor: {
            message: ch.src_messages?.[0]?.pivot?.brief,
            id: ch.src_messages?.[0]?.id,
          },
        })),
        start: response.data.data.chanck[0].id,
        end: response.data.data.chanck[response.data.data.chanck.length - 1].id,
        prev: response.data.data.next,
        next: response.data.data.previous,
      },
    });

    //if chunk is loaded
    //set message

    //if chunk is nt loaded
    //check if can be merged with any if true merge chunks then set that chunk to active
    //if cannot be merged add a new chunk and set it to active
  };

export const initializeConversations = (currentUserId) => async (dispatch) => {
  Helper.cl(IMApi, "imr 2.1");
  
  const response = await IMApi.getAllConversations();
  Helper.cl(response.data?.data?.models,"imr 2.2 response.data?.data?.models");
  if (!response.data.status) 
    return;
    //  throw new Error(response.data.msg);
  Helper.cl("imr 2.3");
  const payload = {
    chats: {
      byId: response.data?.data?.models?.reverse().reduce((prev, curr) => {
        const initialChunkId = chunkIdsGenerator.next().value;
        Helper.cl(initialChunkId, "imr2.3 initialChunkId");

        Helper.cl(curr.users_chat, "curr.users_chat");
        Helper.cl(
          curr.users_chat?.find?.((u_ch) => u_ch.id === currentUserId)?.pivot
            .is_admin,
          "curr.users_chat?.find?.(u_ch) => u_ch.id === currentUserId)?.is_admin"
        );
        Helper.cl(curr.name, "curr.curr.name");
        // Helper.cl(curr.name,"curr.curr.name")
        return {
          ...prev,
          [curr.id]: {
            id: curr.id,
            created_at: curr.created_at,
            isAdmin: curr.users_chat?.find?.(
              (u_ch) => u_ch.userId === currentUserId
            )?.pivot.is_admin,
            loaded: false,
            name: curr.name,
            description: curr.description,
            image: curr.image,
            unreadMessages:
            curr?.messages&&curr.messages?.length?
                 (
                    (curr.users_chat?.find?.
                      ((u_ch) => u_ch.userId === currentUserId)?.pivot?.pointer)
                            ==(curr?.messages[(curr?.messages?.length)-1]?.id
                            )
                     )
                            ?""//in case pointer= last message
                            :"+1"//in case pointer != last message
                            :"",// in case no messages at all
            activeChunk: `chat-${curr.id}-${initialChunkId}`,
            tags: curr.tags ?? [],
            entities: {
              users: {
                byId:
                  curr?.users_chat?.reduce((prev_us, curr_us) => {
                    //Helper.cl(curr_us.username,"curr_us.username,");
                    //Helper.cl( curr_us.full_name,"curr_us.full_name,");
                    //Helper.cl( curr_us.email1," curr_us.email1,");
                    //Helper.cl(curr_us.online,"curr_us.online");
                    //Helper.cl( curr_us?.image?.url,"curr_us?.image?.url");
                    //Helper.cl(curr_us.type,"curr_us.type");
                    //Helper.cl(curr_us.pointer,"curr_us.pointer");
                    //Helper.cl(curr_us.seen,"curr_us.seen");
                    return {
                      ...prev_us,
                      [curr_us.userId]: {
                        id: curr_us.id,
                        userId:curr_us.userId,
                        username: curr_us.full_name + "_" + curr_us.id,
                        name: curr_us.full_name,
                        email: curr_us.email,
                        online:curr_us.online,
                        image: curr_us?.image,
                        type: "teacher",
                        pointer: curr_us.pivot.pointer,
                        seen: curr_us.pivot.seen,
                      },
                    };
                  }, {}) ?? {},
                ids: curr?.users_chat?.map((chu) => chu.userId) ?? [],
              },
              messages: {
                byId:
                  // curr?.messages.map(currMessage=>
                  //       {[currMessage.id]:
                  //       {
                  //         id: currMessage.id,
                  //         message: currMessage.message,
                  //         tags:
                  //         currMessage.user_message
                  //             ?.find?.((um) => um.user_id === currentUserId)
                  //             ?.tags.map((t) => t.name) ?? [],
                  //         user_id: currMessage.user_id,
                  //         status: MessageStatus.SUCCESS,
                  //         attachments: currMessage.resources,
                  //         created_at: currMessage.created_at,
                  //         updated_at: currMessage.updated_at,
                  //         replyFor: {
                  //           message:
                  //           currMessage?.src_messages?.[0]?.pivot?.brief,
                  //           id: currMessage?.src_messages?.[0]?.id,
                  //         },
                  //         // replies: {
                  //         //     byId: curr.message?.reply_for?.reduce((prev, curr) => ({ ...prev, [curr.id]: { ...curr } }), {}) ?? {},
                  //         //     ids: curr.message?.reply_for?.map(i => i.id) ?? []
                  //         // }
                  //       }
                  //       }
                  //     )

                  curr?.messages.reduce((prev_msgs, currMessage) => {
                    return {
                      ...prev_msgs,
                      [currMessage.id]: {
                        id: currMessage.id,
                        message: currMessage.message,
                        tags:
                          currMessage.user_message
                            ?.find?.((um) => um.user_id === currentUserId)
                            ?.tags.map((t) => t.name) ?? [],
                        user_id: currMessage.user_id,
                        status: MessageStatus.SUCCESS,
                        attachments: currMessage.resources,
                        created_at: currMessage.created_at,
                        updated_at: currMessage.updated_at,
                        replyFor: {
                          message: currMessage?.src_messages?.[0]?.pivot?.brief,
                          id: currMessage?.src_messages?.[0]?.id,
                        },
                      },
                    };
                  }, {}) ?? {},

                ids: curr?.messages.map((m) => m?.id) ?? [],
              },
              chunks: {
                byId: {
                  [`chat-${curr.id}-${initialChunkId}`]: {
                    start: curr?.messages[curr?.messages.length-1]?.id,
                    end:  curr?.messages[curr?.messages.length-1]?.id,
                    nextPage: curr?.messages[curr?.messages.length-1]?.id?? -1,
                    prevPage: -1,
                  },
                },
                ids: [`chat-${curr.id}-${initialChunkId}`],
              },
            },
          },
        };
      }, {}),

      ids: response.data?.data?.models?.map((ch) => ch.id) ?? [],
    },
    unread:response.data?.other1,
    loggedUserId:currentUserId
  };
Helper.cl(currentUserId,'currentUserId');
  dispatch({ type: INITIALIZE_IM, payload });

  //fetch settings
  const response2 = await IMApi.getSettings();
  if (!response2.data.status) {return}
    //throw new Error(response2.data.msg);
  dispatch({
    type: SET_SETTINGS,
    payload: {
      archive_chats_times: response2.data?.data?.archive_chats_times ?? "10",
      enable_notifications: response2.data?.data?.enable_notifications ?? true,
      show_chat_folders: response2.data.data.show_chat_folders ?? true,
    },
  });
};

export const fetchMoreMessages =
  (id, nextPage = null, direction, currentUserId) =>
  async (dispatch) => {
    const response = await IMApi.fetchMoreMessages(
      id,
      nextPage,
      direction ? "previous" : "next"
    );
    if (!response.data?.status) throw new Error(response.data?.msg);

    const sortedMessageIds = response.data?.data?.models?.nextMessages
      ?.map((msg) => msg.id)
      .sort((a, b) => a - b);
    dispatch({
      type: GET_MESSSAGES,
      payload: {
        chatId: id,
        nextPage: direction ? response.data?.data?.models?.next : -1,
        prevPage: direction ? -1 : response.data?.data?.models?.next,
        direction: direction,
        start: sortedMessageIds[0],
        end: sortedMessageIds[sortedMessageIds.length - 1],
        messages: {
          byId:
            response.data?.data?.models?.nextMessages?.reduce(
              (prev, curr) => ({
                ...prev,
                [curr.id]: {
                  ...curr,
                  src_messages: undefined,
                  attachments: curr.resources,
                  // replies: {
                  //     byId: curr.reply_for?.reduce((prev, curr) => ({
                  //         ...prev,
                  //         [curr.id]: curr
                  //     }), {}) ?? {},
                  //     ids: curr.reply_for?.map(i => i.id) ?? []
                  // },
                  replyFor: {
                    message: curr?.src_messages?.[0]?.pivot?.brief,
                    id: curr?.src_messages?.[0]?.id,
                  },
                  user_message: undefined,
                  tags:
                    curr.user_message
                      ?.find?.((um) => um.user_id === currentUserId)
                      ?.tags.map((t) => t.name) ?? [],
                  status: MessageStatus.SUCCESS,
                },
              }),
              {}
            ) ?? {},
          ids: sortedMessageIds ?? [],
          // nextPage: response.data?.data?.models?.next
        },
      },
    });
  };

export const changePointer = (chatId) => async (dispatch) => {
  const response = await IMApi.changePointer(chatId);
  if (!response.data.status) 
  ;
  //throw new Error(response.data.msg);
 else
  dispatch({ type: CHANGE_POINTER, payload: chatId });
};
export const getAllReportCategories = (chatId) => async (dispatch) => {
  const response = await IMApi.getAllReportCategories(chatId);


};

export const saveReport = () => async (dispatch) => {
  const response = await IMApi.saveReport(chatId);
};
export const selectChat = (id) => ({
  type: SELECT_CHAT,
  payload: id,
});

export const pushChat_old = (chId) => async (dispatch) => {
  let response = await IMApi.getChat(chId);
  if (!response.data.status) throw new Error(response.data.msg);
  const initialChunkId = chunkIdsGenerator.next().value;
  const ch = response.data?.data?.models;
  Helper.cl(ch,"ch");
  const payload = {
    // [ch.id]: {
    id: ch.id,
    loaded: true,
    created_at: ch.created_at,
    activeChunk: `chat-${ch.id}-${initialChunkId}`,
    isAdmin: false,
    name: ch.name,
    image: ch.image,
    tags: [],
    shouldNotBeActive: true,
    // name: 'forward',
    entities: {
      users: {
        byId:
          ch?.users_chat?.reduce(
            (prev_us, curr_us) => ({
              ...prev_us,
              [curr_us.id]: {
                id: curr_us.id,
                username: curr_us.username,
                name: curr_us.full_name,
                email: curr_us.email1,
                online: curr_us.online,
                image: curr_us?.image?.url,
                type: curr_us.type,
                pointer: curr_us.pointer,
                seen: curr_us.seen,
              },
            }),
            {}
          ) ?? {},
        ids: ch?.users_chat?.map((chu) => chu.id) ?? [],
      },
      messages: {
        byId: {
          [ch?.messages?.[0].id]: {
            id: ch?.messages?.[0].id,
            message: ch?.messages?.[0].message,
            user_id: ch?.messages?.[0].user_id,
            attachments: ch?.messages?.[0].resources,
            status: MessageStatus.SUCCESS,
            created_at: ch?.messages?.[0].created_at,
            updated_at: ch?.messages?.[0].updated_at,
          },
        },
        ids: [ch?.messages?.[0]?.id],
      },
      chunks: {
        byId: {
          [`chat-${ch.id}-${initialChunkId}`]: {
            start: ch?.messages?.[0].id,
            end: ch?.messages?.[0].id,
            nextPage: ch?.messages?.[0]?.id,
            prevPage: -1,
          },
        },
        ids: [`chat-${ch.id}-${initialChunkId}`],
      },
    },

    // }
  };

  dispatch({ type: PUSH_CONVERSATION, payload });
};
export const pushChat = (chId) => async (dispatch) => {
  Helper.cl(chId,"pushChat chId");
  let response = await IMApi.getChat(chId);
  Helper.cl(chId,"pushChat chId11");
  if (!response.data.status) throw new Error(response.data.msg);
  Helper.cl(chId,"pushChat chId22");
  const initialChunkId = chunkIdsGenerator.next().value;
  Helper.cl(chId,"pushChat chId33");
  const ch = response.data?.data?.models;
Helper.cl(ch,"ch44");
  const payload = {
    // [ch.id]: {
    id: ch.id,
    loaded: true,
    created_at: ch.created_at,
    activeChunk: `chat-${ch.id}-${initialChunkId}`,
    isAdmin: false,
    name: ch.name,
    description: ch.description,
    image: ch.image,
    tags: [],
    shouldNotBeActive: true,
    // name: 'forward',
    entities: {
      users: {
        byId:
          ch?.users_chat?.reduce(
            (prev_us, curr_us) => ({
              ...prev_us,
              // [curr_us.id]: {
              //   id: curr_us.id,
              //   username: curr_us.full_name,
              //   name: curr_us.full_name,
              //   email: curr_us.email,
              //   online: false,
              //   image: curr_us?.image?.url,
              //   type: curr_us.type,
              //   pointer: curr_us?.pivot?.pointer,
              //   seen: curr_us.seen,
              // },
               [curr_us.userId]: {
                        id: curr_us.id,
                        userId:curr_us.userId,
                        username: curr_us.full_name + "_" + curr_us.id,
                        name: curr_us.full_name,
                        email: curr_us.email,
                        online: curr_us.online,
                        image: curr_us?.image,
                        type: "teacher",
                        pointer: curr_us.pivot.pointer,
                        seen: curr_us.pivot.seen,
                      },
            }),
            {}
          ) ?? {},
        ids: ch?.users_chat?.map((chu) => chu.userId) ?? [],
      },
       messages:{//{(ch?.messages?.[0]?.id)? {
        byId: {},////{
      //     [ch?.messages?.[0].id]: {
      //       id: ch?.messages?.[0].id,
      //       message: ch?.messages?.[0].message,
      //       user_id: ch?.messages?.[0].user_id,
      //       attachments: ch?.messages?.[0].resources,
      //       status: MessageStatus.SUCCESS,
      //       created_at: ch?.messages?.[0].created_at,
      //       updated_at: ch?.messages?.[0].updated_at,
      //     },
         //},
         ids: []},
      // }:null,
      chunks: 1?{//ch?.messages?.[0]?.id?{
        byId: {
          [`chat-${ch.id}-${initialChunkId}`]: {
            start: 0,//ch?.messages?.[0].id,
            end: 0,//ch?.messages?.[0].id,
            nextPage:0,// ch?.messages?.[0]?.id,
            prevPage: -0,//1,
          },
        },
        ids: [`chat-${ch.id}-${initialChunkId}`],
      }:null,
    },

    // }
  };
  Helper.cl(payload,"payload");
dispatch({ type: PUSH_CONVERSATION, payload });
};
export const forwardMessage =
  (message, contactOrChatId, loggedUserId, isExisting) => async (dispatch) => {
    if (isExisting) {
      const messageId = messageIdsGenerator.next().value;

      dispatch({
        type: PUSH_MESSAGE,
        payload: {
          chatId: contactOrChatId,
          message: {
            tags: [MessageTags.Forward],
            chat_id: contactOrChatId,
            created_at: new Date(),
            message: message.message,
            attachments: message.attachments,
            updated_at: new Date(),
            user_id: loggedUserId,
            id: messageId,
            status: MessageStatus.PENDING,
          },
        },
      });

      let response = await IMApi.forwardMessagetoExistingChat(
        contactOrChatId,
        message.id
      );

      if (!response.data.status) {
        dispatch({
          type: CHANGE_MESSAGE_STATUS,
          payload: {
            chatId: +response.data?.data?.data?.chat_id,
            id: messageId,
            // newId:null,
            status: MessageStatus.ERROR,
          },
        });
        throw new Error(response.data.msg);
      }

      dispatch({
        type: CHANGE_MESSAGE_STATUS,
        payload: {
          chatId: +response.data?.data?.data?.chat_id,
          id: messageId,
          attachments: response.data?.data?.data?.resources,
          newId: +response.data?.data?.data.id,
          status: MessageStatus.SUCCESS,
        },
      });
      return;
    }
    // forwardMessagetoContact: (contactId, msgId) =>    im_post(`api/chats/forward-messaege/${msgId}/user/${contactId}`),
    // forwardMessagetoExistingChat: (chatId, msgId) =>    im_post(`api/chats/forward-messaege/${msgId}/chat/${chatId}`),
                               
    let response = await IMApi.forwardMessagetoContact(
      contactOrChatId,
      message.id
    );
    if (!response.data.status) throw new Error(response.data.msg);

    const initialChunkId = chunkIdsGenerator.next().value;
    const ch = response.data?.data?.models;

    const payload = {
      // [ch.id]: {
      id: ch.id,
      loaded: true,
      created_at: ch.created_at,
      activeChunk: `chat-${ch.id}-${initialChunkId}`,
      isAdmin: true,
      tags: [],
      // name: 'forward',
      entities: {
        users: {
          byId:
            ch?.users_chat?.reduce(
              (prev_us, curr_us) => ({
                ...prev_us,
                [curr_us.id]: {
                  id: curr_us.id,
                  username: curr_us.username,
                  name: curr_us.full_name,
                  email: curr_us.email1,
                  online: curr_us.online,
                  image: curr_us?.image?.url,
                  type: curr_us.type,
                  pointer: curr_us.pointer,
                  seen: curr_us.seen,
                },
              }),
              {}
            ) ?? {},
          ids: ch?.users_chat?.map((chu) => chu.id) ?? [],
        },
        messages: {
          byId: {
            [ch?.messages?.[0].id]: {
              id: ch?.messages?.[0].id,
              message: ch?.messages?.[0].message,
              user_id: ch?.messages?.[0].user_id,
              attachments: ch?.messages?.[0].resources,
              status: MessageStatus.SUCCESS,
              created_at: ch?.messages?.[0].created_at,
              updated_at: ch?.messages?.[0].updated_at,
            },
          },
          ids: [ch?.messages?.[0]?.id],
        },
        chunks: {
          byId: {
            [`chat-${ch.id}-${initialChunkId}`]: {
              start: ch?.messages?.[0].id,
              end: ch?.messages?.[0].id,
              nextPage: ch?.messages?.[0]?.id,
              prevPage: -1,
            },
          },
          ids: [`chat-${ch.id}-${initialChunkId}`],
        },
      },

      // }
    };
    dispatch({ type: PUSH_CONVERSATION, payload: payload });
  };

export const pushMessage = (chatId, message) => ({
  type: PUSH_MESSAGE,
  payload: {
    chatId: chatId,
    message: message,
  },
});

export const messageUnsent = (chatId, msgId) => ({
  type: UNSEND_MESSAGE,
  payload: { chatId, msgId },
});

export const chatSeen = (chatId, userId, pointer) => ({
  type: CHAT_SEEN,
  payload: {
    chatId,
    pointer,
    userId,
  },
});

export const sendMessage = (chatId, message, replyFor) => async (dispatch) => {
  const tempId = "msg" + messageIdsGenerator.next().value;

  dispatch({
    type: PUSH_MESSAGE,
    payload: {
      chatId: chatId,
      message: {
        tags: [replyFor && MessageTags.Reply],
        chat_id: chatId,
        created_at: new Date(),
        message: message.content,
        attachments: message.attachments,
        updated_at: new Date(),
        user_id: message.senderId,
        id: tempId,
        status: MessageStatus.PENDING,
        replyFor: replyFor && { message: replyFor.message, id: replyFor.id },
      },
    },
  });

  try {
    const response = await (replyFor
      ? IMApi.replyToMessage(replyFor.id, {
          message: message.content,
          Attachments: message.attachments,
        })
      : IMApi.sendToChat(chatId, {
          message: message.content,
          Attachments: message.attachments,
        }));

    if (!response.data?.status) {
      throw new Error(response.data?.msg);
    }
    dispatch({
      type: CHANGE_MESSAGE_STATUS,
      payload: {
        chatId: chatId,
        id: tempId,
        newId: response.data?.data?.models.id,
        status: MessageStatus.SUCCESS,
        attachments: response.data?.data?.models.resources.map((rs) => ({
          ...rs,
          thumbUrl: rs.url,
        })),
      },
    });
  } catch (err) {
    dispatch({
      type: CHANGE_MESSAGE_STATUS,
      payload: {
        chatId: chatId,
        id: tempId,
        newId: null,
        status: MessageStatus.ERROR,
      },
    });
    throw err;
  }
};

// export const recieveMessage = data => dispatch => {

//     dispatch({ type: PUSH_MESSAGE, payload: data });
// }

// export const recieveConversation = data => dispatch => {
//     dispatch({ type: PUSH_CONVERSATION, payload: { conversation: { id: data.conversationId ?? data.groupId, name: data.name, numberOfUnreadMessages: 0, photoPath: "" }, participants: data.user2 ? [data.user2, data.user1] : []/* data.participants.map(id=>({id: id,username: "",}))*/ } });
// }

export const createChat =
  (recievers, message, currentUser, groupName, image) => async (dispatch) => {
    groupName = groupName ? { name: groupName, image: image } : {};

    const response = await IMApi.createConversation({
      users_id: recievers.map((rc) => rc.id),
      message: message.content,
      Attachments: message.attachments,
      ...groupName,
    });

    if (!response.data?.status) throw new Error(response.data?.msg);
    const ch = response.data?.data?.models;

    const initialChunkId = chunkIdsGenerator.next().value;

    const payload = {
      // [ch.id]: {
      loaded: true,
      id: ch.id,
      created_at: ch.created_at,
      activeChunk: `chat-${ch.id}-${initialChunkId}`,
      isAdmin: true,
      tags: [],
      // name: groupName,
      // image: image,
      ...groupName,
      entities: {
        users: {
          byId:
            ch?.users_chat?.reduce(
              (prev_us, curr_us) => ({
                ...prev_us,
                [curr_us.id]: {
                  id: curr_us.id,
                  username: curr_us.username,
                  name: curr_us.full_name,
                  email: curr_us.email1,
                  online: curr_us.online,
                  image: curr_us?.image?.url,
                  type: curr_us.type,
                  pointer: curr_us.pointer,
                  seen: curr_us.seen,
                },
              }),
              {}
            ) ?? {},
          ids: ch?.users_chat?.map((chu) => chu.id) ?? [],
        },
        messages: {
          byId: {
            [ch?.messages?.[0].id]: {
              id: ch?.messages?.[0].id,
              message: ch?.messages?.[0].message,
              attachments: ch?.messages?.[0].resources,
              user_id: ch?.messages?.[0].user_id,
              tags: [],
              status: MessageStatus.SUCCESS,
              created_at: ch?.messages?.[0].created_at,
              updated_at: ch?.messages?.[0].updated_at,
            },
          },
          ids: [ch?.messages?.[0]?.id],
        },
        chunks: {
          byId: {
            [`chat-${ch.id}-${initialChunkId}`]: {
              start: ch?.messages?.[0].id,
              end: ch?.messages?.[0].id,
              nextPage: ch?.messages?.[0]?.id,
              prevPage: -1,
            },
          },
          ids: [`chat-${ch.id}-${initialChunkId}`],
        },
      },

      // }
    };

    dispatch({ type: PUSH_CONVERSATION, payload });
  };

export const editGroup = (chatId, groupName, image,description) => async (dispatch) => {
  const groupName1 = { new_group_name: groupName, group_image: image ,description: description};
  const response = await IMApi.editGroup(chatId, { payload: groupName1 });
  if (!response.data.status) throw new Error(response.data.msg);
  dispatch({
    type: UPDATE_GROUP,
    payload: {
      chatId,
      groupName,
      image: response.data?.data?.updated_chat?.image,
      description:description
    },
  });
};

export const tagMessage = (chatId, messageId, payload) => async (dispatch) => {
  const response = await IMApi.tagMessage(chatId, messageId, {
    payload: payload,
  });
  if (!response.data.status) throw new Error(response.data.msg);
  dispatch({
    type: TAG_MESSAGE,
    payload: { chatId, messageId, tag: payload.tag },
  });
};

export const deleteMessage =
  (chatId, messageId, status) => async (dispatch) => {
    if (status !== MessageStatus.SUCCESS) {
      dispatch({ type: DELETE_MESSAGE, payload: { chatId, messageId } });
      return;
    }

    const response = await IMApi.deleteMessage(messageId);
    if (!response.data.status) throw new Error(response.data.msg);

    dispatch({ type: DELETE_MESSAGE, payload: { chatId, messageId } });
  };

export const unsendMessage = (chatId, msgId) => async (dispatch) => {
  const response = await IMApi.unsendMessage(msgId);
  if (!response?.data?.status) {
    throw new Error(response.data.msg);
  }
  dispatch({ type: UNSEND_MESSAGE, payload: { chatId, msgId } });
};

export const setForwardedMessage = (msgId) => ({
  type: SET_FORWARDED_MESSAGE,
  payload: msgId,
});

export const setRepliedOnMessageId = (msgId) => ({
  type: SET_REPLIED_MESSAGE,
  payload: msgId,
});

export const tagChat = (chatId, tag, tagged) => async (dispatch) => {
  const response = await IMApi.tagChat(chatId, {
    payload: { tag: tag, value: !tagged },
  });
  if (!response.data.status) throw new Error(response.data.msg);

  dispatch({ type: TAG_CHAT, payload: { chatId, tag } });
};

export const deleteChat = (chatId) => async (dispatch) => {
  const response = await IMApi.deleteChat(chatId);
  if (!response.data.status) throw new Error(response.data.msg);
  dispatch({ type: DELETE_CHAT, payload: chatId });
};

export const selectGroup = (groupTag) => ({
  type: SELECT_GROUP,
  payload: groupTag,
});

export const selectMultipleMessages = (messageId) => ({
  type: SELECT_MULTIPLE_MESSAGES,
  payload: messageId,
});

//chatId = null to reset
export const selectMultipleChats = (chatId) => ({
  type: SELECT_MULTIPLE_CHATS,
  payload: chatId,
});

export const updateImageThumbnail = (
  chatId,
  msgId,
  resourceId,
  newThumNail
) => ({
  type: UPDATE_IMAGE_THUMBNAIL,
  payload: { chatId, msgId, resourceId, newThumNail },
});
