import {
	GET_COURSE_CONTENT_REQUEST,
	GET_COURSE_CONTENT_SUCCESS,
	GET_COURSE_CONTENT_ERROR,
	GET_RAB_BOOKS_SUCCESS,
	GET_RAB_BOOKS_ERROR,
	GET_COURSE_CONTENTS_BY_COURSE_ID_SUCCESS,
	GET_COURSE_CONTENTS_BY_COURSE_ID_ERROR,
	GET_LESSON_ATTACHMENTS_REQUEST,
	GET_LESSON_ATTACHMENTS,
	GET_LESSON_ATTACHMENTS_ERROR,
	GET_LESSON_VC_RECORDINGS_REQUEST,
	GET_LESSON_VC_RECORDINGS,
	GET_LESSON_VC_RECORDINGS_ERROR,
	GET_LESSON_ASSIGNMENTS_REQUEST,
	GET_LESSON_ASSIGNMENTS,
	GET_LESSON_ASSIGNMENTS_ERROR,
	GET_LESSON_VC_SUPPLEMENT_RECORDINGS_REQUEST,
	GET_LESSON_VC_SUPPLEMENT_RECORDINGS,
	GET_LESSON_VC_SUPPLEMENT_RECORDINGS_ERROR,
	SHOW_LESSON_FROM_BOOK,
	GET_PERIOD_REQUEST,
	GET_PERIOD_SUCCESS,
	GET_PERIOD_ERROR,
	POST_ADD_LESSON_REQUEST,
	POST_ADD_LESSON_SUCCESS,
	POST_ADD_LESSON_ERROR,
	CHANGE_STATUS_LESSON_REQUEST,
	CHANGE_STATUS_LESSON_SUCCESS,
	CHANGE_STATUS_LESSON_ERROR,
	DELETE_LESSON_REQUEST,
	DELETE_LESSON_SUCCESS,
	DELETE_LESSON_ERROR,
	CONFIRM_DELETE_LESSON_REQUEST,
	CONFIRM_DELETE_LESSON_SUCCESS,
	CONFIRM_DELETE_LESSON_ERROR,
	SET_LESSON_ID,
	GET_SECTION_LESSONS_REQUEST,
	GET_SECTION_LESSONS_SUCCESS,
	GET_SECTION_LESSONS_ERROR,
	GET_SECTION_STUDENTS_REQUEST,
	GET_SECTION_STUDENTS_SUCCESS,
	GET_SECTION_STUDENTS_ERROR,
	REMOVE_LESSON_LESSON_REQUEST,
	REMOVE_LESSON_LESSON_SUCCESS,
	REMOVE_LESSON_LESSON_ERROR,
	REMOVE_BADGE_REQUEST,
	REMOVE_BADGE_SUCCESS,
	REMOVE_BADGE_ERROR,
	REFRESH_LISTER_BADGE,
	ADD_BADGE_REQUEST,
	ADD_BADGE_SUCCESS,
	ADD_BADGE_ERROR,
} from "./teacherTypes";
import { teacherApi } from "api/teacher";
import { studentCoursesAPis } from "api/student/courses";
import Helper from "components/Global/RComs/Helper";
import Swal, { DANGER, SUCCESS } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

export const addBadgeAsync = (data, handleClose, handleEmptyState) => async (dispatch) => {
	dispatch({ type: ADD_BADGE_REQUEST });
	try {
		const response = await teacherApi.addBadge(data);

		if (response.data.status === 1) {
			dispatch({
				type: ADD_BADGE_SUCCESS,
			});
			handleClose();
			handleEmptyState();
			dispatch(refreshListerBadgeAsync(true));
		} else {
			dispatch({ type: ADD_BADGE_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: ADD_BADGE_ERROR,
		});
		toast.error(error?.message);
	}
};

export const removeBadgeAsync = (badgeId, hideAlertDeleteBadge) => async (dispatch) => {
	dispatch({ type: REMOVE_BADGE_REQUEST });

	try {
		const response = await teacherApi.removeBadge(badgeId);

		if (response.data.status === 1) {
			dispatch({
				type: REMOVE_BADGE_SUCCESS,
			});
			hideAlertDeleteBadge();
			dispatch(refreshListerBadgeAsync(true));
		} else {
			dispatch({
				type: REMOVE_BADGE_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: REMOVE_BADGE_ERROR,
		});
		toast.error(error?.message);
	}
};

export const refreshListerBadgeAsync = (refreshListerBadge) => async (dispatch) => {
	dispatch({ type: REFRESH_LISTER_BADGE, payload: { refreshListerBadge } });
};

export const fetchCourseContent = (rabId) => async (dispatch) => {
	dispatch({ type: GET_COURSE_CONTENT_REQUEST });

	try {
		const response = await teacherApi.getCourseContent(rabId);

		if (response.data.status === 1) {
			const coursesData = response.data.data.models;

			dispatch({
				type: GET_COURSE_CONTENT_SUCCESS,
				payload: coursesData,
			});
		} else {
			dispatch({
				type: GET_COURSE_CONTENT_ERROR,
				payload: response.data.message,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_COURSE_CONTENT_ERROR,
		});
		toast.error(error?.message);
	}
};

export const deleteLessonAsync = (lessonId, RabId, hideAlert, handleOpenConfirmDeleteLessonModal) => async (dispatch) => {
	dispatch({ type: DELETE_LESSON_REQUEST });

	try {
		const response = await teacherApi.deleteLesson(lessonId);

		if (response.data.status === 1) {
			const data = response.data.data.data;

			dispatch({
				type: DELETE_LESSON_SUCCESS,
				payload: { data },
			});
			hideAlert();
			dispatch(fetchCourseContent(RabId));
		} else if (response.data.status === 0 && response.data.data) {
			const data = response.data.data.data;
			hideAlert();
			handleOpenConfirmDeleteLessonModal();
			dispatch({
				type: DELETE_LESSON_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: DELETE_LESSON_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: DELETE_LESSON_ERROR,
		});
		toast.error(error?.message);
	}
};

export const confirmDeleteLessonAsync = (lessonId, RabId, handleCloseConfirmDeleteLessonModal) => async (dispatch) => {
	dispatch({ type: CONFIRM_DELETE_LESSON_REQUEST });

	try {
		const response = await teacherApi.confirmDeleteLesson(lessonId);

		if (response.data.status === 1) {
			dispatch({
				type: CONFIRM_DELETE_LESSON_SUCCESS,
			});
			handleCloseConfirmDeleteLessonModal();
			dispatch(fetchCourseContent(RabId));
		} else {
			dispatch({ type: CONFIRM_DELETE_LESSON_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: CONFIRM_DELETE_LESSON_ERROR,
		});
		toast.error(error?.message);
	}
};

export const setLessonIdAsync = (lessonId) => async (dispatch) => {
	dispatch({
		type: SET_LESSON_ID,
		payload: { lessonId },
	});
};

export const ShowLessonFromBook = (LessonId, BookId) => async (dispatch) => {
	dispatch({
		type: SHOW_LESSON_FROM_BOOK,
		payload: { LessonId, BookId },
	});
};

export const getPeriodsAsync = () => async (dispatch) => {
	dispatch({ type: GET_PERIOD_REQUEST });

	try {
		const response = await teacherApi.getPeriods();

		if (response.data.status === 1) {
			const periodData = response.data.data.periods;
			dispatch({
				type: GET_PERIOD_SUCCESS,
				payload: periodData,
			});
		} else {
			dispatch({ type: GET_PERIOD_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_PERIOD_ERROR,
		});
		toast.error(error?.message);
	}
};

export const fetchRabBooks = (rabId) => async (dispatch) => {
	try {
		const response = await teacherApi.getRabBooks(rabId);
		if (response.data.status === 1) {
			const rabBooks = response.data.data;
			dispatch({
				type: GET_RAB_BOOKS_SUCCESS,

				payload: rabBooks,
			});
		}
		dispatch({
			type: GET_RAB_BOOKS_ERROR,
			payload: response.data.message,
		});
	} catch (error) {
		dispatch({
			type: GET_RAB_BOOKS_ERROR,
			payload: error.response?.data?.message || "something went wrong",
		});
	}
};

export const getLessonAssignments = (lessonID) => {
	return async (dispatch) => {
		dispatch({ type: GET_LESSON_ASSIGNMENTS_REQUEST });
		try {
			const response = await studentCoursesAPis.fetchLessonAssignments(lessonID);
			var lessonAssignments = response.data.data.objList;
			dispatch({
				type: GET_LESSON_ASSIGNMENTS,
				error: null,
				lessonAssignments,
			});
		} catch (error) {
			dispatch({
				type: GET_LESSON_ASSIGNMENTS_ERROR,
				error: error.message,
			});
		}
	};
};

export const getLessonVcRecordings = (lessonID) => {
	return async (dispatch) => {
		dispatch({ type: GET_LESSON_VC_RECORDINGS_REQUEST });
		try {
			const response = await studentCoursesAPis.fetchLessonVcRecordings(lessonID);
			var lessonVcRecordings = response.data.data.data;

			dispatch({
				type: GET_LESSON_VC_RECORDINGS,
				error: null,
				lessonVcRecordings,
			});
		} catch (error) {
			dispatch({
				type: GET_LESSON_VC_RECORDINGS_ERROR,
				error: error.message,
			});
		}
	};
};

export const getLessonVcSupplementRecordings = (lessonID) => {
	return async (dispatch) => {
		dispatch({ type: GET_LESSON_VC_SUPPLEMENT_RECORDINGS_REQUEST });

		try {
			const response = await studentCoursesAPis.fetchLessonVcSupplementRecordings(lessonID);
			var lessonVcSupplementRecordings = response.data.data.data;
			dispatch({
				type: GET_LESSON_VC_SUPPLEMENT_RECORDINGS,
				error: null,
				lessonVcSupplementRecordings,
			});
		} catch (error) {
			dispatch({
				type: GET_LESSON_VC_SUPPLEMENT_RECORDINGS_ERROR,
				error: error.message,
			});
		}
	};
};

export const getLessonAttachments = (lessonID) => {
	return async (dispatch) => {
		dispatch({ type: GET_LESSON_ATTACHMENTS_REQUEST });

		try {
			const response = await studentCoursesAPis.fetchLessonAttachments(lessonID);
			var lessonAttachments = response.data.data.data;
			dispatch({
				type: GET_LESSON_ATTACHMENTS,
				error: null,
				lessonAttachments,
			});
		} catch (error) {
			dispatch({
				type: GET_LESSON_ATTACHMENTS_ERROR,
				error: error.message,
			});
		}
	};
};

export const saveLessonCourse = (LessonId) => {
	return (dispatch) => {
		dispatch({
			type: "SAVE_LESSON_COURSE_RAB",
			LessonId,
		});
	};
};

export const getStudentCourseContentsByCourseId = (courseId) => {
	return async (dispatch) => {
		dispatch({ type: "SHOW_LOADING" });

		try {
			const response = await studentCoursesAPis.fetchCourseContentsByCourseId(courseId);
			let courseContentsData = response.data;
			dispatch({
				type: "GET_COURSE_CONTENTS_BY_COURSE_ID_SUCCESS",
				error: null,
				courseContentsData,
			});
		} catch (error) {
			dispatch({
				type: "GET_COURSE_CONTENTS_BY_COURSE_ID_ERROR",
				error: error.message,
			});
		}
	};
};

export const viewBookRab = (rabId) => async (dispatch) => {
	try {
		const response = await studentCoursesAPis.fetchCourseContentsByCourseId(rabId);
		if (response.data.status == 1) {
			let ArrayBook = response.data;
			dispatch({
				type: "GET_BOOK_RAB",
				ArrayBook,
			});

			return Promise.resolve(ArrayBook);
		} else {
			dispatch({
				type: "GET_BOOK_RAB_ERROR",
				actionError: response.data.msg,
			});
		}
	} catch (error) {
		dispatch({
			type: "GET_BOOK_RAB_ERROR",
			//   error: error.message,
		});
	}
};
export const getCourseContentsByCourseId = (course_id) => {
	return (dispatch) => {
		dispatch({ type: "SHOW_LOADING" });
		teacherApi
			.getCoursesContent(course_id)
			.then((response) => {
				dispatch({
					type: GET_COURSE_CONTENTS_BY_COURSE_ID_SUCCESS,
					error: null,
					payload: response.data.data,
				});
			})
			.catch((error) => {
				dispatch({
					type: GET_COURSE_CONTENTS_BY_COURSE_ID_ERROR,
					error: error.message,
				});
			});
	};
};
