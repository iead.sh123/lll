import { teacherApi } from "api/teacher";
import Swal, { DANGER } from "utils/Alert";
import {
	TEACHER_VIEW_EVENT_CALENDER_REQUEST,
	TEACHER_VIEW_EVENT_CALENDER_SUCCESS,
	TEACHER_VIEW_EVENT_CALENDER_ERROR,
} from "./teacherTypes";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

export const teacherViewEventCalendar = () => async (dispatch) => {
	dispatch({ type: TEACHER_VIEW_EVENT_CALENDER_REQUEST });

	try {
		const response = await teacherApi.getTeacherCalendarEvents();

		if (response.data.status === 1) {
			const periods = response.data.data.periods;
			const weeks = response.data.data.weeks;
			let output = [];
			Object.values(weeks).forEach((week) => {
				Object.values(week).forEach((days) => {
					Object.values(days).forEach((event) => {
						event.lessons.map((lesson) => {
							let selectedPeriod = periods[lesson.period - 1];
							if (lesson.title1 || lesson.title) {
								output.push({
									title: lesson.title1,
									start: new Date(lesson.date + " " + selectedPeriod.start_time),
									end: new Date(lesson.date + " " + selectedPeriod.end_time),
									allDay: false,
									color: lesson.color,
								});
							}
						});
					});
				});
			});
			dispatch({
				type: TEACHER_VIEW_EVENT_CALENDER_SUCCESS,
				payload: { authError: null, events: output, period: periods },
			});
		} else {
			dispatch({ type: TEACHER_VIEW_EVENT_CALENDER_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: TEACHER_VIEW_EVENT_CALENDER_REQUEST,
		});
		toast.error(error?.message);
	}
};
