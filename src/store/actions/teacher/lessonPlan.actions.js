import { lessonPlanApi } from "api/teacher/lessonPlan";
import Swal, { SUCCESS, DANGER } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { handleError } from "utils/HandleError";
import {
	GET_LESSON_LESSONPLAN_REQUEST,
	GET_LESSON_LESSONPLAN_SUCCESS,
	GET_LESSON_LESSONPLAN_ERROR,
	GET_LESSON_PLAN_PRINTING_REQUEST,
	GET_LESSON_PLAN_PRINTING_SUCCESS,
	GET_LESSON_PLAN_PRINTING_ERROR,
	ADD_LESSON_LESSONPLAN_REQUEST,
	ADD_LESSON_LESSONPLAN_SUCCESS,
	ADD_LESSON_LESSONPLAN_ERROR,
	ADD_DATA_TO_LESSON_PLAN_ITEMS,
	ADD_NEW_LESSON_PLAN_ITEMS,
	ADD_VALIDATION_TO_LESSON_PLAN,
	SET_LESSON_PLAN_VALUE,
	REMOVE_LESSON_PLAN_ITEM,
	CLEAR_LESSON_PLAN_ITEM,
	GET_VALIDATION_LESSON_PLAN,
	PICK_A_RUBRIC_TO_LESSON_PLAN_ITEM,
	DELETE_RUBRIC_FROM_LESSON_PLAN_ITEM,
	IMPORT_LESSON_PLAN_TO_LESSON_CONTENT_REQUEST,
	IMPORT_LESSON_PLAN_TO_LESSON_CONTENT_SUCCESS,
	IMPORT_LESSON_PLAN_TO_LESSON_CONTENT_ERROR,
	GET_LESSON_PLAN_BY_LESSON_PLAN_ID_REQUEST,
	GET_LESSON_PLAN_BY_LESSON_PLAN_ID_SUCCESS,
	GET_LESSON_PLAN_BY_LESSON_PLAN_ID_ERROR,
	EXPORT_LESSON_PLAN_AS_PDF_REQUEST,
	EXPORT_LESSON_PLAN_AS_PDF_SUCCESS,
	EXPORT_LESSON_PLAN_AS_PDF_ERROR,
} from "./teacherTypes";
import { handleShouldNotBeAddedLessonContent, addNewLessonContentToModuleContent } from "../global/coursesManager.action";
import ConvertBinaryToPdf from "utils/convertBinaryToPdf";
import { toast } from "react-toastify";

//---------------------------------------------Lesson Plan Service---------------------------------------------

export const createLessonPlanStore = () => {
	const guid = Math.floor(1000 + Math.random() * 9000);
	const lessonPlan = {
		idTemp: guid,
		name: "",
		lesson_nb: "",
		unit_nb: "",
		tags: [],
		lesson_plan_items: [],
		published: false,
	};
	return (dispatch) => {
		dispatch({
			type: GET_LESSON_LESSONPLAN_SUCCESS,
			lessonPlan: lessonPlan,
		});
	};
};

export const getLessonPlans = (lessonId) => async (dispatch) => {
	dispatch({ type: GET_LESSON_LESSONPLAN_REQUEST });
	try {
		const response = await lessonPlanApi.getLessonPlan(lessonId);

		if (response.data.status === 1) {
			let data = response.data.data.data;

			if (data != null) {
				dispatch({
					type: GET_LESSON_LESSONPLAN_SUCCESS,
					lessonPlan: data,
					tags: [],
				});
			} else {
				dispatch(createLessonPlanStore());
			}
		} else {
			dispatch({
				type: GET_LESSON_LESSONPLAN_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_LESSON_LESSONPLAN_ERROR,
		});
		toast.error(error?.message);
	}
};

export const getBase64ToLessonPlan = (lessonPlanId) => async (dispatch) => {
	dispatch({ type: GET_LESSON_PLAN_PRINTING_REQUEST });
	try {
		const response = await lessonPlanApi.getLessonPlanPrinting(lessonPlanId);

		if (response.data.status == 1) {
			const urlBase64 = response.data.data.base_64;
			dispatch({
				type: GET_LESSON_PLAN_PRINTING_SUCCESS,
				payload: { urlBase64 },
			});
		} else {
			dispatch({
				type: GET_LESSON_PLAN_PRINTING_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_LESSON_PLAN_PRINTING_ERROR,
		});
		toast.error(error?.message);
	}
};

export const changeLessonPlanValue = (key, value) => {
	return (dispatch) => {
		dispatch({
			type: SET_LESSON_PLAN_VALUE,
			key: key,
			value: value,
		});
	};
};

export const getValidationLessonPlan = () => {
	return (dispatch) => {
		const initialValidationData = {
			name: "",
			lesson_nb: null,
			unit_nb: null,
			lesson_plan_items: [],
		};
		dispatch({
			type: GET_VALIDATION_LESSON_PLAN,
			payload: { initialValidationData },
		});
	};
};

export const addValidationToLessonPlan = (name, id, message) => {
	return (dispatch) => {
		dispatch({
			type: ADD_VALIDATION_TO_LESSON_PLAN,
			payload: { name, id, message },
		});
	};
};

export const addDataToLessonPlanItem = (value, name, id) => {
	return (dispatch) => {
		dispatch({
			type: ADD_DATA_TO_LESSON_PLAN_ITEMS,
			payload: { value, name, id },
		});
	};
};

export const addNewLessonPlanItems = (indexAdd) => {
	return (dispatch) => {
		dispatch({
			type: ADD_NEW_LESSON_PLAN_ITEMS,
			payload: { indexAdd },
		});
	};
};

export const removeLessonPlanItem = (id) => {
	return (dispatch) => {
		dispatch({
			type: REMOVE_LESSON_PLAN_ITEM,
			payload: { id },
		});
	};
};

export const clearLessonPlanItem = (id) => {
	return (dispatch) => {
		dispatch({
			type: CLEAR_LESSON_PLAN_ITEM,
			payload: { id },
		});
	};
};

export const addLessonPlanAsync = (publishLessonPlan, lessonPlanId, history) => async (dispatch) => {
	dispatch({ type: ADD_LESSON_LESSONPLAN_REQUEST });
	try {
		const response = await lessonPlanApi.storeLessonLessonPlan(publishLessonPlan);

		if (response.data.status === 1) {
			dispatch({
				type: ADD_LESSON_LESSONPLAN_SUCCESS,
			});

			history.goBack();
			// dispatch(getLessonPlans(lessonId));
		} else {
			dispatch({
				type: ADD_LESSON_LESSONPLAN_ERROR,
				payload: response.data.message,
			});
			handleError(response.data.msg);
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: ADD_LESSON_LESSONPLAN_ERROR,
		});
		toast.error(error?.message);
	}
};

export const pickARubricToLessonPlanItem = (data, lessonPlanItemId) => {
	return (dispatch) => {
		dispatch({
			type: PICK_A_RUBRIC_TO_LESSON_PLAN_ITEM,
			payload: { data, lessonPlanItemId },
		});
	};
};

export const deleteRubricFromLessonPlanItem = (lessonPlanItemId) => {
	return (dispatch) => {
		dispatch({
			type: DELETE_RUBRIC_FROM_LESSON_PLAN_ITEM,
			payload: { lessonPlanItemId },
		});
	};
};

export const importLessonPlanToLessonContent =
	(dataSending, courseModule, moduleContent, handleCloseLessonPlanModal, emptyState) => async (dispatch) => {
		dispatch({ type: IMPORT_LESSON_PLAN_TO_LESSON_CONTENT_REQUEST });
		try {
			const response = await lessonPlanApi.importLessonPlan(dataSending);

			if (response.data.status == 1) {
				const data = response.data.data;
				dispatch({
					type: IMPORT_LESSON_PLAN_TO_LESSON_CONTENT_SUCCESS,
				});

				dispatch(handleShouldNotBeAddedLessonContent(false));
				dispatch(addNewLessonContentToModuleContent(courseModule, moduleContent, data, "lesson_plan"));
				handleCloseLessonPlanModal && handleCloseLessonPlanModal();
				emptyState && emptyState();
			} else {
				dispatch({
					type: IMPORT_LESSON_PLAN_TO_LESSON_CONTENT_ERROR,
				});
				toast.error(response.data.msg);
			}
		} catch (error) {
			dispatch({
				type: IMPORT_LESSON_PLAN_TO_LESSON_CONTENT_ERROR,
			});
			toast.error(error?.message);
		}
	};

export const getLessonPlanByLessonPlanId = (lessonPlanId) => async (dispatch) => {
	dispatch({ type: GET_LESSON_PLAN_BY_LESSON_PLAN_ID_REQUEST });
	try {
		const response = await lessonPlanApi.lessonPlanByLessonPlanId(lessonPlanId);

		if (response.data.status === 1) {
			let data = response.data.data;

			dispatch({
				type: GET_LESSON_PLAN_BY_LESSON_PLAN_ID_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: GET_LESSON_PLAN_BY_LESSON_PLAN_ID_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_LESSON_PLAN_BY_LESSON_PLAN_ID_ERROR,
		});
		toast.error(error?.message);
	}
};

export const exportLessonPlanAsPdf = (lessonPlanId) => async (dispatch) => {
	dispatch({ type: EXPORT_LESSON_PLAN_AS_PDF_REQUEST });
	try {
		const response = await lessonPlanApi.exportLessonPlanAsPdf(lessonPlanId);

		if (response.data.code !== 500) {
			let data = response.data;

			ConvertBinaryToPdf(data);
			dispatch({
				type: EXPORT_LESSON_PLAN_AS_PDF_SUCCESS,
			});
		} else {
			dispatch({
				type: EXPORT_LESSON_PLAN_AS_PDF_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: EXPORT_LESSON_PLAN_AS_PDF_ERROR,
		});
		toast.error(error?.message);
	}
};
