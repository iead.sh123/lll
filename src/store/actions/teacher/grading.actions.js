import { teacherApi } from "api/teacher";
import Swal, { DANGER } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { TEACHER_VIEW_GRADING_BOOK, SET_GRADING_BOOK, VIEW_RAB_EVALUATION } from "./teacherTypes";

export const teacherViewGradingBook = (rad_id) => {
	return (dispatch) => {
		dispatch({ type: "SHOW_LOADING" });
		teacherApi
			.viewGradingBook(rad_id)
			.then((response) => {
				if (response.data.status === 1) {
					dispatch({
						type: TEACHER_VIEW_GRADING_BOOK,
						authError: null,
						tmp: response.data.data,
					});
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const teacherSetGradingBook = (gradingBook) => {
	return (dispatch) => {
		dispatch({
			type: SET_GRADING_BOOK,
			gradingBook,
		});
	};
};

export const RabEvaluation = (rab_id) => {
	return (dispatch) => {
		teacherApi
			.getRabEvaluation(rab_id)
			.then((response) => {
				if (response.data.status === 1) {
					dispatch({
						type: VIEW_RAB_EVALUATION,
						authError: null,
						rabEvaluationData: response.data.data,
					});
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};
