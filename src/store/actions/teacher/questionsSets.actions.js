import { questionsSetApi } from "api/teacher/questionsSet";
import { toast } from "react-toastify";
import {
	GET_PATTERN_REQUEST,
	GET_PATTERN_SUCCESS,
	GET_PATTERN_ERROR,
	GET_STUDENT_DETAILS_REQUEST,
	GET_STUDENT_DETAILS_SUCCESS,
	GET_STUDENT_DETAILS_ERROR,
	POST_STUDENT_DETAILS_REQUEST,
	POST_STUDENT_DETAILS_SUCCESS,
	POST_STUDENT_DETAILS_ERROR,
	UPDATE_STUDENTS_DETAIL_FIELDS,
	SET_STUDENT_ID,
	SET_QUESTION_SET_ID,
	GET_QUESTION_SETS_REQUEST,
	GET_QUESTION_SETS_SUCCESS,
	GET_QUESTION_SETS_ERROR,
	DELETE_QUESTION_SET_REQUEST,
	DELETE_QUESTION_SET_SUCCESS,
	DELETE_QUESTION_SET_ERROR,
	GET_SELECTED_PATTERN_INFO,
	FILL_QS_INFO,
	GET_SINGLE_COURSE_QS_REQUEST,
	GET_SINGLE_COURSE_QS_SUCCESS,
	GET_SINGLE_COURSE_QS_ERROR,
	GET_ALL_COURSES_REQUEST,
	GET_ALL_COURSES_SUCCESS,
	GET_ALL_COURSES_ERROR,
	GET_ALLOWED_APPS_REQUEST,
	GET_ALLOWED_APPS_SUCCESS,
	GET_ALLOWED_APPS_ERROR,
	EDIT_SETTINGS,
	UPDATE_GROUP_NAME,
	DELETE_TOPIC_FROM_GROUP,
	QS_PATTERN_ADD_TOPIC_TO_GROUP,
	QS_PATTERN_SELECT_SPECIFIC_GROUP,
	QS_PATTERN_Delete_GROUP,
	QS_PATTERN_ADD_GROUP,
	GET_TOPICS_BY_COURSE_ID_SUCCESS,
	GET_TOPICS_BY_COURSE_ID_ERROR,
	QS_DELETE_PATTERN,
	CLEAR_QS_QLLOWED_APPS,
	GET_SELECTED_PATTERN_ID,
	GET_SELECTED_GROUP_ID,
	GET_SELECTED_TOPIC,
	NOT_SAVE_EDIT,
	ADD_NEW_PATTERN,
	UPDATE_QS_VALUE,
	CREATE_COPY_QS_VALUE,
	UPDATE_COURSES_VALUE,
	CLEAR_QS_COURSES,
	SAVE_QS_REQUEST,
	SAVE_QS_ERROR,
	CREATE_QUESTION_SETS_COPY_REQUEST,
	CREATE_QUESTION_SETS_COPY_ERROR,
	CALC_TOTAL_GRADE_GROUP,
	UPDATE_GROUP_GRADE_VALUE,
	UPDATE_TOPICS_GRADE_VALUE,
	CHANGE_PATTERN_NAME,
	GET_SELECTED_PATTERN_TOTAL_GRADE,
	EMPTY_STORE_QS,
	UPDATE_STUDENTS_MARK_FIELDS,
} from "./teacherTypes";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";

export const dispatchFetchQuestionsSets = (courseId) => {
	return async (dispatch) => {
		dispatch({ type: GET_QUESTION_SETS_REQUEST });
		await questionsSetApi
			.getCourseQsInfo(courseId)
			.then((response) => {
				if (response.data.status === 1) {
					dispatch({
						type: GET_QUESTION_SETS_SUCCESS,
						authError: null,
						payload: response.data.data,
					});
				} else {
					toast.error(response.data.msg);
					dispatch({
						type: GET_QUESTION_SETS_ERROR,
						payload: response.data.msg,
					});
				}
			})
			.catch((error) => {
				toast.error(error?.message);
				dispatch({ type: GET_QUESTION_SETS_ERROR, payload: error.message });
			});
	};
};

export const deleteQuestionSet = (qs_id, course_id, index) => {
	return async (dispatch) => {
		dispatch({
			type: DELETE_QUESTION_SET_REQUEST,
		});
		await questionsSetApi
			.deleteQuestionS(qs_id, course_id)
			.then((response) => {
				if (response.data.msg == null) {
					dispatch({
						type: DELETE_QUESTION_SET_SUCCESS,
						qs_id: index,
					});
				}
				if (response.data.msg == "qslinked") {
					toast.error(`QuestionSet is Linked!`);

					dispatch({
						type: DELETE_QUESTION_SET_ERROR,
						payload: "QuestionSet is Linked!",
					});
				}
				if (response.data.code === 417) {
					dispatch({
						type: DELETE_QUESTION_SET_ERROR,
						payload: response.data.msg,
					});
					toast.error(response.data.msg);
				}
			})
			.catch((error) => {
				dispatch({ type: DELETE_QUESTION_SET_ERROR, payload: error.message });
				toast.error(error?.message);
			});
	};
};

export const qsPagination = (urlPage) => {
	return async (dispatch) => {
		await questionsSetApi
			.pagenation(urlPage)
			.then((response) => {
				if (response.data.status === 1) {
					dispatch({
						type: GET_QUESTION_SETS_SUCCESS,
						authError: null,
						payload: response.data.data,
					});
				} else {
					toast.error(response.data.msg);
					dispatch({
						type: GET_QUESTION_SETS_ERROR,
						payload: response.data.msg,
					});
				}
			})
			.catch((error) => {
				toast.error(error?.message);
				dispatch({ type: GET_QUESTION_SETS_ERROR, payload: error.message });
			});
	};
};

export const getSingleQsInfosById = (course_id, qs_id) => {
	if (qs_id) {
		return async (dispatch) => {
			dispatch({ type: GET_SINGLE_COURSE_QS_REQUEST });
			await questionsSetApi
				.getCourseSingleQs(course_id, qs_id)
				.then((response) => {
					if (response.data.status === 1) {
						dispatch({
							type: GET_SINGLE_COURSE_QS_SUCCESS,
							payload: response.data.data,
						});
					}
					dispatch({
						type: FILL_QS_INFO,
						qs_id,
					});
					let selectedPatternId = null;
					dispatch({
						type: GET_SELECTED_PATTERN_INFO,
						qs_id,
						selectedPatternId,
					});
				})
				.catch((error) => {
					dispatch({
						type: GET_SINGLE_COURSE_QS_ERROR,
						payload: error.message,
					});
					toast.error(error?.message);
				});
		};
	} else {
		const guid = Math.floor(1000 + Math.random() * 9000);
		const questionSet = {
			idTemp: guid,
			name: "",
			question_set_type: "select_question_set_type",
			total_grade: 0,
			qs_patterns: [],
			allowed_apps: [],
			courses: [
				{
					pivot: {
						course_content_id: null,
						page_number: null,
					},
				},
			],
		};
		return (dispatch) => {
			dispatch({
				type: CREATE_COPY_QS_VALUE,
				questionSet,
			});
		};
	}
};

export const createNewCopy = (course_id, qs_id, RabId) => {
	return async (dispatch) => {
		dispatch({
			type: CREATE_QUESTION_SETS_COPY_REQUEST,
		});
		await questionsSetApi
			.createCopy(qs_id, course_id)
			.then((response) => {
				if (response.data.status === 1) {
					const qsID = response.data.data;
					window.open(`/teacher/courses/${course_id}/${RabId}/QuestionSets-Editor/${qsID}`, "_self");
				} else {
					toast.error(response.data.msg);
					dispatch({
						type: CREATE_QUESTION_SETS_COPY_ERROR,
						payload: response.data.msg,
					});
				}
			})
			.catch((error) => {
				toast.error(error?.message);
				dispatch({
					type: CREATE_QUESTION_SETS_COPY_ERROR,
					payload: error.message,
				});
			});
	};
};

export const saveQuestionSet = (QsStore, courseId, RabId) => {
	return async (dispatch) => {
		dispatch({ type: SAVE_QS_REQUEST });
		await questionsSetApi
			.SaveQs(QsStore)
			.then((response) => {
				if (response.data.status == 1) {
					dispatch({ type: EMPTY_STORE_QS });
					history.back();
				} else {
					toast.error(response.data.msg);
					dispatch({ type: SAVE_QS_ERROR, payload: response.data.msg });
				}
			})
			.catch((error) => {
				dispatch({
					type: SAVE_QS_ERROR,
					payload: error.message,
				});
				toast.error(error?.message);
			});
	};
};

export const clearQsAllowedApps = () => {
	return (dispatch) => {
		dispatch({
			type: CLEAR_QS_QLLOWED_APPS,
		});
	};
};

export const clearQsCourses = () => {
	return (dispatch) => {
		dispatch({
			type: CLEAR_QS_COURSES,
		});
	};
};

export const getAllCourses = () => {
	return async (dispatch) => {
		dispatch({ type: GET_ALL_COURSES_REQUEST });
		await questionsSetApi
			.getCourses()
			.then((response) => {
				if (response.data.status === 1) {
					dispatch({ type: GET_ALL_COURSES_SUCCESS, payload: response.data });
				}
				dispatch({ type: GET_ALL_COURSES_ERROR, payload: response.data.msg });
			})
			.catch((error) => {
				dispatch({ type: GET_ALL_COURSES_ERROR, payload: error.message });
			});
	};
};

export const getAllowedApps = () => {
	return async (dispatch) => {
		dispatch({ type: GET_ALLOWED_APPS_REQUEST });
		await questionsSetApi
			.getallowedApp()
			.then((response) => {
				if (response.data.status == 1) {
					let data = response.data.data;
					dispatch({
						type: GET_ALLOWED_APPS_SUCCESS,
						data,
					});
				} else {
					dispatch({
						type: GET_ALLOWED_APPS_ERROR,
						actionError: response.data.msg,
					});
				}
			})
			.catch((error) => {
				dispatch({
					type: GET_ALLOWED_APPS_ERROR,
					actionError: error.message,
				});
			});
	};
};

export const editSettings = (app, checked) => {
	return (dispatch) => {
		dispatch({
			type: EDIT_SETTINGS,
			app,
			checked,
		});
		dispatch({
			type: NOT_SAVE_EDIT,
		});
	};
};

export const addGroup = (group, patternId) => {
	return (dispatch) => {
		dispatch({
			type: QS_PATTERN_ADD_GROUP,
			group,
			patternId,
		});

		dispatch({
			type: NOT_SAVE_EDIT,
		});
	};
};

export const deleteGroup = (index, groupId) => {
	return (dispatch) => {
		dispatch({
			type: QS_PATTERN_Delete_GROUP,
			index,
			groupId,
		});
		dispatch({
			type: GET_SELECTED_PATTERN_TOTAL_GRADE,
		});
		dispatch({
			type: NOT_SAVE_EDIT,
		});
	};
};

export const selectSpecificGroup = (groupId) => {
	return (dispatch) => {
		dispatch({
			type: QS_PATTERN_SELECT_SPECIFIC_GROUP,
			groupId,
		});
	};
};

export const deleteTopicFromGroup = (topicId, index) => {
	return (dispatch) => {
		dispatch({
			type: DELETE_TOPIC_FROM_GROUP,
			topicId,
			index,
		});
		dispatch({
			type: CALC_TOTAL_GRADE_GROUP,
		});
		dispatch({
			type: GET_SELECTED_PATTERN_TOTAL_GRADE,
		});
		dispatch({
			type: NOT_SAVE_EDIT,
		});
	};
};

export const updateGroup = (group) => {
	return (dispatch) => {
		dispatch({
			type: UPDATE_GROUP_NAME,
			group,
		});
		dispatch({
			type: NOT_SAVE_EDIT,
		});
	};
};

export const addTopicToGroup = (topic) => {
	return (dispatch) => {
		dispatch({
			type: QS_PATTERN_ADD_TOPIC_TO_GROUP,
			topic,
		});
		dispatch({
			type: CALC_TOTAL_GRADE_GROUP,
		});
		dispatch({
			type: GET_SELECTED_PATTERN_TOTAL_GRADE,
		});
		dispatch({
			type: NOT_SAVE_EDIT,
		});
	};
};

export const getTopicsByCourseId = (course_id, keyword = null, page_url = null) => {
	return async (dispatch) => {
		let url =
			keyword != null
				? "api/topics/get-course-topics/search/" + keyword + "/course/" + course_id
				: "api/topics/get-course-topics/" + course_id;
		let targetUrl = page_url ? page_url : url;
		await questionsSetApi
			.getCourseTopics(targetUrl)
			.then((response) => {
				if (response.data.status == 1) {
					var topics = response.data.data.data;
					dispatch({
						type: GET_TOPICS_BY_COURSE_ID_SUCCESS,
						error: null,
						topics: topics,
						topicPaginator: response.data.data,
					});
				} else {
					dispatch({
						type: GET_TOPICS_BY_COURSE_ID_ERROR,
						payload: response.data.msg,
					});
				}
			})
			.catch((error) => {
				dispatch({
					type: GET_TOPICS_BY_COURSE_ID_ERROR,
					payload: error.message,
				});
				toast.error(error?.message);
			});
	};
};

export const GetSelectedPatternInfo = (selectedPatternId, qs_id) => {
	return (dispatch) => {
		dispatch({
			type: GET_SELECTED_PATTERN_INFO,
			qs_id,
			selectedPatternId,
		});
	};
};

export const deletePattern = (patternId) => {
	return (dispatch) => {
		dispatch({
			type: QS_DELETE_PATTERN,
			patternId,
		});
		dispatch({
			type: NOT_SAVE_EDIT,
		});
	};
};

export const setSelectedPatternId = (patternId) => {
	return (dispatch) => {
		dispatch({
			type: GET_SELECTED_PATTERN_ID,
			patternId,
		});
	};
};
export const setSelectedPatternTotalGrade = () => {
	return (dispatch) => {
		dispatch({
			type: GET_SELECTED_PATTERN_TOTAL_GRADE,
		});
	};
};

export const setSelectedGroupId = (group) => {
	return (dispatch) => {
		dispatch({
			type: GET_SELECTED_GROUP_ID,
			group,
		});
		dispatch({
			type: CALC_TOTAL_GRADE_GROUP,
		});
		dispatch({
			type: GET_SELECTED_PATTERN_TOTAL_GRADE,
		});
	};
};

export const setSelectedTopic = (topic) => {
	return (dispatch) => {
		dispatch({
			type: GET_SELECTED_TOPIC,
			topic,
		});
	};
};

export const addNewPattern = (pName, qsID) => {
	const guid = Math.floor(1000 + Math.random() * 9000);
	const pattern = {
		idTemp: guid,
		name: pName,
		question_set_id: qsID,
		pattern_groups: [],
	};
	return (dispatch) => {
		dispatch({
			type: ADD_NEW_PATTERN,
			pattern,
		});
		dispatch({
			type: NOT_SAVE_EDIT,
		});
	};
};

export const editQuestionSetValue = (prop, value) => {
	return (dispatch) => {
		dispatch({
			type: UPDATE_QS_VALUE,
			prop,
			value,
		});
		dispatch({
			type: NOT_SAVE_EDIT,
		});
	};
};

export const editCoursesValue = (prop, value) => {
	return (dispatch) => {
		dispatch({
			type: UPDATE_COURSES_VALUE,
			prop,
			value,
		});
		dispatch({
			type: NOT_SAVE_EDIT,
		});
	};
};

export const updateGroupGrade = (value) => {
	return (dispatch) => {
		dispatch({
			type: UPDATE_GROUP_GRADE_VALUE,
			value,
		});
		dispatch({
			type: NOT_SAVE_EDIT,
		});
	};
};

export const updateTopicsGrade = (value) => {
	return (dispatch) => {
		dispatch({
			type: UPDATE_TOPICS_GRADE_VALUE,
			value,
		});
		dispatch({
			type: NOT_SAVE_EDIT,
		});
	};
};

export const handlePatternName = (value) => {
	return (dispatch) => {
		dispatch({
			type: CHANGE_PATTERN_NAME,
			pName: value.patternName,
			patternId: value.patternId,
		});
		dispatch({
			type: NOT_SAVE_EDIT,
		});
	};
};

export const getQuestionSetPattern = (rabId, patternId) => async (dispatch) => {
	dispatch({ type: GET_PATTERN_REQUEST });

	try {
		const response = await questionsSetApi.getQsPattern(rabId, patternId);

		if (response.data.status === 1) {
			dispatch({
				type: GET_PATTERN_SUCCESS,
				payload: {
					groups: response.data.data,
					msg: response.data.msg,
				},
			});
		} else {
			toast.error(response.data.msg);
			dispatch({ type: GET_PATTERN_ERROR, payload: response.data.message });
		}
	} catch (error) {
		toast.error(error?.message);
		dispatch({
			type: GET_PATTERN_ERROR,
			payload: error.response?.data?.message || "something went wrong",
		});
	}
};

export const getQuestionSetStudentDetails = (rabId, patternId, QuestionId) => async (dispatch) => {
	dispatch({ type: GET_STUDENT_DETAILS_REQUEST });

	try {
		const response = await questionsSetApi.getQsStudentDetails(rabId, patternId, QuestionId);

		if (response.data.status === 1) {
			dispatch({
				type: GET_STUDENT_DETAILS_SUCCESS,
				payload: {
					StudentDetails: response.data.data,
				},
			});
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: GET_STUDENT_DETAILS_ERROR,
				payload: response.data.message,
			});
		}
	} catch (error) {
		toast.error(error?.message);
		dispatch({
			type: GET_STUDENT_DETAILS_ERROR,
			payload: error.response?.data?.message || "something went wrong",
		});
	}
};

export const updateQuestionSetStudentDetails = (QuestionId, data) => async (dispatch) => {
	dispatch({ type: POST_STUDENT_DETAILS_REQUEST });

	try {
		const response = await questionsSetApi.updateStudentDetails(QuestionId, data);

		if (response.data.status === 1) {
			dispatch({
				type: POST_STUDENT_DETAILS_SUCCESS,
			});
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: POST_STUDENT_DETAILS_ERROR,
			});
		}
	} catch (error) {
		toast.error(error?.message);
		dispatch({
			type: POST_STUDENT_DETAILS_ERROR,
		});
	}
};

export const updateStudentsDetailFields = (temp) => {
	return (dispatch) => {
		dispatch({
			type: UPDATE_STUDENTS_DETAIL_FIELDS,
			temp,
		});
	};
};

export const updateStudentsMarkFields = (temp) => {
	return (dispatch) => {
		dispatch({
			type: UPDATE_STUDENTS_MARK_FIELDS,
			temp,
		});
	};
};

export const setStudentId = (id) => {
	return (dispatch) => {
		dispatch({
			type: SET_STUDENT_ID,
			id,
		});
	};
};

export const setQuestionSetId = (id) => {
	return (dispatch) => {
		dispatch({
			type: SET_QUESTION_SET_ID,
			id,
		});
	};
};
