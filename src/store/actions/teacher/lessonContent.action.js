import { lessonContentApi } from "api/global/lessonContent";
import Swal, { DANGER, SUCCESS } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import {
	ADD_LESSON_LINK_REQUEST,
	ADD_LESSON_LINK_SUCCESS,
	ADD_LESSON_LINK_ERROR,
	EDIT_LESSON_LINK_REQUEST,
	EDIT_LESSON_LINK_SUCCESS,
	EDIT_LESSON_LINK_ERROR,
	REMOVE_LESSON_LINK_REQUEST,
	REMOVE_LESSON_LINK_SUCCESS,
	REMOVE_LESSON_LINK_ERROR,
	SHOW_LESSON_LINK_REQUEST,
	SHOW_LESSON_LINK_SUCCESS,
	SHOW_LESSON_LINK_ERROR,
	ADD_LESSON_TEXT_REQUEST,
	ADD_LESSON_TEXT_SUCCESS,
	ADD_LESSON_TEXT_ERROR,
	ADD_LIVE_SESSION_REQUEST,
	ADD_LIVE_SESSION_SUCCESS,
	ADD_LIVE_SESSION_ERROR,
	EDIT_LESSON_TEXT_REQUEST,
	EDIT_LESSON_TEXT_SUCCESS,
	EDIT_LESSON_TEXT_ERROR,
	REMOVE_LESSON_TEXT_REQUEST,
	REMOVE_LESSON_TEXT_SUCCESS,
	REMOVE_LESSON_TEXT_ERROR,
	SHOW_LESSON_TEXT_REQUEST,
	SHOW_LESSON_TEXT_SUCCESS,
	SHOW_LESSON_TEXT_ERROR,
	ADD_LESSON_FILE_REQUEST,
	ADD_LESSON_FILE_SUCCESS,
	ADD_LESSON_FILE_ERROR,
	REMOVE_LESSON_FILE_REQUEST,
	REMOVE_LESSON_FILE_SUCCESS,
	REMOVE_LESSON_FILE_ERROR,
	REFRESH_LISTER,
	REFRESH_LISTER_TEXT,
	REFRESH_LISTER_FILE,
	SET_LESSON_LINK_ID,
	SET_LESSON_TEXT_ID,
	DELETE_LESSON_CONTENT_REQUEST,
	DELETE_LESSON_CONTENT_SUCCESS,
	DELETE_LESSON_CONTENT_ERROR,
	EDIT_LESSON_ATTACHMENT_REQUEST,
	EDIT_LESSON_ATTACHMENT_SUCCESS,
	EDIT_LESSON_ATTACHMENT_ERROR,
	LIVE_SESSION_BY_ID_REQUEST,
	LIVE_SESSION_BY_ID_SUCCESS,
	LIVE_SESSION_BY_ID_ERROR,
	GET_LESSON_CONTENT_BY_LESSON_ID_REQUEST,
	GET_LESSON_CONTENT_BY_LESSON_ID_SUCCESS,
	GET_LESSON_CONTENT_BY_LESSON_ID_ERROR,
} from "./teacherTypes";
import {
	handleShouldNotBeAddedLessonContent,
	addNewLessonContentToModuleContent,
	editLessonContentFromModuleContent,
	reloadLiveSession,
} from "../global/coursesManager.action";
import { toast } from "react-toastify";

export const getLessonContentByLessonIdAsync = (lessonId, withoutLiveSessions, published) => async (dispatch) => {
	dispatch({ type: GET_LESSON_CONTENT_BY_LESSON_ID_REQUEST });
	try {
		const response = await lessonContentApi.getLessonContentByLessonId(lessonId, withoutLiveSessions, published);

		if (response.data.status === 1) {
			const data = response.data.data;

			dispatch({
				type: GET_LESSON_CONTENT_BY_LESSON_ID_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: GET_LESSON_CONTENT_BY_LESSON_ID_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_LESSON_CONTENT_BY_LESSON_ID_ERROR });
		toast.error(error?.message);
	}
};

export const deleteLessonContentAsync = (moduleId, moduleContentId, lessonContentId, lessonContentType, hideAlert) => async (dispatch) => {
	dispatch({ type: DELETE_LESSON_CONTENT_REQUEST });
	try {
		const response = await lessonContentApi.deleteLessonContent(lessonContentId, lessonContentType);

		if (response.data.status === 1) {
			dispatch({
				type: DELETE_LESSON_CONTENT_SUCCESS,
				payload: { moduleId, moduleContentId, lessonContentId },
			});
			hideAlert && hideAlert();
			dispatch(handleShouldNotBeAddedLessonContent(false));
		} else {
			dispatch({ type: DELETE_LESSON_CONTENT_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: DELETE_LESSON_CONTENT_ERROR });
		toast.error(error?.message);
	}
};

export const addLessonTextAsync = (lessonId, dataSending, courseModule, moduleContent) => async (dispatch) => {
	dispatch({ type: ADD_LESSON_TEXT_REQUEST });
	try {
		const response = await lessonContentApi.addLessonText(lessonId, dataSending);

		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: ADD_LESSON_TEXT_SUCCESS,
			});
			dispatch(handleShouldNotBeAddedLessonContent(false));
			dispatch(addNewLessonContentToModuleContent(courseModule, moduleContent, data));
		} else {
			dispatch({ type: ADD_LESSON_TEXT_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: ADD_LESSON_TEXT_ERROR,
		});
		toast.error(error?.message);
	}
};

export const addLessonFileAsync = (lessonId, dataSending, courseModule, moduleContent) => async (dispatch) => {
	dispatch({ type: ADD_LESSON_FILE_REQUEST });
	try {
		const response = await lessonContentApi.addLessonFile(lessonId, dataSending);

		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: ADD_LESSON_FILE_SUCCESS,
			});
			dispatch(handleShouldNotBeAddedLessonContent(false));
			dispatch(addNewLessonContentToModuleContent(courseModule, moduleContent, data, "attachment"));
		} else {
			dispatch({ type: ADD_LESSON_FILE_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: ADD_LESSON_FILE_ERROR,
		});
		toast.error(error?.message);
	}
};

export const addLessonLinkAsync = (lessonId, dataSending, courseModule, moduleContent) => async (dispatch) => {
	dispatch({ type: ADD_LESSON_LINK_REQUEST });
	try {
		const response = await lessonContentApi.addLessonLink(lessonId, dataSending);

		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: ADD_LESSON_LINK_SUCCESS,
			});
			dispatch(handleShouldNotBeAddedLessonContent(false));
			dispatch(addNewLessonContentToModuleContent(courseModule, moduleContent, data, "link"));
		} else {
			dispatch({ type: ADD_LESSON_LINK_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: ADD_LESSON_LINK_ERROR,
		});
		toast.error(error?.message);
	}
};

export const addLiveSessionAsync =
	(lessonId, courseModule, moduleContent, handleOpenLiveSessionModal, setMeetingId, setLessonId) => async (dispatch) => {
		dispatch({ type: ADD_LIVE_SESSION_REQUEST });
		try {
			const response = await lessonContentApi.addLiveSession(lessonId);

			if (response.data.status === 1) {
				const data = response.data.data;

				dispatch({
					type: ADD_LIVE_SESSION_SUCCESS,
				});
				dispatch(handleShouldNotBeAddedLessonContent(false));
				dispatch(addNewLessonContentToModuleContent(courseModule, moduleContent, data, "live_session"));
				handleOpenLiveSessionModal();
				setMeetingId(data.live_session.meeting_id);
				setLessonId(data.lesson_id);
			} else {
				dispatch({ type: ADD_LIVE_SESSION_ERROR });
				toast.error(response.data.msg);
			}
		} catch (error) {
			dispatch({
				type: ADD_LIVE_SESSION_ERROR,
			});
			toast.error(error?.message);
		}
	};

export const editLessonAttachmentAsync =
	(moduleId, moduleContentId, lessonContentId, lessonContentType, dataSending, setEditAttachment) => async (dispatch) => {
		dispatch({ type: EDIT_LESSON_ATTACHMENT_REQUEST });
		try {
			const response = await lessonContentApi.editLessonAttachment(lessonContentId, dataSending);

			if (response.data.status === 1) {
				const data = response.data.data;
				dispatch({
					type: EDIT_LESSON_ATTACHMENT_SUCCESS,
				});
				dispatch(handleShouldNotBeAddedLessonContent(false));
				dispatch(editLessonContentFromModuleContent(moduleId, moduleContentId, lessonContentId, lessonContentType, data));
				setEditAttachment && setEditAttachment(false);
			} else {
				dispatch({ type: EDIT_LESSON_ATTACHMENT_ERROR });
				toast.error(response.data.msg);
			}
		} catch (error) {
			dispatch({
				type: EDIT_LESSON_ATTACHMENT_ERROR,
			});
			toast.error(error?.message);
		}
	};

export const editLessonLinkAsync = (moduleId, moduleContentId, lessonContentId, lessonContentType, dataSending) => async (dispatch) => {
	dispatch({ type: EDIT_LESSON_LINK_REQUEST });
	try {
		const response = await lessonContentApi.editLessonLink(lessonContentId, dataSending);

		if (response.data.status === 1) {
			// new update 14/11
			// const data = response.data.data[0];
			const data = response.data.data;

			dispatch({
				type: EDIT_LESSON_LINK_SUCCESS,
			});

			dispatch(handleShouldNotBeAddedLessonContent(false));
			dispatch(editLessonContentFromModuleContent(moduleId, moduleContentId, lessonContentId, lessonContentType, data));
		} else {
			dispatch({ type: EDIT_LESSON_LINK_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: EDIT_LESSON_LINK_ERROR,
		});
		toast.error(error?.message);
	}
};

export const editLessonTextAsync = (moduleId, moduleContentId, lessonContentId, lessonContentType, dataSending) => async (dispatch) => {
	dispatch({ type: EDIT_LESSON_TEXT_REQUEST });
	try {
		const response = await lessonContentApi.editLessonText(lessonContentId, dataSending);

		if (response.data.status === 1) {
			const data = response.data.data;

			dispatch({
				type: EDIT_LESSON_TEXT_SUCCESS,
			});
			dispatch(handleShouldNotBeAddedLessonContent(false));
			dispatch(editLessonContentFromModuleContent(moduleId, moduleContentId, lessonContentId, lessonContentType, data));
		} else {
			dispatch({ type: EDIT_LESSON_TEXT_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: EDIT_LESSON_TEXT_ERROR,
		});
		toast.error(error?.message);
	}
};

export const liveSessionByIdAsync = (content, lessonContent) => async (dispatch) => {
	dispatch({ type: LIVE_SESSION_BY_ID_REQUEST });
	try {
		const response = await lessonContentApi.liveSessionById(lessonContent.id);

		if (response.data.status === 1) {
			const data = response.data.data.live_session;
			dispatch({
				type: LIVE_SESSION_BY_ID_SUCCESS,
			});

			dispatch(reloadLiveSession(content, lessonContent, data));
		} else {
			dispatch({ type: LIVE_SESSION_BY_ID_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: LIVE_SESSION_BY_ID_ERROR,
		});
		toast.error(error?.message);
	}
};

// _____________________________________________________________________________________________________________

export const removeLessonLinkAsync = (id) => async (dispatch) => {
	dispatch({ type: REMOVE_LESSON_LINK_REQUEST });
	try {
		const response = await lessonContentApi.removeLessonLink(id);

		if (response.data.status === 1) {
			dispatch({
				type: REMOVE_LESSON_LINK_SUCCESS,
			});
		} else {
			dispatch({ type: REMOVE_LESSON_LINK_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: REMOVE_LESSON_LINK_ERROR,
		});
		toast.error(error?.message);
	}
};

export const showLessonLinkAsync = (lessonLinkId) => async (dispatch) => {
	dispatch({ type: SHOW_LESSON_LINK_REQUEST });
	try {
		const response = await lessonContentApi.showLessonLink(lessonLinkId);

		if (response.data.status === 1) {
			const showLessonLinkData = response.data.data;
			dispatch({
				type: SHOW_LESSON_LINK_SUCCESS,
				payload: { showLessonLinkData },
			});
		} else {
			dispatch({ type: SHOW_LESSON_LINK_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: SHOW_LESSON_LINK_ERROR,
		});
		toast.error(error?.message);
	}
};

export const removeLessonTextAsync = (id) => async (dispatch) => {
	dispatch({ type: REMOVE_LESSON_TEXT_REQUEST });
	try {
		const response = await lessonContentApi.removeLessonText(id);

		if (response.data.status === 1) {
			dispatch({
				type: REMOVE_LESSON_TEXT_SUCCESS,
			});
		} else {
			dispatch({ type: REMOVE_LESSON_TEXT_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: REMOVE_LESSON_TEXT_ERROR,
		});
		toast.error(error?.message);
	}
};

export const showLessonTextAsync = (lessonTextId) => async (dispatch) => {
	dispatch({ type: SHOW_LESSON_TEXT_REQUEST });
	try {
		const response = await lessonContentApi.showLessonText(lessonTextId);

		if (response.data.status === 1) {
			const showLessonTextData = response.data.data;
			dispatch({
				type: SHOW_LESSON_TEXT_SUCCESS,
				payload: { showLessonTextData },
			});
		} else {
			dispatch({ type: SHOW_LESSON_TEXT_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: SHOW_LESSON_TEXT_ERROR,
		});
		toast.error(error?.message);
	}
};

export const removeLessonFileAsync = (id) => async (dispatch) => {
	dispatch({ type: REMOVE_LESSON_FILE_REQUEST });
	try {
		const response = await lessonContentApi.removeLessonFile(id);

		if (response.data.status === 1) {
			dispatch({
				type: REMOVE_LESSON_FILE_SUCCESS,
			});
		} else {
			dispatch({ type: REMOVE_LESSON_FILE_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: REMOVE_LESSON_FILE_ERROR,
		});
		toast.error(error?.message);
	}
};

export const refreshListerAsync = (refreshLister) => async (dispatch) => {
	dispatch({ type: REFRESH_LISTER, payload: { refreshLister } });
};

export const refreshListerTextAsync = (refreshListerText) => async (dispatch) => {
	dispatch({
		type: REFRESH_LISTER_TEXT,
		payload: { refreshListerText },
	});
};

export const refreshListerFileAsync = (refreshListerFile) => async (dispatch) => {
	dispatch({
		type: REFRESH_LISTER_FILE,
		payload: { refreshListerFile },
	});
};

export const setLessonLinkIdAsync = (lessonLinkId) => async (dispatch) => {
	dispatch({ type: SET_LESSON_LINK_ID, payload: { lessonLinkId } });
};

export const setLessonTextIdAsync = (lessonTextId) => async (dispatch) => {
	dispatch({ type: SET_LESSON_TEXT_ID, payload: { lessonTextId } });
};
