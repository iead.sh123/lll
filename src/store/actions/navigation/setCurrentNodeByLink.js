import { SET_CURRENT_NODE_BY_LINK } from "../global/globalTypes";

const setCurrentNodeByLink = (payload) => ({
  type: SET_CURRENT_NODE_BY_LINK,
  payload: payload,
});

export default setCurrentNodeByLink;
