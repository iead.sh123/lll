import { schoolInitiationApi } from "api/global/schoolInitiation";
import {
	GET_SEMESTERS_BY_ORGANIZATION_REQUEST,
	GET_SEMESTERS_BY_ORGANIZATION_SUCCESS,
	GET_SEMESTERS_BY_ORGANIZATION_ERROR,
	GET_GRADE_SCALES_REQUEST,
	GET_GRADE_SCALES_SUCCESS,
	GET_GRADE_SCALES_ERROR,
	GET_APP_GRADE_LEVELS_REQUEST,
	GET_APP_GRADE_LEVELS_SUCCESS,
	GET_APP_GRADE_LEVELS_ERROR,
	GET_MAIN_COURSES_REQUEST,
	GET_MAIN_COURSES_SUCCESS,
	GET_MAIN_COURSES_ERROR,
	GET_EDUCATION_STAGE_REQUEST,
	GET_EDUCATION_STAGE_SUCCESS,
	GET_EDUCATION_STAGE_ERROR,
	SAVE_EDUCATION_STAGE_REQUEST,
	SAVE_EDUCATION_STAGE_SUCCESS,
	SAVE_EDUCATION_STAGE_ERROR,
	SAVE_GRADE_LEVEL_REQUEST,
	SAVE_GRADE_LEVEL_SUCCESS,
	SAVE_GRADE_LEVEL_ERROR,
	SAVE_CURRICULA_REQUEST,
	SAVE_CURRICULA_SUCCESS,
	SAVE_CURRICULA_ERROR,
	EDUCATION_STAGE_BY_ID_REQUEST,
	EDUCATION_STAGE_BY_ID_SUCCESS,
	EDUCATION_STAGE_BY_ID_ERROR,
	GRADE_LEVELS_BY_ID_REQUEST,
	GRADE_LEVELS_BY_ID_SUCCESS,
	GRADE_LEVELS_BY_ID_ERROR,
	CURRICULA_BY_ID_REQUEST,
	CURRICULA_BY_ID_SUCCESS,
	CURRICULA_BY_ID_ERROR,
	SCHOOL_INITIATION_TREE_REQUEST,
	SCHOOL_INITIATION_TREE_SUCCESS,
	SCHOOL_INITIATION_TREE_ERROR,
	FILL_NAME_SCHOOL_INITIATION,
} from "./globalTypes";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

export const getSemestersByOrganizationAsync = (withOutPagination) => async (dispatch) => {
	dispatch({ type: GET_SEMESTERS_BY_ORGANIZATION_REQUEST });
	try {
		const response = await schoolInitiationApi.getSemestersByOrganization(withOutPagination);

		if (response.data.status === 1) {
			let data = response.data.data;

			dispatch({
				type: GET_SEMESTERS_BY_ORGANIZATION_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: GET_SEMESTERS_BY_ORGANIZATION_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_SEMESTERS_BY_ORGANIZATION_ERROR,
		});
		toast.error(error?.message);
	}
};

export const educationStageAsync = (forDropDownList) => async (dispatch) => {
	dispatch({ type: GET_EDUCATION_STAGE_REQUEST });
	try {
		const response = await schoolInitiationApi.educationStage(forDropDownList);

		if (response.data.status === 1) {
			let data = response.data.data.data;

			dispatch({
				type: GET_EDUCATION_STAGE_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: GET_EDUCATION_STAGE_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_EDUCATION_STAGE_ERROR,
		});
		toast.error(error?.message);
	}
};

export const getGradeScalesAsync = (forDropDownList) => async (dispatch) => {
	dispatch({ type: GET_GRADE_SCALES_REQUEST });
	try {
		const response = await schoolInitiationApi.getGradeScales(forDropDownList);

		if (response.data.status === 1) {
			const data = response.data.data;

			dispatch({
				type: GET_GRADE_SCALES_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: GET_GRADE_SCALES_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_GRADE_SCALES_ERROR,
		});
		toast.error(error?.message);
	}
};

export const getAppGradeLevelsAsync = (forDropDownList) => async (dispatch) => {
	dispatch({ type: GET_APP_GRADE_LEVELS_REQUEST });
	try {
		const response = await schoolInitiationApi.getAppGradeLevels(forDropDownList);

		if (response.data.status === 1) {
			let data = response.data.data;

			dispatch({
				type: GET_APP_GRADE_LEVELS_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: GET_APP_GRADE_LEVELS_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_APP_GRADE_LEVELS_ERROR,
		});
		toast.error(error?.message);
	}
};

export const getMainCoursesAsync = (forDropDownList) => async (dispatch) => {
	dispatch({ type: GET_MAIN_COURSES_REQUEST });
	try {
		const response = await schoolInitiationApi.getMainCourses(forDropDownList);

		if (response.data.status === 1) {
			let data = response.data.data;

			dispatch({
				type: GET_MAIN_COURSES_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: GET_MAIN_COURSES_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_MAIN_COURSES_ERROR,
		});
		toast.error(error?.message);
	}
};

export const saveEducationStageAsync = (data, setMessage, setIdAdded) => async (dispatch) => {
	dispatch({ type: SAVE_EDUCATION_STAGE_REQUEST });
	try {
		const response = await schoolInitiationApi.saveEducationStage(data);

		if (response.data.status === 1) {
			dispatch({
				type: SAVE_EDUCATION_STAGE_SUCCESS,
			});
			setMessage(response.data.msg);
			setIdAdded(response.data.data.id);
		} else {
			dispatch({
				type: SAVE_EDUCATION_STAGE_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: SAVE_EDUCATION_STAGE_ERROR,
		});
		toast.error(error?.message);
	}
};

export const saveGradeLevelAsync = (data, setMessage, setIdAdded) => async (dispatch) => {
	dispatch({ type: SAVE_GRADE_LEVEL_REQUEST });
	try {
		const response = await schoolInitiationApi.saveGradeLevel(data);

		if (response.data.status === 1) {
			dispatch({
				type: SAVE_GRADE_LEVEL_SUCCESS,
			});
			setMessage(response.data.msg);
			setIdAdded(response.data.data.id);
		} else {
			dispatch({
				type: SAVE_GRADE_LEVEL_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: SAVE_GRADE_LEVEL_ERROR,
		});
		toast.error(error?.message);
	}
};

export const saveCurriculaAsync = (data, setMessage, setIdAdded) => async (dispatch) => {
	dispatch({ type: SAVE_CURRICULA_REQUEST });
	try {
		const response = await schoolInitiationApi.saveCurricula(data);

		if (response.data.status === 1) {
			dispatch({
				type: SAVE_CURRICULA_SUCCESS,
			});
			setMessage(response.data.msg);
			setIdAdded(response.data.data.id);
		} else {
			dispatch({
				type: SAVE_CURRICULA_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: SAVE_CURRICULA_ERROR,
		});
		toast.error(error?.message);
	}
};

export const educationStageByIdAsync = (educationStageId, educationStageState) => async (dispatch) => {
	dispatch({ type: EDUCATION_STAGE_BY_ID_REQUEST });
	try {
		const response = await schoolInitiationApi.educationStageById(educationStageId);

		if (response.data.status === 1) {
			dispatch({
				type: EDUCATION_STAGE_BY_ID_SUCCESS,
			});

			educationStageState(response.data.data);
			dispatch(
				fillNameSchoolInitiation({
					label: response.data.data.name,
					value: response.data.data.id,
				})
			);
		} else {
			dispatch({
				type: EDUCATION_STAGE_BY_ID_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: EDUCATION_STAGE_BY_ID_ERROR,
		});
		toast.error(error?.message);
	}
};

export const gradeLevelsByIdAsync = (gradeLevelId, gradeLevelsState) => async (dispatch) => {
	dispatch({ type: GRADE_LEVELS_BY_ID_REQUEST });
	try {
		const response = await schoolInitiationApi.gradeLevelsById(gradeLevelId);

		if (response.data.status === 1) {
			dispatch({
				type: GRADE_LEVELS_BY_ID_SUCCESS,
			});
			gradeLevelsState(response.data.data);
			dispatch(
				fillNameSchoolInitiation({
					label: response.data.data.name,
					value: response.data.data.id,
				})
			);
		} else {
			dispatch({
				type: GRADE_LEVELS_BY_ID_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GRADE_LEVELS_BY_ID_ERROR,
		});
		toast.error(error?.message);
	}
};

export const curriculaByIdAsync = (curriculumId, curriculaState) => async (dispatch) => {
	dispatch({ type: CURRICULA_BY_ID_REQUEST });
	try {
		const response = await schoolInitiationApi.curriculaById(curriculumId);

		if (response.data.status === 1) {
			dispatch({
				type: CURRICULA_BY_ID_SUCCESS,
			});
			curriculaState(response.data.data);
			dispatch(
				fillNameSchoolInitiation({
					label: response.data.data.name,
					value: response.data.data.id,
				})
			);
		} else {
			dispatch({
				type: CURRICULA_BY_ID_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: CURRICULA_BY_ID_ERROR,
		});
		toast.error(error?.message);
	}
};

export const schoolInitiationTreeAsync = () => async (dispatch) => {
	dispatch({ type: SCHOOL_INITIATION_TREE_REQUEST });
	try {
		const response = await schoolInitiationApi.tree();

		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: SCHOOL_INITIATION_TREE_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: SCHOOL_INITIATION_TREE_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: SCHOOL_INITIATION_TREE_ERROR,
		});
		toast.error(error?.message);
	}
};

export const fillNameSchoolInitiation = (name) => {
	return (dispatch) => {
		dispatch({
			type: FILL_NAME_SCHOOL_INITIATION,
			payload: { data: name },
		});
	};
};
