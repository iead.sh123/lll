import {
	FLAG_TO_CHECK_IF_WEEKLY_SCHEDULE_HAVE_EVENTS,
	GET_SCHOOL_INFO,
	EDIT_SCHOOL_INFO_SUCCESS,
	EDIT_SCHOOL_INFO_REQUEST,
	EDIT_SCHOOL_INFO_ERROR,
	GET_SCHOOL_TREE_REQUEST,
	GET_SCHOOL_TREE_ERROR,
	GET_SCHOOL_TREE_SUCCESS,
	SET_OPEN_STAGE_ID,
	SET_OPEN_GRADE_LEVEL_ID,
	SET_OPEN_CURRICULA,
	ADD_EDUCATION_STAGE,
	ADD_GRADE_LEVEL,
	ADD_CURRICULA,
	SAVE_EDUCATION_STAGE,
	SAVE_GRADE_LEVEL,
	SAVE_CURRICULA,
	DELETE_EDUCATION_STAGE,
	DELETE_GRADE_LEVEL,
	DELETE_CURRUICLA,
	DISABLE_ACTIVE_STAGE,
	DISABLE_ACTIVE_GRADE_LEVEL,
	DISABLE_ACTIVE_CURRICULA,
	GET_EDUCATION_PRINCIPALS,
	GET_DATA_FOR_TYPE,
	SELECT_ALL_PRINCIPALS,
	SELECT_ALL_STUDENTS,
	CLEAR_ALL_PRINCIPALS,
	CLEAR_ALL_STUDENTS,
	SELECT_PRINCIPAL,
	SELECT_STUDENT,
	UN_SELECT_PRINCIPAL,
	UN_SELECT_STUDENT,
	REORDER_EDUCATION_STAGES,
	REORDER_GRADE_LEVELS,
	REORDER_CURRICULAS,
	UPDATE_EDUCATION_ORDER,
	UPDATE_GRADE_LEVEL_ORDER,
	UPDATE_CURRICULA_ORDER,
	SET_ACTIVE_COURSES,
	SET_COURSES_CATEGORIES,
	FETCHING_COURSES,
	TOGGLE_COURSE_SELECTION,
	GET_CANDIDATE_PRINCIPALS,
	SELECT_CANDIDATE_PRINCIPAL,
	SELECT_CANDIDATE_STUDENT,
	UN_SELECT_CANDIDATE_PRINCIPAL,
	UN_SELECT_CANDIDATE_STUDENT,
	ADD_PRINCAPLE_TO_STAGE,
	GET_STUDENT_FOR_GRADE,
	GET_CANDIDATE_STUDENTS_FOR_GRADE,
	ADD_STUDENTS_TO_GRADE,
	DELETE_STUDENTS_FROM_GRADE,
	DELETE_PRINCIPALS_FOR_STAGE,
	GET_ORGANIZATION_PERIODS,
	TOGGLE_PERIOD_SAVED_STATUS,
	DELETE_PERIOD,
	GET_CURRICULA_COURSES,
	ADD_COURSE_TO_CURRICULA,
	CHANGE_PERIOD_INPUT_VALUE,
	EDIT_PERIOD,
	ADD_NEW_PERIOD,
	CHANGE_PERIOD_FROM_TIME,
	CHANGE_PERIOD_TO_TIME,
	SET_GRADE_SCALE,
	EDIT_SCALE_DETAIL,
	SET_GRADE_ASSESSMENTS,
	DELETE_ASSESSMENT,
	ADD_NEW_ASSESSMENT,
	GETTING_PERIODS,
	FINISHING_PERIODS,
	GETTING_GRADE_SCALE,
	FINISHING_GRADE_SCALE,
	GETTING_PRINCIPALS,
	FINISHING_PRINCIPALS,
	GETTING_STUDENTS,
	FINISHING_STUDENTS,
	GETTING_ASSESSMENTS,
	FINISHING_ASSESSMENTS,
	GETTING_CANDIDATES_PRINCIPALS,
	FINISHING_CANDIDATES_PRINCIPALS,
	GETTING_CANDIDATES_STUDENTS,
	FINISHING_CANDIDATES_STUDENTS,
	GETTING_CURRICULA_CONTENT,
	FINISHING_CURRICULA_CONTENT,
	GETTING_COURSES,
	FINISHING_COURSES,
	UPDATE_ASSESSMENT,
	ADD_STUDENT_AFTER_CREATING,
	ADD_PRINCIAPLE_AFTER_CREATING,
	GETTING_SCHOOL_INFO,
	FINISHING_SCHOOL_INFO,
	GETTING_SCHOOL_TREE,
	FINISHING_SCHOOL_TREE,
	FINISHING_CURRICULA_CATEGORY,
	CREATEING_CURRICULA_CATEGORY,
	SAVE_CATEGORY_AS_CURRICULA,
} from "./globalTypes";
import { schoolManagementAPI } from "api/SchoolManagement";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import { types } from "logic/SchoolManagement/constants/index";
import { dataTypes } from "logic/SchoolManagement/constants";
import { coursesManagerApi } from "api/global/coursesManager";
import axios from "axios";
import { faLessThan } from "@fortawesome/free-solid-svg-icons";
import tr from "components/Global/RComs/RTranslator";
import { Bounce, Slide, toast } from "react-toastify";
import RFlex from "components/Global/RComs/RFlex/RFlex";

export const checkIfWeeklyScheduleHaveEvents = (flag) => {
	return (dispatch) => {
		dispatch({
			type: FLAG_TO_CHECK_IF_WEEKLY_SCHEDULE_HAVE_EVENTS,
			payload: { flag },
		});
	};
};

export const getSchoolInfo = (organization_id, history) => async (dispatch) => {
	dispatch({
		type: GETTING_SCHOOL_INFO,
	});
	const payload = {
		payload: {
			TableName: "organizations",
			id: organization_id,
		},
	};
	let response = null;
	try {
		response = await schoolManagementAPI.getSchoolInfo(payload);
		if (!response.data.status) throw new Error(response.data.msg);
	} catch (error) {
		toast.error(error.message);
	}
	if (response) {
		const name = response?.data?.data?.records?.name;
		const email = response?.data?.data?.records?.contact_information;
		const url = response?.data?.data?.records?.facebook_page;
		const address = response?.data?.data?.records?.address;
		const image = response?.data?.data?.records?.logo[0]?.hash_id;
		const info = { name: name, email: email, url: url, address: address, image: image };
		const isEdited = response?.data?.data?.records?.first_edited;
		dispatch({
			type: GET_SCHOOL_INFO,
			payload: { info: info },
		});
		dispatch({
			type: FINISHING_SCHOOL_INFO,
		});
		console.log("isEdited", isEdited);
		!isEdited ? "" : history.replace("/g/school-management");
		// if (notEdited)
		//     return false
		// else
		//     return true
	}
};

export const editSchoolInfo = (organization_id, values, upload_id, hash_id, history, navigate) => async (dispatch) => {
	try {
		dispatch({
			type: EDIT_SCHOOL_INFO_REQUEST,
		});
		const payload = {
			payload: {
				TableName: "organizations",
				record: {
					id: organization_id,
					organization_type_id: 1,
					parent_id: null,
					first_edited: 1,
					semesters_number: 1,
					education_system_id: "2",
					address: values?.address,
					contact_information: values?.email,
					facebook_page: values?.url,
					logo: upload_id ? [{ url: { upload_id: upload_id } }] : [{ hash_id: hash_id }],
				},
			},
		};
		const response = await schoolManagementAPI.editSchoolInfo(payload);
		if (!response.data.status) {
			dispatch({
				type: EDIT_SCHOOL_INFO_ERROR,
			});
			throw new Error(response.data.msg);
		}
		if (response) {
			// dispatch({
			//     type: EDIT_SCHOOL_INFO_SUCCESS,
			//     payload: payload?.payload?.record
			// })
			dispatch(getSchoolInfo(organization_id));
			navigate ? history.replace("/g/school-management") : "";
		}
	} catch (error) {
		toast.error(error.message);
	}
};

export const getSchoolTree = (organizationId) => async (dispatch) => {
	dispatch({
		type: GETTING_SCHOOL_TREE,
	});
	const school = {
		stageIds: [],
	};
	const stages = {};
	const gradeLevels = {};
	const curriculas = {};

	try {
		const response = await schoolManagementAPI.getSchoolTree();
		const response2 = await schoolManagementAPI.getDefaultValues(organizationId);
		if (!response.data.status) {
			dispatch({
				type: GET_SCHOOL_TREE_ERROR,
			});
			throw new Error(response.data.msg);
		}
		if (!response2.data.status) {
			dispatch({
				type: GET_SCHOOL_TREE_ERROR,
			});
			throw new Error(response.data.msg);
		}
		// ... other properties from the school
		const schoolData = response?.data?.data;
		schoolData?.education_stages?.map((stage) => {
			school.stageIds.push(stage?.id);

			const temp_stage = { gradeLevelIds: [] };

			for (let key in stage) {
				if (key !== "grade_levels") {
					temp_stage[key] = stage[key];
				}
			}
			// ... other properties from the stage

			stage.grade_levels?.forEach((gradeLevel) => {
				temp_stage.gradeLevelIds.push(gradeLevel.id);
				const temp_grade_level = { curriculaIds: [] };
				for (let key in gradeLevel) {
					if (key !== "curricula") {
						temp_grade_level[key] = gradeLevel[key];
					}
				}

				gradeLevel?.curricula?.forEach((curriculum) => {
					temp_grade_level.curriculaIds?.push(curriculum.id);
					const temp_curricula = { studentsIds: [], ...curriculum };
					//TODO: add Curricula students
					curriculas[curriculum?.id] = temp_curricula;
				});
				//TODO: add gradelevel student
				gradeLevels[gradeLevel?.id] = temp_grade_level;
			});
			stages[stage.id] = temp_stage;
		});
		// Add any specific properties you want to include
		for (let key in schoolData) {
			if (key !== "education_stages") {
				school[key] = schoolData[key];
			}
		}
		const defautlGradeScale = response2?.data?.data?.default_gradeScale;
		const defaultSemester = response2?.data?.data?.default_semester;
		const defaultMainCourse = response2?.data?.data?.main_course;

		dispatch({
			type: GET_SCHOOL_TREE_SUCCESS,
			payload: {
				school: school,
				stages: stages,
				gradeLevels: gradeLevels,
				curriculas: curriculas,
				defautlGradeScale: defautlGradeScale,
				defaultSemester: defaultSemester,
				defaultMainCourse: defaultMainCourse,
			},
		});

		dispatch({
			type: FINISHING_SCHOOL_TREE,
		});
	} catch (error) {
		toast.error(error.message);
	}
	// Add remaining properties from the school
};

export const setOpenId = (collapseType, id) => (dispatch) => {
	switch (collapseType) {
		case types.Stage:
			dispatch({
				type: SET_OPEN_STAGE_ID,
				payload: { collapseType: collapseType, id: id },
			});
			break;
		case types.GradeLevel:
			dispatch({
				type: SET_OPEN_GRADE_LEVEL_ID,
				payload: { collapseType: collapseType, id: id },
			});
			break;
		case types.Curricula:
			dispatch({
				type: SET_OPEN_CURRICULA,
				payload: { collapseType: collapseType, id: id },
			});
	}
};
export const disableActiveCollapse = (collapseType) => (dispatch) => {
	switch (collapseType) {
		case types.Stage:
			dispatch({
				type: DISABLE_ACTIVE_STAGE,
			});
			break;
		case types.GradeLevel:
			dispatch({
				type: DISABLE_ACTIVE_GRADE_LEVEL,
			});
			break;
		case types.Curricula:
			dispatch({
				type: DISABLE_ACTIVE_CURRICULA,
			});
	}
};

export const addSpecificCollapse = (collapseType, nextIndex, fatherId, order) => (dispatch) => {
	switch (collapseType) {
		case types.Stage:
			dispatch({
				type: ADD_EDUCATION_STAGE,
				payload: { collapseType: collapseType, id: -nextIndex, organization_id: fatherId, order: order },
			});
			break;
		case types.GradeLevel:
			dispatch({
				type: ADD_GRADE_LEVEL,
				payload: { collapseType: collapseType, id: -nextIndex, educationStageId: fatherId, order: order },
			});
			break;
		case types.Curricula:
			dispatch({
				type: ADD_CURRICULA,
				payload: { collapseType: collapseType, id: -nextIndex, gradeLevelId: fatherId, order: order },
			});
	}
};

export const saveEducationStage = (id, name, order, organization_id) => async (dispatch) => {
	try {
		let payload = {};
		id > 0 ? (payload = { id: id, name: name, order: order, organization_id: organization_id }) : (payload = { name: name, order: order });
		const response = await schoolManagementAPI?.addEductaionStage(payload);
		if (!response.data.status) throw new Error(response.data.msg);

		if (response) {
			const new_id = response?.data?.data?.id;
			dispatch({
				type: SAVE_EDUCATION_STAGE,
				payload: { id: new_id, oldId: id <= 0 ? id : new_id, name: name },
			});
		}
		return true;
	} catch (error) {
		toast.error(error.message);
		return false;
	}
};

export const saveGradeLevel = (id, name, order, education_stage_id) => async (dispatch, getState) => {
	try {
		const defaultGradeScaleId = getState().schoolManagementRed.defaultGradeScaleId;
		const defaultMainCourseId = getState().schoolManagementRed.defaultMainCourseId;
		const defaultSemesterId = getState().schoolManagementRed.defaultSemesterId;
		const defaultSemesterName = getState().schoolManagementRed.defaultSemesterName;
		const code = education_stage_id + " " + order;
		let payload = {};
		id > 0
			? (payload = {
					id: id,
					title: name,
					grade_level_order: order,
					education_stage_id: education_stage_id,
					code: "",
					grade_scale_id: defaultGradeScaleId,
			  })
			: (payload = {
					title: name,
					grade_level_order: order,
					education_stage_id: education_stage_id,
					code: "",
					grade_scale_id: defaultGradeScaleId,
			  });

		const response = await schoolManagementAPI?.addGradeLevel(payload);

		if (!response.data.status) throw new Error(response.data.msg);

		if (response) {
			const new_id = response?.data?.data?.id;
			let payload2 = null;
			let curriculaId = null;
			let response2 = null;
			if (id <= 0) {
				payload2 = {
					name: "default Curricula",
					grade_level_id: new_id,
					order: 1,
					main_course_id: defaultMainCourseId,
					semester_id: defaultSemesterId,
					main_course: { value: defaultMainCourseId, label: defaultSemesterName },
				};
				response2 = await schoolManagementAPI.addCurricula(payload2);
				if (!response2.data.status) throw new Error(response2.data.msg);
			}
			curriculaId = response2?.data.data.id;
			console.log("aaaa", curriculaId);
			dispatch({
				type: SAVE_GRADE_LEVEL,
				payload: {
					id: new_id,
					oldId: id <= 0 ? id : new_id,
					title: name,
					stageId: education_stage_id,
					defaultCurricula: id <= 0 ? { id: curriculaId, ...payload2 } : null,
				},
			});
		}
		return true;
	} catch (error) {
		toast.error(error.message);
		return false;
	}
};

export const saveCurricula = (id, name, order, grade_level_id) => async (dispatch, getState) => {
	try {
		const defaultMainCourseId = getState().schoolManagementRed.defaultMainCourseId;
		const defaultSemesterId = getState().schoolManagementRed.defaultSemesterId;
		const defaultSemesterName = getState().schoolManagementRed.defaultSemesterName;
		let payload = {};
		id > 0
			? (payload = {
					id: id,
					name: name,
					grade_level_id: grade_level_id,
					order: order,
					main_course_id: defaultMainCourseId,
					semester_id: defaultSemesterId,
					main_course: { value: defaultMainCourseId, label: defaultSemesterName },
			  })
			: (payload = {
					name: name,
					grade_level_id: grade_level_id,
					order: order,
					main_course_id: defaultMainCourseId,
					semester_id: defaultSemesterId,
					main_course: { value: defaultMainCourseId, label: defaultSemesterName },
			  });
		const response = await schoolManagementAPI.addCurricula(payload);

		if (!response.data.status) throw new Error(response.data.msg);

		if (response) {
			const new_id = response?.data?.data?.id;
			dispatch({
				type: SAVE_CURRICULA,
				payload: { id: new_id, oldId: id <= 0 ? id : new_id, name: name, gradeLevelId: grade_level_id },
			});
		}
		console.log("hello");
		return true;
	} catch (error) {
		toast.error(error.message);
		return false;
	}
};

export const deleteSpecificCollapse = (collapseType, id, fatherId, order) => async (dispatch, getState) => {
	try {
		const payload = {
			payload: {
				TableName: collapseType == types.Stage ? "education_stages" : collapseType == types.GradeLevel ? "grade_levels" : "curricula",
				id: id,
			},
		};

		const response = await schoolManagementAPI?.deleteSpecificCollapse(payload);
		if (!response.data.status) throw new Error(response.data.msg);
		if (response) {
			switch (collapseType) {
				case types.Stage:
					dispatch({
						type: DELETE_EDUCATION_STAGE,
						payload: { collapseType: collapseType, id: id, order: order },
					});
					break;
				case types.GradeLevel:
					dispatch({
						type: DELETE_GRADE_LEVEL,
						payload: { collapseType: collapseType, id: id, stageId: fatherId, order: order },
					});
					break;
				case types.Curricula:
					dispatch({
						type: DELETE_CURRUICLA,
						payload: { collapseType: collapseType, id: id, gradeLevelId: fatherId },
					});
			}
			let oldIds = null;
			let newIds = null;
			let response2 = null;
			let newOrder = null;
			switch (collapseType) {
				case types.Stage:
					oldIds = getState().schoolManagementRed.school.stageIds;
					newIds = oldIds.filter((id, index) => oldIds != id);
					response2 = await schoolManagementAPI.reorderEducationStages(fatherId, { ids: newIds });
					if (!response2.data.status) throw new Error(response.data.msg);
					newOrder = response?.data?.data;
					dispatch({
						type: UPDATE_EDUCATION_ORDER,
						payload: { newOrder: newOrder },
					});
					return;
				case types.GradeLevel:
					oldIds = getState().schoolManagementRed.stages[fatherId].gradeLevelIds;
					newIds = oldIds.filter((id, index) => oldIds != id);
					response2 = await schoolManagementAPI.reorderGradeLevels(fatherId, { ids: newIds });
					if (!response2.data.status) throw new Error(response.data.msg);
					newOrder = response?.data?.data;
					dispatch({
						type: UPDATE_GRADE_LEVEL_ORDER,
						payload: { newOrder: newOrder },
					});
					return;
				case types.Curricula:
					oldIds = getState().schoolManagementRed.gradeLevels[fatherId].curriculaIds;
					newIds = oldIds.filter((id, index) => oldIds != id);
					response2 = await schoolManagementAPI.reorderCurricula(fatherId, { ids: newIds });
					if (!response2.data.status) throw new Error(response.data.msg);
					newOrder = response?.data?.data;
					dispatch({
						type: UPDATE_CURRICULA_ORDER,
						payload: { newOrder: newOrder },
					});
					return;
			}
		}
	} catch (error) {
		toast.error(error.message);
	}
};

export const getPrincipalForEducation = (educationStageId, order) => async (dispatch) => {
	try {
		dispatch({
			type: GETTING_PRINCIPALS,
		});
		const response = await schoolManagementAPI.getPrincipalForEducation(educationStageId, order);
		if (!response.data.status) {
			dispatch({
				type: GET_SCHOOL_TREE_ERROR,
			});
			throw new Error(response.data.msg);
		}
		const data = response?.data?.data;
		const principalsIds = [];

		const principals = data.reduce((result, item) => {
			const { id } = item;
			principalsIds.push(id);
			result[id] = { ...item };
			result[id]["selected"] = false;
			result["selectMode"] = false;
			result[id].image = item.profile?.hash_id || null;
			return result;
		}, {});
		principals["ids"] = principalsIds;

		dispatch({
			type: GET_EDUCATION_PRINCIPALS,
			payload: { principals: principals },
		});
		dispatch({
			type: FINISHING_PRINCIPALS,
		});
	} catch (error) {
		toast.error(error.message);
		dispatch({
			type: FINISHING_PRINCIPALS,
		});
	}
};
export const getCandidatePrincipalForEducation =
	(url = null, params) =>
	async (dispatch) => {
		try {
			dispatch({
				type: GETTING_CANDIDATES_PRINCIPALS,
			});
			const educationStageId = params.educationStageId;
			const prefix = params.prefix || "";
			const newUrl = url ? url + "&education_stage_id=" + educationStageId + "&prefix=" + prefix : null;
			const response = await schoolManagementAPI.getCandidatePrincipalForEducation(newUrl, educationStageId, prefix);
			if (!response.data.status) {
				dispatch({
					type: GET_SCHOOL_TREE_ERROR,
				});
				throw new Error(response.data.msg);
			}
			const data = response?.data?.data?.data;
			const candidatePrincipalsIds = [];
			const candidatePrincipals = data.reduce((result, item) => {
				const { id } = item;
				candidatePrincipalsIds.push(id);
				result[id] = { ...item };
				result[id].image = item.image?.hash_id || null;
				result[id]["selected"] = false;
				return result;
			}, {});
			candidatePrincipals["ids"] = candidatePrincipalsIds;
			const pagination = response?.data?.data;
			candidatePrincipals["current_page"] = pagination.current_page;
			candidatePrincipals["first_page_url"] = pagination.first_page_url;
			candidatePrincipals["last_page"] = pagination.last_page;
			candidatePrincipals["last_page_url"] = pagination.last_page_url;
			candidatePrincipals["next_page_url"] = pagination.next_page_url;
			candidatePrincipals["per_page"] = pagination.per_page;
			candidatePrincipals["prev_page_url"] = pagination.prev_page_url;
			candidatePrincipals["total"] = pagination.total;
			dispatch({
				type: GET_CANDIDATE_PRINCIPALS,
				payload: { candidatePrincipals },
			});
			dispatch({
				type: FINISHING_CANDIDATES_PRINCIPALS,
			});
		} catch (error) {
			toast.error(error.message);
			dispatch({
				type: FINISHING_CANDIDATES_PRINCIPALS,
			});
		}
	};
export const getStudentsForGrade = (gradeLevelId, order) => async (dispatch) => {
	try {
		dispatch({
			type: GETTING_STUDENTS,
		});
		const response = await schoolManagementAPI.getStudentsForGrade(gradeLevelId, order);
		if (!response.data.status) {
			dispatch({
				type: GET_SCHOOL_TREE_ERROR,
			});
			throw new Error(response.data.msg);
		}
		const data = response?.data?.data;
		const studentsIds = [];

		const students = data.reduce((result, item) => {
			const { id } = item;
			studentsIds.push(id);
			result[id] = { ...item };
			result[id]["selected"] = false;
			result["selectMode"] = false;
			result[id].image = item.profile?.hash_id || null;
			return result;
		}, {});
		students["ids"] = studentsIds;
		dispatch({
			type: GET_STUDENT_FOR_GRADE,
			payload: { students: students },
		});
		dispatch({
			type: FINISHING_STUDENTS,
		});
	} catch (error) {
		toast.error(error.message);
		dispatch({
			type: FINISHING_STUDENTS,
		});
	}
};
export const getCandidateStudentsForGrade =
	(url = null, params) =>
	async (dispatch) => {
		try {
			dispatch({
				type: GETTING_CANDIDATES_STUDENTS,
			});
			const gradeLevelId = params.gradeLevelId;
			const prefix = params.prefix || "";
			const newUrl = url ? url + "&prefix=" + prefix : "";
			const response = await schoolManagementAPI.getCandidateStudentsForGrade(newUrl, gradeLevelId, prefix);
			if (!response.data.status) {
				dispatch({
					type: GET_SCHOOL_TREE_ERROR,
				});
				throw new Error(response.data.msg);
			}
			const data = response?.data?.data?.data;
			const candidateStudentsIds = [];
			const candidateStudents = data.reduce((result, item) => {
				const { id } = item;
				candidateStudentsIds.push(id);
				result[id] = { ...item };
				result[id].image = item.image?.hash_id || null;
				result[id]["selected"] = false;
				return result;
			}, {});
			candidateStudents["ids"] = candidateStudentsIds;
			const pagination = response?.data?.data;
			candidateStudents["current_page"] = pagination.current_page;
			candidateStudents["first_page_url"] = pagination.first_page_url;
			candidateStudents["last_page"] = pagination.last_page;
			candidateStudents["last_page_url"] = pagination.last_page_url;
			candidateStudents["next_page_url"] = pagination.next_page_url;
			candidateStudents["per_page"] = pagination.per_page;
			candidateStudents["prev_page_url"] = pagination.prev_page_url;
			candidateStudents["total"] = pagination.total;
			dispatch({
				type: GET_CANDIDATE_STUDENTS_FOR_GRADE,
				payload: { candidateStudents: candidateStudents },
			});
			dispatch({
				type: FINISHING_CANDIDATES_STUDENTS,
			});
		} catch (error) {
			toast.error(error.message);
			dispatch({
				type: FINISHING_CANDIDATES_STUDENTS,
			});
		}
	};

export const selectAll = (type) => (dispatch) => {
	switch (type) {
		case dataTypes.Principals:
			dispatch({
				type: SELECT_ALL_PRINCIPALS,
			});
			return;
		case dataTypes.Students:
			dispatch({
				type: SELECT_ALL_STUDENTS,
			});
			return;
	}
};
export const clearAllSelection = (type) => (dispatch) => {
	switch (type) {
		case dataTypes.Principals:
			dispatch({
				type: CLEAR_ALL_PRINCIPALS,
			});
			return;
		case dataTypes.Students:
			dispatch({
				type: CLEAR_ALL_STUDENTS,
			});
			return;
	}
};

export const selectUser = (type, id) => (dispatch) => {
	switch (type) {
		case dataTypes.Principals:
			dispatch({
				type: SELECT_PRINCIPAL,
				payload: { id: id },
			});
			return;
		case dataTypes.Students:
			dispatch({
				type: SELECT_STUDENT,
				payload: { id: id },
			});
			return;
	}
};
export const unSelectUser = (type, id) => (dispatch) => {
	switch (type) {
		case dataTypes.Principals:
			dispatch({
				type: UN_SELECT_PRINCIPAL,
				payload: { id: id },
			});
			return;
		case dataTypes.Students:
			dispatch({
				type: UN_SELECT_STUDENT,
				payload: { id: id },
			});
			return;
	}
};
export const selectCandidate = (type, id) => (dispatch) => {
	switch (type) {
		case dataTypes.Principals:
			dispatch({
				type: SELECT_CANDIDATE_PRINCIPAL,
				payload: { id: id },
			});
			return;
		case dataTypes.Students:
			dispatch({
				type: SELECT_CANDIDATE_STUDENT,
				payload: { id: id },
			});
			return;
	}
};

export const unSelectCandidate = (type, id) => (dispatch) => {
	switch (type) {
		case dataTypes.Principals:
			dispatch({
				type: UN_SELECT_CANDIDATE_PRINCIPAL,
				payload: { id: id },
			});
			return;
		case dataTypes.Students:
			dispatch({
				type: UN_SELECT_CANDIDATE_STUDENT,
				payload: { id: id },
			});
			return;
	}
};

export const reorderStages = (organizationId, sourceIndex, destinationIndex) => async (dispatch, getState) => {
	// getState().schoolManagementRed
	try {
		const previousStageIds = getState().schoolManagementRed.school.stageIds;
		let newStageIds = [...previousStageIds];
		const sourceId = newStageIds[sourceIndex];
		const destenationId = newStageIds[destinationIndex];
		console.log("reorder", sourceId, destenationId);
		if (sourceId <= 0 || destenationId <= 0) {
			toast.warning("Please, Save The Education Stage First");
			return;
		}
		const removedElement = newStageIds.splice(sourceIndex, 1)[0];
		newStageIds.splice(destinationIndex, 0, removedElement);
		newStageIds = newStageIds.filter((id) => id > 0);
		const payload = { ids: newStageIds };
		dispatch({
			type: REORDER_EDUCATION_STAGES,
			payload: payload,
		});
		const response = await schoolManagementAPI.reorderEducationStages(organizationId, payload);
		if (!response.data.status) {
			dispatch({
				type: GET_SCHOOL_TREE_ERROR,
			});
			throw new Error(response.data.msg);
		}
		const newOrder = response?.data?.data;
		dispatch({
			type: UPDATE_EDUCATION_ORDER,
			payload: { newOrder: newOrder },
		});
	} catch (error) {
		toast.error(error.message);
	}
};

export const reorderGradeLevels = (educationStageId, sourceIndex, destinationIndex) => async (dispatch, getState) => {
	try {
		const previousGradeLevelIds = getState().schoolManagementRed.stages[educationStageId]?.gradeLevelIds;
		let newGradeLevelIds = [...previousGradeLevelIds];
		const sourceId = newGradeLevelIds[sourceIndex];
		const destenationId = newGradeLevelIds[destinationIndex];
		console.log("reorder", sourceId, destenationId);
		if (sourceId <= 0 || destenationId <= 0) {
			toast.warning("Please, Save The Grade Level First");
			return;
		}
		const removedElement = newGradeLevelIds.splice(sourceIndex, 1)[0];
		newGradeLevelIds.splice(destinationIndex, 0, removedElement);
		newGradeLevelIds = newGradeLevelIds.filter((id) => id > 0);

		const payload = { ids: newGradeLevelIds };
		dispatch({
			type: REORDER_GRADE_LEVELS,
			payload: { ids: payload.ids, educationStageId: educationStageId },
		});
		const response = await schoolManagementAPI.reorderGradeLevels(educationStageId, payload);
		if (!response.data.status) {
			dispatch({
				type: GET_SCHOOL_TREE_ERROR,
			});
			throw new Error(response.data.msg);
		}
		const newOrder = response?.data?.data;
		dispatch({
			type: UPDATE_GRADE_LEVEL_ORDER,
			payload: { newOrder: newOrder },
		});
	} catch (error) {
		toast.error(error.message);
	}
};

export const reorderCurricula = (gradeLevelId, sourceIndex, destinationIndex) => async (dispatch, getState) => {
	try {
		const previousCurriculaIds = getState().schoolManagementRed.gradeLevels[gradeLevelId]?.curriculaIds;
		let newCurriculaIds = [...previousCurriculaIds];
		const sourceId = newCurriculaIds[sourceIndex];
		const destenationId = newCurriculaIds[destinationIndex];
		console.log("reorder", sourceId, destenationId);
		if (sourceId <= 0 || destenationId <= 0) {
			toast.warning("Please, Save The Curricula First");
			return;
		}
		const removedElement = newCurriculaIds.splice(sourceIndex, 1)[0];
		newCurriculaIds.splice(destinationIndex, 0, removedElement);
		newCurriculaIds = newCurriculaIds.filter((id) => id > 0);
		const payload = { ids: newCurriculaIds };
		dispatch({
			type: REORDER_CURRICULAS,
			payload: { ids: payload.ids, gradeLevelId: gradeLevelId },
		});
		const response = await schoolManagementAPI.reorderCurricula(gradeLevelId, payload);
		if (!response.data.status) {
			dispatch({
				type: GET_SCHOOL_TREE_ERROR,
			});
			throw new Error(response.data.msg);
		}
		const newOrder = response?.data?.data;
		dispatch({
			type: UPDATE_CURRICULA_ORDER,
			payload: { newOrder: newOrder },
		});
	} catch (error) {
		toast.error(error.message);
	}
};

export const getCouresesCategories = () => async (dispatch) => {
	try {
		const response = await schoolManagementAPI.allCategories();
		if (!response.data.status) {
			dispatch({
				type: GET_SCHOOL_TREE_ERROR,
			});
			throw new Error(response.data.msg);
		}
		const categories = response.data.data;
		dispatch({
			type: SET_COURSES_CATEGORIES,
			payload: { categories: categories },
		});
	} catch (error) {
		toast.error(error.message);
	}
};
export const getAllCourses =
	(url, data, noCurricula = false) =>
	async (dispatch, getState) => {
		try {
			let curriculuaId = getState().schoolManagementRed.openCurriculaId;
			if (noCurricula) curriculuaId = null;
			const name = data.name || "";
			const category_id = data.category_id || null;
			let newUrl = null;
			newUrl = url ? url + `?name=${name}` + `&category_id=${category_id}` : null;
			dispatch({
				type: GETTING_COURSES,
			});
			let response = await schoolManagementAPI.getCoursesByCategoryId(url, { curriculuaId, ...data });
			if (!response.data.status) {
				dispatch({
					type: GET_SCHOOL_TREE_ERROR,
				});
				throw new Error(response.data.msg);
			}
			const keyValueCourses = {};
			const activeCourses = response.data.data;
			const courseIds = activeCourses.data.map((course, index) => {
				course["selected"] = false;
				keyValueCourses[course.id] = course;
				return course.id;
			});
			activeCourses["courseIds"] = courseIds;
			activeCourses.data = keyValueCourses;
			dispatch({
				type: SET_ACTIVE_COURSES,
				payload: { courses: activeCourses },
			});
			dispatch({
				type: FINISHING_COURSES,
			});
		} catch (error) {
			toast.error(error.message);
			dispatch({
				type: FINISHING_COURSES,
			});
		}
	};

export const addPrincipalsToStage = () => async (dispatch, getState) => {
	try {
		const candidatePrincipals = getState().schoolManagementRed.candidatePrincipals;
		const educationStageId = getState().schoolManagementRed.openStageId;
		const selectedPrincipalsIds = candidatePrincipals.ids.filter((key, index) => candidatePrincipals[key].selected);
		if (selectedPrincipalsIds.length == 0) {
			console.log("returned ");
			return;
		}
		const data = selectedPrincipalsIds.map((key) => ({
			education_stage_id: educationStageId,
			user_id: candidatePrincipals[key].id,
		}));
		const payload = { data: data };
		const response = await schoolManagementAPI.addMultiplePrincipalsToStage(payload);
		if (!response.data.status) {
			dispatch({
				type: GET_SCHOOL_TREE_ERROR,
			});
			throw new Error(response.data.msg);
		}
		dispatch({
			type: ADD_PRINCAPLE_TO_STAGE,
			payload: { newIds: selectedPrincipalsIds },
		});
	} catch (error) {
		toast.error(error.message);
	}
};
export const removePrinipalsFromStage = () => async (dispatch, getState) => {
	try {
		const principals = getState().schoolManagementRed.principals;
		const educationStageId = getState().schoolManagementRed.openStageId;
		const selectedPrincipalsIds = principals.ids.filter((key, index) => principals[key].selected);
		if (selectedPrincipalsIds.length == 0) {
			console.log("returned ");
			return;
		}

		const payload = { user_ids: selectedPrincipalsIds };
		const response = await schoolManagementAPI.removePrincipalsFromStage(educationStageId, payload);
		if (!response.data.status) {
			dispatch({
				type: GET_SCHOOL_TREE_ERROR,
			});
			throw new Error(response.data.msg);
		}
		dispatch({
			type: DELETE_PRINCIPALS_FOR_STAGE,
			payload: { deletedIds: selectedPrincipalsIds },
		});
	} catch (error) {
		toast.error(error.message);
	}
};

export const addStudentsToGrade = () => async (dispatch, getState) => {
	try {
		const candidateStudents = getState().schoolManagementRed.candidateStudents;
		const gradeLevelId = getState().schoolManagementRed.openGradLevelId;
		const selectedStudentsIds = candidateStudents.ids.filter((key, index) => candidateStudents[key].selected);
		if (selectedStudentsIds.length == 0) {
			console.log("returned ");
			return;
		}

		const payload = { user_ids: selectedStudentsIds };
		const response = await schoolManagementAPI.addStudentsToGrade(gradeLevelId, payload);
		if (!response.data.status) {
			dispatch({
				type: GET_SCHOOL_TREE_ERROR,
			});
			throw new Error(response.data.msg);
		}
		dispatch({
			type: ADD_STUDENTS_TO_GRADE,
			payload: { newIds: selectedStudentsIds },
		});
	} catch (error) {
		toast.error(error.message);
	}
};

export const removeStudentsFromGrade = () => async (dispatch, getState) => {
	try {
		const students = getState().schoolManagementRed.students;
		const gradeLevelId = getState().schoolManagementRed.openGradLevelId;
		const selectedStudentsIds = students.ids.filter((key, index) => students[key].selected);
		if (selectedStudentsIds.length == 0) {
			console.log("returned ");
			return;
		}

		const payload = { user_ids: selectedStudentsIds };
		const response = await schoolManagementAPI.removeStudentsFromGrade(gradeLevelId, payload);
		if (!response.data.status) {
			dispatch({
				type: GET_SCHOOL_TREE_ERROR,
			});
			throw new Error(response.data.msg);
		}
		dispatch({
			type: DELETE_STUDENTS_FROM_GRADE,
			payload: { deletedIds: selectedStudentsIds },
		});
	} catch (error) {
		toast.error(error.message);
	}
};

export const getAllPeriods =
	(firstTime = true) =>
	async (dispatch) => {
		try {
			if (firstTime)
				dispatch({
					type: GETTING_PERIODS,
				});
			const payload = {
				payload: {
					TableName: "periods",
					Filters: [],
				},
			};
			const response = await schoolManagementAPI.getOrganizationPeriods(payload);
			if (!response.data.status) {
				dispatch({
					type: GET_SCHOOL_TREE_ERROR,
				});
				throw new Error(response.data.msg);
			}
			const data = response?.data?.data?.records?.data;
			const periodsIds = [];
			const periods = data.reduce((result, item) => {
				const { id } = item;
				result[id] = { ...item };
				result[id]["saved"] = true;
				result[id]["invalidateTime"] = false;
				result[id]["start_time"] = item.start_time.slice(0, 5);
				result[id]["end_time"] = item.end_time.slice(0, 5);
				result[id]["errors"] = {};
				result[id]["touched"] = { start_time: false, end_time: false, title: false };
				periodsIds.push(id);
				return result;
			}, {});
			periods["ids"] = periodsIds;
			dispatch({
				type: GET_ORGANIZATION_PERIODS,
				payload: { periods: periods },
			});
			if (firstTime)
				dispatch({
					type: FINISHING_PERIODS,
				});
		} catch (error) {
			toast.error(error.message);
			dispatch({
				type: FINISHING_PERIODS,
			});
		}
	};

export const togglePeriodSavedStatus = (id) => async (dispatch) => {
	dispatch({
		type: TOGGLE_PERIOD_SAVED_STATUS,
		payload: { id: id },
	});
};

export const editOrAddPeriod = (data) => async (dispatch) => {};

export const removePeriod = (id) => async (dispatch) => {
	try {
		if (id > 0) {
			const payload = {
				payload: {
					id: id,
					TableName: "periods",
				},
			};
			const response = await schoolManagementAPI.deletePeriod(payload);
			if (!response.data.status) {
				dispatch({
					type: GET_SCHOOL_TREE_ERROR,
				});
				throw new Error(response.data.msg);
			}
		}
		dispatch({
			type: DELETE_PERIOD,
			payload: { id: id },
		});
	} catch (error) {
		toast.error(error.message);
	}
};

export const getCurriculaContent = (curriculaId) => async (dispatch, getState) => {
	try {
		const curriculas = getState().schoolManagementRed.curriculas;
		const currentCurricula = curriculas[curriculaId];

		const masterCourses = JSON.parse(currentCurricula.master_courses);
		if (masterCourses?.length <= 0 || !masterCourses) {
			return;
		}
		dispatch({
			type: GETTING_CURRICULA_CONTENT,
		});
		const ids = masterCourses.reduce((result, id) => {
			result = result + id + ",";
			return result;
		}, "");
		const response = await schoolManagementAPI.getMultipleCourses(ids);
		if (!response.data.status) {
			dispatch({
				type: GET_SCHOOL_TREE_ERROR,
			});
			throw new Error(response.data.msg);
		}
		const data = response.data.data;
		const activeCoursesIds = [];
		const activeCourses = data.reduce((result, item) => {
			const { id } = item;
			result[id] = { ...item };
			activeCoursesIds.push(id);
			return result;
		}, {});
		activeCourses["ids"] = activeCoursesIds;
		dispatch({
			type: GET_CURRICULA_COURSES,
			payload: { activeCourses: activeCourses },
		});
		dispatch({
			type: FINISHING_CURRICULA_CONTENT,
		});
	} catch (error) {
		toast.error(error.message);
		dispatch({
			type: FINISHING_CURRICULA_CONTENT,
		});
	}
};
export const handleCourseClicked = (course) => async (dispatch, getState) => {
	try {
		const payload = { master_courses: [course.id] };
		const curriculaId = getState().schoolManagementRed.openCurriculaId;
		const response = await schoolManagementAPI.addToMasterCourses(curriculaId, payload);
		if (!response?.data?.status) {
			dispatch({
				type: GET_SCHOOL_TREE_ERROR,
			});
			throw new Error(response.data.msg);
		}
		const master_courses = response?.data?.data?.curriculum?.master_courses;
		dispatch({
			type: ADD_COURSE_TO_CURRICULA,
			payload: { newCourse: course, master_courses, curriculaId },
		});
	} catch (error) {
		toast.error(error.message);
	}
};

export const handleAddingCategory = (category, courses, coursesIds) => async (dispatch, getState) => {
	try {
		const gradeLevelId = getState().schoolManagementRed.openGradLevelId;
		const defaultMainCourseId = getState().schoolManagementRed.defaultMainCourseId;
		const defaultSemesterId = getState().schoolManagementRed.defaultSemesterId;
		const defaultSemesterName = getState().schoolManagementRed.defaultSemesterName;
		dispatch({
			type: CREATEING_CURRICULA_CATEGORY,
		});
		let payload = null;
		let curriculaId = null;
		payload = {
			name: category.label,
			grade_level_id: gradeLevelId,
			main_course_id: defaultMainCourseId,
			semester_id: defaultSemesterId,
			main_course: { value: defaultMainCourseId, label: defaultSemesterName },
		};
		const response = await schoolManagementAPI.addCurricula(payload);
		if (!response.data.status) throw new Error(response.data.msg);
		let payload2 = {};
		let response2 = null;
		let master_courses = "[]";
		curriculaId = response?.data.data.id;
		if (coursesIds.length > 0) {
			payload2 = { master_courses: coursesIds };
			response2 = await schoolManagementAPI.addToMasterCourses(curriculaId, payload2);
			if (!response2.data.status) throw new Error(response2.data.msg);
			master_courses = response2?.data?.data?.curriculum?.master_courses;
		}
		dispatch({ type: SAVE_CATEGORY_AS_CURRICULA, payload: { curriculaId, master_courses, courses, coursesIds, category, gradeLevelId } });
		dispatch({
			type: FINISHING_CURRICULA_CATEGORY,
		});
	} catch (error) {
		toast.error(error.message);
	}
};
export const sendNotificationToPrincipals = () => async (dispatch, getState) => {
	try {
		const principals = getState().schoolManagementRed.principals;
		const selectedPrincipalsIds = principals.ids.filter((key, index) => principals[key].selected);
		if (selectedPrincipalsIds.length == 0) {
			console.log("returned ");
			return;
		}
		const payload = {
			user_ids: selectedPrincipalsIds,
			title: "New Education Stage",
			body: "You have been assigned to education stage",
			table_name: "education_stage",
			row_id: 10,
		};
		const response = await schoolManagementAPI.sendNotificationToPrincipals(payload);
		if (!response?.data?.status) {
			dispatch({
				type: GET_SCHOOL_TREE_ERROR,
			});
			throw new Error(response.data.msg);
		}
		console.log("Success");
		return true;
	} catch (error) {
		toast.error(error.message);
		return false;
	}
};
export const sendNotificationToStudents = () => async (dispatch, getState) => {
	try {
		const students = getState().schoolManagementRed.students;
		const selectedStudentsIds = students.ids.filter((key, index) => students[key].selected);
		if (selectedStudentsIds.length == 0) {
			console.log("returned ");
			return;
		}
		const payload = {
			user_ids: selectedStudentsIds,
			title: "New grade level",
			body: "You have been assigned to a new gradel level",
			table_name: "grade_level",
			row_id: 10,
		};
		const response = await schoolManagementAPI.sendNotificationToStudents(payload);
		if (!response?.data?.status) {
			dispatch({
				type: GET_SCHOOL_TREE_ERROR,
			});
			throw new Error(response.data.msg);
		}
		console.log("Success");
		return true;
	} catch (error) {
		toast.error(error.message);
		return false;
	}
};

export const changePeriodInputValue = (periodId, newTitle) => (dispatch) => {
	dispatch({
		type: CHANGE_PERIOD_INPUT_VALUE,
		payload: { id: periodId, newTitle },
	});
};

export const addOrEditPeriod =
	(data, oldId = null) =>
	async (dispatch, getState) => {
		try {
			const maxNumber = data.number;
			if (oldId <= 0)
				getState().schoolManagementRed.periods.ids.map((period) => {
					if (period.number > maxNumber) {
						maxNumber = period.number;
					}
				});

			const payload = {
				payload: {
					TableName: "periods",
					record: {
						...data,
						number: oldId <= 0 ? maxNumber + 1 : data.number,
					},
				},
			};
			const response = await schoolManagementAPI.addOrganizationPeriod(payload);
			if (!response?.data?.status) {
				throw new Error(response.data.msg);
			}
			if (response) {
				// const newData = response?.data?.data?.models;
				// dispatch({
				// 	type: EDIT_PERIOD,
				// 	payload: { newData: newData, oldId: oldId ? oldId : newData.id },
				// });
				await dispatch(getAllPeriods(false));
				return true;
			}
		} catch (error) {
			toast.error(error.message);
			return false;
		}
	};

export const addNewPeriod = () => (dispatch) => {
	dispatch({
		type: ADD_NEW_PERIOD,
	});
};
export const changePeriodFromTime = (period, newTime, callback) => (dispatch) => {
	dispatch({
		type: CHANGE_PERIOD_FROM_TIME,
		payload: { id: period.id, newTime },
	});

	if (callback && typeof callback === "function") {
		callback();
	}
};
export const changePeriodToTime = (period, newTime) => (dispatch) => {
	dispatch({
		type: CHANGE_PERIOD_TO_TIME,
		payload: { id: period.id, newTime },
	});

	// if (callback && typeof callback === 'function') {
	//     callback();
	// }
};

export const getGradeScale = () => async (dispatch, getState) => {
	dispatch({
		type: GETTING_GRADE_SCALE,
	});
	const defaultGradeScaleId = getState().schoolManagementRed.defaultGradeScaleId;
	const payload = {
		payload: {
			masterTable: "grade_scales",
			masterId: defaultGradeScaleId,
			pivotTable: "grade_scale_details",
			detailTable: "grade_scale_details",
		},
	};

	const response = await schoolManagementAPI.getGradeScale(payload);
	try {
		if (!response?.data?.status) {
			throw new Error(response.data.msg);
		}
	} catch (error) {
		toast.error(error.message);
		dispatch({
			type: FINISHING_GRADE_SCALE,
		});
	}
	if (response) {
		const data = response?.data?.data?.records?.data;
		const gradeScaleIds = [];
		const gradeScale = data.reduce((result, item) => {
			const { id } = item;
			result[id] = { ...item };
			result[id]["saved"] = { letter: true, GPA: true, min: true, max: true };
			result[id]["touched"] = { letter: false, GPA: false, min: false, max: false };
			gradeScaleIds.push(id);
			return result;
		}, {});
		gradeScale["ids"] = gradeScaleIds;
		dispatch({
			type: SET_GRADE_SCALE,
			payload: { gradeScale: gradeScale },
		});
		dispatch({
			type: FINISHING_GRADE_SCALE,
		});
	}
};

export const editScaleValue = (data) => async (dispatch, getState) => {
	const defaultGradeScaleId = getState().schoolManagementRed.defaultGradeScaleId;

	const payload = {
		payload: {
			TableName: "grade_scale_details",
			record: { ...data, grade_scale_id: defaultGradeScaleId },
		},
	};
	const response = await schoolManagementAPI.addOrEditScaleDetail(payload);
	try {
		if (!response?.data?.status) {
			throw new Error(response.data.msg);
		}
	} catch (error) {
		toast.error(error.message);
		return false;
	}
	if (response) {
		return true;
	}
};

export const getAllAssessments = (gradeLevelId) => async (dispatch, getState) => {
	dispatch({
		type: GETTING_ASSESSMENTS,
	});
	const response = await schoolManagementAPI.getAssessments(gradeLevelId);
	try {
		if (!response?.data?.status) {
			throw new Error(response.data.msg);
		}
	} catch (error) {
		toast.error(error.message);
		dispatch({
			type: FINISHING_ASSESSMENTS,
		});
		return false;
	}
	if (response) {
		const data = response?.data?.data?.assessments;
		const assessmentsIds = [];
		const assessments = data.reduce((result, item) => {
			const { id } = item;
			result[id] = { ...item };
			result[id]["touched"] = { name: false, timestamp: false };
			result[id]["saved"] = true;
			assessmentsIds.push(id);
			return result;
		}, {});
		assessments["ids"] = assessmentsIds;
		dispatch({
			type: SET_GRADE_ASSESSMENTS,
			payload: { assessments },
		});
		dispatch({
			type: FINISHING_ASSESSMENTS,
		});
	}
};

export const addOrEditAssessment = (data) => async (dispatch, getState) => {
	const gradeLevelId = getState().schoolManagementRed.openGradLevelId;
	const payload = {
		...data,
		id: data.id <= 0 ? null : data.id,
		grade_level_id: gradeLevelId,
	};
	const response = await schoolManagementAPI.addOrEditAssessments(payload);
	try {
		if (!response?.data?.status) {
			throw new Error(response.data.msg);
		}
	} catch (error) {
		toast.error(error.message);
		return false;
	}
	const id = response?.data?.data?.id;
	dispatch({
		type: UPDATE_ASSESSMENT,
		payload: { id: id, oldId: data.id <= 0 ? data.id : null, otherData: data },
	});

	if (response) return true;
};

export const deleteAssessment = (assessment) => async (dispatch) => {
	if (assessment.id > 0) {
		const response = await schoolManagementAPI.deleteAssessment(assessment.id);
		try {
			if (!response?.data?.status) {
				throw new Error(response.data.msg);
			}
		} catch (error) {
			toast.error(error.message);
			return false;
		}
	}
	dispatch({
		type: DELETE_ASSESSMENT,
		payload: { id: assessment.id },
	});
};

export const addNewAssessment = () => (dispatch, getState) => {
	const openGradLevelId = getState().schoolManagementRed.openGradLevelId;
	dispatch({
		type: ADD_NEW_ASSESSMENT,
		payload: { gradeLevelId: openGradLevelId },
	});
};

export const createStudent = (data) => async (dispatch, getState) => {
	const gradeLevelId = getState().schoolManagementRed.openGradLevelId;
	dispatch(getCandidateStudentsForGrade(null, { gradeLevelId: gradeLevelId }));
	// const student = data?.data?.data?.record_details;
	// dispatch({
	// 	type: ADD_STUDENT_AFTER_CREATING,
	// 	payload: { student: student },
	// });
};

export const createPrincipale = (data) => async (dispatch, getState) => {
	const stageId = getState().schoolManagementRed.openStageId;
	dispatch(getCandidatePrincipalForEducation(null, { educationStageId: stageId }));
	// const principale = data?.data?.data?.record_details;
	// dispatch({
	// 	type: ADD_PRINCIAPLE_AFTER_CREATING,
	// 	payload: { principale: principale },
	// });
};

export const createAssessmentsEvents = () => async (dispatch, getState) => {
	const gradeLevelId = getState().schoolManagementRed.openGradLevelId;
	const response = await schoolManagementAPI.createAssessmentsEvents(gradeLevelId);
	try {
		if (!response?.data?.status) {
			throw new Error(response.data.msg);
		}
	} catch (error) {
		toast.error(error.message);
		return false;
	}
	if (response) {
	}
};
