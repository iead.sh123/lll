import { unitPlanApi } from "api/unitPlan";
import {
	GET_UNIT_PLAN_REQUEST,
	GET_UNIT_PLAN_SUCCESS,
	GET_UNIT_PLAN_ERROR,
	SHOW_UNIT_PLAN_EDITOR,
	SET_UNIT_PLAN_VALUE,
	ADD_SECTION_TO_UNIT_PLAN,
	ADD_DATA_TO_SECTION,
	ADD_ITEMS_TO_SECTION,
	REMOVE_SECTION,
	PICK_A_RUBRIC_TO_UNIT_PLAN_SECTION,
	PICK_A_LESSON_PLAN_TO_UNIT_PLAN_SECTION,
	REMOVE_RUBRIC_FROM_SECTION,
	REMOVE_LESSON_PLAN_FROM_SECTION,
	REMOVE_ATTACHMENT_FROM_SECTION,
	REMOVE_ITEM_FROM_SECTION,
	REMOVE_ATTACHMENT_FROM_ITEMS,
	SAVE_UNIT_PLAN_REQUEST,
	SAVE_UNIT_PLAN_SUCCESS,
	SAVE_UNIT_PLAN_ERROR,
	GET_VALIDATION_UNIT_PLAN,
	ADD_VALIDATION_TO_UNIT_PLAN,
	CREATE_UNIT_PLAN_TEMPLATE,
	EMPTY_STORE,
} from "./globalTypes";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

//-------------- Start UnitPlan Actions Service--------------

export const showUnitPlanEditorAsync = (value) => {
	return (dispatch) => {
		dispatch({
			type: SHOW_UNIT_PLAN_EDITOR,
			payload: { value },
		});
	};
};

export const initUnitPlan = () => {
	const unitPlan = {
		description: "",
		unit_number: "",
		title: "",
		time: "",
		uplsections: [],
		tags: [],
	};

	return (dispatch) => {
		dispatch({
			type: GET_UNIT_PLAN_SUCCESS,
			payload: { data: unitPlan },
		});
	};
};

export const getValidationUnitPlan = () => {
	return (dispatch) => {
		const initialValidationData = {
			description: "",
			unit_number: "",
			title: "",
			time: "",
			uplsections: [],
		};
		dispatch({
			type: GET_VALIDATION_UNIT_PLAN,
			payload: { initialValidationData },
		});
	};
};

export const addValidationToUnitPlan = ({ name, message, sectionId, itemId }) => {
	return (dispatch) => {
		dispatch({
			type: ADD_VALIDATION_TO_UNIT_PLAN,
			payload: { name, message, sectionId, itemId },
		});
	};
};

export const getUnitPlanByIdAsync = (unitPlanId) => async (dispatch) => {
	dispatch({ type: GET_UNIT_PLAN_REQUEST });
	try {
		const response = await unitPlanApi.getUnitPlanById(unitPlanId);

		if (response.data.status === 1) {
			let data = response.data.data;

			dispatch({
				type: GET_UNIT_PLAN_SUCCESS,
				payload: { data },
			});
			// dispatch(createUnitPlanTemplate(data));
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: GET_UNIT_PLAN_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: GET_UNIT_PLAN_ERROR,
		});
		toast.error(error?.message);
	}
};

export const getUnitPlanByIdToCreateTemplateAsync = (unitPlanId) => async (dispatch) => {
	dispatch({ type: GET_UNIT_PLAN_REQUEST });
	try {
		const response = await unitPlanApi.getUnitPlanById(unitPlanId);

		if (response.data.status === 1) {
			let data = response.data.data;

			dispatch({
				type: GET_UNIT_PLAN_SUCCESS,
				payload: { data },
			});
			dispatch(createUnitPlanTemplate(data));
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: GET_UNIT_PLAN_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: GET_UNIT_PLAN_ERROR,
		});
		toast.error(error?.message);
	}
};

export const setUnitPlanValue = (name, value) => {
	return (dispatch) => {
		dispatch({
			type: SET_UNIT_PLAN_VALUE,
			name: name,
			value: value,
		});
	};
};

export const addSectionToUnitPlan = (indexAdd) => {
	return (dispatch) => {
		const unitPlanSection = {
			fakeId: Math.random(10000),
			name: "",
			description: "",
			rubric: null,
			rubric_id: null,
			lesson_plan: {},
			lesson_plan_id: null,
			attachments: [],
			tags: [],
			items: [],
		};
		dispatch({
			type: ADD_SECTION_TO_UNIT_PLAN,
			payload: { unitPlanSection, indexAdd },
		});
	};
};

export const addDataToSection = (value, name, sectionId) => {
	return (dispatch) => {
		dispatch({
			type: ADD_DATA_TO_SECTION,
			payload: { value, name, sectionId },
		});
	};
};

export const addItemsToSection = (sectionId, itemId, name, value, type, visible) => {
	return (dispatch) => {
		const unitPlanItem = {
			fakeId: Math.random(10000),
			text: name == "text" ? value : "",
			rubric: {},
			rubric_id: null,
			lesson_plan: {},
			lesson_plan_id: null,
			attachments: [],
			tags: [],
			items: [],
			visible: visible ? visible : false,
		};

		dispatch({
			type: ADD_ITEMS_TO_SECTION,
			payload: { sectionId, itemId, name, value, type, unitPlanItem },
		});
	};
};

export const removeItemFromSection = (sectionId, itemId) => {
	return (dispatch) => {
		dispatch({
			type: REMOVE_ITEM_FROM_SECTION,
			payload: { sectionId, itemId },
		});
	};
};

export const removeSection = (sectionId) => {
	return (dispatch) => {
		dispatch({
			type: REMOVE_SECTION,
			payload: { sectionId },
		});
	};
};

export const pickARubricToUnitPlanSection = (rubricObj, sectionId) => {
	return (dispatch) => {
		dispatch({
			type: PICK_A_RUBRIC_TO_UNIT_PLAN_SECTION,
			payload: { rubricObj, sectionId },
		});
	};
};

export const pickALessonPlanToUnitPlanSection = (lessonPlanObj, sectionId) => {
	return (dispatch) => {
		dispatch({
			type: PICK_A_LESSON_PLAN_TO_UNIT_PLAN_SECTION,
			payload: { lessonPlanObj, sectionId },
		});
	};
};

export const removeRubricFromSection = (sectionId) => {
	return (dispatch) => {
		dispatch({
			type: REMOVE_RUBRIC_FROM_SECTION,
			payload: { sectionId },
		});
	};
};

export const removeLessonPlanFromSection = (sectionId) => {
	return (dispatch) => {
		dispatch({
			type: REMOVE_LESSON_PLAN_FROM_SECTION,
			payload: { sectionId },
		});
	};
};

export const removeAttachmentFromSection = (sectionId) => {
	return (dispatch) => {
		dispatch({
			type: REMOVE_ATTACHMENT_FROM_SECTION,
			payload: { sectionId },
		});
	};
};

export const removeAttachmentFromItems = (sectionId, itemId, type) => {
	return (dispatch) => {
		dispatch({
			type: REMOVE_ATTACHMENT_FROM_ITEMS,
			payload: { sectionId, itemId, type },
		});
	};
};

export const saveUnitPlanAsync = (data, history) => async (dispatch) => {
	dispatch({ type: SAVE_UNIT_PLAN_REQUEST, payload: { data } });
	try {
		const response = await unitPlanApi.saveUnitplan(data);

		if (response.data.status === 1) {
			dispatch({
				type: SAVE_UNIT_PLAN_SUCCESS,
				payload: { data },
			});
			dispatch(emptyUnitPlan());
			history.goBack();
		} else {
			dispatch({
				type: SAVE_UNIT_PLAN_ERROR,
				payload: { data },
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: SAVE_UNIT_PLAN_ERROR,
			payload: { data },
		});
		toast.error(error?.message);
	}
};

export const createUnitPlanTemplate = (data) => {
	return (dispatch) => {
		dispatch({
			type: CREATE_UNIT_PLAN_TEMPLATE,
			payload: { data },
		});
	};
};

export const emptyUnitPlan = () => {
	return (dispatch) => {
		dispatch({
			type: EMPTY_STORE,
			payload: { data: null },
		});
	};
};
//-------------- End UnitPlan Actions Service--------------
