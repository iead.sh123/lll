import {
	CHANGE_RUBRIC_STATUS_REQUEST,
	CHANGE_RUBRIC_STATUS_SUCCESS,
	CHANGE_RUBRIC_STATUS_ERROR,
	REMOVE_RUBRIC_REQUEST,
	REMOVE_RUBRIC_SUCCESS,
	REMOVE_RUBRIC_ERROR,
	EXPORT_RUBRIC_AS_PDF_REQUEST,
	EXPORT_RUBRIC_AS_PDF_SUCCESS,
	EXPORT_RUBRIC_AS_PDF_ERROR,
	GET_RUBRIC_BY_ID_REQUEST,
	GET_RUBRIC_BY_ID_SUCCESS,
	GET_RUBRIC_BY_ID_ERROR,
} from "./globalTypes";
import Swal, { SUCCESS, DANGER } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { rubricApi } from "api/global/rubric";
import ConvertBinaryToPdf from "utils/convertBinaryToPdf";
import { toast } from "react-toastify";

export const changeRubricStatusAsync = (rubricId, getDataFromBackend, specific_url, setTableData) => async (dispatch) => {
	dispatch({
		type: CHANGE_RUBRIC_STATUS_REQUEST,
	});

	try {
		const response = await rubricApi.changeRubricStatus(rubricId);

		if (response.data.status === 1) {
			dispatch({
				type: CHANGE_RUBRIC_STATUS_SUCCESS,
				payload: { rubricId },
			});
			const response = await getDataFromBackend(specific_url);
			setTableData(response);
		} else {
			dispatch({
				type: CHANGE_RUBRIC_STATUS_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: CHANGE_RUBRIC_STATUS_ERROR,
		});
		toast.error(error?.message);
	}
};

export const removeRubricAsync = (rubricId, getDataFromBackend, specific_url, setTableData) => async (dispatch) => {
	dispatch({
		type: REMOVE_RUBRIC_REQUEST,
	});

	try {
		const response = await rubricApi.removeRubric(rubricId);

		if (response.data.status === 1) {
			dispatch({
				type: REMOVE_RUBRIC_SUCCESS,
				payload: { rubricId },
			});
			const response = await getDataFromBackend(specific_url);
			setTableData(response);
		} else {
			dispatch({
				type: REMOVE_RUBRIC_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: REMOVE_RUBRIC_ERROR,
		});
		toast.error(error?.message);
	}
};

export const exportRubricAsPdfAsync = (rubricId) => async (dispatch) => {
	dispatch({
		type: EXPORT_RUBRIC_AS_PDF_REQUEST,
	});

	try {
		const response = await rubricApi.exportRubricAsPdf(rubricId);

		if (response.data.code !== 500) {
			const data = response.data;

			ConvertBinaryToPdf(data);

			dispatch({
				type: EXPORT_RUBRIC_AS_PDF_SUCCESS,
			});
		} else {
			dispatch({
				type: EXPORT_RUBRIC_AS_PDF_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: EXPORT_RUBRIC_AS_PDF_ERROR,
		});
		toast.error(error?.message);
	}
};

export const getRubricByIdAsync = (rubricId) => async (dispatch) => {
	dispatch({
		type: GET_RUBRIC_BY_ID_REQUEST,
	});

	try {
		const response = await rubricApi.getRubricById(rubricId);

		if (response.data.status === 1) {
			const data = response.data;

			dispatch({
				type: GET_RUBRIC_BY_ID_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: GET_RUBRIC_BY_ID_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: GET_RUBRIC_BY_ID_ERROR,
		});
		toast.error(error?.message);
	}
};
