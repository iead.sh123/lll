//------------------------------------Social Media
export const GET_POST_SERVICE_REQUEST = "GET_POST_SERVICE_REQUEST";
export const GET_POST_SERVICE_SUCCESS = "GET_POST_SERVICE_SUCCESS";
export const GET_POST_SERVICE_ERROR = "GET_POST_SERVICE_ERROR";
export const GET_SPECIFIC_POST_REQUEST = "GET_SPECIFIC_POST_REQUEST";
export const GET_SPECIFIC_POST_SUCCESS = "GET_SPECIFIC_POST_SUCCESS";
export const GET_SPECIFIC_POST_ERROR = "GET_SPECIFIC_POST_ERROR";
export const GET_POSTS_BY_USER_REQUEST = "GET_POSTS_BY_USER_REQUEST";
export const GET_POSTS_BY_USER_SUCCESS = "GET_POSTS_BY_USER_SUCCESS";
export const GET_POSTS_BY_USER_ERROR = "GET_POSTS_BY_USER_ERROR";
export const SEARCH_POSTS_REQUEST = "SEARCH_POSTS_REQUEST";
export const SEARCH_POSTS_SUCCESS = "SEARCH_POSTS_SUCCESS";
export const SEARCH_POSTS_ERROR = "SEARCH_POSTS_ERROR";
export const GET_ACCESS_LEVELS_BY_USERS_REQUEST =
  "GET_ACCESS_LEVELS_BY_USERS_REQUEST";
export const GET_ACCESS_LEVELS_BY_USERS_SUCCESS =
  "GET_ACCESS_LEVELS_BY_USERS_SUCCESS";
export const GET_ACCESS_LEVELS_BY_USERS_ERROR =
  "GET_ACCESS_LEVELS_BY_USERS_ERROR";
export const ADD_LIKE_TO_POST = "ADD_LIKE_TO_POST";
export const ADD_LIKE_TO_COMMENT = " ADD_LIKE_TO_COMMENT";
export const LOAD_MORE_TO_LIKES = " LOAD_MORE_TO_LIKES";
export const ADD_COMMENT_TO_POST = "ADD_COMMENT_TO_POST";
export const UPDATE_COMMENT_TO_POST = "UPDATE_COMMENT_TO_POST";
export const DELETE_COMMENT_TO_POST = "DELETE_COMMENT_TO_POST";
export const LOAD_MORE_TO_COMMENTS = "LOAD_MORE_TO_COMMENTS";
export const UPDATE_POST_SERVICE = "UPDATE_POST_SERVICE";
export const DELETE_POST_SERVICE = "DELETE_POST_SERVICE";
export const PROGRESS_BAR = "PROGRESS_BAR";
