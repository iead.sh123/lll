import {
	LOGIN_REQUEST,
	LOGIN_SUCCESS,
	LOGIN_ERROR,
	REGISTER_REQUEST,
	REGISTER_SUCCESS,
	REGISTER_ERROR,
	GET_USER_PHOTO,
	LOGOUT_REQUEST,
	LOGOUT_SUCCESS,
	LOGOUT_ERROR,
	REFRESH_TOKEN,
	SET_USER_PHOTO,
	UPDATE_PASSWORD_REQUEST,
	UPDATE_PASSWORD_SUCCESS,
	UPDATE_PASSWORD_ERROR,
	RESET_PASSWORD_REQUEST,
	RESET_PASSWORD_SUCCESS,
	RESET_PASSWORD_ERROR,
	CHANGE_PASSWORD_REQUEST,
	CHANGE_PASSWORD_SUCCESS,
	CHANGE_PASSWORD_ERROR,
	LOADING_FALSE,
	REFRESH_REQUEST,
	REFRESH_ERROR,
	REFRESH_SUCCESS,
	ME_SUCCESS,
	SWITCH_TYPE,
	ALL_ORGANIZATIONS_REQUEST,
	ALL_ORGANIZATIONS_SUCCESS,
	ALL_ORGANIZATIONS_ERROR,
	CHANGE_SELECTED_STUDENT,
} from "./globalTypes";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { authApi } from "api/auth";
import { persistor } from "store/index.js";
import { Services } from "engine/services";
import { post } from "config/api";
import { removeToken } from "views/auth/FireBaseServices";
import Helper from "components/Global/RComs/Helper";
import store from "store";
import { IMApi } from "api/IMNewCycle";
import { toast } from "react-toastify";

export const getAllOrganizationsAsync = () => async (dispatch) => {
	dispatch({
		type: ALL_ORGANIZATIONS_REQUEST,
	});

	try {
		const response = await authApi.allOrganizations();
		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: ALL_ORGANIZATIONS_SUCCESS,
				payload: { data },
			});
		} else {
			toast.error(response.data.msg);
			dispatch({
				type: ALL_ORGANIZATIONS_ERROR,
			});
		}
	} catch (error) {
		toast.error(error?.message);
		dispatch({
			type: ALL_ORGANIZATIONS_ERROR,
		});
	}
};

export const signUpAsync = (data, callback) => async (dispatch) => {
	dispatch({
		type: REGISTER_REQUEST,
	});

	// try {
	const response = await authApi.register(data);
	if (response.data.status == 1) {
		const rdata = response.data.data;
		dispatch({
			type: REGISTER_SUCCESS,
			payload: { rdata },
		});
		const credentials = { username: data?.email, password: data?.password };
		//-----------------------------------------------
		dispatch({
			type: LOGIN_REQUEST,
		});
		try {
			const response = await authApi.login(credentials);
			if (response.data.status === 1) {
				dispatch({
					type: LOGIN_SUCCESS,
					payload: response.data.data,
				});
				await me(callback, dispatch);
			} else {
				toast.error(tr`Incorrect UserName Or Password`);

				dispatch({
					type: LOGIN_ERROR,
					payload: response.data.msg,
				});
			}
		} catch (error) {
			toast.error(
				error.message == "Request failed with status code 401" ? tr("Incorrect UserName Or Password") : tr("Incorrect UserName Or Password")
			);

			dispatch({
				type: LOGIN_ERROR,
				payload: error.message,
			});
		}
	} else {
		toast.error(response.data.msg);
		dispatch({
			type: REGISTER_ERROR,
		});
	}
};

export const signIn = (credentials, callback) => async (dispatch) => {
	dispatch({
		type: LOGIN_REQUEST,
	});
	try {
		const response = await authApi.login(credentials);
		if (response.data.status === 1) {
			dispatch({
				type: LOGIN_SUCCESS,
				payload: response.data.data,
			});
			await me(callback, dispatch);
		} else {
			toast.error(tr`Incorrect Email Or Password`);
		}
	} catch (error) {
		toast.error(tr`Incorrect Email Or Password`);
		dispatch({
			type: LOGIN_ERROR,
			payload: error.message,
		});
	}
};
export const changeStudentId = (studentId) => async (dispatch) => {
	dispatch({
		type: CHANGE_SELECTED_STUDENT,
		payload: { studentId: studentId },
	});
};
const me = async (callback, dispatch) => {
	try {
		const response = await authApi.me();
		if (response.data.status === 1) {
			dispatch({
				type: ME_SUCCESS,
				payload: response.data.data,
			});
			//------------------------------------------
			response.data.data.user?.original?.image?.url &&
				dispatch({
					type: SET_USER_PHOTO,
					payload: response.data.data.user?.original?.image?.url,
				});
			callback(response.data.data);
		} else {
			toast.error(tr`Incorrect Email Or Password`);
		}
	} catch (error) {
		toast.error(tr`Incorrect Email Or Password`);
	}
};

export const refresh = (payload, callback) => async (dispatch) => {
	dispatch({
		type: REFRESH_REQUEST,
	});

	//try {
	const response = await post(Services.authentication.backend + "api/auth/refresh", payload);

	if (response?.data?.status === 1) {
		dispatch({
			type: REFRESH_SUCCESS,
			payload: response.data.data,
		});
		await me(callback, dispatch);
	} else {
		toast.error(response?.data?.msg);
		dispatch({
			type: LOGIN_ERROR,
			payload: response?.data?.msg,
		});
	}
};

export const switchAccountType = (type_id, organization_id) => async (dispatch) => {
	try {
		Helper.cl("switchAccountType ");
		Helper.fastPost(
			Services.auth_organization_management.backend + `api/auth/switch/`,
			{ type_id: type_id, organization_id: organization_id },
			"switched successfully",
			"error while switching",
			(data) => {
				Helper.cl(data.data.new_type, "fastPOst data.data");
				dispatch({
					type: SWITCH_TYPE,

					// current_type: payload.type_string,
					// current_type: payload.type_id,
					// current_organization: payload.organization_id,
					payload: {
						type_string: data.data.new_type.name,
						type_id: data.data.new_type.name,
						organization_id: data.data.new_type.current_organization,
					},
				});
				// alert(data.current_organization);
				Helper.cl("before Refresh");
				const x = refresh({ refresh_token: store.getState().auth.refresh_token }, () => {});
				Helper.cl("before hh");
				x(dispatch);
				Helper.cl("after Refresh");
			},
			() => {
				Helper.cl("error while switching");
			}
		);
	} catch (error) {
		Helper.cl(error, "switchAccountType error");
	}
};

export const loadingFalse = () => async (dispatch) => {
	dispatch({
		type: LOADING_FALSE,
	});
};

export const resetPassword = (req) => async (dispatch) => {
	dispatch({
		type: RESET_PASSWORD_REQUEST,
	});

	try {
		const response = await authApi.reset(req);

		if (response.data.status === 1) {
			dispatch({
				type: RESET_PASSWORD_SUCCESS,
			});
		} else {
			toast.error(response.data.msg);

			dispatch({
				type: RESET_PASSWORD_ERROR,
			});
		}
	} catch (error) {
		toast.error(error?.message);

		dispatch({
			type: RESET_PASSWORD_ERROR,
		});
	}
};

export const changePassword = (req, history) => async (dispatch) => {
	dispatch({
		type: CHANGE_PASSWORD_REQUEST,
	});
	try {
		const response = await authApi.change_password(req);

		if (response.data.status === 1) {
			dispatch({
				type: CHANGE_PASSWORD_SUCCESS,
			});

			history.push(`${process.env.REACT_APP_BASE_URL}/auth/loader`);
		} else {
			toast.error(response.data.msg);

			dispatch({
				type: CHANGE_PASSWORD_ERROR,
			});
		}
	} catch (error) {
		toast.error(error?.message);

		dispatch({
			type: CHANGE_PASSWORD_ERROR,
		});
	}
};

export const logout = (history) => async (dispatch) => {
	dispatch({ type: LOGOUT_REQUEST });
	IMApi.setOnline(0);
	removeToken();
	try {
		const response = await authApi.logout();
		if (response.data.status === 1) {
			dispatch({
				type: LOGOUT_SUCCESS,
			});

			if (history) {
				history.replace(process.env.REACT_APP_BASE_URL + "/auth/login");
				persistor.purge();
			}
		} else {
			persistor.purge();
			history.replace(process.env.REACT_APP_BASE_URL + "/auth/login");
			dispatch({ type: LOGOUT_ERROR });

			toast.error(response.data.msg);
		}
	} catch (error) {
		persistor.purge();
		history.replace(process.env.REACT_APP_BASE_URL + "/auth/login");
		dispatch({ type: LOGOUT_ERROR });
		toast.error(error?.message);
	}
};

// export const logout = (history) => async (dispatch) => {
// 	//

// 	// persistor.purge();
// 	// //location.reload();
// 	// window.location.replace(process.env.REACT_APP_BASE_URL + "/landing/home");
// 	try {
// 		//alert("logout")

// 		IMApi.setOnline(0);
// 		removeToken();
// 		//------------------------unregiester the applicatin of firebase

// 		// if ("serviceWorker" in navigator) {
// 		// 	Helper.cl("logout f1");
// 		// 	navigator.serviceWorker.getRegistrations().then((registrations) => {
// 		// 		Helper.cl("logout f2");
// 		// 		registrations.forEach((registration) => {
// 		// 			Helper.cl(registration, "logout f3 registration");
// 		// 			registration.unregister();
// 		// 		});
// 		// 	});
// 		// }

// 		//--------------------------------
// 		const response = await authApi.logout();

// 		if (response?.data?.status === 1) {
// 			dispatch({
// 				type: LOGOUT_SUCCESS,
// 			});

// 			persistor.purge();
// 			//location.reload();

// 			history.replace(process.env.REACT_APP_BASE_URL + "/auth/login")

// 			// window.location.replace(process.env.REACT_APP_BASE_URL + "/auth/login");
// 			//window.location.replace(process.env.REACT_APP_BASE_URL + "/landing/home");
// 		} else {
// 			persistor.purge();
// 			//location.reload();
// 			history.replace(process.env.REACT_APP_BASE_URL + "/auth/login")
// 			dispatch({ type: LOGOUT_ERROR, payload: response.data.msg });
// 		}
// 	} catch (error) {
// 		Helper.cl(error, "logout error");
// 		persistor.purge();
// 		//location.reload();
// 		history.replace(process.env.REACT_APP_BASE_URL + "/auth/login");
// 		dispatch({ type: LOGOUT_ERROR, payload: error.message });
// 	}
// };

export const UpdatePassword = (newPassword, history, user, setShow, errorConfirmPassword) => async (dispatch) => {
	dispatch({
		type: UPDATE_PASSWORD_REQUEST,
	});
	try {
		if (newPassword.old_password == "") {
			toast.error(tr`Old Password Is Required`);

			dispatch({
				type: UPDATE_PASSWORD_ERROR,
			});
		} else if (newPassword.new_password == "") {
			toast.error(tr`New Password Is Required`);

			dispatch({
				type: UPDATE_PASSWORD_ERROR,
			});
		} else if (newPassword.confirm_password == undefined || "") {
			toast.error(tr`Confirm Password Is Required`);

			dispatch({
				type: UPDATE_PASSWORD_ERROR,
			});
		} else if (newPassword.new_password !== newPassword.confirm_password) {
			toast.error(tr`The two passwords dose not match`);

			dispatch({
				type: UPDATE_PASSWORD_ERROR,
			});
		} else {
			const response = await authApi.updatePassword(newPassword);
			if (response?.data?.status === 1) {
				dispatch({
					type: UPDATE_PASSWORD_SUCCESS,
					payload: response?.data?.data,
				});
				// dispatch({
				//   type: REFRESH_TOKEN,
				//   payload: response.data.data,
				// });

				// if (response.data.data.user.original.password_changed !== 0) {
				if (user.password_changed === 0) {
					if (user?.type == "teacher") {
						history.push(`${process.env.REACT_APP_BASE_URL}/teacher/dashboard`);
					} else if (user?.type == "student") {
						history.push(`${process.env.REACT_APP_BASE_URL}/student`);
					} else if (user?.type == "seniorteacher" || user?.type == "principal" || user?.type == "school_advisor") {
						history.push(`${process.env.REACT_APP_BASE_URL}/senior-teacher/dashboard`);
					} else if (user?.type == "parent") {
						history.push(`${process.env.REACT_APP_BASE_URL}/parent/parenthome`);
					} else if (user?.type == "employee") {
						history.push(`${process.env.REACT_APP_BASE_URL}/admin/dashboard`);
					}
				} else {
					setShow(false);
				}
			} else {
				dispatch({
					type: UPDATE_PASSWORD_ERROR,
					payload: response.data.msg,
				});

				toast.error(response.data.msg);
			}
		}
	} catch (error) {
		dispatch({
			type: UPDATE_PASSWORD_ERROR,
		});
		toast.error(error?.message);
	}
};

export const GetUserPhoto = () => async (dispatch) => {
	try {
		const response = await authApi.getUserPhoto();
		if (response.data.status === 1) {
			dispatch({
				type: GET_USER_PHOTO,
				payload: response.data?.data?.length ? response.data?.data[0]?.url : null,
			});
		}
	} catch (error) {
		toast.error(error?.message);
	}
};

export const UpdateUserPhoto = (image) => async (dispatch) => {
	try {
		const response = await authApi.updateUserPhoto(image);
		if (response.data.status === 1) {
			const newResponse = await authApi.getUserPhoto();
			dispatch({
				type: "GET_USER_PHOTO",
				payload: newResponse.data?.data[0]?.url,
			});
			dispatch({
				type: SET_USER_PHOTO,
				payload: newResponse.data?.data[0]?.url,
			});
		}
	} catch (error) {
		toast.error(error?.message);
	}
};

export const DeleteUserPhoto = () => async (dispatch) => {
	try {
		const response = await authApi.deleteUserPhoto();

		if (response.data.status === 1) {
			dispatch({
				type: "GET_USER_PHOTO",
				payload: "",
			});
			dispatch({
				type: SET_USER_PHOTO,
				payload: "",
			});
		}
	} catch (error) {
		toast.error(error?.message);
	}
};
