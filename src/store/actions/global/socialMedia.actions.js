import {
	GET_POST_REQUEST,
	GET_POST_SUCCESS,
	GET_POST_ERROR,
	SEARCH_POSTS_REQUEST,
	SEARCH_POSTS_SUCCESS,
	SEARCH_POSTS_ERROR,
	GET_ACCESS_LEVELS_BY_USERS_REQUEST,
	GET_ACCESS_LEVELS_BY_USERS_SUCCESS,
	GET_ACCESS_LEVELS_BY_USERS_ERROR,
	ADD_LIKE,
	GET_SPECIFIC_POST,
	GET_POSTS_BY_USER,
	GET_ACCESS_LEVELS_BY_USERS,
	Social_Media_Pagination,
	SEARCH_POSTS,
	GET_Post,
	ADD_COMMENT,
	UPDATE_POST,
	DELETE_POST,
	UPDATE_COMMENT,
	DELETE_COMMENT,
	PROGRESS_BAR,
	LOAD_MORE_COMMENTS,
	ADD_LIKE_COMMENT,
} from "./globalTypes";

import { socialMediaApi } from "api/socialMedia";
import Swal, { SUCCESS, DANGER } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

export const getPost = (pageNumber) => async (dispatch) => {
	dispatch({ type: GET_POST_REQUEST });
	try {
		const response = await socialMediaApi.getAllPost(pageNumber);
		if (response.data.status === 1) {
			const LoadMore =
				response.data.data.posts.next_page_url &&
				response.data.data.posts.next_page_url.substr(+response.data.data.posts.next_page_url.indexOf("=") + 1);
			dispatch({
				type: GET_POST_SUCCESS,
				payload: {
					socialPosts: response.data.data.models.data,
					loadMorePage: LoadMore,
				},
			});
		} else {
			dispatch({ type: GET_POST_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_POST_ERROR });
		toast.error(error?.message);
	}
};

// export const getPost = () => {
//   return (dispatch) => {
//     socialMediaApi
//       .getAllPost()
//       .then((response) => {
//         if (response.data.status === 1) {
//           dispatch({
//             type: GET_Post,
//             authError: null,
//             social: response.data,
//           });
//         }
//       })
//       .catch((error) => {
//         toast.error(error?.message);
//       });
//   };
// };

export const searchByPost = (query, pageNumber) => async (dispatch) => {
	dispatch({ type: SEARCH_POSTS_REQUEST });
	try {
		const response = await socialMediaApi.searchPost(query, pageNumber);
		if (response.data.status === 1) {
			const LoadMore =
				response.data.data.models.next_page_url &&
				response.data.data.models.next_page_url.substr(+response.data.data.models.next_page_url.indexOf("=") + 1);

			dispatch({
				type: SEARCH_POSTS_SUCCESS,
				payload: {
					socialPosts: response.data.data.models.data,
					loadMorePage: LoadMore,
				},
			});
		} else {
			dispatch({ type: SEARCH_POSTS_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: SEARCH_POSTS_ERROR });
		toast.error(error?.message);
	}
};

// export const searchByPost = (query) => {
//   return (dispatch) => {
//     socialMediaApi
//       .searchPost(query)
//       .then((response) => {
//         if (response.data.status === 1) {
//           dispatch({
//             type: SEARCH_POSTS,
//             authError: null,
//             searchResult: response.data,
//           });
//         }
//       })
//       .catch((error) => {
//         toast.error(error?.message);
//       });
//   };
// };

export const addNewPost = (postData) => async (dispatch) => {
	dispatch({ type: ADD_NEW_POST_REQUEST });
	try {
		const response = await socialMediaApi.NewPost(postData);
		if (response.data.status === 1) {
			dispatch({
				type: ADD_NEW_POST_SUCCESS,
				payload: {
					newPost: response.data.data.models.data,
				},
			});
		} else {
			dispatch({ type: ADD_NEW_POST_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: ADD_NEW_POST_ERROR });
		toast.error(error?.message);
	}
};

// export const addNewPost = (postData) => {
//   return (dispatch) => {
//     dispatch(showLoading());
//     socialMediaApi
//       .NewPost(postData)
//       .then((response) => {
//         if (response.data.status == 1) {
//           dispatch(hideLoading());
//           dispatch(getPost());
//         } else {
//           toast.error(response.data.msg);
//         }
//       })
//       .catch((error) => {
//         toast.error(error?.message);
//       });
//   };
// };

export const writeComment = (commentData) => {
	return (dispatch) => {
		socialMediaApi
			.addComment(commentData)
			.then((response) => {
				if (response.data.status == 1) {
					dispatch({
						type: ADD_COMMENT,
						authError: null,
						addcomment: response.data,
					});
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const addLike = (likeData) => {
	return (dispatch) => {
		socialMediaApi
			.writeLike(likeData)
			.then((response) => {
				if (response.data.status == 1) {
					dispatch({
						type: likeData.comment ? ADD_LIKE_COMMENT : ADD_LIKE,
						authError: null,
						addLike: response.data,
						postId: likeData.postId,
						contentId: likeData.content_id,
					});
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const updatePost = (FormData, PostId) => {
	return (dispatch) => {
		socialMediaApi
			.editPost(FormData, PostId)
			.then((response) => {
				alert(response.data.status);
				if (response.data.status == 1) {
					dispatch({
						type: UPDATE_POST,
						authError: null,
						updatepost: response.data.data,
					});
					dispatch({
						type: PROGRESS_BAR,
						progressbar: 0,
					});
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const deletePost = (PostId) => {
	return (dispatch) => {
		socialMediaApi
			.removePost(PostId)
			.then((response) => {
				if (response.data.status == 1) {
					dispatch({
						type: DELETE_POST,
						authError: null,
						deletepost: response.data,
						PostId: PostId,
					});
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const updateComment = (FormData, CommentId, PostId) => {
	return (dispatch) => {
		socialMediaApi
			.editComment(FormData, CommentId)
			.then((response) => {
				if (response.data.status == 1) {
					dispatch({
						type: UPDATE_COMMENT,
						updatecomment: response.data.data,
						PostId: PostId,
					});
				} else {
					toast.error(response.data.msg);
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const deleteComment = (CommentId, PostId) => {
	return (dispatch) => {
		socialMediaApi
			.removeComment(CommentId)
			.then((response) => {
				if (response.data.status == 1) {
					dispatch({
						type: DELETE_COMMENT,
						authError: null,
						deletecomment: response.data,
						CommentId: CommentId,
						PostId: PostId,
					});
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const loadMoreComments = (postId, Next) => {
	return (dispatch) => {
		socialMediaApi
			.moreComments(postId, Next)
			.then((response) => {
				if (response.data.status === 1) {
					dispatch({
						type: LOAD_MORE_COMMENTS,
						authError: null,
						loadResult: response.data,
						postId: postId,
					});
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const loadMoreLikes = (contentId, Next, likeComment, postId) => {
	return (dispatch) => {
		socialMediaApi
			.moreLikes(contentId, Next)
			.then((response) => {
				if (response.data.status === 1) {
					dispatch({
						type: "LOAD_MORE_LIKES",
						authError: null,
						loadResult: response.data,
						likeComment: likeComment,
						postId: postId,
						contentId: contentId,
					});
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const getAccessLevels = (text) => async (dispatch) => {
	dispatch({ type: GET_ACCESS_LEVELS_BY_USERS_REQUEST });
	try {
		const response = await socialMediaApi.AccessLevels(text);
		if (response.data.status === 1) {
			dispatch({
				type: GET_ACCESS_LEVELS_BY_USERS_SUCCESS,
				payload: {
					accesslevels: response.data.data.models,
				},
			});
		} else {
			dispatch({ type: GET_ACCESS_LEVELS_BY_USERS_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_ACCESS_LEVELS_BY_USERS_ERROR });
		toast.error(error?.message);
	}
};

// export const getAccessLevels = (text) => {
//   return (dispatch) => {
//     socialMediaApi
//       .AccessLevels(text)
//       .then((response) => {
//         if (response.data.status === 1) {
//           dispatch({
//             type: GET_ACCESS_LEVELS_BY_USERS,
//             authError: null,
//             accesslevels: response.data,
//           });
//         }
//       })
//       .catch((error) => {
//         toast.error(error?.message);
//       });
//   };
// };

export const socialMediaPagination = (UrlPage, userId, query) => {
	let url = "";
	if (userId) {
		url = UrlPage + "&user_id=" + userId;
	} else if (query) {
		url = UrlPage + "&query=" + query;
	} else {
		url = UrlPage;
	}
	const type = userId ? "postByUser" : "allPosts";
	return (dispatch) => {
		socialMediaApi
			.pagenation(url)
			.then((response) => {
				if (response.data.status === 1) {
					dispatch({
						type: Social_Media_Pagination,
						authError: null,
						social: response.data,
						typeAction: type,
					});
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

// to empty state from post old
export const emptySpecificPost = () => {
	return (dispatch) => {
		dispatch({
			type: GET_SPECIFIC_POST,
			authError: null,
			specificpost: [],
		});
	};
};
//empty state posts for all users
export const emptyPostsByUser = () => {
	return (dispatch) => {
		dispatch({
			type: GET_POSTS_BY_USER,
			authError: null,
			postsbyuser: [],
		});
	};
};

export const getSpecificPost = (urlPostId) => {
	return (dispatch) => {
		socialMediaApi
			.specificpost(urlPostId)
			.then((response) => {
				if (response.data.status === 1) {
					dispatch({
						type: GET_SPECIFIC_POST,
						authError: null,
						specificpost: response.data,
					});
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const filteringByTag = (tag) => {
	return (dispatch) => {
		socialMediaApi
			.filtringTag(tag)
			.then((response) => {
				if (response.data.status === 1) {
					dispatch({
						type: GET_POSTS_BY_USER,
						authError: null,
						postsbyuser: response.data,
					});
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};

export const getPostsByUser = (UserId) => {
	return (dispatch) => {
		socialMediaApi
			.postByUser(UserId)
			.then((response) => {
				if (response.data.status === 1) {
					dispatch({
						type: GET_POSTS_BY_USER,
						authError: null,
						postsbyuser: response.data,
					});
				}
			})
			.catch((error) => {
				toast.error(error?.message);
			});
	};
};
