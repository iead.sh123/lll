import { questionSetApi } from "api/global/questionSet";
import {
	INITIATE_QUESTION_SET_FORMATE,
	FILL_QUESTION_SET_INFO,
	CREATE_QUESTION_SET_GROUP,
	CREATE_NEW_QUESTION,
	REMOVE_QUESTION_GROUP,
	REMOVE_QUESTION_TYPE,
	FILL_DATA_TO_QUESTION_TYPE,
	CREATE_QUESTION_SET_REQUEST,
	CREATE_QUESTION_SET_SUCCESS,
	CREATE_QUESTION_SET_ERROR,
	ADD_CHOICES_TO_QUESTION,
	FILL_DATA_IN_CHOICES_QUESTIONS,
	REMOVE_CHOICE_FROM_QUESTION,
	FILL_DATA_IN_CORRECT_ANSWER_TO_SOME_QUESTIONS,
	ADD_PAIRS_TO_MATCH_QUESTION,
	ADD_BEHAVIOR_TO_QUESTION_SET_FROM_MODULE_CONTENT_REQUEST,
	ADD_BEHAVIOR_TO_QUESTION_SET_FROM_MODULE_CONTENT_SUCCESS,
	ADD_BEHAVIOR_TO_QUESTION_SET_FROM_MODULE_CONTENT_ERROR,
	CALCULATE_POINTS,
	ALL_QUESTION_SET_REQUEST,
	ALL_QUESTION_SET_SUCCESS,
	ALL_QUESTION_SET_ERROR,
	PUBLISH_AND_UN_PUBLISH_QUESTION_SET_REQUEST,
	PUBLISH_AND_UN_PUBLISH_QUESTION_SET_SUCCESS,
	PUBLISH_AND_UN_PUBLISH_QUESTION_SET_ERROR,
	REMOVE_QUESTION_SET_REQUEST,
	REMOVE_QUESTION_SET_SUCCESS,
	REMOVE_QUESTION_SET_ERROR,
	QUESTION_SET_BY_ID_REQUEST,
	QUESTION_SET_BY_ID_SUCCESS,
	QUESTION_SET_BY_ID_ERROR,
	UPDATE_QUESTION_SET_REQUEST,
	UPDATE_QUESTION_SET_SUCCESS,
	UPDATE_QUESTION_SET_ERROR,
	SUBMISSIONS_QUESTION_SET_REQUEST,
	SUBMISSIONS_QUESTION_SET_SUCCESS,
	SUBMISSIONS_QUESTION_SET_ERROR,
	DELETE_SUBMISSION_REQUEST,
	DELETE_SUBMISSION_SUCCESS,
	DELETE_SUBMISSION_ERROR,
	GET_QUESTIONS_TO_SOLVE_REQUEST,
	GET_QUESTIONS_TO_SOLVE_SUCCESS,
	GET_QUESTIONS_TO_SOLVE_ERROR,
	GET_LEARNER_SOLUTION_TO_SOLVE_REQUEST,
	GET_LEARNER_SOLUTION_TO_SOLVE_SUCCESS,
	GET_LEARNER_SOLUTION_TO_SOLVE_ERROR,
	GET_QUESTIONS_TO_REVIEW_REQUEST,
	GET_QUESTIONS_TO_REVIEW_SUCCESS,
	GET_QUESTIONS_TO_REVIEW_ERROR,
	SAVE_LEARNER_SOLUTION_REQUEST,
	SAVE_LEARNER_SOLUTION_SUCCESS,
	SAVE_LEARNER_SOLUTION_ERROR,
	INITIATE_SOLVE_QUESTIONS,
	FILL_DATA_TO_SOLVE,
	CONFIRM_THE_SOLVE_TO_THE_QUESTION,
	RESET_SOLVE_QUESTION,
	SOLVED_PERCENTAGE,
	INITIATE_QUESTION_SET_CORRECTION,
	ADD_FEEDBACK_TO_QUESTION,
	CALC_QUESTIONS_MARK,
	ADD_TEACHER_FEEDBACK_TO_QS,
	CORRECTION_OF_QUESTIONS_REQUEST,
	CORRECTION_OF_QUESTIONS_SUCCESS,
	CORRECTION_OF_QUESTIONS_ERROR,
	FILL_DATA_TO_SOLVE_IN_ORDERING_QUESTION,
	CREATE_NEW_QUESTION_GROUP,
	ADD_DATA_TO_QUESTIONS_GROUP,
	CREATE_NEW_QUESTIONS_TO_QUESTION_GROUP,
	CHANGE_ALL_REQUIRED_ANSWER_TO_FALSE,
	QUESTIONS_BANK_REQUEST,
	QUESTIONS_BANK_SUCCESS,
	QUESTIONS_BANK_ERROR,
	SET_FILTERS_DATA,
	LEARNER_CONTEXT_CREATOR_REQUEST,
	LEARNER_CONTEXT_CREATOR_SUCCESS,
	LEARNER_CONTEXT_CREATOR_ERROR,
	EXPORT_QUESTION_BANK_TO_QUESTION_EDITOR,
	REMOVE_ORDERING_QUESTION,
	GET_QUESTIONS_TO_REVIEW_TEACHER_REQUEST,
	GET_QUESTIONS_TO_REVIEW_TEACHER_SUCCESS,
	GET_QUESTIONS_TO_REVIEW_TEACHER_ERROR,
	EMPTY_QUESTIONS_FROM_QUESTION_SET,
	CREATE_QUESTION_TO_QUESTIONS_BANK_REQUEST,
	CREATE_QUESTION_TO_QUESTIONS_BANK_SUCCESS,
	CREATE_QUESTION_TO_QUESTIONS_BANK_ERROR,
	FILL_DATA_IN_QUESTION_SET_TO_EDIT,
	UPDATE_QUESTIONS_BANK_REQUEST,
	UPDATE_QUESTIONS_BANK_SUCCESS,
	UPDATE_QUESTIONS_BANK_ERROR,
	REMOVE_QUESTIONS_BANK_REQUEST,
	REMOVE_QUESTIONS_BANK_SUCCESS,
	REMOVE_QUESTIONS_BANK_ERROR,
	ALL_QUESTION_SET_FOR_COURSE_REQUEST,
	ALL_QUESTION_SET_FOR_COURSE_SUCCESS,
	ALL_QUESTION_SET_FOR_COURSE_ERROR,
	PICK_QUESTION_SET_TO_MODULE_REQUEST,
	PICK_QUESTION_SET_TO_MODULE_SUCCESS,
	PICK_QUESTION_SET_TO_MODULE_ERROR,
} from "./globalTypes";
import Swal, { DANGER } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { handleShouldNotBeAddedModuleContent } from "./coursesManager.action";
import { coursesManagerApi } from "api/global/coursesManager";
import { pushDataToCourseModule } from "./coursesManager.action";
import { toast } from "react-toastify";

const idsGenerator = (function* () {
	let i = 1;
	while (true) yield i++;
})();
const idsOrderGenerator = (function* () {
	let i = 1;
	while (true) yield i++;
})();

export const addBehaviorToQuestionSetFromModuleContentAsync =
	(moduleId, moduleContentId, questionSetType, contentId, data) => async (dispatch) => {
		dispatch({
			type: ADD_BEHAVIOR_TO_QUESTION_SET_FROM_MODULE_CONTENT_REQUEST,
		});

		try {
			const response = await questionSetApi.addBehaviorToQuestionSetFromModuleContent(questionSetType, contentId, data);

			if (response.data.status === 1) {
				const newResponse = response.data.data;
				dispatch({
					type: ADD_BEHAVIOR_TO_QUESTION_SET_FROM_MODULE_CONTENT_SUCCESS,
					payload: { moduleId, moduleContentId, newResponse },
				});
			} else {
				dispatch({
					type: ADD_BEHAVIOR_TO_QUESTION_SET_FROM_MODULE_CONTENT_ERROR,
				});
				toast.error(response.data.msg);
			}
		} catch (error) {
			dispatch({
				type: ADD_BEHAVIOR_TO_QUESTION_SET_FROM_MODULE_CONTENT_ERROR,
			});
			toast.error(error?.message);
		}
	};

export const createQuestionSetAsync = (questionSetType, questionSetData, moduleId, history) => async (dispatch) => {
	dispatch({ type: CREATE_QUESTION_SET_REQUEST });

	try {
		let response;
		if (moduleId) {
			response = await questionSetApi.createQuestionSetToModule(moduleId, questionSetType, questionSetData);
		} else {
			response = await questionSetApi.createQuestionSet(questionSetType, questionSetData);
		}

		if (response.data.status === 1) {
			dispatch({
				type: CREATE_QUESTION_SET_SUCCESS,
			});
			history.goBack();
			if (moduleId) {
				dispatch(handleShouldNotBeAddedModuleContent(false));
			}
		} else {
			dispatch({ type: CREATE_QUESTION_SET_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: CREATE_QUESTION_SET_ERROR,
		});
		toast.error(error?.message);
	}
};
export const questionSetByIdAsync = (questionSetType, questionSetId) => async (dispatch) => {
	dispatch({ type: QUESTION_SET_BY_ID_REQUEST });

	try {
		const response = await questionSetApi.questionSetById(questionSetType, questionSetId);

		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: QUESTION_SET_BY_ID_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: QUESTION_SET_BY_ID_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: QUESTION_SET_BY_ID_ERROR,
		});
		toast.error(error?.message);
	}
};

export const updateQuestionSetAsync = (questionSetType, questionSetId, questionSetData, history) => async (dispatch) => {
	dispatch({ type: UPDATE_QUESTION_SET_REQUEST });

	try {
		const response = await questionSetApi.updateQuestionSet(questionSetType, questionSetId, questionSetData);

		if (response.data.status === 1) {
			dispatch({
				type: UPDATE_QUESTION_SET_SUCCESS,
			});
			history.goBack();
		} else {
			dispatch({ type: UPDATE_QUESTION_SET_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: UPDATE_QUESTION_SET_ERROR,
		});
		toast.error(error?.message);
	}
};

export const deleteQuestionSetAsync = (questionSetType, questionSetId, handleCloseModal) => async (dispatch) => {
	dispatch({ type: REMOVE_QUESTION_SET_REQUEST });

	try {
		const response = await questionSetApi.deleteQuestionSet(questionSetType, questionSetId);

		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: REMOVE_QUESTION_SET_SUCCESS,
				payload: { data, questionSetId },
			});
			if (handleCloseModal) {
				handleCloseModal();
			}
		} else {
			dispatch({ type: REMOVE_QUESTION_SET_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: REMOVE_QUESTION_SET_ERROR,
		});
		toast.error(error?.message);
	}
};

export const submissionsQuestionSetAsync = (questionSetType, moduleContextId, searchData, graded) => async (dispatch) => {
	dispatch({ type: SUBMISSIONS_QUESTION_SET_REQUEST });

	try {
		const response = await questionSetApi.submissionsQuestionSet(questionSetType, moduleContextId, searchData, graded);

		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: SUBMISSIONS_QUESTION_SET_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: SUBMISSIONS_QUESTION_SET_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: SUBMISSIONS_QUESTION_SET_ERROR,
		});
		toast.error(error?.message);
	}
};
export const removeSubmissionAsync = (questionSetType, questionSetId) => async (dispatch) => {
	dispatch({ type: DELETE_SUBMISSION_REQUEST });

	try {
		const response = await questionSetApi.deleteSubmission(questionSetType, questionSetId);

		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: DELETE_SUBMISSION_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: DELETE_SUBMISSION_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: DELETE_SUBMISSION_ERROR,
		});
		toast.error(error?.message);
	}
};
export const initiateQuestionSetFormate = (courseId, curriculumId) => async (dispatch) => {
	const questionSetFormate = {
		name: "",
		description: "",
		files: [],
		tags: [],
		points: 1,
		curriculumId: curriculumId,
		courseId: courseId,
		groups: [],
	};
	dispatch({
		type: INITIATE_QUESTION_SET_FORMATE,
		payload: { questionSetFormate },
	});
};

export const fillQuestionSetInfo = (key, value) => async (dispatch) => {
	dispatch({
		type: FILL_QUESTION_SET_INFO,
		payload: { key, value },
	});
};

export const createNewQuestion = (data, questionSetIndex) => async (dispatch) => {
	let newFormate = {};
	if (data.name == "MULTIPLE_CHOICES_MULTIPLE_ANSWERS" || data.name == "MULTIPLE_CHOICES_SINGLE_ANSWER") {
		newFormate = {
			fakeId: Math.random(),
			questions: [
				{
					fakeId: Math.random(),
					type: data.name,
					text: "",
					points: 1,
					correctAnswer: "",
					order: 0,
					files: [],
					choices: [
						{
							fakeId: Math.random(),
							order: 1,
							text: "",
							files: [],
						},
						{
							fakeId: Math.random(),
							order: 2,
							text: "",
							files: [],
						},
					],
				},
			],
		};
	} else if (data.name == "TRUEFALSE") {
		newFormate = {
			fakeId: Math.random(),
			questions: [
				{
					fakeId: Math.random(),
					type: data.name,
					text: "",
					points: 1,
					correctAnswer: "",
					order: 0,
					files: [],
					trueFalseStatement: [
						{
							fakeId: Math.random(),
							order: 1,
							text: "true",
							files: [],
						},
						{
							fakeId: Math.random(),
							order: 2,
							text: "false",
							files: [],
						},
					],
				},
			],
		};
	} else if (data.name == "MATCH") {
		newFormate = {
			fakeId: idsGenerator.next().value,
			questions: [
				{
					fakeId: idsGenerator.next().value,
					type: data.name,
					text: "",
					points: 1,
					correctAnswer: "1:2,3:4",
					order: 0,
					files: [],
					pairs: [
						{
							fakeId: idsGenerator.next().value,
							order: idsOrderGenerator.next().value,
							text: "",
							files: [],
							isRight: true,
						},
						{
							fakeId: idsGenerator.next().value,
							order: idsOrderGenerator.next().value,
							text: "",
							files: [],
							isRight: false,
						},
						{
							fakeId: idsGenerator.next().value,
							order: idsOrderGenerator.next().value,
							text: "",
							files: [],
							isRight: true,
						},
						{
							fakeId: idsGenerator.next().value,
							order: idsOrderGenerator.next().value,
							text: "",
							files: [],
							isRight: false,
						},
					],
				},
			],
		};
	} else {
		newFormate = {
			fakeId: Math.random(),
			questions: [
				{
					fakeId: Math.random(),
					type: data.name,
					text: "",
					points: 1,
					correctAnswer: "",
					order: 0,
					files: [],
					pairs: [],
					sequencingStatements: [],
				},
			],
		};
	}
	dispatch({
		type: CREATE_NEW_QUESTION,
		payload: { newFormate, questionSetIndex },
	});
};

export const createNewQuestionGroup = (questionSetIndex, question) => async (dispatch) => {
	// #PS : Here i pass question in params because i reuse create question group actions in question bank editor ,
	// same functionality between tow editor but in question bank editor we need to send first object from group
	// response question {icon:'' , label:'group' , value:'GROUP'}
	let FID = Math.random();
	let newFormate = {
		fakeId: FID,
		isGroup: question ? (question?.value == "GROUP" ? true : false) : true,
		title: "",
		description: "",
		points: 1,
		requiredQuestionsToAnswer: 0,
		tags: [],
		questions: [],
	};
	dispatch({
		type: CREATE_NEW_QUESTION_GROUP,
		payload: { newFormate, questionSetIndex },
	});

	// To Question Bank Editor
	if (newFormate && question && question.value !== "GROUP") {
		dispatch(createNewQuestionsToQuestionGroup(question, null, +FID));
	}
};

export const createNewQuestionsToQuestionGroup = (data, questionSetIndex, groupId) => async (dispatch) => {
	let newFormate = {};
	if (["MULTIPLE_CHOICES_MULTIPLE_ANSWERS", "MULTIPLE_CHOICES_SINGLE_ANSWER"].includes(data.name || data.value)) {
		newFormate = {
			fakeId: Math.random(),
			type: data.name || data.value,
			text: "",
			points: 1,
			correctAnswer: "",
			order: 0,
			files: [],
			choices: [
				{
					fakeId: Math.random(),
					order: 1,
					text: "",
					files: [],
				},
				{
					fakeId: Math.random(),
					order: 2,
					text: "",
					files: [],
				},
			],
		};
	} else if (data.name == "TRUEFALSE" || data.value == "TRUEFALSE") {
		newFormate = {
			fakeId: Math.random(),
			type: data.name || data.value,
			text: "",
			points: 1,
			correctAnswer: "",
			order: 0,
			files: [],
			trueFalseStatement: [
				{
					fakeId: Math.random(),
					order: 1,
					text: "true",
					files: [],
				},
				{
					fakeId: Math.random(),
					order: 2,
					text: "false",
					files: [],
				},
			],
		};
	} else if (data.name == "MATCH" || data.value == "MATCH") {
		newFormate = {
			fakeId: idsGenerator.next().value,
			type: data.name || data.value,
			text: "",
			points: 1,
			correctAnswer: "1:2,3:4",
			order: 0,
			files: [],
			pairs: [
				{
					fakeId: idsGenerator.next().value,
					order: idsOrderGenerator.next().value,
					text: "",
					files: [],
					isRight: true,
				},
				{
					fakeId: idsGenerator.next().value,
					order: idsOrderGenerator.next().value,
					text: "",
					files: [],
					isRight: false,
				},
				{
					fakeId: idsGenerator.next().value,
					order: idsOrderGenerator.next().value,
					text: "",
					files: [],
					isRight: true,
				},
				{
					fakeId: idsGenerator.next().value,
					order: idsOrderGenerator.next().value,
					text: "",
					files: [],
					isRight: false,
				},
			],
		};
	} else {
		newFormate = {
			fakeId: Math.random(),
			type: data.name || data.value,
			text: "",
			points: 1,
			correctAnswer: "",
			order: 0,
			files: [],
			pairs: [],
			sequencingStatements: [],
		};
	}

	dispatch({
		type: CREATE_NEW_QUESTIONS_TO_QUESTION_GROUP,
		payload: { newFormate, questionSetIndex, groupId },
	});
};

export const removeQuestionGroup = (questionGroupId, questionTypeId, isGroup) => async (dispatch) => {
	dispatch({
		type: REMOVE_QUESTION_GROUP,
		payload: { questionGroupId, questionTypeId, isGroup },
	});
};

export const removeQuestionType = (questionGroupId, questionTypeId) => async (dispatch) => {
	dispatch({
		type: REMOVE_QUESTION_TYPE,
		payload: { questionGroupId, questionTypeId },
	});
};

export const fillDataToQuestionType = (questionGroupId, questionTypeId, key, value) => async (dispatch) => {
	dispatch({
		type: FILL_DATA_TO_QUESTION_TYPE,
		payload: { questionGroupId, questionTypeId, key, value },
	});
};

export const addChoicesToQuestion = (questionGroupId, questionTypeId, choicesLength, questionType) => async (dispatch) => {
	let obj = {
		fakeId: Math.random(),
		text: "",
		order: choicesLength + 1,
		files: [],
	};
	dispatch({
		type: ADD_CHOICES_TO_QUESTION,
		payload: { questionGroupId, questionTypeId, obj, questionType },
	});
};

export const fillDataInChoicesAnswers = (questionGroupId, questionTypeId, choiceId, key, value, questionType) => async (dispatch) => {
	dispatch({
		type: FILL_DATA_IN_CHOICES_QUESTIONS,
		payload: {
			questionGroupId,
			questionTypeId,
			choiceId,
			key,
			value,
			questionType,
		},
	});
};

export const removeChoiceFromQuestion = (questionGroupId, questionTypeId, choiceId, questionType, choiceOrder) => async (dispatch) => {
	dispatch({
		type: REMOVE_CHOICE_FROM_QUESTION,
		payload: {
			questionGroupId,
			questionTypeId,
			choiceId,
			questionType,
			choiceOrder,
		},
	});
};

export const fillDataInCorrectAnswerToSomeQuestions = (questionGroupId, questionTypeId, questionType, value) => async (dispatch) => {
	dispatch({
		type: FILL_DATA_IN_CORRECT_ANSWER_TO_SOME_QUESTIONS,
		payload: {
			questionGroupId,
			questionTypeId,
			questionType,
			value,
		},
	});
};

export const addPairsToMatchQuestion = (questionGroupId, questionTypeId, parisLength) => async (dispatch) => {
	let newFormate = [
		{
			fakeId: idsGenerator.next().value,
			order: idsOrderGenerator.next().value,
			text: "",
			files: [],
			isRight: true,
		},
		{
			fakeId: idsGenerator.next().value,
			order: idsOrderGenerator.next().value,
			text: "",
			files: [],
			isRight: false,
		},
	];

	dispatch({
		type: ADD_PAIRS_TO_MATCH_QUESTION,
		payload: { questionGroupId, questionTypeId, newFormate },
	});
};

export const calculatePoints = () => async (dispatch) => {
	dispatch({
		type: CALCULATE_POINTS,
	});
};

export const allQuestionSetAsync = (questionSetType, courseId, curriculumId, searchData, filterTab) => async (dispatch) => {
	dispatch({
		type: ALL_QUESTION_SET_REQUEST,
	});

	try {
		const response = courseId
			? await questionSetApi.allQuestionSet(questionSetType, courseId, curriculumId, searchData, filterTab)
			: await questionSetApi.allQuestionSetByCurriculum(questionSetType, curriculumId, searchData);

		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: ALL_QUESTION_SET_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({
				type: ALL_QUESTION_SET_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: ALL_QUESTION_SET_ERROR,
		});
		toast.error(error?.message);
	}
};

export const publishAndUnPublishQuestionSetAsync = (questionSetType, questionSetId, publish) => async (dispatch) => {
	dispatch({ type: PUBLISH_AND_UN_PUBLISH_QUESTION_SET_REQUEST });
	try {
		const response = publish
			? await questionSetApi.unPublishQuestionSet(questionSetType, questionSetId)
			: await questionSetApi.publishQuestionSet(questionSetType, questionSetId);

		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: PUBLISH_AND_UN_PUBLISH_QUESTION_SET_SUCCESS,
				payload: { questionSetId, publish, data },
			});
		} else {
			dispatch({ type: PUBLISH_AND_UN_PUBLISH_QUESTION_SET_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: PUBLISH_AND_UN_PUBLISH_QUESTION_SET_ERROR });
		toast.error(error?.message);
	}
};

export const removeQuestionSetAsync = (moduleContentId) => async (dispatch) => {
	dispatch({ type: PUBLISH_AND_UN_PUBLISH_QUESTION_SET_REQUEST });
	try {
		const response = await coursesManagerApi.unPublishModuleContent(moduleContentId);

		if (response.data.status === 1) {
			dispatch({
				type: PUBLISH_AND_UN_PUBLISH_QUESTION_SET_SUCCESS,
				payload: { moduleContentId, publish },
			});
		} else {
			dispatch({ type: PUBLISH_AND_UN_PUBLISH_QUESTION_SET_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: PUBLISH_AND_UN_PUBLISH_QUESTION_SET_ERROR });
		toast.error(error?.message);
	}
};

//to solve question by student
export const getQuestionsToSolve = (questionSetType, moduleContextId) => async (dispatch) => {
	dispatch({ type: GET_QUESTIONS_TO_SOLVE_REQUEST });
	try {
		const response = await questionSetApi.getQuestionsToSolve(questionSetType, moduleContextId);
		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: GET_QUESTIONS_TO_SOLVE_SUCCESS,
				payload: { data },
			});

			dispatch(initiateSolveQuestions());
		} else {
			dispatch({ type: GET_QUESTIONS_TO_SOLVE_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_QUESTIONS_TO_SOLVE_ERROR });
		toast.error(error?.message);
	}
};

//to correct question by teacher
export const getLearnerSolutionToSolveAsync = (questionSetType, questionSetContextId, learnerId) => async (dispatch) => {
	dispatch({ type: GET_LEARNER_SOLUTION_TO_SOLVE_REQUEST });
	try {
		const response = await questionSetApi.getLearnerSolutionToSolve(questionSetType, questionSetContextId, learnerId);

		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: GET_LEARNER_SOLUTION_TO_SOLVE_SUCCESS,
				payload: { data },
			});
			dispatch(initiateQuestionSetCorrection());
		} else {
			dispatch({ type: GET_LEARNER_SOLUTION_TO_SOLVE_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_LEARNER_SOLUTION_TO_SOLVE_ERROR });
		toast.error(error?.message);
	}
};

//to review marks by (student)
export const getQuestionsToReviewAsync = (questionSetType, questionSetContextId) => async (dispatch) => {
	dispatch({ type: GET_QUESTIONS_TO_REVIEW_REQUEST });
	try {
		const response = await questionSetApi.getQuestionsToReview(questionSetType, questionSetContextId);

		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: GET_QUESTIONS_TO_REVIEW_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: GET_QUESTIONS_TO_REVIEW_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_QUESTIONS_TO_REVIEW_ERROR });
		toast.error(error?.message);
	}
};

//to review marks by (teacher)
export const getQuestionsToReviewTeacherAsync = (questionSetType, questionSetContextId, learnerId) => async (dispatch) => {
	dispatch({ type: GET_QUESTIONS_TO_REVIEW_TEACHER_REQUEST });
	try {
		const response = await questionSetApi.getQuestionsToReviewTeacher(questionSetType, questionSetContextId, learnerId);

		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: GET_QUESTIONS_TO_REVIEW_TEACHER_SUCCESS,
				payload: { data },
			});
			dispatch(initiateQuestionSetCorrection());
		} else {
			dispatch({ type: GET_QUESTIONS_TO_REVIEW_TEACHER_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: GET_QUESTIONS_TO_REVIEW_TEACHER_ERROR });
		toast.error(error?.message);
	}
};

export const saveLearnerSolutionAsync =
	(questionSetType, questionSetContextId, data, handleCloseSubmitAnswerModal, handleRedirectToModuleAfterSubmitted) => async (dispatch) => {
		dispatch({ type: SAVE_LEARNER_SOLUTION_REQUEST });
		try {
			const response = await questionSetApi.solveQuestions(questionSetType, questionSetContextId, data);

			if (response.data.status === 1) {
				dispatch({
					type: SAVE_LEARNER_SOLUTION_SUCCESS,
				});
				if (handleCloseSubmitAnswerModal) {
					handleCloseSubmitAnswerModal();
					handleRedirectToModuleAfterSubmitted();
				}
			} else {
				dispatch({ type: SAVE_LEARNER_SOLUTION_ERROR });
				toast.error(response.data.msg);
			}
		} catch (error) {
			dispatch({ type: SAVE_LEARNER_SOLUTION_ERROR });
			toast.error(error?.message);
		}
	};

export const initiateSolveQuestions = () => async (dispatch) => {
	dispatch({
		type: INITIATE_SOLVE_QUESTIONS,
	});
};

export const fillDataToSolve =
	(questionGroupId, groupContextId, questionContextId, questionType, key, value, isCorrect) => async (dispatch) => {
		dispatch({
			type: FILL_DATA_TO_SOLVE,
			payload: {
				questionGroupId,
				groupContextId,
				questionContextId,
				questionType,
				key,
				value,
				isCorrect,
			},
		});
	};

export const fillDataToSolveInOrderingQuestion =
	(questionGroupId, questionId, groupContextId, questionContextId, questionType, isCorrect, droppableOrder, draggableOrder) =>
	async (dispatch) => {
		dispatch({
			type: FILL_DATA_TO_SOLVE_IN_ORDERING_QUESTION,
			payload: {
				questionGroupId,
				questionId,
				groupContextId,
				questionContextId,
				questionType,
				isCorrect,
				droppableOrder,
				draggableOrder,
			},
		});
	};

export const removeOrderingQuestion =
	(questionGroupId, questionId, groupContextId, questionContextId, questionType, isCorrect, draggableOrder) => async (dispatch) => {
		dispatch({
			type: REMOVE_ORDERING_QUESTION,
			payload: {
				questionGroupId,
				questionId,
				groupContextId,
				questionContextId,
				questionType,
				isCorrect,
				draggableOrder,
			},
		});
	};

export const confirmTheSolveToTheQuestion = (questionGroupId, questionId) => async (dispatch) => {
	dispatch({
		type: CONFIRM_THE_SOLVE_TO_THE_QUESTION,
		payload: {
			questionGroupId,
			questionId,
		},
	});
};

export const resetSolveQuestion =
	(questionGroupId, questionId, requiredQuestion, moduleContextQuestionId, submissionType) => async (dispatch) => {
		dispatch({
			type: RESET_SOLVE_QUESTION,
			payload: {
				questionGroupId,
				questionId,
				requiredQuestion,
				moduleContextQuestionId,
				submissionType,
			},
		});
	};

export const solvedPercentage = () => async (dispatch) => {
	dispatch({
		type: SOLVED_PERCENTAGE,
	});
};

export const initiateQuestionSetCorrection = () => async (dispatch) => {
	dispatch({
		type: INITIATE_QUESTION_SET_CORRECTION,
	});
};

export const addFeedbackToQuestion = (learnerContextQuestionId, key, value) => async (dispatch) => {
	dispatch({
		type: ADD_FEEDBACK_TO_QUESTION,
		payload: { learnerContextQuestionId, key, value },
	});
};

export const addTeacherFeedbackToQs = (key, value) => async (dispatch) => {
	dispatch({
		type: ADD_TEACHER_FEEDBACK_TO_QS,
		payload: { key, value },
	});
};

export const calcQuestionsMark = () => async (dispatch) => {
	dispatch({
		type: CALC_QUESTIONS_MARK,
	});
};

export const correctionOfQuestionsAsync = (questionSetType, learnerQuestionSetId, data, handleBack) => async (dispatch) => {
	dispatch({ type: CORRECTION_OF_QUESTIONS_REQUEST });
	try {
		const response = await questionSetApi.correctionOfQuestions(questionSetType, learnerQuestionSetId, data);

		if (response.data.status === 1) {
			dispatch({
				type: CORRECTION_OF_QUESTIONS_SUCCESS,
			});

			if (handleBack) {
				handleBack();
			}
		} else {
			dispatch({ type: CORRECTION_OF_QUESTIONS_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: CORRECTION_OF_QUESTIONS_ERROR });
		toast.error(error?.message);
	}
};

export const addDataToQuestionsGroup = (questionGroupId, key, value) => async (dispatch) => {
	dispatch({
		type: ADD_DATA_TO_QUESTIONS_GROUP,
		payload: { questionGroupId, key, value },
	});
};

export const changeAllRequiredAnswerToFalse = (questionGroupId, key, value) => async (dispatch) => {
	dispatch({
		type: CHANGE_ALL_REQUIRED_ANSWER_TO_FALSE,
		payload: { questionGroupId, key, value },
	});
};

//Questions Bank
export const questionsBanksAsync = (URL, data, allData) => async (dispatch) => {
	dispatch({ type: QUESTIONS_BANK_REQUEST });
	try {
		const response = await questionSetApi.questionsBank(URL, data, allData);

		if (response.data.status === 1) {
			const allData = response.data.data;
			const data = response.data.data.items;
			dispatch({
				type: QUESTIONS_BANK_SUCCESS,
				payload: { allData, data },
			});
		} else {
			dispatch({ type: QUESTIONS_BANK_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: QUESTIONS_BANK_ERROR });
		toast.error(error?.message);
	}
};

export const learnerContextCreatorAsync = (searchText) => async (dispatch) => {
	dispatch({ type: LEARNER_CONTEXT_CREATOR_REQUEST });
	try {
		const response = await questionSetApi.leanerContextCreator(searchText);

		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: LEARNER_CONTEXT_CREATOR_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: LEARNER_CONTEXT_CREATOR_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: LEARNER_CONTEXT_CREATOR_ERROR });
		toast.error(error?.message);
	}
};

export const setFiltersData = (key, value) => async (dispatch) => {
	dispatch({
		type: SET_FILTERS_DATA,
		payload: { key, value },
	});
};

export const exportQuestionBankToQuestionEditor = (isGroup, groupId, questionSetIndex, question) => async (dispatch) => {
	dispatch({
		type: EXPORT_QUESTION_BANK_TO_QUESTION_EDITOR,
		payload: { isGroup, groupId, questionSetIndex, question },
	});
};

export const emptyQuestionsFromQuestionSet = () => async (dispatch) => {
	dispatch({
		type: EMPTY_QUESTIONS_FROM_QUESTION_SET,
	});
};
export const fillDataInQuestionSetToEdit = (question) => async (dispatch) => {
	dispatch({
		type: FILL_DATA_IN_QUESTION_SET_TO_EDIT,
		payload: { question },
	});
};

export const createQuestionToQuestionsBank = (type, parentId, data, handleCloseQuestionEditor) => async (dispatch) => {
	dispatch({ type: CREATE_QUESTION_TO_QUESTIONS_BANK_REQUEST });
	try {
		const response = await questionSetApi.createQuestionToQuestionsBank(type, parentId, data);

		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: CREATE_QUESTION_TO_QUESTIONS_BANK_SUCCESS,
				payload: { data },
			});
			if (handleCloseQuestionEditor) {
				handleCloseQuestionEditor();
			}
		} else {
			dispatch({ type: CREATE_QUESTION_TO_QUESTIONS_BANK_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: CREATE_QUESTION_TO_QUESTIONS_BANK_ERROR });
		toast.error(error?.message);
	}
};

export const updateQuestionsBankAsync = (questionGroupId, data, handleCloseQuestionEditor) => async (dispatch) => {
	dispatch({ type: UPDATE_QUESTIONS_BANK_REQUEST });
	try {
		const response = await questionSetApi.updateQuestionsBank(questionGroupId, data);

		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: UPDATE_QUESTIONS_BANK_SUCCESS,
				payload: { questionGroupId, data },
			});
			if (handleCloseQuestionEditor) {
				handleCloseQuestionEditor();
			}
		} else {
			dispatch({ type: UPDATE_QUESTIONS_BANK_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: UPDATE_QUESTIONS_BANK_ERROR });
		toast.error(error?.message);
	}
};

export const removeQuestionsBankAsync = (questionGroupId, handleClose) => async (dispatch) => {
	dispatch({ type: REMOVE_QUESTIONS_BANK_REQUEST });
	try {
		const response = await questionSetApi.deleteQuestionsBank(questionGroupId);

		if (response.data.status === 1) {
			dispatch({
				type: REMOVE_QUESTIONS_BANK_SUCCESS,
				payload: { questionGroupId },
			});
			if (handleClose) {
				handleClose();
			}
		} else {
			dispatch({ type: REMOVE_QUESTIONS_BANK_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: REMOVE_QUESTIONS_BANK_ERROR });
		toast.error(error?.message);
	}
};

export const allQuestionSetForCourseAsync = (URL, courseId, query) => async (dispatch) => {
	dispatch({ type: ALL_QUESTION_SET_FOR_COURSE_REQUEST });
	try {
		const response = await questionSetApi.allQuestionSetForCourse(URL, courseId, query);

		if (response.data.status === 1) {
			const data = response.data.data;
			dispatch({
				type: ALL_QUESTION_SET_FOR_COURSE_SUCCESS,
				payload: { data },
			});
		} else {
			dispatch({ type: ALL_QUESTION_SET_FOR_COURSE_ERROR });
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({ type: ALL_QUESTION_SET_FOR_COURSE_ERROR });
		toast.error(error?.message);
	}
};

export const pickQuestionSetToModuleAsync =
	(moduleId, questionSetType, questionSetId, handleClosePickFromCourseModal) => async (dispatch) => {
		dispatch({ type: PICK_QUESTION_SET_TO_MODULE_REQUEST });
		try {
			const response = await questionSetApi.pickQuestionSetToModule(moduleId, questionSetType, questionSetId);

			if (response.data.status === 1) {
				const data = response.data.data;
				dispatch({
					type: PICK_QUESTION_SET_TO_MODULE_SUCCESS,
				});
				dispatch(pushDataToCourseModule(data, moduleId));
				if (handleClosePickFromCourseModal) {
					handleClosePickFromCourseModal();
				}
			} else {
				dispatch({ type: PICK_QUESTION_SET_TO_MODULE_ERROR });
				toast.error(response.data.msg);
			}
		} catch (error) {
			dispatch({ type: PICK_QUESTION_SET_TO_MODULE_ERROR });
			toast.error(error?.message);
		}
	};
