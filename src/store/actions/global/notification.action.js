import {
	RECEIVE_NEW_NOTIFICATIONS,
	SET_NOTIFYME_SUCCESS,
	REMOVE_NOTIFICATION,
	GET_ANNOUNCEMENT_FROM_LOCAL_STORAGE,
	NOTIFICATION_UNSEEN_REQUEST,
	NOTIFICATION_UNSEEN_SUCCESS,
	NOTIFICATION_UNSEEN_ERROR,
	VIEW_NOTIFICATIONS_REQUEST,
	VIEW_NOTIFICATIONS_SUCCESS,
	VIEW_NOTIFICATIONS_ERROR,
	TRACKABLE_RECORD_ASYNC,
} from "./globalTypes";
import Swal, { SUCCESS, DANGER } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { notificationApi } from "api/global/notification";
import { ANNOUNCEMENT_REAL_TIME_SUCCESS } from "../admin/adminType";
import Helper from "components/Global/RComs/Helper";
import { toast } from "react-toastify";

export const notificationsUnseenAsync = () => async (dispatch) => {
	dispatch({ type: NOTIFICATION_UNSEEN_REQUEST });

	try {
		const response = await notificationApi.notificationsUnSeen();
		if (response.data.status === 1) {
			const notificationUnseen = response.data.data.data;
			const notificationUnseenTotalRecord = response.data.data.total;
			const notifyme = response.data.other.notifyme;
			dispatch({
				type: NOTIFICATION_UNSEEN_SUCCESS,
				payload: { notificationUnseen, notificationUnseenTotalRecord },
			});

			dispatch({
				type: SET_NOTIFYME_SUCCESS,
				payload: { notifyMe: notifyme },
			});
		} else {
			dispatch({ type: NOTIFICATION_UNSEEN_ERROR });
		}
	} catch (error) {
		dispatch({
			type: NOTIFICATION_UNSEEN_ERROR,
		});
		toast.error(error?.message);
	}
};

export const viewNotificationsAsync = (notificationId) => async (dispatch) => {
	dispatch({ type: VIEW_NOTIFICATIONS_REQUEST });

	try {
		const response = await notificationApi.viewNotifications(notificationId);
		//const newData = response.data.data.notifications;
		// const totalRecord = response.data.data.total_records;
		if (response.data.status === 1) {
			dispatch({
				type: VIEW_NOTIFICATIONS_SUCCESS,
				payload: { notification_id: notificationId },
			});
		} else {
			dispatch({ type: VIEW_NOTIFICATIONS_ERROR });
		}
	} catch (error) {
		dispatch({
			type: VIEW_NOTIFICATIONS_ERROR,
		});
		toast.error(error?.message);
	}
};

export const receiveNewNotifications = (data) => async (dispatch) => {
	dispatch({
		type: RECEIVE_NEW_NOTIFICATIONS,
		payload: data,
	});
	// if (data.notifications[0].payload.payload.type == "announcements") {
	//   dispatch({
	//     type: ANNOUNCEMENT_REAL_TIME_SUCCESS,
	//     payload: data,
	//   });
	// }
};

export const setNotifyMe = (data) => async (dispatch) => {
	try {
		const response = await notificationApi.setNotifyMe(data);
		if (response.data.status === 1) {
			dispatch({
				type: SET_NOTIFYME_SUCCESS,
				payload: { notifyMe: data },
			});
		}
	} catch (error) {}
};

//For Add New Data In Notifications
export const trackableRecordAsync = (notifications, countNotifications) => async (dispatch) => {
	dispatch({
		type: TRACKABLE_RECORD_ASYNC,
		payload: {
			notifications,
			countNotifications,
		},
	});
};
