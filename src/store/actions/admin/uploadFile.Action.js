import {
	UPLOAD_FILE_REQUEST,
	UPLOAD_FILE_SUCCESS,
	UPLOAD_FILE_ERROR,
	IMPORT_FROM_FILE_REQUEST,
	IMPORT_FROM_FILE_SUCCESS,
	IMPORT_FROM_FILE_ERROR,
	INITIAL_RESPONSE,
	MAPPING_DATA,
} from "./adminType";
import { adminApi } from "api/admin/UsersManagments/index";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

export const uploadFile = (data) => async (dispatch) => {
	dispatch({ type: UPLOAD_FILE_REQUEST });
	try {
		const response = await adminApi.postUploadFile(data);
		if (response.data.status == 1) {
			let AllTableData = response.data.data;
			dispatch({
				type: UPLOAD_FILE_SUCCESS,
				payload: { AllTableData },
			});
		} else {
			dispatch({
				type: UPLOAD_FILE_ERROR,
			});
		}
	} catch (error) {
		toast.error(error?.message);

		dispatch({
			type: UPLOAD_FILE_ERROR,
		});
	}
};

export const saveFile = (data) => async (dispatch) => {
	dispatch({ type: IMPORT_FROM_FILE_REQUEST });
	try {
		const response = await adminApi.postSaveFile(data);
		if (response.data.status == 1) {
			dispatch({
				type: IMPORT_FROM_FILE_SUCCESS,
				payload: true,
			});
		} else {
			toast.error(response.data.msg);

			dispatch({
				type: IMPORT_FROM_FILE_ERROR,
			});
		}
	} catch (error) {
		toast.error(error?.message);

		dispatch({
			type: IMPORT_FROM_FILE_ERROR,
		});
	}
};

export const initialResponse = (data) => async (dispatch) => {
	dispatch({
		type: INITIAL_RESPONSE,
		data,
	});
};

export const mappingData = (Value, Id) => async (dispatch) => {
	dispatch({
		type: MAPPING_DATA,
		payload: { Value, Id },
	});
};
