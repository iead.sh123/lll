import {
	STUDENTS_ERRORS_REQUEST,
	STUDENTS_ERRORS_SUCCESS,
	STUDENTS_ERRORS_ERROR,
	SEND_TO_PARENT_APP_REQUEST,
	SEND_TO_PARENT_APP_SUCCESS,
	SEND_TO_PARENT_APP_ERROR,
} from "./adminType";
import { adminApi } from "api/admin/UsersManagments/index";
import Swal, { SUCCESS, DANGER, WARNING } from "utils/Alert";
import tr from "components/Global/RComs/RTranslator";
import { toast } from "react-toastify";

export const studentsErrorsAsync = (school_class, setOpenStudentError, setOpenStudentNotify) => async (dispatch) => {
	dispatch({ type: STUDENTS_ERRORS_REQUEST });
	try {
		const response = await adminApi.studentsErrors(school_class);
		if (response.data.status == 1) {
			const data = response.data.data;
			dispatch({
				type: STUDENTS_ERRORS_SUCCESS,
				payload: { data },
			});
			if (data && data?.students?.length > 0) {
				setOpenStudentError(true);
			} else {
				toast.warning(tr`We will notify you after finish the process`);
				dispatch(sendToParentApp(school_class, setOpenStudentError));
			}
		} else {
			dispatch({
				type: STUDENTS_ERRORS_ERROR,
			});
		}
	} catch (error) {
		dispatch({
			type: STUDENTS_ERRORS_ERROR,
		});
		toast.error(error?.message);
	}
};

export const sendToParentApp = (school_class, setOpenStudentError) => async (dispatch) => {
	dispatch({ type: SEND_TO_PARENT_APP_REQUEST });
	setOpenStudentError(false);
	toast.warning(tr`We will notify you after finish the process`);

	try {
		const response = await adminApi.confirmationSendPdfToParent(school_class);

		if (response.data.status == 1) {
			dispatch({
				type: SEND_TO_PARENT_APP_SUCCESS,
			});
		} else {
			dispatch({
				type: SEND_TO_PARENT_APP_ERROR,
			});
			toast.error(response.data.msg);
		}
	} catch (error) {
		dispatch({
			type: SEND_TO_PARENT_APP_ERROR,
		});
		toast.error(error?.message);
	}
};
