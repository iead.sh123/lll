import {
  GET_ALL_CARDS_REQUEST,
  GET_ALL_CARDS_SUCCESS,
  GET_ALL_CARDS_ERROR,
  USER_INTERACTIONS_REQUEST,
  USER_INTERACTIONS_SUCCESS,
  USER_INTERACTIONS_ERROR,
  USER_LOGS_REQUEST,
  USER_LOGS_SUCCESS,
  USER_LOGS_ERROR,
} from "../../actions/global/globalTypes";
import produce from "immer";

const initState = {
  cards: [],
  loadingCards: false,
};

const TrackServiceReducer = (state = initState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case GET_ALL_CARDS_REQUEST: {
        draft.loadingCards = true;
        return draft;
      }
      case GET_ALL_CARDS_SUCCESS: {
        draft.loadingCards = false;
        draft.cards = action.payload.data;
        return draft;
      }
      case GET_ALL_CARDS_ERROR: {
        draft.loadingCards = false;
        return draft;
      }

      //------------------Services--------------------

      default:
        return draft;
    }
  });
export default TrackServiceReducer;
