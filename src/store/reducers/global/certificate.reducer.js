import {
  GET_CERTIFICATES_FOR_SECTION_REQUEST,
  GET_CERTIFICATES_FOR_SECTION_SUCCESS,
  GET_CERTIFICATES_FOR_SECTION_ERROR,
} from "../../actions/global/globalTypes";
import produce from "immer";

const initState = {
  certificate: [],
  certificateLoading: false,
};

const certificateReducer = (state = initState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case GET_CERTIFICATES_FOR_SECTION_REQUEST:
        draft.certificateLoading = true;
        return draft;
      case GET_CERTIFICATES_FOR_SECTION_SUCCESS:
        draft.certificateLoading = false;
        draft.certificate = action.payload.data;
        return draft;
      case GET_CERTIFICATES_FOR_SECTION_ERROR:
        draft.certificateLoading = false;
        return draft;

      default:
        return draft;
    }
  });
export default certificateReducer;
