import {
  SET_CHAT,
  SET_USER_CHATS,
  SEE_MORE,
  SET_CHAT_MESSAGES,
  Add_PUSHED_MESSAGE,
  SET_CHAT_USERS,
  SET_PUSHER_IM_SUBSCRIBED,
  SET_SELECTED_CHAT_ID,
  SHOW_FLOATING_CHAT,
  OPEN_NOTES,
  HANDLE_NOTES,
  Add_PUSHED_CHAT,
  ADD_USER_CHAT,
  UPDATE_USER_CHAT,
  SET_ACTIVE_CHAT,
  ADD_TO_CHAT,
} from "store/actions/global/globalTypes";
import { produce, current } from "immer";
import Helper from "components/Global/RComs/Helper";
import { HIDE_NOTES_INITIATOR } from "store/actions/global/globalTypes";
import { MARK_CHATS_AS_READ } from "store/actions/global/globalTypes";

const initState = {
  chats: [],
  subscribed: false,
  selectedChatID: -1,
  selectedChatIndex_Global: 0,
  showFloatingChat: false,
  showNotesInitiator: false,
  loggedUserId: -1,
  lastReadChat: { id: -1, changed: true },
  current_note: {
    table_name: "",
    row_id: -1,
    context: "",
    context_id: -1,
    nameToShow: "",
  },
};

const findChat = (chats, id) => {
  let selectedChatIndex = -1;
  chats.map((c, i) => {
    if (c.id == id) {
      selectedChatIndex = i;
    }
  });
  return selectedChatIndex;
};

const findNotes = (chats, table_name, row_id, context, context_id) => {
  let selectedChatIndex = -1;

  chats.map((c, i) => {
    if (c.note_history && c.note_history.length > 0 && c.note_history[0]) {
      if (
        c.note_history[0].note_historable_type == table_name &&
        c.note_history[0].note_historable_id == row_id &&
        c.note_history[0].context_type == context &&
        c.note_history[0].context_id == context_id
      ) {
        selectedChatIndex = i;
      } else {
      }
    }
  });
  return selectedChatIndex;
};
const calculateUnread = (chat, user) => {
  if (user.pointer != chat?.message?.id) {
    let unread_messages = 0;
    let unread_flag = "";
    if (!chat.messages) return ["+", 1];
    else {
      for (let i = 0; i < chat.messages.length; i++) {
        if (i > 10) {
          unread_flag = "+";
        }
        if (user.pointer == chat.messages[i].id) {
          break;
        } else {
          unread_messages++;
        }
      }
      return [unread_flag, unread_messages];
    }
  } else return ["", 0];
};

const IMReducer = (state = initState, action) =>
  produce(state, (draft) => {
    let selectedChatIndex = -1;
    switch (action.type) {
      case SET_USER_CHATS:
        draft.selectedChatIndex_Global = -1;
        draft.chats = action.chats;
        draft.loggedUserId = action.loggedUserId;
        //---------------------------set the pointer

        action.chats.map((chat) => {
          chat.users_chat.map((user, i) => {
            if (draft.loggedUserId == user.id) {
              const [flag, unread] = calculateUnread(chat, user);

              chat.unread = unread;
              chat.unreadflag = flag;
            }
          });
        });
        //----------------------------------
        if (action.chats.length > 0) draft.selectedChatIndex_Global = 0;
        return draft;
      //----------------   dispatch({type:SET_CHAT_MESSAGES,chatid:selectedID,messages:res.data.data.models.messages})
      case SET_CHAT_MESSAGES:
        selectedChatIndex = findChat(draft.chats, action.chatid);
        draft.selectedChatIndex_Global = selectedChatIndex;
        if (selectedChatIndex != -1) {
          //   if(!draft.chats[selectedChatIndex].messages)
          draft.chats[selectedChatIndex].messages = [];

          action.messages.map((mess, i) => {
            draft.chats[selectedChatIndex].messages.push(mess);
          });
          draft.chats[selectedChatIndex].users = action.users;
          draft.chats[selectedChatIndex].firstMessage = action.firstMessage;

          if (
            draft &&
            draft.chats &&
            draft.chats[selectedChatIndex] &&
            draft.chats[selectedChatIndex].messages &&
            draft.chats[selectedChatIndex].messages[0] &&
            draft.chats[selectedChatIndex].messages[0].id ==
              action.firstMessage.id
          ) {
            draft.chats[selectedChatIndex].hideLoadMore = true;
          }
        }
        return draft;

      //----------------   dispatch( {type:Add_PUSHED_MESSAGE,message:message,chatid:chatid} );

      case Add_PUSHED_MESSAGE:
        selectedChatIndex = findChat(draft.chats, action.chatid);

        //-------------------
        if (selectedChatIndex != -1) {
          if (!draft.chats[selectedChatIndex].messages)
            draft.chats[selectedChatIndex].messages = [];

          draft.chats[selectedChatIndex].messages.push(action.message);
          draft.chats[selectedChatIndex].message = action.message;

          draft.chats[selectedChatIndex].users = action.users;

          if (draft.selectedChatID != draft.chats[selectedChatIndex].id) {
            draft.chats[selectedChatIndex].unread += 1;
            draft.lastReadIndicatorChange = {};
          } else {
            // same chat that is opened
            draft.chats[selectedChatIndex].unread = 0;
            draft.lastReadChat = {
              id: draft.selectedChatID,
              changed: !draft.lastReadChat.changed,
            }; //just change something so that it's changed
          }
        } // It's a new chat
        else {
        }
        //---------------------------sort chats
        let sortedchats = [];
        sortedchats.push(draft.chats[selectedChatIndex]);
        draft.chats.forEach((element, index) => {
          if (index != selectedChatIndex) sortedchats.push(element);
        });
        draft.chats = sortedchats;
        //-----------------------handle selected chat
        selectedChatIndex = findChat(draft.chats, draft.selectedChatID);
        draft.selectedChatIndex_Global = selectedChatIndex;
        //----------------------------------------------

        //-----------------------------------
        return draft;

      //----------------   dispatch( {type:Add_PUSHED_MESSAGE,message:message,chatid:chatid} );
      case SET_PUSHER_IM_SUBSCRIBED:
        draft.subscribed = action.state;
        return draft;
      //----------------   dispatch({type:SET_SELECTED_CHAT_ID,id:id})

      case SET_SELECTED_CHAT_ID:
        draft.selectedChatID = action.id;
        selectedChatIndex = findChat(draft.chats, draft.selectedChatID);
        draft.selectedChatIndex_Global = selectedChatIndex;

        return draft;
      //----------------   dispatch({type:SHOW_FLOATING_CHAT,state:true})

      case SHOW_FLOATING_CHAT:
        draft.showFloatingChat = action.state;

        return draft;

      //---------------- dispatch({type:HANDLE_NOTES,message:"",table_name:"resources",row_id: itemId,context:"Course",context_id:course_id,nameToShow:slkjgf})
      //dispatch({type:HANDLE_NOTES,table_name:"resources",row_id: item.id,context:"Course",context_id:course_id,nameToShow:item.name})
      case HANDLE_NOTES:
        selectedChatIndex = findNotes(
          draft.chats,
          action.table_name,
          action.row_id,
          action.context,
          action.context_id
        );
        draft.current_note = {
          table_name: action.table_name,
          row_id: action.row_id,
          context: action.context,
          context_id: action.context_id,
          nameToShow: action.nameToShow,
        };
        if (selectedChatIndex >= 0) {
          draft.selectedChatID = draft.chats[selectedChatIndex].id;
          draft.selectedChatIndex_Global = selectedChatIndex;
          draft.showFloatingChat = true;
        } else {
          draft.showNotesInitiator = true;
        }
        return draft;

      case HIDE_NOTES_INITIATOR:
        draft.showNotesInitiator = false;
        return draft;
      //---------------- dispatch({type:OPEN_NOTES,Table:"lessons",row:lesson.id})
      case OPEN_NOTES:
        //draft.showFloatingChat=action.state;

        return draft;
      //----------------  dispatch({type:ADD_PUSHED_CHAT, chat: res.data.data})

      case Add_PUSHED_CHAT:
        if (!draft.chats || !draft.chats[0]) {
          draft.chats = [];
        }

        draft.chats.push(action.chat);
        draft.chats[draft.chats.length - 1].message =
          draft.chats[draft.chats.length - 1].messages[
            draft.chats[draft.chats.length - 1].messages.length - 1
          ];

        let selectedchat = draft.chats[draft.chats.length - 1];

        if (
          selectedchat &&
          selectedchat.messages &&
          selectedchat.messages[0] &&
          selectedchat.id == action.chat.id
        ) {
          draft.chats[draft.chats.length - 1].hideLoadMore = true;
        }
        let unread = 1;
        if (selectedchat.messages[0].user_id == draft.loggedUserId) unread = 0;

        selectedchat.unread = unread;
        //---------------------------handle unread

        //-----------------------------sort chats
        let sortedchats1 = [];
        selectedChatIndex = draft.chats.length - 1;
        sortedchats1.push(draft.chats[selectedChatIndex]);
        draft.chats.forEach((element, index) => {
          if (index != selectedChatIndex) sortedchats1.push(element);
        });
        draft.chats = sortedchats1;
        //-----------------------handle selected chat

        // ------------------------------
        return draft;
      //--------------dispatch( {type:SET_CHAT_USERS,chat_users:data.chat_users,chatid:data.chat_id} );

      case SET_CHAT_USERS:
        selectedChatIndex = findChat(draft.chats, action.chatid);
        if (selectedChatIndex != -1) {
          //   if(!draft.chats[selectedChatIndex].messages)
          draft.chats[selectedChatIndex].users_chat = [];
          action.chat_users.map((mess, i) => {
            draft.chats[selectedChatIndex].users_chat.push(mess);

            const chat = draft.chats[selectedChatIndex];
            chat.users_chat.map((user, i) => {
              if (draft.loggedUserId == user.id) {
                const [flag, unread] = calculateUnread(chat, user);

                chat.unread = unread;
                chat.unreadflog = flag;
              }
            });
          });
          // draft.chats[selectedChatIndex].users=action.users;
        }
        return draft;
      //-------------- dispatch({type:SEE_MORE ,messages: res.data.data.models.nextMessages,chat_id:selectedID})

      case SEE_MORE:
        selectedChatIndex = findChat(draft.chats, action.chat_id);
        if (selectedChatIndex != -1) {
          if (!draft.chats[selectedChatIndex].previousMessages) {
            draft.chats[selectedChatIndex].previousMessages = [];
          }

          draft.chats[selectedChatIndex].previousMessages.push({
            index: draft.chats[selectedChatIndex].previousMessages.length,
            messages: action.messages,
          });
        }
        return draft; //---------------------------------------------------         default:
        return state;
      case MARK_CHATS_AS_READ:
        draft.chats.forEach((chat) => {
          chat.pivot ? (chat.pivot.seen = 1) : (chat.pivot = { seen: 1 });
        });
        return draft;
    }
  });

export default IMReducer;
