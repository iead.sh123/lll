import Helper from "components/Global/RComs/Helper";
import { produce } from "immer";
import {
	INITIATE_QUESTION_SET_FORMATE,
	FILL_QUESTION_SET_INFO,
	CREATE_QUESTION_SET_GROUP,
	CREATE_NEW_QUESTION,
	REMOVE_QUESTION_GROUP,
	REMOVE_QUESTION_TYPE,
	FILL_DATA_TO_QUESTION_TYPE,
	CREATE_QUESTION_SET_REQUEST,
	CREATE_QUESTION_SET_SUCCESS,
	CREATE_QUESTION_SET_ERROR,
	ADD_CHOICES_TO_QUESTION,
	FILL_DATA_IN_CHOICES_QUESTIONS,
	REMOVE_CHOICE_FROM_QUESTION,
	FILL_DATA_IN_CORRECT_ANSWER_TO_SOME_QUESTIONS,
	ADD_PAIRS_TO_MATCH_QUESTION,
	CALCULATE_POINTS,
	ALL_QUESTION_SET_REQUEST,
	ALL_QUESTION_SET_SUCCESS,
	ALL_QUESTION_SET_ERROR,
	PUBLISH_AND_UN_PUBLISH_QUESTION_SET_REQUEST,
	PUBLISH_AND_UN_PUBLISH_QUESTION_SET_SUCCESS,
	PUBLISH_AND_UN_PUBLISH_QUESTION_SET_ERROR,
	REMOVE_QUESTION_SET_REQUEST,
	REMOVE_QUESTION_SET_SUCCESS,
	REMOVE_QUESTION_SET_ERROR,
	QUESTION_SET_BY_ID_REQUEST,
	QUESTION_SET_BY_ID_SUCCESS,
	QUESTION_SET_BY_ID_ERROR,
	UPDATE_QUESTION_SET_REQUEST,
	UPDATE_QUESTION_SET_SUCCESS,
	UPDATE_QUESTION_SET_ERROR,
	SUBMISSIONS_QUESTION_SET_REQUEST,
	SUBMISSIONS_QUESTION_SET_SUCCESS,
	SUBMISSIONS_QUESTION_SET_ERROR,
	DELETE_SUBMISSION_REQUEST,
	DELETE_SUBMISSION_SUCCESS,
	DELETE_SUBMISSION_ERROR,
	GET_QUESTIONS_TO_SOLVE_REQUEST,
	GET_QUESTIONS_TO_SOLVE_SUCCESS,
	GET_QUESTIONS_TO_SOLVE_ERROR,
	GET_LEARNER_SOLUTION_TO_SOLVE_REQUEST,
	GET_LEARNER_SOLUTION_TO_SOLVE_SUCCESS,
	GET_LEARNER_SOLUTION_TO_SOLVE_ERROR,
	GET_QUESTIONS_TO_REVIEW_REQUEST,
	GET_QUESTIONS_TO_REVIEW_SUCCESS,
	GET_QUESTIONS_TO_REVIEW_ERROR,
	GET_QUESTIONS_TO_REVIEW_TEACHER_REQUEST,
	GET_QUESTIONS_TO_REVIEW_TEACHER_SUCCESS,
	GET_QUESTIONS_TO_REVIEW_TEACHER_ERROR,
	SAVE_LEARNER_SOLUTION_REQUEST,
	SAVE_LEARNER_SOLUTION_SUCCESS,
	SAVE_LEARNER_SOLUTION_ERROR,
	INITIATE_SOLVE_QUESTIONS,
	FILL_DATA_TO_SOLVE,
	CONFIRM_THE_SOLVE_TO_THE_QUESTION,
	RESET_SOLVE_QUESTION,
	SOLVED_PERCENTAGE,
	INITIATE_QUESTION_SET_CORRECTION,
	ADD_FEEDBACK_TO_QUESTION,
	CALC_QUESTIONS_MARK,
	ADD_TEACHER_FEEDBACK_TO_QS,
	CORRECTION_OF_QUESTIONS_REQUEST,
	CORRECTION_OF_QUESTIONS_SUCCESS,
	CORRECTION_OF_QUESTIONS_ERROR,
	FILL_DATA_TO_SOLVE_IN_ORDERING_QUESTION,
	CREATE_NEW_QUESTION_GROUP,
	ADD_DATA_TO_QUESTIONS_GROUP,
	CREATE_NEW_QUESTIONS_TO_QUESTION_GROUP,
	CHANGE_ALL_REQUIRED_ANSWER_TO_FALSE,
	QUESTIONS_BANK_REQUEST,
	QUESTIONS_BANK_SUCCESS,
	QUESTIONS_BANK_ERROR,
	SET_FILTERS_DATA,
	LEARNER_CONTEXT_CREATOR_REQUEST,
	LEARNER_CONTEXT_CREATOR_SUCCESS,
	LEARNER_CONTEXT_CREATOR_ERROR,
	EXPORT_QUESTION_BANK_TO_QUESTION_EDITOR,
	REMOVE_ORDERING_QUESTION,
	EMPTY_QUESTIONS_FROM_QUESTION_SET,
	CREATE_QUESTION_TO_QUESTIONS_BANK_REQUEST,
	CREATE_QUESTION_TO_QUESTIONS_BANK_SUCCESS,
	CREATE_QUESTION_TO_QUESTIONS_BANK_ERROR,
	FILL_DATA_IN_QUESTION_SET_TO_EDIT,
	UPDATE_QUESTIONS_BANK_REQUEST,
	UPDATE_QUESTIONS_BANK_SUCCESS,
	UPDATE_QUESTIONS_BANK_ERROR,
	REMOVE_QUESTIONS_BANK_REQUEST,
	REMOVE_QUESTIONS_BANK_SUCCESS,
	REMOVE_QUESTIONS_BANK_ERROR,
	ALL_QUESTION_SET_FOR_COURSE_REQUEST,
	ALL_QUESTION_SET_FOR_COURSE_SUCCESS,
	ALL_QUESTION_SET_FOR_COURSE_ERROR,
	PICK_QUESTION_SET_TO_MODULE_REQUEST,
	PICK_QUESTION_SET_TO_MODULE_SUCCESS,
	PICK_QUESTION_SET_TO_MODULE_ERROR,
} from "store/actions/global/globalTypes";

const InitState = {
	allQuestionSet: {},
	allSubmissions: {},
	questionSet: {},
	filterTabs: [],
	questionsToSolve: {},
	questionAnswers: { questionAnswers: [] },
	questionsCorrection: {
		totalTeacherMark: 0,
		teacherFeedback: "",
		sendNotification: false,
		questionsMarks: {},
	},
	questionsBank: [],
	questionsBankEditor: {},
	questionsBankFilters: {},
	learnerContextCreator: [],
	questionsBankPagination: {
		firstPageUrl: "",
		lastPageUrl: "",
		currentPage: 1,
		lastPage: 0,
		prevPageUrl: "",
		nextPageUrl: "",
		total: 0,
	},

	allQuestionSetForCourse: [],
	allQuestionSetForCoursePagination: {
		firstPageUrl: "",
		lastPageUrl: "",
		currentPage: 1,
		lastPage: 0,
		prevPageUrl: "",
		nextPageUrl: "",
		total: 0,
	},
	finalMarkToStudent: 0,
	correctQuestionsCount: 0,
	allQuestionSetLoading: false,
	createQuestionSetLoading: false,
	questionSetByIdLoading: false,
	deleteQuestionSetLoading: false,
	allSubmissionsLoading: false,
	questionsToSolveLoading: false,
	saveLearnerSolutionLoading: false,
	percentageOfSolvingQuestions: 0,
	correctionOfQuestions: false,
	questionsBankLoading: false,
	learnerContextCreatorLoading: false,
	createQuestionToQuestionsBankLoading: false,
	removeQuestionsBankLoading: false,
	allQuestionSetForCourseLoading: false,
	test: null,
};

let ind;
let index;
let indd;
let inddd;

const correctAnswersToTheMatchingQuestion = (pairs) => {
	const formattedData = [];
	for (let i = 0; i < pairs.length; i += 2) {
		const pair = `${pairs[i].order}:${pairs[i + 1].order}`;
		formattedData.push(pair);
	}
	const result = formattedData.join(",");
	return result;
};

const correctAnswersToTheSequencingStatementsQuestions = (sequencingStatements) => {
	const formattedData = [];
	for (let i = 0; i < sequencingStatements.length; i++) {
		const sequencingStatement = `${sequencingStatements[i].order}`;
		formattedData.push(sequencingStatement);
	}
	const result = formattedData.join(",");
	return result;
};

// const correctAnswersToTheChoicesQuestions = (() => {
//   const formattedData = [];

//   return (choices) => {
//     for (let i = 0; i < choices.length; i++) {
//       const isIdSelected = formattedData.includes(choices[i].order);
//       if (!isIdSelected) {
//         formattedData.push(choices[i].order);
//       } else {
//         const index = formattedData.indexOf(choices[i].order);

//         if (index !== -1) {
//           formattedData.splice(index, 1);
//         }
//       }
//     }
//     return formattedData;
//   };
// })();
// questions: [
//   {
//     choices: [
//       {
//         fakeId: 0.5090606844425993,
//         files: [],
//         order: 1,
//         text: "",
//       },
//       {
//         fakeId: 0.2090606844425993,
//         files: [],
//         order: 2,
//         text: "",
//       },
//     ],
//     correctAnswer: "",
//   },
//   {
//     choices: [
//       {
//         fakeId: 0.30490606844425993,
//         files: [],
//         order: 1,
//         text: "",
//       },
//       {
//         fakeId: 0.209420606844425993,
//         files: [],
//         order: 2,
//         text: "",
//       },
//     ],
//     correctAnswer: "",
//   },
// ];
const correctAnswersToTheChoicesQuestions = (() => {
	const formattedData = [];

	return (clickedOrder) => {
		const isIdSelected = formattedData.includes(clickedOrder);

		if (!isIdSelected) {
			formattedData.push(clickedOrder);
		} else {
			const index = formattedData.indexOf(clickedOrder);

			if (index !== -1) {
				formattedData.splice(index, 1);
			}
		}

		return formattedData;
	};
})();

const correctAnswersWhenSolvingInMatchQuestion = (() => {
	const selectedIds = [];

	return (id) => {
		const isIdSelected = selectedIds.includes(id);
		if (!isIdSelected) {
			selectedIds.push(id);
		} else {
			const index = selectedIds.indexOf(id);
			if (index !== -1) {
				selectedIds.splice(index, 1);
			}
		}
		return selectedIds;
	};
})();
const questionSetReducer = (state = InitState, action) =>
	produce(state, (draft) => {
		switch (action.type) {
			case CREATE_QUESTION_SET_REQUEST:
				draft.createQuestionSetLoading = true;
				return draft;
			case CREATE_QUESTION_SET_SUCCESS:
				draft.createQuestionSetLoading = false;
				// draft.questionSet = {};
				return draft;
			case CREATE_QUESTION_SET_ERROR:
				draft.createQuestionSetLoading = false;
				return draft;

			case QUESTION_SET_BY_ID_REQUEST:
				draft.questionSetByIdLoading = true;
				return draft;
			case QUESTION_SET_BY_ID_SUCCESS:
				draft.questionSetByIdLoading = false;
				draft.questionSet = action.payload.data;
				return draft;
			case QUESTION_SET_BY_ID_ERROR:
				draft.questionSetByIdLoading = false;
				return draft;

			case UPDATE_QUESTION_SET_REQUEST:
				draft.createQuestionSetLoading = true;
				return draft;
			case UPDATE_QUESTION_SET_SUCCESS:
				draft.createQuestionSetLoading = false;
				// draft.questionSet = {};
				return draft;
			case UPDATE_QUESTION_SET_ERROR:
				draft.createQuestionSetLoading = false;
				return draft;

			case REMOVE_QUESTION_SET_REQUEST:
				draft.deleteQuestionSetLoading = true;
				return draft;
			case REMOVE_QUESTION_SET_SUCCESS:
				draft.deleteQuestionSetLoading = false;
				ind = draft.allQuestionSet.records.findIndex((el) => el.id == action.payload.questionSetId);
				if (ind !== -1) {
					draft.allQuestionSet.records.splice(ind, 1);
					draft.filterTabs.forEach((tab) => {
						const updatedCount = action.payload.data[tab.title];
						if (updatedCount !== undefined) {
							tab.count = updatedCount;
						}
					});
				}
				return draft;
			case REMOVE_QUESTION_SET_ERROR:
				draft.deleteQuestionSetLoading = false;
				return draft;

			case SUBMISSIONS_QUESTION_SET_REQUEST:
				draft.allSubmissionsLoading = true;
				return draft;
			case SUBMISSIONS_QUESTION_SET_SUCCESS:
				draft.allSubmissionsLoading = false;
				draft.allSubmissions = action.payload.data;
				return draft;
			case SUBMISSIONS_QUESTION_SET_ERROR:
				draft.allSubmissionsLoading = false;
				return draft;

			case DELETE_SUBMISSION_REQUEST:
				return draft;
			case DELETE_SUBMISSION_SUCCESS:
				return draft;
			case DELETE_SUBMISSION_ERROR:
				return draft;

			case INITIATE_QUESTION_SET_FORMATE:
				draft.questionSet = action.payload.questionSetFormate;
				return draft;
			case FILL_QUESTION_SET_INFO:
				draft.questionSet[action.payload.key] = action.payload.value;
				return draft;

			case CREATE_NEW_QUESTION:
				if (!action.payload.questionSetIndex) {
					draft.questionSet.groups.unshift(action.payload.newFormate);
				} else {
					draft.questionSet.groups.splice(action.payload.questionSetIndex, 0, action.payload.newFormate);
				}
				return draft;

			case CREATE_NEW_QUESTION_GROUP:
				if (!action.payload.questionSetIndex) {
					draft.questionSet.groups.unshift(action.payload.newFormate);
				} else {
					draft.questionSet.groups.splice(action.payload.questionSetIndex, 0, action.payload.newFormate);
				}
				return draft;

			case CREATE_NEW_QUESTIONS_TO_QUESTION_GROUP:
				ind = draft.questionSet.groups.findIndex((el) => (el.id || el.fakeId) == action.payload.groupId);

				if (!action.payload.questionSetIndex) {
					if (ind !== -1) {
						draft.questionSet.groups[ind].questions.unshift(action.payload.newFormate);
					}
				} else {
					if (ind !== -1) {
						draft.questionSet.groups[ind].questions.splice(action.payload.questionSetIndex, 0, action.payload.newFormate);
					}
				}
				return draft;

			case REMOVE_QUESTION_GROUP:
				ind = draft.questionSet.groups.findIndex((group) => (group.id ?? group.fakeId) == action.payload.questionGroupId);

				if (ind !== -1) {
					if (action?.payload?.isGroup) {
						if (action.payload.questionTypeId) {
							index = draft.questionSet.groups[ind].questions.findIndex(
								(question) => (question.id ?? question.fakeId) == action.payload.questionTypeId
							);
							if (index !== -1) {
								draft.questionSet.groups[ind].questions.splice(index, 1);
							}
						} else {
							draft.questionSet.groups.splice(ind, 1);
						}
					} else {
						draft.questionSet.groups.splice(ind, 1);
					}
				}
				return draft;

			case REMOVE_QUESTION_TYPE:
				ind = draft.questionSet.groups.findIndex((group) => (group.id ?? group.fakeId) == action.payload.questionGroupId);
				if (ind !== -1) {
					index = draft.questionSet.groups[ind].questions.findIndex(
						(question) => (question.id ?? question.fakeId) == action.payload.questionTypeId
					);

					if (index !== -1) {
						draft.questionSet.groups[ind].questions.splice(index, 1);
					}
				}
				return draft;

			case FILL_DATA_TO_QUESTION_TYPE:
				ind = draft.questionSet.groups.findIndex((group) => (group.id ?? group.fakeId) == action.payload.questionGroupId);
				if (ind !== -1) {
					index = draft.questionSet.groups[ind].questions.findIndex(
						(question) => (question.id ?? question.fakeId) == action.payload.questionTypeId
					);
					if (index !== -1) {
						draft.questionSet.groups[ind].questions[index][action.payload.key] = action.payload.value;
					}
				}
				return draft;

			case FILL_DATA_IN_CORRECT_ANSWER_TO_SOME_QUESTIONS:
				ind = draft.questionSet.groups.findIndex((group) => (group.id ?? group.fakeId) == action.payload.questionGroupId);
				if (ind !== -1) {
					index = draft.questionSet.groups[ind].questions.findIndex(
						(question) => (question.id ?? question.fakeId) == action.payload.questionTypeId
					);
					if (index !== -1) {
						if (action.payload.questionType == "SEQUENCING") {
							const result = correctAnswersToTheSequencingStatementsQuestions(
								draft.questionSet.groups[ind].questions[index].sequencingStatements
							);

							draft.questionSet.groups[ind].questions[index].correctAnswer = result;
						}
						if (action.payload.questionType == "MULTIPLE_CHOICES_MULTIPLE_ANSWERS") {
							const result = correctAnswersToTheChoicesQuestions(action.payload.value);

							draft.questionSet.groups[ind].questions[index].correctAnswer = result.join(",");
						}
					}
				}
				return draft;

			case ADD_CHOICES_TO_QUESTION:
				ind = draft.questionSet.groups.findIndex((group) => (group.id ?? group.fakeId) == action.payload.questionGroupId);
				if (ind !== -1) {
					index = draft.questionSet.groups[ind].questions.findIndex(
						(question) => (question.id ?? question.fakeId) == action.payload.questionTypeId
					);
					if (index !== -1) {
						if (action.payload.questionType == "choice") {
							draft.questionSet.groups[ind].questions[index].choices.push(action.payload.obj);
						} else if (action.payload.questionType == "ordering") {
							draft.questionSet.groups[ind].questions[index].sequencingStatements.push(action.payload.obj);
						}
					}
				}
				return draft;

			case FILL_DATA_IN_CHOICES_QUESTIONS:
				ind = draft.questionSet.groups.findIndex((group) => (group.id ?? group.fakeId) == action.payload.questionGroupId);
				if (ind !== -1) {
					index = draft.questionSet.groups[ind].questions.findIndex(
						(question) => (question.id ?? question.fakeId) == action.payload.questionTypeId
					);
					if (index !== -1) {
						// start questionType choice (multiple Choice , multiple Choice [single answer] , true or false)
						if (action.payload.questionType == "choice") {
							let choiceInd = draft.questionSet.groups[ind].questions[index].choices.findIndex(
								(choice) => (choice?.id ?? choice?.fakeId) == action.payload.choiceId
							);
							if (choiceInd !== -1) {
								draft.questionSet.groups[ind].questions[index].choices[choiceInd][action.payload.key] = action.payload.value;
							}
						}
						// end questionType choice

						// start questionType ordering (ordering)
						else if (action.payload.questionType == "ordering") {
							let orderingInd = draft.questionSet.groups[ind].questions[index].sequencingStatements.findIndex(
								(choice) => (choice?.id ?? choice?.fakeId) == action.payload.choiceId
							);
							if (orderingInd !== -1) {
								draft.questionSet.groups[ind].questions[index].sequencingStatements[orderingInd][action.payload.key] = action.payload.value;
							}
						}
						// end questionType ordering

						// start questionType pairs (match)
						else {
							let matchInd = draft.questionSet.groups[ind].questions[index].pairs.findIndex(
								(choice) => (choice?.id ?? choice?.fakeId) == action.payload.choiceId
							);
							if (matchInd !== -1) {
								draft.questionSet.groups[ind].questions[index].pairs[matchInd][action.payload.key] = action.payload.value;
							}
						}
						// end questionType pairs
					}
				}
				return draft;

			case REMOVE_CHOICE_FROM_QUESTION:
				ind = draft.questionSet.groups.findIndex((group) => (group.id ?? group.fakeId) == action.payload.questionGroupId);
				if (ind !== -1) {
					index = draft.questionSet.groups[ind].questions.findIndex(
						(question) => (question.id ?? question.fakeId) == action.payload.questionTypeId
					);

					if (index !== -1) {
						// start questionType choice (multiple Choice , multiple Choice [single answer] , true or false)
						if (action.payload.questionType == "choice") {
							let choiceInd = draft.questionSet.groups[ind].questions[index].choices.findIndex(
								(choice) => (choice?.id ?? choice?.fakeId) == action.payload.choiceId
							);
							if (choiceInd !== -1) {
								draft.questionSet.groups[ind].questions[index].choices.splice(choiceInd, 1);

								if (draft.questionSet.groups[ind].questions[index].type == "MULTIPLE_CHOICES_SINGLE_ANSWER") {
									let deleteCorrectAnswer = draft.questionSet.groups[ind].questions[index].choices.some(
										(el) => el.order === action.payload.choiceOrder
									);
									if (!deleteCorrectAnswer) {
										draft.questionSet.groups[ind].questions[index].correctAnswer = null;
									}
								} else if (draft.questionSet.groups[ind].questions[index].type == "MULTIPLE_CHOICES_MULTIPLE_ANSWERS") {
								}
							}
						}
						// end questionType choice

						// start questionType ordering (ordering)
						else if (action.payload.questionType == "ordering") {
							let orderingInd = draft.questionSet.groups[ind].questions[index].sequencingStatements.findIndex(
								(choice) => (choice?.id ?? choice?.fakeId) == action.payload.choiceId
							);
							if (orderingInd !== -1) {
								draft.questionSet.groups[ind].questions[index].sequencingStatements.splice(orderingInd, 1);

								//remove order from correctAnswer
								const result = correctAnswersToTheSequencingStatementsQuestions(
									draft.questionSet.groups[ind].questions[index].sequencingStatements
								);

								draft.questionSet.groups[ind].questions[index].correctAnswer = result;
							}
						}
						// end questionType ordering

						// start questionType pairs (match)
						else {
							draft.questionSet.groups[ind].questions[index].pairs = draft.questionSet.groups[ind].questions[index].pairs.filter(
								(item) => item.order !== action.payload.choiceOrder && item.order !== action.payload.choiceOrder - 1
							);

							const result = correctAnswersToTheMatchingQuestion(draft.questionSet.groups[ind].questions[index].pairs);
							draft.questionSet.groups[ind].questions[index].correctAnswer = result;
						}
						// end questionType pairs
					}
				}
				return draft;

			case ADD_PAIRS_TO_MATCH_QUESTION:
				ind = draft.questionSet.groups.findIndex((group) => (group.id ?? group.fakeId) == action.payload.questionGroupId);
				if (ind !== -1) {
					index = draft.questionSet.groups[ind].questions.findIndex(
						(question) => (question.id ?? question.fakeId) == action.payload.questionTypeId
					);

					if (index !== -1) {
						const newPairs = draft.questionSet.groups[ind].questions[index].pairs.concat(action.payload.newFormate);
						draft.questionSet.groups[ind].questions[index].pairs = newPairs;

						const result = correctAnswersToTheMatchingQuestion(draft.questionSet.groups[ind].questions[index].pairs);
						draft.questionSet.groups[ind].questions[index].correctAnswer = result;
					}
				}
				return draft;

			// case CALCULATE_POINTS:
			// 	const result = draft.questionSet.groups.map((group) => {
			// 		if (group.requiredQuestionsToAnswer && group.points) {
			// 			return parseInt((+group.requiredQuestionsToAnswer * +group.points).toString(), 10);
			// 		} else {
			// 			const totalPoints = group.questions.reduce((acc, question) => +acc + +question.points, 0);

			// 			return parseInt(totalPoints.toString(), 10);
			// 		}
			// 	});

			// 	const totalPointsSum = result.reduce((acc, points) => +acc + +points, 0);
			// 	draft.questionSet.points = totalPointsSum;

			// 	return draft;

			// (i write this code because ,in javaScript, numbers are represented using 64 bits,
			// and  when add large number BigInt it solve the problem)
			case CALCULATE_POINTS:
				const result = draft.questionSet.groups.map((group) => {
					if (group.requiredQuestionsToAnswer && group.points) {
						return BigInt(group.requiredQuestionsToAnswer) * BigInt(group.points);
					} else {
						const totalPoints = group.questions.reduce((acc, question) => acc + BigInt(question.points), BigInt(0));
						return totalPoints;
					}
				});

				const totalPointsSum = result.reduce((acc, points) => acc + points, BigInt(0));
				draft.questionSet.points = totalPointsSum.toString(); // Convert back to string

				return draft;

			case ALL_QUESTION_SET_REQUEST:
				draft.allQuestionSetLoading = true;
				return draft;
			case ALL_QUESTION_SET_SUCCESS:
				draft.allQuestionSetLoading = false;
				draft.allQuestionSet = action.payload.data;
				if (action.payload.data.filtersCount) {
					draft.filterTabs = [];
					for (let i = 0; i < Object.keys(action.payload.data.filtersCount).length; i++) {
						draft.filterTabs.push({
							title: Object.keys(action.payload.data.filtersCount)[i],
							count: Object.values(action.payload.data.filtersCount)[i],
						});
					}
				}
				return draft;
			case ALL_QUESTION_SET_ERROR:
				draft.allQuestionSetLoading = false;
				return draft;

			case PUBLISH_AND_UN_PUBLISH_QUESTION_SET_REQUEST:
				return draft;
			case PUBLISH_AND_UN_PUBLISH_QUESTION_SET_SUCCESS:
				for (let i = 0; i < draft.allQuestionSet.records.length; i++) {
					if (draft.allQuestionSet.records[i].id == action.payload.questionSetId) {
						draft.allQuestionSet.records[i].is_published = action.payload.publish ? false : true;
					}
				}
				draft.filterTabs.forEach((tab) => {
					const updatedCount = action.payload.data[tab.title];
					if (updatedCount !== undefined) {
						tab.count = updatedCount;
					}
				});
				return draft;
			case PUBLISH_AND_UN_PUBLISH_QUESTION_SET_ERROR:
				return draft;

			case GET_QUESTIONS_TO_SOLVE_REQUEST:
				draft.questionsToSolveLoading = true;
				return draft;
			case GET_QUESTIONS_TO_SOLVE_SUCCESS:
				draft.questionsToSolveLoading = false;
				draft.questionsToSolve = action.payload.data;
				return draft;
			case GET_QUESTIONS_TO_SOLVE_ERROR:
				draft.questionsToSolveLoading = false;
				return draft;

			case GET_LEARNER_SOLUTION_TO_SOLVE_REQUEST:
				draft.questionsToSolveLoading = true;
				return draft;
			case GET_LEARNER_SOLUTION_TO_SOLVE_SUCCESS:
				draft.questionsToSolveLoading = false;
				draft.questionsToSolve = action.payload.data;
				return draft;
			case GET_LEARNER_SOLUTION_TO_SOLVE_ERROR:
				draft.questionsToSolveLoading = false;
				return draft;

			case GET_QUESTIONS_TO_REVIEW_REQUEST:
				draft.questionsToSolveLoading = true;
				return draft;
			case GET_QUESTIONS_TO_REVIEW_SUCCESS:
				draft.questionsToSolveLoading = false;
				draft.questionsToSolve = action.payload.data;
				return draft;
			case GET_QUESTIONS_TO_REVIEW_ERROR:
				draft.questionsToSolveLoading = false;
				return draft;

			case GET_QUESTIONS_TO_REVIEW_TEACHER_REQUEST:
				draft.questionsToSolveLoading = true;
				return draft;
			case GET_QUESTIONS_TO_REVIEW_TEACHER_SUCCESS:
				draft.questionsToSolveLoading = false;
				draft.questionsToSolve = action.payload.data;
				return draft;
			case GET_QUESTIONS_TO_REVIEW_TEACHER_ERROR:
				draft.questionsToSolveLoading = false;
				return draft;

			case SAVE_LEARNER_SOLUTION_REQUEST:
				draft.saveLearnerSolutionLoading = true;
				return draft;
			case SAVE_LEARNER_SOLUTION_SUCCESS:
				draft.saveLearnerSolutionLoading = false;
				return draft;
			case SAVE_LEARNER_SOLUTION_ERROR:
				draft.saveLearnerSolutionLoading = false;
				return draft;

			case INITIATE_SOLVE_QUESTIONS:
				draft.questionAnswers.questionAnswers = draft.questionsToSolve.questionGroups.flatMap((group) =>
					group.questions.map((question) => ({
						answer: "",
						files: [],
						groupId: group.id,
						submission_type: question?.type?.name,
						questionContextId: question.module_context_question_id,
						groupContextId: group.module_context_group_id,
						isCorrect: false,
					}))
				);
				return draft;

			case FILL_DATA_TO_SOLVE:
				ind = draft.questionAnswers.questionAnswers.findIndex((el) => el.questionContextId == action.payload.questionContextId);
				if (ind !== -1) {
					let data = draft.questionAnswers.questionAnswers[ind];

					if (action.payload.questionType == "MULTIPLE_CHOICES_MULTIPLE_ANSWERS") {
						// split the answer into an array of characters
						const answerSet = new Set(
							data.answer
								.split("")
								.map((char) => {
									const num = Number(char);
									return !isNaN(num) ? num : null; // convert non-numeric characters to null
								})
								.filter((num) => num !== null)
						); // remove null values

						// check if the value is selected
						const isIdSelected = answerSet.has(+action.payload.value);
						// toggle the value
						if (isIdSelected) {
							answerSet.delete(action.payload.value);
						} else {
							answerSet.add(action.payload.value);
						}

						// join the set back into a string
						data.answer = Array.from(answerSet).join(",");
						data["isCorrect"] = action.payload.isCorrect;
					} else if (action.payload.questionType == "MATCH") {
						const result = correctAnswersWhenSolvingInMatchQuestion(action.payload.value);
						data["answer"] = result.join(",");
						data["isCorrect"] = action.payload.isCorrect;
					} else {
						data[action.payload.key] = action.payload.value;
						data["isCorrect"] = action.payload.isCorrect;
					}
				}
				return draft;

			case FILL_DATA_TO_SOLVE_IN_ORDERING_QUESTION:
				ind = draft.questionsToSolve.questionGroups.findIndex((el) => el.id == action.payload.questionGroupId);
				if (ind !== -1) {
					indd = draft.questionsToSolve.questionGroups[ind].questions.findIndex((ell) => ell.id == action.payload.questionId);

					if (indd !== -1) {
						index = draft.questionsToSolve.questionGroups[ind].questions[indd].sequencingStatements.findIndex(
							(item) => item.order == action.payload.droppableOrder
						);

						if (index !== -1) {
							draft.questionsToSolve.questionGroups[ind].questions[indd].sequencingStatements[index].orderSelected =
								action.payload.draggableOrder;

							inddd = draft.questionAnswers.questionAnswers.findIndex((el) => el.questionContextId == action.payload.questionContextId);
							if (inddd !== -1) {
								let data = draft.questionAnswers.questionAnswers[inddd];
								data["answer"] = draft.questionsToSolve.questionGroups[ind].questions[indd].sequencingStatements
									.filter((value) => value.orderSelected)
									.map((el) => el.orderSelected)
									.join(",");
								data["isCorrect"] = action.payload.isCorrect;
							}
						}
					}
				}

				return draft;

			case REMOVE_ORDERING_QUESTION:
				ind = draft.questionsToSolve.questionGroups.findIndex((el) => el.id == action.payload.questionGroupId);
				if (ind !== -1) {
					indd = draft.questionsToSolve.questionGroups[ind].questions.findIndex((ell) => ell.id == action.payload.questionId);
					if (indd !== -1) {
						index = draft.questionsToSolve.questionGroups[ind].questions[indd].sequencingStatements.findIndex(
							(item) => item.order == action.payload.draggableOrder
						);
						if (index !== -1) {
							delete draft.questionsToSolve.questionGroups[ind].questions[indd].sequencingStatements[index].orderSelected;

							inddd = draft.questionAnswers.questionAnswers.findIndex((el) => el.questionContextId == action.payload.questionContextId);
							if (inddd !== -1) {
								let data = draft.questionAnswers.questionAnswers[inddd];
								data["answer"] = draft.questionsToSolve.questionGroups[ind].questions[indd].sequencingStatements
									.filter((value) => value.orderSelected)
									.map((el) => el.orderSelected)
									.join(",");
								data["isCorrect"] = action.payload.isCorrect;
							}
						}
					}
				}
				return draft;

			case CONFIRM_THE_SOLVE_TO_THE_QUESTION:
				ind = draft.questionsToSolve.questionGroups.findIndex((el) => el.id == action.payload.questionGroupId);
				if (ind !== -1) {
					indd = draft.questionsToSolve.questionGroups[ind].questions.findIndex((ell) => ell.id == action.payload.questionId);
					if (indd !== -1) {
						draft.questionsToSolve.questionGroups[ind].questions[indd].isRequired = false;
						draft.questionsToSolve.questionGroups[ind].questions[indd].isSolved = true;
					}
				}
				return draft;

			case RESET_SOLVE_QUESTION:
				ind = draft.questionsToSolve.questionGroups.findIndex((el) => el.id == action.payload.questionGroupId);
				if (ind !== -1) {
					indd = draft.questionsToSolve.questionGroups[ind].questions.findIndex((ell) => ell.id == action.payload.questionId);

					if (indd !== -1) {
						draft.questionsToSolve.questionGroups[ind].questions[indd].isRequired = action.payload.requiredQuestion;
						draft.questionsToSolve.questionGroups[ind].questions[indd].isSolved = false;

						// i use this condition because in ordering question i add orderSelected on each object in sequencingStatements array ,
						// and after that loop on array and set orderSelected ids in [answer] key in [FILL_DATA_TO_SOLVE_IN_ORDERING_QUESTION]
						// here i need to remove all orderSelected from array when add reset button
						if (action.payload.submissionType == "SEQUENCING") {
							draft.questionsToSolve.questionGroups[ind].questions[indd]?.sequencingStatements.map(function (item) {
								delete item.orderSelected;
								return item;
							});
						}
					}
				}
				index = draft.questionAnswers.questionAnswers.findIndex((el) => el.questionContextId == action.payload.moduleContextQuestionId);
				if (index !== -1) {
					let data = draft.questionAnswers.questionAnswers[index];
					data["answer"] = "";
					data["files"] = [];
					data["isCorrect"] = false;
				}

				return draft;

			case SOLVED_PERCENTAGE:
				let count = 0;
				for (let i = 0; i < draft.questionAnswers.questionAnswers.length; i++) {
					let groupCount = 0;
					if (draft.questionAnswers.questionAnswers[i].isCorrect == true) {
						count += 1;
					}
				}
				draft.percentageOfSolvingQuestions = (count / draft.questionsToSolve.questionGroups.length) * 100;
				draft.correctQuestionsCount = count;

				// Create an object to store counts for each group
				// const groupCounts = {};

				// // Iterate over the answers and count correct answers for each group
				// for (let i = 0; i < draft.questionAnswers.questionAnswers.length; i++) {
				//   const { groupId, isCorrect } =
				//     draft.questionAnswers.questionAnswers[i];

				//   if (isCorrect) {
				//     if (!groupCounts[groupId]) {
				//       groupCounts[groupId] = 1;
				//     } else {
				//       groupCounts[groupId]++;
				//     }
				//   }
				// }

				// // Calculate the total count of correct answers and the total count of groups
				// let totalCount = 0;
				// let totalGroupCount = 0;

				// for (const groupId in groupCounts) {
				//   totalCount += groupCounts[groupId];
				//   totalGroupCount++;
				// }

				// // Calculate the percentage
				// const percentageOfSolvingQuestions =
				//   (totalCount / totalGroupCount) * 100;

				return draft;

			case INITIATE_QUESTION_SET_CORRECTION:
				draft.questionsToSolve.questionGroups.forEach((group, groupIndex) => {
					group.questions.forEach((question) => {
						draft.questionsCorrection.questionsMarks[question.learner_context_question_id] = {
							mark: question.teacherMark,
							feedback: question.teacherFeedback,
						};
					});
				});

				draft.questionsCorrection.totalTeacherMark = draft.questionsToSolve.totalTeacherMark ?? 0;
				draft.questionsCorrection.teacherFeedback = draft.questionsToSolve.teacherFeedback ?? "";
				return draft;

			case ADD_FEEDBACK_TO_QUESTION:
				draft.questionsCorrection.questionsMarks[action.payload.learnerContextQuestionId][action.payload.key] = action.payload.value;
				return draft;

			case ADD_TEACHER_FEEDBACK_TO_QS:
				draft.questionsCorrection[action.payload.key] = action.payload.value;
				return draft;

			case CALC_QUESTIONS_MARK:
				const totalMark = Object.values(draft.questionsCorrection.questionsMarks).reduce((total, question) => +total + +question.mark, 0);

				draft.finalMarkToStudent = totalMark;
				return draft;

			case CORRECTION_OF_QUESTIONS_REQUEST:
				draft.correctionOfQuestions = true;
				return draft;
			case CORRECTION_OF_QUESTIONS_SUCCESS:
				draft.correctionOfQuestions = false;
				return draft;
			case CORRECTION_OF_QUESTIONS_ERROR:
				draft.correctionOfQuestions = false;
				return draft;

			case ADD_DATA_TO_QUESTIONS_GROUP:
				ind = draft.questionSet.groups.findIndex((group) => (group.id ?? group.fakeId) == action.payload.questionGroupId);
				if (ind !== -1) {
					draft.questionSet.groups[ind][action.payload.key] = action.payload.value;
				}
				return draft;

			case CHANGE_ALL_REQUIRED_ANSWER_TO_FALSE:
				ind = draft.questionSet.groups.findIndex((group) => (group.id ?? group.fakeId) == action.payload.questionGroupId);
				if (ind !== -1) {
					if (draft.questionSet.groups[ind].questions.length > 0) {
						for (let i = 0; i < draft.questionSet.groups[ind].questions.length; i++) {
							draft.questionSet.groups[ind].questions[i][action.payload.key] = action.payload.value;
						}
					}
				}
				return false;

			case QUESTIONS_BANK_REQUEST:
				draft.questionsBankLoading = true;
				return draft;
			case QUESTIONS_BANK_SUCCESS:
				draft.questionsBankLoading = false;
				draft.questionsBank = action.payload.data;
				draft.questionsBankPagination = {
					firstPageUrl: action.payload.allData.links.first,
					lastPageUrl: action.payload.allData.links.last,
					currentPage: action.payload.allData.meta.currentPage,
					lastPage: action.payload.allData.meta.totalPages,
					prevPageUrl: action.payload.allData.links.previous,
					nextPageUrl: action.payload.allData.links.next,
					total: action.payload.allData.meta.totalPages,
				};
				return draft;
			case QUESTIONS_BANK_ERROR:
				draft.questionsBankLoading = false;
				return draft;

			case SET_FILTERS_DATA:
				draft.questionsBankFilters[action.payload.key] = action.payload.value;
				return draft;

			case LEARNER_CONTEXT_CREATOR_REQUEST:
				draft.learnerContextCreatorLoading = true;
				return draft;
			case LEARNER_CONTEXT_CREATOR_SUCCESS:
				draft.learnerContextCreatorLoading = false;
				draft.learnerContextCreator = action.payload.data;
				return draft;
			case LEARNER_CONTEXT_CREATOR_ERROR:
				draft.learnerContextCreatorLoading = false;
				return draft;

			case EXPORT_QUESTION_BANK_TO_QUESTION_EDITOR:
				if (action.payload.groupId) {
					if (!action.payload.questionSetIndex) {
						ind = draft.questionSet.groups.findIndex((el) => (el.id || el.fakeId) == action.payload.groupId);
						if (ind !== -1) {
							// draft.questionSet.groups[ind].questions.unshift(action.payload.newFormate);
							draft.questionSet.groups[ind].questions.unshift(action.payload.question.questions[0]);
						}
					} else {
						ind = draft.questionSet.groups.findIndex((el) => (el.id || el.fakeId) == action.payload.groupId);
						if (ind !== -1) {
							// draft.questionSet.groups[ind].questions.splice(action.payload.questionSetIndex, 0, action.payload.newFormate);
							draft.questionSet.groups[ind].questions.splice(action.payload.questionSetIndex, 0, action.payload.question.questions[0]);
						}
					}
				} else {
					if (!action.payload.questionSetIndex) {
						draft.questionSet.groups.unshift(action.payload.question);
					} else {
						draft.questionSet.groups.splice(action.payload.questionSetIndex, 0, action.payload.question);
					}
				}

				return draft;

			//For Question Bank Editor When Close Modal
			case EMPTY_QUESTIONS_FROM_QUESTION_SET:
				draft.questionSet.groups = [];
				return draft;

			case FILL_DATA_IN_QUESTION_SET_TO_EDIT:
				draft.questionSet.groups.push(action.payload.question);
				return draft;

			case CREATE_QUESTION_TO_QUESTIONS_BANK_REQUEST:
				draft.createQuestionToQuestionsBankLoading = true;
				return draft;
			case CREATE_QUESTION_TO_QUESTIONS_BANK_SUCCESS:
				draft.createQuestionToQuestionsBankLoading = false;
				draft.questionsBank.unshift(action.payload.data);
				return draft;
			case CREATE_QUESTION_TO_QUESTIONS_BANK_ERROR:
				draft.createQuestionToQuestionsBankLoading = false;
				return draft;

			case UPDATE_QUESTIONS_BANK_REQUEST:
				draft.createQuestionToQuestionsBankLoading = true;
				return draft;
			case UPDATE_QUESTIONS_BANK_SUCCESS:
				draft.createQuestionToQuestionsBankLoading = false;
				ind = draft.questionsBank.findIndex((el) => el.id == action.payload.questionGroupId);
				if (ind !== -1) {
					draft.questionsBank[ind] = action.payload.data;
				}
				return draft;
			case UPDATE_QUESTIONS_BANK_ERROR:
				draft.createQuestionToQuestionsBankLoading = false;
				return draft;

			case REMOVE_QUESTIONS_BANK_REQUEST:
				draft.removeQuestionsBankLoading = true;

				return draft;

			case REMOVE_QUESTIONS_BANK_SUCCESS:
				draft.removeQuestionsBankLoading = false;
				ind = draft.questionsBank.findIndex((el) => el.id == action.payload.questionGroupId);
				if (ind !== -1) {
					draft.questionsBank.splice(ind, 1);
				}
				return draft;

			case REMOVE_QUESTIONS_BANK_ERROR:
				draft.removeQuestionsBankLoading = false;
				return draft;

			case ALL_QUESTION_SET_FOR_COURSE_REQUEST:
				draft.allQuestionSetForCourseLoading = true;
				return draft;
			case ALL_QUESTION_SET_FOR_COURSE_SUCCESS:
				draft.allQuestionSetForCourseLoading = false;
				draft.allQuestionSetForCourse = action.payload.data.items;
				draft.allQuestionSetForCoursePagination = {
					firstPageUrl: action.payload.data.links.first,
					lastPageUrl: action.payload.data.links.last,
					nextPageUrl: action.payload.data.links.next,
					prevPageUrl: action.payload.data.links.previous,
					currentPage: action.payload.data.meta.currentPage,
					lastPage: action.payload.data.meta.totalPages,
					total: action.payload.data.meta.totalPages,
				};
				return draft;
			case ALL_QUESTION_SET_FOR_COURSE_ERROR:
				draft.allQuestionSetForCourseLoading = false;
				return draft;

			// case PICK_QUESTION_SET_TO_MODULE_REQUEST:
			// 	return draft;
			// case PICK_QUESTION_SET_TO_MODULE_SUCCESS:
			// 	return draft;
			// case PICK_QUESTION_SET_TO_MODULE_ERROR:
			// 	return draft;

			default:
				return draft;
		}
	});

export default questionSetReducer;
