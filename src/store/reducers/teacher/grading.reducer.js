import {
  TEACHER_VIEW_GRADING_BOOK,
  SET_GRADING_BOOK,
  VIEW_RAB_EVALUATION,
} from "store/actions/teacher/teacherTypes";
import produce from "immer";

const initState = {
  gradingbook: [],
  rabevaluations: [],
  bellCurve: [],
  bellCurveMark: [],
  bellCurveLoading: [],
};

const gradingReducer = (state = initState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case TEACHER_VIEW_GRADING_BOOK:
        draft.gradingbook = action.tmp;
        return draft;
      case SET_GRADING_BOOK:
        draft.gradingbook = action.gradingBook;
        return draft;
      case VIEW_RAB_EVALUATION:
        draft.rabevaluations = action.rabEvaluationData;
        return draft;

      default:
        return draft;
    }
  });

export default gradingReducer;
