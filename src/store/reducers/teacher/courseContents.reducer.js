import {
  GET_COURSE_CONTENT_REQUEST,
  GET_COURSE_CONTENT_SUCCESS,
  GET_COURSE_CONTENT_ERROR,
  GET_RAB_BOOKS_REQUEST,
  GET_RAB_BOOKS_SUCCESS,
  GET_RAB_BOOKS_ERROR,
  GET_COURSE_CONTENTS_BY_COURSE_ID_SUCCESS,
  GET_COURSE_CONTENTS_BY_COURSE_ID_ERROR,
  SHOW_LESSON_FROM_BOOK,
  GET_PERIOD_REQUEST,
  GET_PERIOD_SUCCESS,
  GET_PERIOD_ERROR,
  POST_ADD_LESSON_REQUEST,
  POST_ADD_LESSON_SUCCESS,
  POST_ADD_LESSON_ERROR,
  CHANGE_STATUS_LESSON_REQUEST,
  CHANGE_STATUS_LESSON_SUCCESS,
  CHANGE_STATUS_LESSON_ERROR,
  DELETE_LESSON_REQUEST,
  DELETE_LESSON_SUCCESS,
  DELETE_LESSON_ERROR,
  CONFIRM_DELETE_LESSON_REQUEST,
  CONFIRM_DELETE_LESSON_SUCCESS,
  CONFIRM_DELETE_LESSON_ERROR,
  SET_LESSON_ID,
  GET_SECTION_LESSONS_REQUEST,
  GET_SECTION_LESSONS_SUCCESS,
  GET_SECTION_LESSONS_ERROR,
  GET_SECTION_STUDENTS_REQUEST,
  GET_SECTION_STUDENTS_SUCCESS,
  GET_SECTION_STUDENTS_ERROR,
  REMOVE_LESSON_LESSON_REQUEST,
  REMOVE_LESSON_LESSON_SUCCESS,
  REMOVE_LESSON_LESSON_ERROR,
  REMOVE_BADGE_REQUEST,
  REMOVE_BADGE_SUCCESS,
  REMOVE_BADGE_ERROR,
  REFRESH_LISTER_BADGE,
  ADD_BADGE_REQUEST,
  ADD_BADGE_SUCCESS,
  ADD_BADGE_ERROR,
} from "store/actions/teacher/teacherTypes";
import produce from "immer";

const initState = {
  loading: false,
  lessonLoading: true,
  courseContents: [],
  rabBooks: [],
  periods: [],
  deleteLessonData: [],
  LESSONID: null,

  deleteLessonLoading: false,
  confirmDeleteLessonLoading: false,
  periodsLoading: false,
  error: null,
  startPage: null,
  endPage: null,
  url: "",

  sectionBadgeLoading: false,
  refreshListerBadge: false,
};

let ind;
const coursesReducer = (state = initState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      //------------Start Section Service-------------

      case ADD_BADGE_REQUEST:
        draft.sectionBadgeLoading = true;
        return draft;
      case ADD_BADGE_SUCCESS:
        draft.sectionBadgeLoading = false;
        return draft;
      case ADD_BADGE_ERROR:
        draft.sectionBadgeLoading = false;
        return draft;

      case REMOVE_BADGE_REQUEST:
        draft.sectionBadgeLoading = true;
        return draft;
      case REMOVE_BADGE_SUCCESS:
        draft.sectionBadgeLoading = false;
        return draft;
      case REMOVE_BADGE_ERROR:
        draft.sectionBadgeLoading = false;
        return draft;

      case REFRESH_LISTER_BADGE:
        draft.refreshListerBadge = action.payload.refreshListerBadge;
        return draft;
      //-------------End Section Service--------------

      case SET_LESSON_ID:
        draft.LESSONID = action.payload.lessonId;
        return draft;
      case CONFIRM_DELETE_LESSON_REQUEST:
        draft.confirmDeleteLessonLoading = true;
        return draft;

      case CONFIRM_DELETE_LESSON_SUCCESS:
        draft.confirmDeleteLessonLoading = false;

        return draft;

      case CONFIRM_DELETE_LESSON_ERROR:
        draft.confirmDeleteLessonLoading = false;
        return draft;

      case DELETE_LESSON_REQUEST:
        draft.deleteLessonLoading = true;
        return draft;

      case DELETE_LESSON_SUCCESS:
        draft.deleteLessonLoading = false;
        draft.deleteLessonData = action.payload.data;
        return draft;

      case DELETE_LESSON_ERROR:
        draft.deleteLessonLoading = false;
        return draft;

      case GET_PERIOD_REQUEST:
        draft.periodsLoading = true;
        return draft;

      case GET_PERIOD_SUCCESS:
        draft.periods = action.payload;
        draft.periodsLoading = false;
        return draft;

      case GET_PERIOD_ERROR:
        draft.periodsLoading = false;
        return draft;

      case GET_RAB_BOOKS_SUCCESS: {
        draft.rabBooks = action.payload;
        draft.loading = true;
        return draft;
      }

      case GET_RAB_BOOKS_ERROR: {
        draft.error = action.payload;
        draft.loading = false;
        return draft;
      }

      case GET_COURSE_CONTENT_REQUEST: {
        draft.lessonLoading = true;
        return draft;
      }

      case GET_COURSE_CONTENT_SUCCESS: {
        draft.lessonLoading = false;
        draft.courseContents = action.payload;

        return draft;
      }

      case GET_COURSE_CONTENT_ERROR: {
        draft.lessonLoading = false;
        draft.error = action.payload;
        return draft;
      }

      case GET_COURSE_CONTENTS_BY_COURSE_ID_SUCCESS: {
        draft.loading = false;
        draft.courseContents = action.payload;
        return draft;
      }

      case GET_COURSE_CONTENTS_BY_COURSE_ID_ERROR: {
        draft.loading = false;
        return draft;
      }

      case SHOW_LESSON_FROM_BOOK: {
        ind = draft.courseContents.findIndex(
          (el) => el.id == action.payload.LessonId
        );
        if (ind !== -1) {
          let indd = draft.courseContents[ind].course_contents.findIndex(
            (el) => el.id == action.payload.BookId
          );

          if (indd != -1) {
            let data = draft.courseContents[ind].course_contents[indd];

            draft.startPage = data.pivot.start_page;
            draft.endPage = data.pivot.end_page;
            draft.url = data.url;
          }
        }

        return draft;
      }
    }
  });

export default coursesReducer;
