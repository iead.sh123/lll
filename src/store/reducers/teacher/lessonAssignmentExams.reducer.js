import {
  GET_QUESTION_SET_STUDENT_AND_PATTERNS,
  GET_QUESTION_SET_SUGGESTED_PERIODS,
  GET_QUESTION_SET_lESSON_CLOSED_REQUEST,
  GET_QUESTION_SET_lESSON_CLOSED_SUCCESS,
  GET_QUESTION_SET_lESSON_CLOSED_ERROR,
  SHOW_REPORT_GRID_VIEW_SUCCESS,
  GET_REPORT_BY_ID_SUCCESS,
} from "store/actions/teacher/teacherTypes";

const initState = {
  loading: true,

  questionsSetStudent: {},
  questionsSetSuggesteds: [],
  questionsSetLessonClosed: [],
  ReportGird: {},
  RepostById: {},
  questionsSetLessonClosedLoading: false,
};

const lessonAssignmentExamReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case GET_QUESTION_SET_STUDENT_AND_PATTERNS:
      return {
        ...state,
        questionsSetStudent: payload,
      };

    case GET_QUESTION_SET_SUGGESTED_PERIODS:
      return {
        ...state,
        questionsSetSuggesteds: [...payload],
      };

    case GET_QUESTION_SET_lESSON_CLOSED_REQUEST:
      return {
        ...state,
        questionsSetLessonClosedLoading: true,
      };

    case GET_QUESTION_SET_lESSON_CLOSED_SUCCESS:
      return {
        ...state,
        questionsSetLessonClosed: [...payload],
        questionsSetLessonClosedLoading: false,
      };

    case GET_QUESTION_SET_lESSON_CLOSED_ERROR:
      return {
        ...state,
        questionsSetLessonClosedLoading: false,
      };

    case GET_REPORT_BY_ID_SUCCESS:
      return {
        ...state,
        RepostById: payload,
      };

    case SHOW_REPORT_GRID_VIEW_SUCCESS:
      return {
        ...state,
        ReportGird: payload,
      };

    default:
      return state;
  }
};
export default lessonAssignmentExamReducer;
