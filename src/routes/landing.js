import GAllCourses from "logic/landing/AllCourses/GAllCourses";
import GDetailTabs from "logic/GCoursesManager/Tabs/GDetailTabs";
import { GHome } from "logic/landing/Home/GHome";

const routes = [
	{
		path: "/home",
		name: "Home",
		component: GHome,
		layout: process.env.REACT_APP_BASE_URL + "/landing",
	},
	{
		path: "/all-courses",
		name: "All Courses",
		component: GAllCourses,
		layout: process.env.REACT_APP_BASE_URL + "/landing",
	},
	{
		path: "/all-course/:params",
		name: "All Courses",
		component: GAllCourses,
		layout: process.env.REACT_APP_BASE_URL + "/landing",
	},
	{
		path: "/course/:courseId/course-overview",
		name: "Course Overview",
		component: GDetailTabs,
		layout: process.env.REACT_APP_BASE_URL + "/landing",
	},
];
export default routes;
