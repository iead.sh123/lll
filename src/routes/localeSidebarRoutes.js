const localeSidebarRoutes = {
	admin: [
		{
			name: "dashboard",
			title: "Dashboard",
			order: 0,
			items: [],
			disabled: true,
		},
		{
			name: "calender",
			title: "Calender",
			order: 5,
			items: [],
			disabled: true,
		},
		{
			name: "course catalog",
			title: "Course Catalog",
			order: 206,
			items: [],
		},
		{
			name: "academics",
			title: "Academics",
			order: 205,
			items: [],
		},
		{
			name: "users_and_permissions",
			title: "Users & Permissions",
			items: [
				{
					name: "recent activities",
					title: "Recent Activities",
				},
				{
					name: "users",
					title: "Users",
					table_name: "users",
				},
				{
					name: "roles",
					title: "Roles",
					table_name: "roles",
				},
				{
					name: "permissions",
					title: "Permissions",
					table_name: "permissions",
				},
			],
			collapse: true,
			state: 4,
			order: 30,
		},
		{
			name: "users_management",
			title: "Users Management",
			items: [
				{
					name: "users_1",
					title: "Users",
				},
				{
					name: "roles_1",
					title: "Roles",
				},
				{
					name: "user_types",
					title: "User Types",
				},
				{
					name: "users_groups",
					title: "Users Groups",
				},
			],
			collapse: true,
			state: 5,
			order: 31,
		},
		{
			name: "file_management",
			title: "file management",
			items: [
				{
					name: "my_files",
					title: "My files",
				},
				{
					name: "trash",
					title: "Trash",
				},
				{
					name: "favorite",
					title: "Favorite",
				},
				{
					name: "shared",
					title: "Shared",
				},
			],
			collapse: true,
			state: 6,
			order: 32,
		},
		{
			name: "collaboration",
			title: "Collaboration",
			items: [
				{
					name: "setting",
					title: "Setting",
					disabled: true,
				},
				{
					name: "agreements",
					title: "Agreements",
					disabled: true,
				},
				{
					name: "collaboration_center",
					title: "Collaboration Center",
					disabled: true,
				},
				{
					name: "collaboration_area",
					title: "Collaboration Area",
					disabled: true,
				},
			],
			collapse: true,
			disabled: true,
			state: 11,
			order: 80,
		},
		{
			name: "community",
			title: "Community",
			order: 150,
			items: [],
			disabled: true,
		},
		{
			name: "chat",
			title: "Chat",
			order: 200,
			items: [],
			disabled: true,
		},
		// {
		// 	name: "meetings",
		// 	title: "Meetings",
		// 	order: 201,
		// 	items: [],
		// 	disabled: true,
		// },
		// {
		// 	name: "terms_management",
		// 	title: "Terms Management",
		// 	order: 202,
		// 	items: [],
		// },
		// {
		// 	name: "school_management",
		// 	title: "School Management",
		// 	order: 203,
		// 	items: [],
		// },
	],

	course_adminiStrator: [
		{
			name: "dashboard",
			title: "Dashboard",
			order: 0,
			items: [],
			disabled: true,
		},
		{
			name: "calender",
			title: "Calender",
			order: 5,
			items: [],
			disabled: true,
		},
		{
			name: "my_courses",
			title: "My Courses",
			order: 8,
			items: [],
		},
		{
			name: "courses manager",
			title: "Courses Manager",
			order: 10,
			items: [],
		},
		{
			name: "collaboration",
			title: "Collaboration",
			items: [
				{
					name: "setting",
					title: "Setting",
					disabled: true,
				},
				{
					name: "agreements",
					title: "Agreements",
					disabled: true,
				},
				{
					name: "collaboration_center",
					title: "Collaboration Center",
					disabled: true,
				},
				{
					name: "collaboration_area",
					title: "Collaboration Area",
					disabled: true,
				},
			],
			collapse: true,
			disabled: true,
			state: 11,
			order: 80,
		},
		{
			name: "community",
			title: "Community",
			order: 150,
			items: [],
			disabled: true,
		},
		{
			name: "chat",
			title: "Chat",
			order: 200,
			items: [],
			disabled: true,
		},
		{
			name: "meetings",
			title: "Meetings",
			order: 201,
			items: [],
			disabled: true,
		},
	],

	curriculum_director: [
		{
			name: "dashboard",
			title: "Dashboard",
			order: 0,
			items: [],
			disabled: true,
		},
		{
			name: "calender",
			title: "Calender",
			order: 5,
			items: [],
			disabled: true,
		},
		{
			name: "my_courses",
			title: "My Courses",
			order: 8,
			items: [],
		},
		{
			name: "collaboration",
			title: "Collaboration",
			items: [
				{
					name: "collaboration_area",
					title: "Collaboration Area",
					disabled: true,
				},
			],
			collapse: true,
			disabled: true,
			state: 11,
			order: 80,
		},
		{
			name: "community",
			title: "Community",
			order: 150,
			items: [],
			disabled: true,
		},
		{
			name: "chat",
			title: "Chat",
			order: 200,
			items: [],
			disabled: true,
		},
		{
			name: "meetings",
			title: "Meetings",
			order: 201,
			items: [],
			disabled: true,
		},
	],

	facilitator: [
		{
			name: "dashboard",
			title: "Dashboard",
			order: 0,
			items: [],
			disabled: true,
		},
		{
			name: "calender",
			title: "Calender",
			order: 5,
			items: [],
			disabled: true,
		},
		{
			name: "my_courses",
			title: "My Courses",
			order: 8,
			items: [],
		},
		{
			name: "collaboration",
			title: "Collaboration",
			items: [
				{
					name: "collaboration_area",
					title: "Collaboration Area",
					disabled: true,
				},
			],
			collapse: true,
			disabled: true,
			state: 11,
			order: 80,
		},
		{
			name: "community",
			title: "Community",
			order: 150,
			items: [],
			disabled: true,
		},
		{
			name: "chat",
			title: "Chat",
			order: 200,
			items: [],
			disabled: true,
		},
		{
			name: "meetings",
			title: "Meetings",
			order: 201,
			items: [],
			disabled: true,
		},
	],

	learner: [
		{
			name: "dashboard",
			title: "Dashboard",
			order: 0,
			items: [],
			disabled: true,
		},
		{
			name: "calender",
			title: "Calender",
			order: 5,
			items: [],
			disabled: true,
		},
		{
			name: "my_courses",
			title: "My Courses",
			order: 8,
			items: [],
		},
		{
			name: "community",
			title: "Community",
			order: 150,
			items: [],
			disabled: true,
		},
		{
			name: "chat",
			title: "Chat",
			order: 200,
			items: [],
			disabled: true,
		},
		{
			name: "meetings",
			title: "Meetings",
			order: 201,
			items: [],
			disabled: true,
		},
	],

	mentor: [],
};
export default localeSidebarRoutes;
