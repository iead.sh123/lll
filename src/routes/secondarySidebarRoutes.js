import { genericPath, baseURL } from "engine/config";
import GSyllabus from "logic/Courses/Syllabus/GSyllabus";
import GFavorite from "logic/FileManagement/GFavorite";
import GMyFiles from "logic/FileManagement/GMyFiles";
import GTrash from "logic/FileManagement/GTrash";
import GCalender from "logic/calender/GCalender";

import GRubricLister from "logic/Courses/Rubrics/GRubricLister";
import GRubricEditor from "logic/Courses/Rubrics/GRubricEditor";
import GRubricViewer from "logic/Courses/Rubrics/GRubricViewer";
import GLessonPLanLister from "logic/Courses/LessonPlans/GLessonPLanLister";
import GLessonPLanEditor from "logic/Courses/LessonPlans/GLessonPLanEditor";
import GLessonPLanViewer from "logic/Courses/LessonPlans/GLessonPLanViewer";

import GDiscussion from "logic/Courses/Discussion/GDiscussion";
import GDiscussionDetails from "logic/Courses/Discussion/GDiscussionDetails";

import GLesson from 'logic/Courses/Lesson/GLesson'
import GAddLessonDetails from 'logic/Courses/Lesson/GAddLessonDetails'
import GLessonStudent from 'logic/Courses/Lesson/Student/GLessonStudent'
import GShowLessonStudent from "logic/Courses/Lesson/Student/GShowLessonStudent"

export const SecondarySidebarRoutes = [
	// - - - - - - - - Syllabus - - - - - - - -
	{
		name: "syllabus",
		path: "/course-catalog/master-course/:courseId/syllabus",
		component: GSyllabus,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
	},
	//  - - - - - - - - Master Course Lessons - - - - - - - -
	{
		name: "lessons",
		path: "/course-catalog/master-course/:courseId/lessons",
		component: GLesson,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
	},
	{
		name: "lessons",
		path: "/course-catalog/master-course/:courseId/lessons/add/:lessonId",
		component: GAddLessonDetails,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
		invisible: true,
	},
	//  - - - - - - - - Course Lessons - - - - - - - -
	{
		name: "course lessons",
		path: "/course-catalog/:courseId/course lessons",
		component: GLesson,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
	},
	{
		name: "course lessons",
		path: "/course-catalog/:courseId/course lessons/add/:lessonId",
		component: GAddLessonDetails,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
		invisible: true,
	},
	//  - - - - - - - - Student Lessons - - - - - - 
	{
		name: "student lessons",
		path: "/course-catalog/:courseId/student lessons",
		component: GLessonStudent,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
	},
	{
		name: "student lessons",
		path:  "/course-catalog/:courseId/student lessons/show/:lessonId",
		component: GShowLessonStudent,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
		invisible: true,
	},

	// - - - - - - - - Rubric - - - - - - - -
	{
		name: "rubrics",
		path: "/course-catalog/master-course/:courseId/rubrics",
		component: GRubricLister,
		layout: `${baseURL}/${genericPath}`,
	},

	{
		name: "rubricsEditor",
		path: "/course-catalog/master-course/:courseId/rubric/:rubricId",
		component: GRubricEditor,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		name: "rubricsEditor",
		path: "/course-catalog/master-course/:courseId/rubric/add",
		component: GRubricEditor,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		name: "rubricsViewer",
		path: "/course-catalog/master-course/:courseId/rubric/:rubricId/view",
		component: GRubricViewer,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	// - - - - - - - - Files - - - - - - - -
	{
		name: "my_files",
		path: "/course-catalog/master-course/:courseId/my-files",
		component: GMyFiles,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
	},
	{
		name: "trash",
		path: "/course-catalog/master-course/:courseId/trash",
		component: GTrash,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
	},
	{
		name: "favorite",
		path: "/course-catalog/master-course/:courseId/favorite",
		component: GFavorite,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
	},

	// - - - - - - - - Lesson Plans - - - - - - - -
	{
		name: "lesson_plans",
		path: "/course-catalog/master-course/:courseId/lesson-plans",
		component: GLessonPLanLister,
		layout: `${baseURL}/${genericPath}`,
	},

	{
		name: "lessonPlanEditor",
		path: "/course-catalog/master-course/:courseId/lesson-plan/:lessonPlanId",
		component: GLessonPLanEditor,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		name: "lessonPlanEditor",
		path: "/course-catalog/master-course/:courseId/lesson-plan/add",
		component: GLessonPLanEditor,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		name: "lessonPlanViewer",
		path: "/course-catalog/master-course/:courseId/lesson-plan/:lessonPlanId/view",
		component: GLessonPLanViewer,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	// - - - - - - - - Calendar - - - - - - - -
	{
		name: "calander",
		path: "/course-catalog/course/:courseId/calendar",
		component: GCalender,
		layout: `${baseURL}/${genericPath}`,
		types: ["student,learner"],
	},

	//  - - - - - - - - Discussion - - - - - - - -
	{
		name: "discussion",
		path: "/course-catalog/master-course/:courseId/discussion",
		component: GDiscussion,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
	},
	{
		name: "discussion",
		path: "/course-catalog/master-course/:courseId/discussion/:discussionId",
		component: GDiscussionDetails,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
		invisible: true,
	},
];
