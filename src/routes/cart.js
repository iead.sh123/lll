import PaymentSuccess from "components/widgets/navbar/cart/Payment/PaymentSuccess";
import Payment from "components/widgets/navbar/cart/Payment";

const routes = [
	{
		path: "/payment",
		name: "Payment",
		component: Payment,
		layout: process.env.REACT_APP_BASE_URL + "/cart",
	},
	{
		path: "/payment-success/:orderId",
		name: "Payment",
		component: PaymentSuccess,
		layout: process.env.REACT_APP_BASE_URL + "/cart",
	},
];
export default routes;
