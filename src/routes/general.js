import { baseURL, genericPath } from "engine/config";

import GNotifications from "logic/General/GNotifications";

const routes = [
  {
    name: "notifications",
    title: "notifications",
    path: "/notifications",
    mini: "SR",
    component: GNotifications,
    layout: `${baseURL}/${genericPath}`,
    invisible: true,
  },
];

export default routes;
