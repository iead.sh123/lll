import { baseURL, genericPath } from "engine/config";
import { GeneralTest } from "components/Global/RComs/Tests/GeneralTest";
import { FileTest } from "components/Global/RComs/Tests/FileTest";
import GLessonPlanPrinting from "logic/Courses/CourseManagement/LessonPlans/GLessonPlanPrinting";
import GenericUILister from "logic/GenericUi/GenericUILister";
import GenericAddContainer from "logic/GenericUi/GenericAddContainer";
import GenericEditor from "logic/GenericUi/GenericEditor";
import LearningObject from "logic/LearningObject/LearningObject";
import GNotifications from "logic/General/GNotifications";
import GNotification from "logic/General/GNotification";
import GCalender from "logic/calender/GCalender";
import IM from "views/Teacher/IM/IM";
import IMTest from "views/Teacher/IM/IMTest";
import GDiscussion from "logic/Discussion/GDiscussion";
import GDiscussionFilter from "logic/Discussion/GDiscussionFilter";
import GSchoolInitiation from "logic/SchoolInitiation/GSchoolInitiation";
import Users from "logic/UsersAndPermissions/Users/Users";
import ManagementTabs from "logic/UsersAndPermissions/ManagementTabs/ManagementTabs";
import GRoleLister from "logic/UsersAndPermissions/Roles/GRoleLister";
import PermissionCategoriesTabs from "logic/UsersAndPermissions/Permissions/PermissionCategoriesTabs";
import RecentActivities from "logic/UsersAndPermissions/RecentActivities/RecentActivities";
import GAddRole from "logic/UsersAndPermissions/Roles/GAddRole";
import GCoursesManager from "logic/GCoursesManager/GCoursesManager";
import GDetailTabs from "logic/GCoursesManager/Tabs/GDetailTabs";
import { GCollaborationAreaContents } from "logic/Collaboration/GCollaborationAreaContents";
// import { GCollaborationAgreements } from "logic/Collaboration/GCollaborationAgreements";
// import { GCollaborationSetting } from "logic/Collaboration/GCollaborationSetting";
import GCollaborationSetting from "logic/GCollaboration/GCollaborationSetting";
import { GCollaborationCenter } from "logic/Collaboration/GCollaborationCenter";
// import { GCollaborationArea } from "logic/Collaboration/GCollaborationArea";
import { GCollaborationTest } from "../logic/Collaboration/GCollaborationTest";
import { CollaborationTest } from "components/Global/RComs/Tests/CollaborationTest";

import SwitchAccount from "view/User/switch-account";
import GMyCourses from "logic/Courses/MyCourses/GMyCourses";
import Profile from "view/User/profile";
import GCurricula from "logic/Courses/MyCurricula/GMyCurricula";
import GDashboard from "logic/GDashboard/GDashboard";
import GSchoolCreating from "logic/SchoolManagement/GSchoolCreating";
import GSchoolCreatingV2 from "logic/SchoolManagementV2/GSchoolCreatingV2";
import GSchoolLister from "logic/SchoolManagement/GSchoolLister";
import GSchoolListerV2 from "logic/SchoolManagementV2/GSchoolListerV2";
import GTermsManagement from "logic/TermsManagement/GTermsManagement";
import GPaymentPolicy from "logic/Organization/PaymentPolicy/GPaymentPolicy";
import GArchive from "logic/Organization/Archive/GArchive";
import GCollaborationArea from "logic/GCollaboration/GCollaborationArea";
import GCollaborationAgreements from "logic/GCollaboration/GCollaborationAgreements";
import GMyFiles from "logic/FileManagement/GMyFiles";
import GFavorite from "logic/FileManagement/GFavorite";
import GShared from "logic/FileManagement/GShared";
import GRoels from "logic/UsersManagement/Roles";
import GUsers from "logic/UsersManagement/Users";
import GUserTypes from "logic/UsersManagement/UserTypes";
import GUserGroups from "logic/UsersManagement/UserGroups";
import GViewRole from "logic/UsersManagement/Roles/GViewRole";
import GCreateRole from "logic/UsersManagement/Roles/GCreateRole";
import GCreateUserType from "logic/UsersManagement/UserTypes/GCreateUserType";
import GViewUserType from "logic/UsersManagement/UserTypes/GViewUserType";
import GCreateUser from "logic/UsersManagement/Users/GCreateUser";
import GViewUser from "logic/UsersManagement/Users/GViewUser";
import Departments from "logic/DepartmentsAndPrograms/Departments/Departments";
import GCreateUserGroup from "logic/UsersManagement/UserGroups/GCreateUserGroup";
import GViewUserGroup from "logic/UsersManagement/UserGroups/GViewUserGroup";
import GTrash from "logic/FileManagement/GTrash";
import GCoursesCatalog2 from "logic/Courses/CourseCatalog/GCoursesCatalog2";
import GSyllabus from "logic/Courses/Syllabus/GSyllabus";
import GCoursesCatalog from "logic/CourseCatalog/GCoursesCatalog";


const routes = [
	//______________Profile______________
	{
		path: "/user/profile",
		component: Profile,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		path: "/user/switch-account",
		component: SwitchAccount,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	//______________courses manager______________

	// ----Edit Course With Category----
	{
		path: "/courses-manager/editor/course/:courseId/category/:categoryId/:status",
		icon: "nc-icon nc-world-2",
		component: GDetailTabs,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	// ----Create New Course With Category----
	{
		path: "/courses-manager/editor/category/:categoryId/:status",
		icon: "nc-icon nc-world-2",
		component: GDetailTabs,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	// ----Edit Course----
	{
		path: "/courses-manager/editor/course/:courseId/:status",
		icon: "nc-icon nc-world-2",
		component: GDetailTabs,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	// ----Create New Course----
	{
		path: "/courses-manager/editor/:status",
		icon: "nc-icon nc-world-2",
		component: GDetailTabs,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	//___________________School Initiation___________________
	{
		path: "/school-initiation/:sIType/:sITypeId/:tabTitle/:semesterId",
		icon: "nc-icon nc-calendar-60",
		component: GSchoolInitiation,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		path: "/school-initiation/:sIType/:sITypeId/:tabTitle",
		icon: "nc-icon nc-calendar-60",
		component: GSchoolInitiation,
		layout: `${baseURL}/${genericPath}`,
	},

	{
		name: "notifications",
		path: "/notifications",
		mini: "SR",
		component: GNotifications,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		name: "notifications",
		path: "/notification/:group_id",
		mini: "SR",
		component: GNotification,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	//_____________________ School Management______________________

	{
		name: "school_management",
		path: "/school-management/create",
		title: "School Management",
		icon: "fas fa-school",
		component: GSchoolCreatingV2,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		path: "/school-management",
		component: GSchoolListerV2,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	//___________________IM___________________
	{
		name: "IM",
		path: "/IM",
		component: IM,
		layout: `${baseURL}/${genericPath}`,
		icon: "nc-icon nc-world-2",
	},
	{
		name: "IM",
		path: "/IMTest",
		component: IMTest,
		layout: `${baseURL}/${genericPath}`,
		icon: "nc-icon nc-world-2",
	},
	{
		name: "FileTest",
		path: "/FileTest",
		component: FileTest,
		layout: `${baseURL}/${genericPath}`,
		icon: "nc-icon nc-world-2",
	},
	{
		name: "GeneralTest",
		path: "/GeneralTest",
		component: GeneralTest,
		layout: `${baseURL}/${genericPath}`,
		icon: "nc-icon nc-world-2",
	},
	{
		name: "CollaborationTest",
		path: "/CollaborationTest",
		component: CollaborationTest,
		layout: `${baseURL}/${genericPath}`,
		icon: "fa-solid fa-earth-americas",
	},
	{
		name: "calender",
		path: "/calender",
		component: GCalender,
		layout: `${baseURL}/${genericPath}`,
		icon: "fa-solid fa-calendar-days",
	},

	//___________________Learning Objects___________________
	{
		name: "LearningObject",
		path: "/section/learning-object/section/:sectionId",
		icon: "nc-icon nc-world-2",
		component: LearningObject,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		name: "LearningObject",
		path: "/lesson/learning-object/:lessonId",
		icon: "nc-icon nc-world-2",
		component: LearningObject,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		name: "LearningObject",
		path: "/lesson-content/learning-object/lesson/:lessonId",
		icon: "nc-icon nc-world-2",
		component: LearningObject,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		name: "LearningObject",
		path: "/lesson-content/learning-object/lesson-content/:lessonContentId",
		icon: "nc-icon nc-world-2",
		component: LearningObject,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	//___________________Lesson Plan___________________
	{
		name: "LessonPlanPrinting",
		path: "/lesson-plan/:lessonId/printing",
		icon: "nc-icon nc-world-2",
		component: GLessonPlanPrinting,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	//___________________Generic Ui___________________
	{
		path: "/add/:entity_specific_name/:tabTitle?",
		name: "Add",
		symbol: "a",
		component: GenericAddContainer,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		path: "/manage/:entity_specific_name/:entity_specific_id/:tabTitle?",
		name: "Manage",
		symbol: "M",
		component: GenericEditor,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	//___________________Application Management___________________
	{
		name: "application_management",
		title: "Application Management",
		icon: "fa-solid fa-gauge-high",
	},
	{
		table_name: "organization_types",
		name: "organizations_types",
		title: "Organizations Types",
		path: "/organizations_types",
		mini: "OT",
		component: GenericUILister,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		table_name: "organizations",
		name: "organizations",
		title: "Organizations",
		path: "/organizations",
		mini: "O",
		component: GenericUILister,
		layout: `${baseURL}/${genericPath}`,
	},
	// {
	// 	table_name: "user_types",
	// 	name: "user_types",
	// 	title: "User Types",
	// 	path: "/user_types",
	// 	mini: "Ut",
	// 	component: GenericUILister,
	// 	layout: `${baseURL}/${genericPath}`,
	// },
	{
		table_name: "app_grade_levels",
		name: "app_grade_levels",
		title: "App Grade Levels",
		path: "/app_grade_levels",
		mini: "Ut",
		component: GenericUILister,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		table_name: "grade_level_types",
		name: "grade_level_types",
		title: "Grade Level Types",
		path: "/grade_level_types",
		mini: "Ut",
		component: GenericUILister,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		table_name: "education_systems",
		name: "education_systems",
		title: "Education Systems ",
		path: "/education_systems",
		mini: "Ut",
		component: GenericUILister,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		table_name: "calender_event_types",
		name: "calender_event_types",
		title: "Calender Event Types ",
		path: "/calender_event_types",
		mini: "Ce",
		component: GenericUILister,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		table_name: "main_courses",
		name: "main_courses",
		title: "Main Courses ",
		path: "/main_courses",
		mini: "Ce",
		component: GenericUILister,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		table_name: "sidebar_categories",
		name: "sidebar_categories",
		title: "Sidebar Categories ",
		path: "/sidebar_categories",
		mini: "Ce",
		component: GenericUILister,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		table_name: "sidebar_items",
		name: "sidebar_items",
		title: "Sidebar Items ",
		path: "/sidebar_items",
		mini: "Ce",
		component: GenericUILister,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		table_name: "sharable_content_types",
		name: "sharable_content_types",
		title: "Sharable Content Types",
		path: "/sharable_content_types",
		mini: "Ce",
		component: GenericUILister,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		table_name: "scope_categories",
		name: "scope_categories",
		title: "Scope Categories",
		path: "/scope_categories",
		mini: "SG",
		component: GenericUILister,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		table_name: "scopes",
		name: "scopes",
		title: "Scope scopes",
		path: "/scopes",
		mini: "SC",
		component: GenericUILister,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		table_name: "roles_categories",
		name: "roles_categories",
		title: "Role Categories",
		path: "/roles_categories",
		mini: "RC",
		component: GenericUILister,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		table_name: "permission_categories",
		name: "permission_categories",
		title: "Permission Categories",
		path: "/permission_categories",
		mini: "SC",
		component: GenericUILister,
		layout: `${baseURL}/${genericPath}`,
	},

	{
		table_name: "frontend_components",
		name: "frontend_components",
		title: "Frontend Components",
		path: "/frontend_components",
		mini: "FC",
		component: GenericUILister,
		layout: `${baseURL}/${genericPath}`,
	},

	//___________________Terms Manager___________________
	{
		name: "terms_management",
		title: "Terms Management",
		path: "/terms-management",
		icon: "fas fa-graduation-cap",
		component: GTermsManagement,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		name: "academics",
		title: "Academics",
		path: "/academics",
		icon: "fas fa-graduation-cap",
		component: GTermsManagement,
		layout: `${baseURL}/${genericPath}`,
	},

	//______________________________________________________

	//___________________Manage School___________________
	// {
	// 	name: "manage_school",
	// 	title: "Manage School",
	// 	icon: "fa-solid fa-school",
	// },

	// {
	// 	table_name: "education_stages",
	// 	name: "education_stages",
	// 	title: "Education Stages",
	// 	path: "/education_stages",
	// 	mini: "Ut",
	// 	component: GenericUILister,
	// 	layout: `${baseURL}/${genericPath}`,
	// },
	// {
	// 	table_name: "grade_scales",
	// 	name: "grade_scales",
	// 	title: "Grade Scales",
	// 	path: "/grade_scales",
	// 	mini: "Ut",
	// 	component: GenericUILister,
	// 	layout: `${baseURL}/${genericPath}`,
	// },
	// {
	// 	table_name: "grade_scale_details",
	// 	name: "grade_scale_details",
	// 	title: "Grade Scale Details",
	// 	path: "/grade_scale_details",
	// 	mini: "Ut",
	// 	component: GenericUILister,
	// 	layout: `${baseURL}/${genericPath}`,
	// },
	// {
	// 	table_name: "periods",
	// 	name: "periods",
	// 	title: "Periods",
	// 	path: "/periods",
	// 	mini: "PR",
	// 	component: GenericUILister,
	// 	layout: `${baseURL}/${genericPath}`,
	// },
	// {
	// 	table_name: "grade_levels",
	// 	name: "grade_levels",
	// 	title: "Grade Levels",
	// 	path: "/grade_levels",
	// 	mini: "Ut",
	// 	component: GenericUILister,
	// 	layout: `${baseURL}/${genericPath}`,
	// },

	//___________________Permission & Securable___________________
	{
		name: "permissions_and_securables",
		title: "Permissions And Securables",
		icon: "fa-solid fa-user-lock",
	},
	// {
	//   table_name: "permissions",
	//   name: "permissions",
	//   title: "Permissions",
	//   path: "/permissions",
	//   mini: "P",
	//   component: GenericUILister,
	//   layout: `${baseURL}/${genericPath}`,
	// },
	{
		table_name: "securables",
		name: "Securables",
		title: "securables",
		path: "/securables",
		mini: "s",
		component: GenericUILister,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		table_name: "secure_tables",
		name: "secure_tables",
		title: "Secure Tables",
		path: "/secure_tables",
		mini: "ST",
		component: GenericUILister,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		table_name: "secure_actions",
		name: "secure_actions",
		title: "Secure Actions",
		path: "/secure_actions",
		mini: "SA",
		component: GenericUILister,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		table_name: "secure_reports",
		name: "secure_reports",
		title: "Secure Reports",
		path: "/secure_reports",
		mini: "SR",
		component: GenericUILister,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		table_name: "secure_records",
		name: "secure_records",
		title: "Secure Records",
		path: "/secure_records",
		mini: "SR",
		component: GenericUILister,
		layout: `${baseURL}/${genericPath}`,
	},
	//-------------------------------------------------Collaboration
	{
		name: "collaboration",
		title: "Collaboration",
		icon: "fa-solid fa-earth-americas",
	},
	{
		name: "setting",
		title: "Setting",
		path: "/setting",
		mini: "P",
		component: GCollaborationSetting,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		name: "agreements",
		title: "Agreements",
		path: "/agreements",
		mini: "P",
		component: GCollaborationAgreements,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		name: "collaboration_center",
		title: "Collaboration Center",
		path: "/collaboration_center",
		mini: "P",
		component: GCollaborationCenter,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		name: "collaboration_area",
		title: "Collaboration Area",
		path: "/collaboration_area",
		mini: "P",
		component: GCollaborationArea,
		layout: `${baseURL}/${genericPath}`,
	},

	{
		name: "collaboration_area_contents",
		title: "collaboration_area_contents",
		path: "/collaboration_area_contents/:context_name/:context_id/:content_type/:singleSelect?",
		mini: "P",
		component: GCollaborationAreaContents,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		name: "collaboration_area_contents",
		title: "collaboration_area_contents",
		path: "/collaboration_area_contents/:agreement_id",
		mini: "P",
		component: GCollaborationAreaContents,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		name: "collaboration_area_contents",
		title: "collaboration_area_contents",
		path: "/collaboration_area_contents",
		mini: "P",
		component: GCollaborationAreaContents,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		name: "collaboration_area_contents_test",
		title: "collaboration_area_contents_test",
		path: "/collaboration_area_contents_test",
		mini: "P",
		component: GCollaborationTest,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	//___________________Community___________________

	{
		name: "specific-posts",
		path: `/specific-posts/:userId`,
		component: GDiscussion,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		name: "search-community",
		path: `/search-community/:searchQuery`,
		component: GDiscussion,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		name: "filter-community",
		path: `/filter-community/:categoryName`,
		component: GDiscussionFilter,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		name: "community",
		title: "Community",
		path: "/community",
		icon: "fa-solid fa-users",
		component: GDiscussion,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		name: "chat",
		title: "Chat",
		path: "/IM",
		icon: "fa-solid fa-comments",
		component: IM,
		layout: `${baseURL}/${genericPath}`,
	},
	//______________courses manager______________

	//___________________Users & Permissions___________________

	{
		name: "users_and_permissions",
		title: "Users & Permissions",
		icon: "fa-solid fa-user-lock",
	},
	{
		path: "/users-and-permissions/:userAndPermissionType/:permissionId/category/:categoryId/:tabTitle",
		mini: "Ut",
		component: ManagementTabs,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		name: "recent activities",
		title: "Recent Activities",
		path: "/users-and-permissions/recent-activities",
		mini: "Ut",
		component: RecentActivities,
		layout: `${baseURL}/${genericPath}`,
	},

	{
		path: "/users-and-permissions/:userAndPermissionType/:organizationUserId/:tabTitle/:tabName",
		mini: "Ut",
		component: ManagementTabs,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		name: "users",
		title: "Users",
		path: "/users-and-permissions/user-type/:tabTitle",
		mini: "Ut",
		component: Users,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		path: "/users-and-permissions/:userAndPermissionType/:roleId/:tabTitle",
		mini: "Ut",
		component: ManagementTabs,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		path: "/users-and-permissions/role/add",
		mini: "Ut",
		component: GAddRole,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		name: "roles",
		title: "Roles",
		path: "/users-and-permissions/roles",
		mini: "Ut",
		component: GRoleLister,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		name: "permissions",
		title: "Permissions",
		path: "/users-and-permissions/permissions/:categoryId",
		mini: "Ut",
		component: PermissionCategoriesTabs,
		layout: `${baseURL}/${genericPath}`,
	},

	//___________________End Users & Permissions___________________

	{
		name: "my_courses",
		title: "My Courses",
		path: "/my-courses",
		icon: "fa-solid fa-book-open",
		component: GMyCourses,
		layout: `${baseURL}/${genericPath}`,
	},

	{
		name: "my_curricula",
		title: "My Curricula",
		path: "/my-curricula",
		icon: "fa-solid fa-book-open",
		component: GCurricula,
		layout: `${baseURL}/${genericPath}`,
	},

	{
		name: "dashboard",
		title: "Dashboard",
		path: "/dashboard",
		icon: "fa-solid fa-book-open",
		component: GDashboard,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		name: "meetings",
		title: "Meetings",
		path: "/meetings",
		icon: "fas fa-handshake",
		component: <></>,
		layout: `${baseURL}/${genericPath}`,
	},

	//- - - - - - - - - - - - Payment- - - - - - - - - - - -
	{
		name: "payment",
		title: "Payment",
		icon: "fa-solid fa-hand-holding-dollar",
	},

	{
		name: "discounts_and_payment_policy",
		title: "Discounts And Payment Policy",
		path: "/organization/payment-policy-and-discounts",
		mini: "D",
		component: GPaymentPolicy,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		name: "archive",
		title: "Archive",
		path: "/organization/archive",
		mini: "A",
		component: GArchive,
		layout: `${baseURL}/${genericPath}`,
	},

	//- - - - - - - - - - - - File Management- - - - - - - - - - - -
	{
		name: "file_management",
		title: "File Management",
		icon: "fas fa-folder-open",
	},
	{
		path: "/file-management/my-files/:folderId",
		component: GMyFiles,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		name: "my_files",
		title: "My Files",
		path: "/file-management/my-files",
		mini: "M",
		component: GMyFiles,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		name: "trash",
		title: "trash",
		path: "/file-management/trash",
		mini: "T",
		component: GTrash,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		name: "favorite",
		title: "Favorite",
		path: "/file-management/favorite",
		mini: "F",
		component: GFavorite,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		name: "shared",
		title: "Shared",
		path: "/file-management/shared",
		mini: "S",
		component: GShared,
		layout: `${baseURL}/${genericPath}`,
	},
	//------------------------------------Users Management-----------------------------
	{
		name: "users_management",
		title: "Users Management",
		icon: "fas fa-user-lock",
	},
	{
		path: "/users-management/users/add",
		component: GCreateUser,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		path: "/users-management/users/:userId",
		component: GViewUser,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		name: "users_1",
		title: "Users",
		path: "/users-management/users",
		mini: "U",
		component: GUsers,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		path: "/users-management/roles/add",
		component: GCreateRole,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		path: "/users-management/roles/:roleId",
		component: GViewRole,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		name: "roles_1",
		title: "Roles",
		path: "/users-management/roles",
		mini: "R",
		component: GRoels,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		path: "/users-management/user-types/add",
		component: GCreateUserType,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		path: "/users-management/user-types/:organizationUserTypeId",
		component: GViewUserType,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		name: "user_types",
		title: "User Types",
		path: "/users-management/user-types",
		mini: "UT",
		component: GUserTypes,
		layout: `${baseURL}/${genericPath}`,
	},
	{
		path: "/users-management/user-groups/add",
		component: GCreateUserGroup,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		path: "/users-management/user-groups/:groupId",
		component: GViewUserGroup,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		name: "users_groups",
		title: "Users Groups",
		path: "/users-management/user-groups",
		mini: "UG",
		component: GUserGroups,
		layout: `${baseURL}/${genericPath}`,
	},
	//--------------------------------------Programs-------------------------------------------
	{
		name: "departments",
		title: "Departments",
		path: "/departments",
		mini: "DP",
		component: Departments,
		layout: `${baseURL}/${genericPath}`,
	},

	//- - [Course Catalog] - - - - - - - - - Course - - - - - - - - - - - -

	// {
	// 	path: "/courses-catalog/category/:categoryId",
	// 	icon: "nc-icon nc-world-2",
	// 	component: GCoursesCatalog,
	// 	layout: `${baseURL}/${genericPath}`,
	// 	invisible: true,
	// },
	// {
	// 	name: "course catalog",
	// 	path: "/courses-catalog",
	// 	title: "Course Catalog",
	// 	icon: "nc-icon nc-world-2",
	// 	component: GCoursesCatalog,
	// 	layout: `${baseURL}/${genericPath}`,
	// 	invisible: true,
	// },

	{
		path: "/courses-catalog/department/:departmentId",
		icon: "nc-icon nc-world-2",
		component: GCoursesCatalog2,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		name: "course catalog",
		path: "/courses-catalog",
		title: "Course Catalog",
		icon: "nc-icon nc-world-2",
		component: GCoursesCatalog2,
		layout: `${baseURL}/${genericPath}`,
	},

	//- - [Course Manager] - - - - - - - - - Course - - - - - - - - - - - -
	{
		path: "/courses-manager/department/:departmentId",
		icon: "nc-icon nc-world-2",
		component: GCoursesManager,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		name: "courses manager",
		title: "Courses Manager",
		path: "/courses-manager",
		icon: "fa-solid fa-folder-plus",
		component: GCoursesManager,
		layout: `${baseURL}/${genericPath}`,
	},
];

export default routes;
