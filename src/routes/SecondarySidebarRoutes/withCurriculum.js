import { baseURL, genericPath } from "engine/config";
import GUnitPlanEditor from "logic/Courses/CourseManagement/UnitPlans/UnitPlanEditor/GUnitPlanEditor";
import GLessonPlan from "logic/Courses/CourseManagement/LessonPlans/GLessonPlan";
import GLessonPlanLister from "logic/Courses/CourseManagement/LessonPlans/GLessonPlanLister";
import GUnitPlanLister from "logic/Courses/CourseManagement/UnitPlans/UnitPlanLister/GUnitPlanLister";
import GBadges from "logic/Courses/CourseManagement/Badges/GBadges";
import GBooksLister from "logic/SchoolInitiation/Books/GBooksLister";
import GQuestionSetLister from "logic/Courses/CourseManagement/QuestionSet/GQuestionSetLister";
import GQuestionSetEditor from "logic/Courses/CourseManagement/QuestionSet/GQuestionSetEditor";
import GQuestionsBank from "logic/Courses/CourseManagement/QuestionSet/GQuestionsBank";
import GLessonPlanViewer from "logic/Courses/CourseManagement/LessonPlans/GLessonPlanViewer";
import GUnitPlanViewer from "logic/Courses/CourseManagement/UnitPlans/UnitPlanViewer/GUnitPlanViewer";
import GRubricLister from "logic/Courses/Rubrics/GRubricLister";
import GRubricEditor from "logic/Courses/Rubrics/GRubricEditor";
import GStudentsDetailsByRubric from "logic/Courses/Rubrics/GStudentsDetailsByRubric";
import GGradeStudentByStudent from "logic/Courses/Rubrics/GGradeStudentByStudent";

export const routes = [
	// --------- Books ---------
	{
		name: "books",
		path: "/course-management/curricula/:curriculumId/books",
		component: GBooksLister,
		layout: `${baseURL}/${genericPath}`,
		type: "senior_teacher",
	},
	// _________________________________________________________________

	// --------- Start Unit Plans ---------
	{
		name: "unit_plans",
		path: "/course-management/curricula/:curriculumId/unit-plans",
		component: GUnitPlanLister,
		layout: `${baseURL}/${genericPath}`,
		type: "senior_teacher",
	},

	{
		name: "unit_plans",
		path: "/course-management/curricula/:curriculumId/editor/unit-plans",
		component: GUnitPlanEditor,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
		type: "senior_teacher",
	},

	{
		name: "unit_plans",
		path: "/course-management/curricula/:curriculumId/editor/unit-plans/:unitPlanId",
		component: GUnitPlanEditor,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
		type: "senior_teacher",
	},

	{
		name: "unit_plans",
		path: "/course-management/curricula/:curriculumId/editor/unit-plans/:unitPlanId/:params",
		component: GUnitPlanEditor,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
		type: "senior_teacher",
	},

	{
		name: "unit_plans",
		path: "/course-management/curricula/:curriculumId/unit-plans/:unitPlanId",
		component: GUnitPlanViewer,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
		type: "senior_teacher",
	},

	// --------- End Unit Plans ---------

	// --------- Start Lesson Plans ---------

	{
		name: "lesson_plans",
		path: "/course-management/curricula/:curriculumId/lesson-plans",
		component: GLessonPlanLister,
		layout: `${baseURL}/${genericPath}`,
		type: "senior_teacher",
	},

	{
		name: "lesson_plans",
		path: "/course-management/curricula/:curriculumId/editor/lesson-plans",
		component: GLessonPlan,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
		type: "senior_teacher",
	},

	{
		name: "lesson_plans",
		path: "/course-management/curricula/:curriculumId/editor/lesson-plans/:lessonPlanId",
		component: GLessonPlan,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
		type: "senior_teacher",
	},

	{
		name: "lesson_plan",
		path: "/course-management/curricula/:curriculumId/editor/lesson-plan/:lessonPlanId",
		component: GLessonPlanViewer,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	// _________________________________________________________________

	// --------- Rubric ---------

	{
		name: "rubrics",
		path: "/course-management/curricula/:curriculumId/cohort/:cohortId/rubrics",
		component: GRubricLister,
		layout: `${baseURL}/${genericPath}`,
		disabled: true,
	},

	{
		name: "rubricsEditor",
		path: "/course-management/curricula/:curriculumId/cohort/:cohortId/editor/rubrics",
		component: GRubricEditor,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		name: "rubricsEditor",
		path: "/course-management/curricula/:curriculumId/cohort/:cohortId/editor/rubric/:rubricId",
		component: GRubricEditor,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		name: "rubricsViewer",
		path: "/course-management/curricula/:curriculumId/cohort/:cohortId/rubric/:rubricId/viewer",
		component: GRubricEditor,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		name: "studentsDetails",
		path: "/course-management/curricula/:curriculumId/cohort/:cohortId/rubric/:rubricId/students",
		component: GStudentsDetailsByRubric,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		name: "gradeStudentByStudent",
		path: "/course-management/curricula/:curriculumId/cohort/:cohortId/rubric/:rubricId/grade-student",
		component: GGradeStudentByStudent,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	// _________________________________________________________________

	// --------- Question Set ---------

	{
		name: "assignments",
		path: "/course-management/curricula/:curriculumId/assignments",
		component: GQuestionSetLister,
		layout: `${baseURL}/${genericPath}`,
		type: "senior_teacher",
	},

	{
		name: "exams",
		path: "/course-management/curricula/:curriculumId/exams",
		component: GQuestionSetLister,
		layout: `${baseURL}/${genericPath}`,
		type: "senior_teacher",
	},

	{
		name: "polls",
		path: "/course-management/curricula/:curriculumId/polls",
		component: GQuestionSetLister,
		layout: `${baseURL}/${genericPath}`,
		type: "senior_teacher",
	},

	{
		name: "quizzes",
		path: "/course-management/curricula/:curriculumId/quizzes",
		component: GQuestionSetLister,
		layout: `${baseURL}/${genericPath}`,
		type: "senior_teacher",
	},

	{
		name: "surveys",
		path: "/course-management/curricula/:curriculumId/surveys",
		component: GQuestionSetLister,
		layout: `${baseURL}/${genericPath}`,
		type: "senior_teacher",
	},

	{
		name: "projects",
		path: "/course-management/curricula/:curriculumId/projects",
		component: GQuestionSetLister,
		layout: `${baseURL}/${genericPath}`,
		type: "senior_teacher",
	},
	{
		name: "questions_bank",
		path: "/course-management/curricula/:curriculumId/questions-bank",
		component: GQuestionsBank,
		layout: `${baseURL}/${genericPath}`,
	},

	{
		name: "question-set-editor",
		path: "/course-management/curricula/:curriculumId/editor/:questionSetType",
		component: GQuestionSetEditor,
		layout: `${baseURL}/${genericPath}`,
		type: "senior_teacher",
		invisible: true,
	},

	{
		path: "/course-management/curricula/:curriculumId/choose-questions-bank/:questionSetType",
		component: GQuestionsBank,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	// _________________________________________________________________

	// ___________________________Badges_____________________________

	{
		name: "badges",
		path: "/course-management/curricula/:curriculumId/badges",
		component: GBadges,
		layout: `${baseURL}/${genericPath}`,
		type: "senior_teacher",
	},
	// _________________________________________________________________
];
