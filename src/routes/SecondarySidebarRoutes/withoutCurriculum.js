import { baseURL, genericPath } from "engine/config";
import GDiscussion from "logic/Discussion/GDiscussion";
import GCourseModule from "logic/GCoursesManager/GCourseModule/GCourseModule";
import GUnitPlanEditor from "logic/Courses/CourseManagement/UnitPlans/UnitPlanEditor/GUnitPlanEditor";
import GLessonPlan from "logic/Courses/CourseManagement/LessonPlans/GLessonPlan";
import GDetailTabs from "logic/GCoursesManager/Tabs/GDetailTabs";
import GLessonPlanLister from "logic/Courses/CourseManagement/LessonPlans/GLessonPlanLister";
import GUnitPlanLister from "logic/Courses/CourseManagement/UnitPlans/UnitPlanLister/GUnitPlanLister";
import GBadges from "logic/Courses/CourseManagement/Badges/GBadges";
import GBooksLister from "logic/SchoolInitiation/Books/GBooksLister";
import GStudents from "logic/Courses/CourseManagement/Students/GStudents";
import GQuestionSetLister from "logic/Courses/CourseManagement/QuestionSet/GQuestionSetLister";
import GQuestionSetEditor from "logic/Courses/CourseManagement/QuestionSet/GQuestionSetEditor";
import GSolveQuestionSet from "logic/Courses/CourseManagement/QuestionSet/GSolveQuestionSet";
import GSubmissionsLister from "logic/Courses/CourseManagement/QuestionSet/GSubmissionsLister";
import Grading from "logic/grading/Grading";
import GQuestionsBank from "logic/Courses/CourseManagement/QuestionSet/GQuestionsBank";
import GLessonsViewer from "logic/Courses/CourseManagement/Lessons/GLessonsViewer";
import GLessonPlanViewer from "logic/Courses/CourseManagement/LessonPlans/GLessonPlanViewer";
import GRubricLister from "logic/Courses/Rubrics/GRubricLister";
import GRubricEditor from "logic/Courses/Rubrics/GRubricEditor";
import GAttendanceDetailsLister from "logic/Courses/CourseManagement/Attendance/GAttendanceDetailsLister";
import GStudentAttendanceDetails from "logic/Courses/CourseManagement/Attendance/GStudentAttendanceDetails";
import GUnitPlanViewer from "logic/Courses/CourseManagement/UnitPlans/UnitPlanViewer/GUnitPlanViewer";
import GCourseResources from "logic/Courses/CourseManagement/CourseResources/GCourseResources";
import StudentsMarksByItem from "logic/grading/pages/courseFinalMarks/StudentsMarksByItem";
import GStudentsDetailsByRubric from "logic/Courses/Rubrics/GStudentsDetailsByRubric";
import GGradeStudentByStudent from "logic/Courses/Rubrics/GGradeStudentByStudent";
import GOneStudentGrades from "logic/grading/pages/oneStudent/GOneStudentGrades";
import GGradingManagement from "logic/Courses/CourseManagement/Grading/GGradingManagement";
import RItemsSlider from "logic/Courses/CourseManagement/Grading/RItemsSlider";
import GSyllabus from "logic/Courses/Syllabus/GSyllabus";
import GCalender from "logic/calender/GCalender";

export const routes = [
	{
		name: "syllabus",
		path: "/course-catalog/master-course/:courseId/syllabus",
		component: GSyllabus,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
	},
	{
		name: "attendance",
		path: "/course-catalog/master-course/:courseId/attendance",
		component: GAttendanceDetailsLister,
		layout: `${baseURL}/${genericPath}`,
		types: ["student,learner"],
	},
	{
		name: "calander",
		path: "/course-catalog/course/:courseId/calendar",
		component: GCalender,
		layout: `${baseURL}/${genericPath}`,
		types: ["student,learner"],
	},
	{
		path: "/course-management/course/:courseId/cohort/:cohortId/category/:categoryId/modules/view/:viewAsStudent?",
		component: GCourseModule,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
		invisible: true,
	},
	{
		path: "/course-management/course/:courseId/cohort/:cohortId/category/:categoryId/modules/view/:viewAsStudent?/:params",
		component: GCourseModule,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
		invisible: true,
	},
	{
		path: "/course-management/course/:courseId/cohort/:cohortId/modules/view/:viewAsStudent?/:params",
		component: GCourseModule,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
		invisible: true,
	},
	{
		path: "/course-management/course/:courseId/cohort/:cohortId/modules/view/:viewAsStudent?",
		component: GCourseModule,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
		invisible: true,
	},
	{
		name: "modules",
		path: "/course-management/course/:courseId/cohort/:cohortId/modules",
		component: GCourseModule,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
	},
	{
		name: "modules",
		path: "/course-management/course/:courseId/cohort/:cohortId/category/:categoryId/modules",
		component: GCourseModule,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
		invisible: true,
	},
	{
		name: "modules",
		path: "/course-management/course/:courseId/cohort/:cohortId/category/:categoryId/modules/:params",
		component: GCourseModule,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
		invisible: true,
	},
	{
		name: "books",
		path: "/course-management/course/:courseId/cohort/:cohortId/books",
		component: GBooksLister,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
		disabled: true,
	},

	// _________________________________________________________________
	// --------- Start Unit Plans ---------
	{
		name: "unit_plans",
		path: "/course-management/course/:courseId/cohort/:cohortId/unit-plans",
		component: GUnitPlanLister,
		layout: `${baseURL}/${genericPath}`,
	},

	{
		name: "unit_plans",
		path: "/course-management/course/:courseId/cohort/:cohortId/editor/unit-plans",
		component: GUnitPlanEditor,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		name: "unit_plans",
		path: "/course-management/course/:courseId/cohort/:cohortId/editor/unit-plans/:unitPlanId/preview",
		component: GUnitPlanEditor,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		name: "unit_plans",
		path: "/course-management/course/:courseId/cohort/:cohortId/editor/unit-plans/:unitPlanId",
		component: GUnitPlanEditor,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		name: "unit_plans",
		path: "/course-management/course/:courseId/cohort/:cohortId/editor/unit-plans/:unitPlanId/:params",
		component: GUnitPlanEditor,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		name: "unit_plans",
		path: "/course-management/course/:courseId/cohort/:cohortId/unit-plans/:unitPlanId",
		component: GUnitPlanViewer,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	// --------- End Unit Plans ---------

	// --------- Start Lesson Plans ---------
	{
		name: "lesson_plans",
		path: "/course-management/course/:courseId/cohort/:cohortId/lesson-plans",
		component: GLessonPlanLister,
		layout: `${baseURL}/${genericPath}`,
	},

	{
		name: "lesson_plans",
		path: "/course-management/course/:courseId/cohort/:cohortId/lesson/:lessonId/editor/lesson-plans",
		component: GLessonPlan,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		name: "lesson_plans",
		path: "/course-management/course/:courseId/cohort/:cohortId/editor/lesson-plans",
		component: GLessonPlan,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		name: "lesson_plans",
		path: "/course-management/course/:courseId/cohort/:cohortId/lesson/:lessonId/editor/lesson-plans/:lessonPlanId",
		component: GLessonPlan,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		name: "lesson_plans",
		path: "/course-management/course/:courseId/cohort/:cohortId/editor/lesson-plans/:lessonPlanId",
		component: GLessonPlan,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		name: "lesson_plan",
		path: "/course-management/course/:courseId/cohort/:cohortId/lesson-plan/:lessonPlanId",
		component: GLessonPlanViewer,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	// _________________________________________________________________

	// --------- Rubric ---------

	{
		name: "rubrics",
		path: "/course-management/course/:courseId/cohort/:cohortId/rubrics",
		component: GRubricLister,
		layout: `${baseURL}/${genericPath}`,
		disabled: true,
	},

	{
		name: "rubricsEditor",
		path: "/course-management/course/:courseId/cohort/:cohortId/editor/rubrics",
		component: GRubricEditor,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		name: "rubricsEditor",
		path: "/course-management/course/:courseId/cohort/:cohortId/editor/rubric/:rubricId",
		component: GRubricEditor,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		name: "rubricsViewer",
		path: "/course-management/course/:courseId/cohort/:cohortId/rubric/:rubricId/viewer",
		component: GRubricEditor,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		name: "studentsDetails",
		path: "/course-management/course/:courseId/cohort/:cohortId/rubric/:rubricId/students",
		component: GStudentsDetailsByRubric,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		name: "gradeStudentByStudent",
		path: "/course-management/course/:courseId/cohort/:cohortId/rubric/:rubricId/grade-student",
		component: GGradeStudentByStudent,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	// _________________________________________________________________

	// --------- Question Set ---------
	{
		name: "assignments",
		path: "/course-management/course/:courseId/cohort/:cohortId/assignments",
		component: GQuestionSetLister,
		layout: `${baseURL}/${genericPath}`,
	},

	{
		name: "exams",
		path: "/course-management/course/:courseId/cohort/:cohortId/exams",
		component: GQuestionSetLister,
		layout: `${baseURL}/${genericPath}`,
	},

	{
		name: "polls",
		path: "/course-management/course/:courseId/cohort/:cohortId/polls",
		component: GQuestionSetLister,
		layout: `${baseURL}/${genericPath}`,
	},

	{
		name: "quizzes",
		path: "/course-management/course/:courseId/cohort/:cohortId/quizzes",
		component: GQuestionSetLister,
		layout: `${baseURL}/${genericPath}`,
	},

	{
		name: "surveys",
		path: "/course-management/course/:courseId/cohort/:cohortId/surveys",
		component: GQuestionSetLister,
		layout: `${baseURL}/${genericPath}`,
	},

	{
		name: "projects",
		path: "/course-management/course/:courseId/cohort/:cohortId/projects",
		component: GQuestionSetLister,
		layout: `${baseURL}/${genericPath}`,
		disabled: true,
	},

	{
		name: "course_resources",
		path: "/course-management/course/:courseId/cohort/:cohortId/course-resources",
		component: GCourseResources,
		layout: `${baseURL}/${genericPath}`,
		disabled: true,
	},

	{
		name: "questions_bank",
		path: "/course-management/course/:courseId/cohort/:cohortId/questions-bank",
		component: GQuestionsBank,
		layout: `${baseURL}/${genericPath}`,
	},

	{
		name: "question-set-editor",
		path: "/course-management/course/:courseId/cohort/:cohortId/editor/:questionSetType",
		component: GQuestionSetEditor,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		path: "/course-management/course/:courseId/cohort/:cohortId/category/:categoryId/module/:moduleId/editor/:questionSetType",
		component: GQuestionSetEditor,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		name: "question-set-editor",
		path: "/course-management/course/:courseId/cohort/:cohortId/editor/:questionSetType/:questionSetId",
		component: GQuestionSetEditor,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		path: "/course-management/course/:courseId/cohort/:cohortId/category/:categoryId/module/:moduleId/editor/:questionSetType/:questionSetId",
		component: GQuestionSetEditor,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		path: "/course-management/course/:courseId/cohort/:cohortId/module/:moduleId/editor/:questionSetType/:questionSetId",
		component: GQuestionSetEditor,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		path: "/course-management/course/:courseId/cohort/:cohortId/module/:moduleId/editor/:questionSetType",
		component: GQuestionSetEditor,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		path: "/course-management/course/:courseId/cohort/:cohortId/module/:moduleId/student/:questionSetType/:studentStatus",
		component: GSubmissionsLister,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
		types: ["student", "learner"],
	},
	{
		path: "/course-management/course/:courseId/cohort/:cohortId/category/:categoryId/module/:moduleId/student/:questionSetType/:studentStatus",
		component: GSubmissionsLister,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
		types: ["student", "learner"],
	},

	{
		path: "/course-management/course/:courseId/cohort/:cohortId/module/:moduleId/:questionSetType/:questionSetId?/:submitted/:startPage?",
		component: GSolveQuestionSet,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
		types: ["student", "learner"],
	},
	{
		path: "/course-management/course/:courseId/cohort/:cohortId/module/:moduleId/learner/:learnerId/:questionSetType/:questionSetId?/:submitted",
		component: GSolveQuestionSet,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
		types: ["student", "learner"],
	},
	{
		path: "/course-management/course/:courseId/cohort/:cohortId/category/:categoryId/module/:moduleId/:questionSetType/:questionSetId?/:submitted/:startPage?",
		component: GSolveQuestionSet,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
		types: ["student", "learner"],
	},
	{
		path: "/course-management/course/:courseId/cohort/:cohortId/category/:categoryId/module/:moduleId/learner/:learnerId/:questionSetType/:questionSetId?/:submitted",
		component: GSolveQuestionSet,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
		types: ["student", "learner"],
	},

	{
		path: "/course-management/course/:courseId/cohort/:cohortId/category/:categoryId/choose-questions-bank/:questionSetType",
		component: GQuestionsBank,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		path: "/course-management/course/:courseId/cohort/:cohortId/choose-questions-bank/:questionSetType",
		component: GQuestionsBank,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	{
		path: "/course-management/course/:courseId/cohort/:cohortId/category/:categoryId/lesson/:lessonId/meeting/:meetingId",
		component: GLessonsViewer,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		path: "/course-management/course/:courseId/cohort/:cohortId/lesson/:lessonId/meeting/:meetingId",
		component: GLessonsViewer,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},

	// _________________________________________________________________

	// ___________________________Badges_____________________________
	{
		name: "badges",
		path: "/course-management/course/:courseId/cohort/:cohortId/badges",
		component: GBadges,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
	},

	// _______________________Certificates_____________________________
	// {
	// 	name: "certificates",
	// 	path: "/course-management/course/:courseId/cohort/:cohortId/certificates",
	// 	component: {},
	// 	layout: `${baseURL}/${genericPath}`,
	// 	types: ["student", "learner"],
	// 	disabled: true,
	// },
	// ______________________Grads Books______________________________
	{
		name: "grade_book",
		path: "/course-management/course/:courseId/cohort/:cohortId/gradebook",
		component: GGradingManagement,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
		disabled: false,
	},
	{
		path: "/course-management/course/:courseId/cohort/:cohortId/gradebook/items/:itemType/:itemId",
		component: GGradingManagement,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
		invisible: true,
	},
	{
		path: "/course-management/course/:courseId/cohort/:cohortId/gradebook/type/:parentType/:parentId/alternatives/:alternativeType/:alternativeId",
		component: GGradingManagement,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
		invisible: true,
	},
	{
		path: "/course-management/course/:courseId/cohort/:cohortId/gradebook/studentDetails/:studentId",
		component: GGradingManagement,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
		invisible: true,
	},

	// {
	// 	name: "grade_book",
	// 	path: "/course-management/course/:courseId/cohort/:cohortId/gradebook/mark-details", //:groupId//:itemId
	// 	component: StudentsMarksByItem,
	// 	layout: `${baseURL}/${genericPath}`,
	// 	types: ["student", "learner"],
	// 	disabled: true,
	// },

	// {
	// 	name: "grade_book",
	// 	path: "/course-management/course/:courseId/cohort/:cohortId/gradebook/student-item-details/:ItemId/:studentId?",
	// 	component: GOneStudentGrades,
	// 	layout: `${baseURL}/${genericPath}`,
	// 	types: ["student", "learner"],
	// 	disabled: true,
	// },
	// _________________________________________________________________

	{
		name: "discussion",
		path: "/course-management/course/:courseId/cohort/:cohortId/discussion",
		component: GDiscussion,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
		disabled: true,
	},
	// _______________________students_________________________________

	{
		name: "learners",
		path: "/course-management/course/:courseId/cohort/:cohortId/learners",
		component: GStudents,
		layout: `${baseURL}/${genericPath}`,
	},
	// _________________________________________________________________

	// {
	// 	name: "attendance",
	// 	path: "/course-management/course/:courseId/cohort/:cohortId/attendance",
	// 	component: GAttendanceDetailsLister,
	// 	layout: `${baseURL}/${genericPath}`,
	// 	type: "teacher",
	// 	disabled: false,
	// },

	// {
	// 	name: "studentAttendance",
	// 	path: "/course-management/course/:courseId/cohort/:cohortId/attendance/student/:studentId",
	// 	component: GAttendanceDetailsLister,
	// 	layout: `${baseURL}/${genericPath}`,
	// 	type: "teacher",
	// 	invisible: true,
	// },

	// {
	// 	name: "group_sets",
	// 	path: "/course-management/course/:courseId/cohort/:cohortId/group-sets",
	// 	component: {},
	// 	layout: `${baseURL}/${genericPath}`,
	// 	types: ['student','learner'],
	// 	disabled: true,
	// },
	// _________________________________________________________________

	{
		name: "course_information",
		path: "/course-management/course/:courseId/cohort/:cohortId/course-information",
		component: GDetailTabs,
		layout: `${baseURL}/${genericPath}`,
		hidden: "school",
	},
	{
		name: "course_information",
		path: "/course-management/course/:courseId/cohort/:cohortId/category/:categoryId/course-information",
		component: GDetailTabs,
		layout: `${baseURL}/${genericPath}`,
		invisible: true,
	},
	{
		name: "course_overview",
		path: "/course-management/course/:courseId/cohort/:cohortId/course-overview",
		component: GDetailTabs,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
		hidden: "school",
	},
	{
		name: "course_overview",
		path: "/course-management/course/:courseId/cohort/:cohortId/category/:categoryId/course-overview",
		component: GDetailTabs,
		layout: `${baseURL}/${genericPath}`,
		types: ["student", "learner"],
		invisible: true,
	},
];
