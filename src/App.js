import { Switch, Route, Redirect } from "react-router-dom";
import { PublicClientApplication } from "@azure/msal-browser";
import { MsalProvider } from "@azure/msal-react";
import { azureConfig } from "config/azure";
import { Suspense } from "react";
import GenericLayout from "layouts/GenericLayout";
import LandingLayout from "layouts/LandingLayout";
import AuthLayout from "layouts/Auth";
import CartLayout from "layouts/CartLayout";

// function unregisterFirebaseServiceWorker() {
// 	if ("serviceWorker" in navigator) {
// 		navigator.serviceWorker
// 			.getRegistration("/firebase-messaging-sw.js")
// 			.then((registration) => {
// 				if (registration) {
// 					registration
// 						.unregister()
// 						.then(() => {
// 						})
// 						.catch((error) => {
// 							console.error("Error while unregistering Firebase Service Worker:", error);
// 						});
// 				}
// 			})
// 			.catch((error) => {
// 				console.error("Error getting Firebase Service Worker registration:", error);
// 			});
// 	}
// }

// Event listener for page unload
const App = () => {
	const publicClientApplication = new PublicClientApplication({
		auth: {
			clientId: azureConfig.appId,
			redirectUri: azureConfig.redirectUri,
		},
	});
	// useEffect(() => {
	// 	if ("serviceWorker" in navigator) {
	// 		Helper.cl(navigator, "appjsworker f1 ");
	// 		navigator.serviceWorker.ready.then((registration) => {
	// 			Helper.cl(registration, "appjsworker f2 ");
	// 			registration.unregister().then(() => {
	// 				Helper.cl("appjsworker f3 ");
	// 				registration.update();
	// 				Helper.cl("appjsworker f4 ");
	// 			});
	// 		});

	// 		window.addEventListener("load", () => {
	// 			navigator.serviceWorker
	// 				.register("/firebase-messaging-sw.js")
	// 				.then((registration) => {
	// 				})
	// 				.catch((error) => {
	// 					console.error("appjsworker Service Worker registration failed:", error);
	// 				});
	// 		});
	// 	}

	// 	window.addEventListener("unload", () => {
	// 		unregisterFirebaseServiceWorker();
	// 	});
	// }, []);
	return (
		<Suspense fallback="loading112">
			<MsalProvider instance={publicClientApplication}>
				<Switch>
					<Route path={process.env.REACT_APP_BASE_URL + "/g"} render={(props) => <GenericLayout {...props} />} />
					<Route path={process.env.REACT_APP_BASE_URL + "/auth"} render={(props) => <AuthLayout {...props} />} />
					<Route path={process.env.REACT_APP_BASE_URL + "/landing"} render={(props) => <LandingLayout {...props} />} />
					<Route path={process.env.REACT_APP_BASE_URL + "/cart"} render={(props) => <CartLayout {...props} />} />
					<Redirect to={process.env.REACT_APP_BASE_URL + "/auth/login"} />
					{/* <Redirect to={process.env.REACT_APP_BASE_URL + "/landing/home"} /> */}
				</Switch>
				<div id="sweetAlert-98iop76524"></div>
			</MsalProvider>
		</Suspense>
	);
};

export default App;
