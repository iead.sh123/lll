import React from "react";
import PerfectScrollbar from "perfect-scrollbar";
import routes from "routes/cart";
import { Route, Switch } from "react-router-dom";

var ps;

function CartLayout() {
	const fullPages = React.useRef();

	React.useEffect(() => {
		if (navigator.platform.indexOf("Win") > -1) {
			ps = new PerfectScrollbar(fullPages.current);
		}
		return function cleanup() {
			if (navigator.platform.indexOf("Win") > -1) {
				ps.destroy();
			}
		};
	});

	const getRoutes = (routes) => {
		return routes.map((prop, key) => {
			if (prop.collapse) {
				return getRoutes(prop.views);
			}

			if (prop.layout === process.env.REACT_APP_BASE_URL + "/cart") {
				return <Route path={prop.layout + prop.path} component={prop.component} key={key} />;
			} else {
				return null;
			}
		});
	};
	return (
		<div ref={fullPages}>
			<Switch>{getRoutes(routes)}</Switch>
		</div>
	);
}

export default CartLayout;
