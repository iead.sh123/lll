import React, { useEffect, useState } from "react";
import { routes as secondary_sidebar_routes } from "routes/SecondarySidebarRoutes/withoutCurriculum";
import { routes as secondary_sidebar_with_curriculum_routes } from "routes/SecondarySidebarRoutes/withCurriculum";

import { Route, Switch, useLocation } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { genericPath } from "engine/config";
import { Services } from "engine/services";
import { get } from "config/api";
import RSecondarySidebar from "view/SecondarySidebar/RsecondarySidebar";
import PerfectScrollbar from "perfect-scrollbar";
import GenericUILister from "logic/GenericUi/GenericUILister";
import withDirection from "hocs/withDirection";
import initiateToken from "views/auth/FireBaseServices";
import Sidebar from "components/Sidebar/Sidebar.js";
import routes from "routes/namedGenericRoutes";
import Loader from "utils/Loader";
import NavbarLayouts from "components/widgets/navbar";
import { sidebarOnline } from "engine/config";
import localeSidebarRoutes from "routes/localeSidebarRoutes";
import useIMInit from "views/Teacher/IM/useIMInit";
import { IMApi } from "api/IMNewCycle";
import { removeApisNotWorkingFromLocale } from "engine/config";
import { online } from "engine/config";
import { SecondarySidebarRoutes } from "routes/secondarySidebarRoutes";

var ps;

function GenericLayout(props) {
	const location = useLocation();
	const dispatch = useDispatch();
	if (!removeApisNotWorkingFromLocale) {
		useIMInit();
	}
	const tempSwitchRoutes = [];
	const tempSwitchSecondSideBarRoutes = [];

	const courseIdMatch =
		location.pathname.match(/\/course-management\/course\/([^/]+)/) || location.pathname.match(/\/course-catalog\/([^/]+)/);
	const curriculumIdMatch = location.pathname.match(/\/course-management\/curricula\/([^/]+)/);
	const courseId = courseIdMatch ? courseIdMatch[1] : null;
	const curriculumId = curriculumIdMatch ? curriculumIdMatch[1] : null;

	const mainPanel = React.useRef();
	const [switchRoutes, setSwitchRoutes] = useState([]);
	const [sidebarRoutes, setSidebarRoutes] = useState([]);
	const [switchSecondSideBarRoutes, setSwitchSecondSideBarRoutes] = useState([]);
	const [sidebarSecondSideBarRoutes, setSidebarSecondSideBarRoutes] = useState([]);

	const [showSecondarySidebar, setShowSecondarySidebar] = useState(false);

	const { user } = useSelector((state) => state?.auth);

	const curriculumDirectorTypes = ["school_advisor", "senior_teacher", "principal", "Curriculum Director", "curriculum_director"].includes(
		user?.type
	);
	const adminTypes = ["admin", "Admin"].includes(user?.type);
	const courseAdministratorTypes = ["Course Administrator", "course_administrator", "Systems Administrator"].includes(user?.type);
	const facilitatorTypes = ["teacher", "facilitator", "Teacher", "Facilitator"].includes(user?.type);
	const learnerTypes = ["learner", "student", "Learner", "Student"].includes(user?.type);
	const mentorTypes = ["Mentor"].includes(user?.type);
	const parentTypes = ["parent"].includes(user?.type);

	useEffect(() => {
		if (navigator.platform.indexOf("Win") > -1) {
			document.documentElement.className += " perfect-scrollbar-on";
			document.documentElement.classList.remove("perfect-scrollbar-off");
			ps = new PerfectScrollbar(mainPanel.current);
		}
		return function cleanup() {
			if (navigator.platform.indexOf("Win") > -1) {
				ps.destroy();
				document.documentElement.className += " perfect-scrollbar-off";
				document.documentElement.classList.remove("perfect-scrollbar-on");
			}
		};
	});

	useEffect(() => {
		document.documentElement.scrollTop = 0;
		document.scrollingElement.scrollTop = 0;
		// mainPanel.current.scrollTop = 0;
	}, [location]);

	useEffect(() => {
		if (location.pathname.includes("/course-management/") || location.pathname.includes("/course-catalog/")) {
			setShowSecondarySidebar(true);
		} else {
			setShowSecondarySidebar(false);
		}
	}, [location]);

	//minimize_and_maximize_sidebar
	useEffect(() => {
		const PathName = location.pathname.includes("/course-management/") || location.pathname.includes("/course-catalog/");
		const Paths =
			location.pathname.startsWith(`/${genericPath}/courses-manager`) || location.pathname.includes(`/${genericPath}/course-catalog`);

		if (Paths) {
			document.body.classList.add("sidebar-mini");
		}
		// document.body.classList.add("sidebar-mini");

		const minimize_sidebar = () => {
			if (!document.body.classList.contains(PathName ? "sidebar-mini-with-second-sidebar" : "sidebar-mini")) {
				document.body.classList.add(PathName ? "sidebar-mini-with-second-sidebar" : "sidebar-mini");
			}
		};

		const maximize_sidebar = () => {
			if (!PathName) {
				document.body.classList.remove("sidebar-mini-with-second-sidebar");
			} else {
				if (document.body.classList.contains("sidebar-mini-with-second-sidebar") && !Paths) {
					document.body.classList.remove("sidebar-mini");
					document.body.classList.remove("sidebar-mini-with-second-sidebar");
				} else if (document.body.classList.contains("sidebar-mini-with-second-sidebar")) {
					document.body.classList.remove("sidebar-mini-with-second-sidebar");
				} else if (document.body.classList.contains("sidebar-min") && !Paths) {
					document.body.classList.remove("sidebar-mini");
				} else if (!Paths) {
					document.body.classList.remove("sidebar-mini");
				}
			}
		};

		minimize_sidebar();
		if (showSecondarySidebar) {
			minimize_sidebar();
		} else {
			maximize_sidebar();
			// minimize_sidebar();
		}
	}, [showSecondarySidebar, location]);

	const [sidebarLoading, setSidebarLoading] = useState();

	const getData = async () => {
		setSidebarLoading(true);
		const sidebarData = await get(`${Services.auth_organization_management.backend}api/sidebar/getSidebar`);
		if (sidebarData) {
			const tempSidebarObjects = getRoutesNew(routes, sidebarData.data.data.sidebar ?? []);
			setSwitchRoutes(tempSwitchRoutes);
			setSidebarRoutes(tempSidebarObjects);
			setSidebarLoading(false);
		}
		if (!removeApisNotWorkingFromLocale) {
			await initiateToken(user?.id);
			IMApi.setOnline(1);
		}
	};

	const getDataLocale = async () => {
		setSidebarLoading(true);
		await initiateToken(user?.id);

		const tempSidebarObjects = getRoutesNew(
			routes,
			curriculumDirectorTypes
				? localeSidebarRoutes["curriculum_director"]
				: adminTypes
				? localeSidebarRoutes["admin"]
				: courseAdministratorTypes
				? localeSidebarRoutes["course_adminiStrator"]
				: facilitatorTypes
				? localeSidebarRoutes["facilitator"]
				: learnerTypes
				? localeSidebarRoutes["learner"]
				: localeSidebarRoutes["admin"]
		);
		setSwitchRoutes(tempSwitchRoutes);
		setSidebarRoutes(tempSidebarObjects);
		setSidebarLoading(false);
	};

	//To Fetch First Sidebar Routes
	// useEffect(() => {
	// 	if (sidebarOnline) {
	// 		getDataLocale();
	// 	} else {
	// 		getData();
	// 	}
	// }, []);

	useEffect(() => {
		if (user) {
			if (sidebarOnline) {
				getDataLocale();
			} else {
				getData();
			}
		} else {
			getDataLocale();
		}
	}, [user?.current_organization, user?.current_type, user?.organization_id]);

	useEffect(() => {
		// if (courseId ? secondary_sidebar_routes : curriculumId ? secondary_sidebar_with_curriculum_routes : []) {
		if (online ? secondary_sidebar_routes : SecondarySidebarRoutes) {
			const tempSecondSidebarObjects = getRouteToSecondSideBar();
			setSwitchSecondSideBarRoutes(tempSwitchSecondSideBarRoutes);
			setSidebarSecondSideBarRoutes(tempSecondSidebarObjects);
		}
	}, [courseId, curriculumId]);

	const getRouteToSecondSideBar = () => {
		let tempSecondSidebarObjects = [];
		let rS = null;

		let userType = user && user?.type;
		let organizationType = user && user?.organization_type?.toLowerCase();

		const seniorTeacherTypes = ["school_advisor", "senior_teacher", "principal", "Curriculum Director"].includes(userType);
		const adminTypes = ["admin", "Course Administrator", "Systems Administrator"].includes(userType);
		const teacherTypes = ["teacher", "facilitator"].includes(userType);
		const studentTypes = ["learner", "student"].includes(userType);
		const anotherTypes = ["Mentor"].includes(userType);
		const parentTypes = ["parent"].includes(userType);

		let removeTabsFromSchool =
			organizationType == "school"
				? (online ? secondary_sidebar_routes : SecondarySidebarRoutes).filter((el) => el.hidden !== "school")
				: online
				? secondary_sidebar_routes
				: SecondarySidebarRoutes;

		let secondaryRoutes = [];
		if (courseId) {
			if (studentTypes) {
				secondaryRoutes = removeTabsFromSchool.filter((route) => route?.types?.includes("student") || route?.types?.includes("learner"));
			} else if (teacherTypes || seniorTeacherTypes) {
				secondaryRoutes = removeTabsFromSchool.filter((route) => route?.name !== "course_information");
			} else {
				secondaryRoutes = removeTabsFromSchool;
			}
		} else if (curriculumId) {
			secondaryRoutes = secondary_sidebar_with_curriculum_routes.filter((route) => route?.type && route?.type == "senior_teacher");
		}

		for (let i = 0; i < secondaryRoutes.length; i++) {
			rS = (
				<Route path={`${secondaryRoutes[i]?.layout + secondaryRoutes[i]?.path}`} component={secondaryRoutes[i]?.component} key={i} exact />
			);
			if (rS) {
				tempSwitchSecondSideBarRoutes.push(rS);
			}

			if (secondaryRoutes && secondaryRoutes.length > 0) {
				tempSecondSidebarObjects.push({
					...secondaryRoutes[i],
					path: secondaryRoutes[i].path,
					component: secondaryRoutes[i].component,
					layout: secondaryRoutes[i].layout,
					icon: secondaryRoutes[i].icon,
					mini: secondaryRoutes[i].mini,
					disabled: secondaryRoutes[i].disabled,
				});
			}
		}

		return tempSecondSidebarObjects;
	};

	const getRoutesNew = (routes, sidebarEntries, level = 0) => {
		let tempSidebarObjects = [];
		if (!sidebarEntries || sidebarEntries.length < 1) return <>No Data</>;
		let result = false;
		sidebarEntries.map((side, key) => {
			const prop = side;
			if (prop.collapse) {
				let innerSidebarRoutes = getRoutesNew(routes, prop.items, level + 1);
				prop.items = innerSidebarRoutes;
			} else {
				let r = null;

				for (let i = 0; i < routes.length; i++) {
					if (routes[i]?.table_name) {
						r = (
							<Route
								path={`${routes[i]?.layout + routes[i]?.path}`}
								render={() => <GenericUILister table_name={routes[i]?.table_name} />}
								key={key}
							/>
						);
					} else {
						r = <Route path={`${routes[i]?.layout + routes[i]?.path}`} component={routes[i]?.component} key={key} />;
					}

					if (r) {
						tempSwitchRoutes.push(r);
					}
				}
			}
			result = routes.filter((route) => route.name == side.name);

			if (result && result.length > 0) {
				tempSidebarObjects.push({
					...prop,
					path: result[0].path,
					component: result[0].component,
					layout: result[0].layout,
					icon: result[0].icon,
					mini: result[0].mini,
					disabled: side.disabled,
				});
			}
		});

		return tempSidebarObjects;
	};
	return (
		<div className="wrapper">
			<Sidebar {...props} routes={sidebarRoutes} sidebarLoading={sidebarLoading} bgColor={"light-blue"} activeColor={"primary"} />
			{showSecondarySidebar && (
				<RSecondarySidebar secondSideBarRoutes={sidebarSecondSideBarRoutes} bgColor={"light-blue"} activeColor={"primary"} />
			)}

			<div className={props.dir ? "main-panel" : "main-panel-rtl"} ref={mainPanel}>
				<div className="content">
					<NavbarLayouts {...props} dashboard={true} />
					{sidebarLoading ? <Loader /> : <>{switchRoutes && switchRoutes.length > 0 && <Switch>{switchRoutes}</Switch>}</>}
					{switchSecondSideBarRoutes && <Switch>{switchSecondSideBarRoutes}</Switch>}
					{/* {<>{switchRoutes && switchRoutes.length > 0 && <Switch>{switchRoutes}</Switch>}</>}
					{switchSecondSideBarRoutes && <Switch>{switchSecondSideBarRoutes}</Switch>} */}
				</div>
			</div>
		</div>
	);
}

export default withDirection(GenericLayout);
